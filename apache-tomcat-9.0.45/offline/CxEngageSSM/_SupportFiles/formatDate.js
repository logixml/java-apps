//This sample displays an alert box

function formatDate(dateString, offset)
{
	var date = new Date(new Date(dateString).getTime() - ((offset || 0) * 60000));
	return date.toLocaleString().replace("UTC", "");
}