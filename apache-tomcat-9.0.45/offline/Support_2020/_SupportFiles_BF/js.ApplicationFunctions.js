/*
 * Copyright (c) 2000 - 2020 by Benefitfocus.com, Inc., All Rights Reserved
 */
 
/**
 * Hide all the filters and show the a particular filter on side bar
 * @param {string} filtername - The ID of the element that should be displayed
 */
function showFilters(filtername) {	
	$('.filters').hide();
	$('#'+filtername+'.filters').show();
	$('#divSidebarMain').toggleClass('closed');
	$('#divSidebar').delay(500).animate({
	scrollTop: 0
	}, 'slow');
}

/**
 * Sort a table element in a given order
 * @param {Object} table - The table object
 * @param {string} order - The order in which the table should be sorted. asc or desc
 */
function sortTable(table, order) {
    var asc   = order === 'asc',
        tbody = table.find('tbody');

    tbody.find('tr').sort(function(a, b) {
        if (asc) {
            return $('td:first', a).text().localeCompare($('td:first', b).text());
        } else {
            return $('td:first', b).text().localeCompare($('td:first', a).text());
        }
    }).appendTo(tbody);
}

/**
 * Open and Close the Left Nav sidebar
 */
function openCloseLeftNav() {
	$('#divPage').toggleClass('closed').toggleClass('open');
	var Sidebar = $('#divPage').hasClass('closed') ? 'closed':'open';
	$('#Sidebar').val(Sidebar);
}

/**
 * Clear all NavMaster Hidden Fields and Control Values for Filters
 */
function clearAll(report) {
	//Reset Employee Filter values and refresh rowEmployeeCount div element
	$('#txtEmployeeMin').val('');
	$('#txtEmployeeMax').val('');
	$('#inpHdnEmployeeMin').val('');
	$('#inpHdnEmployeeMax').val('');

	//Reset Point in Time Filter values and refresh rowPointsInTime div element
	$('#inpSelListPointInTimeMonthStart').val('');
	$('#inpSelListPointInTimeYearStart').val('');
	$('#inpHdnPointInTimeYearStart').val('');
	$('#inpHdnPointInTimeMonthStart').val('');
	$('#inpHdnPointInTimeMonthNameStart').val('');
	$('#inpHdnPointInTimeDateStart').val('');

	// Reset Benefit Type Traditional
	$('#inpHdnBenefitTypeTraditionalFilter_temp').val('');
	$('#inpHdnBenefitTypeTraditionalFilter_Captions_temp').val('');
	$('#inpHdnBenefitTypeTraditionalFilter').val('');
	$('#inpHdnBenefitTypeTraditionalFilter_Captions').val('');
	
	// Reset Benefit Type Voluntary
	$('#inpHdnBenefitTypeVoluntaryFilter_temp').val('');
	$('#inpHdnBenefitTypeVoluntaryFilter_Captions_temp').val('');
	$('#inpHdnBenefitTypeVoluntaryFilter').val('');
	$('#inpHdnBenefitTypeVoluntaryFilter_Captions').val('');
	
	// Reset Benefit Type InVoluntary
	$('#inpHdnBenefitTypeInVoluntaryFilter_temp').val('');
	$('#inpHdnBenefitTypeInVoluntaryFilter_Captions_temp').val('');
	$('#inpHdnBenefitTypeInVoluntaryFilter').val('');
	$('#inpHdnBenefitTypeInVoluntaryFilter_Captions').val('');
	
	$('#inpHdnBenefitTypes').val('');
	$('#inpHdnBenefitTypes_Captions').val('');	

	//Reset Sic Codes
	$('#inpHdnSicCodes').val('');
	$('#inpHdnDivisionFilter').val('');
	$('#inpHdnDivisionFilter_Captions').val('');
	$('#inpHdnSicCodesDesc').val('');
	$('#inpHdnDivisionFilter_temp').val('');
	$('#inpHdnDivisionFilter_Captions_temp').val('');
	$('#inpHdnSicCodesJsonArray').val('');
	$('#inpHdnSicCodes_Temp').val('');
	
	//Reset Geographic details
	$('#txtZipCode').val('')
	$('#inpHdnZipCode').val('');
	$('#inpHdnStateCodes').val('');
	$('#inpHdnStateCodesDesc').val('');
	$('#inpHdnStateCodesDesc_Temp').val('');
	$('#inpHdnZipAndStateCodesDesc').val('');
	$('#inpHdnStateCodesJsonArray').val('');

	//Ajax refresh the following divs - rowPointsInTime,rowEmployeeCount,rowBenefits,rowIndustryDetails
	rdAjaxRequestWithFormVars('rdAjaxCommand=RefreshElement&rdRefreshElementID=rowPointsInTime,rowEmployeeCount,rowBenefits,rowIndustryDetails,rowsFiltersBenefitTypeTraditional,rowsFiltersIndustryDetails,rowsFiltersBenefitTypeVoluntary,rowsFiltersBenefitTypeInVoluntary,rowGeographicDetails,rowsFiltersGeographyDetails&rdReport=' + report + '','false','',true,null,null,['','',''],true);
}

/**
 * Close the Sidebar - NavMaster
 */
function closeLeftNav() {
	$('#divSidebarMain').toggleClass('closed');
}

/**
 * Save Point in Time
 */
function savePointInTime(report) {

	// Return today's date and time
	const currentTime = new Date();

	// returns the month (from 0 to 11)
	const currentMonth = currentTime.getMonth() + 1;

	// returns the year (four digits)
	const currentYear = currentTime.getFullYear();

	// Validate correct month and year
	if (($('#inpSelListPointInTimeMonthStart').val() == '' && $('#inpSelListPointInTimeYearStart').val() != '') || 
		 $('#inpSelListPointInTimeMonthStart').val() != '' && $('#inpSelListPointInTimeYearStart').val() == '') {
		alert('Please check the Month and/or year selection');
		return;
	}

	// Validate that month is an already completed month, i.e. the following is an error:  
	//     if year is current year and selected month is greater than or equal to current month
	if ( parseInt($('#inpSelListPointInTimeYearStart').val()) === currentYear && parseInt($('#inpSelListPointInTimeMonthStart').val()) >= currentMonth ) {
		alert('Month must be a month that has already completed.');
		return;
	}

	//Save Data to Hidden Fields
	$('#inpHdnPointInTimeMonthStart').val($('#inpSelListPointInTimeMonthStart').val());	
	$('#inpHdnPointInTimeMonthNameStart').val($('#inpSelListPointInTimeMonthStart').find(':selected').text());	
	$('#inpHdnPointInTimeYearStart').val($('#inpSelListPointInTimeYearStart').val());	

	$('#inpHdnPointInTimeDateStart').val($('#inpSelListPointInTimeYearStart').val() + '-' + $('#inpSelListPointInTimeMonthStart').val());	

	$('#divSidebarMain').toggleClass('closed');
	
	//Ajax refresh the following divs - rowPointsInTime
	rdAjaxRequestWithFormVars('rdAjaxCommand=RefreshElement&rdRefreshElementID=rowPointsInTime&rdReport=' + report + '','false','',true,null,null,['','',''],true);
	
}

/**
 * Save Employee Counts
 */
function saveEmployeeCounts(report) {
	var empMinVal = $('#txtEmployeeMin').val();
	var empMaxVal = $('#txtEmployeeMax').val();
	var decimalPattern = /[-+]|[\.]|[eE]/gm;	//regular exp to validate if any decimals or exponentials are entered.

	//If one of the Min or Max Employee Count is blank then error	
	if ((empMinVal.trim() == '' && empMaxVal.trim() != '') || 
		 empMinVal.trim() != '' && empMaxVal.trim() == '') {
		alert('Please verify min and/or max Employee counts are entered');
		return;
	}

	//Validation of negative counts
	//If both of the Min and Max Employee Count are equal 0 then error
	if ((empMinVal.trim() == '0' && empMaxVal.trim() == '0') ||
		empMinVal.trim().match(decimalPattern) || empMaxVal.trim().match(decimalPattern)) {
		alert('Please enter valid employee counts');
		return;
	}

	//Validation of Min and Max Employee Range
	if (parseInt(empMinVal) > parseInt(empMaxVal)) {
		alert('Please verify the max Employee count is greater than min Employee count');
		return;
	}

	//Populate Hidden fields with data from controls
	$('#inpHdnEmployeeMin').val(empMinVal);	
	$('#inpHdnEmployeeMax').val(empMaxVal);

	$('#divSidebarMain').toggleClass('closed');

	//Ajax refresh the following divs - rowEmployeeCount
	rdAjaxRequestWithFormVars('rdAjaxCommand=RefreshElement&rdRefreshElementID=rowEmployeeCount&rdReport=' + report  +'','false','',true,null,null,['','',''],true);	
}

/**
 * Save Industry Details
 */
function saveIndustryDetails(report, sicCodes) {
	//transform comma seperated sic codes to json array format
	var arr = sicCodes.split(',');
	var temp = '["' + arr.join('","') + '"]';	//json array
	$('#inpHdnSicCodesJsonArray').val(temp);
	$('#inpHdnSicCodes').val(sicCodes);

	$('#divSidebarMain').toggleClass('closed');
	
	//Ajax refresh the following divs - rowIndustryDetails
	rdAjaxRequestWithFormVars('rdAjaxCommand=RefreshElement&rdRefreshElementID=rowIndustryDetails&rdReport=' + report + '','false','',true,null,null,['','',''],true);	
}

/**
 * Save BenefitTypes
 */
function saveBenefitTypes(report,maxBenefitTypes) {
    //Save Data to Hidden Fields    
    var benefitTypeArray = [];
    var benefitCaptionArray = [];
    var i = 0;
    if($('#inpHdnBenefitTypeTraditionalFilter_Captions').val() != '' && $('#inpHdnBenefitTypeTraditionalFilter').val() != '') {
        benefitCaptionArray[i] = $('#inpHdnBenefitTypeTraditionalFilter_Captions').val();
        benefitTypeArray[i] = $('#inpHdnBenefitTypeTraditionalFilter').val();
        i++;
    }
    if($('#inpHdnBenefitTypeVoluntaryFilter_Captions').val() != '' && $('#inpHdnBenefitTypeVoluntaryFilter').val() != '') {
        benefitCaptionArray[i] = $('#inpHdnBenefitTypeVoluntaryFilter_Captions').val();
        benefitTypeArray[i] = $('#inpHdnBenefitTypeVoluntaryFilter').val();
        i++;
    }
    if($('#inpHdnBenefitTypeInVoluntaryFilter_Captions').val() != '' && $('#inpHdnBenefitTypeInVoluntaryFilter').val() != '') {
        benefitCaptionArray[i] = $('#inpHdnBenefitTypeInVoluntaryFilter_Captions').val();
        benefitTypeArray[i] = $('#inpHdnBenefitTypeInVoluntaryFilter').val();
    }
    
    $('#inpHdnBenefitTypes').val(benefitCaptionArray.join());
    $('#inpHdnBenefitTypes_Captions').val(benefitCaptionArray.join());
            
    var valueArray = $('#inpHdnBenefitTypes').val().split(',');
    if(valueArray.length <= maxBenefitTypes) {
        var selectedCaptions = $('#inpHdnBenefitTypes_Captions').val().trim();
        var captionArray = selectedCaptions.split(',');    
        var captionSize = valueArray.length;
        if(captionSize > 1) {
            var tempCaption = captionArray[0].substring(0,20) + "... +" + (captionSize-1) +  " more...";
            $('#inpHdnBenefitTypes_Captions').val(tempCaption);
        }
    }else {
        alert("Please select a maximum of "+ maxBenefitTypes +" benefit types");
        return false;
    }

    $('#divSidebarMain').toggleClass('closed');
    
    //Ajax refresh the following divs - rowBenefits
    rdAjaxRequestWithFormVars('rdAjaxCommand=RefreshElement&rdRefreshElementID=rowBenefits&rdReport=' + report +'','false','',true,null,null,['','',''],true);    
}

/**
 * Move the popup window to center of the screen and add custom styling
 * Apply custom styling on the popup window
 * @param {string} popupDivID - The ID of the element that should be displayed on the center
 */
function centerPopup(popupDivID)
{
	$('#ppAddBookmarks_'+popupDivID).attr('rdlocation','center');
	$('#rdPopupPanelTable_ppAddBookmarks_' + popupDivID).attr('class','bookmark-save-rdPopuppanel');	
}

/**
 * Add caption to a popup window
 * Apply custom styling on the popup window
 * @param {string} divId - The ID of the popup panel
 * @param {string} caption - Text to be displayed
 */
function ppPnlCaptionTitle(divId, caption)
{
	$('#'+divId).text(caption);
	$('#'+divId).attr('class','bookmark-save-rdPopupPanelTitleCaption');
}

/**
 * Set font style of an element to Benton Sans Light
 * @param {string} divID - The ID of the element
 */
function captionFont(divId)
{
	$('#'+divId+'-Caption').attr('style','color:#FFFFFF !important; font-family: Benton Sans Light;');
}

/**
 * Clear the text on an input text box on click event
 * Trim spaces from input text box on blur envent
 * @param {string} divID - The ID of the iput text box
 */
function trimInputText(divId)
{
	//empty the text box on click
	$('#'+divId+':text').click(function(){$('#'+divId+':text').val('')});
	//trim spaces for description
	$('#'+divId).blur(function()
	{
		$(this).val($.trim($(this).val())); 	
		return true;
	});
}

/**
 * Action for Run Benchmark click event
 */
function doRunBenchmark(report)
{
	if (($('#inpHdnPointInTimeDateStart').val() == '') || ($('#inpHdnBenefitTypes_Captions').val() == '')) {
        alert('Point in Time and Benefits selection are required to run benchmark');
        return false;
    }
	
	//Set flag to indicate run Benchmark was run
	$('#inpHdnRunBenchmark').val('1');
	
	//Update divContent	
	rdAjaxRequestWithFormVars('rdAjaxCommand=RefreshElement&rdRefreshElementID=divContent&rdReport=' + report +'','false','',true,null,null,['','',''],true);	
	
	return true;
}

/**
 * Save Geography Details
 *
 * StateCodes and Zipcode are set in the hidden fields based on user selection in the controls
 */
function saveGeographyDetails(report) {

	var zipCode = $('#txtZipCode').val();
	var stateCodes = $('#inpHdnStateCodes').val();
	var decimalPattern = /[-+]|[\.]|[\,]|[eE]/gm;	//regular exp to validate if any decimals or exponentials are entered.

	//Validation of negative value
	if (zipCode.trim() == '0' || zipCode.trim().match(decimalPattern)) {
		alert('Please enter valid Zip code');
		return;
	}

	if (zipCode.trim().length !=0 && zipCode.trim().length != 5) {
		alert('Zip code should be 5 digits');
		return;
	}

	//Populate Hidden fields with data from controls
	$('#inpHdnZipCode').val(zipCode);
    $('#inpHdnStateCodes').val(stateCodes);

	 if($('#inpHdnZipCode').val().length !=0 && $('#inpHdnStateCodes').val().length !=0){
	 	$('#inpHdnZipAndStateCodesDesc').val($('#inpHdnZipCode').val()+','+$('#inpHdnStateCodesDesc').val());
	 }else if($('#inpHdnZipCode').val().length !=0 && $('#inpHdnStateCodes').val().length ==0){
	 	$('#inpHdnZipAndStateCodesDesc').val($('#inpHdnZipCode').val())
	 }else if($('#inpHdnZipCode').val().length ==0 && $('#inpHdnStateCodes').val().length !=0){
	 	$('#inpHdnZipAndStateCodesDesc').val($('#inpHdnStateCodesDesc').val())
	 }
	 
	 //transform comma seperated state codes to json array format
	var arr = stateCodes.split(',');
	var temp = '["' + arr.join('","') + '"]';	//json array
	if(temp != '[""]')
	{
		$('#inpHdnStateCodesJsonArray').val(temp);
	}
	$('#divSidebarMain').toggleClass('closed');

    //Ajax refresh the rowGeographicDetails
    rdAjaxRequestWithFormVars('rdAjaxCommand=RefreshElement&rdRefreshElementID=rowGeographicDetails&rdReport=' + report + '','false','',true,null,null,['','',''],true);
}

/**
 * Add selected Industry Groups to the selection list
 *
 * SIC Codes and the group description are set in the hidden fields based on user selection in the controls.
 */
function addIndustryCodesToFilters(sicCodes,maxIndustrySicCodes, report)
{
	if(sicCodes != '')
	{
		var valueArray = sicCodes.split(',');
		if(valueArray.length <= maxIndustrySicCodes)
		{
			var selectedCaptions = $('#inpHdnSicCodesDesc_Temp').val().trim();
			var captionArray = selectedCaptions.split(',');
			var captionSize = valueArray.length;
			if(captionSize > 1)
			{
				var tempCaption = captionArray[0].substring(0,20) + "... +" + (captionSize-1) +  " more...";
				$('#inpHdnSicCodesDesc').val(tempCaption);
			}
			else
			{
				$('#inpHdnSicCodesDesc').val($('#inpHdnSicCodesDesc_Temp').val().trim());
			}
			$('#inpHdnSicCodes_Temp').val(sicCodes);

			//refresh element
			rdAjaxRequestWithFormVars('rdAjaxCommand=RefreshElement&rdRefreshElementID=rowMajorGroupsSelectedText&rdReport=' + report + '','false','',true,null,null,['','',''],true);
			$('#divSaveIndustryDetails').show();
		}
		else
		{
			alert("Please select a maximum of "+ maxIndustrySicCodes +" industry codes");
		}
	}
	else
	{
		//refresh element
		rdAjaxRequestWithFormVars('rdAjaxCommand=RefreshElement&rdRefreshElementID=rowMajorGroupsSelectedText&rdReport=' + report + '','false','',true,null,null,['','',''],true);
		$('#rowMajorGroups').hide();
		$('#divSaveIndustryDetails').show();
	}

}

/**
 * Clear selected SIC codes on change of Division Filter
 *
 * SIC Codes and the group description are set in the hidden fields based on user selection in the controls.
 */
function clearIndustryOnDivisionFiltersChange()
{
	//Reset Sic Codes on change of division filter selection
	$('#inpHdnSicCodes').val('');
	$('#inpHdnSicCodesDesc').val('');	
	$('#inpHdnSicCodesJsonArray').val('');
	$('#inpHdnSicCodes_Temp').val('');	
}

/**
 * A generic function that would call a function from string
 *
 * @param {string} functionName - The name of the function to be called from here.
 */
function evaluateFunction(functionName) {
	window[functionName]();
}
