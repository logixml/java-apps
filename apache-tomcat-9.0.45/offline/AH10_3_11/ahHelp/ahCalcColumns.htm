<html>
<head>
<link rel="stylesheet" type="text/css" href="../AdHocHelp.css" />
</head>
<body style='border:0;overflow:visible;'>

<h4><a name="_Toc297114519" class="title">Calculated Columns</a></h4>


<p class=MsoNormal><i>Calculated Columns </i>offer the ability to create new
columns for the report based on a specified formula applied to data from
existing columns.</p>


<p class=MsoNormal>In addition to the availability of data source columns,
customized columns can be created that consist of calculations performed on
data from other columns in the report. Calculations are performed with date,
numeric and non-numeric data types. The operands of the formula are either
constants or the names of existing data columns included in the report. Users
can create formulas from the six provided operators, use the predefined
functions or utilize any SQL function supported by the selected report
database.</p>


<p class=MsoNormal><img border=0 width=576 height=348
src="Report_Design_Guide_files/image074.jpg"></p>


<p class=MsoNormal>The goal of the calculated column process is to create and
name a <i>Definition</i> that the reporting database can interpret and return
data. Consequently, the <i>Definition</i> must conform to the reporting DBMS SQL
syntax rules for a column.</p>


<p class=MsoNormal>It is not required that any of the helpful controls are used
to create the <i>Definition</i>. Knowledgeable users can simply enter the
definition in the text box. It is highly recommended, however, that columns are
inserted into the <i>Definition</i> by clicking on the column from the data
object/column tree. The column reference will be placed at the last cursor
position in the <i>Definition</i> textbox.</p>


<p class=MsoNormal>In the upper left corner of the <i>Calculated Columns</i>
tab are the most common functions that are used in the definition of a
calculated column.</p>

<p class=MsoNormal><img border=0 width=227 height=48
src="Report_Design_Guide_files/image075.jpg"></p>


<p class=MsoNormal>The functions are categorized according to the generic data
types of columns; date, text, and numeric data. The dropdown lists may be
viewed by hovering the mouse over the buttons. Click on the function to insert
the reference into the <i>Definition</i> textbox. For the functions requiring
additional arguments or information, a dialog will be presented to complete the
function. For example, the <i>Text/Concatenate</i> function will display the
following dialog:</p>


<p class=MsoNormal><img border=0 width=487 height=192
src="Report_Design_Guide_files/image076.jpg"></p>


<p class=MsoNormal>To complete this particular dialog, click on the <i>String1</i>
text box and then click on a column. The column reference will be placed into
the text box. Only the columns relevant to the function type will be presented
for selection. Repeat the process for <i>String2</i> and click on <b>OK</b> to
post the function into the <i>Definition</i> text box. The other function
dialogs behave similarly.</p>



<p class=MsoNormal>On the left side of the <i>Calculated Columns</i> tab of the
<i>Select or Modify Data Source</i> dialog is the data objects/columns tree. To
place the column reference into the <i>Definition</i>, click on the column.</p>



<p class=MsoNormal>After a calculated column has been defined, the data
objects/columns tree will be refreshed and the calculated column added to the
tree along with two management actions. Click on the <img border=0 width=11
height=13 src="Report_Design_Guide_files/image077.gif"> icon to edit the
calculated column definition. Click on the <img border=0 width=9 height=9
src="Report_Design_Guide_files/image078.gif"> to remove the calculated column.</p>


<p class=MsoNormal>At the top of the right side of the <i>Calculated Columns</i>
tab are operators that may be added to the <i>Definition</i>.</p>


<p class=MsoNormal><img border=0 width=259 height=34
src="Report_Design_Guide_files/image079.jpg"></p>


<p class=MsoNormal>Clicking on any of the operators will place the symbol in
the <i>Definition</i> at the last cursor position. The <i>More</i> dropdown
list contains the <i>AND,</i> <i>OR</i>, and <i>NOT</i> operators that may be
added to the <i>Definition</i> by clicking on them.</p>


<p class=MsoNormal>The <i>Definition</i> text box is the actual work area. It
may be populated from the functions, columns and operators available or the
user can enter the calculated column definition directly by typing in the
textbox.</p>


<p class=MsoNormal>The <b>Test</b> button will verify that the calculated
column definition meets the syntax rules for the reporting DBMS. If so, a
mini-report displaying the calculated column and the underlying data from the
reporting database will be shown.</p>


<p class=MsoNormal>Ad Hoc must know the data type of the resultant calculated
column. In most cases the data type can be accurately determined from the data
type of the underlying columns or calculation. The <i>Auto</i> option allows Ad
Hoc to use the implied data type. To provide the specific data type or override
the implied data type, either select the data type from the <i>Type</i> dropdown
list or click on the <b>Determine Type</b> button.</p>


<p class=MsoNormal>Enter the <i>Name</i> of the calculated column in the
provided textbox.</p>


<p class=MsoNormal>The <b>Preview Selected Data</b> button will display a
report of all of the columns, including the defined calculated column in the <i>Selected
Data Preview</i> dialog. From this report dialog, clicking on the <b>Select and
Continue</b> button will save the current calculated column definition and
dismiss all of the dialogs.</p>


<p class=MsoNormal>The <b>Save</b> button verifies the <i>Definition</i>,
stores the <i>Name</i> and <i>Definition</i> temporarily, clears the <i>Definition</i>
textbox and updates the data object / column tree.</p>


<p class=MsoNormal>The <b>Clear</b> button erases the contents of the <i>Definition</i>
textbox.</p>


<p class=MsoNormal>The <b>New</b> button clears the contents of the <i>Definition</i>
textbox and resets the Name. If the existing <i>Definition</i> has not been
saved, a confirmation dialog will be shown.</p>


<p class=MsoNormal>The <b>Cancel</b> button will discard any changes made in
the <i>Select or Modify Data Source</i> dialog and dismiss the dialog.</p>


<p class=MsoNormal>Click the <b>OK</b> button to save all of the changes made
in the <i>Select or Modify Data Source</i> dialog. The newly defined calculated
columns are then available for selection in the Report Builder.</p>


<table class=MsoNormalTable border=0 cellspacing=0 cellpadding=0
 style='background:#F3F3F3;border-collapse:collapse'>
 <tr>
  <td width=590 valign=top style='width:6.15in;padding:0in 5.4pt 0in 5.4pt'>
  <p class=Arial><span style='font-style:normal'>Note:</span></p>
  </td>
 </tr>
 <tr>
  <td width=590 valign=top style='width:6.15in;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal><span style='color:red'>A calculated column must be used
  in the report for the column to be saved in the report definition. Unused
  calculated columns are automatically removed from the report definition.</span></p>
  </td>
 </tr>
</table>

<span style='font-size:12.0pt;font-family:"Arial","sans-serif"'><br clear=all
style='page-break-before:always'>
</span>


</body>
</html>
