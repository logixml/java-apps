<%@ Page Language="vb" AutoEventWireup="false" Codebehind="ReportingDatabase.aspx.vb" Theme="Green" Inherits="LogiAdHoc.ahDbAdmin_ReportingDatabase" Culture="auto" meta:resourcekey="PageResource1" UICulture="auto" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register TagPrefix="AdHoc" TagName="AdminMainMenu" Src="~/ahControls/AdminMainMenu.ascx" %>
<%@ Register TagPrefix="AdHoc" TagName="NavMenu" Src="~/ahControls/NavMenu.ascx" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Metadata Connection</title>
    <script language="JavaScript" type="text/javascript" src="../ahScripts/ahChange.js"></script>
    <script type="text/javascript">
        window.onbeforeunload = AlertOnExit;
    </script>
</head>
<body id="bod">
    <AdHoc:AdminMainMenu ID="menu" runat="server" />
    
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server" />
    <div id="submenu">
        <AdHoc:NavMenu ID="subnav" runat="server" />
    </div>
        <input type="hidden" id="ahDirty" name="ahDirty" runat="server" />
        
        <script type="text/javascript">
        Sys.WebForms.PageRequestManager.getInstance().add_endRequest(EndRequestHandler);
        function EndRequestHandler(sender, args) {
            noMessage=false;
            $find('ahSavePopupBehavior').hide();        
        }
        </script> 
        
        <div class="divForm">
            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                <ContentTemplate>
                   <%-- <h2> Reporting Database Connection</h2>
                    <br />
                    <br />--%>
                    <p class="info">
                        A connection string is required to establish database connectivity. 
                        You can type in the string, copy and paste an existing string 
                        or use any of the provided wizards to build a connection string.
                    </p>
                    <input type="hidden" id="ihDatabaseID" runat="server" />
                    <table>
                        <tr>
                            <td width="120px">Database Label:</td>
                            <td>
                                <asp:TextBox ID="txtDatabaseLabel" Width="500px" runat="server" />
                                <asp:RequiredFieldValidator ID="rtvDatabaseName" runat="server" ErrorMessage="Database Label is required."
                                    ControlToValidate="txtDatabaseLabel" ValidationGroup="Connection">*</asp:RequiredFieldValidator>
                                <asp:CustomValidator ID="cvValidName" runat="server" ControlToValidate="txtDatabaseLabel"
                                    ErrorMessage="This Database Label already exists in the database. Please select a different database label."
                                    OnServerValidate="IsNameValid" EnableClientScript="False" ValidationGroup="Connection">*</asp:CustomValidator>
                            </td>
                        </tr>
                        <tr>
                            <td width="120px">Connection String:</td>
                            <td>
                                <asp:TextBox ID="txtConnString" Rows="10" TextMode="MultiLine" Width="500px" runat="server" />
                                <asp:RequiredFieldValidator ID="rfvConnString" runat="server" ErrorMessage="Connection String is required."
                                    ControlToValidate="txtConnString" ValidationGroup="Connection" >*</asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td></td>
                            <td>
                                <div id="divWizards" runat="server" >
                                    <asp:LinkButton ID="lbtnMySQL" runat="server" Text="Build a MySQL connection string..."
                                         CausesValidation="false" OnClick="lbtnMySQL_Click" />
                                    <br />
                                    <asp:LinkButton ID="lbtnOracle" runat="server" Text="Build an Oracle connection string..."
                                         CausesValidation="false" OnClick="lbtnOracle_Click" />
                                    <br />
                                    <asp:LinkButton ID="lbtnPostgreSQL" runat="server" Text="Build a PostgreSQL connection string..."
                                         CausesValidation="false" OnClick="lbtnPostgreSQL_Click" />
                                    <br />
                                    <asp:LinkButton ID="lbtnSqlServer" runat="server" Text="Build a SQL Server connection string..."
                                         CausesValidation="false" OnClick="lbtnSqlServer_Click" />
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td width="120px">Command Timeout:</td>
                            <td>
                                <asp:TextBox ID="txtCommandTimeout" Width="100px" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td width="120px">Database:</td>
                            <td>
                                <asp:DropDownList ID="ddlDatabaseType" runat="server" Width="100px" AutoPostBack="true" OnSelectedIndexChanged="ddlDatabaseType_SelectedIndexChanged">
                                    <asp:ListItem Text="SQL Server" Value="1" />
                                    <asp:ListItem Text="MySQL" Value="3" />
                                    <asp:ListItem Text="Oracle" Value="4" />
                                    <asp:ListItem Text="Sybase" Value="5" />
                                    <%--<asp:ListItem Text="SAND" Value="6" />--%>
                                    <asp:ListItem Text="Cache" Value="7" />
                                    <asp:ListItem Text="IBM DB2" Value="8" />
                                    <asp:ListItem Text="Informix" Value="10" />
                                    <asp:ListItem Text="PostgreSQL" Value="12" />
                                    <asp:ListItem Text="PI" Value="9" />
                                    <asp:ListItem Text="Other" Value="11" />
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td width="120px">Connection Type:</td>
                            <td>
                                <asp:UpdatePanel ID="UP1" runat="server" UpdateMode="conditional">
                                    <ContentTemplate>
                                        <asp:DropDownList ID="ddlConnectionType" runat="server" Width="100px">
                                        </asp:DropDownList>
                                    </ContentTemplate>
                                    <Triggers>
                                        <asp:AsyncPostBackTrigger ControlID="ddlDatabaseType" EventName="SelectedIndexChanged" />
                                    </Triggers>
                                </asp:UpdatePanel>
                            </td>
                        </tr>
                    </table>
                    <asp:ValidationSummary ID="vsummary1" runat="server" ValidationGroup="Connection"/>
                    <br />
                    <asp:Button ID="btnTest" runat="server" CssClass="command" OnClick="TestConnection"
                        Text="Test Connection" ToolTip="Test Connection" ValidationGroup="Connection"/>
                    <asp:Button ID="btnGetSchema" runat="server" CssClass="command" OnClick="GetSchema"
                        Text="Get Schema" ToolTip="Get Database Schema" CausesValidation="false"/>
                    <br />
                    <br />
                    <asp:Button ID="btnSave" runat="server" CssClass="command" OnClick="SaveConnection"
                        Text="Save Connection" ToolTip="Save Connection" ValidationGroup="Connection" 
                        OnClientClick="javascript: document.getElementById('ahDirty').value=0;ShowSaveAnimation('Connection');"/>
                    <asp:Button ID="btnCancel" runat="server" CssClass="command" OnClick="BackToDatabaseList"
                        Text="Back to Database List" ToolTip="Back to Database List" CausesValidation="false" />

                    <asp:Button runat="server" ID="Button1" Style="display: none" />
                    <ajaxToolkit:ModalPopupExtender runat="server" ID="ahModalPopup" BehaviorID="ahModalPopupBehavior"
                        TargetControlID="Button1" PopupControlID="ahPopup" BackgroundCssClass="modalBackground"
                        DropShadow="false">
                    </ajaxToolkit:ModalPopupExtender>
                    
                    <asp:Panel runat="server" CssClass="modalPopup" ID="ahPopup" Style="display: none;width: 300;">
                        <div class="modalPopupHandle">
                            <asp:Label ID="lblModalPopupHeader" runat="server"></asp:Label>
                        </div>
                        <div class="modalDiv">
                            <asp:UpdatePanel ID="upPopup" runat="server">
                                <ContentTemplate>     
                                    <input type="hidden" id="ihPopupType" value="0" runat="server" />
                                    
                                    <div id="divResults" runat="server">
                                        <h3>
                                            <b>Test Result: </b>
                                            <asp:Label ID="lblResult" runat="server"></asp:Label>
                                        </h3>
                                        <div id="divErrorMessage" runat="server" style="color: Red;">
                                            <b>Error Message: </b>
                                            <br /> 
                                            <asp:Label ID="lblErrorMessage" runat="server"></asp:Label>
                                        </div>
                                    </div>
                                    <div id="divConnectionParams" runat="server">
                                        <table>
                                            <tr>
                                                <td>Server:</td>
                                                <td>
                                                    <asp:TextBox ID="txtServer" runat="server" Width="180"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td><asp:Label ID="lblDatabase" runat="server" Text="Database:" /></td>
                                                <td>
                                                    <asp:TextBox ID="txtDatabase" runat="server" Width="180"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr id="trPort" runat="server"> 
                                                <td>Port:</td>
                                                <td>
                                                    <asp:TextBox ID="txtPort" runat="server" Width="180"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr> 
                                                <td>Username:</td>
                                                <td>
                                                    <asp:TextBox ID="txtUsername" runat="server" Width="180"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr> 
                                                <td>Password:</td>
                                                <td>
                                                    <asp:TextBox ID="txtPassword" runat="server" Width="180"></asp:TextBox>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                    <p>
                                    </p>
                                    <div id="divButtons" runat="server">
                                        <asp:Button ID="btnClosePopupOK" CssClass="command" OnClick="ClosePopupOK_OnClick" runat="server"
                                            Text="<%$ Resources:LogiAdHoc, OKWithSpaces %>" UseSubmitBehavior="false" ValidationGroup="popup" />
                                        <asp:Button ID="btnClosePopupCancel" CssClass="command" OnClick="ClosePopupCancel_OnClick"
                                            runat="server" Text="<%$ Resources:LogiAdHoc, Cancel %>" CausesValidation="False"/>
                                    </div>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </div>
                    </asp:Panel>
                    
                    <div id="divSaveAnimation">
                        <asp:Button runat="server" ID="btnSaveAnimation" Style="display: none" />
                        <ajaxToolkit:ModalPopupExtender runat="server" ID="ahSavePopup" BehaviorID="ahSavePopupBehavior"
                            TargetControlID="btnSaveAnimation" PopupControlID="pnlSaving" BackgroundCssClass="modalBackground"
                            DropShadow="False">
                        </ajaxToolkit:ModalPopupExtender>
                        <asp:Panel id="pnlSaving" CssClass="savePopup" runat="server" style="display:none;">
                            <table><tr><td>
                            <asp:Image ID="imgSave" runat="server" SkinID="imgSaving" EnableViewState="false" AlternateText="<%$ Resources:LogiAdHoc, SaveImageAltText %>" />
                            </td>
                            <td valign="middle">
                            <asp:Label ID="lblSaveText" CssClass="lblSavePopup" runat="server" EnableViewState="false" Text="<%$ Resources:LogiAdHoc, PleaseWait %>"/>
                            </td></tr></table>
                        </asp:Panel>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
        
    </form>
</body>
</html>
