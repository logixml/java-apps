<%@ Page Language="vb" AutoEventWireup="false" Codebehind="ConnectionTest.aspx.vb" Inherits="LogiAdHoc.ConnectionTest" Culture="auto" meta:resourcekey="PageResource1" UICulture="auto" %>

<%@ Register TagPrefix="AdHoc" TagName="AdminMainMenu" Src="~/ahControls/AdminMainMenu.ascx" %>
<%@ Register TagPrefix="AdHoc" TagName="NavMenu" Src="~/ahControls/NavMenu.ascx" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Connection Test</title>
    <link href="../App_Themes/Green/AdHoc.css" rel="stylesheet" type="text/css" />
</head>
<body id="bod">
    <AdHoc:AdminMainMenu ID="menu" runat="server" />
    
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server" />
    <div id="submenu">
        <AdHoc:NavMenu ID="subnav" runat="server" />
    </div>
        <div class="divForm">
            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                <ContentTemplate>
                
                   <%-- <h2> Logi Ad Hoc Reporting Connection Test</h2>
                    <br />
                    <br />--%>
                    <asp:RadioButton ID="rbtnTestConnID" runat="server" AutoPostBack="true" GroupName="Selection" 
                        Checked="true" Text="Connection ID" OnCheckedChanged="ChangeSelection" 
                        ToolTip="Select this option to test a pre-defined connection from settings file." />
                    <asp:RadioButton ID="rbtnTestConnStr" runat="server" AutoPostBack="true"
                        GroupName="Selection" OnCheckedChanged="ChangeSelection" Text="Connection String"
                        ToolTip="Select this option to test the specified connection string." />
                    <br />
                    <br />
                    <table>
                        <tr>
                            <td>
                                Connection ID:
                            </td>
                            <td id="tdConnectionID" runat="server">
                                <asp:DropDownList ID="ddlConnectionID" runat="server" >
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td width="120px">
                                Connection String:
                            </td>
                            <td id="tdConnectionString" runat="server">
                                <asp:TextBox ID="txtConnString" Rows="10" TextMode="MultiLine" Width="500px" runat="server" />
                            </td>
                        </tr>
                    </table>
                    <br />
                    <AdHoc:LogiButton ID="btnTest" runat="server" OnClick="TestConnection"
                        Text="Test Connection" ToolTip="Test Connection" />
                    
                    <asp:Button runat="server" ID="Button1" Style="display: none" />
                    <ajaxToolkit:ModalPopupExtender runat="server" ID="ahModalPopup" BehaviorID="ahModalPopupBehavior"
                        TargetControlID="Button1" PopupControlID="ahPopup" BackgroundCssClass="modalBackground"
                        DropShadow="false">
                    </ajaxToolkit:ModalPopupExtender>
                    
                    <asp:Panel runat="server" CssClass="modalPopup" ID="ahPopup" Style="display: none; width: 306px;">
                        <div class="modalPopupHandle" style="width: 300px;">
                            Test Results
                        </div>
                        <div class="modalDiv">
                            <asp:UpdatePanel ID="upPopup" runat="server">
                                <ContentTemplate>     
                                    <div id="divResults" runat="server">
                                        <h3>
                                            <b>Test Result: </b>
                                            <asp:Label ID="lblResult" runat="server"></asp:Label>
                                        </h3>
                                        <div id="divErrorMessage" runat="server" style="color: Red;">
                                            <b>Error Message: </b>
                                            <br /> 
                                            <asp:Label ID="lblErrorMessage" runat="server"></asp:Label>
                                        </div>
                                    </div>
                                    <p>
                                    </p>
                                    <div id="divButtons" runat="server">
                                        <asp:Button ID="btnClosePopupOK" CssClass="command" OnClick="ClosePopupOK_OnClick" runat="server"
                                            Text="<%$ Resources:LogiAdHoc, OKWithSpaces %>" UseSubmitBehavior="false" />
                                    </div>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </div>
                    </asp:Panel>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </form>
</body>
</html>
