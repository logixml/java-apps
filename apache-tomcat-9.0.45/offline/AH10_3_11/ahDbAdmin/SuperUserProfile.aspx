<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="SuperUserProfile.aspx.vb" Inherits="LogiAdHoc.ahDbAdmin_SuperUserProfile" Culture="auto" meta:resourcekey="PageResource1" UICulture="auto" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>

<%@ Register TagPrefix="AdHoc" TagName="AdminMainMenu" Src="~/ahControls/AdminMainMenu.ascx" %>
<%@ Register TagPrefix="AdHoc" TagName="NavMenu" Src="~/ahControls/NavMenu.ascx" %>
<%@ Register TagPrefix="AdHoc" TagName="PasswordControl" Src="~/ahControls/PasswordControl.ascx" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>User Profile</title>
    <link href="../App_Themes/Green/AdHoc.css" rel="stylesheet" type="text/css" />
    <%--<script language="JavaScript" type="text/javascript" src="../ahScripts/ahChange.js"></script>
    <script type="text/javascript">
        window.onbeforeunload = AlertOnExit;
    </script>--%>
</head>
<body id="bod" >
    <AdHoc:AdminMainMenu ID="menu" runat="server" />
    <form id="form1" runat="server">
            
        <asp:ScriptManager ID="ScriptManager1" EnablePartialRendering="true" runat="server"  />
    <div id="submenu">
        <AdHoc:NavMenu ID="subnav" runat="server" />
    </div>
        
        <%--<script type="text/javascript">
        Sys.WebForms.PageRequestManager.getInstance().add_endRequest(EndRequestHandler);
        function EndRequestHandler(sender, args) {
            $find('ahSavePopupBehavior').hide();
        }
        </script>--%>
        
        <div class="divForm">
        <%--<input type="hidden" id="ahDirty" name="ahDirty" runat="server" />
        <input id="UserID" type="hidden" runat="server">--%>
        
            <table style="height: 60px">
                <tr>
                    <td width="125">
                        <asp:Localize ID="Localize2" runat="server" Text="<%$ Resources:LogiAdHoc, UserName %>"></asp:Localize>:
                    </td>
                    <td>
                        <asp:Label ID="username" runat="server" meta:resourcekey="usernameResource1" /></td>
                </tr>
                <tr id="rowPassword" runat="server">
                    <td>
                        <asp:Localize ID="Localize3" runat="server" Text="<%$ Resources:LogiAdHoc, Password %>"></asp:Localize>:
                    </td>
                    <td>
                        <asp:Button ID="SetPassword" OnClick="ShowPassword" CssClass="command" Text="Set Password"
                            runat="server" ValidationGroup="User" meta:resourcekey="SetPasswordResource1" />
                        <%--<asp:CustomValidator ID="cvPassword" runat="server" EnableClientScript="False" ErrorMessage="You must enter a password for this user."
                            OnServerValidate="IsPasswordEntered" ValidationGroup="User" meta:resourcekey="cvPasswordResource1">*</asp:CustomValidator>--%>
                    </td>
                </tr>
                <%--<tr>
                    <td>
                    <asp:Localize ID="Localize4" runat="server" Text="<%$ Resources:LogiAdHoc, FirstName %>"></asp:Localize>:
                    </td>
                    <td>
                        <input id="firstname" type="text" maxlength="20" size="20" runat="server"></td>
                </tr>
                <tr>
                    <td>
                    <asp:Localize ID="Localize5" runat="server" Text="<%$ Resources:LogiAdHoc, LastName %>"></asp:Localize>:
                    </td>
                    <td>
                        <input id="lastname" type="text" maxlength="20" size="20" runat="server"></td>
                </tr>
                <tr id="tdEmail" runat="server">
                    <td>
                    <asp:Localize ID="Localize6" runat="server" Text="<%$ Resources:LogiAdHoc, EmailAddress %>"></asp:Localize>:
                    </td>
                    <td>
                        <input id="email" type="text" maxlength="50" size="50" runat="server">
                        <asp:RegularExpressionValidator ID="eavEmail" runat="server" ControlToValidate="email" ValidationGroup="User"
                            ErrorMessage="This is not a properly formatted email address." ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" meta:resourcekey="eavEmailResource1">*</asp:RegularExpressionValidator>
                        <asp:CustomValidator ID="cvUserSubscribed" runat="server" ControlToValidate="email" ValidationGroup="User" ValidateEmptyText="true"  
                            OnServerValidate="IsEmailValid" ErrorMessage="Then email address cannot be blank when you are subscribed to one or more scheduled reports." >*</asp:CustomValidator>
                    </td>
                </tr>--%>
            </table>
            
            <div>
                <asp:Button runat="server" ID="Button1" Style="display: none" />
                <ajaxToolkit:ModalPopupExtender runat="server" ID="ahModalPopup" BehaviorID="ahModalPopupBehavior"
                    TargetControlID="Button1" PopupControlID="ahPopup" BackgroundCssClass="modalBackground"
                    DropShadow="False">
                </ajaxToolkit:ModalPopupExtender>
                <asp:Panel runat="server" CssClass="modalPopup" ID="ahPopup" Style="display: none;
                    width: 306;">
                    <div class="modalPopupHandle" style="width: 300px;">
                        <asp:Localize ID="PopupHeader" runat="server" Text="Set Password" meta:resourcekey="PopupHeaderResource1"></asp:Localize>
                    </div>
                    <div class="modalDiv">
                        <asp:UpdatePanel ID="UP2" UpdateMode="Conditional" runat="server">
                            <ContentTemplate>
                                <AdHoc:PasswordControl ID="UserPassword" runat="server" />
                            </ContentTemplate>
                            <Triggers>
                                <asp:AsyncPostBackTrigger ControlID="SetPassword" EventName="Click" />
                            </Triggers>
                        </asp:UpdatePanel>
                    </div>
                </asp:Panel>
            </div>

        <%--<div id="divButtons" class="divButtons">
            <asp:Button ID="Save" CssClass="command" runat="server" OnClick="SaveUser" ToolTip="<%$ Resources:LogiAdHoc, SaveTooltip %>"
                Text="<%$ Resources:LogiAdHoc, Save %>" UseSubmitBehavior="false" ValidationGroup="User" />
            <asp:Button ID="Cancel" CssClass="command" runat="server" OnClick="CancelUser" ToolTip="<%$ Resources:LogiAdHoc, CancelTooltip %>"
                Text="<%$ Resources:LogiAdHoc, BackToReportsList %>" CausesValidation="False" />
            <asp:ValidationSummary ID="vsummary" runat="server" ValidationGroup="User" meta:resourcekey="vsummaryResource1" />
        </div>--%>
       <%-- <div id="divSaveAnimation">
            <asp:Button runat="server" ID="btnSaveAnimation" Style="display: none" />
            <ajaxToolkit:ModalPopupExtender runat="server" ID="ahSavePopup" BehaviorID="ahSavePopupBehavior"
                TargetControlID="btnSaveAnimation" PopupControlID="pnlSaving" BackgroundCssClass="modalBackground"
                DropShadow="False">
            </ajaxToolkit:ModalPopupExtender>
            <asp:Panel id="pnlSaving" CssClass="savePopup" runat="server" style="display:none;">
                <table><tr><td>
                <asp:Image ID="imgSave" runat="server" SkinID="imgSaving" EnableViewState="false"  AlternateText="<%$ Resources:LogiAdHoc, SaveImageAltText %>"/>
                </td>
                <td valign="middle">
                <asp:Label ID="lblSaveText" CssClass="lblSavePopup" runat="server" EnableViewState="false" Text="<%$ Resources:LogiAdHoc, PleaseWait %>"/>
                </td></tr></table>
            </asp:Panel>
        </div>--%>
        <%--<div id="divHelp" class="help">
            <a href="../Help.aspx?src=2" target="_blank">
        <asp:Localize ID="GetHelp" runat="server" meta:resourcekey="GetHelp" Text="Get help with the User Profile screen."></asp:Localize>                
            </a>
        </div>--%>
        </div>
    </form>
</body>
</html>
