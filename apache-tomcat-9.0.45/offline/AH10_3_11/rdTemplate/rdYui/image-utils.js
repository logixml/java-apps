// JSLint options:
/*global LogiXML, YUI, document, window */

YUI.add('image-utils', function(Y) {
	"use strict";
	
	var WRAPPER_CLASS = 'chartfx-wrapper',
		imageNS = Y.namespace('LogiXML.Image'),
		WRAPPER_USAGE_COUNT = 'wrapUsageCount',
		isNumber = Y.Lang.isNumber,
	
	wrapImage = function( imgNode ) {
		var wrapper = imgNode.get('parentNode'),
			usageCount;
		
		// See if parentNode is already wrapper
		if ( wrapper.get('tagName') !== 'DIV' || !wrapper.hasClass( WRAPPER_CLASS ) ) {
			imgNode.wrap('<div></div>');
			wrapper = imgNode.get('parentNode');
			wrapper.addClass( WRAPPER_CLASS );
			// TODO: add better code for keeping original styling of node, refer to resize-base.js for examples
		}
		
		/*
		 * Several classes use the image wrapping code, keep track of how many by storing a count on the the wrapper.
		 * This way when a class goes to cleanup it can check if wrapper is still needed
		 */
		usageCount = wrapper.getData( WRAPPER_USAGE_COUNT );
		if ( isNumber( usageCount ) ) {
			usageCount = usageCount + 1;
		}
		else {
			usageCount = 1;
		}
		wrapper.setData( WRAPPER_USAGE_COUNT, usageCount );
		
		return wrapper;
	},
	
	unwrapImage = function( imgNode ) {
		var wrapper = imgNode.get('parentNode'),
			removeWrapper = false,
			usageCount;
		
		if ( wrapper instanceof Y.Node && wrapper.hasClass( WRAPPER_CLASS ) ) {
			usageCount = wrapper.getData( WRAPPER_USAGE_COUNT );
			if ( isNumber( usageCount ) ) {
				usageCount = usageCount - 1;
				
				if ( usageCount === 0 ) {
					removeWrapper = true;
				}
			}
			else {
				removeWrapper = true;
			}
		}
		
		if ( removeWrapper ) {
			imgNode.unwrap();
			wrapper.destroy();
		}
	},
	
	getImageFromMap = function( mapNode ) {
		if ( mapNode && mapNode instanceof Y.Node ) {
			return Y.one('img[usemap="#' + mapNode.getAttribute('name') + '"]');
		}
		return null;
	},
	
	getChartMapName = function( element ) {
		var node, usemap;
		
		// YUI Node
		if ( element instanceof Y.Node ) {
			node = element._node;
		}
		// DOM Node
		else if ( element.nodeType ) {
			node = element;
		}
		// Can't work with anything else
		else {
			return undefined;
		}
		
		// Not an IMG tag
		if ( node.tagName !== 'IMG' ) {
			return undefined;
		}
		
		usemap = node.getAttribute('usemap');
		if ( usemap ) {
			usemap = usemap.split('#')[1];
			return usemap;
		}
		return undefined;
	},
	
	getMapfromImage = function( element ) {
		var mapName = getChartMapName( element );
		
		if ( mapName ) {
			return Y.one('map[name=' + mapName + ']');
		}
		return null;
	},
	
	isImageLoaded = function( element ) {
		var node, naturalWidth;
		
		// YUI Node
		if ( element instanceof Y.Node ) {
			node = element._node;
		}
		// DOM Node
		else if ( element.nodeType ) {
			node = element;
		}
		// Can't work with anything else
		else {
			return undefined;
		}
		
		// Not an IMG tag
		if ( node.tagName !== 'IMG' ) {
			return undefined;
		}
		
		// Hack for IE 7/8
		if ( Y.UA.ie ) {
			return node.readyState === 'complete';
		}
		// Good Browsers
		naturalWidth = node.naturalWidth;
		return !(typeof naturalWidth === 'undefined' || naturalWidth === 0 );
	};
	
	// Make calls available under Y
	imageNS.wrapImage = wrapImage;
	imageNS.unwrapImage = unwrapImage;
	imageNS.getImageFromMap = getImageFromMap;
	imageNS.getChartMapName = getChartMapName;
	imageNS.getMapfromImage = getMapfromImage;
	imageNS.isImageLoaded = isImageLoaded;

}, '1.0.0', {requires: ['dom', 'node'] });