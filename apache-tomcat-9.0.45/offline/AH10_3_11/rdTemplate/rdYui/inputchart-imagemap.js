// JSLint options:
/*jslint evil: true*/
/*global LogiXML, YUI, document, window */

YUI.add('inputchart-imagemap', function(Y) {
	"use strict";
	
	var ORIENTATION_VERTICAL = 'vertical',
		CHART_BAR = 'bar',
		CHART_LINE = 'line',
		CHART_AREA = 'area',
		BOUNDING_BOX = 'boundingBox',
		
	// Create new Overlay class which adds size constraints.  Add CSS_PREFIX to match original class
	MinDimensionOverlay = Y.Base.create('resizableOverlay', Y.LogiXML.DrawableOverlay, [Y.LogiXML.DrawableOverlaySizeConstrain], null, { CSS_PREFIX: 'drawableoverlay' } );

	/*
	* This class extends the base Input Chart by calculating what areas from an image map are highlighted.
	* The selection point of each type of chart and shape is documented throughout the code.
	*/
	var InputChartImageMap = function() {
		InputChartImageMap.superclass.constructor.apply(this, arguments);
	};
	
	Y.namespace('LogiXML.Form').InputChartImageMap = InputChartImageMap;
	
	Y.extend(InputChartImageMap, Y.LogiXML.Form.InputChart, {
	
		initializer : function(config) {
			this._selectNode = this._findSelectElement();
			this.on('inputChart:reset', this._clearSelectValues, this);
			this.on('inputChart:runAction', this._onRunAction, this);
		},
		
		destructor : function() {
			this._selectNode.empty();
		},
		
		/**
		 * Override
		 */
		_createOverlay : function() {
			var overlay = new MinDimensionOverlay( this._getOverlayConfig() );
			overlay.render();
			return overlay;
		},
		
		/**
		* Override
		*/
		_parseHTMLConfig : function() {
			InputChartImageMap.superclass._parseHTMLConfig.call(this, arguments);
			
			var configNode = this.get('configNode');
			this._inputSelectName = configNode.getAttribute('data-inputSelectName');
		},
		
		_onRunAction : function() {
			var selectionPoints = this._calculateSelectionPoints(),
				region = Y.DOM.region( this._overlay.get(BOUNDING_BOX).getDOMNode() ),
				highlightedNodes = [],
				selectNode = this._selectNode,
				optionNode = null,
				submitAction = this._submitAction;
			
			Y.each( selectionPoints, function(point) {
				if ( point.x >= region.left &&
					 point.x <= region.right &&
					 point.y >= region.top &&
					 point.y <= region.bottom ) {
					 
					 highlightedNodes.push( point.node );
				}
			});
			// Remove old selections
			selectNode.empty();
			
			if ( highlightedNodes.length < 1 ) {
				return;
			}
			
			// Build list of selected areas
			Y.each( highlightedNodes, function(node) {
				optionNode = Y.Node.create( '<option>' );
				
				optionNode.setAttribute('value', node.getAttribute('data-valueColumn'));
				optionNode.setAttribute('selected', 'selected');
				
				selectNode.prepend( optionNode );
			});
			
			// Submit Form if need be
			if ( submitAction ) {
				eval( submitAction );
			}
		},
		
		_findSelectElement : function() {
			var selectNode = Y.one('#' + this._inputSelectName);
			
			selectNode.setAttribute('multiple', 'multiple');
			selectNode.setStyle('display', 'none');
			
			return selectNode;
		},
		
		_clearSelectValues : function() {
			this._selectNode.empty();
		},
		
		/*
		 * Bar - choose the top of the bar in the middle
		 * Line - Middle of the area, 4 points half, 6 points middle area with angle
		 * Area - 
		 * Scatter - N/A
		 * Timeline - N/A
		 */
		_calculateSelectionPoints : function() {
				
			var chartType = this.get('chartType').toLowerCase(),
				chartOrientation = this.get('chartOrientation'),
				chart3D = this.get('chart3D'),
				pageOffset = this.get('chart').getXY(),
				areaNodes = this._mapNode.all('area'),
				selectionPoint,
				selectionPoints = [],
				calculateSelectionPoint;
			
			calculateSelectionPoint = Y.bind( function( chartOrientation, areaNode ) {
				// TODO: Disabled caching until code is changed to work around page position changes
				//selectionPoint = areaNode.getData('selectionPoint');
				
				//if ( selectionPoint === undefined || selectionPoint === null ) {
				
					// Area map coordinates have their 0,0 position in the top left corner of the image
					if ( chartType === CHART_LINE ) {
						selectionPoint = InputChartImageMap.calculateLineSelectionPoint( areaNode, chartOrientation );
					}
					else if ( chartType === CHART_BAR ) {
						// 3D Bar
						if ( chart3D ) {
							selectionPoint = InputChartImageMap.calculate3dBarSelectionPoint( areaNode, chartOrientation );
						}
						// 2D Bar
						else {
							selectionPoint = InputChartImageMap.calculate2dBarSelectionPoint( areaNode, chartOrientation );
						}
					}
					else if ( chartType === CHART_AREA ) {
						selectionPoint = InputChartImageMap.calculateAreaSelectionPoint( areaNode, chartOrientation );
					}
					
					// Adjust selection to take into account position of chart on page
					selectionPoint.x = pageOffset[0] + selectionPoint.x;
					selectionPoint.y = pageOffset[1] + selectionPoint.y;
					
					// Cache data in node for future lookup
					//areaNode.setData('selectionPoint', selectionPoint);
				//}
				
				selectionPoints.push( selectionPoint );
			}, this, chartOrientation );
			
			areaNodes.each( calculateSelectionPoint );
			
			return selectionPoints;
		}
		
	}, {
		NAME: 'inputChartImageMap',
		CSS_PREFIX: 'inputChartImageMap',
		SUPPORTED_CHART_TYPES: [CHART_BAR, CHART_AREA],
		
		parseAreaCoordinates : function( areaNode ) {
			
			var node, coordinateString, areaType, coordinates,
				numerofCoordinates, rectAreaCoordinates,
				coordinatePairs = [], i;

			// YUI Node
			if ( areaNode instanceof Y.Node ) {
				node = areaNode.getDOMNode();
			}
			// DOM Node
			else if ( LogiXML.isDomNode( areaNode ) ) {
				node = Y.one( areaNode );
			}
			// Can't work with anything else
			else {
				return undefined;
			}
			
			coordinateString = node.getAttribute('coords');
			areaType = node.getAttribute('shape');
			
			// Make sure we have everything needed
			if ( LogiXML.String.isBlank( coordinateString ) || LogiXML.String.isBlank( areaType ) ) {
				return undefined;
			}
			
			coordinates = coordinateString.split(',');
			numerofCoordinates = coordinates.length;
			
			/* 
			 * http://www.w3.org/TR/xhtml2/mod-csImgMap.html
			 * 
			 * rect: left-x, top-y, right-x, bottom-y
			 * poly: x1, y1, x2, y2, ..., xN, yN. If the first and last x and y coordinate pairs
			 * are not the same, infer an additional coordinate pair to close the polygon.
			 */
			if ( areaType.toLowerCase() === 'rect' ) {
				rectAreaCoordinates = {
					'leftX' : parseInt( coordinates[0], 10 ),
					'topY' : parseInt( coordinates[1], 10 ),
					'rightX' : parseInt( coordinates[2], 10 ),
					'bottomY' : parseInt( coordinates[3], 10 )
				};
				
				return rectAreaCoordinates;
			}
			
			if ( areaType.toLowerCase() === 'poly' ) {
				for ( i = 0 ; i < numerofCoordinates; i = i + 2 ) {
					coordinatePairs[i/2] = { 'x' : parseInt( coordinates[i], 10 ), 'y' : parseInt( coordinates[i+1], 10 ) };
				}
				return coordinatePairs;
			}
			
			return undefined;
		},
		
		determineParallelogramMiddlePoint : function( coordinates ) {
			if ( coordinates.length !== 4 ) {
				return undefined;
			}
			
			/*
			* Parrallelogram is just a slanted rectangle, recreate the rectangle by finding low/high x/y values
			*/
			var selectionPoint = {},
				point = coordinates[0],
				lowX = point.x,
				highX = point.x,
				lowY = point.y,
				highY = point.y,
				length = coordinates.length,
				x, y, i;

			for ( i = 1; i < length; i += 1 ) {
				x = coordinates[i].x;
				y = coordinates[i].y;
				
				if ( x <= lowX ) {
					lowX = x;
				}
				else if ( x > highX ) {
					highX = x;
				}
				
				if ( y <= lowY ) {
					lowY = y;
				}
				else if ( y > highY ) {
					highY = y;
				}
			}
			selectionPoint.x = (highX - lowX) / 2 + lowX;
			selectionPoint.y = (highY - lowY) / 2 + lowY;
			
			return selectionPoint;
		},
		
		// Selection point is middle of the bar, at the highest value
		calculate2dBarSelectionPoint : function( areaNode, orientation ) {
					
			var selectionPoint = { node: areaNode },
				coordinates = InputChartImageMap.parseAreaCoordinates( areaNode );
				
			// Vertical
			if ( orientation === ORIENTATION_VERTICAL ) {
				selectionPoint.x = (coordinates.rightX - coordinates.leftX) / 2 + coordinates.leftX;
				selectionPoint.y = coordinates.topY;
			}
			// Horizontal
			else {
				// Remeber 0,0 is top left corner of image, so bottom of area is the highest value
				selectionPoint.y = (coordinates.bottomY - coordinates.topY) / 2 + coordinates.topY;
				selectionPoint.x = coordinates.rightX;
			}
			
			return selectionPoint;
		},
		
		// Selection is point is middle of the cube, at the highest value, center of the parallelogram
		calculate3dBarSelectionPoint : function( areaNode, orientation ) {
					
			var selectionPoint = { node: areaNode },
				coordinates = InputChartImageMap.parseAreaCoordinates( areaNode ),
				width, height, widthOffset;
				
			// Vertical
			if ( orientation === ORIENTATION_VERTICAL ) {
				// ChartDirector starts in bottom left corner, but goes counter-clockwise.
				// Points we care about, 4, 5, 6
				
				// Top of the bar(cube) is a parallelogram.  We want the middle of it,
				// which is half the height of the parallelogram and half the width plus the
				// slant offset/2 (horizontal distance between 5 and 6).
				width = coordinates[3].x - coordinates[4].x;
				height = coordinates[5].y - coordinates[4].y;
				widthOffset = (coordinates[4].x - coordinates[5].x) / 2;
				
				selectionPoint.x = coordinates[5].x + width / 2 + widthOffset;
				selectionPoint.y = coordinates[5].y - height / 2;
			}
			// Horizontal
			else {
			
				// ChartDirector starts in top left corner, but goes counter-clockwise.
				// Points we care about, 4, 5, 6
				
				// Top of the bar(cube) is a parallelogram.  We want the middle of it,
				// which is half the height of the parallelogram and half the width plus the
				// slant offset/2 (horizontal distance between 4 and 5).
				width = coordinates[4].x - coordinates[3].x;
				height = coordinates[4].y - coordinates[5].y;
				widthOffset = (coordinates[4].x - coordinates[3].x) / 2;
				
				selectionPoint.x = coordinates[3].x + width / 2 + widthOffset;
				selectionPoint.y = coordinates[3].y - height / 2;
			}
			
			return selectionPoint;
		},
		
		/* 
		* Areas are really rectangles with 1 or 2 triangles on top.  Selection is top-middle of area
		* All areas are made up of 4 or 6 points
		*
		* |\/|		|\
		* |  |  or  | |
		* ----		---
		*/
		calculateAreaSelectionPoint : function( areaNode, orientation ) {
					
			var selectionPoint = { node: areaNode },
				coordinates = InputChartImageMap.parseAreaCoordinates( areaNode );
			
			// ChartDirectory outputs 4 or 6 points for area charts
			
			// Vertical
			if ( orientation === ORIENTATION_VERTICAL ) {
				// Points start in top left corner and go counter-clockwise.
			
				// Midpoint is 6
				if ( coordinates.length === 6 ) {
					selectionPoint.x = coordinates[5].x;
					selectionPoint.y = coordinates[5].y;
				}
				// The end areas have 4 points, essentially rectangle with right-angle triangle on top
				else if ( coordinates.length === 4 ) {
					// Subtract the dimensions, even if difference is negative, adding to other dimension will correct it
					// Height
					selectionPoint.y = coordinates[3].y + ( coordinates[0].y - coordinates[3].y ) / 2;
					// Width
					selectionPoint.x = coordinates[3].x + ( coordinates[0].x - coordinates[3].x ) / 2;
				}
			}
			// Horizontal
			else {
				// Here points start bottom left corner and go counter-clockwise
				
				// Midpoint is 3
				if ( coordinates.length === 6 ) {
					selectionPoint.x = coordinates[2].x;
					selectionPoint.y = coordinates[2].y;
				}
				
				// Bottom left corner, counter-clockwise
				// The end areas have 4 points, essentially rectangle with right-angle triangle on top
				else if ( coordinates.length === 4 ) {
					// Subtract the dimensions, even if difference is negative, adding to other dimension will correct it
					// Height
					selectionPoint.y = coordinates[3].y + (coordinates[0].y - coordinates[3].y) / 2;
					// Width
					selectionPoint.x = coordinates[1].x + (coordinates[2].x - coordinates[1].x) / 2;
				}
			}
			
			return selectionPoint;
		},
		
		/*
		* Line charts areas can have varying sizes, but they always consist of 4 ot 6 points.  With 4 points 
		* just calculate middle of Parallelogram and you're good.  With 6 points, take the two middle points
		* and calculate the midpoint between those two.
		*/
		calculateLineSelectionPoint : function( areaNode, orientation ) {
					
			var coordinates = InputChartImageMap.parseAreaCoordinates( areaNode ),
				selectionPoint = {};
			
			// Vertical
			if ( orientation === ORIENTATION_VERTICAL ) {
				// 6 points, ChartDirector starts left-top most point and works clockwise
				// Our mid points are 2 and 5 then.
				if ( coordinates.length === 6 ) {
					selectionPoint.y = (coordinates[4].y - coordinates[1].y) / 2 + coordinates[1].y;
					selectionPoint.x = coordinates[1].x;
				}
				
				// 4 points, starting position varies in line chart.  Points create a parallelogram
				else if ( coordinates.length === 4 ) {
					selectionPoint = InputChartImageMap.determineParallelogramMiddlePoint( coordinates );
				}
			}
			// Horizontal
			else {
				// 6 points, ChartDirector starts in the bottom left corner and works clockwise
				// Our mid points are 2 and 5 then.
				if ( coordinates.length === 6 ) {
					selectionPoint.x = (coordinates[4].x - coordinates[1].x) / 2 + coordinates[1].x;
					selectionPoint.y = coordinates[1].y;
				}
				
				// 4 points, starting position varies in line chart.  Points create a parallelogram
				else if ( coordinates.length === 4 ) {
					selectionPoint = InputChartImageMap.determineParallelogramMiddlePoint( coordinates );
				}
			}
			selectionPoint.node = areaNode;
			
			return selectionPoint;
		}
	});
	
}, '1.0.0', {requires: ['inputchart-base', 'drawable-overlay-size-constrain']});