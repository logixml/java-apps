// JSLint options:
/*global LogiXML, YUI, document, window */

YUI.add('drawable-overlay-size-constrain', function(Y) {
	"use strict";
	
	var Lang = Y.Lang,
		BOUNDING_BOX = 'boundingBox';
	
	/*
	 * Enables size constrains for the overlay
	 * Use minWidth/minHeight prevent an overlay from being drawn unless it's greater than those values
	 *
	 * Can also set minWidth/minHeight to container width/height values to essentially fill an entire region.
	 * You must add Y.WidgetPositionConstrain for this to work correctly though.
	 */
	function DrawableOverlaySizeConstrain() {
	
		this._handles_SizeConstrain = {
			bindUI: Y.after(this._bindUISizeConstrain, this, 'bindUI'),
			destructor: Y.before(this._destructorSizeConstrain, this, 'destructor'),
			afterMouseDown: Y.after(this._afterMouseDownSizeConstrain, this, '_onMouseDown')
		};
		
		this._overlayNodeSizeConstrain = this.get( BOUNDING_BOX );
		this._heightMaxed_SizeConstrain = false;
		this._widthMaxed_SizeConstrain = false;
	}
	
	DrawableOverlaySizeConstrain.ATTRS = {
		// Minimum height in px
		minHeight: {
			value: 5,
			setter: function(value) {
				// Prevent someone from setting minHeight greater than constraining node height
				var constrainNodeHeight = this.get('constrain').get('offsetHeight');
				return value > constrainNodeHeight ? constrainNodeHeight : value;
			},
			validator: '_defDimensionValidator'
		},
		
		// Minimum width in px
		minWidth: {
			value: 5,
			setter: function(value) {
				// Prevent someone from setting minWidth greater than constraining node width
				var constrainNodeWidth = this.get('constrain').get('offsetWidth');
				return value > constrainNodeWidth ? constrainNodeWidth : value;
			},
			validator: '_defDimensionValidator'
		},
		
		// Initial height in px
		initialHeight: {
			value: 0,
			setter: function(value) {
				var minHeight = this.get('minHeight');
				return minHeight > value ? minHeight : value;
			},
			validator: '_defDimensionValidator'
		},
		
		// Initial height in px
		initialWidth: {
			value: 0,
			setter: function(value) {
				var minWidth = this.get('minWidth');
				return minWidth > value ? minWidth : value;
			},
			validator: '_defDimensionValidator'
		},
		
		_defDimensionValidator: function(value) {
			return Lang.isNumber(value) && value > -1;
		}
	};
	
	DrawableOverlaySizeConstrain.prototype = {
		
		_bindUISizeConstrain : function() {
			var resize = this._resize,
				constrainNode = this.get('constrain');
				
			if ( resize ) {
				resize.plug( Y.Plugin.ResizeConstrained, {
					constrain: constrainNode,
					minWidth: this.get('minWidth'),
					minHeight: this.get('minHeight')
				});
			}
			
			this.on('draw:end', this._onDrawEnd, this);
			this.on('draw:draw', this._onDraw, this);
		},
		
		_onDraw : function(ev) {
			var minHeight = this.get('minHeight'),
				minWidth = this.get('minWidth'),
				overlayNode = this._overlayNodeSizeConstrain,
				initialXY = this.get('initialXY'),
				adjustedX = initialXY[0],
				adjustedY = initialXY[1],
				initialX, initialY, newX, newY,
				adjustedDimensions = { width: minWidth, height: minHeight };
			
			// If width is set to max don't adjust width, no need since overlay will fill width of constrainingnode
			if ( !this._widthMaxed_SizeConstrain ) {
				initialX = initialXY[0];
				newX = ev.pageX;
				
				// Mouse has moved left of the initial starting point 
				if ( newX < initialX ) {
					adjustedX = newX;
					adjustedDimensions.width = initialX - newX;
				}
				else {
					adjustedDimensions.width = newX - initialX;
				}
			}
			
			// If height is set to max don't adjust height, no need since overlay will fill height of constrainingnode
			if ( !this._heightMaxed_SizeConstrain ) {
				initialY = initialXY[1];
				newY = ev.pageY;
				
				//  Mouse has moved above the initial starting point
				if ( newY < initialY ) {
					adjustedY = newY;
					adjustedDimensions.height = initialY - newY;
				}
				else {
					adjustedDimensions.height = newY - initialY;
				}
			}
			
			overlayNode.set('offsetWidth', adjustedDimensions.width);
			overlayNode.set('offsetHeight', adjustedDimensions.height);
			
			// Widget-position-constrain keeps the XY values constrained
			this.move( adjustedX, adjustedY );
			ev.preventDefault();
		},

		_onDrawEnd : function(ev) {
			var overlayNode = this.get(BOUNDING_BOX),
				minHeight = this.get('minHeight'),
				minWidth = this.get('minWidth');
			
			// Only keep the overlay if it's bigger than min height/width.  Stop click event
			if ( overlayNode.get('offsetHeight') < minHeight || overlayNode.get('offsetWidth') < minWidth )
			{
				ev.stopImmediatePropagation();
				this.fire( 'draw:cancel' );
			}
		},
		
		_afterMouseDownSizeConstrain : function() {
			var constrainNode = this.get('constrain'),
				constrainRegion = Y.DOM.region( constrainNode.getDOMNode() ),
				initialXY = this.get('initialXY'),
				minHeight = this.get('minHeight'),
				minWidth = this.get('minWidth');
			
			// Min Height is same as constraining node height, adjust initialXY so Y is 'top' of constraining node
			if ( constrainNode.get('offsetHeight') === minHeight ) {
				initialXY[1] = constrainRegion.top;
				this._heightMaxed_SizeConstrain = true;
			}
			
			// Min Width is same as constraining node width, adjust initialXY so X is 'left' of constraining node
			if ( constrainNode.get('offsetWidth') === minWidth ) {
				initialXY[0] = constrainRegion.left;
				this._widthMaxed_SizeConstrain = true;
			}
		},
		
		_destructorSizeConstrain : function() {
			this._overlayNodeSizeConstrain = null;
			this._clearHandlesSizeConstrain();
        },
		
		_clearHandlesSizeConstrain : function() {
			var handles = this._handles;
			
			Y.each( handles, function(item) {
				if ( item ) {
					if ( Lang.isArray( item ) ) {
						Y.each( item, function(handle) {
							handle.detach();
						});
						item = [];
					}
					else {
						item.detach();
						item = null;
					}
				}
			});
		}
		
		/**
		* Provided by drawableOverlay
		* Here used for resize constrain
		 
		constrain: {
			value: null,
			setter: Y.one
		}
		*/
	};
	
	Y.namespace('LogiXML').DrawableOverlaySizeConstrain = DrawableOverlaySizeConstrain;
	
}, '1.0.0', {requires: ['event-custom', 'widget-position-constrain']} );