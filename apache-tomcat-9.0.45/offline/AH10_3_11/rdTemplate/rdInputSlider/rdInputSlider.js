﻿var rdSliderElements = new Array(0)

YUI().use('slider', 'rdslider', 'dualslider', function(Y) {

	LogiXML.InputSlider.rdInputSliderLoad = function(sSliderId) {
		var eleSlider = document.getElementById(sSliderId)	
		
		if (!rdIsParentVisible(eleSlider)) {return} //Load it later, when there's an Action.ShowElement.
		if (eleSlider) {
			var eleYuiSlider = rdGetSliderElement(sSliderId) 
			if (eleYuiSlider) {return} //Already initialized.
			
			var sSliderOrientation = eleSlider.getAttribute('SliderOrientation');
			var sSlider2Id = eleSlider.getAttribute('Slider2Id');
			var nSliderLength = parseInt(eleSlider.getAttribute('SliderLength'));
			var nDecimalPoints = parseInt(eleSlider.getAttribute('DecimalPoints'));
			var decimalFactor = Math.pow(10,nDecimalPoints);
			var thumbSeparationValue = parseFloat(eleSlider.getAttribute('ThumbSeparationValue'));
			var nTickCount = parseInt(eleSlider.getAttribute('TickCount'));
			var nMinConstraint = parseInt(eleSlider.getAttribute('MinConstraint'));
			var nMaxConstraint = parseInt(eleSlider.getAttribute('MaxConstraint'));
			var sliderBackgroundUrl = eleSlider.getAttribute('BackgroundImageUrl');
			var nThumbOffsetLeft = parseInt(eleSlider.getAttribute('SliderThumbOffsetLeft'));
			var nThumbOffsetTop = parseInt(eleSlider.getAttribute('SliderThumbOffsetTop'));		
			var nNonZeroStartValue;       
			var eleYuiSlider
					
			if (!sSlider2Id) {	
			
				//One thumb				
				if (sSliderOrientation && sSliderOrientation == 'Vertical') {
					//Create the control				
					eleYuiSlider = new Y.rdSlider({
						id: sSliderId,
						axis: 'y',
						min: nMinConstraint * decimalFactor,
						max: nMaxConstraint * decimalFactor,
						length: nSliderLength
					});
				}
				else {
					//Create the control				
					eleYuiSlider = new Y.rdSlider({
						id: sSliderId,							
						min: nMinConstraint * decimalFactor,
						max: nMaxConstraint * decimalFactor,
						length: nSliderLength
					});		
				}				 
					
				//Initialize tick count				
				if (nTickCount) eleYuiSlider.set('tickCount', nTickCount);	
				
				eleYuiSlider.renderRail = function() {
					return Y.one('#rdBackground_' + sSliderId);
				};				
				eleYuiSlider.renderThumb = function() {
					return this.rail.one('#rdThumb_' + sSliderId);
				};				
				eleYuiSlider.render('#rdInputSliderBg_' + sSliderId);
				
				//Set offset
				if (nThumbOffsetLeft) eleYuiSlider.thumb.setStyle('paddingLeft', nThumbOffsetLeft);
				if (nThumbOffsetTop) eleYuiSlider.thumb.setStyle('paddingTop', nThumbOffsetTop);
				
				if (eleSlider.value.length == 0) {
				    eleSlider.value = nMinConstraint
				}
				eleYuiSlider.setValue(eleSlider.value * decimalFactor);	
			
					
			}
			else {  			
				//Two thumbs
				var eleSlider2 = eleSlider.parentNode.nextSibling.firstChild;
							
				//Create the control			
				if (sSliderOrientation && sSliderOrientation == 'Vertical') {
					//Create the control				
					eleYuiSlider = new Y.DualSlider({
						id: sSliderId,
						axis: 'y',
						min: nMinConstraint * decimalFactor,
						max: nMaxConstraint * decimalFactor,
						length: nSliderLength							
					});
				}
				else {
					//Create the control				
					eleYuiSlider = new Y.DualSlider({
						id: sSliderId,							
						min: nMinConstraint * decimalFactor,
						max: nMaxConstraint * decimalFactor,
						length: nSliderLength
					});							
				}
				//Initialize tick count				
				if (nTickCount) eleYuiSlider.set('tickCount', nTickCount);	
				
				//Initialize thumb separation
				eleYuiSlider.set('thumbSeparation', thumbSeparationValue * decimalFactor);

				eleYuiSlider.renderRail = function() {
					return Y.one('#rdBackground_' + sSliderId);
				};
				eleYuiSlider.renderThumb = function() {
					return this.rail.one('#rdThumb_' + sSliderId);
				};				
				eleYuiSlider.renderThumb2 = function() {
					return this.rail.one('#rdThumb_' + sSlider2Id);
				};				
				eleYuiSlider.render('#rdInputSliderBg_' + sSliderId);
				
				//Set offset
				if (nThumbOffsetLeft) {
					eleYuiSlider.thumb.setStyle('paddingLeft', nThumbOffsetLeft);
					eleYuiSlider.thumb2.setStyle('paddingLeft', nThumbOffsetLeft);
				}
				if (nThumbOffsetTop) {
					eleYuiSlider.thumb.setStyle('paddingTop', nThumbOffsetTop);
					eleYuiSlider.thumb2.setStyle('paddingTop', nThumbOffsetTop);
				}
				
				//Set value for first thumb
				if (eleSlider.value.length == 0) {
				    eleSlider.value = nMinConstraint
				}
				//Ensure adequate separation from the end.
				if (parseInt(eleSlider.value) + thumbSeparationValue > nMaxConstraint ) {
					eleSlider.value = nMaxConstraint - thumbSeparationValue
		        }
				eleYuiSlider.setValue(eleSlider.value * decimalFactor);	
				//Set value for second thumb
				if (eleSlider2.value.length == 0){
				    eleSlider2.value = nMaxConstraint
				}
				//Ensure adequate separation.
				if (parseInt(eleSlider.value) + thumbSeparationValue > eleSlider2.value  ) {
					eleSlider2.value = Math.min(parseInt(eleSlider.value) + thumbSeparationValue, nMaxConstraint)
		        }
				eleYuiSlider.setValue2(eleSlider2.value * decimalFactor);	
			
			}
			
			eleYuiSlider.after(['valueChange', 'value2Change'], function(e) {
				var eleYuiSlider = e.currentTarget;
				var eleSlider = document.getElementById(eleYuiSlider.get('id'));				
				
				if (e.attrName == 'value2')
					eleSlider = document.getElementById(sSlider2Id);				
								
				if (eleSlider) {				
				
					//Handle decimals
					eleSlider.value = e.newVal / decimalFactor;
					
					//Fire the onchange event for special ondrag element.
					var eleSliderChange = document.getElementById(sSliderId + "_ondrag")
					if (eleSliderChange)
						eleSliderChange.onchange();		
				}
			});
			eleYuiSlider.after(['slideEnd', 'railMouseDown'], function(e) {
				var eleYuiSlider = e.currentTarget;
				eleYuiSlider.syncUI();  

				//Fire the onchange event for special slideEnd element.
				var eleSliderChange = document.getElementById(sSliderId + '_onchange');
				if (Y.Lang.isValue(eleSliderChange))
					eleSliderChange.onchange();	
				else { //#16960 also support onChange case
					eleSliderChange = document.getElementById(sSliderId + '_onChange');
					
					if (Y.Lang.isValue(eleSliderChange))
						eleSliderChange.onchange();	
				}
				
			});

			//Set background image if set
			if (sliderBackgroundUrl) {				
				eleYuiSlider.rail.setStyle('backgroundImage', 'url("' + sliderBackgroundUrl + '")');
				var bgImage = document.createElement('img');
				bgImage.src = sliderBackgroundUrl;				
				//Set rail dimension to fit the image
				switch (sSliderOrientation) {
					case 'Horizontal':						
						setTimeout(function() {eleYuiSlider.rail.setStyle('height', bgImage.height + 'px');}, 100);					
						break;
					case 'Vertical':						
						setTimeout(function() {eleYuiSlider.rail.setStyle('width', bgImage.width + 'px');}, 100);
						break;
				}				
			}					
		}
	};
	
	//Initilize all sliders on the page
	LogiXML.InputSlider._rdInitInputSliders = function() {
		var sliderNodes = Y.all('.yui3-slider-rail');
		
		for (var i = 0; i < sliderNodes.size(); i++) {
			//Make sure that the slider is not already initilized.
			if (sliderNodes.item(i).one('.yui3-dd-draggable') === null)		
				LogiXML.InputSlider.rdInputSliderLoad(sliderNodes.item(i).getDOMNode().parentNode.id.replace('rdInputSliderBg_',''));
		}
	}
	
	Y.on('domready', function(e) {
		//Initialize sliders
		LogiXML.InputSlider._rdInitInputSliders();		
		
		//Wire up for re-int after refreshelement
		LogiXML.Ajax.AjaxTarget().on('reinitialize', function(e) { LogiXML.InputSlider._rdInitInputSliders(); });
	});	
});

function rdGetSliderElement(sSliderId) {
    for (var i = 0; i < rdSliderElements.length; i++) {
        if (rdSliderElements[i]) { //Could be nulled by rdInputSliderUnload()
            if (rdSliderElements[i].id) {
                //One thumb
                if (rdSliderElements[i].id=="rdInputSliderBg_" + sSliderId) {
                    return rdSliderElements[i]
                }
            }else{
                //Two thumbs
                if (rdSliderElements[i].activeSlider.id=="rdInputSliderBg_" + sSliderId) {
                    return rdSliderElements[i].activeSlider
                }
            }
        }
    }
}

function rdIsParentVisible(ele) {
	// See if there are any parent elements, above the current element that are invisible.
	var eleParent = ele.parentNode
	while (eleParent.tagName!="BODY") {
		if (eleParent.style.display=="none") {
			return false 
		} 
		eleParent = eleParent.parentNode
	}
	return true
}

function rdShowHiddenInputSliders(eleParent) {
    var sHtml = eleParent.innerHTML
    var nCurrPos = sHtml.indexOf("rdInputSliderBg_")
    while (nCurrPos!=-1) {
        if (sHtml.substring(nCurrPos - 5,nCurrPos + 5).indexOf('id=') != -1) { //13695
            var sSliderId = sHtml.substring(nCurrPos + 16,sHtml.indexOf(' ',nCurrPos))
            if (sSliderId.indexOf('"')!=-1){
                sSliderId = sSliderId.substring(0,sSliderId.indexOf('"')); //For Mozilla
            }
			if (Y.one('#rdInputSliderBg_' + sSliderId).one('.yui3-dd-draggable') === null)
				LogiXML.InputSlider.rdInputSliderLoad(sSliderId);
        }
        nCurrPos = sHtml.indexOf("rdInputSliderBg_",nCurrPos + 1);
    }
}