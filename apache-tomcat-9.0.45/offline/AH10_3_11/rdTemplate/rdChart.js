function rdChartLoad(imgChart, bOptional) {
	//Re-request the chart for the AutoSizer?
	if (imgChart.src.indexOf("rd1x1Trans.gif") != -1) {	
		
		var eleContainer = imgChart;
		var bIsDashboardFreeformLayoutAutoSize = null;
		
		var parentDiv = findAncestorByIdPrefix('rdDashboardPanel-', imgChart);
		if (parentDiv && parentDiv.style.position == 'absolute') 
			bIsDashboardFreeformLayoutAutoSize = true;			
		
		if((bIsDashboardFreeformLayoutAutoSize != null) && (typeof(bOptional) == 'undefined')){
			setTimeout(function(){rdChartLoad(imgChart, "rdFreeformLayoutAutoSizer")}, 100); 
			return;
		}			
		if(bOptional) eleContainer = imgChart.parentNode;
		
		while (eleContainer.clientWidth==0)
			eleContainer = eleContainer.parentNode;
						
		if(bOptional == 'rdFreeformLayoutAutoSizer'){
		
			eleContainer = findAncestorByIdPrefix('Dashboard2PanelContent', imgChart).getElementsByTagName("DIV")[0];
			if (eleContainer.style.cssText == '') {   // Wait till the panels style is set so that the image size can be calculated.
				rdResizePanelContent(eleContainer, parentDiv.id);
				setTimeout(function(){rdChartLoad(imgChart, "rdFreeformLayoutAutoSizer")}, 100); 
				return;
			}
			
			var nPanelChildrenOffsetHeight = 0;
			var aPanelChildren = eleContainer.childNodes;
			for (var i=0; i < aPanelChildren.length; i++) {
				var aPanelChild = aPanelChildren[i];
				if (aPanelChild.id == imgChart.id) continue;					
				nPanelChildrenOffsetHeight += (navigator.appVersion.match('MSIE 7.0') != null ?  (isNaN(aPanelChild.clientHeight) ? 0 : aPanelChild.clientHeight):(isNaN(aPanelChild.offsetHeight) ? 0 : aPanelChild.offsetHeight));										
				//**aPanelChild.offsetHeight returns bad height values with IE7 standards, go with aPanelChild.clientHeight**
			}							
			imgChart.height = eleContainer.clientHeight - (nPanelChildrenOffsetHeight + 15);
		}
					
		var sSrc =  imgChart.src.replace("rdTemplate/rd1x1Trans.gif","rdTemplate/rdChart2.aspx")
		sSrc += "&rdAutoWidth=" + eleContainer.clientWidth;
		if(bOptional == 'rdFreeformLayoutAutoSizer')
			sSrc += '&rdFreeformPanelAutoSizerHeight=' + imgChart.height;    // This height value is used in the rdChart.vb between show/change dashboard postbacks.			
		
		imgChart.src = sSrc;			
		imgChart.width = eleContainer.clientWidth;					
		
		return		
	}

	//Does this chart support drill-down?
	if (imgChart.src.indexOf("rdDrillDownID=")!=-1) { 
		var sDrilldownID = imgChart.src;
		sDrilldownID = sDrilldownID.substring( sDrilldownID.indexOf("rdDrillDownID=") + 14 );
		sDrilldownID = sDrilldownID.substring( 0, sDrilldownID.indexOf("&") );
		LogiXML.rd.downloadImageMap( sDrilldownID );
	}
	
	//Does this chart have a resizer?
	if (imgChart.src.indexOf("rdResizer=")!=-1) {
		Y.on('domready', function(e) {
			rdInitResizer(imgChart);		
		});
	}	
			
	// Take the loader background image away...
	imgChart.style.backgroundImage = "";
}

function findAncestorByIdPrefix(prefix, elem) {
	while (elem.parentNode) {
		elem = elem.parentNode;
		
		if (elem.id && elem.id.indexOf(prefix) == 0)
			return elem;
	}
	
	return null;
}

function rdChartError(imgChart) {
	if (imgChart.parentNode.tagName != "A") {
		parentChart = imgChart.parentNode
		//Create a link pointing to the error page.
		var aLink = document.createElement("A")
		aLink.href = imgChart.src + "&rdChartDebug=True"
		//Make a new IMG inside of the anchor that points to the error GIF.
		var imgError = document.createElement("IMG")
		imgError.src = "rdTemplate/rdChartError.gif"
		aLink.appendChild(imgError)
		parentChart.appendChild(aLink)
		//Remove the chart image.
		parentChart.removeChild(imgChart)
	}
}

LogiXML.rd.downloadImageMap = function( imageMapID ) {
	
	// Use Ajax request to pull down HTML with image map
	var url = "rdTemplate/rdChart2.aspx",
	    cfg = {
		method: 'GET',
		data: "rdDrillDownID=" + imageMapID + "&" + Math.floor(Math.random() * 100000),
		on: {
			success: LogiXML.rd.attachImageMap
		},
		arguments: {
			success: imageMapID
		}
	};
	
	Y.io( url, cfg );
};

LogiXML.rd.attachImageMap = function( transactionID, response, args ) {

	if ( response.responseText ) {
		/* HTML returned is incorrect, should fix at some point, but for now clean it up a bit
		 * ex.<html><map>...</map></html>
		 */
		var localYUI = Y,
			serverResponseText = response.responseText,
			cleanMap = serverResponseText.substring( serverResponseText.indexOf('<map'), serverResponseText.indexOf('</html>') ),
			body = localYUI.one(document.body),
			imageMapID = args.success,
			currentMap = localYUI.one( 'map[name=' + imageMapID +']' ),
			
			// Has image been resized or refreshed?  If so then we need to update existing map
			associatedImage = localYUI.one( 'img[usemap=#' + imageMapID +']' );
		
		/* Charts and their corresponding imagemaps aren't loaded at the same time.  A seperate AJAX request, this is 
		 * the response function, pulls down imagemaps after onload event for chart.  If user updates fast enough, they can
		 * overwrite the old image with new one before we've loaded the imagemap.  Thus associatedImage will be null.
		 * So null check until better event management or chart/imagemap are downloaded at same time.
		 */
		if ( associatedImage == null ) {
		    return;
		}
		
		/* Firefox seems to have a quirk where it internally caches the <map> specified by use usemap attribute.
		 * You can delete the map from DOM and Firefox will still act like the map is there.  To work
		 * around this remove the usemap attribute, *don't blank it*, and set to new map.
		 */
		if ( currentMap ) {
			associatedImage.removeAttribute( 'usemap' );
			currentMap.remove();
		}
		currentMap = localYUI.Node.create( cleanMap );
		body.append( currentMap );
		
		associatedImage.setAttribute( 'usemap', '#' + imageMapID );
	}
};