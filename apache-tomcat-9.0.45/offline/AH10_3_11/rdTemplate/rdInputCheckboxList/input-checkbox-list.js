YUI.add('rd-inputCheckList-plugin', function (Y) {
	'use strict';
    //create namespaced plugin class
    Y.namespace('LogiXML').rdInputCheckList = Y.Base.create("rdInputCheckList", Y.Plugin.Base, [], {
		_noneSelectedText: 'Select options',
		_selectedText: '# selected',
		_multiple: true,
		_isDropdown: false,
		_isOpen: false,
		_valueDelimiter: ",",
		_saveInLocalStorage: false,
		_columns: 1,
		_checkAllIsVisible: false,
		_actions: [],
        //constructor
        initializer: function () {
			this._container = this.get("host");
			this._id = this._container.getAttribute("id");
			if (!Y.Lang.isValue(this._container) || !Y.Lang.isValue(this._id)) {
				return;
			}
			var sIsDropdown = this._container.getAttribute("data-dropdown");
			if (Y.Lang.isString(sIsDropdown) && sIsDropdown.toLowerCase() === "true") {
				this._isDropdown = true;
			}
			var multiple = this._container.getAttribute("data-multiple");
			this._multiple = LogiXML.String.isBlank(multiple) ? true : multiple == "False" ? false : true;
			// is there check-all?
			this._checkAllBtn = this._container.one("#" + this._id + "_check_all");
			this._inputs = this._container.all('input[type="checkbox"][id^="' + this._id + '_List"]');
			//IE7
			if (this._inputs.size() > 0 && this._inputs.item(0).get("id").indexOf("check_all") != -1) {
				this._inputs = this._inputs.slice(1, this._inputs.size());
			}	
			if (this._checkAllBtn) {
				if (this._multiple == false || this._inputs.size() <= 1) {
					this._checkAllBtn.get('parentNode').hide();
				} else {
					this._checkAllIsVisible = true;
				}
			}
			this._noneSelectedText = this._container.getAttribute("data-noneselected-caption");
			this._selectedText = this._container.getAttribute("data-selected-caption");
			if (this._isDropdown === true) {
				this._dropDownHandler = Y.one("#" + this._id + "_handler");
				this._caption = this._dropDownHandler.one("span.rd-checkboxlist-caption");
				this._img = this._dropDownHandler.one("span.rd-checkboxlist-icon");
				this._spacer = Y.Node.create('<img style="height:1px;" src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7" />');
				this._container.append(this._spacer);
				//attach events
				this._docClickHandle = Y.one("document").on("mousedown", function (e) {this.onDocMouseDown(e); }, this);
				this._dropDownHandle = this._dropDownHandler.on("click", function (e) {this.togglePopup(e); }, this);
			}
			if (this._checkAllIsVisible === true) {
			    this._handleCheckAll = this._checkAllBtn.on("click", function (e) {this.onCheckAllClicked(e); }, this);
			}
			this._inputsHandle = this._inputs.on("click", function (e) {this.onCheckboxClicked(e); }, this);
			//Subscribe for Ajax refreshing?
			if (Y.Lang.isValue(LogiXML) && Y.Lang.isValue(LogiXML.Ajax)) {
				var id = this._id;
				LogiXML.Ajax.AjaxTarget().on('reinitialize', function (e) {var chkList = Y.one("#" + id); if (Y.Lang.isValue(chkList)) {chkList.plug(Y.LogiXML.rdInputCheckList);}});
			}
			
			var actAttributes = ["data-action-onclick","data-action-onchange"], i, length = actAttributes.length, action;
			for (i = 0; i < length; i++) {
				action = this._container.getAttribute(actAttributes[i]);
				if (LogiXML.String.isNotBlank(action)) {
					if (action.indexOf("javascript:") == 0) {
						action = action.substring("javascript:".length);
					}
				}
				this._actions.push(action.replace(/'/g,"\""));
			}
			this.onCheckboxClicked(null);
        },

        //clean up on destruction
        destructor: function () {
			if (this._handleCheckAll) {
				this._handleCheckAll.detach();
				this._handleCheckAll = null;
			}
			if (this._dropDownHandle) {
				this._dropDownHandle.detach();
				this._dropDownHandle = null;
			}
			if (this._inputsHandle) {
				this._inputsHandle.detach();
				this._inputsHandle = null;
			}
			if (this._docClickHandle) {
				this._docClickHandle.detach();
				this._docClickHandle = null;
			}
        },
		onCheckAllClicked: function (e) {
			var isChecked = e.currentTarget.get("checked");
			this._inputs.each(function (node) {
				node.set("checked", isChecked);
			});
			this.onCheckboxClicked({checkall:true});
		},
		open: function () {
			if (this._isOpen === true) {
				return;
			}
			var popupWidth = this._dropDownHandler.get('offsetWidth');
			var popupPosition = this._dropDownHandler.getXY();
			popupPosition[1]  += this._dropDownHandler.get('offsetHeight');
			this._container.setStyles({
				position: "absolute",
				display: "block"
			});
			this._container.setXY(popupPosition);
			this.setPopupWidth();
			this._isOpen = true;
		},
		close: function (e) {
			if (this._isOpen) {
			    this._container.hide();
				this._isOpen = false;
			}
		},
		togglePopup: function () {
			if (!this._isOpen) {
				this.open();
			} else {
				this.close();
			}
		},
		setPopupWidth : function () {
			var ddWidth = this._dropDownHandler.get('offsetWidth');
			this._spacer.setStyle('width', ddWidth + 'px');
			var popupWidth = this._container.get('offsetWidth');
			var diff = popupWidth - ddWidth;
			if (diff > 0) {
				this._spacer.setStyle('width', (ddWidth - diff)  + 'px');
			}
		},
		onDocMouseDown: function (e) {
			if (this._isOpen && e.target !== this._dropDownHandler && !this._dropDownHandler.contains(e.target) && !this._container.contains(e.target)) {
				this.close();
			}
		},
		onCheckboxClicked: function (e) {
			var checked = [];
			var id = this._id;
			this._inputs.each(function (node) {
				if (node.get("checked") === true && node.get("name") === id) {
				    checked.push(node);
				}
			});
			var i, caption = "", length = checked.length;
			if (this._multiple == false && e) {
				for (i = 0; i < length; i++) {
					if (checked[i] != e.currentTarget) {
						checked[i].set('checked', false);
					}
				}
				checked = [];
				length = 0;
				if (e.currentTarget.get('checked') == true) {
					checked.push(e.currentTarget);
					length = 1;
				}
			}
			if (this._checkAllIsVisible) {
				if (checked.length == this._inputs.size()) {
					this._checkAllBtn.set('checked', true);
				} else {
					this._checkAllBtn.set('checked', false);
				}
			}
			if (this._isDropdown == true) {
				for (i = 0; i < length; i++) {
					if (i > 0) {
						caption += ", ";
					}
					caption += checked[i].get('parentNode').one('span').get('text');
				}
				this.setCaption(caption, checked);
			}
			if (e && this._actions.length > 0) {
				for (i = 0; i < this._actions.length; i++) {
					eval(this._actions[i]);
				}
			}
		},
		setCaption: function (caption, checked) {
			if (caption == "") {
				this._caption.set('text', this._noneSelectedText);
				return;
			}
			this._dropDownHandler.setStyle('position', 'absolute');
			//measure caption length
			this._caption.hide();
			var mSpan = Y.Node.create('<span style="visibility: hidden;">0</span>');
			this._dropDownHandler.append(mSpan);
			var oldHeight = mSpan.get('offsetHeight');
			mSpan.set('text', caption);
			var textWidth = mSpan.get('offsetWidth');
			var newHeight = mSpan.get('offsetHeight');
			this._dropDownHandler.removeChild(mSpan);
			mSpan.destroy();
			//mSpan.get('parentNode').removeChild(mSpan);
			this._caption.show();
			//calculate space for caption
			var allowedWidth = this._dropDownHandler.get('offsetWidth') - this._img.get('offsetWidth');
			var padding = this._caption.getX() - this._dropDownHandler.getX();
			allowedWidth -= padding * 2;
			if (allowedWidth > textWidth && oldHeight == newHeight) {
				this._caption.set('text', caption);
			} else {
				this._caption.set('text', this._selectedText.replace("#", checked.length).replace("#", this._inputs._nodes.length));
			}
			this._dropDownHandler.setStyle('position', 'relative');
		}
    }, {
		NAME: "rdInputCheckList",
        NS: "rdInputCheckList",
        ATTRS: {}
    });
}, "1.0.0", { requires: ["base", "plugin", "node", "json"] });

if (this.LogiXML === undefined) {
    this.LogiXML = {};
    this.LogiXML.rd = {};
} else if (this.LogiXML.rd === undefined) {
    this.LogiXML.rd = {};
}

//Temporary. Should be moved somewhere else.
LogiXML.rd.getInputElementListValue = function (elementId) {
	'use strict';
	var listContainer = Y.one("#" + elementId);
	if (!Y.Lang.isValue(listContainer)) {
		return null;
	}
	var sValue = '';
	var sValueDelimiter = listContainer.getAttribute("rdInputValueDelimiter");
	var listItems = listContainer.all('[id^=' + elementId + '_List]');
	var i, listLength = listItems.size(), listItemType = "";
	for (i = 0; i < listLength; i++) {
		listItemType = listItems.item(i).getAttribute('type');
		switch (listItemType) {
		case "checkbox":
			if (listItems.item(i).get('checked') == true) {
				sValue += listItems.item(i).get('value') + sValueDelimiter;
			}
			break;
		}
	}
	if (sValue.length > 1) {
		sValue = sValue.substring(0, sValue.length - 1);
	}
	return sValue;
};
LogiXML.rd.setInputElementListValue = function (elementId, sValue) {
	'use strict';
	var listContainer = Y.one("#" + elementId);
	if (!Y.Lang.isValue(listContainer)) {
		return;
	}
	var sValueDelimiter = listContainer.getAttribute("rdInputValueDelimiter");
	var aValues = sValue.split(sValueDelimiter);
	var listItems = listContainer.all('[id^=' + elementId + '_List]');
	var i, listLength = listItems.size(), listItemType = "";
	for (i = 0; i < listLength; i++) {
		listItemType = listItems.item(i).getAttribute('type');
		switch (listItemType) {
		case "checkbox":
			if (Y.Array.indexOf(aValues, listItems.item(i).get('value')) != -1) {
				listItems.item(i).set('checked', true);
			}
			break;
		}
	}
};