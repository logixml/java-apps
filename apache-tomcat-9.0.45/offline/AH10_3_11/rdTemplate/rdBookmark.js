function rdAddBookmark(sActionId, sReport, sBookmarkReqIds, sBookmarkSessionIds, sCollection, sName, sCust1, sCust2, sDescription, sDescriptionMessage, nRowNr) {

    try{    // Added this as the validation gets called after this function and the input box never gets validated.
		var sErrorMsg = rdValidateForm()
		if (sErrorMsg) {
			return
		}
	}
	catch(e){}
    var sDesc = sDescription
    if (sDescriptionMessage.length!=0){
        sDesc = rdParentPopupPanel.getElementsByTagName("Input")[0].value;
    } 
    
    //Get the list of request parameters.
    var sReqParams
    sReqParams  = "&rdActionId=" + sActionId
    sReqParams += "&rdReport=" + sReport
    sReqParams += "&rdBookmarkReqIds=" + encodeURIComponent(sBookmarkReqIds)
    sReqParams += "&rdBookmarkSessionIds=" + encodeURIComponent(sBookmarkSessionIds)
    sReqParams += "&rdBookmarkCollection=" + encodeURIComponent(sCollection)
    sReqParams += "&rdBookmarkName=" + encodeURIComponent(sName)
    sReqParams += "&rdBookmarkCustomColumn1=" + encodeURIComponent(sCust1)
    sReqParams += "&rdBookmarkCustomColumn2=" + encodeURIComponent(sCust2)
    sReqParams += "&rdBookmarkDescription=" + encodeURIComponent(sDesc)
    var aIds = sBookmarkReqIds.split(",")
    for (var i=0; i < aIds.length; i++) {
        var sId = aIds[i]
        var ele = document.getElementById("rdBookmarkReqId_" + sId)
        if(ele){    //#12959.
            sReqParams += "&" + sId + "=" + encodeURIComponent(ele.innerHTML) //10574
        }
    }
    bSubmitFormAfterAjax = true
    rdAjaxRequest("rdAjaxCommand=rdAjaxNotify&rdNotifyCommand=AddBookmark" + sReqParams)
    
    if(rdParentPopupPanel){
        ShowElement(this.id,rdParentPopupPanel.id,'Hide')
    }
}

function rdEditBookmark(sActionId, sReport, BookmarkCollection, BookmarkID, sDescription, sDescriptionMessage, nRowNr, eleUpdateId) {
    try{    // Added this as the validation gets called after this function and the input box never gets validated.
		var sErrorMsg = rdValidateForm()
		if (sErrorMsg) {
			return
		}
	}
	catch(e){}
    var sDesc = sDescription
    if (sDescriptionMessage.length!=0){
        sDesc = rdParentPopupPanel.getElementsByTagName("Input")[0].value;
    }
    
    var sReqParams
    sReqParams  = "&rdActionId=" + sActionId
    sReqParams += "&rdReport=" + sReport
    sReqParams += "&rdBookmarkCollection=" + BookmarkCollection
    sReqParams += "&rdBookmarkID=" + BookmarkID
    sReqParams += "&rdBookmarkDescription=" + rdAjaxEncodeValue(sDesc)
    bSubmitFormAfterAjax = true //#12541.
    rdAjaxRequest("rdAjaxCommand=rdAjaxNotify&rdNotifyCommand=EditBookmark" + sReqParams)
   
   if(rdParentPopupPanel){        
        ShowElement(this.id,rdParentPopupPanel.id,'Hide')
    }
    
    if (eleUpdateId) {
        //Special for ReportCenter. Update the text.
        var eleUpdate = document.getElementById(eleUpdateId)
        if (eleUpdate) {
           if (eleUpdate.textContent != undefined) {
                eleUpdate.textContent = sDesc //Mozilla, Webkit
            } else {
                eleUpdate.innerText = sDesc //IE
            }
        }
    }

}

function rdRemoveBookmark(sActionId, sReport, BookmarkCollection, BookmarkID, sConfirm, eleRemoveId, sReportCenterID, sChildActionScript) {

    if (sConfirm) {
        if(sChildActionScript){ // show the confirm only when there is a child actionscript(a child action element to the action.RemoveBookmark) #15350, #15314.
		    if (!confirm(sConfirm)) {
			    return
		    }
		}
	}
    var sReqParams
    sReqParams  = "&rdActionId=" + sActionId
    sReqParams += "&rdReport=" + sReport
    sReqParams += "&rdBookmarkCollection=" + BookmarkCollection
    sReqParams += "&rdBookmarkID=" + BookmarkID
    bSubmitFormAfterAjax = true //#12541, #10184.
    rdAjaxRequest("rdAjaxCommand=rdAjaxNotify&rdNotifyCommand=RemoveBookmark" + sReqParams)
    
    if (eleRemoveId) {
         rdAjaxRequest('rdAjaxCommand=RefreshElement&rdReport=' + sReport + '&rdRefreshElementID=' + sReportCenterID) 
    }
    if (sChildActionScript) {
		if (sChildActionScript.length != 0) {
			if (sChildActionScript.toLowerCase().indexOf("javascript:") == 0) {
		        sChildActionScript = new Function(sChildActionScript.substr(11))    //Label
		    }else{
		        sChildActionScript = new Function(sChildActionScript)   //Button
		    }
		    sChildActionScript()
		}
	}
}




