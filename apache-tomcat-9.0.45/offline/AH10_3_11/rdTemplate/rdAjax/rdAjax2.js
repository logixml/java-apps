var req;

var sCurrentRequestParams = ""
var bSubmitFormAfterAjax = false

function rdAjaxRequest(commandParams, bValidate, sConfirm, bProcess) {
	if (commandParams.search("rdRequestForwarding=Form") != -1) {
		rdAjaxRequestWithFormVars(commandParams, bValidate, sConfirm, bProcess);
		return;
	}

	if (bValidate) {
		if (bValidate.length != 0) {
	        if (bValidate == "true") {
		        var sErrorMsg = rdValidateForm()
		        if (sErrorMsg) {
			        alert(sErrorMsg);
			        return;
		        }
            }
        }
	}
	if (sConfirm) {
		if (sConfirm.length != 0) {
			if (!confirm(sConfirm)) {
				return;
			}
		}
	}
	
	var sUrl = "rdTemplate/rdAjax/rdAjax.aspx";
	if (bProcess) {
		if (bProcess.length != 0) {
	        if (bProcess == "true") {
	            sUrl = "rdProcess.aspx";
            }
        }
	}
   
    if (sCurrentRequestParams.length > 0) {		
        if (commandParams.indexOf('rdRET=True') != -1 && sCurrentRequestParams == commandParams) { //16442
            return; //We're still processing this request, no need to resend.
        }else{
            setTimeout('rdAjaxRequest("' + commandParams + '")', Math.floor(Math.random() * 1000));  //Wait a random amount of time between 0 and 1 second.
            return;    
        }
    }
    sCurrentRequestParams = commandParams;
    rdShowAjaxFeedback(true,commandParams);

	try {		
		/* Configuration object for POST transaction */
		var cfg = {
			method: "POST",
			data: commandParams,
			on: {
				success: handleSuccess,
				failure: handleFailure
			}
		};
		req = Y.io(sUrl, cfg);
	}
	catch (e) {
		commandParams = commandParams.replace('rdAjaxCommand','rdAjaxAbort') 
		window.open(sUrl + "?" + commandParams,'_self')
	}
}

var handleSuccess = function(id, o, args){

	if(o.responseText !== undefined)
        rdUpdatePage(o.responseXML, o.responseText);
		
}
var handleFailure = function(id, o, args){
    if(o.responseText != undefined) {
        document.write(o.responseText); //9390
    } //Otherwise, the user likely left this page.15770
}

function rdAjaxRequestWithFormVars(commandURL, bValidate, sConfirm, bFromOnClick, bProcess) {
	//Build the request URL.
    if (bFromOnClick) 
        commandURL = decodeURIComponent(commandURL)  //onClick and other events need decoding.#6549 and 	
	
	//Form vars:
	var sPrevRadioId
	var frm = document.rdForm
	if (!frm) {
	    return  //The debug page is likely the current document.
	}
	for (var i=0; i < frm.elements.length; i++) { 
		if (frm.elements[i].name.lastIndexOf("-PageNr") != -1) 
			if (frm.elements[i].name.lastIndexOf("-PageNr") == frm.elements[i].name.length-7)
				continue;  //Don't forward the interactive page nr.			
		
		//Don't forward security stuff - it's already in session vars.
		if (frm.elements[i].name == "rdUsername") continue;
		if (frm.elements[i].name == "rdPassword") continue;
		if (frm.elements[i].name == "rdFormLogon") continue;
		
		//Don't forward a variable that's already in the list, perhaps from LinkParams.
		if (commandURL.indexOf("&" + frm.elements[i].name + "=")!=-1) continue;
		
		switch (frm.elements[i].type) {
			case 'hidden':  
			case 'text':  
			case 'email':  
			case 'number':  
			case 'tel':  
			case 'textarea':  
			case 'password':  
			case 'select-one':  
			case 'file':  
				var sValue = rdGetFormFieldValue(frm.elements[i])
				commandURL += '&' + frm.elements[i].name + "=" + rdAjaxEncodeValue(sValue)
				break;
			case 'select-multiple':
				var selectedItems = new Array(); 
				for (var k = 0; k < frm.elements[i].length; k++) { 
					if (frm.elements[i].options[k].selected) {
						selectedItems[selectedItems.length] = frm.elements[i].options[k].value
					}
				}
				if (typeof window.rdInputValueDelimiter == 'undefined'){window.rdInputValueDelimiter=','}
				var sValue = selectedItems.join(rdInputValueDelimiter)
				if (sValue.length > 0) { //#5846
				    commandURL += '&' + frm.elements[i].name + "=" + rdAjaxEncodeValue(sValue) 
				}
				break;
			case 'checkbox':
				if (frm.elements[i].checked) {
					var sValue = rdGetFormFieldValue(frm.elements[i])
					commandURL += '&' + frm.elements[i].name + "=" + rdAjaxEncodeValue(sValue)
				}
				break;
			case 'radio': 
				var sRadioId = 'rdRadioButtonGroup' + frm.elements[i].name
				if (sPrevRadioId != sRadioId) {
					sPrevRadioId = sRadioId
					var sValue = rdGetFormFieldValue(document.getElementById(sRadioId))
					commandURL += '&' + frm.elements[i].name + "=" + rdAjaxEncodeValue(sValue)
				}
				break;
		}
	}
    commandURL = commandURL.replace("rdRequestForwarding=Form","") //Don't loop back here again from rdAjaxRequest()
	rdAjaxRequest(commandURL, bValidate, sConfirm, bProcess)
}

function rdAjaxEncodeValue(sValue){
    sValue = encodeURI(sValue)
    sValue = sValue.replace(/&/g,"%26")  //replace &
    sValue = sValue.replace(/\+/g,"%2B") //replace +
    return sValue
}

function rdUpdatePage(xmlResponse, sResponse) {	
            
	if (sResponse.length != 0) {
	    if (sResponse.indexOf('meta name="rdDebug"')!=-1) { //8465
	        rdReportResponseError(sResponse)
            sCurrentRequestParams = "";
	        return
	    }		
	    if (!xmlResponse.documentElement) {
	        rdReportResponseError(sResponse)
            sCurrentRequestParams = "";
	        return
	    }		
	    if (!xmlResponse.documentElement.getAttribute("rdAjaxCommand")){
	        rdReportResponseError(sResponse)
            sCurrentRequestParams = "";
	        return
	    }
	    
	    window.status =""		
		switch (xmlResponse.documentElement.getAttribute("rdAjaxCommand")) {
		
				case 'RefreshDataTable':					
					//Find the HTML TABLE's DIV.
					var sTableDivID = xmlResponse.documentElement.getAttribute('id')
					var eleTableDiv = document.getElementById(sTableDivID)
					if (eleTableDiv) {
					
						//Write the response html to the page, replacing the original table.
						var eleNew = replaceHTMLElement(eleTableDiv, xmlResponse, eleTableDiv.id);						
													
						//#15577.
						if(eleTableDiv.id.indexOf("dtAnalysisGrid") > -1){
						    var sDataCacheKey = xmlResponse.documentElement.getAttribute("rdDataCache");
						    var aExports = new Array("Excel", "CSV", "PDF");
						    var i=0;
						    for(i=0; i<aExports.length;i++){
						        var eleActExport = document.getElementById("actExport" + aExports[i]);	
						        var sExportPathHref = eleActExport.href;
						        var aHref = sExportPathHref.split("&");
						        var j=0; 
						        var sHref = '';
						        for(j=0;j<aHref.length;j++){
						            if(aHref[j].indexOf("rdDataCache")> -1){
						                sHref += ("rdDataCache=" + sDataCacheKey + "&");
						            }else{						               
						                sHref += (aHref[j] + (j== aHref.length-1 ? "" : "&"));
						            }						            
						        }
						        eleActExport.href = sHref;						        
						    }						    		
						}		
					}
        		break;
					
				case 'RefreshElement':					
					var sElementIDs = xmlResponse.documentElement.getAttribute('rdRefreshElementID').split(",")
					for (var i=0; i <  sElementIDs.length; i++) { 
						var eleOld = document.getElementById(sElementIDs[i])
						
						if (eleOld) {														
							//Write the response html to the page, replacing the original table.							
							var eleNew = replaceHTMLElement(eleOld, xmlResponse, sElementIDs[i]);	
							if(sElementIDs[i].indexOf('rdDashboard2PanelContent_') > -1){   //#15622.
							    if(eleNew != null){
							    	var eleContentDiv = elePanelContent = document.getElementById(sElementIDs[i]);
							        if(elePanelContent != null){
							            while(elePanelContent){
							                if(elePanelContent.id != null){
   							                    if(elePanelContent.id.indexOf('rdDashboardPanel-') > -1){
   							                        elePanelContent.style.cursor = 'auto';
							                                var eleDashboardContentDiv = Y.Selector.ancestor(elePanelContent, '.yui-content', false);
							                                setTimeout(function(){ReCalculateDashboardPanelSize(elePanelContent.id, 1)}, 400);					                    
   							                        break;
   							                    }else{
   							                        elePanelContent = elePanelContent.parentNode;
   							                    }
   							                }	
   							            }	  
   							        }
   							    } 
   							} else if(sElementIDs[i].indexOf('rdDashboardParamsID-') > -1){
   							    var sPanelIdWithGuid = sElementIDs[i].replace('rdDashboardParamsID', '');   		
   							    var eleElementsToHideOnParamsCancel = document.getElementById('rdElementsToHideOnParamsCancel' + sPanelIdWithGuid);	
   							    ShowElement(null,eleElementsToHideOnParamsCancel.value,'Toggle');
   							}
						    
						    //Tabs elements need to run some script #11085
							var eleTabs = null;								
							var eleTables = document.getElementById(sElementIDs[i]).getElementsByTagName('table');
							for (var j = 0; j < eleTables.length; j++) {
								if (eleTables[j].getAttribute("rdElement") == 'Tabs')
									eleTabs = eleTables[j];
							}
													
							if (eleTabs) {
								var scriptTab = document.getElementById("rdTabsScript_" + eleTabs.getAttribute('id'));									
								if (scriptTab) {
									//Make the script.
									var sScript = "var currTabIndex = document.getElementById('rdActiveTabIndex_" + eleTabs.getAttribute('id') + "').value;";
									sScript += "currTabIndex=isNaN(parseInt(currTabIndex))?0:parseInt(currTabIndex);"
									sScript += scriptTab.text;
									sScript += "rdTabs_" + eleTabs.getAttribute('id') + ".set('activeIndex',currTabIndex);";										
									//Run the script.
									eval(sScript);
								}
							}
						
						    // Animated Charts.
						    if(eleOld.getAttribute('id').match('rdAnimatedChart')){						        
				                if(xmlResponse.text){   //IE.
				                    if (xmlResponse.text.toString().match('rdLoadAnimatedChart'))					               
                                        rdRerenderAnimatedChart(xmlResponse.text.substring(xmlResponse.text.indexOf('rdLoadAnimatedChart'), xmlResponse.text.length), eleOld);                                        
                                }
                                else{   //FF, Chrome.
                                    if (xmlResponse.documentElement.textContent.toString().match('rdLoadAnimatedChart'))	
                                        rdRerenderAnimatedChart(xmlResponse.documentElement.textContent.substring(xmlResponse.documentElement.textContent.indexOf('rdLoadAnimatedChart'), xmlResponse.documentElement.textContent.length), eleOld);    
                                }
						    }
						    // Fusion Maps.
						    if(eleOld.getAttribute('id').match('rdFusionMap')){						        
				                if(xmlResponse.text){   //IE.
				                    if (xmlResponse.text.toString().match('rdLoadFusionMap'))					               
                                        rdRerenderAnimatedMap(xmlResponse.text.substring(xmlResponse.text.indexOf('rdLoadFusionMap'), xmlResponse.text.length), eleOld);                                        
                                }
                                else{   //FF, Chrome.
                                    if (xmlResponse.documentElement.textContent.toString().match('rdLoadFusionMap'))	
                                        rdRerenderAnimatedMap(xmlResponse.documentElement.textContent.substring(xmlResponse.documentElement.textContent.indexOf('rdLoadFusionMap'), xmlResponse.documentElement.textContent.length), eleOld);    
                                }
						    }
						    
                            if (eleNew) {
                                if (eleNew.getAttribute("rdPopupPanel")=="True") {
                                    //PopupPanel is getting re-hidden with Action.Refresh.  If it was modal, get rid of the shading.
                                    var rdModalShade = rdGetModalShade(document.getElementById(eleNew.getAttribute('id')));
                                    if (rdModalShade != null) {
                                        rdModalShade.style.display="none"
                                    }
                                }                               
                                                            
                                //Tab panels need to get re-hidden and re-shown.
                                if (eleNew.getAttribute("id").indexOf("rdTabPanel_")==0) {
                                    var eleActiveTab = document.getElementById(eleNew.getAttribute("id"))
                                    var eleTabs = eleActiveTab.parentNode
                                    for (var i=0; i < eleTabs.childNodes.length; i++) {
                                        if (eleTabs.childNodes[i].id == eleActiveTab.id) {
                                            eleTabs.childNodes[i].style.display=""
                                        }else{
                                            eleTabs.childNodes[i].style.display="none"
                                        }
                                    }
                                }								
							}                            
						}
						
					}
					break;
					
				case 'CalendarRefreshElement':  // Block added to support the Ajax refresh for the calendar element
					var sElementIDs = xmlResponse.documentElement.getAttribute('rdCalendarRefreshElementID').split(",")
					for (var i=0; i <  sElementIDs.length; i++) { 						
						var sElementID = sElementIDs[i]; 
						eleOld = document.getElementById(sElementID)

//					    var sRowIdentifier = sElementID.substring(sElementID.lastIndexOf('_'));
//					    if(sRowIdentifier.indexOf('Row') == -1) sRowIdentifier = '';
//						sElementID = sElementID.replace(sRowIdentifier, ''); //#14844.
												
						if (eleOld) {
						
							var eleNew;
							
							if ((eleOld.tagName.toUpperCase() != "INPUT"))
								eleNew = replaceHTMLElement(eleOld, xmlResponse, sElementID);
																			
							if(eleNew){
								if (eleNew.getAttribute("rdElementIdentifier")=="Calendar"){
									rdOnloadColoring(eleNew.getAttribute("id"));
									rdOnLoadJavascriptAddition(eleNew.getAttribute("id"));
								}
								if (eleNew.getAttribute("id").match("TableForInputTime")){
									rdResizeAMPMTable();
									rdNeedForSecondsDisplay();
									rdLoadInputTimeDefaultValue();
								}   
							}   
                        }
					}
					break;

				case 'UpdateTreeBranchRows':
					//Find the end position of the clicked table row.
					var sRowGUID =  xmlResponse.documentElement.getAttribute('rdRowGUID')
					var sTableDivID = xmlResponse.documentElement.getAttribute('id')
					var eleTableDiv = document.getElementById(sTableDivID)
					if (eleTableDiv) {
						var sTable = eleTableDiv.innerHTML
						var nInsertPos = sTable.indexOf(sRowGUID)
						nInsertPos = sTable.indexOf("rdRowEnd",nInsertPos)
						nInsertPos = sTable.indexOf("</tr>",nInsertPos) + 5

						//Remove the response's outer DIV.
						sResponse = sResponse.substr(sResponse.indexOf(">") + 1)
						sResponse = sResponse.substr(0,sResponse.lastIndexOf("<"))

						//Insert the returned rows.
						eleTableDiv.innerHTML = sTable.substr(0,nInsertPos) + sResponse + sTable.substr(nInsertPos)
						////Remove the outer DIV.
						//sResponse = sResponse.substr(sResponse.indexOf(">") + 1)
						//sResponse = sResponse.substr(0,sResponse.lastIndexOf("<"))
						
					}
					break;
					
				case 'UpdateMapImage':
				    //Used by AWS Map Images
					var sImageID = xmlResponse.documentElement.getAttribute('id')
					var eleImage = document.getElementById(sImageID)
					if (eleImage) {
					    //Update the image SRC.
		                var sImageSrc = xmlResponse.documentElement.getAttribute('rdSrc')
		                eleImage.setAttribute("src",sImageSrc)
                    }
                    break;
					
				case 'RequestRefreshElement':
				    //Request back to the server so that just this element is refreshed.
					var sElementID = xmlResponse.documentElement.getAttribute('ElementID')
					var sReport = xmlResponse.documentElement.getAttribute('rdReport')
				    rdAjaxRequest('rdAjaxCommand=RefreshElement&rdRefreshElementID=' + sElementID + '&rdReport=' + sReport + 'rdRequestForwarding=Form');
					break;
 
				case 'RequestRefreshPage':
				    var sHref = window.location.href
				    window.location.href = sHref
					break;

				case 'RunJavascript':
				    eval(xmlResponse.documentElement.getAttribute("Script"))
					break;
					
				case 'ShowElement':
				    ShowElement(null,xmlResponse.documentElement.getAttribute("ElementID"),xmlResponse.documentElement.getAttribute("Action"));
					break;

				case 'ShowStatus':
				    window.status = xmlResponse.documentElement.getAttribute("Status")
				    
				    //Hide the popup when status returns.
				    if (typeof(rdEmailReportPopupId) != 'undefined') {  
				        if (rdEmailReportPopupId != null) {   
                            ShowElement(null,rdEmailReportPopupId,'Hide')
                            rdEmailReportPopupId = undefined
                        }
                    }
                    
					break;
					
		}
		
		LogiXML.Ajax.AjaxTarget().fire('reinitialize');
	
		if (typeof window.rdRepositionSliders != 'undefined') {
			//Move CellColorSliders, if there are any.
			rdRepositionSliders()
		}		
	}
	
	//May need to run some script.
    rdAjaxRunOnLoad(xmlResponse)   
	    
	sCurrentRequestParams = ""
	bSubmitFormAfterAjax = false
	
    //Manage feedback.
    var bStopFeedback = true
	if (xmlResponse.documentElement.getAttribute("rdAjaxCommand")){
	    if (xmlResponse.documentElement.getAttribute("rdAjaxCommand").indexOf("Request")!=-1) { 
	        //Keep feedback going if we're making another request with RequestRefreshElement or RequestRefreshPage.
	        bStopFeedback = false
        }
    }
    if (bStopFeedback) 
	    rdShowAjaxFeedback(false);
}

 function replaceHTMLElement(eleOld, xmlResponse, newElementID) {
	var eleNew, newYuiNode, xmlNode, placeHolder
		oldYuiNode = Y.one( eleOld );
	
	if (xmlResponse.evaluate) {
		//Chrome, Safari, Opera
		xmlNode = xmlResponse.evaluate("//*[@id='" + newElementID + "']", xmlResponse, null, XPathResult.ANY_TYPE, null);    						    
		eleNew = xmlNode.iterateNext();
		if ( eleNew ) {
			newYuiNode = Y.Node.create( getNodeInnerText(eleNew) );						
		}
	}	 	
	else {
		//IE									
		eleNew = xmlResponse.selectSingleNode("//*[@id='" + newElementID + "']");
		if ( eleNew ) {
			newYuiNode = Y.Node.create( eleNew.xml );
		}
	}
	
	if ( newYuiNode ) {
		// Run destroy against YUI node to cleanup any attached classes and then
		// remove it from dom.
		// Use placeholder element to prevent duplicate IDs from being written to DOM
		placeHolder = Y.Node.create( '<div style="display: none;"></div>' );
		oldYuiNode.insert( placeHolder, 'before' );
		oldYuiNode.empty();
		oldYuiNode.destroy();
		eleOld.parentNode.removeChild( eleOld );
		
		placeHolder.replace( newYuiNode );
		placeHolder.remove( true );
	}

	return eleNew;
}

function getNodeInnerText(xmlObj)
{
   var innerText = "";
   var aIndex;
   var attrib;
   
   innerText += assembleElementStartTag(xmlObj);
   innerText += assembleElementChildren(xmlObj);
   innerText += "</" + xmlObj.nodeName + ">";
   
   return innerText;
}

function assembleElementChildren(xmlObj)
{
   var children = "";
   var cIndex = 0;
   var child;
   for (cIndex=0;cIndex<xmlObj.childNodes.length;cIndex++)
   {
       child = xmlObj.childNodes[cIndex];
       if (child.nodeName != null)
       {
			
           if (child.nodeName != "#text")
           {
			   //#16852
			   if (child.nodeName == 'BR') {
				   children += '<BR />';
			   }
			   else {
				   children += assembleElementStartTag(child);
				   children += assembleElementChildren(child);
				   children += "</" + child.nodeName + ">";
			   }
           }
           else
           {
               if (child.nodeValue != null)
               { children +=  xmlFriendlyValue(child.nodeValue); }
           }
       }       
   }
   
   return children;
}

function assembleElementStartTag(xmlObj)
{
   var element = "";
   element += "<" + xmlObj.nodeName;
   
   if (xmlObj.attributes != null)
   {
       var aIndex = 0;
       for (aIndex=0;aIndex<xmlObj.attributes.length;aIndex++)
       {
           attrib = xmlObj.attributes[aIndex];
           element += " " + attrib.nodeName + "=\"" + xmlFriendlyValue(attrib.nodeValue) + "\"";
       }
   }
   
   element += ">";
   if (xmlObj.nodeValue != null)
   { element += xmlObj.nodeValue; }
   
   return element;
}

function xmlFriendlyValue(inValue)
{
   var outValue = inValue;
   outValue = outValue.toString().replace(/>/g,"&gt;") //10228
   outValue = outValue.toString().replace(/</g,"&lt;")
   outValue = outValue.toString().replace(/"/g,"&quot;")
   return outValue;
}

function rdAjaxRunOnLoad(xml) {
    var scripts = xml.getElementsByTagName('SCRIPT')
    for (var i=0; i < scripts.length; i++) {
        var attrRun = scripts[i].attributes.getNamedItem('rdAjaxRunOnLoad')
        if (attrRun) {
            if (attrRun.value == 'True') {
                if (scripts[i].text) 
                    eval(scripts[i].text); //IE
                else 
                    eval(scripts[i].textContent);                
            }
        }
    }
}

function rdGetFormFieldValue(fld) {
	
	var sValue

	if (fld.id.indexOf("rdRadioButtonGroup") == 0) {
		// Radio buttons
		sFieldId = fld.id.replace(/rdRadioButtonGroup/g, '')
		var cInputs = document.getElementsByTagName("INPUT")
		for (var i = 0; i < cInputs.length; i++) {
			if (cInputs[i].name == sFieldId) {
				if (cInputs[i].checked) {
					sValue = cInputs[i].value
					break
				}
			}
		}
		if (sValue == undefined) {
				sValue = ''
			}

	} else {
		// All other fields
		if (fld.value.length == 0) {
			sValue = ''
		} else {
			sValue = fld.value
		}
	}
	return sValue	
}

function rdReportResponseError(sResponse) {
    try {
        var nPosDebugUrl = sResponse.indexOf("rdDebugUrl=")
        if (nPosDebugUrl != -1) { 
            //Normal path, redirect to the debug page.
            var sDebugUrl = sResponse.substring(nPosDebugUrl + 12)         
            sDebugUrl = sDebugUrl.substring(0,sDebugUrl.indexOf("\""))
            window.location = sDebugUrl
        }else{
            window.top.document.body.innerHTML = sResponse
        }
    }
    catch (e) {	
    }
}


var sFeedbackShowElementID  //#11292.
var sFeedbackHideElementID
function rdShowAjaxFeedback(bShow, sCommandParams) {
    var rdCurrFeedbackElementShow
    var rdCurrFeedbackElementHide
    
    //Undo the previous feedback.
    if(sFeedbackShowElementID)
        for(i=0;i<sFeedbackShowElementID.length;i++){           
            rdCurrFeedbackElementShow=document.getElementById(sFeedbackShowElementID[i].trim())
            if (rdCurrFeedbackElementShow) {
                rdCurrFeedbackElementShow.style.display="none" 
                rdCurrFeedbackElementShow=null
            }
        }
    if(sFeedbackHideElementID)
        for(i=0;i<sFeedbackHideElementID.length;i++){           
            rdCurrFeedbackElementHide=document.getElementById(sFeedbackHideElementID[i].trim())
            if (rdCurrFeedbackElementHide) {
                rdCurrFeedbackElementHide.style.display="" 
                rdCurrFeedbackElementHide=null
            }
        }
    document.documentElement.style.cursor = "auto"

    if (bShow) {
        var sParams
        //Show an element
        sParams = sCommandParams.split("&rdFeedbackShowElementID=")
        if (sParams.length > 1) {
            sFeedbackShowElementID = sParams[1].split("&")[0].split(',')
            for(i=0;i<sFeedbackShowElementID.length;i++){           
                rdCurrFeedbackElementShow=document.getElementById(sFeedbackShowElementID[i].trim())
                if (rdCurrFeedbackElementShow) {
                    rdCurrFeedbackElementShow.style.display="" 
                }
            }
        }
        //Hide an element
        sParams = sCommandParams.split("&rdFeedbackHideElementID=")
        if (sParams.length > 1) {
            sFeedbackHideElementID = sParams[1].split("&")[0].split(',')
            for(i=0;i<sFeedbackHideElementID.length;i++){           
                rdCurrFeedbackElementHide=document.getElementById(sFeedbackHideElementID[i].trim())
                if (rdCurrFeedbackElementHide) {
                    rdCurrFeedbackElementHide.style.display="none" 
                }
            }
        }
    }
}