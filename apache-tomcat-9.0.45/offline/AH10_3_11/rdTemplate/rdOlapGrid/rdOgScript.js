
function rdOgShowTabsState(sPanelIdentifier) {
    rdOgShowTabState("Dimension", sPanelIdentifier)
    rdOgShowTabState("Measure", sPanelIdentifier)
    rdOgShowTabState("CalculatedMeasure", sPanelIdentifier)
    rdOgShowTabState("Filter", sPanelIdentifier)
    rdOgShowTabState("Chart", sPanelIdentifier)
    rdOgShowTabState("Heatmap", sPanelIdentifier)
}
function rdOgShowTabState(sTabName, sPanelName) {
    var elePanel = document.getElementById("row" + sTabName)
    if (elePanel) {
        var eleTab = document.getElementById("col" + sTabName)
        if (eleTab) {
            if (elePanel.style.display == "none") {
                eleTab.setAttribute("CLASS","rdOgUnselectedTab")
                eleTab.setAttribute("className","rdOgUnselectedTab")//for IE7
                
            } else {
                eleTab.setAttribute("CLASS","rdOgSelectedTab")
                eleTab.setAttribute("className","rdOgSelectedTab")  //for IE7
                if(sPanelName == sTabName)  //#11734.
                    rdFadeElementIn(elePanel,250)
            }
        }
    }
}

function rdOgHideRemainingTabs(sPanelId){
    var aPanels = new Array("Dimension", "Measure", "CalculatedMeasure", "Filter");
    for(i=0;i<aPanels.length;i++){
        var elePanel = document.getElementById("row" + aPanels[i]);
        if(elePanel)
            if(elePanel.id != sPanelId)
                if(elePanel.style.display != "none")
                    ShowElement(elePanel.parentNode.id, elePanel.id, 'Hide');

    }
}

function rdOgBatchCommand(sCommand, sOgTableID, sDimension, sGUID) {

    document.rdForm.rdOgBatchCommands.value += sCommand + "," + sDimension + ";"

//javascript:rdOgBatchCommand("DimAddLeft","otOlapGrid", 1, "@Data.rdUNIQUE_NAME~")

    //Handle the UI.
    
    var eleClickedLink = rdFindLink(sGUID)
    var eleClickedImage = eleClickedLink.firstChild
    
    switch (sCommand) {
	    case 'DimAddLeft':
	        eleClickedLink.href = eleClickedLink.href.replace("DimAddLeft","DimDelLeft")
            eleClickedImage.src = "rdTemplate/rdOlapGrid/rdOlapDelDim.gif"
            eleClickedImage.title = "Remove this Dimension from the table."
		    break;
	    case 'DimAddTop':
	        eleClickedLink.href = eleClickedLink.href.replace("DimAddTop","DimDelTop")
            eleClickedImage.src = "rdTemplate/rdOlapGrid/rdOlapDelDim.gif"
            eleClickedImage.title = "Remove this Dimension from the table."
		    break;
	    case 'DimDelLeft':
	        eleClickedLink.href = eleClickedLink.href.replace("DimDelLeft","DimAddLeft")
            eleClickedImage.src = "rdTemplate/rdOlapGrid/rdOlapAddLeft.gif"
            eleClickedImage.title = "Move this Dimension to the Left axis."
		    break;
	    case 'DimDelTop':
	        eleClickedLink.href = eleClickedLink.href.replace("DimDelTop","DimAddTop")
            eleClickedImage.src = "rdTemplate/rdOlapGrid/rdOlapAddTop.gif"
            eleClickedImage.title = "Move this Dimension to the Top axis."
		    break;
	    case 'MeasureAdd':
	        eleClickedLink.href = eleClickedLink.href.replace("Add","Del")
            eleClickedImage.src = "rdTemplate/rdOlapGrid/rdOlapDelDim.gif"
            eleClickedImage.title = "Remove this Measure from the table."
		    break;
	    case 'MeasureDel':
	        eleClickedLink.href = eleClickedLink.href.replace("Del","Add")
            eleClickedImage.src = "rdTemplate/rdOlapGrid/rdOlapAddMeasure.gif"
            eleClickedImage.title = "Add this Measure to the table."
		    break;
    }
    
    //Dimension links
    if (sCommand.indexOf("Dim")!=-1){
        var elePartnerLink
        var elePartnerImage
        if (sCommand.indexOf("DimAdd")==0){
            //Clicked a Dimension.  May have to change the "partner" link too.
            if (sCommand.indexOf("Left")!=-1){
                //Get the "Top" link.
                elePartnerLink = eleClickedLink.nextSibling.nextSibling
            } else {
                //Get the "Left" link.
                elePartnerLink = eleClickedLink.previousSibling.previousSibling
            }
            elePartnerImage = elePartnerLink.firstChild
            
            elePartnerLink.href = elePartnerLink.href.replace("DimDel","DimAdd")
            switch (sCommand) {
                case 'DimAddLeft':
                    elePartnerImage.src = elePartnerImage.src.replace("DelDim.gif","AddTop.gif")
                    elePartnerImage.title = "Move this Dimension to the Top axis."
                    break;
                case 'DimAddTop':
                    elePartnerImage.src = elePartnerImage.src.replace("DelDim.gif","AddLeft.gif")
                    elePartnerImage.title = "Move this Dimension to the Left axis."
                    break;
            }
        }
        
        //Hide the message about adding a left dimension.
        if (sCommand=="DimAddLeft"){
            var eleLeftError = document.getElementById("divDimensionError-NoLeft")
            if (eleLeftError) {eleLeftError.style.display="none"}
        }
    }

    //Show the Update Table button.
    if (sCommand.indexOf("Dim")!=-1){
        var eleUpdateDimensions = document.getElementById("divBatchUpdateTableDims")
        eleUpdateDimensions.style.display=""
    } else {
        var eleUpdateDimensions = document.getElementById("divBatchUpdateTableMeasures")
        eleUpdateDimensions.style.display=""
    }
    
}

function rdFindLink(sGUID) {
	var cA = document.getElementsByTagName("A")
	for (var i = 0; i < cA.length; i++) {
		if (cA[i].href.indexOf(sGUID) != -1) {
			return cA[i]
		}
	}

}

function rdOgBatchDrilldownAll(sDrilldownDimension) {
    document.rdForm.rdOgBatchCommands.value = ""

	var cA = document.getElementsByTagName("A")
	var sUrl
	var sDrilldownPositions = ""
	for (var i = 0; i < cA.length; i++) {
		if (cA[i].href.indexOf("rdOgCommand=Drill") != -1) {
		    if (cA[i].href.indexOf("rdOlapTableID=otOlapGrid") != -1) { //11670
		        if (decodeURIComponent(decodeURIComponent(cA[i].href)).indexOf("rdDrilldownDimension=" + sDrilldownDimension.replace(/ /g,"+")) != -1) { //11673, 15289
		            if (cA[i].href.indexOf("rdDrilledDown=False") != -1) {
    		            if (cA[i].innerHTML.indexOf("rdOgBlank.gif") == -1) {  //12247
		                    //This is a drilldown link under the particular dimension.
		                    var sDrilldownPos = cA[i].href.split("rdDrilldownPosition=") //Get just the drilldown value.
		                    sDrilldownPos = sDrilldownPos[1].split("&rdDrill")
		                    sDrilldownPos = sDrilldownPos[0]
        		            
		                    if (!sUrl) {  //Do this just on the first loop.
		                        //Remove from the link.  Gets added as a form var below because it's too big.
		                        sUrl = cA[i].href.replace("rdDrilldownPosition=" + sDrilldownPos,"")
		                    }
        		            
		                    if (sDrilldownPos.indexOf("%5b")==0) {
		                        //IE6 does a different encoding when it returns the HREF.  4508
		                        sDrilldownPos = sDrilldownPos.replace(/%5b/g,"%255b").replace(/%5d/g,"%255d")
		                    }
	                        sDrilldownPos = sDrilldownPos.replace(/%20/g," ") //11673
		                    sDrilldownPositions += sDrilldownPos + ","
		                }
		            }
                }
            }
		}
	}
	
	if (sDrilldownPositions.length!=0) {
	    hiddenDrilldownPosition=document.createElement("INPUT");
	    hiddenDrilldownPosition.type="HIDDEN"
	    hiddenDrilldownPosition.id="rdDrilldownPosition"
	    hiddenDrilldownPosition.name="rdDrilldownPosition"
	    hiddenDrilldownPosition.value=sDrilldownPositions
	    document.rdForm.appendChild(hiddenDrilldownPosition);
	    SubmitForm(sUrl,'','false','',true) // 15289
	}

}

var rdFiltersDict
function rdInitFilterClicks() {
    if (location.href.indexOf("rdShowFilterPopup")==-1) {return}
    
    //Handle events for the Filter PopupPanel. Run when the page is loaded.
    //Look in this element to re-click previously-clicked positions.
    //Load previous clicks into the collection.
    
    var eleHiddenFilterDimension = document.getElementById("hiddenFilterDimension") 
    
    //Load up the click history.
    var eleFilterClicks = document.getElementById("rdFilterClickHistory") 
    rdFiltersDict = rdClicksStringToDictionary(eleFilterClicks.value)
    if (rdFiltersDict.length==0) {
        return //there's not history.
    }

    //Set clicked state based on previous clicks.    
    var i = 1
    eleChk = document.getElementById("chkFilter_Row" + i)
    while (eleChk) {
        var sPosition = document.getElementById("lblFilterPosition_Row" + i).innerHTML
        switch(rdFiltersDict.rdLookup(sPosition)) {
		    case "some":
		        eleChk.setAttribute("rdCheckState","some")
			    break
		    case "true":
		        eleChk.setAttribute("rdCheckState","true")
			    break
		    case "false":
		        eleChk.setAttribute("rdCheckState","false")
			    break
		    default: //Not set, get the checked state from the parent.
		        if (i==1) {
		            eleChk.setAttribute("rdCheckState","true")
		        }else{
		            eleChk.setAttribute("rdCheckState",rdGetParentCheckedState(sPosition,i))  //Always true or false.
		        }
		        
        }
        i += 1
        eleChk = document.getElementById("chkFilter_Row" + i)
    }
    var nRowCnt = i - 1
    
    rdUpdateParentStates(nRowCnt)
    rdUpdateCheckboxAppearance()
}

function rdOgFilterMember(nRowNr, sFilterDimension) {
    var eleClicked = document.getElementById("chkFilter_Row" + nRowNr)
    var sClickedPosition = document.getElementById("lblFilterPosition_Row" + nRowNr).innerHTML
    
    if (eleClicked.checked == false && eleClicked.getAttribute("rdCheckState") == "some") {
        eleClicked.checked = true   //Reset it from "some" to checked.
    }

    //Handle auto-checking of child nodes and dimming of parent nodes.
    var i = 1
    var eleChk = document.getElementById("chkFilter_Row" + i)
    while (eleChk) {
        if(eleChk.id != "chkFilter_Row" + i) break; //#12925.
        //rdSetCheckboxOpacity(eleChk,1) //Reset the opacity to full.
        var sPosition = document.getElementById("lblFilterPosition_Row" + i).innerHTML
        if (sPosition == sClickedPosition) {
             //This checkbox was the one clicked.
         	eleChk.setAttribute("rdCheckState", eleChk.checked.toString())
        }else if (sPosition.indexOf(sClickedPosition) == 0) {
            //This checkbox is a child of the clicked one.
   			eleChk.setAttribute("rdCheckState",eleClicked.checked.toString())
        }
        i += 1
        eleChk = document.getElementById("chkFilter_Row" + i)
    }
    var nRowCnt = i - 1
    
    //Loop back through backwards
    for (i=i-1; i>0; i--) {    
        var eleChk = document.getElementById("chkFilter_Row" + i)
        var sPosition = document.getElementById("lblFilterPosition_Row" + i).innerHTML
        
        //Save the checked state in an element passed back and forth from the server.
        //Set this element that will be used by script to reset checks,
        //and also used on the server side to set the filters.
        if (rdFiltersDict.rdLookup(sPosition)) {
            rdFiltersDict.rdDelete(sPosition)  //Remove the old click record for this position.
        }
        rdFiltersDict.rdDelete(sPosition)
        rdFiltersDict.rdAdd(sPosition,eleChk.getAttribute("rdCheckState"))

    }
    
    rdUpdateParentStates(nRowCnt)
    rdUpdateCheckboxAppearance()
    
    //Remove the save state of all child positions of the clicked one. Only need history for Positions with "some".
    for(i=rdFiltersDict.rdKeys.length-1; i > -1; i--) {
        sPosition = rdFiltersDict.rdKeys[i]
        if (sPosition != sClickedPosition && sPosition.indexOf(sClickedPosition)!=-1){
            rdFiltersDict.rdDelete(sPosition)
        }
    }
    
    var eleFilterClicks = document.getElementById("rdFilterClickHistory")
    eleFilterClicks.value = rdClicksDictionaryToString(rdFiltersDict)
    
    //Hide the Done button if the first row is un-checked.
    if (document.getElementById("chkFilter_Row1").getAttribute("rdCheckState")=="false"){
        ShowElement(null,"lblFilterDone",'Hide');
    }else{
        ShowElement(null,"lblFilterDone",'Show');
    }
}

function rdUpdateParentStates(nRowCnt) {
    //Loop back through backwards
    for (i=nRowCnt-1; i>0; i--) {    
        // set the state for parent positions
        var eleChk = document.getElementById("chkFilter_Row" + i)
        var sPosition = document.getElementById("lblFilterPosition_Row" + i).innerHTML
        var sCurrState = rdGetDescendantCheckedState(eleChk,sPosition)
        switch (sCurrState) {
		    case "?":  //No open descendants
			    break
		    default: // some, true or false
		        if (eleChk.getAttribute("rdCheckState") != sCurrState) {
		            eleChk.setAttribute("rdCheckState",sCurrState)
                    rdFiltersDict.rdDelete(sPosition)
                    rdFiltersDict.rdAdd(sPosition,eleChk.getAttribute("rdCheckState"))
                }
			    break
        }
    }
}

function rdUpdateCheckboxAppearance() {
    var i = 1
    var eleChk = document.getElementById("chkFilter_Row" + i)
    while (eleChk) {
        //Set the state of each checkbox from the rdCheckState attribute.
        switch (eleChk.getAttribute("rdCheckState")) {
            case "some":
                rdSetCheckboxOpacity(eleChk,.3)
                eleChk.checked=true
                break
            case "true":
                rdSetCheckboxOpacity(eleChk,1)
                eleChk.checked=true
                break
            case "false":
                rdSetCheckboxOpacity(eleChk,1)
                eleChk.checked=false
                break
        }
        i += 1
        eleChk = document.getElementById("chkFilter_Row" + i)
    }
}

function rdGetDescendantCheckedState(eleAncestorChk, sAncestorPosition) {
    //Return "true", "false" or "some" depending on the checked state of descendants.
    var sReturn = "?"
    var i = 1
    var eleChk = document.getElementById("chkFilter_Row" + i)
    while (eleChk) {
        if(eleChk.id != "chkFilter_Row" + i) break; //#12925.
        var sPosition = document.getElementById("lblFilterPosition_Row" + i).innerHTML
        if (sPosition != sAncestorPosition  && sPosition.indexOf(sAncestorPosition) == 0) {
            //This checkbox is a descendant.
            if (sReturn == "?") {
                sReturn = eleChk.getAttribute("rdCheckState")
            }else if(sReturn != eleChk.getAttribute("rdCheckState")) {
                return "some"
            }
        }
        i += 1
        eleChk = document.getElementById("chkFilter_Row" + i)
    }
    return sReturn
}

function rdGetParentCheckedState(sChildPosition, i) {
    //Return "true", "false" or "some" depending on the checked state of the parent position.
    //Loop back through backwards
    for (i=i-1; i>0; i--) {    
        var sPosition = document.getElementById("lblFilterPosition_Row" + i).innerHTML
        if (sChildPosition.indexOf(sPosition) == 0) {
            //This is the parent.
            var eleChk = document.getElementById("chkFilter_Row" + i)
            return eleChk.getAttribute("rdCheckState")
        }
    }
    return "true"  //Should never get this far.
}

function rdSetCheckboxOpacity(eleChk,alpha){
    if (eleChk.currentStyle) {
        //For IE, prevent too much transparency by setting a background color.
        var popupPanel = document.getElementById("rdPopupPanelTable_popupFilter")
        //eleChk.offsetParent.style.backgroundColor=popupPanel.currentStyle.backgroundColor -Commented out to address #13147.
        rdSetElementOpacity(eleChk.offsetParent,1)
    }
    rdSetElementOpacity(eleChk,alpha)
}


function rdClicksDictionaryToString(dictClicks) {
    var sClicks = ""
    for(i=0; i < dictClicks.rdKeys.length; i++) {
        sPosition = dictClicks.rdKeys[i]
        sClicks += sPosition + "_rdClk_" + dictClicks[sPosition] + "_rdEnd_"
    }
    return sClicks
}
function rdClicksStringToDictionary(sClicks) {
    var dict = new rdDictionary()
    var clicks = sClicks.split(/_rdEnd_/)
    for (var i=0; i<clicks.length; i++) {
        if(clicks[i].length > 0) {
            var sPosition = clicks[i].split(/_rdClk_/)[0]   
            dict.rdAdd(sPosition, clicks[i].split(/_rdClk_/)[1])
        }
    }
    return dict
}

///////Dictionary Object from http://forums.asp.net/t/1324415.aspx ///////////
function rdLookup(key) 
{
  	try {
        return(this[key]);
    }
    catch(e){}
}
function rdDelete() 
{
  for (c=0; c < rdDelete.arguments.length; c++) 
  {
    this[rdDelete.arguments[c]] = null;
  }
  // Adjust the keys (not terribly efficient)
  var keys = new Array()
  for (var i=0; i<this.rdKeys.length; i++)
  {
    if(this[this.rdKeys[i]] != null)
      keys[keys.length] = this.rdKeys[i];
  }
  this.rdKeys = keys;
}
function rdAdd() 
{
  for (c=0; c < rdAdd.arguments.length; c+=2) 
  {
    // Add the property
    this[rdAdd.arguments[c]] = rdAdd.arguments[c+1];
    // And add it to the keys array
    this.rdKeys[this.rdKeys.length] = rdAdd.arguments[c];
  }
}
function rdDictionary() 
{
  this.rdAdd = rdAdd;
  this.rdLookup = rdLookup;
  this.rdDelete = rdDelete;
  this.rdKeys = new Array();
}  
