<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="WebStudioInterface.aspx.vb" Inherits="LogiAdHoc.WebStudio_WebStudioInterface" %>

<%@ Register TagPrefix="AdHoc" TagName="MainMenu" Src="~/ahControls/MainMenu.ascx" %>
<%@ Register TagPrefix="AdHoc" TagName="NavMenu" Src="~/ahControls/NavMenu.ascx" %>
<%@ Register TagPrefix="AdHoc" TagName="BreadCrumbTrail" Src="~/ahControls/BreadCrumbTrail.ascx" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Web Studio</title>
    <link rel="shortcut icon" href="../ahImages/flav.ico" />
    <script language="JavaScript" type="text/javascript" src="../ahScripts/ahChange.js"></script>
</head>
<body id="bod">
    <AdHoc:MainMenu ID="menu" runat="server" />
    
    <form id="form1" runat="server">
	<div id="submenu"><AdHoc:NavMenu id="subnav" runat="server" /></div>	
    <AdHoc:BreadCrumbTrail ID="bct" runat="server" />
        
        <asp:ScriptManager ID="ScriptManager1" runat="server" EnablePartialRendering=true  />

        <script type="text/javascript">
//        Sys.WebForms.PageRequestManager.getInstance().add_endRequest(EndRequestHandler);
//        function EndRequestHandler(sender, args) {
//            if (args.get_error() != undefined && args.get_error().httpStatusCode == '500') {
//                var errorMessage = args.get_error().message;
//                args.set_errorHandled(true);
//            }
//        }
        function calcHeight()
        {
          //find the height of the internal page
          var the_height = document.getElementById('ifrmWebStudio').contentWindow.document.body.scrollHeight;

          //change the height of the iframe
          document.getElementById('ifrmWebStudio').height = the_height;
        }
        
        </script> 

        <div>
            <iframe id="ifrmWebStudio" runat="server" style="position:absolute; left:0px;" width="100%" frameborder="0">
                Test IFrame
            </iframe>
        </div>
    </form>
</body>
</html>
