<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="Search.ascx.vb" Inherits="LogiAdHoc.Search" %>

<asp:Panel ID="pnlSearch" runat="server" DefaultButton="btnSearch" CssClass="formSearch">
    <asp:UpdatePanel ID="UP1" runat="server" UpdateMode="Conditional" RenderMode="Inline">
        <ContentTemplate>
            <asp:TextBox ID="SearchText" runat="server" CssClass="searchText" title="Search Text" meta:resourcekey="SearchTextResource1"></asp:TextBox><asp:ImageButton ID="imgClear" runat="server" SkinID="imgClear" CssClass="btnClear" CausesValidation="false" AlternateText="Clear search phrase" ToolTip="Clear search phrase" meta:resourcekey="btnClearSrch" />
            <AdHoc:LogiButton ID="btnSearch" Text="Search" runat="server" OnClick="btnSearch_Click" CausesValidation="false" meta:resourcekey="btnSearchResource1" UseSubmitBehavior="false" />
            <asp:Panel ID="pnlMessage" runat="server">
                <asp:Localize ID="Localize2" runat="server" meta:resourcekey="LiteralResource2" Text="No records found."></asp:Localize>
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Panel>
