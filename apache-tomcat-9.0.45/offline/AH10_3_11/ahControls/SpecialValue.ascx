<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="SpecialValue.ascx.vb" Inherits="LogiAdHoc.SpecialValue" %>
<%@ Register TagPrefix="wizard" TagName="simpledatebox" Src="../ahControls/DateBox.ascx" %>
<%@ Register TagPrefix="wizard" TagName="numbox" Src="../ahControls/NumBox.ascx" %>
<asp:Label ID="lblddlValueType" runat="server" CssClass="NoShow" Text="Value Type" AssociatedControlID="ddlValueType" meta:resourcekey="lblddlValueTypeResource1"></asp:Label>
<asp:DropDownList ID="ddlValueType" AutoPostBack="True" runat="server" meta:resourcekey="ddlValueTypeResource1">
    <asp:ListItem Value="0" Text="<%$ Resources:LogiAdHoc, SpecificValue %>" />
    <asp:ListItem Value="1" Text="<%$ Resources:LogiAdHoc, PreDefinedDate %>" />
    <asp:ListItem Value="2" Text="<%$ Resources:LogiAdHoc, OtherDataColumn %>" />
    <asp:ListItem Value="6" Text="<%$ Resources:LogiAdHoc, InputFilter %>" />
    <asp:ListItem Value="7" Text="<%$ Resources:LogiAdHoc, SessionParamValue %>" />
    <asp:ListItem Value="8" Text="<%$ Resources:LogiAdHoc, AllValues %>" />
</asp:DropDownList>
<asp:Label ID="lblTxtValue" runat="server" CssClass="NoShow" Text="Value" AssociatedControlID="txt" meta:resourcekey="lbltxtValueResource1"></asp:Label>
<asp:Textbox ID="txt" Runat="server" meta:resourcekey="txtResource1" />
<wizard:numbox id="NumBox" runat="server" usevalidator="False" />
<wizard:simpledatebox ID="DateBox" runat="server" />
<asp:Label ID="lblddlTokenDates" runat="server" CssClass="NoShow" Text="Pre-Defined Date" AssociatedControlID="ddlTokenDates" meta:resourcekey="lblddlTokenDatesResource1"></asp:Label>
<asp:DropDownList ID="ddlTokenDates" runat="server" meta:resourcekey="ddlTokenDatesResource1" >
    <asp:ListItem Value="@Date.Today~" meta:resourcekey="ListItemResource4">Today</asp:ListItem>
    <asp:ListItem Value="@Date.Yesterday~" meta:resourcekey="ListItemResource5">Yesterday</asp:ListItem>
    <asp:ListItem Value="@Date.Tomorrow~" meta:resourcekey="ListItemResource6">Tomorrow</asp:ListItem>
    <asp:ListItem Value="@Date.LastWeekStart~" meta:resourcekey="ListItemResource7">Last Week Start</asp:ListItem>
    <asp:ListItem Value="@Date.LastWeekEnd~" meta:resourcekey="ListItemResource8">Last Week End</asp:ListItem>
    <asp:ListItem Value="@Date.ThisWeekStart~" meta:resourcekey="ListItemResource9">This Week Start</asp:ListItem>
    <asp:ListItem Value="@Date.ThisWeekEnd~" meta:resourcekey="ListItemResource10">This Week End</asp:ListItem>
    <asp:ListItem Value="@Date.NextWeekStart~" meta:resourcekey="ListItemResource11">Next Week Start</asp:ListItem>
    <asp:ListItem Value="@Date.NextWeekEnd~" meta:resourcekey="ListItemResource12">Next Week End</asp:ListItem>
    <asp:ListItem Value="@Date.LastMonthStart~" meta:resourcekey="ListItemResource13">Last Month Start</asp:ListItem>
    <asp:ListItem Value="@Date.LastMonthEnd~" meta:resourcekey="ListItemResource14">Last Month End</asp:ListItem>
    <asp:ListItem Value="@Date.ThisMonthStart~" meta:resourcekey="ListItemResource15">This Month Start</asp:ListItem>
    <asp:ListItem Value="@Date.ThisMonthEnd~" meta:resourcekey="ListItemResource16">This Month End</asp:ListItem>
    <asp:ListItem Value="@Date.NextMonthStart~" meta:resourcekey="ListItemResource17">Next Month Start</asp:ListItem>
    <asp:ListItem Value="@Date.NextMonthEnd~" meta:resourcekey="ListItemResource18">Next Month End</asp:ListItem>
    <asp:ListItem Value="@Date.LastQuarterStart~" meta:resourcekey="ListItemResource19">Last Quarter Start</asp:ListItem>
    <asp:ListItem Value="@Date.LastQuarterEnd~" meta:resourcekey="ListItemResource20">Last Quarter End</asp:ListItem>
    <asp:ListItem Value="@Date.ThisQuarterStart~" meta:resourcekey="ListItemResource21">This Quarter Start</asp:ListItem>
    <asp:ListItem Value="@Date.ThisQuarterEnd~" meta:resourcekey="ListItemResource22">This Quarter End</asp:ListItem>
    <asp:ListItem Value="@Date.NextQuarterStart~" meta:resourcekey="ListItemResource23">Next Quarter Start</asp:ListItem>
    <asp:ListItem Value="@Date.NextQuarterEnd~" meta:resourcekey="ListItemResource24">Next Quarter End</asp:ListItem>
    <asp:ListItem Value="@Date.LastYearStart~" meta:resourcekey="ListItemResource25">Last Year Start</asp:ListItem>
    <asp:ListItem Value="@Date.LastYearEnd~" meta:resourcekey="ListItemResource26">Last Year End</asp:ListItem>
    <asp:ListItem Value="@Date.ThisYearStart~" meta:resourcekey="ListItemResource27">This Year Start</asp:ListItem>
    <asp:ListItem Value="@Date.ThisYearEnd~" meta:resourcekey="ListItemResource28">This Year End</asp:ListItem>
    <asp:ListItem Value="@Date.NextYearStart~" meta:resourcekey="ListItemResource29">Next Year Start</asp:ListItem>
    <asp:ListItem Value="@Date.NextYearEnd~" meta:resourcekey="ListItemResource30">Next Year End</asp:ListItem>
    <asp:ListItem Value="@Date.10DaysAgo~" meta:resourcekey="ListItemResource31">10 Days Ago</asp:ListItem>
    <asp:ListItem Value="@Date.30DaysAgo~" meta:resourcekey="ListItemResource32">30 Days Ago</asp:ListItem>
    <asp:ListItem Value="@Date.60DaysAgo~" meta:resourcekey="ListItemResource33">60 Days Ago</asp:ListItem>
    <asp:ListItem Value="@Date.90DaysAgo~" meta:resourcekey="ListItemResource34">90 Days Ago</asp:ListItem>
    <asp:ListItem Value="@Date.365DaysAgo~" meta:resourcekey="ListItemResource35">365 Days Ago</asp:ListItem>
    <asp:ListItem Value="@Date.LastFiscalQuarterStart~" meta:resourcekey="ListItemResource36">Last Fiscal Quarter Start</asp:ListItem>
    <asp:ListItem Value="@Date.LastFiscalQuarterEnd~" meta:resourcekey="ListItemResource37">Last Fiscal Quarter End</asp:ListItem>
    <asp:ListItem Value="@Date.ThisFiscalQuarterStart~" meta:resourcekey="ListItemResource38">This Fiscal Quarter Start</asp:ListItem>
    <asp:ListItem Value="@Date.ThisFiscalQuarterEnd~" meta:resourcekey="ListItemResource39">This Fiscal Quarter End</asp:ListItem>
    <asp:ListItem Value="@Date.NextFiscalQuarterStart~" meta:resourcekey="ListItemResource40">Next Fiscal Quarter Start</asp:ListItem>
    <asp:ListItem Value="@Date.NextFiscalQuarterEnd~" meta:resourcekey="ListItemResource41">Next Fiscal Quarter End</asp:ListItem>
    <asp:ListItem Value="@Date.LastFiscalYearStart~" meta:resourcekey="ListItemResource42">Last Fiscal Year Start</asp:ListItem>
    <asp:ListItem Value="@Date.LastFiscalYearEnd~" meta:resourcekey="ListItemResource43">Last Fiscal Year End</asp:ListItem>
    <asp:ListItem Value="@Date.ThisFiscalYearStart~" meta:resourcekey="ListItemResource44">This Fiscal Year Start</asp:ListItem>
    <asp:ListItem Value="@Date.ThisFiscalYearEnd~" meta:resourcekey="ListItemResource45">This Fiscal Year End</asp:ListItem>
    <asp:ListItem Value="@Date.NextFiscalYearStart~" meta:resourcekey="ListItemResource46">Next Fiscal Year Start</asp:ListItem>
    <asp:ListItem Value="@Date.NextFiscalYearEnd~" meta:resourcekey="ListItemResource47">Next Fiscal Year End</asp:ListItem>
</asp:DropDownList>
<asp:Label ID="lblddlDBColumn" runat="server" CssClass="NoShow" Text="Column" AssociatedControlID="ddlDBColumn" meta:resourcekey="lblddlDBColumnResource1"></asp:Label>
<asp:DropDownList ID="ddlDBColumn" runat="server" meta:resourcekey="ddlDBColumnResource1" />
<asp:Label ID="lblddlInputFilter" runat="server" CssClass="NoShow" Text="Filter" AssociatedControlID="ddlInputFilter" meta:resourcekey="lblddlInputFilterResource1"></asp:Label>
<asp:DropDownList ID="ddlInputFilter" runat="server" />
<asp:Label ID="lblddlSessionParameter" runat="server" CssClass="NoShow" Text="Session Parameter" AssociatedControlID="ddlSessionParameter" meta:resourcekey="lblddlSessionParameterResource1"></asp:Label>
<asp:DropDownList ID="ddlSessionParameter" runat="server" />
<div id="divTextArea" runat="server">
    <asp:Label ID="lbltxaTextArea" runat="server" CssClass="NoShow" Text="Values" AssociatedControlID="txaTextArea" meta:resourcekey="lbltxaTextAreaResource1"></asp:Label>
    <asp:TextBox ID="txaTextArea" runat="server" TextMode="MultiLine" Rows="8" meta:resourcekey="txaTextAreaResource1" />
</div>