<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="NamedDateBox.ascx.vb" Inherits="LogiAdHoc.NamedDateBox" %>
<%@ Register tagprefix="wizard" tagname="datebox" src="~/ahControls/DateBox.ascx" %>
	<asp:DropDownList ID="DateName" AutoPostBack="True" Runat="server" meta:resourcekey="DateNameResource1">
		<asp:ListItem meta:resourcekey="ListItemResource1">Specific Date</asp:ListItem>
		<asp:ListItem Value="@Date.Today~" meta:resourcekey="ListItemResource2">Today</asp:ListItem>
		<asp:ListItem Value="@Date.Yesterday~" meta:resourcekey="ListItemResource3">Yesterday</asp:ListItem>
		<asp:ListItem Value="@Date.Tomorrow~" meta:resourcekey="ListItemResource4">Tomorrow</asp:ListItem>
		<asp:ListItem Value="@Date.LastWeekStart~" meta:resourcekey="ListItemResource5">Last Week Start</asp:ListItem>
		<asp:ListItem Value="@Date.LastWeekEnd~" meta:resourcekey="ListItemResource6">Last Week End</asp:ListItem>
		<asp:ListItem Value="@Date.ThisWeekStart~" meta:resourcekey="ListItemResource7">This Week Start</asp:ListItem>
		<asp:ListItem Value="@Date.ThisWeekEnd~" meta:resourcekey="ListItemResource8">This Week End</asp:ListItem>
		<asp:ListItem Value="@Date.NextWeekStart~" meta:resourcekey="ListItemResource9">Next Week Start</asp:ListItem>
		<asp:ListItem Value="@Date.NextWeekEnd~" meta:resourcekey="ListItemResource10">Next Week End</asp:ListItem>
		<asp:ListItem Value="@Date.LastMonthStart~" meta:resourcekey="ListItemResource11">Last Month Start</asp:ListItem>
		<asp:ListItem Value="@Date.LastMonthEnd~" meta:resourcekey="ListItemResource12">Last Month End</asp:ListItem>
		<asp:ListItem Value="@Date.ThisMonthStart~" meta:resourcekey="ListItemResource13">This Month Start</asp:ListItem>
		<asp:ListItem Value="@Date.ThisMonthEnd~" meta:resourcekey="ListItemResource14">This Month End</asp:ListItem>
		<asp:ListItem Value="@Date.NextMonthStart~" meta:resourcekey="ListItemResource15">Next Month Start</asp:ListItem>
		<asp:ListItem Value="@Date.NextMonthEnd~" meta:resourcekey="ListItemResource16">Next Month End</asp:ListItem>
		<asp:ListItem Value="@Date.LastQuarterStart~" meta:resourcekey="ListItemResource17">Last Quarter Start</asp:ListItem>
		<asp:ListItem Value="@Date.LastQuarterEnd~" meta:resourcekey="ListItemResource18">Last Quarter End</asp:ListItem>
		<asp:ListItem Value="@Date.ThisQuarterStart~" meta:resourcekey="ListItemResource19">This Quarter Start</asp:ListItem>
		<asp:ListItem Value="@Date.ThisQuarterEnd~" meta:resourcekey="ListItemResource20">This Quarter End</asp:ListItem>
		<asp:ListItem Value="@Date.NextQuarterStart~" meta:resourcekey="ListItemResource21">Next Quarter Start</asp:ListItem>
		<asp:ListItem Value="@Date.NextQuarterEnd~" meta:resourcekey="ListItemResource22">Next Quarter End</asp:ListItem>
		<asp:ListItem Value="@Date.LastYearStart~" meta:resourcekey="ListItemResource23">Last Year Start</asp:ListItem>
		<asp:ListItem Value="@Date.LastYearEnd~" meta:resourcekey="ListItemResource24">Last Year End</asp:ListItem>
		<asp:ListItem Value="@Date.ThisYearStart~" meta:resourcekey="ListItemResource25">This Year Start</asp:ListItem>
		<asp:ListItem Value="@Date.ThisYearEnd~" meta:resourcekey="ListItemResource26">This Year End</asp:ListItem>
		<asp:ListItem Value="@Date.NextYearStart~" meta:resourcekey="ListItemResource27">Next Year Start</asp:ListItem>
		<asp:ListItem Value="@Date.NextYearEnd~" meta:resourcekey="ListItemResource28">Next Year End</asp:ListItem>
		<asp:ListItem Value="@Date.10DaysAgo~" meta:resourcekey="ListItemResource29">10 Days Ago</asp:ListItem>
		<asp:ListItem Value="@Date.30DaysAgo~" meta:resourcekey="ListItemResource30">30 Days Ago</asp:ListItem>
		<asp:ListItem Value="@Date.60DaysAgo~" meta:resourcekey="ListItemResource31">60 Days Ago</asp:ListItem>
		<asp:ListItem Value="@Date.90DaysAgo~" meta:resourcekey="ListItemResource32">90 Days Ago</asp:ListItem>
		<asp:ListItem Value="@Date.365DaysAgo~" meta:resourcekey="ListItemResource33">365 Days Ago</asp:ListItem>
        <asp:ListItem Value="@Date.LastFiscalQuarterStart~" meta:resourcekey="ListItemResource34">Last Fiscal Quarter Start</asp:ListItem>
        <asp:ListItem Value="@Date.LastFiscalQuarterEnd~" meta:resourcekey="ListItemResource35">Last Fiscal Quarter End</asp:ListItem>
        <asp:ListItem Value="@Date.ThisFiscalQuarterStart~" meta:resourcekey="ListItemResource36">This Fiscal Quarter Start</asp:ListItem>
        <asp:ListItem Value="@Date.ThisFiscalQuarterEnd~" meta:resourcekey="ListItemResource37">This Fiscal Quarter End</asp:ListItem>
        <asp:ListItem Value="@Date.NextFiscalQuarterStart~" meta:resourcekey="ListItemResource38">Next Fiscal Quarter Start</asp:ListItem>
        <asp:ListItem Value="@Date.NextFiscalQuarterEnd~" meta:resourcekey="ListItemResource39">Next Fiscal Quarter End</asp:ListItem>
        <asp:ListItem Value="@Date.LastFiscalYearStart~" meta:resourcekey="ListItemResource40">Last Fiscal Year Start</asp:ListItem>
        <asp:ListItem Value="@Date.LastFiscalYearEnd~" meta:resourcekey="ListItemResource41">Last Fiscal Year End</asp:ListItem>
        <asp:ListItem Value="@Date.ThisFiscalYearStart~" meta:resourcekey="ListItemResource42">This Fiscal Year Start</asp:ListItem>
        <asp:ListItem Value="@Date.ThisFiscalYearEnd~" meta:resourcekey="ListItemResource43">This Fiscal Year End</asp:ListItem>
        <asp:ListItem Value="@Date.NextFiscalYearStart~" meta:resourcekey="ListItemResource44">Next Fiscal Year Start</asp:ListItem>
        <asp:ListItem Value="@Date.NextFiscalYearEnd~" meta:resourcekey="ListItemResource45">Next Fiscal Year End</asp:ListItem>
	</asp:DropDownList>
	<wizard:datebox id="DateValue" runat="server" />
	