<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="ColPermissions2.ascx.vb" Inherits="LogiAdHoc.ahControls_ColPermissions2" %>

<%@ Register Src="PagingControl.ascx" TagName="PagingControl" TagPrefix="AdHoc" %>

<%--    <div style="height:20px;">
	<h3><asp:Label id="PageCaption" text="Column Access Rights" Runat="server" meta:resourcekey="PageCaptionResource1" /></h3>
    </div>
--%>    
    <div id="divGrid" style="width: 580px; Height: 455px; overflow: auto;">
	    <input type="hidden" id="ObjectID" runat="server" name="ObjectID"/>
	    <input type="hidden" id="RoleID" runat="server" name="RoleID"/>&nbsp;
	    <div id="data_main" style="width:540px;">
	    <div id="activities">
            <table width="100%" cellpadding="0" cellspacing="0">
            <tr width="100%">
            <td align="left" valign="top">
            <AdHoc:LogiButton ID="SetFull" ToolTip="Click to set access level for selected columns to Full." Text="Set To Full" CommandName="SetFull" OnCommand="SetAccessType" Runat="server" meta:resourcekey="SetFullResource1" />
	        <AdHoc:LogiButton ID="SetNone" ToolTip="Click to set access level for selected columns to None." Text="Set To None" CommandName="SetNone" OnCommand="SetAccessType" Runat="server" meta:resourcekey="SetNoneResource1" />
            </td></tr></table>
	    </div>
        <asp:GridView ID="ColumnGrid" runat="server" AutoGenerateColumns="False"
            CssClass="grid" Width="540px" OnRowDataBound="OnColumnItemDataBoundHandler" DataKeyNames="ColumnID" meta:resourcekey="ColumnGridResource1">
            <Columns>
                <asp:TemplateField>
                    <HeaderStyle Width="30px"></HeaderStyle>
                    <HeaderTemplate>
                        <asp:Label ID="lblChkAll" runat="server" AssociatedControlID="CheckAll" Text="<%$ Resources:LogiAdHoc, SelectDeselectAll %>" CssClass="NoShow"></asp:Label>
                        <asp:CheckBox ID="CheckAll" runat="server" ToolTip="<%$ Resources:LogiAdHoc, SelectDeselectAll %>" />
                    </HeaderTemplate>
                    <ItemStyle HorizontalAlign="Center" />
                    <ItemTemplate>
                        <asp:Label ID="lblChk" runat="server" AssociatedControlID="chk_Select" Text="<%$ Resources:LogiAdHoc, SelectDeselect %>" CssClass="NoShow"></asp:Label>
                        <asp:CheckBox ID="chk_Select" runat="server" ToolTip="<%$ Resources:LogiAdHoc, SelectDeselect %>" />
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Column Name" meta:resourcekey="TemplateFieldResource1">
                    <ItemTemplate>
                        <input type="hidden" id="ColumnID" runat="server"/>
                        <asp:Label ID="ColumnName" Runat="server" meta:resourcekey="ColumnNameResource1" />
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Label" meta:resourcekey="TemplateFieldResource2">
                    <ItemTemplate>
                        <asp:Label ID="ColDescription" Runat="server" meta:resourcekey="ColDescriptionResource1" />
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Access" meta:resourcekey="TemplateFieldResource3">
                    <ItemTemplate>
                        <input type="hidden" id="RowChanged" runat="server"/>
                        <input type="hidden" id="ColAccessType" runat="server"/>
                        <%--<asp:DropdownList ID="ColAccessType" AutoPostBack="True" OnSelectedIndexChanged="ColAccessType_SelectedIndexChanged" Runat="server" meta:resourcekey="ColAccessTypeResource1" />--%>
                        <asp:Label ID="lblColAccessType" runat="server" />
                    </ItemTemplate>
                    <HeaderStyle Width="50px" />
                </asp:TemplateField>
            </Columns>
            <HeaderStyle CssClass="gridheader" />
        </asp:GridView>
        </div>
    </div>
<br />
	<%--<div id="actions" runat="server" style="padding-left:4px; padding-top:10px"/>--%>
	<div id="actions" runat="server" style="height:15px; padding-right: 5px; padding-top: 5px;"/>
<br />

	<%--<asp:ValidationSummary id="vsummary" runat="server" meta:resourcekey="vsummaryResource1" />--%>
