<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="SelectFolderControl.ascx.vb" Inherits="LogiAdHoc.SelectFolderControl" %>

<asp:Panel ID="pnlSelectFolderControl" runat="server">
    <table cellpadding="0" cellspacing="4">
        <tr>
            <td>
                <div ID="pnlTreeView" runat="server">
                    <div id="divTreeView" runat="server">
                        <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
                            <ContentTemplate>
                                <asp:TreeView ID="tvFolders" runat="server" ShowLines="True" EnableClientScript="False" >
                                    <NodeStyle HorizontalPadding="4px" ImageUrl="~/ahImages/iconSFolder.gif" />
                                    <SelectedNodeStyle HorizontalPadding="4px" ImageUrl="~/ahImages/iconSFolderOpen.gif" CssClass="treenodeSelected" />
                                </asp:TreeView>
                            </ContentTemplate>
                        </asp:UpdatePanel>                
                    </div>
                </div>
            </td>
        </tr>
    </table>
</asp:Panel>