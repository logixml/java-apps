<%@ Page Language="vb" AutoEventWireup="false" Codebehind="SessionParameters.aspx.vb" Inherits="LogiAdHoc.ahConfiguration_SessionParameters" Culture="auto" meta:resourcekey="PageResource1" UICulture="auto" %>

<%@ Register TagPrefix="AdHoc" TagName="MainMenu" Src="~/ahControls/MainMenu.ascx" %>
<%@ Register TagPrefix="AdHoc" TagName="NavMenu" Src="~/ahControls/NavMenu.ascx" %>
<%@ Register TagPrefix="AdHoc" TagName="BreadCrumbTrail" Src="~/ahControls/BreadCrumbTrail.ascx" %>
<%@ Register TagPrefix="AdHoc" TagName="Search" Src="~/ahControls/Search.ascx" %>
<%@ Register TagPrefix="AdHoc" TagName="PagingControl" Src="../ahControls/PagingControl.ascx" %>
<%@ Register TagPrefix="wizard" TagName="simpledatebox" Src="../ahControls/DateBox.ascx" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Session Parameters</title>
    <link rel="shortcut icon" href="../ahImages/flav.ico" />

    <script language="JavaScript" type="text/javascript" src="../ahScripts/ahChange.js"></script>

    <script type="text/javascript">
        window.onbeforeunload = AlertOnExit;
    </script>

</head>
<body id="bod">
    <AdHoc:MainMenu ID="menu" runat="server" />
    <form id="form1" runat="server">
        <div id="submenu">
            <AdHoc:NavMenu ID="subnav" runat="server" />
        </div>
        <AdHoc:BreadCrumbTrail ID="bct" runat="server" Key="SessionParams" ParentLevels="0" />
        <asp:ScriptManager ID="ScriptManager1" runat="server" />
        
        <script type="text/javascript">
            Sys.WebForms.PageRequestManager.getInstance().add_beginRequest(BeginRequestHandler);
            Sys.WebForms.PageRequestManager.getInstance().add_endRequest(EndRequestHandler);
            function EndRequestHandler(sender, args) {
                $find('ahSavePopupBehavior').hide();
                RestorePopupPosition('ahModalPopupBehavior');
            }
            function BeginRequestHandler(sender, args) {
                SavePopupPosition('ahModalPopupBehavior');
            }
        </script>
        
<table class="limiting"><tr><td>
        <table class="gridForm"><tr><td>
            <input type="hidden" id="ahDirty" name="ahDirty" runat="server" />
                
            <asp:UpdatePanel ID="UP1" UpdateMode="Conditional" runat="server" RenderMode="Inline">
                <ContentTemplate>
                <div id="data_main">
                    <div id="activities">
                        <table width="100%" cellpadding="0" cellspacing="0">
                        <tr width="100%">
                        <td align="left" valign="top">
                        <AdHoc:LogiButton ID="btnAddRow" OnClick="AddRow_OnClick" Text="<%$ Resources:LogiAdHoc, AddWithSpaces %>"
                            runat="server" CausesValidation="false" />
                        <AdHoc:LogiButton ID="btnRemoveRow2" OnClick="RemoveRow_OnClick" Text="<%$ Resources:LogiAdHoc, Delete %>"
                            runat="server" CausesValidation="false" />
                        </td>
                        <td align="right" valign="top">
                        <AdHoc:Search ID="srch" runat="server" Title="Find Session Parameters" meta:resourcekey="AdHocSearch" />
                        </td>
                        </tr>
                        </table>
                    </div>
                    <asp:GridView ID="grdMain" runat="server" AllowPaging="True" AllowSorting="True"
                        AutoGenerateColumns="False" CssClass="grid" DataKeyNames="SessionParameterID"
                        meta:resourcekey="grdMainResource1" OnRowDataBound="OnItemDataBoundHandler" 
                        OnSorting="OnSortCommandHandler">
                        <HeaderStyle CssClass="gridheader"></HeaderStyle>
                        <PagerTemplate>
                            <AdHoc:PagingControl ID="pageCtrl" runat="server" OnGotoNextPage="ChangePageIndex" />
                        </PagerTemplate>
                        <PagerStyle HorizontalAlign="Center" />
                        <FooterStyle CssClass="gridfooter" />
                        <RowStyle CssClass="gridrow" />
                        <AlternatingRowStyle CssClass="gridalternaterow"></AlternatingRowStyle>
                        <Columns>
                            <asp:TemplateField>
                                <HeaderStyle Width="44px"></HeaderStyle>
                                <HeaderTemplate>
                                    <asp:Label ID="lblChkAll" runat="server" AssociatedControlID="CheckAll" Text="<%$ Resources:LogiAdHoc, SelectDeselectAll %>" CssClass="NoShow"></asp:Label>
                                    <asp:CheckBox runat="server" ToolTip="<%$ Resources:LogiAdHoc, SelectDeselectAll %>" ID="CheckAll">
                                    </asp:CheckBox>
                                </HeaderTemplate>
                                <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblChk" runat="server" AssociatedControlID="chk_Select" Text="<%$ Resources:LogiAdHoc, SelectDeselect %>" CssClass="NoShow"></asp:Label>
                                    <asp:CheckBox runat="server" ID="chk_Select" ToolTip="<%$ Resources:LogiAdHoc, SelectDeselect %>">
                                    </asp:CheckBox>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField DataField="ParameterName" HeaderText="Parameter Name" meta:resourcekey="BoundFieldResource1" SortExpression="ParameterName">
                            </asp:BoundField>
                            <asp:TemplateField HeaderText="<%$ Resources:LogiAdHoc, SessionParameters_Type %>">
                                <ItemTemplate>
                                    <asp:Label ID="lblDataTypeCategory" runat="server" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Default Value" meta:resourcekey="BoundFieldResource2">
                                <ItemTemplate>
                                    <asp:Label ID="lblDefaultValue" runat="server" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="<%$ Resources:LogiAdHoc, Actions %>" meta:resourcekey="TemplateFieldResource2">
                                <HeaderStyle Width="60px"></HeaderStyle>
                                <ItemTemplate>
                                    <asp:Image ID="imgActions" AlternateText="<%$ Resources:LogiAdHoc, Actions %>" runat="server" 
                                        ToolTip="<%$ Resources:LogiAdHoc, Actions %>" ImageUrl="~/ahImages/arrowStep.gif" SkinID="imgActions" />
                                    <asp:Panel ID="pnlActionsMenu" runat="server" CssClass="popupMenu" 
                                        HorizontalAlign="Left" Wrap="false" style="display:none;">
                                        <div id="divModify" runat="server" class="hoverMenuActionLink" >
                                            <asp:LinkButton ID="lnkModify" runat="server" OnCommand="EditItem" 
                                                Text="Modify Session Parameter" ToolTip="Modify Session Parameter"
                                                CausesValidation="False" meta:resourcekey="EditItemResource1"></asp:LinkButton>
                                        </div>
                                        <div id="divOrg" runat="server" class="hoverMenuActionLink" >
                                            <asp:LinkButton ID="lnkOrg" runat="server" OnCommand="OrgParams" 
                                                Text="Set by Organization" ToolTip="Set by Organization" 
                                                CausesValidation="False" meta:resourcekey="lnkOrg"></asp:LinkButton>
                                        </div>
                                        <div id="divUsr" runat="server" class="hoverMenuActionLink" >
                                            <asp:LinkButton ID="lnkUsr" runat="server" OnCommand="UsrParams" 
                                                Text="Set by User" ToolTip="Set by User" 
                                                CausesValidation="False" meta:resourcekey="lnkUsr"></asp:LinkButton>
                                        </div>
                                        
                                        <div id="divDelete" runat="server" class="hoverMenuActionLink" visible="false">
                                            <asp:LinkButton ID="lnkDelete" runat="server" OnCommand="DeleteItem" Visible="false" 
                                                Text="Delete Session Parameter" meta:resourcekey="DeleteResource1"></asp:LinkButton>
                                        </div>
                                        
 				                    </asp:Panel>
                                    <ajaxToolkit:HoverMenuExtender ID="hme2" runat="server"
                                        PopupControlID="pnlActionsMenu" PopupPosition="right" 
                                        TargetControlID="imgActions" PopDelay="25" />
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Center"></ItemStyle>
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                </div>
                    <asp:Button runat="server" ID="Button1" Style="display: none" />
                    <ajaxToolkit:ModalPopupExtender runat="server" ID="ahModalPopup" BehaviorID="ahModalPopupBehavior"
                        TargetControlID="Button1" PopupControlID="ahPopup" BackgroundCssClass="modalBackground"
                        DropShadow="false" PopupDragHandleControlID="pnlDragHandle" RepositionMode="None">
                    </ajaxToolkit:ModalPopupExtender>
                    
                    <asp:Panel runat="server" CssClass="modalPopup" ID="ahPopup" Style="display: none;
                        width: 400;">
                        <asp:Panel ID="pnlDragHandle" runat="server" Style="cursor: hand;">
                        <div class="modalPopupHandle">
                            <table cellpadding="0" cellspacing="0" style="width: 100%"><tr><td>
                                    <asp:Localize ID="PopupHeader" runat="server" meta:resourcekey="LiteralResource2" Text="Session Parameter Details"></asp:Localize>
                                </td>
                                <td style="width: 20px;">
                                    <asp:ImageButton ID="imgClosePopup" runat="server" 
                                        OnClick="imgClosePopup_Click" CausesValidation="false"
                                        SkinID="imgbClose" ImageUrl="../ahImages/remove.gif" 
                                        AlternateText="<%$ Resources:LogiAdHoc, CloseWindow %>" />
                            </td></tr></table>
                        </div>
                        </asp:Panel>
                        <div class="modalDiv">
                        <asp:UpdatePanel ID="upPopup" runat="server">
                            <ContentTemplate>
                                <%--<asp:Panel ID="ParamDetails" CssClass="detailpanel" runat="server" meta:resourcekey="ParamDetailsResource1">--%>
                                <input type="hidden" id="NewParamID" runat="server" />
                                <table>
                                    <tr>
                                        <td width="120px" valign="top">
                                            <asp:Localize ID="Localize3" runat="server" meta:resourcekey="LiteralResource3" Text="Parameter Name:"></asp:Localize>
                                        </td>
                                        <td>
                                            <asp:TextBox ID="ParamName" MaxLength="50" Width="150px" runat="server" meta:resourcekey="ParamNameResource1" />
                                            <asp:RequiredFieldValidator ID="rtvParamName" runat="server" ErrorMessage="Parameter Name is required."
                                                ControlToValidate="ParamName" meta:resourcekey="rtvParamNameResource1" ValidationGroup="ParamValue">*</asp:RequiredFieldValidator>
                                            <asp:RegularExpressionValidator ID="revParamName" runat="server" ErrorMessage="<%$ Resources:Errors, Err_SessionParameterNameValid %>" 
                                                ControlToValidate="ParamName" ValidationGroup="ParamValue" ValidationExpression="^[a-zA-Z0-9_]*$" >*</asp:RegularExpressionValidator>
                                            <asp:CustomValidator ID="cvValidName" runat="server" ErrorMessage="<%$ Resources:Errors, Err_SessionParameterNameExists %>" ValidationGroup="ParamValue"
                                                ControlToValidate="ParamName" OnServerValidate="IsNameValid">*</asp:CustomValidator>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td valign="top">
                                            <asp:Localize ID="Localize6" runat="server" meta:resourcekey="LiteralResource6" Text="Type:"></asp:Localize>
                                        </td>
                                        <td>
                                            <asp:DropDownList ID="ddlDataTypeCategory" runat="server" AutoPostBack="true" 
                                                OnSelectedIndexChanged="ddlDataTypeCategory_SelectedIndexChanged">
                                                <asp:ListItem Value="4" Text="<%$ Resources:LogiAdHoc, Res_Date %>" />
                                                <asp:ListItem Value="1" Text="<%$ Resources:LogiAdHoc, Number %>" />
                                                <asp:ListItem Value="1024" Text="<%$ Resources:LogiAdHoc, NumericList %>" />
                                                <asp:ListItem Value="2" Text="<%$ Resources:LogiAdHoc, Text %>" />
                                                <asp:ListItem Value="2048" Text="<%$ Resources:LogiAdHoc, TextualList %>" />
                                             </asp:DropDownList>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:Localize ID="Localize4" runat="server" meta:resourcekey="LiteralResource4" Text="Default Value:"></asp:Localize>
                                        </td>
                                        <td>
                                            <asp:TextBox ID="ParamValue" MaxLength="4000" Width="150px" runat="server" />
                                            <wizard:simpledatebox ID="DateBox" runat="server" ValidationGroup="ParamValue"  />
                                            <asp:TextBox ID="txaTextArea" runat="server" TextMode="MultiLine" Rows="8"  />
                                            <asp:CustomValidator ID="cvValidParamValue" runat="server" ControlToValidate="ParamName" ValidationGroup="ParamValue"
                                                 EnableClientScript="False" ErrorMessage="Default Value must be a valid string" ValidateEmptyText="true" 
                                                 OnServerValidate="IsDefaultValueValid" >*</asp:CustomValidator>
                                        </td>
                                    </tr>
                                </table>
                                <br />
                                <div id="divInfo" runat="server">
                                    <p class="info" id="info" runat="server">
                                        <asp:Localize ID="Localize5" runat="server" Text="<%$ Resources:LogiAdHoc, UseEnterKey %>"></asp:Localize></p>
                                </div>
                                <!-- Buttons -->
                                <table>
                                    <tr>
                                        <td colspan="2">
                                            <AdHoc:LogiButton ID="btnSaveParam" OnClick="SaveParam_OnClick" runat="server" ValidationGroup="ParamValue"
                                                Text="<%$ Resources:LogiAdHoc, OKWithSpaces %>" UseSubmitBehavior="false" />
                                            <AdHoc:LogiButton ID="btnCancelParam" OnClick="CancelParam_OnClick"
                                                runat="server" Text="<%$ Resources:LogiAdHoc, Cancel %>" CausesValidation="False"
                                                meta:resourcekey="btnCancelParamResource1" />
                                            <asp:ValidationSummary ID="vsummary" runat="server" meta:resourcekey="vsummaryResource1" ValidationGroup="ParamValue" />
                                        </td>
                                    </tr>
                                </table>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                        </div>
                    </asp:Panel>
                    &nbsp;
                </ContentTemplate>
                <Triggers>
                    <asp:AsyncPostBackTrigger ControlID="srch" EventName="DoSearch" />
                </Triggers>
            </asp:UpdatePanel>
            <div id="divSaveAnimation">
                <asp:Button runat="server" ID="btnSaveAnimation" Style="display: none" />
                <ajaxToolkit:ModalPopupExtender runat="server" ID="ahSavePopup" BehaviorID="ahSavePopupBehavior"
                    TargetControlID="btnSaveAnimation" PopupControlID="pnlSaving" BackgroundCssClass="modalBackground"
                    DropShadow="False">
                </ajaxToolkit:ModalPopupExtender>
                <asp:Panel id="pnlSaving" CssClass="savePopup" runat="server" style="display:none;">
                    <table><tr><td>
                    <asp:Image ID="imgSave" runat="server" SkinID="imgSaving" EnableViewState="false" AlternateText="<%$ Resources:LogiAdHoc, SaveImageAltText %>" />
                    </td>
                    <td valign="middle">
                    <asp:Label ID="lblSaveText" CssClass="lblSavePopup" runat="server" EnableViewState="false" Text="<%$ Resources:LogiAdHoc, PleaseWait %>"/>
                    </td></tr></table>
                </asp:Panel>
            </div>
        </td></tr></table>
</td></tr></table>
    </form>
</body>
</html>
