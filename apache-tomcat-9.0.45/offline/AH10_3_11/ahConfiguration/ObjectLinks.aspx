<%@ Page Language="vb" AutoEventWireup="false" Codebehind="ObjectLinks.aspx.vb" Inherits="LogiAdHoc.ahConfiguration_ObjectLinks" Culture="auto" meta:resourcekey="PageResource1" UICulture="auto" %>

<%@ Register TagPrefix="AdHoc" TagName="MainMenu" Src="~/ahControls/MainMenu.ascx" %>
<%@ Register TagPrefix="AdHoc" TagName="NavMenu" Src="~/ahControls/NavMenu.ascx" %>
<%@ Register TagPrefix="AdHoc" TagName="BreadCrumbTrail" Src="~/ahControls/BreadCrumbTrail.ascx" %>
<%@ Register TagPrefix="AdHoc" TagName="PagingControl" Src="~/ahControls/PagingControl.ascx" %>
<%@ Register TagPrefix="AdHoc" TagName="SelectReportControl" Src="~/ahControls/SelectReportControl.ascx" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Data Object Links</title>
    <link rel="shortcut icon" href="../ahImages/flav.ico" />
    <script language="JavaScript" type="text/javascript" src="../ahScripts/ahChange.js"></script>
    <script type="text/javascript">
        window.onbeforeunload = AlertOnExit;
    </script>
    <script language="javascript">
    <!--
    function PanelResizing()
    {
        var pnl_width = parseInt(document.getElementById('ucSelectReports_pnlTreeView').style.width)
        var pnl_height = parseInt(document.getElementById('ucSelectReports_pnlTreeView').style.height)
        document.getElementById('ucSelectReports_divTreeView').style.width = pnl_width + 'px';
        document.getElementById('ucSelectReports_divTreeView').style.height = pnl_height + 'px';
        document.getElementById('ucSelectReports_divListBox').style.width = (600 - pnl_width) + 'px';
        document.getElementById('ucSelectReports_divListBox').style.height = pnl_height + 'px';
        ScrollableListBoxRefineHeightAndWidth(document.getElementById('ucSelectReports_lstReports'), 200, 500);
    }
    function popupWin(){
        
        var lnk = document.getElementById("LinkURL")
        var sURL = lnk.value
        //Added a check for http:// as this is the default text and window.open was opening
        //the url in the same window instead of popup if the user clicks on Test URL without 
        //changing anything.
        if (sURL != null && sURL != '' && sURL!= 'http://')
            window.open(sURL); 
        else
            alert('Please enter a valid URL.')
    }
    -->
    </script>
</head>
<body id="bod">
    <AdHoc:MainMenu ID="menu" runat="server" />
    
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server">
        </asp:ScriptManager>
    <div id="submenu">
        <AdHoc:NavMenu ID="subnav" runat="server" />
    </div>

        <asp:UpdatePanel ID="UPBct" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
        <AdHoc:BreadCrumbTrail ID="bct" runat="server" Key="DOLinks" ParentLevels="1" />
            </ContentTemplate>
            <Triggers>
                <asp:AsyncPostBackTrigger ControlID="ddObjectID" EventName="SelectedIndexChanged" />
            </Triggers>
        </asp:UpdatePanel>
        
        <script type="text/javascript">
        Sys.WebForms.PageRequestManager.getInstance().add_beginRequest(BeginRequestHandler);
        Sys.WebForms.PageRequestManager.getInstance().add_endRequest(EndRequestHandler);
        function EndRequestHandler(sender, args) {
            //if (args.get_error() != undefined && args.get_error().httpStatusCode == '500') {
                //var errorMessage = args.get_error().message;
                //args.set_errorHandled(true);
            //}
            RestorePopupPosition('ahModalPopupBehavior');
        }
        function BeginRequestHandler(sender, args) {
            SavePopupPosition('ahModalPopupBehavior');
        }
        </script>

<table class="limiting"><tr><td>
    <table class="gridForm"><tr><td>
            <input type="hidden" id="ahDirty" name="ahDirty" runat="server" />
            <input type="hidden" id="ahParentID" name="ahParentID" runat="server" />
            
            <table id="tbDBConn" runat="server" class="tbTB">
                <tr>
                    <td width="130px">
                        <asp:Localize ID="Localize8" runat="server" Text="<%$ Resources:LogiAdHoc, DatabaseConnection %>"></asp:Localize>
                    </td>
                    <td>
                        <asp:Label ID="lblDBConn" runat="server"></asp:Label>
                    </td>
                </tr>
            </table>
            <table>
                <tr>
                    <td width="130px">
                        <asp:Localize ID="Localize11" runat="server" Text="<%$ Resources:LogiAdHoc, SelectedDataObject %>"></asp:Localize></td>
                    <td>
                        <asp:DropDownList ID="ddObjectID" AutoPostBack="True" runat="server" meta:resourcekey="ddObjectIDResource1" />
                    </td>
                </tr>
            </table>
            <br />
            <asp:UpdatePanel ID="UP1" UpdateMode="Conditional" runat="server">
                <ContentTemplate>
                    <div id="data_main">
                    <asp:GridView ID="grdMain" runat="server" CssClass="grid" DataKeyNames="ColumnID"
                        OnRowDataBound="OnItemDataBoundHandler" AutoGenerateColumns="False" AllowPaging="True" meta:resourcekey="grdMainResource1">
                        <Columns>
                            <asp:TemplateField HeaderText="Column Name" meta:resourcekey="TemplateFieldResource1">
                                <ItemTemplate>
                                    <input type="hidden" id="ColumnID" runat="server" />
                                    <input type="hidden" id="hLinkURL" runat="server" />
                                    <asp:Label ID="ColumnName" runat="server" meta:resourcekey="ColumnNameResource1" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField HeaderText="Label" DataField="Description" meta:resourcekey="BoundFieldResource1" />
                            <asp:TemplateField HeaderText="Link" meta:resourcekey="TemplateFieldResource2">
                                <ItemTemplate>
                                    <asp:Label ID="ColumnLink" runat="server" meta:resourcekey="ColumnLinkResource1"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="<%$ Resources:LogiAdHoc, Actions %>" meta:resourcekey="TemplateFieldResource3">
                                <HeaderStyle Width="50px"></HeaderStyle>
                                <ItemTemplate>
                                    <asp:Image ID="imgActions" AlternateText="<%$ Resources:LogiAdHoc, Actions %>" runat="server" 
                                        ToolTip="<%$ Resources:LogiAdHoc, Actions %>" ImageUrl="~/ahImages/arrowStep.gif" SkinID="imgActions" />
                                    <asp:Panel ID="pnlActionsMenu" runat="server" CssClass="popupMenu" 
                                        HorizontalAlign="Left" Wrap="false" style="display:none;">
                                        <div id="divLink" runat="server" class="hoverMenuActionLink" >
                                            <asp:LinkButton ID="lnkLink" runat="server" OnCommand="ManageLink" Text="Add Link"
                                                CausesValidation="false" meta:resourcekey="LinkResource2" ></asp:LinkButton>
                                        </div>
                                        <div id="divRemovelink" runat="server" class="hoverMenuActionLink" >
                                            <asp:LinkButton ID="lnkRemovelink" runat="server" OnCommand="ManageLink" Text="Remove Link"
                                                CausesValidation="false" meta:resourcekey="RemovelinkResource1" ></asp:LinkButton>
                                        </div>
 				                    </asp:Panel>
                                    <ajaxToolkit:HoverMenuExtender ID="hme2" runat="server"
                                        PopupControlID="pnlActionsMenu" PopupPosition="right" 
                                        TargetControlID="imgActions" PopDelay="25" />
                                        
                                </ItemTemplate><ItemStyle HorizontalAlign="Center"></ItemStyle>
                            </asp:TemplateField>
                        </Columns>
                        <PagerTemplate>
                            <AdHoc:PagingControl ID="pageCtrl" runat="server" OnGotoNextPage="ChangePageIndex" />
                        </PagerTemplate>
                        <PagerStyle HorizontalAlign="Center" />
                        <HeaderStyle CssClass="gridheader" />
                        <RowStyle CssClass="gridrow" />
                        <AlternatingRowStyle CssClass="gridalternaterow" />
                    </asp:GridView>
                    </div>
                    <asp:Button runat="server" ID="Button1" style="display:none"/>
                    <ajaxToolkit:ModalPopupExtender runat="server" ID="ahModalPopup" BehaviorID="ahModalPopupBehavior"
                        TargetControlID="Button1" PopupControlID="ahPopup" BackgroundCssClass="modalBackground"
                        DropShadow="false" PopupDragHandleControlID="pnlDragHandle" RepositionMode="None">
                    </ajaxToolkit:ModalPopupExtender>
                    
                    <asp:Panel runat="server" CssClass="modalPopup" ID="ahPopup" style="display:none;width:800;">
                        <asp:Panel ID="pnlDragHandle" runat="server" Style="cursor: hand;">
                        <div class="modalPopupHandle">
                            <table cellpadding="0" cellspacing="0" style="width: 100%"><tr><td>
                                    <asp:Localize ID="PopupHeader" runat="server"></asp:Localize>
                                </td>
                                <td style="width: 20px;">
                                    <asp:ImageButton ID="imgClosePopup" runat="server" 
                                        OnClick="imgClosePopup_Click" CausesValidation="false"
                                        SkinID="imgbClose" ImageUrl="../ahImages/remove.gif" 
                                        AlternateText="<%$ Resources:LogiAdHoc, CloseWindow %>" />
                            </td></tr></table>
                        </div>
                        </asp:Panel>
                        <div class="modalDiv">
                        <asp:UpdatePanel ID="upPopup" runat="server" >
                            <ContentTemplate>
                                <%--<asp:Panel ID="pnlLink" runat="server" CssClass="detailpanel" meta:resourcekey="pnlLinkResource1">--%>
                                <!-- Link Type: 1- To any URL or 2- To an Ad Hoc report -->
                                <asp:RadioButton ID="LinkGeneral" runat="server" AutoPostBack="True" Text="Link to Any URL"
                                    OnCheckedChanged="LinkingMethod_Changed" Checked="True" GroupName="LinkingMethod" meta:resourcekey="LinkGeneralResource1">
                                </asp:RadioButton>
                                <asp:RadioButton ID="LinkAdHoc" runat="server" AutoPostBack="True" Text="Link to an Ad Hoc Report"
                                    OnCheckedChanged="LinkingMethod_Changed" GroupName="LinkingMethod" meta:resourcekey="LinkAdHocResource1"></asp:RadioButton>
                                <input id="txtProxy" class="hidden" type="text" size="3" value="0"
                                    runat="server" />
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtProxy"
                                    ErrorMessage="<%$ Resources:Errors, Err_RequiredLinkURL %>" ValidationGroup="Link">*</asp:RequiredFieldValidator>
                                <asp:CustomValidator ID="cvPageValidator" runat="server" ControlToValidate="txtProxy"
                                    EnableClientScript="False" ErrorMessage="Data is missing." meta:resourcekey="cvPageValidatorResource1"
                                    OnServerValidate="IsDataValid" ValidationGroup="Link">*</asp:CustomValidator>
                                <input id="LinkReportID" type="hidden" runat="server" />
                                <table>
                                    <tbody>
                                        <tr id="trGeneral" runat="server">
                                            <td runat="server">
                                                <asp:Localize ID="Localize2" runat="server" meta:resourcekey="LiteralResource1" Text="Link URL:"></asp:Localize></td>
                                            <td runat="server">
                                                <table>
                                                    <tr>
                                                        <td>
                                                            <input id="LinkURL" type="text" maxlength="255" style="width:400px;" runat="server" />
                                                            <asp:RequiredFieldValidator ID="rtvLinkURL" runat="server" ControlToValidate="LinkURL"
                                                                ErrorMessage="<%$ Resources:Errors, Err_RequiredLinkURL %>" ValidationGroup="URL">*</asp:RequiredFieldValidator>
                                                            <asp:RegularExpressionValidator ID="revLinkURL" runat="server" ControlToValidate="LinkURL"
                                                                ErrorMessage="<%$ Resources:Errors, Err_InvalidURL %>" ValidationExpression="https?://([\w-])+[\w-:\.]+(/[\w- ./?%&=]*)?"
                                                                ValidationGroup="URL">*</asp:RegularExpressionValidator>
                                                        </td>
                                                        <td>
                                                            <AdHoc:LogiButton ID="btnTestURL" Text="<%$ Resources:LogiAdHoc, TestURL %>" 
                                                        CausesValidation="false" runat="server" OnClientClick="popupWin()" UseSubmitBehavior="false" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr id="trAdHoc" runat="server">
                                            <td runat="server" valign="top">
                                                <asp:Localize ID="Localize3" runat="server" meta:resourcekey="LiteralResource2" Text="Linked Report:"></asp:Localize></td>
                                            <td runat="server">
                                                <asp:Label ID="LinkReport" runat="server" CssClass="inactiveInput" Width="240px"></asp:Label>
                                                <asp:ImageButton ID="btnFindReport" runat="server" AlternateText="Find Report" CausesValidation="False"
                                                    ImageUrl="../ahImages/iconFind.gif" meta:resourcekey="btnFindReportResource1" OnClick="FindReport" ToolTip="Find Report" />
                                                <div id="divReports" runat="server">
                                                    <asp:Panel ID="pnlCondStyleDetails" CssClass="detailpanel" runat="server">
                                                        <table>
                                                            <tr>
                                                                <td>
			                                                        <AdHoc:SelectReportControl ID="ucSelectReports" runat="Server" />
                                                                </td>
                                                                <td>
                                                                    <AdHoc:LogiButton ID="btnFVOk" Width="50px" runat="server" Text="<%$ Resources:LogiAdHoc, OK %>" OnClick="btnFVOk_Click" CausesValidation="False" />
                                                                    <br />
                                                                    <AdHoc:LogiButton ID="btnFVCancel" Width="50px" runat="server" Text="<%$ Resources:LogiAdHoc, Cancel %>" OnClick="btnFVCancel_Click" CausesValidation="False" />
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </asp:Panel>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:Localize ID="Localize4" runat="server" meta:resourcekey="LiteralResource3" Text="Window ID:"></asp:Localize></td>
                                            <td>
                                                <%--<input id="FrameID" type="text" maxlength="25" size="25" runat="server" />--%>
                                                <asp:DropDownList ID="ddlFrameID" runat="server" >
                                                    <asp:ListItem Value="_blank" meta:resourcekey="ListItemResource18">_blank</asp:ListItem>
                                                    <asp:ListItem Value="_parent" meta:resourcekey="ListItemResource19">_parent</asp:ListItem>
                                                    <asp:ListItem Value="_self" meta:resourcekey="ListItemResource20">_self</asp:ListItem>
                                                    <asp:ListItem Value="_top" meta:resourcekey="ListItemResource21">_top</asp:ListItem>
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                                <div class="NoWrap">
                                    <asp:Localize ID="Localize5" runat="server" meta:resourcekey="LiteralResource4" Text="Link Parameters:"></asp:Localize>
                                </div>
                                <asp:GridView ID="grdSecondary" runat="server" CssClass="grid" OnRowDataBound="OnParamItemDataBoundHandler"
                                    AutoGenerateColumns="False" AllowPaging="True" meta:resourcekey="grdSecondaryResource1">
                                    <Columns>
                                        <asp:TemplateField HeaderText="Parameter Name" meta:resourcekey="TemplateFieldResource4">
                                            <ItemTemplate>
                                                <input type="hidden" id="ParameterID" runat="server" />
                                                
                                                <asp:TextBox ID="RowParamName" runat="server" Width="120px" meta:resourcekey="RowParamNameResource1" />
                                                <asp:ImageButton ID="btnFindRowParam" runat="server" AlternateText="Find Request Parameter"
                                                    CausesValidation="False" ImageUrl="../ahImages/iconFind.gif" meta:resourcekey="btnFindParamResource1"
                                                    OnClick="FindParameter" ToolTip="Find Request Parameter" />
                                                <asp:RequiredFieldValidator ID="rtvRowParamName" runat="server" ControlToValidate="RowParamName"
                                                    ErrorMessage="Parameter Name is required." meta:resourcekey="rtvParamNameResource1"
                                                    ValidationGroup="Link">*</asp:RequiredFieldValidator>
                                                <asp:RegularExpressionValidator ID="revRowParamName" runat="server" ControlToValidate="RowParamName"
                                                    ErrorMessage="This is not a valid parameter name." meta:resourcekey="revParamNameResource1"
                                                    ValidationExpression="(^[a-zA-Z][a-zA-Z0-9_ ]*)" ValidationGroup="Link">*</asp:RegularExpressionValidator>
                                                <asp:CustomValidator ID="cvRowParamName" runat="server" ControlToValidate="RowParamName"
                                                    ErrorMessage="This is not a valid parameter name." meta:resourcekey="cvParamNameResource1"
                                                     OnServerValidate="IsParameterNameValid" ValidationGroup="Link" >*</asp:CustomValidator>
                                                <div id="divRowParams" runat="server">
                                                    <br />
                                                    <asp:ListBox ID="lstRowParams" runat="server" Width="120px" AutoPostBack="True" OnSelectedIndexChanged="lstParams_SelectedIndexChanged" meta:resourcekey="lstRowParamsResource1">
                                                    </asp:ListBox>
                                                </div>
                                            </ItemTemplate>
                                            <%--<FooterTemplate>
                                                <asp:TextBox ID="ParamName" runat="server" Width="120px" meta:resourcekey="ParamNameResource1" />
                                                <asp:ImageButton ID="btnFindParam" runat="server" AlternateText="Find Request Parameter"
                                                    CausesValidation="False" ImageUrl="../ahImages/iconFind.gif" meta:resourcekey="btnFindParamResource1"
                                                    OnClick="FindParameter" ToolTip="Find Request Parameter" />
                                                <asp:RequiredFieldValidator ID="rtvParamName" runat="server" ControlToValidate="ParamName"
                                                    ErrorMessage="Parameter Name is required." meta:resourcekey="rtvParamNameResource1"
                                                    ValidationGroup="Link">*</asp:RequiredFieldValidator>
                                                <asp:RegularExpressionValidator ID="revParamName" runat="server" ControlToValidate="ParamName"
                                                    ErrorMessage="This is not a valid parameter name." meta:resourcekey="revParamNameResource1"
                                                    ValidationExpression="(^[a-zA-Z][a-zA-Z0-9_ ]*)" ValidationGroup="Link">*</asp:RegularExpressionValidator>
                                                <asp:CustomValidator ID="cvParamName" runat="server" ControlToValidate="ParamName"
                                                    ErrorMessage="This is not a valid parameter name." meta:resourcekey="cvParamNameResource1"
                                                     OnServerValidate="IsParameterNameValid" ValidationGroup="Link" >*</asp:CustomValidator>
                                                <div id="divParams" runat="server">
                                                    <br />
                                                    <asp:ListBox ID="lstParams" runat="server" Width="120px" AutoPostBack="True" OnSelectedIndexChanged="lstParams_SelectedIndexChanged" meta:resourcekey="lstParamsResource1">
                                                    </asp:ListBox>
                                                </div>
                                            </FooterTemplate>--%>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Parameter Source" meta:resourcekey="TemplateFieldResource5">
                                            <ItemTemplate>
                                                <asp:DropDownList ID="ddlRowParamSource" runat="server" meta:resourcekey="ddlRowParamSourceResource1" />
                                            </ItemTemplate>
                                            <%--<FooterTemplate>
                                                <asp:DropDownList ID="ddlParamSource" runat="server" meta:resourcekey="ddlParamSourceResource1" />
                                            </FooterTemplate>--%>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="<%$ Resources:LogiAdHoc, Actions %>" meta:resourcekey="TemplateFieldResource6">
                                            <HeaderStyle Width="50px"></HeaderStyle>
                                            <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                            <ItemTemplate>
                                                <asp:ImageButton ID="RemoveParam" runat="server" AlternateText="Remove Parameter"
                                                    CommandName="RemoveParam" ImageUrl="../ahImages/remove.gif" meta:resourcekey="RemoveParamResource1"
                                                    OnCommand="ParamModifications" ToolTip="Remove Parameter" />
                                            </ItemTemplate>
                                            <%--<FooterTemplate>
                                                <asp:ImageButton ID="SaveParam" runat="server" AlternateText="<%$ Resources:LogiAdHoc, SaveParameter %>"
                                                    CommandName="SaveParam" ImageUrl="../ahImages/save.gif" OnCommand="ParamModifications"
                                                    ToolTip="<%$ Resources:LogiAdHoc, SaveParameter %>" ValidationGroup="Link" />
                                                <asp:ImageButton ID="CancelParam" runat="server" AlternateText="<%$ Resources:LogiAdHoc, Cancel %>"
                                                    CausesValidation="False" CommandName="CancelParam" ImageUrl="../ahImages/cancel.gif"
                                                    OnCommand="ParamModifications" ToolTip="<%$ Resources:LogiAdHoc, Cancel %>" />
                                            </FooterTemplate>--%>
                                        </asp:TemplateField>
                                    </Columns>
                                    <PagerTemplate>
                                        <AdHoc:PagingControl ID="pageCtrl" runat="server" OnGotoNextPage="ChangeParamPageIndex" />
                                    </PagerTemplate>
                                    <PagerStyle HorizontalAlign="Center"></PagerStyle>
                                    <HeaderStyle CssClass="gridheader"></HeaderStyle>
                                    <%--<FooterStyle CssClass="gridfooter"></FooterStyle>--%>
                                    <RowStyle CssClass="gridrow" />
                                    <AlternatingRowStyle CssClass="gridalternaterow" />
                                </asp:GridView>
                                <br /><table><tr><td>
                                <AdHoc:LogiButton ID="AddParam" runat="server" meta:resourcekey="AddParamResource1"
                                    OnClick="AddParam_OnClick" Text="Add a Link Parameter" ValidationGroup="Link" />
                                </td></tr></table>
                                <asp:ValidationSummary ID="vsummary" runat="server" meta:resourcekey="vsummaryResource1"
                                    ValidationGroup="Link" />
                                <asp:ValidationSummary ID="ValidationSummary1" runat="server" ValidationGroup="URL" meta:resourcekey="vsummaryResource1" />
                                <ul runat="server" id="ErrorList" class="validation_error">
                                    <li>
                                        <asp:Label runat="server" EnableViewState="false" ID="lblValidationMessage"></asp:Label>
                                    </li>
                                </ul>
                                
                                <br />
                                <!-- Buttons -->
                                <table>
                                    <tbody>
                                        <tr>
                                            <td colspan="2">
                                                <AdHoc:LogiButton ID="btnSaveLink" runat="server" OnClick="SaveLink_OnClick"
                                                    Text="<%$ Resources:LogiAdHoc, OKWithSpaces %>" UseSubmitBehavior="false" ValidationGroup="Link" />
                                                <AdHoc:LogiButton ID="btnCancelLink" OnClick="CancelLink_OnClick" runat="server" CausesValidation="False"
                                                    Text="<%$ Resources:LogiAdHoc, Cancel %>" />
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                        </div>
                    </asp:Panel>
                    
                </ContentTemplate>
                <Triggers>
                    <asp:AsyncPostbackTrigger ControlID="ddObjectID" EventName="SelectedIndexChanged" />
                </Triggers>
            </asp:UpdatePanel>
            <div id="divButtons" class="divButtons">
                <AdHoc:LogiButton ID="btnCancel" runat="server" OnClick="Cancel_OnClick"
                    ToolTip="<%$ Resources:LogiAdHoc, BackToObjectsTooltip1 %>" Text="<%$ Resources:LogiAdHoc, BackToObjects %>" 
                    CausesValidation="False" />
            </div>
            <div id="divSaveAnimation">
                <asp:Button runat="server" ID="btnSaveAnimation" Style="display: none" />
                <ajaxToolkit:ModalPopupExtender runat="server" ID="ahSavePopup" BehaviorID="ahSavePopupBehavior"
                    TargetControlID="btnSaveAnimation" PopupControlID="pnlSaving" BackgroundCssClass="modalBackground"
                    DropShadow="False">
                </ajaxToolkit:ModalPopupExtender>
                <asp:Panel id="pnlSaving" CssClass="savePopup" runat="server" style="display:none;">
                    <table><tr><td>
                    <asp:Image ID="imgSave" runat="server" SkinID="imgSaving" EnableViewState="false" AlternateText="<%$ Resources:LogiAdHoc, SaveImageAltText %>" />
                    </td>
                    <td valign="middle">
                    <asp:Label ID="lblSaveText" CssClass="lblSavePopup" runat="server" EnableViewState="false" Text="<%$ Resources:LogiAdHoc, PleaseWait %>"/>
                    </td></tr></table>
                </asp:Panel>
            </div>
            <br />
        </td></tr></table>
</td></tr></table>
    </form>
</body>
</html>
