<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="User.aspx.vb" Inherits="LogiAdHoc.ahConfiguration_User" Culture="auto" meta:resourcekey="PageResource1" UICulture="auto" %>

<%@ Register TagPrefix="AdHoc" TagName="MainMenu" Src="~/ahControls/MainMenu.ascx" %>
<%@ Register TagPrefix="AdHoc" TagName="NavMenu" Src="~/ahControls/NavMenu.ascx" %>
<%@ Register TagPrefix="AdHoc" TagName="BreadCrumbTrail" Src="~/ahControls/BreadCrumbTrail.ascx" %>
<%@ Register TagPrefix="AdHoc" TagName="PasswordControl" Src="~/ahControls/PasswordControl.ascx" %>

<%--<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html xmlns="http://www.w3.org/1999/xhtml">--%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>User</title>
    <link rel="shortcut icon" href="../ahImages/flav.ico" />
    <script language="JavaScript" type="text/javascript" src="../ahScripts/ahChange.js"></script>
    <script type="text/javascript">
        window.onbeforeunload = AlertOnExit;
    </script>
</head>
<body id="bod">
    <AdHoc:MainMenu ID="menu" runat="server" />
    
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server"  />
    <div id="submenu">
        <AdHoc:NavMenu ID="subnav" runat="server" />
    </div>
        
        <asp:UpdatePanel ID="UPBct" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <AdHoc:BreadCrumbTrail ID="bct" runat="server" Key="User" ParentLevels="1" />
            </ContentTemplate>
            <Triggers>
                <asp:AsyncPostBackTrigger ControlID="ddlParentID" EventName="SelectedIndexChanged" />
            </Triggers>
        </asp:UpdatePanel>

        <script type="text/javascript">
        Sys.WebForms.PageRequestManager.getInstance().add_beginRequest(BeginRequestHandler);
        Sys.WebForms.PageRequestManager.getInstance().add_endRequest(EndRequestHandler);
        function EndRequestHandler(sender, args) {
            noMessage=false;
            $find('ahSavePopupBehavior').hide();   
            RestorePopupPosition('ahModalPopupBehavior');
            RestorePopupPosition('ahSaveAsPopupBehavior');     
        }
        function BeginRequestHandler(sender, args) {
            SavePopupPosition('ahModalPopupBehavior');
            SavePopupPosition('ahSaveAsPopupBehavior');
        }
        </script> 

        <div class="divForm">
            <input type="hidden" id="ahDirty" name="ahDirty" runat="server" />
            <input type="hidden" id="ahParentID" name="ahParentID" runat="server" />
            <asp:CheckBox ID="fake" runat="server" AutoPostBack="True" Style="display: none;" meta:resourcekey="fakeResource1" />
            
            <asp:UpdatePanel ID="UpdatePanel1" UpdateMode="Conditional" runat="Server">
                <ContentTemplate>
                    <table id="tbParent" runat="server">
                        <tr>
                            <td width="125px"><asp:Localize ID="Localize6" runat="server" meta:resourcekey="LiteralResource1" Text="Selected User:"></asp:Localize></td>
                            <td>
                                <asp:DropDownList ID="ddlParentID" AutoPostBack="True" runat="server" meta:resourcekey="ddlParentIDResource1" />
                            </td>
                        </tr>
                    </table>
                    <table style="height: 146px">
                        <tr>
                            <td width="125">
                                <asp:Localize ID="Localize1" runat="server" Text="<%$ Resources:LogiAdHoc, UserName %>"></asp:Localize>:
                            </td>
                            <td>
                                <input id="username" type="text" maxlength="50" size="20" runat="server">
                                <asp:RequiredFieldValidator ID="rtvUserName" runat="server" ErrorMessage="Username is required."
                                    ControlToValidate="username" ValidationGroup="User" meta:resourcekey="rtvUserNameResource1">*</asp:RequiredFieldValidator>
                                <asp:RegularExpressionValidator ID="revUserName" runat="server" ControlToValidate="username" ValidationExpression="(^[a-zA-Z0-9_@\-\. ']*)"
                                    ErrorMessage="Username cannot contain any special characters." ValidationGroup="User" meta:resourcekey="revUserNameResource1">*</asp:RegularExpressionValidator>
                                <asp:CustomValidator ID="cvValidName" runat="server" EnableClientScript="False" ErrorMessage="This name is not valid."
                                    ControlToValidate="username" ValidationGroup="User" OnServerValidate="IsNameValid" meta:resourcekey="cvValidNameResource1">*</asp:CustomValidator>
                            </td>
                            <td rowspan="5">
                                <asp:Button runat="server" ID="Button1" Style="display: none" />
                                <ajaxToolkit:ModalPopupExtender runat="server" ID="ahModalPopup" BehaviorID="ahModalPopupBehavior"
                                    TargetControlID="Button1" PopupControlID="ahPopup" BackgroundCssClass="modalBackground"
                                    DropShadow="False" PopupDragHandleControlID="pnlDragHandle" RepositionMode="None">
                                </ajaxToolkit:ModalPopupExtender>
                                <asp:Panel runat="server" CssClass="modalPopup" ID="ahPopup" Style="display: none; width: 280;">
                                    <asp:Panel ID="pnlDragHandle" runat="server" Style="cursor: hand;">
                                    <div class="modalPopupHandle">
                                        <table cellpadding="0" cellspacing="0" style="width: 100%"><tr><td>
                                                <asp:Localize ID="PopupHeader" runat="server" Text="Set Password" meta:resourcekey="PopupHeaderResource1"></asp:Localize>
                                            </td>
                                            <td style="width: 20px;">
                                                <asp:ImageButton ID="imgClosePopup" runat="server" 
                                                    OnClick="imgClosePopup_Click" CausesValidation="false"
                                                    SkinID="imgbClose" ImageUrl="../ahImages/remove.gif" 
                                                    AlternateText="<%$ Resources:LogiAdHoc, CloseWindow %>" />
                                        </td></tr></table>
                                    </div>
                                    </asp:Panel>
                                <div class="modalDiv">
                                    <asp:UpdatePanel ID="UP2" UpdateMode="Conditional" runat="server">
                                        <ContentTemplate>
                                            <AdHoc:PasswordControl ID="UserPassword" runat="server" />
                                        </ContentTemplate>
                                        <Triggers>
                                            <asp:AsyncPostBackTrigger ControlID="btnSetPassword" EventName="Click"/>
                                        </Triggers>
                                    </asp:UpdatePanel>
                                </div>
                                </asp:Panel>
                            </td>
                        </tr>
                        <tr id="rowPassword" runat="server">
                            <td id="Td1" runat="server">
                                <asp:Localize ID="Localize2" runat="server" Text="<%$ Resources:LogiAdHoc, Password %>"></asp:Localize>:
                            </td>
                            <td id="Td2" runat="server">
                                <AdHoc:LogiButton ID="btnSetPassword" OnClick="ShowPassword" Text="Set Password"
                                    CausesValidation="False" runat="server" meta:resourcekey="btnSetPasswordResource1"/>
                                <asp:CustomValidator ID="cvPassword" ValidationGroup="User" runat="server" EnableClientScript="False" ErrorMessage="You must enter a password for this user."
                                    OnServerValidate="IsPasswordEntered">*</asp:CustomValidator>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Localize ID="Localize3" runat="server" Text="<%$ Resources:LogiAdHoc, FirstName %>"></asp:Localize>:
                            </td>
                            <td>
                                <input id="firstname" type="text" maxlength="20" size="20" runat="server"></td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Localize ID="Localize4" runat="server" Text="<%$ Resources:LogiAdHoc, LastName %>"></asp:Localize>:
                            </td>
                            <td>
                                <input id="lastname" type="text" maxlength="20" size="20" runat="server"></td>
                        </tr>
                        <tr id="trEmail" runat="server">
                            <td>
                                <asp:Localize ID="Localize5" runat="server" Text="<%$ Resources:LogiAdHoc, EmailAddress %>"></asp:Localize>:
                            </td>
                            <td>
                                <input id="email" type="text" maxlength="50" size="50" runat="server">
                                <%--<asp:RegularExpressionValidator ID="eavEmail" ValidationGroup="User" runat="server" ControlToValidate="email"
                                    ErrorMessage="This is not a properly formatted email address." EnableClientScript="false" 
                                    ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" meta:resourcekey="eavEmailResource1">*</asp:RegularExpressionValidator>--%>
                                <asp:RegularExpressionValidator ID="eavEmail" ValidationGroup="User" runat="server" ControlToValidate="email"
                                    ErrorMessage="This is not a properly formatted email address." EnableClientScript="false" 
                                    ValidationExpression="((\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*)*((\s)*[;](\s)*)*)*" meta:resourcekey="eavEmailResource1">*</asp:RegularExpressionValidator>
                                <asp:CustomValidator ID="cvUserSubscribed" runat="server" ControlToValidate="email"
                                    ErrorMessage="The email address cannot be blank when the user is subscribed to one or more scheduled reports."
                                    OnServerValidate="IsEmailValid" ValidateEmptyText="true" ValidationGroup="User" meta:resourcekey="cvUserSubscribedResource1">*</asp:CustomValidator>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label for="Locked">
                                    <asp:Localize ID="Localize7" runat="server" meta:resourcekey="LiteralResource2" Text="Locked:"></asp:Localize></label></td>
                            <td>
                                <input id="Locked" type="checkbox" runat="server"></td>
                        </tr>
                        <tr id="trUserGroup" runat="server">
                            <td id="Td3" runat="server">
                                <label for="UserGroup">
                                    <asp:Localize ID="Localize8" runat="server" meta:resourcekey="LiteralResource3" Text="Organization:"></asp:Localize></label></td>
                            <td id="Td4" runat="server">
                                <asp:DropDownList ID="UserGroup" AutoPostBack="True" runat="server">
                                </asp:DropDownList></td>
                        </tr>
                    </table>
                    <h2><asp:Localize ID="Localize11" runat="server" meta:resourcekey="LiteralResource6" Text="User Roles"></asp:Localize></h2>
                    <div id="userroles">
                        <asp:UpdatePanel ID="UP1" UpdateMode="Conditional" runat="server">
                            <ContentTemplate>
                                <table id="ColumnMSL">
                                    <tr>
                                        <td colspan="2">
                                            <label id="lblAvailRoles" runat="server">
                                                <asp:Localize ID="Localize9" runat="server" meta:resourcekey="LiteralResource4" Text="Available Roles"></asp:Localize></label></td>
                                        <td colspan="2">
                                            <label id="lblUserRoles" runat="server">
                                                <asp:Localize ID="Localize10" runat="server" meta:resourcekey="LiteralResource5" Text="Assigned Roles"></asp:Localize></label>
                                            <asp:CustomValidator ID="cvRoleSelected" ControlToValidate="assigned" runat="server" ValidationGroup="User"
                                                EnableClientScript="False" ErrorMessage="You must select at least one role for this user."
                                                OnServerValidate="IsServerRoleAssigned" meta:resourcekey="cvRoleSelectedResource1">*</asp:CustomValidator>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <select id="available" class="MSL" multiple size="15" runat="server" ondblclick="document.getElementById('ahDirty').value=1; __doPostBack('moveright','');" />
                                        </td>
                                        <td>
                                            <asp:ImageButton CssClass="mslbutton" ID="moveright" OnClick="MoveRight_Click" ImageUrl="../ahImages/arrowRight.gif"
                                                CausesValidation="False" runat="server" meta:resourcekey="moverightResource1" />
                                            <asp:ImageButton CssClass="mslbutton" ID="moveleft" OnClick="MoveLeft_Click" ImageUrl="../ahImages/arrowLeft.gif"
                                                CausesValidation="False" runat="server" meta:resourcekey="moveleftResource1" />
                                        </td>
                                        <td>
                                            <select id="assigned" class="MSL" multiple size="15" runat="server" ondblclick="document.getElementById('ahDirty').value=1; __doPostBack('moveleft','');" />
                                        </td>
                                    </tr>
                                </table>
                            </ContentTemplate>
                            <Triggers>                            
                            <asp:AsyncPostBackTrigger ControlID="UserGroup" EventName="SelectedIndexChanged" />
                            </Triggers>
                        </asp:UpdatePanel>
                    </div>
                    <asp:ValidationSummary ID="vsummary" ValidationGroup="User" runat="server" meta:resourcekey="vsummaryResource1" />
                    
                    <div id="divButtons" class="divButtons">
                        <AdHoc:LogiButton ID="btnSave" runat="server" OnClick="SaveUser" ToolTip="<%$ Resources:LogiAdHoc, SaveTooltip1 %>"
                            Text="<%$ Resources:LogiAdHoc, Save %>" UseSubmitBehavior="false" ValidationGroup="User" />
                        <AdHoc:LogiButton ID="btnSaveAs" runat="server" OnClick="SaveUserAs" ValidationGroup="User"
                            ToolTip="<%$ Resources:LogiAdHoc, SaveTooltip1 %>" Text="<%$ Resources:LogiAdHoc, SaveAs %>" />
                        <AdHoc:LogiButton ID="btnCancel" runat="server" OnClick="CancelUser" ToolTip="Click to cancel your unsaved changes and return to the report list."
                            Text="Back to Users" CausesValidation="False" meta:resourcekey="btnCancelResource1" />
                            
                        <asp:Button runat="server" ID="Button2" style="display:none"/>
                        <ajaxToolkit:ModalPopupExtender runat="server" ID="ahSaveAsPopup" BehaviorID="ahSaveAsPopupBehavior"
                            TargetControlID="Button2" PopupControlID="pnlSaveAsPopup" BackgroundCssClass="modalBackground"
                            DropShadow="False" PopupDragHandleControlID="pnlDragHandle2" RepositionMode="None">
                        </ajaxToolkit:ModalPopupExtender>
                        
                        <asp:Panel runat="server" CssClass="modalPopup" ID="pnlSaveAsPopup" style="display:none;width:430;">
                            <asp:Panel ID="pnlDragHandle2" runat="server" Style="cursor: hand;">
                            <div class="modalPopupHandle">
                                <table cellpadding="0" cellspacing="0" style="width: 100%"><tr><td>
                                        <asp:Localize ID="Localize12" runat="server" Text="Save As New User" meta:resourcekey="LiteralResource7" ></asp:Localize>
                                    </td>
                                    <td style="width: 20px;">
                                        <asp:ImageButton ID="imgClosePopup2" runat="server" 
                                            OnClick="imgClosePopup_Click" CausesValidation="false"
                                            SkinID="imgbClose" ImageUrl="../ahImages/remove.gif" 
                                            AlternateText="<%$ Resources:LogiAdHoc, CloseWindow %>" />
                                </td></tr></table>
                            </div>
                            </asp:Panel>
                            <div class="modalDiv">
                            <asp:UpdatePanel ID="upPopup" runat="server" >
                                <ContentTemplate>
                                    <%--<asp:Panel ID="pnlSaveAs" runat="server" CssClass="detailpanel">--%>
                                    <table>
                                        <tr>
                                            <td>
                                                <asp:Localize ID="Localize13" runat="server" Text="<%$ Resources:LogiAdHoc, UserName %>"></asp:Localize>:
                                            </td>
                                            <td>
                                                <input id="txtUsername" type="text" maxlength="50" size="20" runat="server" />
                                                <asp:RequiredFieldValidator ID="rfvSaveAsUsername" runat="server" ErrorMessage="Username is required."
                                                    ControlToValidate="txtUsername" ValidationGroup="SaveAs" meta:resourcekey="rtvUserNameResource1">*</asp:RequiredFieldValidator>
                                                <asp:RegularExpressionValidator ID="revSaveAsUsername" runat="server" ControlToValidate="txtUsername" ValidationExpression="(^[a-zA-Z0-9_@\-\. ']*)"
                                                    ErrorMessage="Username cannot contain any special characters." ValidationGroup="SaveAs" meta:resourcekey="revUserNameResource1">*</asp:RegularExpressionValidator>
                                                <asp:CustomValidator ID="cvSaveAsUsername" runat="server" EnableClientScript="False" ErrorMessage="This name is not valid."
                                                    ControlToValidate="txtUsername" ValidationGroup="SaveAs" OnServerValidate="IsSaveAsNameValid" meta:resourcekey="cvValidNameResource1">*</asp:CustomValidator>
                                            </td>
                                        </tr>
                                    </table>
                            <br />
                            <table>
                                <tr>
                                    <td>
                                        <AdHoc:LogiButton ID="btnSPA_OK" runat="server" OnClick="OK_SPA" ValidationGroup="SaveAs"
                                        ToolTip="<%$ Resources:LogiAdHoc, SaveTooltip1 %>" Text="<%$ Resources:LogiAdHoc, OKWithSpaces %>" UseSubmitBehavior="false" />
                                        <AdHoc:LogiButton ID="btnSPA_Cancel" runat="server" OnClick="Cancel_SPA"
                                        ToolTip="<%$ Resources:LogiAdHoc, CancelTooltip1 %>" Text="<%$ Resources:LogiAdHoc, Cancel %>" CausesValidation="false" />
                                    </td>
                                </tr>
                            </table>
                            <asp:ValidationSummary ID="ValidationSummary2" ValidationGroup="SaveAs" runat="server" meta:resourcekey="vsummaryResource1" />
                            </ContentTemplate>
                                </asp:UpdatePanel>
                            </div>
                        </asp:Panel>
                        
                        
                    </div>
<br />
                    <div id="divSaveAnimation">
                        <asp:Button runat="server" ID="btnSaveAnimation" Style="display: none" />
                        <ajaxToolkit:ModalPopupExtender runat="server" ID="ahSavePopup" BehaviorID="ahSavePopupBehavior"
                            TargetControlID="btnSaveAnimation" PopupControlID="pnlSaving" BackgroundCssClass="modalBackground"
                            DropShadow="False">
                        </ajaxToolkit:ModalPopupExtender>
                        <asp:Panel id="pnlSaving" CssClass="savePopup" runat="server" style="display:none;">
                            <table><tr><td>
                            <asp:Image ID="imgSave" runat="server" SkinID="imgSaving" EnableViewState="false" AlternateText="<%$ Resources:LogiAdHoc, SaveImageAltText %>" />
                            </td>
                            <td valign="middle">
                            <asp:Label ID="lblSaveText" CssClass="lblSavePopup" runat="server" EnableViewState="false" Text="<%$ Resources:LogiAdHoc, PleaseWait %>"/>
                            </td></tr></table>
                        </asp:Panel>
                    </div>
                </ContentTemplate>
                <Triggers>
                    <asp:AsyncPostBackTrigger ControlID="ddlParentID" EventName="SelectedIndexChanged" />
                    <asp:PostBackTrigger ControlID="btnCancel" />
                </Triggers>
            </asp:UpdatePanel>
        </div>
    </form>
</body>
</html>
