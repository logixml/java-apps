<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="Dependencies.aspx.vb" Inherits="LogiAdHoc.Dependencies" %>

<%@ Register TagPrefix="AdHoc" TagName="MainMenu" Src="~/ahControls/MainMenu.ascx" %>
<%@ Register TagPrefix="AdHoc" TagName="NavMenu" Src="~/ahControls/NavMenu.ascx" %>
<%@ Register TagPrefix="AdHoc" TagName="BreadCrumbTrail" Src="~/ahControls/BreadCrumbTrail.ascx" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Dependencies</title>
    <link rel="shortcut icon" href="../ahImages/flav.ico" />
    <script language="JavaScript" type="text/javascript" src="../ahScripts/ahChange.js"></script>
    
    <script type="text/javascript">
    <!--
    function ExpandCollapse(divID) {
        var d = document.getElementById("divDependency" + divID);
        var i = document.getElementById("imgExpand" + divID);
        if (d.style.display=="none") {
            d.style.display="";
            i.src = "../ahImages/collapse_blue.jpg";
        } else {
            d.style.display="none";
            i.src = "../ahImages/expand_blue.jpg";
        }
    }
    -->
    </script>
</head>
<body>
    <AdHoc:MainMenu ID="menu" runat="server" />
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server">
        </asp:ScriptManager>
    <div id="submenu">
        <AdHoc:NavMenu ID="subnav" runat="server" />
    </div>
        
        <%-- Commenting Update panels as there are refresh issues on dynamic controls being added on partial Postback 
            as they were already added during the initial request. So we do a response redirect instead of postback. %> 
        <%--<asp:UpdatePanel ID="UPBct" runat="server" UpdateMode="Conditional">
            <ContentTemplate>--%>
        <AdHoc:BreadCrumbTrail ID="bct" runat="server" Key="Dependencies" />
            <%--</ContentTemplate>
            <Triggers>
                <asp:AsyncPostBackTrigger ControlID="ddParentID" EventName="SelectedIndexChanged" />
            </Triggers>
        </asp:UpdatePanel>--%>
        
        
        <div class="divForm">
        
            <table>
                <tr>
                    <td>
                        <asp:Label ID="lblDDParentID" runat="server" Text="Selected Data Object:"
                             AssociatedControlID="ddParentID"></asp:Label>
                    </td>
                    <td>
                        <asp:DropDownList ID="ddParentID" runat="server" AutoPostBack="True" >
                        </asp:DropDownList>
                    </td>
                </tr>
            </table>
            
            <%--<div id="divButtonsTop" class="divButtons">
                <AdHoc:LogiButton ID="btnCancelTop" runat="server" CausesValidation="False" CssClass="command"
                    OnClick="Cancel_OnClick" Text="<%$ Resources:LogiAdHoc, BackToObjects %>" ToolTip="<%$ Resources:LogiAdHoc, BackToObjectsTooltip %>" />
            </div>--%>
            <br />
            <%--<asp:UpdatePanel ID="UPMain" runat="server" UpdateMode="Conditional">
                <ContentTemplate>--%>
                    <div class="collapsePanelHeader3">
                        <table width="100%">
                            <tr>
                                <td>
                                    <img id="imgExpand1" onclick="ExpandCollapse('1');" src="../ahImages/collapse_blue.jpg" 
                                        title="<%$ Resources:LogiAdHoc, CollapseExpandPanel %>" alt="<%$ Resources:LogiAdHoc, CollapseExpand %>" runat="server" /> 
                                    <asp:Localize ID="Localize1" runat="server" />
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div id="divDependency1" runat="server" style="display: block;">
                    </div>
                    <div class="collapsePanelHeader3">
                        <table width="100%">
                            <tr>
                                <td>
                                    <img id="imgExpand2" onclick="ExpandCollapse('2');" src="../ahImages/collapse_blue.jpg"
                                        title="<%$ Resources:LogiAdHoc, CollapseExpandPanel %>" alt="<%$ Resources:LogiAdHoc, CollapseExpand %>" runat="server" /> 
                                    <asp:Localize ID="Localize2" runat="server" />
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div id="divDependency2" runat="server" style="display: block;">
                    </div>
                    <div class="collapsePanelHeader3">
                        <table width="100%">
                            <tr>
                                <td>
                                    <img id="imgExpand3" onclick="ExpandCollapse('3');" src="../ahImages/collapse_blue.jpg"
                                        title="<%$ Resources:LogiAdHoc, CollapseExpandPanel %>" alt="<%$ Resources:LogiAdHoc, CollapseExpand %>" runat="server" /> 
                                    <asp:Localize ID="Localize3" runat="server" />
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div id="divDependency3" runat="server" style="display: block;">
                    </div>
                    <div class="collapsePanelHeader3">
                        <table width="100%">
                            <tr>
                                <td>
                                    <img id="imgExpand4" onclick="ExpandCollapse('4');" src="../ahImages/collapse_blue.jpg"
                                        title="<%$ Resources:LogiAdHoc, CollapseExpandPanel %>" alt="<%$ Resources:LogiAdHoc, CollapseExpand %>" runat="server" /> 
                                    <asp:Localize ID="Localize4" runat="server" />
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div id="divDependency4" runat="server" style="display: block;">
                    </div>
                <%--</ContentTemplate>
                <Triggers>
                    <asp:AsyncPostBackTrigger ControlID="ddParentID" EventName="SelectedIndexChanged" />
                </Triggers>
            </asp:UpdatePanel>--%>
<div id="divButtons" class="divButtons">
    <AdHoc:LogiButton ID="btnCancel" runat="server" CausesValidation="False"
        OnClick="Cancel_OnClick" Text="<%$ Resources:LogiAdHoc, BackToObjects %>" 
        ToolTip="<%$ Resources:LogiAdHoc, BackToObjectsTooltip %>" />
</div>
<div id="divSaveAnimation">
    <asp:Button runat="server" ID="btnSaveAnimation" Style="display: none" />
    <ajaxToolkit:ModalPopupExtender runat="server" ID="ahSavePopup" BehaviorID="ahSavePopupBehavior"
        TargetControlID="btnSaveAnimation" PopupControlID="pnlSaving" BackgroundCssClass="modalBackground"
        DropShadow="False">
    </ajaxToolkit:ModalPopupExtender>
    <asp:Panel id="pnlSaving" CssClass="savePopup" runat="server" style="display:none;">
        <table><tr><td>
        <asp:Image ID="imgSave" runat="server" SkinID="imgSaving" EnableViewState="false" AlternateText="<%$ Resources:LogiAdHoc, SaveImageAltText %>" />
        </td>
        <td valign="middle">
        <asp:Label ID="lblSaveText" CssClass="lblSavePopup" runat="server" EnableViewState="false" Text="<%$ Resources:LogiAdHoc, PleaseWait %>"/>
        </td></tr></table>
    </asp:Panel>
</div>
<br />
            
        </div>
    </form>
</body>
</html>
