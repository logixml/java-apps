<%@ Page Language="vb" AutoEventWireup="false" Codebehind="Categories.aspx.vb" Inherits="LogiAdHoc.ahConfiguration_Categories" Culture="auto" meta:resourcekey="PageResource1" UICulture="auto" %>

<%@ Register TagPrefix="AdHoc" TagName="MainMenu" Src="~/ahControls/MainMenu.ascx" %>
<%@ Register TagPrefix="AdHoc" TagName="NavMenu" Src="~/ahControls/NavMenu.ascx" %>
<%@ Register TagPrefix="AdHoc" TagName="BreadCrumbTrail" Src="~/ahControls/BreadCrumbTrail.ascx" %>
<%@ Register TagPrefix="AdHoc" TagName="Search" Src="~/ahControls/Search.ascx" %>
<%@ Register TagPrefix="AdHoc" TagName="PagingControl" Src="../ahControls/PagingControl.ascx" %>
<%@ Register TagPrefix="AdHoc" TagName="DatabaseControl" Src="~/ahControls/DatabaseControl.ascx" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Categories</title>
    <link rel="shortcut icon" href="../ahImages/flav.ico" />

    <script language="JavaScript" type="text/javascript" src="../ahScripts/ahChange.js"></script>

    <script type="text/javascript">
        window.onbeforeunload = AlertOnExit;
    </script>

</head>
<body id="bod">
    <AdHoc:MainMenu ID="menu" runat="server" />
    <form id="form1" runat="server">
    <div id="submenu">
        <AdHoc:NavMenu ID="subnav" runat="server" />
    </div>
        <AdHoc:BreadCrumbTrail ID="bct" runat="server" Key="Categories" ParentLevels="0" />
        <asp:ScriptManager ID="ScriptManager1" runat="server" />
        
        <script type="text/javascript">
            Sys.WebForms.PageRequestManager.getInstance().add_beginRequest(BeginRequestHandler);
            Sys.WebForms.PageRequestManager.getInstance().add_endRequest(EndRequestHandler);
            function EndRequestHandler(sender, args) {
                $find('ahSavePopupBehavior').hide();
                RestorePopupPosition('ahModalPopupBehavior');
            }
            function BeginRequestHandler(sender, args) {
                SavePopupPosition('ahModalPopupBehavior');
            }
        </script>
        
<table class="limiting"><tr><td>
        <table class="gridForm"><tr><td>
            <input type="hidden" id="ahDirty" name="ahDirty" runat="server" />
                
            <asp:UpdatePanel ID="UP1" UpdateMode="Conditional" runat="server" RenderMode="Inline">
                <ContentTemplate>
                
                <Adhoc:DatabaseControl ID="ucDatabaseControl" runat="server" ShowAllDatabasesOption="False" />
                <br /><br />
                <br />
                
                <div id="data_main">
                    <div id="activities">
                        <table width="100%" cellpadding="0" cellspacing="0">
                        <tr width="100%">
                        <td align="left" valign="top">
                        <AdHoc:LogiButton ID="btnAddRow" OnClick="AddRow_OnClick" Text="<%$ Resources:LogiAdHoc, AddWithSpaces %>"
                            runat="server" CausesValidation="false" />
                        <AdHoc:LogiButton ID="btnRemoveRow2" OnClick="RemoveRow_OnClick" Text="<%$ Resources:LogiAdHoc, Delete %>"
                            runat="server" CausesValidation="false" />
                        </td>
                        <td align="right" valign="top">
                            <AdHoc:Search ID="srch" runat="server" Title="Find Categories" meta:resourcekey="AdHocSearch" />
                        </td>
                        </tr>
                        </table>
                    </div>
                    <asp:GridView ID="grdMain" runat="server" AllowPaging="True" AllowSorting="True"
                        AutoGenerateColumns="False" CssClass="grid" DataKeyNames="CategoryID"
                        meta:resourcekey="grdMainResource1" OnRowDataBound="OnItemDataBoundHandler" OnSorting="OnSortCommandHandler">
                        <HeaderStyle CssClass="gridheader"></HeaderStyle>
                        <PagerTemplate>
                            <AdHoc:PagingControl ID="pageCtrl" runat="server" OnGotoNextPage="ChangePageIndex" />
                        </PagerTemplate>
                        <PagerStyle HorizontalAlign="Center" />
                        <FooterStyle CssClass="gridfooter" />
                        <RowStyle CssClass="gridrow" />
                        <AlternatingRowStyle CssClass="gridalternaterow"></AlternatingRowStyle>
                        <Columns>
                            <asp:TemplateField>
                                <HeaderStyle Width="30px"></HeaderStyle>
                                <HeaderTemplate>
                                    <asp:Label ID="lblChkAll" runat="server" AssociatedControlID="CheckAll" Text="<%$ Resources:LogiAdHoc, SelectDeselectAll %>" CssClass="NoShow"></asp:Label>
                                    <asp:CheckBox runat="server" ToolTip="<%$ Resources:LogiAdHoc, SelectDeselectAll %>" ID="CheckAll">
                                    </asp:CheckBox>
                                </HeaderTemplate>
                                <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblChk" runat="server" AssociatedControlID="chk_Select" Text="<%$ Resources:LogiAdHoc, SelectDeselect %>" CssClass="NoShow"></asp:Label>
                                    <asp:CheckBox runat="server" ID="chk_Select" ToolTip="<%$ Resources:LogiAdHoc, SelectDeselect %>">
                                    </asp:CheckBox>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField DataField="CategoryName" HeaderText="Category Name" meta:resourcekey="BoundFieldResource1" SortExpression="CategoryName">
                            </asp:BoundField>
                            <asp:BoundField DataField="Description" HeaderText="Description" meta:resourcekey="BoundFieldResource2">
                            </asp:BoundField>
                            <asp:TemplateField HeaderText="<%$ Resources:LogiAdHoc, Actions %>" meta:resourcekey="TemplateFieldResource2">
                                <HeaderStyle Width="50px" />
                                <ItemStyle HorizontalAlign="Center" />
                                <ItemTemplate>
                                    <%--<asp:ImageButton ID="imgModify" SkinID="imgSingleAction" AlternateText="Modify Category" CausesValidation="False"
                                        ToolTip="Modify Category" runat="server" OnCommand="EditItem" meta:resourcekey="EditItemResource1" />--%>
                                    
                                    <asp:Image ID="imgActions" AlternateText="<%$ Resources:LogiAdHoc, Actions %>" runat="server" 
                                        ToolTip="<%$ Resources:LogiAdHoc, Actions %>" ImageUrl="~/ahImages/arrowStep.gif" SkinID="imgActions" />
                                    <asp:Panel ID="pnlActionsMenu" runat="server" CssClass="popupMenu" 
                                        HorizontalAlign="Left" Wrap="false" style="display:none;">
                                        <div id="divModify" runat="server" class="hoverMenuActionLink" >
                                            <asp:LinkButton ID="lnkModify" runat="server" OnCommand="EditItem" Text="Modify Category"
                                                CausesValidation="False" meta:resourcekey="EditItemResource1"></asp:LinkButton>
                                        </div>
                                        <div id="divDelete" runat="server" class="hoverMenuActionLink">
                                            <asp:LinkButton ID="lnkDelete" runat="server" OnCommand="RemoveItem"
                                                Text="Delete Category" CausesValidation="False" meta:resourcekey="DeleteResource1"></asp:LinkButton>
                                        </div>
 				                    </asp:Panel>
                                    <ajaxToolkit:HoverMenuExtender ID="hme2" runat="server"
                                        PopupControlID="pnlActionsMenu" PopupPosition="right" 
                                        TargetControlID="imgActions" PopDelay="25" />
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                </div>
                    <asp:Button runat="server" ID="Button1" Style="display: none" />
                    <ajaxToolkit:ModalPopupExtender runat="server" ID="ahModalPopup" BehaviorID="ahModalPopupBehavior"
                        TargetControlID="Button1" PopupControlID="ahPopup" BackgroundCssClass="modalBackground"
                        DropShadow="false" PopupDragHandleControlID="pnlDragHandle" RepositionMode="None">
                    </ajaxToolkit:ModalPopupExtender>
                    
                    <asp:Panel runat="server" CssClass="modalPopup" ID="ahPopup" Style="display: none; width: 656;">
                        <asp:Panel ID="pnlDragHandle" runat="server" Style="cursor: hand;">
                            <div class="modalPopupHandle" style="width: 650;">
                                 <table cellpadding="0" cellspacing="0" style="width: 100%"><tr><td>
                                        <asp:Localize ID="PopupHeader" runat="server" meta:resourcekey="LiteralResource2" Text="Category Details"></asp:Localize>
                                    </td>
                                    <td style="width: 20px;">
                                        <asp:ImageButton ID="imgClosePopup" runat="server" 
                                            OnClick="imgClosePopup_Click" CausesValidation="false"
                                            SkinID="imgbClose" ImageUrl="../ahImages/remove.gif" 
                                            AlternateText="<%$ Resources:LogiAdHoc, CloseWindow %>" />
                                </td></tr></table>
                            </div>
                        </asp:Panel>
                        <div class="modalDiv">
                        <asp:UpdatePanel ID="upPopup" runat="server">
                            <ContentTemplate>
                                <input type="hidden" id="NewCategoryID" runat="server" />
                                <table cellspacing="0" cellpadding="1" border="0" class="tbTB">
                                    <tr>
                                        <td width="100px">
                                            <asp:Localize ID="Localize3" runat="server" meta:resourcekey="LiteralResource3" Text="Category Name:"></asp:Localize>
                                        </td>
                                        <td>
                                            <asp:TextBox ID="CategoryName" MaxLength="100" Width="200px" runat="server" meta:resourcekey="CategoryNameResource1" />
                                            <asp:RequiredFieldValidator ID="rtvCategoryName" runat="server" ErrorMessage="Category Name is required."
                                                ControlToValidate="CategoryName" meta:resourcekey="rtvCategoryNameResource1">*</asp:RequiredFieldValidator>
                                            <asp:CustomValidator ID="cvValidName" runat="server" ErrorMessage="This name already exists."
                                                ControlToValidate="CategoryName" OnServerValidate="IsNameValid" meta:resourcekey="cvValidNameResource1">*</asp:CustomValidator>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="100px">
                                            <asp:Localize ID="Localize4" runat="server" meta:resourcekey="LiteralResource4" Text="Description:"></asp:Localize>
                                        </td>
                                        <td>
                                            <asp:TextBox ID="CategoryDescription" MaxLength="255" Width="200px" 
                                            runat="server" Rows="3" TextMode="MultiLine" meta:resourcekey="DescriptionResource1" />
                                            <asp:CustomValidator ID="cvMaxLength" runat="server" ErrorMessage="<%$ Resources:Errors, Err_Description255 %>"
                                                ControlToValidate="CategoryDescription" OnServerValidate="ReachedMaxLength">*</asp:CustomValidator>
                                        </td>
                                    </tr>
                                </table>
                                <br />
                                <h2><asp:Localize ID="Localize11" runat="server" meta:resourcekey="LiteralResource6" Text="Objects:"></asp:Localize></h2>
                                <div id="objects">
                                    <asp:UpdatePanel ID="UpdatePanel1" UpdateMode="Conditional" runat="server">
                                        <ContentTemplate>
                                            <table id="ColumnMSL">
                                                <tr>
                                                    <td colspan="2">
                                                        <label id="lblAvailObjects" runat="server">
                                                            <asp:Localize ID="Localize9" runat="server" meta:resourcekey="LiteralResource7" Text="Available Objects"></asp:Localize></label></td>
                                                    <td colspan="2">
                                                        <label id="lblAssignedObjects" runat="server">
                                                            <asp:Localize ID="Localize10" runat="server" meta:resourcekey="LiteralResource8" Text="Assigned Objects"></asp:Localize></label>
                                                            <input type="text" id="fakeObjAssigned" runat="server" style="display: none;"/>
                                                            <asp:CustomValidator ID="cvObjectSelected" runat="server" ControlToValidate="fakeObjAssigned" EnableClientScript="False" 
                                                                ErrorMessage="You must select at least one object for this category." meta:resourcekey="cvObjectSelectedResource1" 
                                                                OnServerValidate="IsObjectAssigned" ValidateEmptyText="true" >*</asp:CustomValidator>
                                                        <%--<asp:CustomValidator ID="cvRoleSelected" ControlToValidate="assigned" runat="server" ValidationGroup="User"
                                                            EnableClientScript="False" ErrorMessage="You must select at least one role for this user."
                                                            OnServerValidate="IsServerRoleAssigned" meta:resourcekey="cvRoleSelectedResource1">*</asp:CustomValidator>--%>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <select id="available" class="MSL" multiple size="15" runat="server" ondblclick="__doPostBack('moveright','');" />
                                                    </td>
                                                    <td>
                                                        <asp:ImageButton CssClass="mslbutton" ID="moveright" OnClick="MoveRight_Click" ImageUrl="../ahImages/arrowRight.gif"
                                                            CausesValidation="False" runat="server" meta:resourcekey="moverightResource1" />
                                                        <asp:ImageButton CssClass="mslbutton" ID="moveleft" OnClick="MoveLeft_Click" ImageUrl="../ahImages/arrowLeft.gif"
                                                            CausesValidation="False" runat="server" meta:resourcekey="moveleftResource1" />
                                                    </td>
                                                    <td>
                                                        <select id="assigned" class="MSL" multiple size="15" runat="server" ondblclick="__doPostBack('moveleft','');" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </div>
<br />
                                <!-- Buttons -->
                                <table>
                                    <tr>
                                        <td colspan="2">
                                            <asp:Button ID="btnSave" runat="server" EnableViewState="false" 
                                                OnClick="SaveCategory" CssClass="NoShow" />
                                            <AdHoc:LogiButton ID="btnSaveCategory" OnClick="SaveCategory_OnClick" runat="server"
                                                Text="<%$ Resources:LogiAdHoc, OKWithSpaces %>" UseSubmitBehavior="false" />
                                            <AdHoc:LogiButton ID="btnCancelCategory" OnClick="CancelCategory_OnClick"
                                                runat="server" Text="<%$ Resources:LogiAdHoc, Cancel %>" CausesValidation="False"
                                                meta:resourcekey="btnCancelCategoryResource1" />
                                            <asp:ValidationSummary ID="vsummary" runat="server" meta:resourcekey="vsummaryResource1" />
                                        </td>
                                    </tr>
                                </table>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                        </div>
                    </asp:Panel>
                    &nbsp;
                </ContentTemplate>
                <Triggers>
                    <asp:AsyncPostBackTrigger ControlID="srch" EventName="DoSearch" />
                </Triggers>
            </asp:UpdatePanel>
            <div id="divSaveAnimation">
                <asp:Button runat="server" ID="btnSaveAnimation" Style="display: none" />
                <ajaxToolkit:ModalPopupExtender runat="server" ID="ahSavePopup" BehaviorID="ahSavePopupBehavior"
                    TargetControlID="btnSaveAnimation" PopupControlID="pnlSaving" BackgroundCssClass="modalBackground"
                    DropShadow="False">
                </ajaxToolkit:ModalPopupExtender>
                <asp:Panel id="pnlSaving" CssClass="savePopup" runat="server" style="display:none;">
                    <table><tr><td>
                    <asp:Image ID="imgSave" runat="server" SkinID="imgSaving" EnableViewState="false" AlternateText="<%$ Resources:LogiAdHoc, SaveImageAltText %>" />
                    </td>
                    <td valign="middle">
                    <asp:Label ID="lblSaveText" CssClass="lblSavePopup" runat="server" EnableViewState="false" Text="<%$ Resources:LogiAdHoc, PleaseWait %>"/>
                    </td></tr></table>
                </asp:Panel>
            </div>
        </td></tr></table>
</td></tr></table>
    </form>
</body>
</html>
