<%@ Page Language="vb" AutoEventWireup="false" Codebehind="Appearances.aspx.vb" Inherits="LogiAdHoc.ahConfiguration_Appearances" Culture="auto" meta:resourcekey="PageResource1" UICulture="auto" %>

<%@ Register TagPrefix="AdHoc" TagName="MainMenu" Src="~/ahControls/MainMenu.ascx" %>
<%@ Register TagPrefix="AdHoc" TagName="NavMenu" Src="~/ahControls/NavMenu.ascx" %>
<%@ Register TagPrefix="AdHoc" TagName="BreadCrumbTrail" Src="~/ahControls/BreadCrumbTrail.ascx" %>
<%@ Register TagPrefix="AdHoc" TagName="Search" Src="~/ahControls/Search.ascx" %>
<%@ Register TagPrefix="AdHoc" TagName="PagingControl" Src="~/ahControls/PagingControl.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Presentation Styles</title>
    <link rel="shortcut icon" href="../ahImages/flav.ico" />

    <script language="JavaScript" type="text/javascript" src="../ahScripts/ahChange.js"></script>

    <script type="text/javascript">
        window.onbeforeunload = AlertOnExit;
    </script>
</head>
<body>
    <AdHoc:MainMenu ID="menu" runat="server" />
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server" />
    <div id="submenu">
        <AdHoc:NavMenu ID="subnav" runat="server" />
    </div>
        <AdHoc:BreadCrumbTrail ID="bct" runat="server" Key="PresentationStyles" ParentLevels="0" />

        <script type="text/javascript">
            Sys.WebForms.PageRequestManager.getInstance().add_endRequest(EndRequestHandler);
            Sys.WebForms.PageRequestManager.getInstance().add_beginRequest(BeginRequestHandler);
            //var X=-1;
            //var Y=-1;
            function EndRequestHandler(sender, args) {
                $find('ahSavePopupBehavior').hide();
                //$find('ahModalPopupBehavior').set_X(X);
                //$find('ahModalPopupBehavior').set_Y(Y);
                RestorePopupPosition('ahModalPopupBehavior');
            }
            function BeginRequestHandler(sender, args) {
                SavePopupPosition('ahModalPopupBehavior');
//                if ($find('ahModalPopupBehavior')._foregroundElement.style.display == '') {
//                    X = $find('ahModalPopupBehavior')._foregroundElement.style.left.replace('px','');
//                    Y = $find('ahModalPopupBehavior')._foregroundElement.style.top.replace('px','');
//                }
//                else {
//                    X = -1;
//                    Y = -1;
//                }
            }
        </script>
        
<table class="limiting"><tr><td>
        <table class="gridForm"><tr><td>
            <input type="hidden" id="ahDirty" name="ahDirty" runat="server" />
            <asp:UpdatePanel ID="UP1" runat="server" UpdateMode="Conditional" RenderMode="Inline">
                <ContentTemplate>
                <div id="data_main">
                    <div id="activities">
                    <table width="100%" cellpadding="0" cellspacing="0">
                    <tr width="100%">
                    <td align="left" valign="top">
                    <AdHoc:LogiButton ID="btnNewAppearance2" OnClick="NewAppearance" CausesValidation="False"
                        Text="<%$ Resources:LogiAdHoc, AddWithSpaces %>" runat="server" />
                    <AdHoc:LogiButton ID="btnRemoveAppearance2" OnClick="RemoveAppearance"
                        CausesValidation="False" Text="<%$ Resources:LogiAdHoc, Delete %>" runat="server" />&nbsp;
                    </td>
                    <td align="right" valign="top">
                        <AdHoc:Search ID="srch" runat="server" Title="Find Styles" meta:resourcekey="AdHocSearch" />
                    </td>
                    </tr>
                    </table>
                    </div>
                    <asp:GridView ID="grdMain" runat="server" CssClass="grid" AutoGenerateColumns="False"
                        AllowPaging="True" DataKeyNames="ClassID" OnRowDataBound="OnItemDataBoundHandler"
                        OnSorting="OnSortCommandHandler" AllowSorting="True" meta:resourcekey="grdMainResource1">
                        <HeaderStyle CssClass="gridheader"></HeaderStyle>
                        <PagerTemplate>
                            <AdHoc:PagingControl ID="pageCtrl" runat="server" OnGotoNextPage="ChangePageIndex" />
                        </PagerTemplate>
                        <PagerStyle HorizontalAlign="Center"></PagerStyle>
                        <RowStyle CssClass="gridrow" />
                        <AlternatingRowStyle CssClass="gridalternaterow"></AlternatingRowStyle>
                        <Columns>
                            <asp:TemplateField>
                                <HeaderStyle Width="30px"></HeaderStyle>
                                <HeaderTemplate>
                                    <asp:Label ID="lblChkAll" runat="server" AssociatedControlID="CheckAll" Text="<%$ Resources:LogiAdHoc, SelectDeselectAll %>" CssClass="NoShow"></asp:Label>
                                    <asp:CheckBox runat="server" ToolTip="<%$ Resources:LogiAdHoc, SelectDeselectAll %>" ID="CheckAll">
                                    </asp:CheckBox>
                                </HeaderTemplate>
                                <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblChk" runat="server" AssociatedControlID="chk_Select" Text="<%$ Resources:LogiAdHoc, SelectDeselect %>" CssClass="NoShow"></asp:Label>
                                    <asp:CheckBox runat="server" ID="chk_Select" ToolTip="<%$ Resources:LogiAdHoc, SelectDeselect %>">
                                    </asp:CheckBox>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField DataField="Class" SortExpression="Class" HeaderText="Class Name" meta:resourcekey="BoundFieldResource1" />
                            <asp:BoundField DataField="FriendlyName" SortExpression="FriendlyName" HeaderText="Friendly Name" meta:resourcekey="BoundFieldResource2" />
                            <asp:TemplateField HeaderText="<%$ Resources:LogiAdHoc, Actions %>">
                                <HeaderStyle Width="50px" />
                                <ItemStyle HorizontalAlign="Center" />
                                <ItemTemplate>
                                    <asp:Image ID="imgActions" AlternateText="<%$ Resources:LogiAdHoc, Actions %>" runat="server" 
                                        ToolTip="<%$ Resources:LogiAdHoc, Actions %>" SkinID="imgActions" />
                                    <asp:Panel ID="pnlActionsMenu" runat="server" CssClass="popupMenu" 
                                        HorizontalAlign="Left" Wrap="false" style="display:none;">
                                        <div id="divModify" runat="server" class="hoverMenuActionLink" >
                                            <asp:LinkButton ID="lnkModify" runat="server" OnCommand="ModifyAppearance" Text="Modify Style"
                                                CausesValidation="false" meta:resourcekey="ModifyResource1"></asp:LinkButton>
                                        </div>
                                        <div id="divDependencies" runat="server" class="hoverMenuActionLink" >
                                            <asp:LinkButton ID="lnkDependencies" runat="server" OnCommand="Dependency" Text="<%$ Resources:LogiAdHoc, ViewDependencyInfo %>"
                                                CausesValidation="false"></asp:LinkButton>
                                        </div>
                                        <div id="divDelete" runat="server" class="hoverMenuActionLink" visible="false">
                                            <asp:LinkButton ID="lnkDelete" runat="server" OnCommand="DeleteItem" Visible="false" 
                                                Text="Delete Style" meta:resourcekey="DeleteResource1"></asp:LinkButton>
                                        </div>
 				                    </asp:Panel>
                                    <ajaxToolkit:HoverMenuExtender ID="hme2" runat="server"
                                        PopupControlID="pnlActionsMenu" PopupPosition="right" 
                                        TargetControlID="imgActions" PopDelay="25" />
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                </div>
                    <asp:Button runat="server" ID="Button1" Style="display: none" />
                    <ajaxToolkit:ModalPopupExtender runat="server" ID="ahModalPopup" BehaviorID="ahModalPopupBehavior"
                        TargetControlID="Button1" PopupControlID="ahPopup" BackgroundCssClass="modalBackground"
                        DropShadow="false" PopupDragHandleControlID="pnlDragHandle" RepositionMode="None">
                    </ajaxToolkit:ModalPopupExtender>
                    
                    <asp:Panel runat="server" CssClass="modalPopup" ID="ahPopup" Style="display: none; width: 800;">
                        <asp:Panel ID="pnlDragHandle" runat="server" Style="cursor: hand;">
                        <div class="modalPopupHandle">
                            <table cellpadding="0" cellspacing="0" style="width: 100%"><tr><td>
                                <asp:Localize ID="PopupHeader" runat="server"></asp:Localize>
                                </td>
                                <td style="width: 20px;">
                                    <asp:ImageButton ID="imgClosePopup" runat="server" 
                                        OnClick="imgClosePopup_Click" CausesValidation="false"
                                        SkinID="imgbClose" ImageUrl="../ahImages/remove.gif" 
                                        AlternateText="<%$ Resources:LogiAdHoc, CloseWindow %>" />
                            </td></tr></table>
                        </div>
                        </asp:Panel>
                        <div class="modalDiv">
                        <asp:UpdatePanel ID="upPopup" runat="server">
                            <ContentTemplate>
                                <%--<asp:Panel runat="server" CssClass="modalPopup" ID="ahPopup" style="display:none;width:800;height:200;">--%>
                                <table class="tbTB">
                                    <tr>
                                        <td valign="top" width="90">
                                            <asp:Localize ID="Localize1" runat="server" meta:resourcekey="LiteralResource1" Text="Class:"></asp:Localize></td>
                                        <td valign="top">
                                            <input id="ClassName" type="text" maxlength="100" size="30" runat="server"><asp:RequiredFieldValidator
                                                ID="rtvClass" runat="server" ErrorMessage="Class is required." ControlToValidate="ClassName"
                                                meta:resourcekey="rtvClassResource1" ValidationGroup="Appearance">*</asp:RequiredFieldValidator>
                                        </td>
                                        <td valign="top">
                                            <asp:ImageButton ID="btnFV3" runat="server" AlternateText="Pick Class from Style Sheet"
                                                CausesValidation="False" ImageUrl="../ahImages/iconFind.gif" meta:resourcekey="btnFV3Resource1"
                                                OnClick="PickFromStyleSheet" ToolTip="Pick Class from Style Sheet" />
                                        </td>
                                        <td>
                                            <div id="divSelectFromCSS" runat="server">
                                                <table>
                                                    <tr>
                                                        <td>
                                                            <asp:ListBox ID="lstList" Rows="8" runat="server" AutoPostBack="True" OnSelectedIndexChanged="lstList_SelectedIndexChanged"
                                                                meta:resourcekey="lstListResource1" />
                                                        </td>
                                                        <td>
                                                            <AdHoc:LogiButton ID="btnFVOk" Width="50px" runat="server" Text="<%$ Resources:LogiAdHoc, OKWithSpaces %>"
                                                                OnClick="btnFVOk_Click" CausesValidation="False" />
                                                            <br />
                                                            <AdHoc:LogiButton ID="btnFVCancel" Width="50px" runat="server" Text="<%$ Resources:LogiAdHoc, Cancel %>"
                                                                OnClick="btnFVCancel_Click" CausesValidation="False" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </div>
                                        </td>
                                        <td width="20">
                                        </td>
                                        <td>
                                            <div id="divSampleText" runat="server">
                                                <asp:Label ID="lblSampleCSSTest" Text="Sample TEXT" runat="server" meta:resourcekey="lblSampleCSSTestResource1" />
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:Localize ID="Localize2" runat="server" meta:resourcekey="LiteralResource2" Text="Friendly Name:"></asp:Localize></td>
                                        <td>
                                            <input id="FriendlyName" type="text" maxlength="100" size="30" runat="server"><asp:RequiredFieldValidator
                                                ID="rtvFriendlyName" runat="server" ErrorMessage="Friendly Name is required." ValidationGroup="Appearance"
                                                ControlToValidate="FriendlyName" meta:resourcekey="rtvFriendlyNameResource1">*</asp:RequiredFieldValidator>
                                            <asp:CustomValidator ID="cvValidName" runat="server" ErrorMessage="This name already exists." ValidationGroup="Appearance"
                                                ControlToValidate="FriendlyName" OnServerValidate="IsNameValid" meta:resourcekey="cvValidNameResource1">*</asp:CustomValidator>
                                        </td>
                                        <td>
                                        </td>
                                        <td>
                                        </td>
                                        <td>
                                            <div id="divErrorText" runat="server">
                                                <asp:Label ID="lblErrorText" Text="Sample text may not be displayed as the specified Class does not exist in the currently specified Default Template."
                                                    runat="server" meta:resourcekey="lblErrorTextResource1" />
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                                <p>
                                </p>
                                <!-- Buttons -->
                                <table>
                                    <tr>
                                        <td colspan="2">
                                            <AdHoc:LogiButton ID="btnSaveClass" OnClick="SaveClass_OnClick" runat="server" UseSubmitBehavior="false"
                                                Text="<%$ Resources:LogiAdHoc, OKWithSpaces %>" ValidationGroup="Appearance" />
                                            <AdHoc:LogiButton ID="btnCancelClass" OnClick="CancelClass_OnClick" runat="server"
                                                Text="<%$ Resources:LogiAdHoc, Cancel %>" CausesValidation="False" />
                                        </td>
                                    </tr>
                                </table>
                                <asp:ValidationSummary ID="vsummary" runat="server" meta:resourcekey="vsummaryResource1" ValidationGroup="Appearance" />
                            </ContentTemplate>
                        </asp:UpdatePanel>
                        </div>
                        <%--</asp:Panel>--%>
                    </asp:Panel>
                </ContentTemplate>
                <Triggers>
                    <asp:AsyncPostBackTrigger ControlID="srch" EventName="DoSearch" />
                </Triggers>
            </asp:UpdatePanel>
            
            <div id="divSaveAnimation">
                <asp:Button runat="server" ID="btnSaveAnimation" Style="display: none" />
                <ajaxToolkit:ModalPopupExtender runat="server" ID="ahSavePopup" BehaviorID="ahSavePopupBehavior"
                    TargetControlID="btnSaveAnimation" PopupControlID="pnlSaving" BackgroundCssClass="modalBackground"
                    DropShadow="False">
                </ajaxToolkit:ModalPopupExtender>
                <asp:Panel id="pnlSaving" CssClass="savePopup" runat="server" style="display:none;">
                    <table><tr><td>
                    <asp:Image ID="imgSave" runat="server" SkinID="imgSaving" EnableViewState="false" AlternateText="<%$ Resources:LogiAdHoc, SaveImageAltText %>" />
                    </td>
                    <td valign="middle">
                    <asp:Label ID="lblSaveText" CssClass="lblSavePopup" runat="server" EnableViewState="false" Text="<%$ Resources:LogiAdHoc, PleaseWait %>"/>
                    </td></tr></table>
                </asp:Panel>
            </div>
        </td></tr></table>
</td></tr></table>
<br />
<br />
    </form>
</body>
</html>
