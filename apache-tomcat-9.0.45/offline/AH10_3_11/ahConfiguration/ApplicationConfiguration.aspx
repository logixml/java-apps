<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="ApplicationConfiguration.aspx.vb" Inherits="LogiAdHoc.ahAdministration_ApplicationConfiguration" Culture="auto" meta:resourcekey="PageResource2" UICulture="auto" %>

<%@ Register TagPrefix="AdHoc" TagName="MainMenu" Src="~/ahControls/MainMenu.ascx" %>
<%@ Register TagPrefix="AdHoc" TagName="NavMenu" Src="~/ahControls/NavMenu.ascx" %>
<%@ Register TagPrefix="AdHoc" TagName="BreadCrumbTrail" Src="~/ahControls/BreadCrumbTrail.ascx" %>
<%@ Register TagPrefix="AdHoc" TagName="ColorSequence" Src="~/ahControls/ColorSequence.ascx" %>
<%@ Register tagprefix="wizard" tagname="datebox1" src="../ahControls/DateBox.ascx" %>
<%@ Register TagPrefix="AdHoc" TagName="PagingControl" Src="~/ahControls/PagingControl.ascx" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Application Settings</title>
    <link rel="shortcut icon" href="../ahImages/flav.ico" />
    <script language="JavaScript" type="text/javascript" src="../ahScripts/ahChange.js"></script>
    <script type="text/javascript" src="../ahScripts/jscolor.js"></script>
    <script type="text/javascript">
        window.onbeforeunload = AlertOnExit;
    </script>
</head>
<body id="bod">
<AdHoc:MainMenu ID="menu" runat="server" />
<form id="form1" runat="server">        
<asp:ScriptManager ID="ScriptManager1" runat="server" EnablePartialRendering="true" />
<div id="submenu">
<AdHoc:NavMenu ID="subnav" runat="server" />
</div>
<AdHoc:BreadCrumbTrail ID="bct" runat="server" Key="AppConfig" ParentLevels="0" />

<div class="divForm">
<input type="hidden" id="ahDirty" name="ahDirty" runat="server" />
<!-- Put other global default settings here.  -->
        
<script type="text/javascript">
Sys.WebForms.PageRequestManager.getInstance().add_beginRequest(BeginRequestHandler);
Sys.WebForms.PageRequestManager.getInstance().add_endRequest(EndRequestHandler);
function EndRequestHandler(sender, args) {
    noMessage=false;
    $find('ahSavePopupBehavior').hide();      
    RestorePopupPosition('ahModalPopupBehavior');  
}
function BeginRequestHandler(sender, args) {
    SavePopupPosition('ahModalPopupBehavior');
}
</script> 

    <asp:UpdatePanel ID="UPMain" runat="server" UpdateMode="Conditional" RenderMode="inline" >
        <ContentTemplate>
        
    <h2><asp:Localize ID="Localize32" runat="server" meta:resourcekey="LiteralResource32" Text="General application settings:"></asp:Localize></h2>
    <table class="tbTB">
        <tr id="trTheme" runat="server">
            <td class="tdCaption">
                <label for="ddlApplicationTheme">
                <asp:Localize ID="Localize5" runat="server" meta:resourcekey="LiteralResource5" Text="Application Theme:"></asp:Localize>
                </label>
            </td>
            <td class="tdContent">
                <asp:DropDownList ID="ddlApplicationTheme" runat="Server">
                </asp:DropDownList>
            </td>
            <td>
                <img id="help4" runat="server" src="../ahImages/iconHelp.gif" />
                <ajaxToolkit:PopupControlExtender ID="PopupControlExtender4" runat="server" TargetControlID="help4" PopupControlID="pnlHelp4" Position="Bottom" DynamicServicePath="" Enabled="True" ExtenderControlID="" />
            </td>
        </tr>
        <tr>
            <td class="tdCaption">
            <label for="RowsPerPage">
                    <asp:Localize ID="Localize10" runat="server" meta:resourcekey="LiteralResource10" Text="Rows per page:"></asp:Localize></label></td>
            <td class="tdContent">
                <input id="RowsPerPage" type="text" maxlength="4" runat="server" name="RowsPerPage">
                <asp:RangeValidator ID="rvRowsPerPage" runat="server" ControlToValidate="RowsPerPage"
                    Type="Integer" MinimumValue="0" MaximumValue="1000" ErrorMessage="Rows per page accepts only integer values between 0 and 1000." meta:resourcekey="rvRowsPerPageResource1">*</asp:RangeValidator>
                <asp:RequiredFieldValidator ID="rtvRowsPerPage" runat="server" ControlToValidate="RowsPerPage"
                    ErrorMessage="Rows per page is required." meta:resourcekey="rtvRowsPerPageResource1">*</asp:RequiredFieldValidator>
            </td>
            <td>
                <img id="help8" runat="server" src="../ahImages/iconHelp.gif" />
                <ajaxToolkit:PopupControlExtender ID="PopupControlExtender8" runat="server" TargetControlID="help8" PopupControlID="pnlHelp8" Position="Bottom" DynamicServicePath="" Enabled="True" ExtenderControlID="" />
            </td>
        </tr>
        <tr>
            <td class="tdCaption">
                <asp:Localize ID="Localize11" runat="server" meta:resourcekey="LiteralResource11" Text="List Search Options:"></asp:Localize></td>
            <td class="tdContent">
                <asp:RadioButton ID="rbtnHideFilter" runat="server" GroupName="SearchFilterCondition"
                    Text="Never show" meta:resourcekey="rbtnHideFilterResource1" /><br />
                <asp:RadioButton ID="rbtnShowFilter" runat="server" Checked="True" GroupName="SearchFilterCondition"
                    Text="Always show" meta:resourcekey="rbtnShowFilterResource1" /><br />
                <asp:RadioButton ID="rbtnShowFilterIf" runat="server" GroupName="SearchFilterCondition"
                    Text="Show if the list contains equal to or more than" meta:resourcekey="rbtnShowFilterIfResource1" />
                <input id="ShowFilter" type="text" maxlength="3" runat="server" name="ShowFilter" style="width: 48px">
                <asp:Localize ID="Localize58" runat="server" text="rows" meta:resourcekey="LiteralResource58"></asp:Localize>
                <asp:CustomValidator ID="cvShowFilter" ControlToValidate="ShowFilter" EnableClientScript="False"
                        ErrorMessage="Find Filter rows value is required." runat="server" ValidateEmptyText="True"
                        OnServerValidate="IsShowFilterValid" meta:resourcekey="cvShowFilterResource1">*</asp:CustomValidator>
            </td>
            <td>
                <img id="help9" runat="server" src="../ahImages/iconHelp.gif" />
                <ajaxToolkit:PopupControlExtender ID="PopupControlExtender9" runat="server" TargetControlID="help9" PopupControlID="pnlHelp9" Position="Bottom" DynamicServicePath="" Enabled="True" ExtenderControlID="" />
            </td>
        </tr>
        <tr>
            <td class="tdCaption">
                <label for="PasswordEntryOption">
                    <asp:Localize ID="Localize6" runat="server" meta:resourcekey="LiteralResource6" Text="Show Password Entry Option:"></asp:Localize></label></td>
            <td class="tdContent">
                <asp:CheckBox ID="chkAllowPasswordEntry" runat="server" meta:resourcekey="chkAllowPasswordEntryResource1" /></td>
            <td>
                <img id="help5" runat="server" src="../ahImages/iconHelp.gif" />
                <ajaxToolkit:PopupControlExtender ID="PopupControlExtender5" runat="server" TargetControlID="help5" PopupControlID="pnlHelp5" Position="Bottom" DynamicServicePath="" Enabled="True" ExtenderControlID="" />
            </td>
        </tr>
        <tr id="trRemoveArchives" runat="Server">
            <td class="tdCaption">
                <asp:Localize ID="Localize9" runat="server" meta:resourcekey="LiteralResource9" Text="Delete archives with report:"></asp:Localize></td>
            <td class="tdContent">
            <asp:CheckBox ID="chkRemoveArchives" runat="server" meta:resourcekey="chkRemoveArchivesResource1" /></td>
            <td>
                <img id="help11" runat="server" src="../ahImages/iconHelp.gif" />
                <ajaxToolkit:PopupControlExtender ID="PopupControlExtender11" runat="server" TargetControlID="help11" PopupControlID="pnlHelp11" Position="Bottom" DynamicServicePath="" Enabled="True" ExtenderControlID="" />
            </td>
        </tr>
        <tr id="trUniqueRptFldName" runat="Server">
            <td class="tdCaption">
                <asp:Localize ID="Localize3" runat="server" meta:resourcekey="LiteralResource59" Text="Unique report/folder name:"></asp:Localize></td>
            <td class="tdContent">
            <asp:CheckBox ID="chkUniqueRptFldName" runat="server" meta:resourcekey="chkUniqueRptFldNameResource1" /></td>
            <td>
                <img id="help29" runat="server" src="../ahImages/iconHelp.gif" />
                <ajaxToolkit:PopupControlExtender ID="PopupControlExtender29" runat="server" TargetControlID="help29" PopupControlID="pnlHelp29" Position="Bottom" DynamicServicePath="" Enabled="True" ExtenderControlID="" />
            </td>
        </tr>
        <tr id="trFirstDayOfFiscalYear" runat="Server">
            <td class="tdCaption">
                <asp:Localize ID="Localize44" runat="server" meta:resourcekey="LiteralResource43" Text="First Day of Fiscal Year:"></asp:Localize></td>
            <td class="tdContent">
                <asp:DropDownList ID="ddlFiscalYearMonth" runat="server" >
                    <asp:ListItem Text="January" Value="01" />
                    <asp:ListItem Text="February" Value="02" />
                    <asp:ListItem Text="March" Value="03" />
                    <asp:ListItem Text="April" Value="04" />
                    <asp:ListItem Text="May" Value="05" />
                    <asp:ListItem Text="June" Value="06" />
                    <asp:ListItem Text="July" Value="07" />
                    <asp:ListItem Text="August" Value="08" />
                    <asp:ListItem Text="September" Value="09" />
                    <asp:ListItem Text="October" Value="10" />
                    <asp:ListItem Text="November" Value="11" />
                    <asp:ListItem Text="December" Value="12" />
                </asp:DropDownList>
                <asp:DropDownList ID="ddlFiscalYearDay" runat="server" >
                    <asp:ListItem Text="01" Value="01" />
                    <asp:ListItem Text="02" Value="02" />
                    <asp:ListItem Text="03" Value="03" />
                    <asp:ListItem Text="04" Value="04" />
                    <asp:ListItem Text="05" Value="05" />
                    <asp:ListItem Text="06" Value="06" />
                    <asp:ListItem Text="07" Value="07" />
                    <asp:ListItem Text="08" Value="08" />
                    <asp:ListItem Text="09" Value="09" />
                    <asp:ListItem Text="10" Value="10" />
                    <asp:ListItem Text="11" Value="11" />
                    <asp:ListItem Text="12" Value="12" />
                    <asp:ListItem Text="13" Value="13" />
                    <asp:ListItem Text="14" Value="14" />
                    <asp:ListItem Text="15" Value="15" />
                    <asp:ListItem Text="16" Value="16" />
                    <asp:ListItem Text="17" Value="17" />
                    <asp:ListItem Text="18" Value="18" />
                    <asp:ListItem Text="19" Value="19" />
                    <asp:ListItem Text="20" Value="20" />
                    <asp:ListItem Text="21" Value="21" />
                    <asp:ListItem Text="22" Value="22" />
                    <asp:ListItem Text="23" Value="23" />
                    <asp:ListItem Text="24" Value="24" />
                    <asp:ListItem Text="25" Value="25" />
                    <asp:ListItem Text="26" Value="26" />
                    <asp:ListItem Text="27" Value="27" />
                    <asp:ListItem Text="28" Value="28" />
                    <asp:ListItem Text="29" Value="29" />
                    <asp:ListItem Text="30" Value="30" />
                    <asp:ListItem Text="31" Value="31" />
                </asp:DropDownList>
            </td>
            <td>
                <img id="help18" runat="server" src="../ahImages/iconHelp.gif" />
                <ajaxToolkit:PopupControlExtender ID="PopupControlExtender18" runat="server" TargetControlID="help18" PopupControlID="pnlHelp18" Position="Bottom" DynamicServicePath="" Enabled="True" ExtenderControlID="" />
            </td>
        </tr>
    </table>
        
    <h2><asp:Localize ID="Localize34" runat="server" meta:resourcekey="LiteralResource34" Text="Report settings:"></asp:Localize></h2>
    <table class="tbTB">
        <tr>
            <td class="tdCaption">
                <label for="ReportTarget">
                    <asp:Localize ID="Localize12" runat="server" meta:resourcekey="LiteralResource12" Text="Report Hyperlink target window:"></asp:Localize></label></td>
            <td class="tdContent">
                <%--<input id="ReportTarget" type="text" runat="server" name="ReportTarget" style="width: 232px">--%>
                <asp:DropDownList ID="ddlReportTarget" runat="server" >
                    <asp:ListItem Value="_blank" meta:resourcekey="ListItemResource18">_blank</asp:ListItem>
                    <asp:ListItem Value="_parent" meta:resourcekey="ListItemResource19">_parent</asp:ListItem>
                    <asp:ListItem Value="_self" meta:resourcekey="ListItemResource20">_self</asp:ListItem>
                    <asp:ListItem Value="_top" meta:resourcekey="ListItemResource21">_top</asp:ListItem>
                </asp:DropDownList>
            </td>
            <td>
                <img id="help10" runat="server" src="../ahImages/iconHelp.gif" />
                <ajaxToolkit:PopupControlExtender ID="PopupControlExtender10" runat="server" TargetControlID="help10" PopupControlID="pnlHelp10" Position="Bottom" DynamicServicePath="" Enabled="True" ExtenderControlID="" />
            </td>
        </tr>
        <tr>
            <td class="tdCaption">
                <asp:Localize ID="Localize45" runat="server" meta:resourcekey="LiteralResource40"
                    Text="Show a disabled link to no-access reports:"></asp:Localize>
            </td>
            <td class="tdContent">
                <asp:CheckBox ID="chkShowNoAccessReports" runat="server" />
            </td>
            <td>
                <img id="help17" runat="server" src="../ahImages/iconHelp.gif" />
                <ajaxToolkit:PopupControlExtender ID="PopupControlExtender17" runat="server" DynamicServicePath=""
                    Enabled="True" ExtenderControlID="" PopupControlID="pnlHelp17" Position="Bottom"
                    TargetControlID="help17">
                </ajaxToolkit:PopupControlExtender>
            </td>
        </tr>
        <tr id="trWebStudioMode" runat="Server">
            <td class="tdCaption">
                <label for="webStudioMode">
                    <asp:Localize ID="Localize13" runat="server" meta:resourcekey="LiteralResource13" Text="Create/Modify reports using Web Studio"></asp:Localize></label></td>
            <td class="tdContent">
                <%--<asp:DropDownList ID="webStudioMode" runat="server" meta:resourcekey="webStudioModeResource1">
                    <asp:ListItem Value="1" meta:resourcekey="ListItemResource1">Allow only from Report's grid</asp:ListItem>
                    <asp:ListItem Value="2" meta:resourcekey="ListItemResource2">Allow only from a &quot;Open&quot; button in main page</asp:ListItem>
                    <asp:ListItem Value="3" meta:resourcekey="ListItemResource3">Show both</asp:ListItem>
                </asp:DropDownList>--%>
                <table>
                    <tr>
                        <td>
                            <asp:CheckBox ID="chkWSReportsGrid" runat="server" Text="From report listing grid" meta:resourcekey="chkWSReportsGridResource1" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:CheckBox ID="chkWSOpenStudio" runat="server" Text="From 'Open Web Studio' button" meta:resourcekey="chkWSOpenStudioGridResource1" />
                        </td>
                    </tr>
                </table>
            </td>
            <td>
                <img id="help12" runat="server" src="../ahImages/iconHelp.gif" />
                <ajaxToolkit:PopupControlExtender ID="PopupControlExtender12" runat="server" TargetControlID="help12" PopupControlID="pnlHelp12" Position="Bottom" DynamicServicePath="" Enabled="True" ExtenderControlID="" />
            </td>
        </tr>
    </table>
    <div id="divAdHocSpecificSettings2" runat="server">
    <table class="tbTB">
        <tr>
            <td class="tdCaption">
                <asp:Localize ID="Localize36" runat="server" meta:resourcekey="LiteralResource36" Text="Show live preview:"></asp:Localize></td>
            <td class="tdContent">
                <asp:CheckBox ID="chkSectionPreview" runat="server" />
            </td>
            <td>
                <img id="help15" runat="server" src="../ahImages/iconHelp.gif" />
                <ajaxToolkit:PopupControlExtender ID="PopupControlExtender15" runat="server" TargetControlID="help15" PopupControlID="pnlHelp15" Position="Bottom" DynamicServicePath="" Enabled="True" ExtenderControlID="" />
            </td>
        </tr>            
        <tr>
            <td class="tdCaption">
                <label for="Encoding">
                    <asp:Localize ID="Localize2" runat="server" meta:resourcekey="LiteralResource2" Text="Encoding:"></asp:Localize></label></td>
            <td class="tdContent">
                <asp:DropDownList ID="ddlEncoding" runat="Server">
                    <asp:ListItem Text="ISO-8859-1 (Latin1 USA, West European)" Value="ISO-8859-1" meta:resourcekey="ListItemResource6"></asp:ListItem>
                    <asp:ListItem Text="ISO-8859-2 (Latin2 East European)" Value="ISO-8859-2" meta:resourcekey="ListItemResource7"></asp:ListItem>                            
                    <asp:ListItem Text="ISO-8859-4 (Latin4 North European)" Value="ISO-8859-4" meta:resourcekey="ListItemResource8"></asp:ListItem>
                    <asp:ListItem Text="ISO-8859-5 (Cyrillic)" Value="ISO-8859-5" meta:resourcekey="ListItemResource9"></asp:ListItem>
                    <asp:ListItem Text="ISO-8859-6 (Arabic)" Value="ISO-8859-6" meta:resourcekey="ListItemResource10"></asp:ListItem>
                    <asp:ListItem Text="ISO-8859-7 (Greek)" Value="ISO-8859-7" meta:resourcekey="ListItemResource11"></asp:ListItem>
                    <asp:ListItem Text="ISO-8859-8 (Hebrew)" Value="ISO-8859-8" meta:resourcekey="ListItemResource12"></asp:ListItem>
                    <asp:ListItem Text="ISO-8859-9 (Latin5 Turkish)" Value="ISO-8859-9" meta:resourcekey="ListItemResource13"></asp:ListItem>
                    <asp:ListItem Text="UTF-8 (Unicode)" Value="UTF-8" meta:resourcekey="ListItemResource14"></asp:ListItem>
                    <asp:ListItem Text="UTF-16 (Unicode)" Value="UTF-16" meta:resourcekey="ListItemResource15"></asp:ListItem>
                    <asp:ListItem Text="Big5 (Chinese)" Value="BIG5" meta:resourcekey="ListItemResource16"></asp:ListItem>
                    <asp:ListItem Text="Windows-1251 (Cyrillic)" Value="WINDOWS-1251" meta:resourcekey="ListItemResource17"></asp:ListItem>
                </asp:DropDownList>
            </td>
            <td>
                <img id="help1" runat="server" src="../ahImages/iconHelp.gif" />
                <ajaxToolkit:PopupControlExtender ID="PopupControlExtender1" runat="server" TargetControlID="help1" PopupControlID="pnlHelp1" Position="Bottom" DynamicServicePath="" Enabled="True" ExtenderControlID="" />
            </td>
        </tr>
        <tr>
            <td class="tdCaption">
                <asp:Localize ID="Localize47" runat="server" meta:resourcekey="LiteralResource47" Text="Default Report Expiration Date:"></asp:Localize></td>
            <td class="tdContent">
                <table>
                    <tr>
                        <td>
                            <asp:RadioButton ID="rbtnExpirationDateNone" runat="server" GroupName="ExpirationDateCondition"
                                Text="None" meta:resourcekey="rbtnExpirationDateNoneResource1" />
                        </td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>
                            <asp:RadioButton ID="rbtnExpirationDateDays" runat="server" GroupName="ExpirationDateCondition"
                                Text="Days" meta:resourcekey="rbtnExpirationDateDaysResource1" />
                        </td>
                        <td>
                            <asp:TextBox ID="txtReportExpirationDateDays" runat="server" />
                            <asp:CustomValidator ID="cvtxtReportExpirationDateDays" ControlToValidate="txtReportExpirationDateDays" EnableClientScript="False"
                                ErrorMessage="Default Report Expiration Date: Days must be a whole number." runat="server" ValidateEmptyText="True"
                                OnServerValidate="IsReportExpirationDateDaysValid" meta:resourcekey="cvtxtReportExpirationDateDaysResource1">*</asp:CustomValidator>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:RadioButton ID="rbtnExpirationDateDate" runat="server" GroupName="ExpirationDateCondition"
                                Text="Date" meta:resourcekey="rbtnExpirationDateDateResource1" />
                        </td>
                        <td>
                            <%--<asp:TextBox ID="txtReportExpirationDateDate" runat="server" />--%>
                            <wizard:datebox1 id="DefaultReportExpirationDate" runat="server" Required="false" />
                            <asp:CustomValidator ID="cvtxtReportExpirationDateDate" ControlToValidate="txtReportExpirationDateDays" EnableClientScript="False"
                                ErrorMessage="Default Report Expiration Date: Date must be a valid date in the future." runat="server" ValidateEmptyText="True"
                                OnServerValidate="IsReportExpirationDateDateValid" meta:resourcekey="cvtxtReportExpirationDateDateResource1">*</asp:CustomValidator>
                        </td>
                    </tr>
                </table>
            </td>
            <td>
                <img id="help21" runat="server" src="../ahImages/iconHelp.gif" />
                <ajaxToolkit:PopupControlExtender ID="PopupControlExtender21" runat="server" TargetControlID="help21" PopupControlID="pnlHelp21" Position="Bottom" DynamicServicePath="" Enabled="True" ExtenderControlID="" />
            </td>
        </tr>
        <tr>
            <td class="tdCaption">
                <label for="TxtAreaChartColorSequence">
                    <asp:Localize ID="Localize14" runat="server" meta:resourcekey="LiteralResource14" Text="Chart color sequence:"></asp:Localize></label></td>
            <td class="tdContent">
                <%--<textarea id="txtAreaChartColorSequence" runat="server" cols="45" rows="4"></textarea>--%>
                <asp:UpdatePanel ID="UP1" runat="server" UpdateMode="Conditional" >
                    <ContentTemplate>
                        <AdHoc:ColorSequence ID="PieChartImage" runat="Server"  />
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
            <td>
                <img id="help13" runat="server" src="../ahImages/iconHelp.gif" />
                <ajaxToolkit:PopupControlExtender ID="PopupControlExtender13" runat="server" TargetControlID="help13" PopupControlID="pnlHelp13" Position="Bottom" DynamicServicePath="" Enabled="True" ExtenderControlID="" />
            </td>
        </tr>
    </table>
    </div>

    <h2>
        <asp:Localize ID="Localize8" runat="server" meta:resourcekey="LiteralResource44"
            Text="Password Management:"></asp:Localize></h2>
    <table class="tbTB">
        <tr>
            <td class="tdCaption">
                <label for="chkChangeAtLogon">
                    <asp:Localize ID="Localize15" runat="server" meta:resourcekey="LiteralResource45"
                        Text="Require change of password at logon:"></asp:Localize></label></td>
            <td class="tdContent">
                <asp:CheckBox ID="chkChangeAtLogon" runat="server" />
            </td>
            <td>
                <img id="help16" runat="server" src="../ahImages/iconHelp.gif" />
                <ajaxToolkit:PopupControlExtender ID="PopupControlExtender16" runat="server" DynamicServicePath=""
                    Enabled="True" ExtenderControlID="" PopupControlID="pnlHelp16" Position="Bottom"
                    TargetControlID="help16">
                </ajaxToolkit:PopupControlExtender>
            </td>
        </tr>
        <tr>
            <td class="tdCaption">
                <label for="txtChangePwdNDays">
                    <asp:Localize ID="Localize16" runat="server" meta:resourcekey="LiteralResource51"
                        Text="Require change of password every "></asp:Localize></label></td>
            <td class="tdContent">
                <asp:TextBox ID="txtChangePwdNDays" runat="server"></asp:TextBox>&nbsp;
                <asp:Localize ID="Localize31" runat="server" meta:resourcekey="LiteralResource50"
                    Text="days"></asp:Localize>
                <asp:RangeValidator ID="rvtxtChangePwdNDays" runat="server" ControlToValidate="txtChangePwdNDays"
                    ErrorMessage="Number of days must be a whole number between 0 and 9999." MaximumValue="9999"
                    meta:resourcekey="rvtxtChangePwdNDaysResource1" MinimumValue="0" Type="Integer">*</asp:RangeValidator>
             </td>
            <td>
                <img id="help27" runat="server" src="../ahImages/iconHelp.gif" />
                <ajaxToolkit:PopupControlExtender ID="PopupControlExtender27" runat="server" DynamicServicePath=""
                    Enabled="True" ExtenderControlID="" PopupControlID="pnlHelp27" Position="Bottom"
                    TargetControlID="help27">
                </ajaxToolkit:PopupControlExtender>
            </td>
        </tr>
        <tr>
            <td class="tdCaption">
                <label for="txtMinPasswordLength">
                    <asp:Localize ID="Localize23" runat="server" meta:resourcekey="LiteralResource48"
                        Text="Minimum password length:"></asp:Localize></label></td>
            <td class="tdContent">
                <asp:TextBox ID="txtMinPasswordLength" runat="server"></asp:TextBox>
                <asp:RangeValidator ID="rvtxtMinPasswordLength" runat="server" ControlToValidate="txtMinPasswordLength"
                    ErrorMessage="Minimum password length must be a whole number between 0 and 50." MaximumValue="50"
                    meta:resourcekey="rvtxtMinPasswordLengthResource1" MinimumValue="0" Type="Integer">*</asp:RangeValidator>
            </td>
            <td>
                <img id="help28" runat="server" src="../ahImages/iconHelp.gif" />
                <ajaxToolkit:PopupControlExtender ID="PopupControlExtender28" runat="server" DynamicServicePath=""
                    Enabled="True" ExtenderControlID="" PopupControlID="pnlHelp28" Position="Bottom"
                    TargetControlID="help28">
                </ajaxToolkit:PopupControlExtender>
            </td>
        </tr>
        <tr>
            <td class="tdCaption">
                <label for="txtMaxPasswordLength">
                    <asp:Localize ID="Localize30" runat="server" meta:resourcekey="LiteralResource49"
                        Text="Maximum password length:"></asp:Localize></label></td>
            <td class="tdContent">
                <asp:TextBox ID="txtMaxPasswordLength" runat="server"></asp:TextBox>
                <asp:RangeValidator ID="rvtxtMaxPasswordLength" runat="server" ControlToValidate="txtMaxPasswordLength"
                    ErrorMessage="Maximum password length must be a whole number between 0 and 50." MaximumValue="50" 
                    meta:resourcekey="rvtxtMaxPasswordLengthResource1" MinimumValue="0" Type="Integer">*</asp:RangeValidator>
            </td>
            <td>
                <img id="help19" runat="server" src="../ahImages/iconHelp.gif" />
                <ajaxToolkit:PopupControlExtender ID="PopupControlExtender19" runat="server" DynamicServicePath=""
                    Enabled="True" ExtenderControlID="" PopupControlID="pnlHelp19" Position="Bottom"
                    TargetControlID="help19">
                </ajaxToolkit:PopupControlExtender>
            </td>
        </tr>
        <tr>
            <td class="tdCaption">
                <label for="txtMaxPasswordLength">
                    <asp:Localize ID="Localize38" runat="server" meta:resourcekey="LiteralResource46"
                        Text="Password Restrictions:"></asp:Localize></label></td>
            <td class="tdContent">
                <table>
                    <tr>
                        <td>
                            <asp:CheckBox ID="chkDiffNewPassword" runat="server" meta:resourcekey="chkDiffNewPasswordResource1"
                                Text="New Password must be different." />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:CheckBox ID="chkPasswordUppercase" runat="server" meta:resourcekey="chkPasswordUppercaseResource1"
                                Text="Include at least one Uppercase letter." />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:CheckBox ID="chkPasswordNumber" runat="server" meta:resourcekey="chkPasswordNumberResource1"
                                Text="Include at least one number." />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:CheckBox ID="chkPasswordSpecialChar" runat="server" meta:resourcekey="chkPasswordSpecialCharResource1"
                                Text="Include at least one special character (~_[]#$%*)." />
                        </td>
                    </tr>
                </table>
            </td>
            <td>
                <img id="help20" runat="server" src="../ahImages/iconHelp.gif" />
                <ajaxToolkit:PopupControlExtender ID="PopupControlExtender20" runat="server" DynamicServicePath=""
                    Enabled="True" ExtenderControlID="" PopupControlID="pnlHelp20" Position="Bottom"
                    TargetControlID="help20">
                </ajaxToolkit:PopupControlExtender>
            </td>
        </tr>
    </table>

    <asp:Panel ID="pnlHelp1" runat="server" CssClass="popupControl" Width="300px" meta:resourcekey="pnlHelp1Resource1">
        <asp:Localize ID="Localize17" runat="server" meta:resourcekey="LiteralResource17" >
        Determines the default character encoding for this application. The default value is UTF-8 (Unicode).</asp:Localize></asp:Panel>

    <asp:Panel ID="pnlHelp4" runat="server" CssClass="popupControl" Width="300px">
        <asp:Localize ID="Localize1" runat="server" meta:resourcekey="LiteralResource1" >
        Sets the theme that determines the application's overall look and color scheme.</asp:Localize></asp:Panel>

    <asp:Panel ID="pnlHelp5" runat="server" CssClass="popupControl" Width="300px" meta:resourcekey="pnlHelp5Resource1">
        <asp:Localize ID="Localize21" runat="server" meta:resourcekey="LiteralResource21" >
        Determines a user's ability to modify their password from within the application. If checked, the Users 
        will be able to modify their passwords using the Profile webpage. Customers integrating a proprietary 
        authentication schema within the application may want to disable password entry.</asp:Localize></asp:Panel>
    
    <%--<asp:Panel ID="pnlHelp7" runat="server" CssClass="popupControl" Width="300px" meta:resourcekey="pnlHelp7Resource1">
        <asp:Localize ID="Localize23" runat="server" meta:resourcekey="LiteralResource23" >
        Determines the visibility of the top menu portion of the application.</asp:Localize>
        </asp:Panel>--%>
    
    <asp:Panel ID="pnlHelp8" runat="server" CssClass="popupControl" Width="300px" meta:resourcekey="pnlHelp8Resource1">
        <asp:Localize ID="Localize24" runat="server" meta:resourcekey="LiteralResource24" >
        Determines if paging is performed on the grids in the user interface and how many rows 
        will appear per page. A value greater than 0 turns the paging feature on and dictates 
        the number of row per page. A value of 0 turns paging off and all rows are displayed in 
        one big list.</asp:Localize>
        </asp:Panel>
    
    <asp:Panel ID="pnlHelp9" runat="server" CssClass="popupControl" Width="300px" meta:resourcekey="pnlHelp9Resource1">
        <asp:Localize ID="Localize25" runat="server" meta:resourcekey="LiteralResource25" >
        Determines whether or not to include a search filter in pages with grids. You can
        enter a value between 1 and 100 for number of rows. This value causes the filter
        to display only if there are more rows present than specified.</asp:Localize>
        </asp:Panel>
        
    <asp:Panel ID="pnlHelp10" runat="server" CssClass="popupControl" Width="300px" meta:resourcekey="pnlHelp10Resource1">
        <asp:Localize ID="Localize26" runat="server" meta:resourcekey="LiteralResource26" >
        Sets the target window for the hyperlinks to run the reports. The valid values can be:<br />
        <b>_blank</b>  Renders the report in a new window without frames. This is the default value.<br />
        <b>_parent</b> Renders the report in the immediate frameset parent. <br />
        <b>_self</b>   Renders the report in the frame with focus. <br />
        <b>_top</b>    Renders the report in the full window without frames. <br />
        </asp:Localize></asp:Panel>
    
    <asp:Panel ID="pnlHelp11" runat="server" CssClass="popupControl" Width="300px" meta:resourcekey="pnlHelp11Resource1">
        <asp:Localize ID="Localize27" runat="server" meta:resourcekey="LiteralResource27" >
        Determines if a report's Archives are automatically deleted when the report is deleted. If checked, 
        the Archives for a report will be deleted if the report is removed. Setting this option to true might 
        result in decreased performance when a report is deleted. A backup of deleted reports and archives 
        are located in the _Definitions\_Reports\_Backup folder.</asp:Localize></asp:Panel>
        
    <asp:Panel ID="pnlHelp12" runat="server" CssClass="popupControl" Width="300px" meta:resourcekey="pnlHelp12Resource1">
        <asp:Localize ID="Localize28" runat="server" meta:resourcekey="LiteralResource28" >
        Determines the availability of Web Studio. Reports may be created and/or modified using the Web Studio 
        report definition editor. The following settings offer the following capabilities:<br />
        - Modify any report from the <b>report listing grid</b> via action icon<br />
        - Create/Modify an Info report via <b>'Open Web Studio'</b> button<br /><br />
        Note: When a report is modified using the 'Modify Report in Web Studio' action icon, 
        the respective report's definition will be displayed in Web Studio.</asp:Localize></asp:Panel>
        
    <asp:Panel ID="pnlHelp13" runat="server" CssClass="popupControl" Width="300px" meta:resourcekey="pnlHelp13Resource1">
        <asp:Localize ID="Localize29" runat="server" meta:resourcekey="LiteralResource29" >
        Determines the default color sequence used in crosstab charts and pie charts. Colors may be added 
        by selecting the desired color in dropdown list and clicking on 'Add' button. Colors can also be 
        update or removed by selecting the desired pie slice and clicking on 'Update' or 'Remove' button. 
        The default sequence is:  Red, Green, Blue, Yellow, Magenta, DeepSkyBlue, Orange, 
        Silver, BlueViolet. <br /><br />
        Note: When there are more slices than colors, the list starts back at the beginning. 
        </asp:Localize></asp:Panel>
        
    <%--<asp:Panel ID="pnlHelp14" runat="server" CssClass="popupControl" Width="300px" meta:resourcekey="pnlHelp14Resource1">
        <asp:Localize ID="Localize30" runat="server" meta:resourcekey="LiteralResource30" >
        Sets the default template to be used by report builder for creating a new report.</asp:Localize></asp:Panel>--%>
    
    <asp:Panel ID="pnlHelp15" runat="server" CssClass="popupControl" Width="300px">
        <asp:Localize ID="Localize37" runat="server" meta:resourcekey="LiteralResource37" >
        Determines the availability of Live Preview in the Report Builder.
        </asp:Localize></asp:Panel>
    
    <asp:Panel ID="pnlHelp16" runat="server" CssClass="popupControl" Width="300px">
        <asp:Localize ID="Localize39" runat="server" meta:resourcekey="LiteralResource52">
        Determines if the users will be required to change their password when they login for the first time and/or after a specified number of days.
        </asp:Localize></asp:Panel>
    
    <asp:Panel ID="pnlHelp17" runat="server" CssClass="popupControl" meta:resourcekey="pnlHelp17Resource1"
        Width="300px">
        <asp:Localize ID="Localize41" runat="server" meta:resourcekey="LiteralResource41">
        Determines whether a user will be able to see a report to which they do not have access to.
        If Checked, reports will show as a disabled link if the user does not have access to any of 
        the objects or columns used in the report. If Unchecked, the reports will be hidden.</asp:Localize></asp:Panel>                    
        
    <asp:Panel ID="pnlHelp18" runat="server" CssClass="popupControl" meta:resourcekey="pnlHelp18Resource1"
        Width="300px">
        <asp:Localize ID="Localize42" runat="server" meta:resourcekey="LiteralResource42">
        Sets the first day of fiscal year which is used when calculating dates and quarters for fiscal calendars.
        </asp:Localize></asp:Panel>
    
     <asp:Panel ID="pnlHelp19" runat="server" CssClass="popupControl" Width="300px">
        <asp:Localize ID="Localize50" runat="server" meta:resourcekey="LiteralResource53">
        Determines the maximum password length. 
        </asp:Localize></asp:Panel>
        
    <asp:Panel ID="pnlHelp20" runat="server" CssClass="popupControl" Width="300px">
        <asp:Localize ID="Localize43" runat="server" meta:resourcekey="LiteralResource54">
        Determines the restrictions on password string when a user tries to change their password.
        </asp:Localize></asp:Panel>
                
    <asp:Panel ID="pnlHelp21" runat="server" CssClass="popupControl" Width="300px">
        <asp:Localize ID="Localize52" runat="server" meta:resourcekey="LiteralResource55">
        Sets the default Report Expiration Date.
        </asp:Localize></asp:Panel>
        
    <asp:Panel ID="pnlHelp27" runat="server" CssClass="popupControl" Width="300px">
        <asp:Localize ID="Localize40" runat="server" meta:resourcekey="LiteralResource56">
        Determines the number of a days a user's password will expire and they will be required to change their password on login.
        </asp:Localize></asp:Panel>
        
    <asp:Panel ID="pnlHelp28" runat="server" CssClass="popupControl" Width="300px">
        <asp:Localize ID="Localize49" runat="server" meta:resourcekey="LiteralResource57">
        Determines the minimum password length.                
        </asp:Localize></asp:Panel>
    
    <asp:Panel ID="pnlHelp29" runat="server" CssClass="popupControl" Width="300px">
        <asp:Localize ID="Localize4" runat="server" meta:resourcekey="LiteralResource60">
        Unique Report/Folder name determines whether duplicate reports/folders are permitted in the instance. 
        If this option is enabled on an instance with duplicate items, a dialog will be presented to allow 
        the duplications to be resolved.
        </asp:Localize></asp:Panel>
                  
    <div id="divButtons" class="divButtons">
        <AdHoc:LogiButton ID="btnSave" runat="server" OnClick="Save_OnClick" ToolTip="<%$ Resources:LogiAdHoc, SaveTooltip %>"
            Text="<%$ Resources:LogiAdHoc, Save %>" UseSubmitBehavior="false" Width="65px" />
        <asp:ValidationSummary ID="vsummary" runat="server" meta:resourcekey="vsummaryResource1" />
    </div>
    <br />

    <asp:Button runat="server" ID="Button1" style="display:none"/>
    <ajaxToolkit:ModalPopupExtender runat="server" ID="ahModalPopup" BehaviorID="ahModalPopupBehavior"
        TargetControlID="Button1" PopupControlID="ahPopup" BackgroundCssClass="modalBackground"
        DropShadow="false" PopupDragHandleControlID="pnlDragHandle" RepositionMode="None">
    </ajaxToolkit:ModalPopupExtender>
    <asp:Panel runat="server" CssClass="modalPopup" ID="ahPopup" style="display:none; width:830;">
        <asp:Panel ID="pnlDragHandle" runat="server" Style="cursor: hand;">
        <div class="modalPopupHandle" >
            <table cellpadding="0" cellspacing="0" style="width: 100%"><tr><td>
                <asp:Localize ID="PopupHeader" runat="server" meta:resourcekey="LiteralResource61" Text="Rename Reports/Folders"></asp:Localize>
                </td>
                <td style="width: 20px;">
                    <asp:ImageButton ID="imgClosePopup" runat="server" 
                        OnClick="imgClosePopup_Click" CausesValidation="false"
                        SkinID="imgbClose" ImageUrl="../ahImages/remove.gif"  
                        AlternateText="<%$ Resources:LogiAdHoc, CloseWindow %>"/>
            </td></tr></table>
        </div>
        </asp:Panel>
        <div class="modalDiv">
        <asp:UpdatePanel ID="upPopup" runat="server" UpdateMode="Conditional" >
            <ContentTemplate>
                <div style="width:800px;">
                <p id="info" runat="server" class="info">
                    <asp:Localize ID="Localize7" runat="server" meta:resourcekey="LiteralResource62"
                        Text="The following reports/ dashboards/ folders do not meet the unique name requirement. They need to be renamed before this setting can be applied."></asp:Localize>
                </p>
                </div>
                <br />
                <div id="divRptFldGrid" runat="server" style="width:800px; height:400px; overflow:auto;">
                    <asp:UpdatePanel ID="UPGrdMain" runat="server" UpdateMode="Conditional" RenderMode="Inline">
                        <ContentTemplate>
                            <asp:GridView ID="grdMain" runat="server" CssClass="gridWline" OnRowDataBound="OnReportFolderItemDataBoundHandler"
                                AutoGenerateColumns="False" AllowPaging="True" meta:resourcekey="grdMainResource1">
                                <Columns>
                                    <asp:BoundField DataField="OBJECTTYPESTR" SortExpression="OBJECTTYPESTR" HeaderText="Type" HeaderStyle-Width="50px" meta:resourcekey="BoundFieldResource1" />
                                    <asp:BoundField DataField="OBJECTNAME" SortExpression="OBJECTNAME" HeaderText="Name" meta:resourcekey="BoundFieldResource2" HeaderStyle-Width="100px"/>
                                    <asp:TemplateField HeaderText="New Name" meta:resourcekey="TemplateFieldResource1">
                                        <ItemTemplate>
                                            <input type="hidden" id="ObjectType" runat="server" />
                                            <input type="hidden" id="ObjectID" runat="server" />
                                            <asp:TextBox ID="txtNewName" runat="server" />
                                        </ItemTemplate>
                                        <HeaderStyle Width="100px" />
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="DATABASENAME" SortExpression="DATABASENAME" HeaderText="Database" HeaderStyle-Width="100px" meta:resourcekey="BoundFieldResource3" />
                                    <asp:BoundField DataField="ORGANIZATION" SortExpression="ORGANIZATION" HeaderText="Organization" HeaderStyle-Width="100px" meta:resourcekey="BoundFieldResource4" />
                                    <asp:BoundField DataField="OWNER" SortExpression="OWNER" HeaderText="Owner" HeaderStyle-Width="100px" meta:resourcekey="BoundFieldResource5" />
                                    <asp:BoundField DataField="Folder" SortExpression="Folder" HeaderText="Folder" HeaderStyle-Width="100px" meta:resourcekey="BoundFieldResource6" />
                                    <asp:BoundField DataField="FOLDERTYPENAME" SortExpression="FOLDERTYPENAME" HeaderText="Folder Type" HeaderStyle-Width="100px" meta:resourcekey="BoundFieldResource7" />
                                </Columns>
                                <PagerTemplate>
                                    <AdHoc:PagingControl ID="pageCtrl" runat="server" OnGotoNextPage="ChangePageIndex" />
                                </PagerTemplate>
                                <PagerStyle HorizontalAlign="Center"></PagerStyle>
                                <HeaderStyle CssClass="gridheader"></HeaderStyle>
                            </asp:GridView> 
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>     
                <br />
                <!-- Buttons -->
                <table>
                    <tr>
                        <td colspan="2">
                            <AdHoc:LogiButton ID="btnSaveItem" OnClick="SaveItem_OnClick" runat="server" CausesValidation="false"
                                Text="<%$ Resources:LogiAdHoc, OKWithSpaces %>" UseSubmitBehavior="false" />
                            <AdHoc:LogiButton ID="btnCancelItem" OnClick="CancelItem_OnClick" runat="server"
                                Text="<%$ Resources:LogiAdHoc, Cancel %>" CausesValidation="False" />
                        </td>
                    </tr>
                </table>
            </ContentTemplate>
        </asp:UpdatePanel>
        </div>
    </asp:Panel>
                  
    <div id="divSaveAnimation">
        <asp:Button runat="server" ID="btnSaveAnimation" Style="display: none" />
        <ajaxToolkit:ModalPopupExtender runat="server" ID="ahSavePopup" BehaviorID="ahSavePopupBehavior"
            TargetControlID="btnSaveAnimation" PopupControlID="pnlSaving" BackgroundCssClass="modalBackground"
            DropShadow="False">
        </ajaxToolkit:ModalPopupExtender>
        <asp:Panel id="pnlSaving" CssClass="savePopup" runat="server" style="display:none;">
            <table><tr><td>
            <asp:Image ID="imgSave" runat="server" SkinID="imgSaving" EnableViewState="false" AlternateText="<%$ Resources:LogiAdHoc, SaveImageAltText %>" />
            </td>
            <td valign="middle">
            <asp:Label ID="lblSaveText" CssClass="lblSavePopup" runat="server" EnableViewState="false" Text="<%$ Resources:LogiAdHoc, PleaseWait %>"/>
            </td></tr></table>
        </asp:Panel>
    </div>
    </ContentTemplate>
    </asp:UpdatePanel>
</div>
        <script type="text/javascript">

            function RebuildColorPicker(myInputElement)
            {
                var myColor = new jscolor.color(myInputElement)
                jscolor.install();
                jscolor.bind();        
            }

        </script>
</form>
</body>
</html>
