<%@ Page Language="vb" AutoEventWireup="false" Codebehind="Catalog.aspx.vb" Inherits="LogiAdHoc.ahConfiguration_Catalog" Culture="auto" meta:resourcekey="PageResource1" UICulture="auto" %>

<%@ Register TagPrefix="AdHoc" TagName="MainMenu" Src="~/ahControls/MainMenu.ascx" %>
<%@ Register TagPrefix="AdHoc" TagName="NavMenu" Src="~/ahControls/NavMenu.ascx" %>
<%@ Register TagPrefix="AdHoc" TagName="BreadCrumbTrail" Src="~/ahControls/BreadCrumbTrail.ascx" %>
<%@ Register TagPrefix="AdHoc" TagName="PagingControl" Src="../ahControls/PagingControl.ascx" %>
<%@ Register TagPrefix="cc1" Assembly="LGXAHWCL" Namespace="LGXAHWCL" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Catalog</title>
    <link rel="shortcut icon" href="../ahImages/flav.ico" />
    <script language="JavaScript" type="text/javascript" src="../ahScripts/ahChange.js"></script>
    <script type="text/javascript">
        window.onbeforeunload = AlertOnExit;
    </script>
    <script type="text/javascript">
    
    //var SelectRowIndex = -1;
    var SelectRowIndex = new Object();
	function SelectRow(gridID, e)
	{
	    if(!e) e = window.event;//Added this for IE
	    
		var obj = e.srcElement;
		if(!obj) obj = e.target;//Added this for Firefox
		if (obj && obj.tagName=="TD") //this a table cell
		{
		    var i = 0;
		    var chk;
		    var selectedIndex = -1; 
		    
		    if (SelectRowIndex[gridID] != null) 
		        selectedIndex = SelectRowIndex[gridID];
		    
		    //get a pointer to the tablerow
		    var row = obj.parentNode;
		    var oRows = document.getElementById(gridID).getElementsByTagName('tr');
		    var l = oRows.length;
		    
		    if (!e.ctrlKey && !e.shiftKey)
		    {
                var trRow;
                for (i=0; i<l; i++) { //i=2
		            trRow = oRows[i];
		            chk = getControlInGrid(trRow, 'INPUT', 'chk_Select');
		            if (chk!=null) {
		                chk.checked = false;
		                resetRowClass(trRow);
		            }
	            }
		    }
		    
		    if (e.shiftKey && selectedIndex >= 0)
		    {
		        if (selectedIndex > row.rowIndex)
		        {
		            var table = row.parentNode;
		            for (var i=row.rowIndex;i<selectedIndex;i++) {
		                trRow = oRows[i];
		                chk = getControlInGrid(trRow, 'INPUT', 'chk_Select');
		                if (chk!=null) {
		                    if(!chk.disabled)
		                    {
		                        chk.checked = true;
		                        trRow.className="SelectedRow";
		                    }
		                }
		            }
		        }
		        else
		        {
		            var table = row.parentNode;
		            for (var i=row.rowIndex;i>selectedIndex;i--) {
		                trRow = oRows[i];
		                chk = getControlInGrid(trRow, 'INPUT', 'chk_Select');
		                if (chk!=null) {
		                    if(!chk.disabled)
		                    {
		                        chk.checked = true;
		                        trRow.className="SelectedRow";
		                    }
		                }
		            }
		        }
		    }
		    else
		    {
		        trRow = oRows[row.rowIndex];
		        chk = getControlInGrid(trRow, 'INPUT', 'chk_Select');
		        if (chk!=null) {
		            if(!chk.disabled)
		            {
		                chk.checked = true;
		                if (chk.checked)
		                   row.className="SelectedRow";
		                else
		                   resetRowClass(row);
		            }
		        }
		    }
		    SelectRowIndex[gridID] = row.rowIndex
		}
	}
	function MouseOver(select, gridID, e)
	{
	    if(!e) e = window.event;//Added this for IE
	    
	    var obj = e.srcElement;
	    if(!obj) obj = e.target;//Added this for Firefox
		if (obj && obj.tagName=="TD") //this a table cell
		{
		    //get a pointer to the tablerow
		    var row = obj.parentNode;
		    chk = getControlInGrid(row, 'INPUT', 'chk_Select');
		    if (chk && !chk.disabled)
		    {
		        if (select && !chk.checked)
		            row.className="SelectedRow";
		        else
		        {
		            if (chk.checked)
		               row.className="SelectedRow";
		            else
		               resetRowClass(row);
		        }
		     }
		}
	}
	function SelectRow_OnClick(chkVal, row)
	{
	    if (chkVal)
	        row.className="SelectedRow";
	    else
	        resetRowClass(row);
	}
	function SelectAll_OnClick(gridID, chkVal, idVal, row)
	{
	    var oRows = document.getElementById(gridID).getElementsByTagName('tr');
        var l = oRows.length;
        var e;
        var s;
        if (idVal.indexOf ('CheckAll') != -1) {
	        var bSet; 
	        // Check if main checkbox is checked, then select or deselect datagrid checkboxes
	        if (chkVal == true) {
		        bSet = true;
	        } else {
		        bSet = false;
	        }
	        var trRow;
            for (i=0; i<l; i++) { //i=2
	            trRow = oRows[i];
	            chk = getControlInGrid(trRow, 'INPUT', 'chk_Select');
	            if (chk!=null) {
	                chk.checked = bSet;
	                if (bSet)
	                    trRow.className="SelectedRow";
	                else
	                    resetRowClass(trRow);
	            }
            }
        }
	}
	function SelectStart(e)
	{
	    if(!e) e = window.event;//Added this for IE
	    var obj = e.srcElement;
		if(!obj) obj = e.target;//Added this for Firefox
		if (obj && obj.tagName=="TD") //this a table cell
		    return false;
    }
    function MouseDown(e)
    {
          if(!e) return false;
          if(e.shiftKey || e.ctrlKey) return false;
    }
    function getControlInGrid(row, tagName, ctrlID)
    {
        var oInputs = row.getElementsByTagName(tagName);
        var l = oInputs.length;
        //var row;
        var inp;
        for (var i=0; i<l; i++) {
            inp = oInputs[i];
            if (inp.id.indexOf(ctrlID) > -1) {
                return inp;
            }
        }
        return null;
    }
    function resetRowClass(row) 
    {
        var ih = getControlInGrid(row, 'INPUT', 'RowClassName');
        if (ih != null) {
            row.className=ih.value;
        }
    }
    
    </script>
</head>
<body id="bod">
    <AdHoc:MainMenu ID="menu" runat="server" />
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server" />
    <div id="submenu">
        <AdHoc:NavMenu ID="subnav" runat="server" />
    </div>
        <asp:UpdatePanel ID="UPBct" runat="server" UpdateMode="Conditional" RenderMode="inline">
            <ContentTemplate>
                <AdHoc:BreadCrumbTrail ID="bct" runat="server" Key="Catalog" ParentLevels="1" />
            </ContentTemplate>
            <Triggers>
                <asp:AsyncPostBackTrigger ControlID="ddObjectID" EventName="SelectedIndexChanged" />
            </Triggers>
        </asp:UpdatePanel>

        <script type="text/javascript">
        Sys.WebForms.PageRequestManager.getInstance().add_beginRequest(BeginRequestHandler);
        Sys.WebForms.PageRequestManager.getInstance().add_endRequest(EndRequestHandler);
        function EndRequestHandler(sender, args) {
            $find('ahSavePopupBehavior').hide();
            RestorePopupPosition('ahModalPopupBehavior');
            noMessage = false;
        }
        function BeginRequestHandler(sender, args) {
            SavePopupPosition('ahModalPopupBehavior');
        }
        </script>

        <div class="divForm">
            <input type="hidden" id="ahParentID" name="ahParentID" runat="server" />
            
            <asp:UpdatePanel ID="UpdatePanel1" UpdateMode="conditional" runat="server" RenderMode="Inline">
                <ContentTemplate>
                    <input type="hidden" id="ahDirty" name="ahDirty" runat="server" />
                    <table id="tbDBConn" runat="server" class="tbTB">
                        <tr>
                            <td width="120px">
                                <asp:Localize ID="Localize9" runat="server" Text="<%$ Resources:LogiAdHoc, DatabaseConnection %>"></asp:Localize>
                            </td>
                            <td>
                                <asp:Label ID="lblDBConn" runat="server"></asp:Label>
                            </td>
                        </tr>
                    </table>
                    <table id="tbObjects" runat="server" class="tbTB">
                        <tr>
                            <td width="120px">
                                <asp:Localize ID="Localize2" runat="server" Text="Selected Catalog:" meta:resourcekey="Localize2"></asp:Localize></td>
                            <td>
                                <asp:DropDownList ID="ddObjectID" AutoPostBack="True" runat="server" meta:resourcekey="ddObjectIDResource1" />
                            </td>
                        </tr>
                    </table>
                    <table class="tbTB">
                        <tr id="trObjectName" runat="server">
                            <td width="120px" runat="server">
                                <asp:Localize ID="Localize3" runat="server" meta:resourcekey="LiteralResource2" Text="Catalog Name:"></asp:Localize></td>
                            <td runat="server">
                                <asp:TextBox ID="ObjectName" MaxLength="128" Width="250px" runat="server" />
                                <asp:RequiredFieldValidator ID="rtvObjectName" runat="server" ErrorMessage="Catalog Name is required."
                                    ControlToValidate="ObjectName" ValidationGroup="Catalog" meta:resourcekey="rtvObjectNameResource1">*</asp:RequiredFieldValidator>
                                <asp:CustomValidator ID="cvObjectNameValid" runat="server" ErrorMessage="This Catalog name already exists in the database. Please select another name."
                                    ControlToValidate="ObjectName" OnServerValidate="IsObjectNameValid" EnableClientScript="False"
                                    ValidationGroup="Catalog" meta:resourcekey="cvObjectNameValidResource1">*</asp:CustomValidator>
                            </td>
                        </tr>
                        <tr>
                            <td width="120px">
                                <asp:Localize ID="Localize6" runat="server" meta:resourcekey="LiteralResource6" Text="Friendly Name:"></asp:Localize></td>
                            <td>
                                <asp:TextBox ID="ObjectLabel" MaxLength="128" Width="250px" runat="server" meta:resourcekey="ObjectLabelResource1" />
                                <%--<asp:RequiredFieldValidator ID="rtvObjectLabel" ControlToValidate="ObjectLabel" ErrorMessage="Label is required."
                                    runat="server" ValidationGroup="Catalog" meta:resourcekey="rtvObjectLabelResource1">*</asp:RequiredFieldValidator>--%>
                                <asp:CustomValidator ID="cvObjectLabel" runat="server" ControlToValidate="ObjectLabel"
                                    EnableClientScript="False" ErrorMessage="Label cannot contain some special characters. Only alpha-numeric characters and _ are allowed in a label."
                                    meta:resourcekey="cvObjectLabelResource1" OnServerValidate="IsObjectLabelValid"
                                    ValidationGroup="Catalog">*</asp:CustomValidator>
                            </td>
                        </tr>
                        <tr id="trCat" runat="server">
                            <td width="120px">
                                <asp:Localize ID="Localize1" runat="server" Text="Data Objects in:" meta:resourcekey="LiteralResource1"></asp:Localize></td>
                            <td>
                                <asp:DropDownList ID="ddlCategories" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlCategories_SelectedIndexChanged" meta:resourcekey="ddlCategoriesResource1">
                                </asp:DropDownList>
                            </td>
                        </tr>
                    </table>
                    <br />
                    <table width="100%">
                        <tr width="100%">
                            <td></td>
                            <td>
                                <h2 id="GridHeader" runat="server">
                                    <asp:Localize ID="Localize7" runat="server" meta:resourcekey="LiteralResource7" Text="Available Columns"></asp:Localize>
                                </h2>
                            </td>
                            <td></td>
                            <td>
                                <h2 id="GridHeader2" runat="server">
                                    <asp:Localize ID="Localize5" runat="server" meta:resourcekey="LiteralResource5" Text="Catalog Columns"></asp:Localize>
                                </h2>
                            </td>
                        </tr>
                        <tr>
                            <td>
                            <table><tr><td>
                                <div id="divObjectsTreeView" runat="server" style="height: 415px; padding-right:30px; overflow: auto; vertical-align: top;">
                                    <cc1:DataSourceTreeView ID="tvDataSource" runat="server" 
                                        ShowCheckBoxes="Parent" ShowLines="true" ExpandDepth="0" 
                                        PopulateNodesFromClient="true" EnableClientScript="False">
                                    </cc1:DataSourceTreeView>
                                </div>
                            <asp:Button ID="btnFakeObjectSelected" runat="server" CssClass="NoShow" />
                            <asp:TextBox ID="txtFakeObjectSelected" runat="server" CssClass="NoShow"></asp:TextBox>
                            <asp:CustomValidator ID="cvObjectSelected" runat="server" ControlToValidate="txtFakeObjectSelected" ValidateEmptyText="true"
                                EnableClientScript="False" ErrorMessage="At least one object should be included in the catalog. Please select the object you wish to include as part of this catalog."
                                meta:resourcekey="cvObjectSelectedResource1" OnServerValidate="IsObjectSelected" ValidationGroup="Catalog">*</asp:CustomValidator>
                            </td></tr></table>
                            </td>
                            <td id="tdAvailableColumns" runat="server">
                                <table onselectstart="javascript: return SelectStart(event);" onmousedown="javascript: return MouseDown(event);" width="100%" ><tr width="100%"><td>
                                    <div id="data_main2">
                                        <div id="divAvailableColumns" runat="server" style="height: 415px; padding-right:20px; overflow: auto; vertical-align: top; min-width:100px;">
                                             <asp:GridView ID="grdAvailableColumns" runat="server" AllowPaging="false" OnRowDataBound="OnAvailableDataBoundHandler" 
                                                CssClass="grid" AutoGenerateColumns="False" meta:resourcekey="grdMainResource1">
                                                <Columns>
                                                    <asp:TemplateField HeaderText="Select" meta:resourcekey="TemplateFieldResource1">
                                                        <HeaderStyle Width="30px"></HeaderStyle>
                                                        <HeaderTemplate>
                                                            <asp:Label ID="lblChkAll" runat="server" AssociatedControlID="CheckAll" Text="<%$ Resources:LogiAdHoc, SelectDeselectAll %>" CssClass="NoShow"></asp:Label>
                                                            <asp:CheckBox ID="CheckAll" runat="server" ToolTip="<%$ Resources:LogiAdHoc, SelectDeselectAll %>"
                                                                meta:resourcekey="CheckAllResource1" />
                                                        </HeaderTemplate>
                                                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                                        <ItemTemplate>
                                                            <input type="hidden" id="ihObjectKey" runat="server" />
                                                            <input type="hidden" id="ihColumnID" runat="server" />
                                                            <input type="hidden" id="RowClassName" runat="server" />
                                                            <asp:CheckBox ID="chk_Select" runat="server" ToolTip="<%$ Resources:LogiAdHoc, SelectDeselect %>" />
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Column" meta:resourcekey="TemplateFieldResource2">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblColumnName" runat="server"></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                </Columns>
                                                <HeaderStyle CssClass="gridheader" />
                                                <RowStyle CssClass="gridrow" />
                                                <AlternatingRowStyle CssClass="gridalternaterow" />
                                            </asp:GridView>
                                        </div>
                                    </div>
                                </td></tr></table>
                            </td>
                            <td id="tdButtons" runat="server" style="padding:0 10px 0 10px;">
                                <asp:ImageButton CssClass="mslbutton" ID="moveright" OnClick="MoveRight_Click" ImageUrl="../ahImages/arrowRight.gif"
                                    CausesValidation="False" runat="server" meta:resourcekey="moverightResource1" />
                                <asp:ImageButton CssClass="mslbutton" ID="moveleft" OnClick="MoveLeft_Click" ImageUrl="../ahImages/arrowLeft.gif"
                                    CausesValidation="False" runat="server" meta:resourcekey="moveleftResource1" />
                            </td>
                            <td id="tdAssignedColumns" runat="server">
                                <table onselectstart="javascript: return SelectStart(event);" onmousedown="javascript: return MouseDown(event);" width="100%" ><tr width="100%"><td>
                                    <div id="data_main">
                                        <div id="divColumns" runat="server" style="height: 415px; padding-right:20px; overflow: auto; vertical-align: top; min-width:100px;">
                                            <asp:GridView ID="grdMain" runat="server" AllowPaging="True" OnRowDataBound="OnItemDataBoundHandler" 
                                                CssClass="grid" AutoGenerateColumns="False" meta:resourcekey="grdMainResource1">
                                                <Columns>
                                                    <asp:TemplateField HeaderText="Include" meta:resourcekey="TemplateFieldResource3">
                                                        <HeaderStyle Width="30px"></HeaderStyle>
                                                        <HeaderTemplate>
                                                            <asp:Label ID="lblChkAll" runat="server" AssociatedControlID="CheckAll" Text="<%$ Resources:LogiAdHoc, SelectDeselectAll %>" CssClass="NoShow"></asp:Label>
                                                            <asp:CheckBox ID="CheckAll" runat="server" ToolTip="<%$ Resources:LogiAdHoc, SelectDeselectAll %>"
                                                                meta:resourcekey="CheckAllResource1" />
                                                        </HeaderTemplate>
                                                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                                        <ItemTemplate>
                                                            <input type="hidden" id="ihColumnID" runat="server" />
                                                            <input type="hidden" id="RowClassName" runat="server" />
                                                            <asp:CheckBox ID="chk_Select" runat="server" ToolTip="<%$ Resources:LogiAdHoc, SelectDeselect %>" />
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Column" meta:resourcekey="TemplateFieldResource4">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblColumnName" runat="server"></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Column Name" meta:resourcekey="TemplateFieldResource5">
                                                        <ItemTemplate>
                                                            <asp:TextBox ID="txtColumnName" runat="server" Columns="30" MaxLength="100"></asp:TextBox>
                                                            <asp:RequiredFieldValidator ID="rtvColumnName" runat="server" ErrorMessage="Column Name is required."
                                                                ControlToValidate="txtColumnName" ValidationGroup="Catalog" meta:resourcekey="rtvColumnNameResource1">*</asp:RequiredFieldValidator>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Friendly Name" meta:resourcekey="TemplateFieldResource6">
                                                        <ItemTemplate>
                                                            <asp:TextBox ID="txtColumnLabel" runat="server" Columns="30" MaxLength="100"></asp:TextBox>
                                                            <asp:RequiredFieldValidator ID="rtvColumnLabel" runat="server" ErrorMessage="Friendly Name is required."
                                                                ControlToValidate="txtColumnLabel" ValidationGroup="Catalog" meta:resourcekey="rtvColumnLabelResource1">*</asp:RequiredFieldValidator>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                </Columns>
                                                <HeaderStyle CssClass="gridheader" />
                                                <RowStyle CssClass="gridrow" />
                                                <AlternatingRowStyle CssClass="gridalternaterow" />
                                            </asp:GridView>
                                        </div>
                                    </div>
                                    <asp:TextBox ID="txtFakeColumnsSelected" runat="server" CssClass="NoShow"></asp:TextBox>
                                    <asp:CustomValidator ID="cvColumnsSelected" runat="server" ControlToValidate="txtFakeColumnsSelected" ValidateEmptyText="true"
                                        EnableClientScript="False" ErrorMessage="At least one column should be included in the catalog. Please select the columns you wish to include as part of this catalog."
                                        meta:resourcekey="cvColumnsSelectedResource1" OnServerValidate="IsColumnSelected" ValidationGroup="Catalog">*</asp:CustomValidator>
                                </td></tr></table>
                            </td>
                        </tr>
                    </table>
                    <ul class="validation_error" id="ErrorList" runat="server">
                        <li>
                            <asp:Label ID="lblValidationMessage" runat="server" meta:resourcekey="lblValidationMessageResource1"></asp:Label></li>
                    </ul>
                    
                    <asp:Button runat="server" ID="Button1" Style="display: none" />
                    <ajaxToolkit:ModalPopupExtender runat="server" ID="ahModalPopup" BehaviorID="ahModalPopupBehavior"
                        TargetControlID="Button1" PopupControlID="ahPopup" BackgroundCssClass="modalBackground"
                        DropShadow="false" PopupDragHandleControlID="pnlDragHandle" RepositionMode="None">
                    </ajaxToolkit:ModalPopupExtender>
                    <asp:Panel runat="server" CssClass="modalPopup" ID="ahPopup" Style="display:none; width:450;">
                        <asp:Panel ID="pnlDragHandle" runat="server" Style="cursor: hand;">
                        <div class="modalPopupHandle">
                            <table cellpadding="0" cellspacing="0" style="width: 100%"><tr><td>
                                    <asp:Localize ID="PopupHeader" runat="server" meta:resourcekey="saveAsResource" Text="Save As New Catalog"></asp:Localize>
                                </td>
                                <td style="width: 20px;">
                                    <asp:ImageButton ID="imgClosePopup" runat="server" 
                                        OnClick="imgClosePopup_Click" CausesValidation="false"
                                        SkinID="imgbClose" ImageUrl="../ahImages/remove.gif" 
                                        AlternateText="<%$ Resources:LogiAdHoc, CloseWindow %>" />
                            </td></tr></table>
                        </div>
                        </asp:Panel>
                        <div class="modalDiv">
                        <asp:UpdatePanel ID="upPopup" runat="server" UpdateMode="Conditional">
                            <ContentTemplate>
                                <table>
                                    <tr>
                                        <td>
                                            <asp:Localize ID="Localize8" runat="server" meta:resourcekey="LiteralResource2" Text="Catalog Name:"></asp:Localize>
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtObjectName" MaxLength="128" Width="250px" runat="server" />
                                            <asp:RequiredFieldValidator ID="rfvtxtPermissionName" runat="server" ErrorMessage="Catalog Name is required."
                                                ControlToValidate="txtObjectName" ValidationGroup="SaveAs" meta:resourcekey="rtvObjectNameResource1">*</asp:RequiredFieldValidator>
                                            <asp:CustomValidator ID="cvtxtObjectName" runat="server" ErrorMessage="This Catalog name already exists in the database. Please select another name."
                                                ControlToValidate="txtObjectName" ValidationGroup="SaveAs" OnServerValidate="IsSaveAsObjectNameValid"
                                                EnableClientScript="false" meta:resourcekey="cvObjectNameValidResource1">*</asp:CustomValidator>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="120px">
                                            <asp:Localize ID="Localize4" runat="server" meta:resourcekey="LiteralResource6" Text="Friendly Name:"></asp:Localize></td>
                                        <td>
                                            <asp:TextBox ID="txtSaveAsObjectLabel" MaxLength="128" Width="250px" runat="server" meta:resourcekey="ObjectLabelResource1" />
                                            <%--<asp:RequiredFieldValidator ID="rtvObjectLabel" ControlToValidate="ObjectLabel" ErrorMessage="Label is required."
                                                runat="server" ValidationGroup="Catalog" meta:resourcekey="rtvObjectLabelResource1">*</asp:RequiredFieldValidator>--%>
                                            <asp:CustomValidator ID="cvtxtSaveAsObjectLabel" runat="server" ControlToValidate="txtSaveAsObjectLabel"
                                                EnableClientScript="False" ErrorMessage="Label cannot contain some special characters. Only alpha-numeric characters and _ are allowed in a label."
                                                meta:resourcekey="cvObjectLabelResource1" OnServerValidate="IsSaveAsObjectLabelValid"
                                                ValidationGroup="SaveAs">*</asp:CustomValidator>
                                        </td>
                                    </tr>
                                </table>
                                <br />
                                <table>
                                    <tr>
                                        <td>
                                            <AdHoc:LogiButton ID="btnSPA_OK" runat="server" OnClick="OK_SPA" ValidationGroup="SaveAs"
                                                ToolTip="<%$ Resources:LogiAdHoc, SaveTooltip1 %>" Text="<%$ Resources:LogiAdHoc, OKWithSpaces %>"
                                                UseSubmitBehavior="false" />
                                            <AdHoc:LogiButton ID="btnSPA_Cancel" runat="server" OnClick="Cancel_SPA"
                                                ToolTip="<%$ Resources:LogiAdHoc, CancelTooltip1 %>" Text="<%$ Resources:LogiAdHoc, Cancel %>"
                                                CausesValidation="false" />
                                        </td>
                                    </tr>
                                </table>
                                <asp:ValidationSummary ID="ValidationSummary1" ValidationGroup="SaveAs" runat="server"
                                    meta:resourcekey="vsummaryResource2" />
                            </ContentTemplate>
                        </asp:UpdatePanel>
                        </div>
                    </asp:Panel>

                    <div id="divSaveAnimation">
                        <asp:Button runat="server" ID="btnSaveAnimation" Style="display: none" />
                        <ajaxToolkit:ModalPopupExtender runat="server" ID="ahSavePopup" BehaviorID="ahSavePopupBehavior"
                            TargetControlID="btnSaveAnimation" PopupControlID="pnlSaving" BackgroundCssClass="modalBackground"
                            DropShadow="False">
                        </ajaxToolkit:ModalPopupExtender>
                        <asp:Panel id="pnlSaving" CssClass="savePopup" runat="server" style="display:none;">
                            <table><tr><td>
                            <asp:Image ID="imgSave" runat="server" SkinID="imgSaving" EnableViewState="false" AlternateText="<%$ Resources:LogiAdHoc, SaveImageAltText %>" />
                            </td>
                            <td valign="middle">
                            <asp:Label ID="lblSaveText" CssClass="lblSavePopup" runat="server" EnableViewState="false" Text="<%$ Resources:LogiAdHoc, PleaseWait %>"/>
                            </td></tr></table>
                        </asp:Panel>
                    </div>
                    
                    <div id="divButtons" class="divButtons">
                        <AdHoc:LogiButton ID="btnSave" runat="server" OnClick="Save_OnClick"
                            UseSubmitBehavior="false" ToolTip="<%$ Resources:LogiAdHoc, SaveTooltip1 %>"
                            Text="<%$ Resources:LogiAdHoc, Save %>" ValidationGroup="Catalog" />
                        <AdHoc:LogiButton ID="btnSaveAs" runat="server" OnClick="SaveVVAs" ValidationGroup="Catalog"
                            ToolTip="<%$ Resources:LogiAdHoc, SaveTooltip1 %>" Text="<%$ Resources:LogiAdHoc, SaveAs %>" />
                        <AdHoc:LogiButton ID="btnCancel" runat="server" OnClick="Cancel_OnClick"
                            ToolTip="Click to cancel your unsaved changes and return to the previous page."
                            Text="Back to Catalogs" CausesValidation="False" meta:resourcekey="btnCancelResource1" />
                        <asp:ValidationSummary ID="vsummary" ValidationGroup="Catalog" runat="server" meta:resourcekey="vsummaryResource1" />
                        <%--<asp:Label ID="lblSemicolon" runat="server" OnPreRender="lblSemicolon_PreRender" />--%>
                    </div>
<br />
                    
                </ContentTemplate>
                <Triggers>
                    <asp:AsyncPostBackTrigger ControlID="ddObjectID" EventName="SelectedIndexChanged" />
                    <asp:PostBackTrigger ControlID="btnCancel" />
                    <asp:PostBackTrigger ControlID="btnSave" />
                </Triggers>
            </asp:UpdatePanel>
        </div>
    </form>
</body>
</html>
