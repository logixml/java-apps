<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="Configuration.aspx.vb" Inherits="LogiAdHoc.ahConfiguration_Configuration" Culture="auto" meta:resourcekey="PageResource1" UICulture="auto" %>

<%@ Register TagPrefix="AdHoc" TagName="MainMenu" Src="~/ahControls/MainMenu.ascx" %>
<%@ Register TagPrefix="AdHoc" TagName="NavMenu" Src="~/ahControls/NavMenu.ascx" %>
<%@ Register TagPrefix="AdHoc" TagName="BreadCrumbTrail" Src="~/ahControls/BreadCrumbTrail.ascx" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Configuration</title>
    <link rel="shortcut icon" href="../ahImages/flav.ico" />
</head>
<body id="bod">
    <AdHoc:MainMenu ID="menu" runat="server" />
        
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server" />
        
        <div id="submenu">
            <AdHoc:NavMenu ID="subnav" runat="server" />
        </div>
        
        <AdHoc:BreadCrumbTrail ID="bct" runat="server" Key="Configuration" />
        
        <div class="divForm" align="center" >
        
        <table width="100%" cellspacing="10px">
            <tr>
                <td id="tdUC" runat="server" class="tdConfig" valign="top" align="left" width="33%">
                    <div class="divConfig">
                    <asp:Localize ID="Localize3" runat="server" meta:resourcekey="LiteralResource1" Text="User Configuration"></asp:Localize>
                    </div>

                    <p id="pUC" runat="server">
                    Logi Ad Hoc gives administrators the ability to create and manage the following user configuration features:
                    </p>
                    <ul>
                        <li id="liUC_UG" runat="server"><asp:Localize ID="Localize4" runat="server" Text="<%$ Resources:LogiAdHoc, Menu_UserGroups %>"></asp:Localize></li>
                        <li id="liUC_R" runat="server"><asp:Localize ID="Localize5" runat="server" Text="<%$ Resources:LogiAdHoc, Menu_Roles %>"></asp:Localize></li>
                        <li id="liUC_U" runat="server"><asp:Localize ID="Localize6" runat="server" Text="<%$ Resources:LogiAdHoc, Menu_Users %>"></asp:Localize></li>
                        <li id="liUC_P" runat="server"><asp:Localize ID="Localize7" runat="server" Text="<%$ Resources:LogiAdHoc, Menu_Permissions %>"></asp:Localize></li>
                    </ul>

                    <div id="divUG" runat="server">
                        <h2><asp:Localize ID="Localize8" runat="server" Text="<%$ Resources:LogiAdHoc, Menu_UserGroups %>"></asp:Localize>:</h2>

                        <p><asp:Localize ID="Localize9" runat="server" meta:resourcekey="LiteralResource2" Text="I want to ..."></asp:Localize></p>
                        
                        <ul>
                        <li><a href="UserGroup.aspx"><asp:Localize ID="Localize10" runat="server" meta:resourcekey="LiteralResource3" Text="Create a new organization."></asp:Localize></a></li>
                        <li><a href="UserGroupList.aspx"><asp:Localize ID="Localize11" runat="server" meta:resourcekey="LiteralResource4" Text="Manage my existing organizations."></asp:Localize></a></li>
                        <li><a href="SessionParameters.aspx"><asp:Localize ID="Localize12" runat="server" meta:resourcekey="LiteralResource5" Text="Create and manage session parameters for organizations."></asp:Localize></a></li>
                        <li><a href="UserGroupSessions.aspx"><asp:Localize ID="Localize13" runat="server" meta:resourcekey="LiteralResource6" Text="Define session parameters for organizations."></asp:Localize></a></li>
                        </ul>
                    </div>

                    <div id="divR" runat="server">
                        <h2><asp:Localize ID="Localize26" runat="server" Text="<%$ Resources:LogiAdHoc, Menu_Roles %>"></asp:Localize>:</h2>

                        <p><asp:Localize ID="Localize16" runat="server" meta:resourcekey="LiteralResource2" Text="I want to ..."></asp:Localize>.</p>

                        <ul>
                            <li><a href="Role.aspx"><asp:Localize ID="Localize27" runat="server" meta:resourcekey="LiteralResource9" Text="Create a new user role."></asp:Localize></a></li>
                            <li><a href="RoleList.aspx"><asp:Localize ID="Localize29" runat="server" meta:resourcekey="LiteralResource10" Text="Manage my existing user roles."></asp:Localize></a></li>
                            <li id="liRolePermissions" runat="server"> <a href="RolePermissions.aspx"><asp:Localize ID="Localize28" runat="server" meta:resourcekey="LiteralResource11" Text="Configure object-level access to my user roles."></asp:Localize></a> </li>
                        </ul>
                    </div>
                    
                    <div id="divU" runat="server">
                        <h2><asp:Localize ID="Localize30" runat="server" Text="<%$ Resources:LogiAdHoc, Menu_Users %>"></asp:Localize>:</h2>

                        <p><asp:Localize ID="Localize17" runat="server" meta:resourcekey="LiteralResource2" Text="I want to ..."></asp:Localize></p>

                        <ul>
                            <li><a href="User.aspx"><asp:Localize ID="Localize31" runat="server" meta:resourcekey="LiteralResource12" Text="Create a new user account."></asp:Localize></a></li>
                            <li><a href="UserList.aspx"><asp:Localize ID="Localize32" runat="server" meta:resourcekey="LiteralResource13" Text="Modify an existing user account."></asp:Localize></a></li>
                        </ul> 
                    </div>

                    <div id="divP" runat="server">
                        <h2><asp:Localize ID="Localize14" runat="server" Text="<%$ Resources:LogiAdHoc, Menu_Permissions %>"></asp:Localize>:</h2>

                        <p><asp:Localize ID="Localize15" runat="server" meta:resourcekey="LiteralResource2" Text="I want to ..."></asp:Localize></p>

                        <ul>
                            <li><a href="Permission.aspx"><asp:Localize ID="Localize24" runat="server" meta:resourcekey="LiteralResource7" Text="Create a new permission."></asp:Localize></a></li>
                            <li><a href="PermissionList.aspx"><asp:Localize ID="Localize25" runat="server" meta:resourcekey="LiteralResource8" Text="Manage my existing permissions."></asp:Localize></a></li>
                        </ul>
                    </div>
                </td>
                <td id="tdDC" runat="server" class="tdConfig" valign="top" align="left" width="33%">
                    <div class="divConfig">
                    <asp:Localize ID="Localize33" runat="server" meta:resourcekey="LiteralResource14" Text="Database Object Configuration"></asp:Localize>
                    </div>

                    <p id="pDC" runat="server">
                    Logi Ad Hoc gives administrators the ability to manage the following database object configuration features:
                    </p>
                    <ul>
                        <li id="liDC_DO" runat="server"><asp:Localize ID="Localize34" runat="server" Text="<%$ Resources:LogiAdHoc, Menu_DataObjects %>"></asp:Localize></li>
                        <li id="liDC_VV" runat="server"><asp:Localize ID="Localize35" runat="server" Text="<%$ Resources:LogiAdHoc, Menu_VirtualViews %>"></asp:Localize></li>
                        <li id="liDC_Rel" runat="server"><asp:Localize ID="Localize36" runat="server" Text="<%$ Resources:LogiAdHoc, Menu_Relationships %>"></asp:Localize></li>
                    </ul>
                    
                    <div id="divDO" runat="server">
                        <h2><asp:Localize ID="Localize37" runat="server" Text="<%$ Resources:LogiAdHoc, Menu_DataObjects %>"></asp:Localize>:</h2>

                        <p><asp:Localize ID="Localize18" runat="server" meta:resourcekey="LiteralResource2" Text="I want to ..."></asp:Localize></p>
                        
                        <ul>
                            <li><a href="ObjectList.aspx"><asp:Localize ID="Localize38" runat="server" meta:resourcekey="LiteralResource15" Text="View a list of database objects."></asp:Localize></a></li>
                            <li><a href="ObjectInfo.aspx"><asp:Localize ID="Localize39" runat="server" meta:resourcekey="LiteralResource16" Text="Configure data object columns."></asp:Localize></a></li>
                            <li><a href="Categories.aspx"><asp:Localize ID="Localize70" runat="server" meta:resourcekey="LiteralResource70" Text="Group data objects in various categories."></asp:Localize></a></li>
                            <li id="liDC_Rel1" runat="server"><a href="RelationshipList.aspx?ObjectID=0"><asp:Localize ID="Localize40" runat="server" meta:resourcekey="LiteralResource17" Text="Create and manage relationships between data objects."></asp:Localize></a></li>
                            <li id="liDC_OPerm" runat="server"><a href="ObjectPermissions.aspx"><asp:Localize ID="Localize41" runat="server" meta:resourcekey="LiteralResource18" Text="Configure object-level access rights."></asp:Localize></a></li>
                            <li id="liUC_SP" runat="server"><a href="SessionParameters.aspx"><asp:Localize ID="Localize42" runat="server" meta:resourcekey="LiteralResource19" Text="Create and manage session parameters for record-level access."></asp:Localize></a></li>
                            <li id="liDC_OParams" runat="server"><a href="ObjectParameters.aspx"><asp:Localize ID="Localize43" runat="server" meta:resourcekey="LiteralResource20" Text="Define fixed parameters for data objects."></asp:Localize></a></li>
                            <li id="liDC_OL" runat="server"><a href="ObjectLinks.aspx"><asp:Localize ID="Localize44" runat="server" meta:resourcekey="LiteralResource21" Text="Configure links."></asp:Localize></a></li>
                        </ul>
                    </div>

                    <div id="divVV" runat="server">
                        <h2><asp:Localize ID="Localize45" runat="server" Text="<%$ Resources:LogiAdHoc, Menu_VirtualViews %>"></asp:Localize>:</h2>

                        <p><asp:Localize ID="Localize19" runat="server" meta:resourcekey="LiteralResource2" Text="I want to ..."></asp:Localize></p>

                        <ul>
                            <li><a href="VirtualView.aspx"><asp:Localize ID="Localize46" runat="server" meta:resourcekey="LiteralResource22" Text="Create a new virtual view."></asp:Localize></a></li>
                            <li><a href="VirtualViewList.aspx"><asp:Localize ID="Localize47" runat="server" meta:resourcekey="LiteralResource23" Text="Manage my existing virtual views."></asp:Localize></a></li>
                        </ul>
                    </div>

                    <div id="divRel" runat="server">
                        <h2><asp:Localize ID="Localize48" runat="server" Text="<%$ Resources:LogiAdHoc, Menu_Relationships %>"></asp:Localize>:</h2>

                        <p><asp:Localize ID="Localize20" runat="server" meta:resourcekey="LiteralResource2" Text="I want to ..."></asp:Localize></p>

                        <ul>
                            <li><a href="Relationship.aspx?ObjectID=0"><asp:Localize ID="Localize49" runat="server" meta:resourcekey="LiteralResource24" Text="Create a new data object relationship."></asp:Localize></a></li>
                            <li><a href="RelationshipList.aspx?ObjectID=0"><asp:Localize ID="Localize50" runat="server" meta:resourcekey="LiteralResource25" Text="Manage my existing data object relationships."></asp:Localize></a></li>
                        </ul>
                    </div>
                </td>
                <td id="tdRC" runat="server" class="tdConfig" valign="top" align="left" width="33%">
                    <div class="divConfig">
                    <asp:Localize ID="Localize51" runat="server" meta:resourcekey="LiteralResource26" Text="Report Configuration"></asp:Localize>
                    </div>

                    <p id="pRC" runat="server">
                    Logi Ad Hoc gives administrators the ability to manage the following report configuration features:
                    </p>
                    <ul>
                        <li id="liRC_RW" runat="server">
                        <asp:Localize ID="Localize52" runat="server" meta:resourcekey="LiteralResource27" Text="Report Builder"></asp:Localize>
                            <ul>
                                <li id="liRC_CF" runat="server"><asp:Localize ID="Localize53" runat="server" Text="<%$ Resources:LogiAdHoc, Menu_CascadingFilters %>"></asp:Localize></li>
                                <li id="liRC_PS" runat="server"><asp:Localize ID="Localize54" runat="server" Text="<%$ Resources:LogiAdHoc, Menu_PresentationStyles %>"></asp:Localize></li>
                                <li id="liRC_DF" runat="server"><asp:Localize ID="Localize71" runat="server" Text="<%$ Resources:LogiAdHoc, Menu_DataFormats %>"></asp:Localize></li>
                            </ul>
                        </li>
                        <li id="liRC_A" runat="server"><asp:Localize ID="Localize55" runat="server" Text="<%$ Resources:LogiAdHoc, Menu_Archives %>"></asp:Localize></li>
                        <li id="liRC_S" runat="server"><asp:Localize ID="Localize56" runat="server" Text="<%$ Resources:LogiAdHoc, Menu_ScheduledReports %>"></asp:Localize></li>
                        <li id="liRC_RS" runat="server">
                        <asp:Localize ID="Localize1" runat="server" Text="<%$ Resources:LogiAdHoc, ReportSettings %>"></asp:Localize>
                        </li>
                        <li id="liRC_AS" runat="server">
                        <asp:Localize ID="Localize66" runat="server" Text="<%$ Resources:LogiAdHoc, Menu_ApplicationConfiguration %>"></asp:Localize>
                        </li>
                    </ul>
                    
                    <div id="divRW" runat="server">
                        <h2><asp:Localize ID="Localize57" runat="server" meta:resourcekey="LiteralResource27" Text="Report Builder"></asp:Localize>:</h2>

                        <p><asp:Localize ID="Localize21" runat="server" meta:resourcekey="LiteralResource2" Text="I want to ..."></asp:Localize></p>

                        <ul>
                            <li id="liRC_CF1" runat="server"><a href="CascadingFilter.aspx"><asp:Localize ID="Localize58" runat="server" meta:resourcekey="LiteralResource28" Text="Create a new cascading filter."></asp:Localize></a></li>
                            <li id="liRC_CF2" runat="server"><a href="CascadingFilterList.aspx"><asp:Localize ID="Localize59" runat="server" meta:resourcekey="LiteralResource29" Text="Manage my existing cascading filters."></asp:Localize></a></li>
                            <li id="liRC_PS1" runat="server"><a href="Appearances.aspx"><asp:Localize ID="Localize60" runat="server" meta:resourcekey="LiteralResource30" Text="Create or manage presentation styles."></asp:Localize></a></li>
                            <li id="liRC_PS2" runat="server"><a href="DataFormats.aspx"><asp:Localize ID="Localize61" runat="server" meta:resourcekey="LiteralResource31" Text="Create or manage data formats."></asp:Localize></a></li>
                        </ul>
                    </div>
                    
                    <div id="divSA" runat="server">
                        <h2><asp:Localize ID="Localize65" runat="server" meta:resourcekey="LiteralResource33" Text="Schedules & Archives:"></asp:Localize></h2>

                        <p><asp:Localize ID="Localize23" runat="server" meta:resourcekey="LiteralResource2" Text="I want to ..."></asp:Localize></p>

                        <ul>
                            <li id="liRC_S1" runat="server"><a href="../ahReport/ScheduledReports.aspx"><asp:Localize ID="Localize63" runat="server" meta:resourcekey="LiteralResource34" Text="Manage the report schedules."></asp:Localize></a></li>
                            <li id="liRC_A1" runat="server"><a href="../ahReport/ArchivedReports.aspx"><asp:Localize ID="Localize64" runat="server" meta:resourcekey="LiteralResource35" Text="Manage the report archives."></asp:Localize></a></li>
                        </ul>
                    </div>
                    
                    <div id="divRS" runat="server">
                        <h2>
                        <asp:Localize ID="Localize2" runat="server" Text="<%$ Resources:LogiAdHoc, ReportSettings %>"></asp:Localize>:
                        </h2>

                        <p><asp:Localize ID="Localize22" runat="server" meta:resourcekey="LiteralResource2" Text="I want to ..."></asp:Localize></p>

                        <ul>
                            <li><a href="Settings.aspx"><asp:Localize ID="Localize62" runat="server" meta:resourcekey="LiteralResource32" Text="Configure general settings for all reports."></asp:Localize></a></li>
                        </ul>
                    </div>
                    
                    <div id="divAS" runat="server">
                        <h2>
                        <asp:Localize ID="Localize67" runat="server" Text="<%$ Resources:LogiAdHoc, Menu_ApplicationConfiguration %>"></asp:Localize>:
                        </h2>

                        <p><asp:Localize ID="Localize68" runat="server" meta:resourcekey="LiteralResource2" Text="I want to ..."></asp:Localize></p>

                        <ul>
                            <li><a href="ApplicationConfiguration.aspx"><asp:Localize ID="Localize69" runat="server" meta:resourcekey="LiteralResource36" Text="Configure general application settings."></asp:Localize></a></li>
                            <li id="liAS_SP" runat="server"><a href="SessionParameters.aspx"><asp:Localize ID="Localize72" runat="server" meta:resourcekey="LiteralResource72" Text="Create and manage session parameters."></asp:Localize></a></li>
                        </ul>
                    </div>
                </td>
            </tr>
        </table>
        </div>        
    </form>
</body>
</html>
