<%@ Page Language="vb" AutoEventWireup="false" Codebehind="VirtualView.aspx.vb" Inherits="LogiAdHoc.ahConfiguration_VirtualView" Culture="auto" meta:resourcekey="PageResource1" UICulture="auto" %>

<%@ Register TagPrefix="AdHoc" TagName="MainMenu" Src="~/ahControls/MainMenu.ascx" %>
<%@ Register TagPrefix="AdHoc" TagName="NavMenu" Src="~/ahControls/NavMenu.ascx" %>
<%@ Register TagPrefix="AdHoc" TagName="BreadCrumbTrail" Src="~/ahControls/BreadCrumbTrail.ascx" %>
<%@ Register TagPrefix="AdHoc" TagName="PagingControl" Src="../ahControls/PagingControl.ascx" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Virtual View</title>
    <link rel="shortcut icon" href="../ahImages/flav.ico" />
    <script language="JavaScript" type="text/javascript" src="../ahScripts/ahChange.js"></script>
    <script type="text/javascript">
        window.onbeforeunload = AlertOnExit;
    </script>
</head>
<body id="bod">
    <AdHoc:MainMenu ID="menu" runat="server" />
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server" />
        <div id="submenu">
            <AdHoc:NavMenu ID="subnav" runat="server" />
        </div>
            
        <asp:UpdatePanel ID="UPBct" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <AdHoc:BreadCrumbTrail ID="bct" runat="server" Key="VirtualView" ParentLevels="1" />
            </ContentTemplate>
            <Triggers>
                <asp:AsyncPostBackTrigger ControlID="ddObjectID" EventName="SelectedIndexChanged" />
            </Triggers>
        </asp:UpdatePanel>

        <script type="text/javascript">
        Sys.WebForms.PageRequestManager.getInstance().add_beginRequest(BeginRequestHandler);
        Sys.WebForms.PageRequestManager.getInstance().add_endRequest(EndRequestHandler);
        function EndRequestHandler(sender, args) {
            $find('ahSavePopupBehavior').hide();
            RestorePopupPosition('ahModalPopupBehavior');
        }
        function BeginRequestHandler(sender, args) {
            SavePopupPosition('ahModalPopupBehavior');
        }
        </script>

        <div class="divForm">
            <input type="hidden" id="ahDirty" name="ahDirty" runat="server" />
            <input type="hidden" id="ahParentID" name="ahParentID" runat="server" />
            <asp:UpdatePanel ID="UpdatePanel1" UpdateMode="conditional" runat="server" RenderMode="Inline">
                <ContentTemplate>
                    
                    
                    <table id="tbDBConn" runat="server" class="tbTB">
                        <tr>
                            <td width="120px">
                                <asp:Localize ID="Localize13" runat="server" Text="<%$ Resources:LogiAdHoc, DatabaseConnection %>"></asp:Localize>
                            </td>
                            <td>
                                <asp:Label ID="lblDBConn" runat="server"></asp:Label>
                            </td>
                        </tr>
                    </table>    
                    <table id="tbObjects" runat="server" class="tbTB">
                        <tr>
                            <td width="120px">
                                <asp:Localize ID="Localize2" runat="server" Text="Selected Virtual View:" meta:resourcekey="Localize2"></asp:Localize></td>
                            <td>
                                <asp:DropDownList ID="ddObjectID" runat="server" meta:resourcekey="ddObjectIDResource1" AutoPostBack="True" />
                            </td>
                        </tr>
                    </table>
                    
                    <table class="tbTB">
                        <tr id="trObjectName" runat="server">
                            <td width="120px" runat="server">
                                <asp:Localize ID="Localize3" runat="server" meta:resourcekey="LiteralResource2" Text="Virtual View Name:"></asp:Localize></td>
                            <td runat="server">
                                <asp:TextBox ID="ObjectName" MaxLength="128" Width="250px" runat="server" />
                                <asp:RequiredFieldValidator ID="rtvObjectName" runat="server" ErrorMessage="Virtual View Name is required."
                                    ControlToValidate="ObjectName" ValidationGroup="VirtualView" meta:resourcekey="rtvObjectNameResource1">*</asp:RequiredFieldValidator>
                                <asp:CustomValidator ID="cvObjectNameValid" runat="server" ErrorMessage="This Virtual View name already exists in the database. Please select another name."
                                    ControlToValidate="ObjectName" OnServerValidate="IsObjectNameValid" EnableClientScript="False"
                                    ValidationGroup="VirtualView" meta:resourcekey="cvObjectNameValidResource1">*</asp:CustomValidator>
                            </td>
                        </tr>
                        <tr>
                            <td width="120px">
                                <asp:Localize ID="Localize1" runat="server" meta:resourcekey="LiteralResource11" Text="Friendly Name:"></asp:Localize></td>
                            <td>
                                <asp:TextBox ID="ObjectLabel" MaxLength="128" Width="250px" runat="server" meta:resourcekey="ObjectLabelResource1" />
                                <%--<asp:RequiredFieldValidator ID="rtvObjectLabel" ControlToValidate="ObjectLabel" ErrorMessage="Label is required."
                                    runat="server" ValidationGroup="Catalog" meta:resourcekey="rtvObjectLabelResource1">*</asp:RequiredFieldValidator>--%>
                                <asp:CustomValidator ID="cvObjectLabel" runat="server" ControlToValidate="ObjectLabel"
                                    EnableClientScript="False" ErrorMessage="Label cannot contain some special characters. Only alpha-numeric characters and _ are allowed in a label."
                                    meta:resourcekey="cvObjectLabelResource1" OnServerValidate="IsObjectLabelValid"
                                    ValidationGroup="VirtualView">*</asp:CustomValidator>
                            </td>
                        </tr>                        
                        <%--<tr>
                            <td width="120px">
                                <asp:Localize ID="Localize4" runat="server" meta:resourcekey="LiteralResource3" Text="Virtual View Alias:"></asp:Localize></td>
                            <td>
                                <asp:UpdatePanel ID="UP2" runat="server" UpdateMode="Conditional">
                                    <ContentTemplate>
                                        <asp:TextBox ID="ObjectAlias" MaxLength="128" Width="250px" runat="server" meta:resourcekey="ObjectAliasResource1" />
                                        <asp:ImageButton ID="btnFV1" runat="server" AlternateText="Generate a unique alias."
                                            CausesValidation="False" ImageUrl="../ahImages/iconCreate.gif" meta:resourcekey="btnFV1Resource1"
                                            OnClick="GenerateUniqueAlias" ToolTip="Generate a unique alias." />
                                        <asp:CustomValidator ID="cvObjectAliasValid" runat="server" ErrorMessage="This Virtual View alias already exists in the database. Please make it unique."
                                            ControlToValidate="ObjectAlias" ValidationGroup="VirtualView" OnServerValidate="IsObjectAliasValid"
                                            EnableClientScript="False" meta:resourcekey="cvObjectAliasValidResource1">*</asp:CustomValidator>
                                        <img id="help1" runat="server" src="../ahImages/iconHelp.gif" />
                                        <ajaxToolkit:PopupControlExtender ID="PopupControlExtender1" runat="server" TargetControlID="help1"
                                            PopupControlID="pnlHelp1" Position="Bottom" BehaviorID="PopupControlExtender1"
                                            DynamicServicePath="" Enabled="True" ExtenderControlID="" />
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </td>
                        </tr>--%>
                        <tr>
                            <td width="120px">
                                <asp:Localize ID="Localize5" runat="server" meta:resourcekey="LiteralResource4" Text="Definition:"></asp:Localize></td>
                            <td>
                                <asp:TextBox ID="Definition" Rows="5" TextMode="MultiLine" Width="500px" runat="server"
                                    meta:resourcekey="DefinitionResource1" />
                                <asp:RequiredFieldValidator ID="rtvDefinition" runat="server" ErrorMessage="Virtual View Definition is required."
                                    ControlToValidate="Definition" meta:resourcekey="rtvDefinitionResource1" ValidationGroup="VirtualView">*</asp:RequiredFieldValidator>
                                <asp:CustomValidator ID="cvTestSuccessful" runat="server" ErrorMessage="You have to enter a SELECT query as Definition."
                                    ControlToValidate="Definition" OnServerValidate="IsTestSuccessful" EnableClientScript="False"
                                    ValidationGroup="VirtualView" meta:resourcekey="cvTestSuccessfulResource1">*</asp:CustomValidator>
                            </td>
                        </tr>
                        <%--<tr>
                            <td>
                                <asp:Button ID="btnTest" CssClass="command" runat="server" OnClick="TestDefinition"
                                    ToolTip="Test Definition" Text="Test Definition" meta:resourcekey="btnTestResource1" />
                            </td>
                        </tr>--%>
                    </table>
                    <AdHoc:LogiButton ID="btnTest" runat="server" OnClick="TestDefinition"
                        ValidationGroup="VirtualView" ToolTip="Test Definition" Text="Test Definition"
                        meta:resourcekey="btnTestResource1" />
                    <h2 id="GridHeader" runat="server">
                        <asp:Localize ID="Localize6" runat="server" meta:resourcekey="LiteralResource5" Text="Virtual View Columns"></asp:Localize></h2>
                    <asp:Panel ID="pnlHelp1" runat="server" CssClass="popupControl" Width="300px" meta:resourcekey="pnlHelp1Resource1">
                        <asp:Localize ID="Localize7" runat="server" meta:resourcekey="LiteralResource6" Text="Virtual View Alias is used in the report queries to represent this data object and
                        it must be unique and comply with your database rules. If you are not sure what
                        to pick, leave this field blank and the application will automatically assign an
                        alias to your Virtual View."></asp:Localize>
                    </asp:Panel>
                    
                    <ul class="validation_error" id="ErrorList" runat="server">
                        <li>
                            <asp:Label ID="lblValidationMessage" runat="server" meta:resourcekey="lblValidationMessageResource1"></asp:Label></li>
                    </ul>
                    
                    <asp:UpdatePanel ID="UP1" UpdateMode="Conditional" runat="server">
                        <ContentTemplate>
                        <table><tr><td>
                            <div id="data_main" runat="server">
                            <asp:GridView ID="grdMain" runat="server" AllowPaging="True" DataKeyNames="ColumnID"
                                OnRowDataBound="OnItemDataBoundHandler" CssClass="grid" AutoGenerateColumns="False"
                                meta:resourcekey="grdMainResource1">
                                <Columns>
                                    <asp:BoundField HeaderText="Column Name" DataField="ColumnName" meta:resourcekey="BoundFieldResource1">
                                        <ItemStyle Width="250px" />
                                    </asp:BoundField>
                                    <asp:BoundField HeaderText="User-defined" DataField="Userdefined" meta:resourcekey="BoundFieldResource5" />
                                    <asp:TemplateField HeaderText="Data Type" meta:resourcekey="TemplateFieldResource1">
                                        <ItemTemplate>
                                            <asp:Label ID="DataType" runat="server" meta:resourcekey="DataTypeResource1" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField HeaderText="Character Length" DataField="CharacterMaxLen" meta:resourcekey="BoundFieldResource2" />
                                    <asp:BoundField HeaderText="Numeric Precision" DataField="NumericPrecision" meta:resourcekey="BoundFieldResource3" />
                                    <asp:BoundField HeaderText="Numeric Scale" DataField="NumericScale" meta:resourcekey="BoundFieldResource4" />
                                </Columns>
                                <PagerStyle HorizontalAlign="Center" />
                                <PagerTemplate>
                                    <AdHoc:PagingControl ID="pageCtrl" runat="server" OnGotoNextPage="ChangePageIndex" />
                                </PagerTemplate>
                                <HeaderStyle CssClass="gridheader" />
                                <RowStyle CssClass="gridrownocolor" />
                                <AlternatingRowStyle CssClass="gridalternaterownocolor" />
                            </asp:GridView>
                            </div>
                        </td></tr></table>
                            <div id="divLegend" class="legend" runat="server">
                                <asp:Localize ID="locLegend" runat="server" Text="<%$ Resources:LogiAdHoc, LegendLabel %>"></asp:Localize>
                                <dl id="dlNewColumn" runat="server">
                                    <dt>
                                        <img id="Img1" runat="server" alt="New" border="0" height="13" meta:resourcekey="LegendImage1"
                                            src="../ahImages/spacer.gif" style="background-color: Black;" title="New" width="13" />
                                    </dt>
                                    <dd style="color: Black;">
                                        <asp:Localize ID="Localize9" runat="server" meta:resourcekey="LiteralResource7" Text="New"></asp:Localize>&nbsp;
                                    </dd>
                                </dl>
                                <dl id="dlUnchangedColumn" runat="server">
                                    <dt>
                                        <img id="Img2" runat="server" alt="Unchanged" border="0" height="13" meta:resourcekey="LegendImage2"
                                            src="../ahImages/spacer.gif" style="background-color: #008800;" title="Unchanged" width="13" />
                                    </dt>
                                    <dd style="color: #008800;">
                                        <asp:Localize ID="Localize10" runat="server" meta:resourcekey="LiteralResource8"
                                            Text="Unchanged"></asp:Localize>&nbsp;
                                    </dd>
                                </dl>
                                <dl id="dlChangedColumn" runat="server">
                                    <dt>
                                        <img id="Img3" runat="server" alt="Changed" border="0" height="13" meta:resourcekey="LegendImage3"
                                            src="../ahImages/spacer.gif" style="background-color: #000088;" title="To be changed" width="13" />
                                    </dt>
                                    <dd style="color: #000088;">
                                        <asp:Localize ID="Localize11" runat="server" meta:resourcekey="LiteralResource9"
                                            Text="To be changed"></asp:Localize>&nbsp;
                                    </dd>
                                </dl>
                                <dl id="dlRemovedColumn" runat="server">
                                    <dt>
                                        <img id="Img4" runat="server" alt="Removed" border="0" height="13" meta:resourcekey="LegendImage4"
                                            src="../ahImages/spacer.gif" style="background-color: #ff0000;" title="To be removed" width="13" />
                                    </dt>
                                    <dd style="color: #ff0000;">
                                        <asp:Localize ID="Localize12" runat="server" meta:resourcekey="LiteralResource10"
                                            Text="To be removed"></asp:Localize>&nbsp;
                                    </dd>
                                </dl>
                            </div>
                        </ContentTemplate>
                        <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="btnTest" EventName="Click" />
                        </Triggers>
                    </asp:UpdatePanel>
                    
<br />
                    <div id="divButtons" class="divButtons">
                        <AdHoc:LogiButton ID="btnSave" runat="server" OnClick="Save_OnClick"
                            UseSubmitBehavior="false" ToolTip="<%$ Resources:LogiAdHoc, SaveTooltip1 %>"
                            Text="<%$ Resources:LogiAdHoc, Save %>" ValidationGroup="VirtualView" />
                        <AdHoc:LogiButton ID="btnSaveAs" runat="server" OnClick="SaveVVAs" ValidationGroup="VirtualView"
                            ToolTip="<%$ Resources:LogiAdHoc, SaveTooltip1 %>" Text="<%$ Resources:LogiAdHoc, SaveAs %>" />
                        <AdHoc:LogiButton ID="btnCancel" runat="server" OnClick="Cancel_OnClick"
                            ToolTip="Click to cancel your unsaved changes and return to the previous page."
                            Text="Back to Virtual Views" CausesValidation="False" meta:resourcekey="btnCancelResource1" />
                    </div>
<br />
                    <%--<asp:UpdatePanel ID="UPSaveAS" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>--%>
                            <asp:Button runat="server" ID="Button1" Style="display: none" />
                            <ajaxToolkit:ModalPopupExtender runat="server" ID="ahModalPopup" BehaviorID="ahModalPopupBehavior"
                                TargetControlID="Button1" PopupControlID="ahPopup" BackgroundCssClass="modalBackground"
                                DropShadow="false" PopupDragHandleControlID="pnlDragHandle" RepositionMode="None">
                            </ajaxToolkit:ModalPopupExtender>
                            <asp:Panel runat="server" CssClass="modalPopup" ID="ahPopup" Style="display:none; width:450;">
                                <asp:Panel ID="pnlDragHandle" runat="server" Style="cursor: hand;">
                                <div class="modalPopupHandle">
                                    <table cellpadding="0" cellspacing="0" style="width: 100%"><tr><td>
                                            <asp:Localize ID="PopupHeader" runat="server" meta:resourcekey="saveAsResource" Text="Save As New Virtual View"></asp:Localize>
                                        </td>
                                        <td style="width: 20px;">
                                            <asp:ImageButton ID="imgClosePopup" runat="server" 
                                                OnClick="imgClosePopup_Click" CausesValidation="false"
                                                SkinID="imgbClose" ImageUrl="../ahImages/remove.gif" 
                                                AlternateText="<%$ Resources:LogiAdHoc, CloseWindow %>" />
                                    </td></tr></table>
                                </div>
                                </asp:Panel>
                                <div class="modalDiv">
                                <asp:UpdatePanel ID="upPopup" runat="server">
                                    <ContentTemplate>
                                        <%--<asp:Panel ID="pnlSaveAs" runat="server" CssClass="detailpanel">--%>
                                        <table>
                                            <tr>
                                                <td>
                                                    <asp:Localize ID="Localize8" runat="server" meta:resourcekey="LiteralResource2" Text="Virtual View Name:"></asp:Localize>
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtObjectName" MaxLength="128" Width="250px" runat="server" />
                                                    <asp:RequiredFieldValidator ID="rfvtxtPermissionName" runat="server" ErrorMessage="Virtual View Name is required."
                                                        ControlToValidate="txtObjectName" ValidationGroup="SaveAs" meta:resourcekey="rtvObjectNameResource1">*</asp:RequiredFieldValidator>
                                                    <asp:CustomValidator ID="cvtxtObjectName" runat="server" ErrorMessage="This Virtual View name already exists in the database. Please select another name."
                                                        ControlToValidate="txtObjectName" ValidationGroup="SaveAs" OnServerValidate="IsSaveAsObjectNameValid"
                                                        EnableClientScript="false" meta:resourcekey="cvObjectNameValidResource1">*</asp:CustomValidator>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="120px">
                                                    <asp:Localize ID="Localize4" runat="server" meta:resourcekey="LiteralResource11" Text="Friendly Name:"></asp:Localize></td>
                                                <td>
                                                    <asp:TextBox ID="txtSaveAsObjectLabel" MaxLength="128" Width="250px" runat="server" meta:resourcekey="ObjectLabelResource1" />
                                                    <%--<asp:RequiredFieldValidator ID="rtvObjectLabel" ControlToValidate="ObjectLabel" ErrorMessage="Label is required."
                                                        runat="server" ValidationGroup="Catalog" meta:resourcekey="rtvObjectLabelResource1">*</asp:RequiredFieldValidator>--%>
                                                    <asp:CustomValidator ID="cvtxtSaveAsObjectLabel" runat="server" ControlToValidate="txtSaveAsObjectLabel"
                                                        EnableClientScript="False" ErrorMessage="Label cannot contain some special characters. Only alpha-numeric characters and _ are allowed in a label."
                                                        meta:resourcekey="cvObjectLabelResource1" OnServerValidate="IsSaveAsObjectLabelValid"
                                                        ValidationGroup="SaveAs">*</asp:CustomValidator>
                                                </td>
                                            </tr>
                                        </table>
                                        <br />
                                        <table>
                                            <tr>
                                                <td>
                                                    <AdHoc:LogiButton ID="btnSPA_OK" runat="server" OnClick="OK_SPA" ValidationGroup="SaveAs"
                                                        ToolTip="<%$ Resources:LogiAdHoc, SaveTooltip1 %>" Text="<%$ Resources:LogiAdHoc, OKWithSpaces %>"
                                                        UseSubmitBehavior="false" />
                                                    <AdHoc:LogiButton ID="btnSPA_Cancel" runat="server" OnClick="Cancel_SPA"
                                                        ToolTip="<%$ Resources:LogiAdHoc, CancelTooltip1 %>" Text="<%$ Resources:LogiAdHoc, Cancel %>"
                                                        CausesValidation="false" />
                                                </td>
                                            </tr>
                                        </table>
                                        <asp:ValidationSummary ID="ValidationSummary1" ValidationGroup="SaveAs" runat="server"
                                            meta:resourcekey="vsummaryResource2" />
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                                </div>
                            </asp:Panel>
                        <%--</ContentTemplate>
                        <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="btnSaveAsTop" EventName="Click" />
                            <asp:AsyncPostBackTrigger ControlID="btnSaveAs" EventName="Click" />
                        </Triggers>
                    </asp:UpdatePanel>--%>
                    <div id="divSaveAnimation">
                        <asp:Button runat="server" ID="btnSaveAnimation" Style="display: none" />
                        <ajaxToolkit:ModalPopupExtender runat="server" ID="ahSavePopup" BehaviorID="ahSavePopupBehavior"
                            TargetControlID="btnSaveAnimation" PopupControlID="pnlSaving" BackgroundCssClass="modalBackground"
                            DropShadow="False">
                        </ajaxToolkit:ModalPopupExtender>
                        <asp:Panel id="pnlSaving" CssClass="savePopup" runat="server" style="display:none;">
                            <table><tr><td>
                            <asp:Image ID="imgSave" runat="server" SkinID="imgSaving" EnableViewState="false" AlternateText="<%$ Resources:LogiAdHoc, SaveImageAltText %>" />
                            </td>
                            <td valign="middle">
                            <asp:Label ID="lblSaveText" CssClass="lblSavePopup" runat="server" EnableViewState="false" Text="<%$ Resources:LogiAdHoc, PleaseWait %>"/>
                            </td></tr></table>
                        </asp:Panel>
                    </div>
                    <asp:ValidationSummary ID="vsummary" ValidationGroup="VirtualView" runat="server" meta:resourcekey="vsummaryResource1" />                    
                </ContentTemplate>
                <Triggers>
                    <asp:PostBackTrigger ControlID="btnCancel" />
                </Triggers>
            </asp:UpdatePanel>
        </div>
    </form>
</body>
</html>
