<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="SessionByOrg.aspx.vb" Inherits="LogiAdHoc.SessionByOrg" meta:resourcekey="PageResource1" UICulture="auto" %>

<%@ Register TagPrefix="AdHoc" TagName="MainMenu" Src="~/ahControls/MainMenu.ascx" %>
<%@ Register TagPrefix="AdHoc" TagName="NavMenu" Src="~/ahControls/NavMenu.ascx" %>
<%@ Register TagPrefix="AdHoc" TagName="Search" Src="~/ahControls/Search.ascx" %>
<%@ Register TagPrefix="AdHoc" TagName="BreadCrumbTrail" Src="~/ahControls/BreadCrumbTrail.ascx" %>
<%@ Register TagPrefix="AdHoc" TagName="PagingControl" Src="~/ahControls/PagingControl.ascx" %>
<%@ Register TagPrefix="wizard" TagName="simpledatebox" Src="../ahControls/DateBox.ascx" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title>Session Parameter by Organization</title>
    <link rel="shortcut icon" href="../ahImages/flav.ico" />
    <script language="JavaScript" type="text/javascript" src="../ahScripts/ahChange.js"></script>

    <script type="text/javascript">
        window.onbeforeunload = AlertOnExit;
    </script>
</head>
<body id="bod">
    <AdHoc:MainMenu ID="menu" runat="server" />
    <form id="form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server" />
    <div id="submenu">
        <AdHoc:NavMenu ID="subnav" runat="server" />
    </div>
    <asp:UpdatePanel ID="UPBct" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <AdHoc:BreadCrumbTrail ID="bct" runat="server" Key="SessionByOrg" ParentLevels="1" />
        </ContentTemplate>
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="ddlParentID" EventName="SelectedIndexChanged" />
        </Triggers>
    </asp:UpdatePanel>
<table class="limiting"><tr><td>
    <table class="gridForm"><tr><td>
        <input type="hidden" id="ahDirty" name="ahDirty" runat="server" />
        <input type="hidden" id="ahParentID" name="ahParentID" runat="server" />
        <table id="tbParent" runat="server">
            <tr>
                <td width="125px">
                    <asp:Localize ID="Localize1" runat="server" Text="Selected Session Parameter:" meta:resourcekey="LiteralResource1" ></asp:Localize></td>
                <td>
                    <asp:DropDownList ID="ddlParentID" AutoPostBack="True" runat="server" meta:resourcekey="ddlParentIDResource1"  />
                </td>
            </tr>
            <tr>
                <td width="125px">
                    <asp:Localize ID="Localize2" runat="server" Text="Default Value:" meta:resourcekey="LiteralResource2" ></asp:Localize></td>
                <td>
                    <asp:UpdatePanel ID="UPDefaultValue" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>
                            <asp:Label ID="lblDefaultValue" runat="server" meta:resourcekey="lblDefaultValueResource1" ></asp:Label>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
            </tr>
            <tr>
                <td width="125px">
                    <asp:Localize ID="Localize3" runat="server" Text="<%$ Resources:LogiAdHoc, SessionParameters_Type %>" ></asp:Localize>:</td>
                <td>
                    <asp:UpdatePanel ID="UPDataTypeCategory" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>
                            <asp:Label ID="lblDataTypeCategory" runat="server" meta:resourcekey="lblDataTypeCategoryResource1" ></asp:Label>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
            </tr>
        </table>
        <br />
        <asp:UpdatePanel ID="UP1" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <div id="data_main">
                    <div id="activities">
                        <table width="100%" cellpadding="0" cellspacing="0">
                            <tr width="100%">
                                <td align="left" valign="top">
                                    <AdHoc:LogiButton ID="btnFollowDefault" OnClick="FollowDefault_OnClick" Text="Restore Default"
                                        runat="server" CausesValidation="false" meta:resourcekey="btnFollowDefaultResource1"/>
                                    <AdHoc:LogiButton ID="btnSetValue" OnClick="SetValue_OnClick" Text="Set Value"
                                        runat="server" CausesValidation="false" meta:resourcekey="btnSetValueResource1"/>
                                </td>
                                <td align="right" valign="top">
                                    <AdHoc:Search ID="srch" runat="server" Title="Find Organizations" meta:resourcekey="AdHocSearch" />
                                </td>
                            </tr>
                        </table>
                    </div>
                <asp:GridView ID="grdMain" runat="server" AllowPaging="True" AllowSorting="True" AutoGenerateColumns="False"
                    CssClass="grid" OnRowDataBound="OnItemDataBoundHandler" OnSorting="OnSortCommandHandler"
                    meta:resourcekey="grdMainResource1">
                <HeaderStyle CssClass="gridheader" />
                <PagerStyle HorizontalAlign="Center" />
                <PagerTemplate>
                    <AdHoc:PagingControl ID="pageCtrl" runat="server" OnGotoNextPage="ChangePageIndex" />
                </PagerTemplate>
                <RowStyle CssClass="gridrow" />
                <AlternatingRowStyle CssClass="gridalternaterow" />
                <Columns>
                    <asp:TemplateField>
                        <HeaderStyle Width="30px"></HeaderStyle>
                        <HeaderTemplate>
                            <asp:Label ID="lblChkAll" runat="server" AssociatedControlID="CheckAll" Text="<%$ Resources:LogiAdHoc, SelectDeselectAll %>" CssClass="NoShow"></asp:Label>
                            <asp:CheckBox runat="server" ToolTip="<%$ Resources:LogiAdHoc, SelectDeselectAll %>" ID="CheckAll">
                            </asp:CheckBox>
                        </HeaderTemplate>
                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                        <ItemTemplate>
                            <asp:Label ID="lblChk" runat="server" AssociatedControlID="chk_Select" Text="<%$ Resources:LogiAdHoc, SelectDeselect %>" CssClass="NoShow"></asp:Label>
                            <asp:CheckBox runat="server" ID="chk_Select" ToolTip="<%$ Resources:LogiAdHoc, SelectDeselect %>">
                            </asp:CheckBox>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField HeaderText="Organization" DataField="GroupName" SortExpression="GroupName" meta:resourcekey="BoundFieldResource1">
                        <HeaderStyle Width="200px" />
                    </asp:BoundField>
                    <asp:TemplateField HeaderText="Follow Default" SortExpression="IsDefault" meta:resourcekey="TemplateFieldResource1">
                        <ItemTemplate>
                            <input type="hidden" id="ihGroupID" runat="server" />
                            <%--<asp:Label ID="lbFollowDefault" runat="server" />--%>
                            <asp:CheckBox ID="chkFollowDefault" runat="server" />
                        </ItemTemplate>
                        <HeaderStyle Width="50px" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Parameter Value" meta:resourcekey="TemplateFieldResource2">
                        <ItemTemplate>
                            <asp:TextBox ID="txtParameterValue" MaxLength="4000" runat="server" Width="170px"></asp:TextBox>
                            <asp:CustomValidator ID="cvTxtParameterValue" runat="server" ControlToValidate="txtParameterValue"
                                EnableClientScript="False" ErrorMessage="Parameter Value must be valid." OnServerValidate="IsParamValueValid" 
                                ValidationGroup="GroupSessionParam" meta:resourcekey="cvTxtParameterValueResource1"
                                ValidateEmptyText="true" >*</asp:CustomValidator>
                        </ItemTemplate>
                        <HeaderStyle Width="200px" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="<%$ Resources:LogiAdHoc, Actions %>">
                        <HeaderStyle Width="50px"></HeaderStyle>
                        <ItemTemplate>
                            <asp:Image ID="imgActions" AlternateText="<%$ Resources:LogiAdHoc, Actions %>" runat="server" 
                                ToolTip="<%$ Resources:LogiAdHoc, Actions %>" ImageUrl="~/ahImages/arrowStep.gif" SkinID="imgActions" />
                            <asp:Panel ID="pnlActionsMenu" runat="server" CssClass="popupMenu" 
                                HorizontalAlign="Left" Wrap="false" style="display:none;">
                                <div id="divModify" runat="server" class="hoverMenuActionLink" >
                                    <asp:LinkButton ID="lnkModify" runat="server" OnCommand="EditItem" Text="Modify" ToolTip="Modify"
                                        CausesValidation="False" meta:resourcekey="ModifyResource1"></asp:LinkButton>
                                </div>
                                <div id="divRestore" runat="server" class="hoverMenuActionLink" >
                                    <asp:LinkButton ID="lnkRestoreDefault" runat="server" OnCommand="RestoreDefault" 
                                        Text="Restore Default" ToolTip="Restore Default" 
                                        CausesValidation="False" meta:resourcekey="RestoreDefaultResource1"></asp:LinkButton>
                                </div>
                            </asp:Panel>
                            <ajaxToolkit:HoverMenuExtender ID="hme2" runat="server"
                                PopupControlID="pnlActionsMenu" PopupPosition="right" 
                                TargetControlID="imgActions" PopDelay="25" />
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                    </asp:TemplateField>
                </Columns>
                </asp:GridView>
                </div>
                <asp:ValidationSummary ID="ValidationSummary1" runat="server" ValidationGroup="GroupSessionParam" />
                <br />
                <div id="divButtons" class="divButtons">
                    <AdHoc:LogiButton ID="btnSave" runat="server" OnClick="Save_OnClick"
                        UseSubmitBehavior="false" ToolTip="<%$ Resources:LogiAdHoc, SaveTooltip1 %>"
                        Text="<%$ Resources:LogiAdHoc, Save %>" ValidationGroup="GroupSessionParam" />
                    <AdHoc:LogiButton ID="btnCancel" runat="server" OnClick="Cancel_OnClick"
                        ToolTip="Click to cancel your unsaved changes and return to the previous page."
                        Text="Back to Session Parameters" CausesValidation="False" meta:resourcekey="btnCancelResource1" />
                </div>
<br />

                <asp:Button runat="server" ID="Button1" Style="display: none" />
                <ajaxToolkit:ModalPopupExtender runat="server" ID="ahModalPopup" BehaviorID="ahModalPopupBehavior"
                    TargetControlID="Button1" PopupControlID="ahPopup" BackgroundCssClass="modalBackground"
                    DropShadow="false" PopupDragHandleControlID="pnlDragHandle" RepositionMode="None">
                </ajaxToolkit:ModalPopupExtender>
                
                <asp:Label ID="lblTest" runat="server">
                </asp:Label>    
                
                <asp:Panel runat="server" CssClass="modalPopup" ID="ahPopup" Style="display: none; width: 366;">
                    <asp:Panel ID="pnlDragHandle" runat="server" Style="cursor: hand;">
                        <div class="modalPopupHandle">
                            <table cellpadding="0" cellspacing="0" style="width: 100%"><tr><td>
                                    <asp:Localize ID="PopupHeader" runat="server" Text="Set Value" meta:resourcekey="LiteralResource3" ></asp:Localize>
                                </td>
                                <td style="width: 20px;">
                                    <asp:ImageButton ID="imgClosePopup" runat="server" 
                                        OnClick="imgClosePopup_Click" CausesValidation="false"
                                        SkinID="imgbClose" ImageUrl="../ahImages/remove.gif" 
                                        AlternateText="<%$ Resources:LogiAdHoc, CloseWindow %>" />
                            </td></tr></table>
                        </div>
                    </asp:Panel>
                    <div class="modalDiv">
                        <asp:UpdatePanel ID="upPopup" runat="server">
                            <ContentTemplate>
                                <table cellspacing="0" cellpadding="1" border="0">
                                    <tr>
                                        <td valign="middle" width="100px">
                                            <asp:Localize ID="Localize4" runat="server" Text="Parameter Value:" meta:resourcekey="LiteralResource4"></asp:Localize>&nbsp;&nbsp;
                                        </td>
                                        <td valign="top">
                                            <asp:TextBox ID="ParamValue" MaxLength="4000" Width="200px" runat="server" />
                                            <wizard:simpledatebox ID="DateBox" runat="server" ValidationGroup="ParamValue" />
                                            <asp:TextBox ID="txaTextArea" runat="server" TextMode="MultiLine" Rows="8"  />
                                            
                                            <asp:TextBox ID="txtFakeParamValue" runat="server" CssClass="NoShow" />
                                            <asp:CustomValidator ID="cvValidParamValue" runat="server" ControlToValidate="txtFakeParamValue"
                                                 EnableClientScript="False" ErrorMessage="Default Value must be a valid string" ValidateEmptyText="true" 
                                                 OnServerValidate="IsSetValueValid" ValidationGroup="ParamValue" 
                                                 meta:resourcekey="cvValidParamValueResource1">*</asp:CustomValidator>
                                                 
                                            <%--<asp:TextBox ID="txtParamValue" TextMode="MultiLine" Columns="50" Rows="5" runat="server"/>
                                            <asp:RequiredFieldValidator ID="rfvParamValue" runat="server" ErrorMessage="Parameter Value is required."
                                                ControlToValidate="txtParamValue" ValidationGroup="ParamValue" >*</asp:RequiredFieldValidator>
                                            <asp:CustomValidator ID="cvMaxLength" ValidationGroup="ParamValue" runat="server" ErrorMessage="<%$ Resources:Errors, Err_ParameterValue255 %>"
                                                ControlToValidate="txtParamValue" OnServerValidate="ReachedMaxLength">*</asp:CustomValidator>--%>
                                                
                                        </td>
                                    </tr>
                                </table>
                                <br />
                                <div id="divInfo" runat="server">
                                    <p class="info" id="info" runat="server">
                                    <asp:Localize ID="Localize5" runat="server" Text="<%$ Resources:LogiAdHoc, UseEnterKey %>"></asp:Localize></p>
                                </div>
                                <!-- Buttons -->
                                <table>
                                    <tr>
                                        <td colspan="2">
                                            <AdHoc:LogiButton ID="btnSaveParam" OnClick="SaveParam_OnClick" runat="server"
                                                Text="<%$ Resources:LogiAdHoc, OKWithSpaces %>" UseSubmitBehavior="false" 
                                                ValidationGroup="ParamValue"/>
                                            <AdHoc:LogiButton ID="btnCancelParam" OnClick="CancelParam_OnClick"
                                                runat="server" Text="<%$ Resources:LogiAdHoc, Cancel %>" CausesValidation="False"/>
                                            <asp:ValidationSummary ID="vsummary" runat="server" ValidationGroup="ParamValue" />
                                        </td>
                                    </tr>
                                </table>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </div>
                </asp:Panel>
                    
                <div id="divSaveAnimation">
                    <asp:Button runat="server" ID="btnSaveAnimation" Style="display: none" />
                    <ajaxToolkit:ModalPopupExtender runat="server" ID="ahSavePopup" BehaviorID="ahSavePopupBehavior"
                        TargetControlID="btnSaveAnimation" PopupControlID="pnlSaving" BackgroundCssClass="modalBackground"
                        DropShadow="False">
                    </ajaxToolkit:ModalPopupExtender>
                    <asp:Panel id="pnlSaving" CssClass="savePopup" runat="server" style="display:none;">
                        <table><tr><td>
                        <asp:Image ID="imgSave" runat="server" SkinID="imgSaving" EnableViewState="false" AlternateText="<%$ Resources:LogiAdHoc, SaveImageAltText %>" />
                        </td>
                        <td valign="middle">
                        <asp:Label ID="lblSaveText" CssClass="lblSavePopup" runat="server" EnableViewState="false" Text="<%$ Resources:LogiAdHoc, PleaseWait %>"/>
                        </td></tr></table>
                    </asp:Panel>
                </div>
            </ContentTemplate>
            <Triggers>
                <asp:AsyncPostBackTrigger ControlID="ddlParentID" EventName="SelectedIndexChanged" />
                <asp:PostBackTrigger ControlID="btnCancel" />
            </Triggers>
        </asp:UpdatePanel>
    </td></tr></table>
</td></tr></table>
    </form>
</body>
</html>
