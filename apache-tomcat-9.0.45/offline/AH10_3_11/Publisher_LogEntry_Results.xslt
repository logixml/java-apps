<?xml version="1.0" encoding="UTF-8" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="html" indent="yes" />

<xsl:template match="LogEntry">
<html>
<head>
		<title>Log Entry</title>
</head>
<body>

	<h1><xsl:value-of select="@Product"/> Changes</h1>
	<h2>Date: <xsl:value-of select="@Date"/></h2>
	
	<xsl:if test="not(DatabaseConnectionMapping|Files)">
	<p>No changes were made to <xsl:value-of select="@Product"/>.</p>
	</xsl:if>
	
	<xsl:apply-templates select="DatabaseConnectionMapping" />
	<xsl:apply-templates select="Files" />
  
</body>
</html>
</xsl:template>

<xsl:template match="DatabaseConnectionMapping">
<h3>Database Connection Mapping</h3>
<xsl:if test="DatabaseConnection">
<p>The following connection mapping was used while publishing reports:</p>
<table border="1">
  <tr>
    <th>Info Connection</th>
    <th>AH Connection</th>
    <th>Action</th>
  </tr>
<xsl:for-each select="DatabaseConnection">
  <tr>
    <td>
      <xsl:value-of select="@InfoConnectionID" />
    </td>
    <td>
      <xsl:value-of select="@MappedConnectionID" />
    </td>
    <td>
      <xsl:value-of select="@Action" />
    </td>
  </tr>
</xsl:for-each>
</table>
</xsl:if>
</xsl:template>

<xsl:template match="Files">
<h3>Files copied</h3>
<p>The following files were copied to <xsl:value-of select="../@Product"/>:</p>

<xsl:if test="File[@Action='Add']">
  <h4>Added files</h4>
	<ul>
		<xsl:for-each select="File[@Action='Add']">
			<li>
        <em>
          <xsl:value-of select="@RelativePath" />
        </em> (Type: <xsl:value-of select="@Type" />) - <xsl:value-of select="@Status" />
      </li>
		</xsl:for-each>
	</ul>
</xsl:if>

<xsl:if test="File[@Action='Update']">
	<h4>Updated files</h4>
	<ul>
		<xsl:for-each select="File[@Action='Update']">
      <li>
        <em>
          <xsl:value-of select="@RelativePath" />
        </em> (Type: <xsl:value-of select="@Type" />) - <xsl:value-of select="@Status" />
      </li>
		</xsl:for-each>
	</ul>
</xsl:if>

<xsl:if test="File[@Action='None']">
	<h4>Files that were not changed.</h4>
	<ul>
		<xsl:for-each select="File[@Action='None']">
      <li>
        <em>
          <xsl:value-of select="@RelativePath" />
        </em> (Type: <xsl:value-of select="@Type" />)
      </li>
		</xsl:for-each>
	</ul>
</xsl:if>

</xsl:template>

</xsl:stylesheet>

  