<%@ Page Language="vb" AutoEventWireup="false" Codebehind="DashboardWizard.aspx.vb"
    Inherits="LogiAdHoc.ahWizard_DashboardWizard" Culture="auto" UICulture="auto"
    meta:resourcekey="PageResource1" %>

<%@ Register Assembly="System.Web.Extensions" Namespace="System.Web.UI" TagPrefix="asp" %>
<%@ Register TagPrefix="AdHoc" TagName="MainMenu" Src="~/ahControls/MainMenu.ascx" %>
<%@ Register TagPrefix="AdHoc" TagName="BreadCrumbTrail" Src="~/ahControls/BreadCrumbTrail.ascx" %>
<%@ Register TagPrefix="AdHoc" TagName="SelectReportControl" Src="~/ahControls/SelectReportControl.ascx" %>
<%@ Register TagPrefix="AdHoc" TagName="SelectFolderControl" Src="~/ahControls/SelectFolderControl.ascx" %>
<%@ Register TagPrefix="AdHoc" TagName="datebox" Src="../ahControls/DateBox.ascx" %>
<%@ Register Src="~/ahControls/PickFolder.ascx" TagName="PickFolder" TagPrefix="AdHoc" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Dashboard Builder</title>
    <link rel="shortcut icon" href="../ahImages/flav.ico" />
    <script language="JavaScript" type="text/javascript" src="../ahScripts/ahChange.js"></script>

    <script type="text/javascript">
    <!--
        //window.onbeforeunload = AlertOnExit;
    //Dashboard Preview Functionality.
    //Change in "DashPreviewWin" name will need to be applied to function lnkbPreview_Click
    var DashPreviewWin=null;
    var bNavigatingAway = false;
    function ConfirmUnload(e)
    {
        bNavigatingAway = false;
        if((!isPostBack) && (!noMessage))
            bNavigatingAway = true;
            
        if ((document.getElementById('ahDirty').value==1) && (!isPostBack) && (!noMessage)) {
            return 'Any changes you have made to data on this page will be lost.';
       }
       noMessage=false;
    }
    function CloseAllPopups()
    {
        if (bNavigatingAway)
        {
            if (DashPreviewWin)
            {
                if (!DashPreviewWin.closed)
                {
                    DashPreviewWin.close();
                }
                DashPreviewWin = null;
            }
        }
    }
    window.onbeforeunload = ConfirmUnload;
    window.onunload = CloseAllPopups;

    function ExpandCollapse() {
        var d = document.getElementById("divRptSettings");
        var i = document.getElementById("imgExpand");
        if (d.style.display=="none") {
            d.style.display="";
            i.src = "../ahImages/collapse_blue.jpg";
        } else {
            d.style.display="none";
            i.src = "../ahImages/expand_blue.jpg";
        }
    }

    //Since we are hiding and showing Select report controls
    //This function needs to be declared here.
    function PanelResizing()
    {
//        var pnl_width = parseInt(document.getElementById('ucSelectReports_pnlTreeView').style.width)
//        var pnl_height = parseInt(document.getElementById('ucSelectReports_pnlTreeView').style.height)
//        document.getElementById('ucSelectReports_divTreeView').style.width = pnl_width + 'px';
//        document.getElementById('ucSelectReports_divTreeView').style.height = pnl_height + 'px';
//        document.getElementById('ucSelectReports_divTreeView').style.overflow = "auto";
//        document.getElementById('ucSelectReports_divListBox').style.width = (600 - pnl_width) + 'px';
        var pnl_width = parseInt($get('ucSelectReports_pnlTreeView').style.width)
        var pnl_height = parseInt($get('ucSelectReports_pnlTreeView').style.height)
        $get('ucSelectReports_divTreeView').style.width = pnl_width + 'px';
        $get('ucSelectReports_divTreeView').style.height = pnl_height + 'px';
        $get('ucSelectReports_divTreeView').style.overflow = "auto";
        $get('ucSelectReports_divListBox').style.width = (600 - pnl_width) + 'px';
        document.getElementById('ucSelectReports_divListBox').style.height = pnl_height + 'px';
        //alert('Test1');
        ScrollableListBoxRefineHeightAndWidth($get('ucSelectReports_lstReports'), 200, 500);
    }
    function popupWin(){
        
        var lnk = document.getElementById("LinkURL")
        var sURL = lnk.value
        //Added a check for http:// as this is the default text and window.open was opening
        //the url in the same window instead of popup if the user clicks on Test URL without 
        //changing anything.
        if (sURL != null && sURL != '' && sURL!= 'http://')
            window.open(sURL); 
        else
            alert('Please enter a valid URL.')
    }
    -->
    </script>

</head>
<body>
    <AdHoc:MainMenu ID="menu" runat="server" />
    <form id="form1" runat="server">
        <AdHoc:BreadCrumbTrail ID="bct" runat="server" Key="DashBuilder" />
        <asp:ScriptManager ID="ScriptManager1" runat="server" EnablePartialRendering="true">
            <Scripts>
                <asp:ScriptReference Path="~/ahScripts/webkit.js" />
            </Scripts>
        </asp:ScriptManager>

        <script type="text/javascript">
        Sys.WebForms.PageRequestManager.getInstance().add_beginRequest(BeginRequestHandler);
        Sys.WebForms.PageRequestManager.getInstance().add_endRequest(EndRequestHandler);
        function EndRequestHandler(sender, args) {
//            if (args.get_error() != undefined && args.get_error().httpStatusCode == '500') {
//                var errorMessage = args.get_error().message;
//                args.set_errorHandled(true);
//            }
            document.body.style.cursor = "default"; 
            noMessage=false;
            $find('ahSavePopupBehavior').hide();
            RestorePopupPosition('ahModalPopupBehavior');
            RestorePopupPosition('mpeSaveAsModalPopupBehavior');
        }
        function BeginRequestHandler(sender, args) {
            SavePopupPosition('ahModalPopupBehavior');
            SavePopupPosition('mpeSaveAsModalPopupBehavior');
        }
        </script>

        <div class="divForm">
            <asp:UpdatePanel ID="UPahDirty" runat="Server" UpdateMode="Conditional">
                <ContentTemplate>
                    <input type="hidden" id="ahDirty" name="ahDirty" runat="server" />
                </ContentTemplate>
            </asp:UpdatePanel>
            <div class="searchDiv">
                <div class="collapsePanelHeader2">
                    <table width="100%">
                        <tr>
                            <td valign="top">
                                <asp:Label ID="lblName" runat="server" CssClass="requiredfield" Text="Dashboard Name:"
                                    meta:resourcekey="lblNameResource2" AssociatedControlID="txtDashboardName" />
                                <asp:TextBox ID="txtDashboardName" CssClass="vAlignMiddle" MaxLength="200" Width="240px"
                                    runat="server" meta:resourcekey="txtDashboardNameResource1" />
                                <asp:RequiredFieldValidator ID="rtvDashboardName" runat="server" ErrorMessage="Dashboard Name is required."
                                    ControlToValidate="txtDashboardName" ValidationGroup="Dashboard" meta:resourcekey="rtvDashboardNameResource1">*</asp:RequiredFieldValidator>
                                <asp:CustomValidator ID="cvValidDashboardName" runat="server" ErrorMessage="Dashboard name already exists."
                                    ValidationGroup="Dashboard" ControlToValidate="txtDashboardName" OnServerValidate="IsDashboardNameValid"
                                    meta:resourcekey="cvValidDashboardNameResource1">*</asp:CustomValidator>
                                <asp:CustomValidator ID="cvValidDashboard" runat="server" ErrorMessage="There is an error in the dashboard."
                                    ValidationGroup="Dashboard" ControlToValidate="txtDashboardName" OnServerValidate="IsDashboardValid"
                                    meta:resourcekey="cvValidDashboardResource1">*</asp:CustomValidator>
                                <input id="errMsg" runat="server" type="hidden" />
                            </td>
                            <td valign="top">
                                <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional" RenderMode="Inline">
                                    <ContentTemplate>
                                        <AdHoc:LogiButton ID="lnkbPreview" runat="server" Text="Preview Dashboard"
                                            ValidationGroup="Dashboard" meta:resourcekey="lnkbPreviewResource1" />
                                        <AdHoc:LogiButton ID="lnkbSave" runat="server" Text="<%$ Resources:LogiAdHoc, Save %>"
                                            ValidationGroup="Dashboard" OnClick="SaveDashboard" UseSubmitBehavior="false" />
                                        <AdHoc:LogiButton ID="lnkbSaveAs" runat="server" Text="<%$ Resources:LogiAdHoc, SaveAs %>"
                                            UseSubmitBehavior="false" ValidationGroup="Dashboard" />
                                        <AdHoc:LogiButton ID="lnkbCancel" runat="server" CausesValidation="False" 
                                            meta:resourcekey="btnCancelResource1" Text="Back to Reports List" />
                                        <asp:Button ID="Button2" runat="server" Style="display: none" />
                                        <ajaxToolkit:ModalPopupExtender ID="mpeSaveAs" runat="server" BackgroundCssClass="modalBackground"
                                            BehaviorID="mpeSaveAsModalPopupBehavior" DropShadow="false" PopupControlID="pnlSaveAs"
                                            TargetControlID="Button2" PopupDragHandleControlID="pnlDragHandle" RepositionMode="None">
                                        </ajaxToolkit:ModalPopupExtender>
                                        <asp:Panel ID="pnlSaveAs" runat="server" CssClass="modalPopup" Style="display: none; width: 806;">
                                            <asp:Panel ID="pnlDragHandle" runat="server" Style="cursor: hand;">
                                            <div class="modalPopupHandle" style="width: 800px;">
                                                <table cellpadding="0" cellspacing="0" style="width: 100%"><tr><td>
                                                        <asp:Localize ID="PopupHeaderSaveAs" runat="server" meta:resourcekey="SaveAsPopupHeader"
                                                            Text="Save As"></asp:Localize>
                                                    </td>
                                                    <td style="width: 20px;">
                                                        <asp:ImageButton ID="imgClosePopup" runat="server" 
                                                            OnClick="imgClosePopup_Click" CausesValidation="false"
                                                            SkinID="imgbClose" ImageUrl="../ahImages/remove.gif"
                                                            AlternateText="<%$ Resources:LogiAdHoc, CloseWindow %>" />
                                                </td></tr></table>
                                            </div>
                                            </asp:Panel>
                                            <div class="modalDiv">
                                                <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                                                    <ContentTemplate>
                                                        <table>
                                                            <tr>
                                                                <td width="100px">
                                                                    <%--Any change in width here will need to be applied in PickFolder.ascx--%>
                                                                    <asp:Label ID="Label139" runat="server" meta:resourcekey="lblNameResource2"
                                                                        Text="Dashboard Name" AssociatedControlID="SaveAsReportName" ></asp:Label>:
                                                                </td>
                                                                <td>
                                                                    <input id="SaveAsReportName" runat="server" maxlength="200" size="45" type="text" />
                                                                    <asp:RequiredFieldValidator ID="rfvtxtSaveAsReportName" runat="server" ControlToValidate="SaveAsReportName"
                                                                        ErrorMessage="Dashboard Name is required." meta:resourcekey="rfvtxtSaveAsReportNameResource"
                                                                        ValidationGroup="SaveAs">*</asp:RequiredFieldValidator>
                                                                    <asp:CustomValidator ID="cvValidSaveAsDashboardName" runat="server" ControlToValidate="SaveAsReportName"
                                                                        ErrorMessage="This dashboard name already exists." meta:resourcekey="cvValidSaveAsDashboardNameResource1"
                                                                        OnServerValidate="IsDashboardSaveAsNameValid" ValidationGroup="SaveAs">*</asp:CustomValidator>
                                                                    <asp:CustomValidator ID="cvtxtSaveAsReportName" runat="server" ControlToValidate="SaveAsReportName"
                                                                        ErrorMessage="There is an error in the dashboard." meta:resourcekey="cvtxtSaveAsReportNameResource1"
                                                                        OnServerValidate="IsSaveAsDashboardValid" ValidationGroup="SaveAs">*</asp:CustomValidator>
                                                                    <input id="SaveAsErrMsg" runat="server" type="hidden" />
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td width="100px">
                                                                    <asp:Label ID="Localize140" runat="server" meta:resourcekey="LiteralResource4"
                                                                        Text="Report Description" AssociatedControlID="txtSaveAsDashboardDescription"></asp:Label>
                                                                </td>
                                                                <td>
                                                                    <asp:TextBox ID="txtSaveAsDashboardDescription" runat="server" Columns="50" MaxLength="255"
                                                                        Rows="5" TextMode="MultiLine">
                                                                    </asp:TextBox>
                                                                    <asp:CustomValidator ID="cvtxtSaveAsDashboardDescription" runat="server" ControlToValidate="txtSaveAsDashboardDescription"
                                                                        ErrorMessage="Dashboard Description can be no more than 255 characters." meta:resourcekey="rtvReportDescriptionResource1"
                                                                        OnServerValidate="ReachedMaxLength" ValidationGroup="SaveAs">*</asp:CustomValidator>
                                                                </td>
                                                            </tr>
                                                            <tr id="trSaveAsExpirationDate" runat="server">
                                                                <td width="100px">
                                                                    <asp:Localize ID="Localize142" runat="server" meta:resourcekey="LiteralResource138"
                                                                        Text="Dashboard Expiration Date"></asp:Localize>
                                                                </td>
                                                                <td>
                                                                    <AdHoc:datebox id="dbSaveAsExpirationDate" runat="server" required="false" />
                                                                    <asp:CustomValidator ID="cvSaveAsExpirationDate" runat="server" ControlToValidate="txtSaveAsDashboardDescription"
                                                                        ErrorMessage="Dashboard Expiration Date should be greater than today." meta:resourcekey="cvReportExpDateResource1"
                                                                        OnServerValidate="IsSaveAsDashboardExpirationDateValid" ValidateEmptyText="true"
                                                                        ValidationGroup="SaveAs">*</asp:CustomValidator>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                        <AdHoc:PickFolder ID="pfSaveAs" runat="server" ShowAllUserGroup="true" />
                                                        <table>
                                                            <tr>
                                                                <td>
                                                                    <AdHoc:LogiButton ID="btnSPA_OK" runat="server" OnClick="OK_SPA" Text="<%$ Resources:LogiAdHoc, OKWithSpaces %>"
                                                                        ToolTip="<%$ Resources:LogiAdHoc, SaveTooltip1 %>" UseSubmitBehavior="false"
                                                                        ValidationGroup="SaveAs" />
                                                                    <AdHoc:LogiButton ID="btnSPA_Cancel" runat="server" CausesValidation="false" 
                                                                        OnClick="Cancel_SPA" Text="<%$ Resources:LogiAdHoc, Cancel %>" ToolTip="<%$ Resources:LogiAdHoc, CancelTooltip1 %>" />
                                                                </td>
                                                            </tr>
                                                        </table>
                                                        <asp:ValidationSummary ID="ValidationSummary13" runat="server" meta:resourcekey="vsummaryResource13"
                                                            ValidationGroup="SaveAs" />
                                                        <asp:Label ID="lblSemicolon13" runat="server" OnPreRender="lblSemicolon_PreRender">
                                                        </asp:Label>
                                                    </ContentTemplate>
                                                </asp:UpdatePanel>
                                            </div>
                                        </asp:Panel>
                                        
                                        <div id="divSaveAnimation">
                                            <asp:Button runat="server" ID="btnSaveAnimation" Style="display: none" />
                                            <ajaxToolkit:ModalPopupExtender runat="server" ID="ahSavePopup" BehaviorID="ahSavePopupBehavior"
                                                TargetControlID="btnSaveAnimation" PopupControlID="pnlSaving" BackgroundCssClass="modalBackground"
                                                DropShadow="False">
                                            </ajaxToolkit:ModalPopupExtender>
                                            <asp:Panel ID="pnlSaving" CssClass="savePopup" runat="server" Style="display: none;">
                                                <table>
                                                    <tr>
                                                        <td>
                                                            <asp:Image ID="imgSave" runat="server" SkinID="imgSaving" EnableViewState="false" AlternateText="<%$ Resources:LogiAdHoc, SaveImageAltText %>" />
                                                        </td>
                                                        <td valign="middle">
                                                            <asp:Label ID="lblSaveText" CssClass="lblSavePopup" runat="server" EnableViewState="false"
                                                                Text="<%$ Resources:LogiAdHoc, PleaseWait %>" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </asp:Panel>
                                        </div>
                                    </ContentTemplate>
                                    <Triggers>
                                        <asp:PostBackTrigger ControlID="lnkbCancel" />
                                    </Triggers>
                                </asp:UpdatePanel>
                            </td>
                            <td align="right" valign="top">
                                <asp:Localize ID="Localize11" runat="server" Text="<%$ Resources:LogiAdHoc, DashboardSettings %>"></asp:Localize>
                                &nbsp;
                                <img id="imgExpand" src="../ahImages/expand_blue.jpg" onclick="ExpandCollapse();" runat="server" alt="Collapse/Expand" meta:resourcekey="CollapseResource1"/>
                            </td>
                        </tr>
                    </table>
                </div>
                <div id="divRptSettings" style="display: none;">
                    <table width="100%">
                        <tr>
                            <td valign="top">
                                <table>
                                    <tr>
                                        <td>
                                            <h2>
                                                <asp:Label ID="lblDescription" runat="server" Text="<%$ Resources:LogiAdHoc, Description %>"
                                                    meta:resourcekey="lblDescriptionResource1" AssociatedControlID="Description" />
                                            </h2>
                                            <asp:TextBox ID="Description" TextMode="MultiLine" Columns="45" Rows="3" runat="server"
                                                meta:resourcekey="DescriptionResource2" />
                                            <asp:CustomValidator ID="cvMaxLength" runat="server" ErrorMessage="<%$ Resources:Errors, Err_Description255 %>"
                                                ValidationGroup="Dashboard" ControlToValidate="Description" OnServerValidate="ReachedMaxLength">*</asp:CustomValidator>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <h2>
                                                <asp:Localize ID="Localize130" runat="server" meta:resourcekey="LiteralResource138"
                                                    Text="Dashboard Expiration Date"></asp:Localize>
                                            </h2>
                                            <AdHoc:datebox ID="ReportExpirationDate" runat="server" Required="false" />
                                            <asp:CustomValidator ID="cvReportExpirationDate" ControlToValidate="Description"
                                                ErrorMessage="Dashboard Expiration Date should be greater than today." OnServerValidate="IsExpirationDateValid"
                                                ValidationGroup="Dashboard" runat="server" ValidateEmptyText="true" meta:resourcekey="cvReportExpDateResource1">*</asp:CustomValidator>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <h2>
                                                <asp:Localize ID="Localize7" runat="server" meta:resourcekey="LiteralResource7"
                                                    Text="Dashboard Header"></asp:Localize>
                                            </h2>
                                            <asp:Panel ID="pnlHeaderInfo" runat="server">
                                                <table>
                                                    <tr>
                                                        <td width="80px">
                                                            <asp:Label ID="Label54" runat="server" AssociatedControlID="ShowTitle" Text="<%$ Resources:LogiAdHoc, Title %>"></asp:Label>:
                                                        </td>
                                                        <td width="125px">
                                                            <asp:CheckBox ID="ShowTitle" runat="server" />
                                                        </td>
                                                        <td width="80px">
                                                            <asp:Label ID="Label52" AssociatedControlID="ShowDate" runat="server" Text="<%$ Resources:LogiAdHoc, Res_Date %>"></asp:Label>:
                                                        </td>
                                                        <td>
                                                            <asp:CheckBox ID="ShowDate" runat="server" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td width="125px">
                                                            <asp:Label ID="Label55" AssociatedControlID="ShowDescription" runat="server" Text="<%$ Resources:LogiAdHoc, Description %>"></asp:Label>:
                                                        </td>
                                                        <td>
                                                            <asp:CheckBox ID="ShowDescription" runat="server" />
                                                        </td>
                                                        <td>
                                                            <asp:Label ID="Label53" AssociatedControlID="ShowTime" runat="server" Text="<%$ Resources:LogiAdHoc, Time %>"></asp:Label>:</td>
                                                        <td>
                                                            <asp:CheckBox ID="ShowTime" runat="server" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </asp:Panel>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                            <td valign="top">
                                <h2>
                                    <asp:Localize ID="Localize19" runat="server" Text="Style" meta:resourcekey="Localize19Resource1"></asp:Localize>
                                </h2>
                                <table>
                                    <tr>
                                        <td width="125px">
                                            <asp:Label ID="Label20" runat="server" Text="Pick a Style:" AssociatedControlID="RptStyle" meta:resourcekey="Localize20Resource1"></asp:Label>
                                        </td>
                                        <td>
                                            <asp:DropDownList ID="RptStyle" runat="server" AutoPostBack="True" meta:resourcekey="RptStyleResource1">
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                </table>
                                <div>
                                    <asp:UpdatePanel ID="UpdatePanel6" runat="server">
                                        <ContentTemplate>
                                            <asp:Image ID="StyleSample" runat="server" AlternateText="Style Image" meta:resourcekey="StyleSampleResource1" />
                                        </ContentTemplate>
                                        <Triggers>
                                            <asp:AsyncPostBackTrigger ControlID="RptStyle" EventName="SelectedIndexChanged" />
                                        </Triggers>
                                    </asp:UpdatePanel>
                                </div>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
            <br />
            <asp:UpdatePanel ID="UPMain" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                <asp:Panel ID="pnlDashPanelInfo" runat="server" >
<table><tr><td>
                <div id="data_main">
                    <div id="activities">
                        <table width="100%" cellpadding="0" cellspacing="0">
                        <tr width="100%">
                        <td align="left" valign="top">
                            <AdHoc:LogiButton ID="btnAddPanel2" Text="Add a Panel" runat="server"
                                 meta:resourcekey="btnAddPanel2Resource1" CausesValidation="False"  />
                            <AdHoc:LogiButton ID="btnRemovePanel2" Text="Delete Panels" runat="server"
                                meta:resourcekey="btnRemovePanel2Resource1" CausesValidation="False"  />
                        </td>
                        </tr>
                        </table>
                    </div>
                        <asp:GridView ID="grdMain" runat="server" DataKeyNames="PanelID" AutoGenerateColumns="False"
                            CssClass="grid" meta:resourcekey="grdMainResource1">
                            <Columns>
                            <asp:TemplateField>
                                <HeaderStyle Width="30px"></HeaderStyle>
                                <HeaderTemplate>
                                    <asp:Label ID="lblChkAll" runat="server" AssociatedControlID="CheckAll" Text="<%$ Resources:LogiAdHoc, SelectDeselectAll %>" CssClass="NoShow"></asp:Label>
                                    <asp:CheckBox runat="server" ToolTip="<%$ Resources:LogiAdHoc, SelectDeselectAll %>" ID="CheckAll" />
                                </HeaderTemplate>
                                <ItemStyle HorizontalAlign="Center" Width="44px"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblChk" runat="server" AssociatedControlID="chk_Select" Text="<%$ Resources:LogiAdHoc, SelectDeselect %>" CssClass="NoShow"></asp:Label>
                                    <asp:CheckBox runat="server" ID="chk_Select" ToolTip="<%$ Resources:LogiAdHoc, SelectDeselect %>" />
                                </ItemTemplate>
                            </asp:TemplateField>
                                <asp:BoundField DataField="Caption" HeaderText="Panel Caption" meta:resourcekey="BoundFieldResource3">
                                </asp:BoundField>
                                <asp:BoundField DataField="InitialColumn" HeaderText="Initial Column" meta:resourcekey="BoundFieldResource4">
                                    <HeaderStyle Width="50px" />
                                </asp:BoundField>
                                <asp:TemplateField HeaderText="<%$ Resources:LogiAdHoc, InitialDisplay %>" meta:resourcekey="BoundFieldResource5">
                                    <HeaderStyle Width="50px" />
                                    <ItemTemplate>
                                        <asp:Label ID="lblInitialDisplay" runat="server" />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="<%$ Resources:LogiAdHoc, Content %>" meta:resourcekey="TemplateFieldResource2">
                                    <ItemTemplate>
                                        <asp:Label ID="lblContent" runat="server" meta:resourcekey="lblContentResource1" />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="<%$ Resources:LogiAdHoc, Actions %>" meta:resourcekey="TemplateFieldResource3">
                                    <HeaderStyle Width="50px" />
                                    <ItemStyle HorizontalAlign="Center" />
                                    <ItemTemplate>
                                        <asp:Image ID="imgActions" AlternateText="<%$ Resources:LogiAdHoc, Actions %>" runat="server" 
                                            ToolTip="<%$ Resources:LogiAdHoc, Actions %>" ImageUrl="~/ahImages/arrowStep.gif" SkinID="imgActions" />
                                        <asp:Panel ID="pnlActionsMenu" runat="server" CssClass="popupMenu" 
                                            HorizontalAlign="Left" Wrap="false" style="display:none;">
                                            <div id="divModify" runat="server" class="hoverMenuActionLink" >
                                                <asp:LinkButton ID="lnkModify" runat="server" OnCommand="EditItem" 
                                                    Text="Modify Dashboard Panel" CausesValidation="False" 
                                                    meta:resourcekey="EditItemResource2"></asp:LinkButton>
                                            </div>
                                            <div id="divMoveup" runat="server" class="hoverMenuActionLink" >
                                                <asp:LinkButton ID="lnkMoveup" runat="server" OnCommand="PanelMoveUp" 
                                                    Text="Move Up" CausesValidation="False" 
                                                    meta:resourcekey="imgMoveupOrderResource1"></asp:LinkButton>
                                            </div>
                                            <div id="divMovedown" runat="server" class="hoverMenuActionLink" >
                                                <asp:LinkButton ID="lnkMovedown" runat="server" OnCommand="PanelMoveDown" 
                                                    Text="Move Down" CausesValidation="False" 
                                                    meta:resourcekey="imgMovedownOrderResource1"></asp:LinkButton>
                                            </div>
 				                        </asp:Panel>
                                        <ajaxToolkit:HoverMenuExtender ID="hme2" runat="server"
                                            PopupControlID="pnlActionsMenu" PopupPosition="right" 
                                            TargetControlID="imgActions" PopDelay="25" />
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                            <HeaderStyle CssClass="gridheader" />
                            <FooterStyle CssClass="gridfooter" />
                            <RowStyle CssClass="gridrow" />
                            <AlternatingRowStyle CssClass="gridalternaterow" />
                        </asp:GridView>
                </div>
</td></tr></table>
                    <asp:Button runat="server" ID="Button1" Style="display: none" />
                    <ajaxToolkit:ModalPopupExtender runat="server" ID="ahModalPopup" BehaviorID="ahModalPopupBehavior"
                        TargetControlID="Button1" PopupControlID="ahPopup" BackgroundCssClass="modalBackground"
                        DropShadow="false" PopupDragHandleControlID="pnlDragHandle2" RepositionMode="None">
                    </ajaxToolkit:ModalPopupExtender>
                    <asp:Panel runat="server" CssClass="modalPopup" ID="ahPopup" Style="display: none;
                        width: 706;">
                        <asp:Panel ID="pnlDragHandle2" runat="server" Style="cursor: hand;">
                            <div class="modalPopupHandle" style="width: 700px;">
                                <table cellpadding="0" cellspacing="0" style="width: 100%">
                                    <tr>
                                        <td>
                                            <asp:Localize ID="Localize1" runat="server" Text="Panel Settings" meta:resourcekey="Localize1Resource1"></asp:Localize>
                                        </td>
                                        <td style="width: 20px;">
                                            <asp:ImageButton ID="imgClosePopup2" runat="server" OnClick="imgClosePopup2_Click"
                                                CausesValidation="false" SkinID="imgbClose" ImageUrl="../ahImages/remove.gif"
                                                AlternateText="<%$ Resources:LogiAdHoc, CloseWindow %>" />
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </asp:Panel>
                        <div class="modalDiv">
                            <asp:UpdatePanel ID="upPopup" runat="server">
                                <ContentTemplate>
                                    <%--<asp:Panel ID="pnlDetails" CssClass="detailpanel" runat="server" meta:resourcekey="pnlDetailsResource1">--%>
                                    <input type="hidden" id="NewPanelID" runat="server" />
                                    <table class="tbTB">
                                        <tr>
                                            <td>
                                                <asp:Label ID="Label2" runat="server" Text="Panel Caption:" AssociatedControlID="txtPanelCaption"
                                                    meta:resourcekey="Localize2Resource1"></asp:Label>
                                            </td>
                                            <td colspan="3">
                                                <asp:TextBox ID="txtPanelCaption" MaxLength="50" Width="300px" runat="server" meta:resourcekey="txtPanelCaptionResource1" />
                                                <asp:RequiredFieldValidator ID="rtvPanelCaption" runat="server" ErrorMessage="Panel Caption is required."
                                                    ControlToValidate="txtPanelCaption" ValidationGroup="grpPanel" meta:resourcekey="rtvPanelCaptionResource1">*</asp:RequiredFieldValidator>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:Label ID="Label3" AssociatedControlID="txtPanelDescription" runat="server" Text="Panel Description:"
                                                    meta:resourcekey="Localize3Resource1"></asp:Label>
                                            </td>
                                            <td colspan="3">
                                                <asp:TextBox ID="txtPanelDescription" MaxLength="200" TextMode="MultiLine" Rows="3"
                                                    Columns="50" runat="server" meta:resourcekey="txtPanelDescriptionResource1" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:Label ID="Label5" runat="server" Text="Initial Column:" AssociatedControlID="ddlInitialColumn"
                                                    meta:resourcekey="Localize5Resource1"></asp:Label>
                                            </td>
                                            <td>
                                                <asp:DropDownList ID="ddlInitialColumn" runat="server" meta:resourcekey="ddlInitialColumnResource1">
                                                    <asp:ListItem Value="1" Text="<%$ Resources:LogiAdHoc, One %>" />
                                                    <asp:ListItem Value="2" Text="<%$ Resources:LogiAdHoc, Two %>" />
                                                    <asp:ListItem Value="3" Text="<%$ Resources:LogiAdHoc, Three %>" />
                                                    <asp:ListItem Value="4" Text="<%$ Resources:LogiAdHoc, Four %>" />
                                                </asp:DropDownList>
                                            </td>
                                            <td>
                                                <asp:Localize ID="Localize6" runat="server" Text="<%$ Resources:LogiAdHoc, InitialDisplay %>"></asp:Localize>:
                                            </td>
                                            <td>
                                                <asp:RadioButtonList ID="rblDisplay" runat="server" RepeatDirection="Horizontal"
                                                    meta:resourcekey="rblDisplayResource1">
                                                    <asp:ListItem Selected="True" Value="1" Text="<%$ Resources:LogiAdHoc, Yes %>" />
                                                    <asp:ListItem Value="0" Text="<%$ Resources:LogiAdHoc, No %>" />
                                                </asp:RadioButtonList>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:Label ID="Label10" runat="server" AssociatedControlID="txtHeight" Text="<%$ Resources:LogiAdHoc, Height %>"></asp:Label>
                                            </td>
                                            <td colspan="3">
                                                <asp:TextBox ID="txtHeight" MaxLength="4" runat="server" meta:resourcekey="txtHeightResource1" />
                                                <asp:RangeValidator ID="rvHeight" runat="server" ValidationGroup="grpPanel" ErrorMessage="Panel Height accepts only integer values between 10 and 5000."
                                                    ControlToValidate="txtHeight" Type="Integer" MinimumValue="10" MaximumValue="5000"
                                                    meta:resourcekey="rvHeightResource1">*</asp:RangeValidator>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="4">
                                                <h3>
                                                    <asp:Localize ID="Localize9" runat="server" Text="Panel Content" meta:resourcekey="Localize9Resource1"></asp:Localize></h3>
                                                <asp:RadioButton ID="LinkPreDefined" runat="server" AutoPostBack="True" Text="Pre-defined Reports"
                                                    OnCheckedChanged="LinkingMethod_Changed" GroupName="LinkingMethod" meta:resourcekey="LinkPreDefinedResource1">
                                                </asp:RadioButton>
                                                <asp:RadioButton ID="LinkFavorite" runat="server" AutoPostBack="True" Text="Favorite Reports"
                                                    OnCheckedChanged="LinkingMethod_Changed" GroupName="LinkingMethod" meta:resourcekey="LinkFavoriteResource1">
                                                </asp:RadioButton>
                                                <asp:RadioButton ID="LinkGeneral" runat="server" AutoPostBack="True" Text="URL" OnCheckedChanged="LinkingMethod_Changed"
                                                    GroupName="LinkingMethod" meta:resourcekey="LinkGeneralResource1"></asp:RadioButton>
                                                <asp:RadioButton ID="LinkAdHoc" runat="server" AutoPostBack="True" Text="Report"
                                                    OnCheckedChanged="LinkingMethod_Changed" GroupName="LinkingMethod" meta:resourcekey="LinkAdHocResource1">
                                                </asp:RadioButton>
                                                <label for="txtProxy" class="NoShow">
                                                    Proxy</label>
                                                <input id="txtProxy" class="hidden" type="text" size="3" value="0" runat="server" />
                                                <asp:CustomValidator ID="cvPanelValidator" runat="server" ControlToValidate="txtProxy"
                                                    ValidationGroup="grpPanel" ErrorMessage="<%$ Resources:Errors,Err_RequiredContent %>"
                                                    EnableClientScript="False" OnServerValidate="IsPanelValid">*</asp:CustomValidator>
                                                <input id="LinkReportID" type="hidden" runat="server" />
                                                <input id="FolderTypeID" type="hidden" runat="server" />
                                                <table>
                                                    <tr>
                                                        <td>
                                                            <asp:Label ID="lblDisplayCaption" runat="server"></asp:Label></td>
                                                        <td>
                                                            <asp:Label ID="lblDisplay" runat="server" BorderWidth="1px" BorderStyle="Inset" Width="200px"
                                                                Height="16px"></asp:Label>
                                                            <asp:ImageButton ID="btnFind" runat="server" ImageUrl="../ahImages/iconFind.gif"
                                                                CausesValidation="False" AlternateText="Change" meta:resourcekey="btnFindResource1">
                                                            </asp:ImageButton>
                                                            <table id="tbLinkURL" runat="server">
                                                                <tr>
                                                                    <td>
                                                                        <asp:Label ID="lblLinkURL" runat="server" CssClass="NoShow" Text="URL" AssociatedControlID="LinkURL"
                                                                            meta:resourcekey="lblLinkURLResource1"></asp:Label>
                                                                        <input id="LinkURL" type="text" maxlength="255" size="60" runat="server" />
                                                                        <asp:RequiredFieldValidator ID="rtvLinkURL" runat="server" ControlToValidate="LinkURL"
                                                                            ErrorMessage="URL is required." ValidationGroup="URL">*</asp:RequiredFieldValidator>
                                                                        <asp:RegularExpressionValidator ID="revLinkURL" runat="server" ControlToValidate="LinkURL"
                                                                            ErrorMessage="<%$ Resources:Errors, Err_InvalidURL %>" ValidationExpression="https?://([\w-])+[\w-:\.]+(/[\w- ./?%&=]*)?"
                                                                            ValidationGroup="URL">*</asp:RegularExpressionValidator>
                                                                    </td>
                                                                    <td>
                                                                        <AdHoc:LogiButton ID="btnTestURL" Text="<%$ Resources:LogiAdHoc, TestURL %>" CausesValidation="false"
                                                                            runat="server" OnClientClick="popupWin()" UseSubmitBehavior="false" />
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>
                                                <div id="divPreDefined" runat="server" style="background-color: White; border: solid 1px gray;">
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                <asp:Label ID="lbllstPredefinedPanel" runat="server" CssClass="NoShow" Text="Predefined Report"
                                                                    AssociatedControlID="lstPredefinedPanel" meta:resourcekey="lbllstPredefinedPanelResource1"></asp:Label>
                                                                <asp:ListBox ID="lstPredefinedPanel" runat="server">
                                                                    <asp:ListItem Value="1" Text="<%$ Resources:LogiAdHoc, stdRptFrequent %>" />
                                                                    <asp:ListItem Value="2" Text="<%$ Resources:LogiAdHoc, stdRptRecent %>" />
                                                                </asp:ListBox>
                                                            </td>
                                                            <td>
                                                                <AdHoc:LogiButton ID="btnPredefinedOK" Width="50px" runat="server" Text="<%$ Resources:LogiAdHoc, OK %>"
                                                                    OnClick="btnPredefinedOK_Click" CausesValidation="False" />
                                                                <br />
                                                                <AdHoc:LogiButton ID="btnPredefinedCancel" Width="50px" runat="server" Text="<%$ Resources:LogiAdHoc, Cancel %>"
                                                                    OnClick="btnPredefinedCancel_Click" CausesValidation="False" />
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </div>
                                                <div id="divFavorite" runat="server" style="background-color: White; border: solid 1px gray;">
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                <AdHoc:SelectFolderControl ID="ucSelectFolders" runat="server" />
                                                            </td>
                                                            <td width="50px">
                                                                <AdHoc:LogiButton ID="btnFavoriteOK" Width="50px" runat="server" Text="<%$ Resources:LogiAdHoc, OK %>"
                                                                    OnClick="btnFavoriteOK_Click" CausesValidation="False" />
                                                                <br />
                                                                <AdHoc:LogiButton ID="btnFavoriteCancel" Width="50px" runat="server" Text="<%$ Resources:LogiAdHoc, Cancel %>"
                                                                    OnClick="btnFavoriteCancel_Click" CausesValidation="False" />
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </div>
                                                <div id="divReports" runat="server" style="background-color: White; border: solid 1px gray;">
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                <AdHoc:SelectReportControl ID="ucSelectReports" DisAllowDashboards="true" runat="server" />
                                                            </td>
                                                            <td>
                                                                <AdHoc:LogiButton ID="btnFVOk" Width="50px" runat="server" Text="<%$ Resources:LogiAdHoc, OKWithSpaces %>"
                                                                    OnClick="btnFVOk_Click" CausesValidation="False" />
                                                                <br />
                                                                <AdHoc:LogiButton ID="btnFVCancel" Width="50px" runat="server" Text="<%$ Resources:LogiAdHoc, Cancel %>"
                                                                    OnClick="btnFVCancel_Click" CausesValidation="False" />
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </div>
                                            </td>
                                        </tr>
                                    </table>
                                    <table>
                                        <tr>
                                            <td>
                                                <br />
                                                <AdHoc:LogiButton ID="btnSavePanel" runat="server" Text="Save Panel" ValidationGroup="grpPanel"
                                                    meta:resourcekey="btnSavePanelResource1" />
                                                <AdHoc:LogiButton ID="btnCancelPanel" runat="server" Text="<%$ Resources:LogiAdHoc, Cancel %>"
                                                    CausesValidation="False" meta:resourcekey="btnCancelPanelResource1" />
                                            </td>
                                        </tr>
                                    </table>
                                    <asp:ValidationSummary ID="vsummaryPanel" runat="server" ValidationGroup="grpPanel" />
                                    <asp:ValidationSummary ID="ValidationSummary3" runat="server" ValidationGroup="URL" />
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </div>
                    </asp:Panel>
                    &nbsp;
                    <asp:UpdatePanel ID="UPDashboardVSummary" runat="server" >
                        <ContentTemplate>
                            <asp:ValidationSummary ID="vsummary" runat="server" ValidationGroup="Dashboard" meta:resourcekey="vsummaryResource1" />
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </asp:Panel>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </form>
</body>
</html>
