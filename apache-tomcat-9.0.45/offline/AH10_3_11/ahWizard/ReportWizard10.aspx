<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="ReportWizard10.aspx.vb" Inherits="LogiAdHoc.ahWizard_ReportWizard10" Culture="auto" meta:resourcekey="PageResourceRZ" UICulture="auto" %>

<%@ Register Assembly="LGXAHWCL" Namespace="LGXAHWCL" TagPrefix="cc1" %>
<%@ Register TagPrefix="AdHoc" TagName="MainMenu" Src="~/ahControls/MainMenu.ascx" %>
<%@ Register TagPrefix="AdHoc" TagName="BreadCrumbTrail" Src="~/ahControls/BreadCrumbTrail.ascx" %>
<%@ Register TagPrefix="wizard" TagName="ScrollTab" Src="~/ahControls/ScrollTab.ascx" %>
<%@ Register TagPrefix="wizard" TagName="navbtns" Src="~/ahControls/wizNavButtons.ascx" %>
<%@ Register TagPrefix="wizard" TagName="datebox" Src="../ahControls/NamedDateBox.ascx" %>
<%@ Register tagprefix="wizard" tagname="datebox1" src="../ahControls/DateBox.ascx" %>
<%@ Register TagPrefix="wizard" TagName="SpecialValue" Src="../ahControls/SpecialValue.ascx" %>
<%@ Register TagPrefix="wizard" TagName="numbox" Src="../ahControls/NumBox.ascx" %>
<%@ Register TagPrefix="wizard" TagName="DatabaseValues" Src="../ahControls/DatabaseValues.ascx" %>
<%@ Register TagPrefix="AdHoc" TagName="ColorSequence" Src="~/ahControls/ColorSequence.ascx" %>
<%@ Register TagPrefix="AdHoc" TagName="PickFolder" Src="~/ahControls/PickFolder.ascx" %>
<%@ Register TagPrefix="AdHoc" TagName="ScrollableListBox" Src="~/ahControls/ScrollableListBox.ascx" %>
<%@ Register TagPrefix="AdHoc" TagName="SelectReportControl" Src="~/ahControls/SelectReportControl.ascx" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title>Report Builder</title>
    <link rel="shortcut icon" href="../ahImages/flav.ico" />
    <script language="JavaScript" type="text/javascript" src="../ahScripts/ahChange.js"></script>
    <script src="../rdTemplate/yui/build/yahoo-dom-event/yahoo-dom-event.js" type="text/javascript"></script>
    <script src="../rdTemplate/yui/build/animation/animation-min.js" type="text/javascript"></script>
    <script src="../rdTemplate/yui/build/dragdrop/dragdrop-min.js" type="text/javascript"></script>
    <script language="JavaScript" type="text/javascript" src="../ahScripts/ahReorderGrid.js"></script>
    <script type="text/javascript" src="../ahScripts/jscolor.js"></script>
    <script language="JavaScript" type="text/javascript" src="../ahScripts/ahReportWizard.js"></script>

    <script type="text/javascript">
        //window.onbeforeunload = AlertOnExit;
        //window.onbeforeunload = ConfirmUnload;
    </script>

    <style type="text/css">
    .popupMenu{
	    background-color:#FFF8C6;
	    border-width:1px;
	    border-style:solid;
	    border-color:Gray;
	    padding:1px;
    }
    .settingsPopupControl{
	    background-color:White;
	    position:absolute;
	    visibility:hidden;
    }
    .rwImageButtonHover{
        background-color:#FFFFCC;
    }
    </style>

    <script type="text/javascript">
    <!--
    //Since we are hiding and showing Select report controls
    //This function needs to be declared here.
    function PanelResizing()
    {
        var d1 = document.getElementById('ucSelectReports_pnlTreeView');
        var d2 = d1.childNodes[0].childNodes[0];
        var pnl_width = parseInt(d1.style.width);
        if (d2) {
            if (parseInt(d2.style.left) == 3) {
                // This is the buggy initial state of splitter
                pnl_width = 200;
                d1.style.width = pnl_width + 'px';
                d2.style.left = '197px';
                var d3 = d1.childNodes[1];
                d3.style.width = pnl_width + 'px';
                var d4 = document.getElementById('ucSelectReports_RCE_ClientState');
                d4.setAttribute('value', '200,200');
            }
        }
        var pnl_height = parseInt(d1.style.height);
        document.getElementById('ucSelectReports_divTreeView').style.width = pnl_width + 'px';
        document.getElementById('ucSelectReports_divTreeView').style.height = pnl_height + 'px';
        document.getElementById('ucSelectReports_divListBox').style.width = (600 - pnl_width) + 'px';
        document.getElementById('ucSelectReports_divListBox').style.height = pnl_height + 'px';
        ScrollableListBoxRefineHeightAndWidth(document.getElementById('ucSelectReports_lstReports'), 200, 500);
    }

    function ExpandCollapse(divID, imgID) {
        var d = document.getElementById(divID);
        var i = document.getElementById(imgID);
        if (d.style.display=="none") {
            d.style.display="";
            i.src = "../ahImages/collapse_blue.jpg";
        } else {
            d.style.display="none";
            i.src = "../ahImages/expand_blue.jpg";
        }
    }

	var lSelected=-1;
	function getDescription(id) {
		if (lSelected != -1) {
			//hide the currently visible explanation
			var e = document.getElementById('ahExp'+lSelected);
			if (e != null) e.style.display = 'none';
			lSelected = -1;
		}

		var el = document.getElementById(id);
		var i;
		try	{
			for (i=0; i<el.options.length; i++) {
				if (el.options[i].selected) {
					var l = el.options[i].value;
					var s=document.getElementById('ahExp'+l);
					if (s != null) {
						lSelected = l;
						s.style.display='';
					}
					break;
				}
			}
		}
		catch(exception) {
		}
		finally {
		}
	}
    var SelectRowIndex = -1;
	function SelectRow(e)
	{
	    if(!e) e = window.event;//Added this for IE

		var obj = e.srcElement;
		if(!obj) obj = e.target;//Added this for Firefox
		if (obj && obj.tagName=="TD") //this a table cell
		{
		    //get a pointer to the tablerow
		    var row = obj.parentNode;
		    var iAdj = parseInt(document.getElementById("javaGrdColumnFormat").value); //1=Java 2=.Net

		    if (!e.ctrlKey && !e.shiftKey)
		    {
		        var table = row.parentNode;
		        for (var i=1;i<table.childNodes.length;i++) {
		            if (table.childNodes[i].tagName == "TR")//Added this for Firefox
		            {
//		                var chk = table.childNodes[i].cells[0].firstChild;
//		                if (chk.checked)
//		                {
//		                    chk.checked = false
		                    table.childNodes[i].className="";
//		                }
		            }
		        }
                var oRows = document.getElementById('grdColumnFormat').getElementsByTagName('tr');                var l = oRows.length;
                var chk;
                var s;
                for (i=iAdj; i<=l; i++) { //i=2
	                var o = pack(i.toString(10),2);
		            s = "grdColumnFormat_ctl" + o + "_chk_Select";
		            chk = document.getElementById(s);
		            if (chk!=null) chk.checked = false;
	            }
		    }

		    if (e.shiftKey && SelectRowIndex >= 0)
		    {
		        if (SelectRowIndex > row.rowIndex)
		        {
		            var table = row.parentNode;
		            for (var i=row.rowIndex;i<SelectRowIndex;i++) {
		                var idx = i+iAdj-1;
		                var o = pack(idx.toString(10),2);
		                var s = "grdColumnFormat_ctl" + o + "_chk_Select";
		                //var chk = table.childNodes[i].cells[0].firstChild;
		                var chk = document.getElementById(s);
		                if(!chk.disabled)
		                {
		                    chk.checked = true;
		                    table.childNodes[i].className="SelectedRow";
		                }
		            }
		        }
		        else
		        {
		            var table = row.parentNode;
		            for (var i=row.rowIndex;i>SelectRowIndex;i--) {
		                var idx = i+iAdj-1;
		                var o = pack(idx.toString(10),2);
		                var s = "grdColumnFormat_ctl" + o + "_chk_Select";
		                var chk = document.getElementById(s);
		                //var chk = table.childNodes[i].cells[0].firstChild;
		                if(!chk.disabled)
		                {
		                    chk.checked = true;
		                    table.childNodes[i].className="SelectedRow";
		                }
		            }
		        }
		    }
		    else
		    {
		        var idx = row.rowIndex+iAdj-1; // Adjust here for Java
		        var o = pack(idx.toString(10),2);
                var s = "grdColumnFormat_ctl" + o + "_chk_Select";
                var chk = document.getElementById(s);
		        //var chk = row.cells[0].firstChild;
		        if(!chk.disabled)
		        {
		            chk.checked = true;
		            if (chk.checked)
		               row.className="SelectedRow";
		            else
		               row.className="";
		        }
		    }
		    SelectRowIndex = row.rowIndex
		}
	}
	function MouseOver(select, e)
	{
	    if(!e) e = window.event;//Added this for IE
	    
	    var obj = e.srcElement;
	    if(!obj) obj = e.target;//Added this for Firefox
		if (obj && obj.tagName=="TD") //this a table cell
		{
		    var iAdj = parseInt(document.getElementById("javaGrdColumnFormat").value); //1=Java 2=.Net
		    //get a pointer to the tablerow
		    var row = obj.parentNode;
		    var idx = row.rowIndex+iAdj-1;
		    var o = pack(idx.toString(10),2);
            var s = "grdColumnFormat_ctl" + o + "_chk_Select";
            var chk = document.getElementById(s);
		    //var chk = row.cells[0].firstChild;
		    if (chk && !chk.disabled)
		    {
		        if (select && !chk.checked)
		        {
		            row.className="SelectedRow";
		        }
		        else
		        {
		            if (chk.checked)
		            {
		               row.className="SelectedRow";
		            }
		            else
		            {
		               row.className="";
		            }
		        }
		     }
		}
	}
	function SelectRow_OnClick(chkVal, row)
	{
	    if (chkVal)
	        row.className="SelectedRow";
	    else
	        row.className="";
	}
	function SelectAll_OnClick(chkVal, idVal, row)
	{
	    var oRows = document.getElementById('grdColumnFormat').getElementsByTagName('tr');
        var l = oRows.length;
        //var l = grdMain.rows.length;
        var e;
        var s;
        if (idVal.indexOf ('CheckAll') != -1) {
	        var bSet; 
	        // Check if main checkbox is checked, then select or deselect GridView checkboxes
	        if (chkVal == true) {
		        bSet = true;
	        } else {
		        bSet = false;
	        }
	        // Loop through all elements
	        var iAdj = parseInt(document.getElementById("javaGrdColumnFormat").value); //1=Java 2=.Net
	        for (i=iAdj; i<=l; i++) {
	            var o = pack(i.toString(10),2);
		        s = "grdColumnFormat_ctl" + o + "_chk_Select";
		        e = document.getElementById(s);
		        if (e!=null) e.checked = bSet;
	        }
	        var table = row.parentNode;
	        if (table.tagName == "TR") 
	            table = table.parentNode;
	        for (var i=1;i<table.childNodes.length;i++) {
	            if (table.childNodes[i].tagName == "TR")//Added this for Firefox
	            {
	                if (bSet)
	                    table.childNodes[i].className="SelectedRow";
	                else
	                    table.childNodes[i].className="";
	            }
	        }
        }
	}
	function confirmColumnDelete(frm, cnfMsg, errMsg)
    {
      // loop through all elements
      for (i = 0; i < frm.length; i++)
      {
        // Look for our checkboxes only
        if (frm.elements[i].name.indexOf("chk_Select") !=  - 1)
        {
          // If any are checked then confirm
          if (frm.elements[i].checked)
            if (cnfMsg == "")
                return true
            else
                return confirm(cnfMsg)
        }
      }
      //No Checkboxes have been checked so show an error message
      alert(errMsg)
      return false
    }
    function imgIframe_RefreshUploadImage()
    {
        //document.form1.btnUploaded.click();
        //document.forms[0].btnUploaded.click();
        var x = document.getElementById("btnUploaded");
        x.click();
    }
    function dsIframe_DataSourceDone(bApply)
    {
        $find('mpeDataSourceBehavior').hide();
        if (bApply) {
            var x = document.getElementById("btnDataSourceDone");
            x.click();
        }
    }
    function FakeValidation(sender, args)
    {
        args.IsValid = true;
    }
    
    function checkPreview() {
        if (document.getElementById("ahSectionPreview").value != "True") {
            return false;
        } else {
            return true;
        }
    }         
    function ConfirmSelectMove(ob, cnfMsg){
        for (var i = 0; i < ob.options.length; i++)
        if (!ob.options[i].selected)
            return true;
        //return confirm("Removing all Grouped Columns will remove all Grouped Aggregates.\n\nAre you sure you want to remove all columns?");
        return confirm(cnfMsg);
    }
    
    //------------------Preview Functionality-------------------------------------------------------------
    //Change in "ReportPreviewWin" name will need to be applied to function lnkbPreview_Click
    var previewPopupWin=null;
    var ReportPreviewWin=null;
    var bNavigatingAway = false;
    
    function OpenPreviewInPopup(srcURL)
    {
        //var ifrm = document.getElementById("ifPreview");
        var WinSettings = "center=yes,resizable=yes,status=no,help=no,scroll=yes,height=260px,width=480px,toolbar=no,directories=no,menubar=no,scrollbars=yes,modal=no,titlebar=no";
        //previewPopupWin = window.open(ifrm.src, 0, WinSettings);
        previewPopupWin = window.open(srcURL, 0, WinSettings);
    }
    function RefreshPreviewInPopup(srcURL)
    {
        if (previewPopupWin)
        {
            if (!previewPopupWin.closed)
            {
                previewPopupWin.SetReload(true);
                //var ifrm = document.getElementById("ifPreview");
                //previewPopupWin.location.href = src;
                previewPopupWin.location.replace(srcURL);
                //previewPopupWin.location.reload();
                //previewPopupWin.SetURL(srcURL);
            }
        }
    }
    function ClosePreviewPopup(bCloseWindow)
    {
        if (previewPopupWin)
        {
            if (!previewPopupWin.closed)
            {
                if(bCloseWindow)
                {
                    previewPopupWin.close();
                }
            }
            var btnClosePopup = document.getElementById("btnClosePopupPreview");
            btnClosePopup.click();
            previewPopupWin = null;
        }
    }
    
    function ConfirmUnload(e)
    {
        bNavigatingAway = false;
        if((!isPostBack) && (!noMessage))
            bNavigatingAway = true;
            
        if ((document.getElementById('ahDirty').value==1) && (!isPostBack) && (!noMessage)) {
            return 'Any changes you have made to data on this page will be lost.';
       }
       noMessage=false;
    }
    function CloseAllPopups()
    {
        if (bNavigatingAway)
        {
            if (previewPopupWin)
            {
                if (!previewPopupWin.closed)
                {
                    previewPopupWin.SetReload(true);
                    previewPopupWin.close();
                }
                previewPopupWin = null;
            }
            if (ReportPreviewWin)
            {
                if (!ReportPreviewWin.closed)
                {
                    ReportPreviewWin.close();
                }
                ReportPreviewWin = null;
            }
        }
    }
    window.onbeforeunload = ConfirmUnload;
    window.onunload = CloseAllPopups;
    
    function ResizeDivForScreenResolution()
    {
        var d = document.getElementById("divGroupingDetailsPopup");
        if (screen.height <= 1000)
        {
            d.style.height = 500;
            d.style.overflow = "auto";
        }
        else
        {
            //d.style.height = 600;
            //d.style.overflow = "auto";
        }
    }

    var activeMenuTab;
    
    function applyMenuHover(o) {
        if (o != activeMenuTab) o.className="tab_hover";
    }
    
    function applyMenuActive(o) {
        o.className="tab_active";
        if (activeMenuTab) {
            activeMenuTab.className="";
            document.getElementById('div'+activeMenuTab.id.substring(2)).style.display="none";
        }
        activeMenuTab=o;
        document.getElementById('div'+activeMenuTab.id.substring(2)).style.display="block";
        
        var ih = document.getElementById('ihSelectedTab');
        ih.value = 'div'+activeMenuTab.id.substring(2);
    }
    
    function applyMenuNone(o) {
        if (o != activeMenuTab) o.className="";
    }
    function restoreActiveTab() {
        if (activeMenuTab) {
            var tab = document.getElementById(activeMenuTab.id);
            activeMenuTab = tab; //THis is running after a postback.
//            tab.className="tab_active";
//            document.getElementById('div'+activeMenuTab.id.substring(2)).style.display="block";
        }
    }
    
    function DataSourceSelect_OnChange(parentID)
    {
        if(!confirm('Are you sure you want to change the data source for active content?'))
        {
            document.getElementById(parentID).selectedIndex = document.getElementById('ihLastDataSourceID').value
            return false;
        }
        
        document.getElementById('ihLastDataSourceID').value = document.getElementById(parentID).selectedIndex
        return true;
    }

//    function goHelp(someID) 
//    {
//        var aHlp = document.getElementById('aHelp');
//        aHlp.setAttribute("href","../Help.aspx?src=" + someID);
//        aHlp.click();
//    }
    function goHelp(someID) 
    {
        var aHlp = document.getElementById('aHelp');
        aHlp.setAttribute("href","../Help.aspx?src=" + someID);
        //aHlp.click();
        
        if (aHlp.click) 
        {
            aHlp.click(); 
        }
        else
        {
            window.open(aHlp.href, "help");
        }
    }
    
    -->
    </script>
    
    <script type="text/javascript">
    <!--
        YAHOO.util.Event.onContentReady("iconBar", function () {
            initializeMenuDragDrop('iconBar');
            //Load the panels list from saved hidden variable.
            //LoadPanelsList();
        });
        
        function fireBeforeFormSubmit() {
            //Save the panels structure to the saved hidden variable.
            //SavePanelsList();
        }
        
        function fireAfterPostbackComplete() {
            //LoadPanelsList();
            initializeMenuDragDrop('iconBar');
        }
    -->
    </script>
    
    <script language="javascript" type="text/javascript">
<!--
    var activeTab;
    var xCoord = 0  ; //Starting Location - left
    var dest_x = 0 ;  //Ending Location - left
    var interval = 8; //Move 10px every initialization
    var _timer = null;
    
    function applyHover(o) {
        if (o != activeTab) o.className="tab_hover";
    }
    
    function applyActive(o) {
        if (activeTab) activeTab.className="";
//        o.className="tab_active";
        activeTab=o;
        
        var i = o.id.lastIndexOf("_") ;
        var pre = "";
        if (i != -1) {
            pre = o.id.substring(0, i + 1);
        }
        var act = document.getElementById(pre + 'actTab');
        act.value=activeTab.id;
        var lnk = document.getElementById('scTabWiz_btnSC');
        noMessage = true;
        lnk.click();
    }
    
    function applyNone(o) {
        if (o != activeTab) o.className="";
    }

    function moveSCLeft() {
	    //Keep on moving the image till the target is achieved
	    if(xCoord>dest_x) xCoord = xCoord - interval; 
	    //Move the image to the new location
	    document.getElementById('scTabWiz_divContainer').style.left = xCoord+'px';
	    if ((xCoord <= dest_x)) {
            if (_timer) {
                clearInterval(_timer);
                _timer = null;
            }
	    }
    }

    function moveSCRight() {
	    //Keep on moving the image till the target is achieved
	    if(xCoord<dest_x) {xCoord = xCoord + interval; }
    	
	    //Move the image to the new location
	    document.getElementById('scTabWiz_divContainer').style.left = xCoord+'px';
	    if ((xCoord >= dest_x)) {
            if (_timer) {
                clearInterval(_timer);
                _timer = null;
            }
	    }
    }

    function moveSC(where) {
    /*
        var i = activeTab.id.lastIndexOf("_") ;
        var pre = "";
        if (i != -1) {
            pre = o.id.substring(0, i + 1);
        }
        var dv = document.getElementById(pre + 'divContainer');        
        */
        var howMuch = 112;
        //var dv = document.getElementById('scTabWiz_divContainer');
        if (where == 0) {            
            dest_x = xCoord + howMuch;
            if (dest_x > 0) dest_x = 0;
		    _timer = setInterval('moveSCRight()',2);
        } else {
            dest_x = xCoord - howMuch;
            var maxLeft = document.getElementById('divTab').offsetWidth - document.getElementById('scTabWiz_divContainer').offsetWidth;
            if (dest_x < maxLeft) dest_x = maxLeft;
		    _timer = setInterval('moveSCLeft()',2);
        }
    }
-->    
</script>

    <script language="javascript" type="text/javascript">
    <!--
    function SelectChartType(divID)
    {
        var div = document.getElementById(divID);
        if (div)
        {
            div.className="rwImageButtonSelected";
            
            var ih = document.getElementById("ihSelectedChartType");
            var currDivID = "btnSelectChartType" + ih.value;
            var oldDiv = document.getElementById(currDivID);
            
            oldDiv.className = "rwImageButton";
            ih.value = div.getAttribute("ahChartTypeID");
        }
    }
    function SelectChartTypeAndClose(divID)
    {
        SelectChartType(divID);
        
        var btn = document.getElementById("btnCSSelectChartTypeOK");
        btn.click();
    }
    -->
    </script>
</head>
<body onresize="resizeTabs();" onload="resizeTabs();">
    <AdHoc:MainMenu ID="menu" runat="server" />
    <form id="form1" runat="server">
        <AdHoc:BreadCrumbTrail ID="bct" runat="server" Key="RptBuilder" />
        <asp:ScriptManager ID="ScriptManager1" runat="server" />
        
        <script type="text/javascript">
        var rm = Sys.WebForms.PageRequestManager.getInstance();
//        rm.add_beginRequest(BeginRequestHandler);
//        function BeginRequestHandler(sender, args) {
//            $find('ahSavePopupBehavior').show();
//        }
        rm.add_initializeRequest(InitializeRequest);
        function InitializeRequest(sender, args) { 
            var prm = Sys.WebForms.PageRequestManager.getInstance();
            if (prm.get_isInAsyncPostBack()) {
                $find('ahSavePopupBehavior').hide();
                args.set_cancel(true);
            }
        }
        
        rm.add_endRequest(EndRequestHandler);
        function EndRequestHandler(sender, args) {
            restoreActiveTab();
            resizeTabs();
            document.body.style.cursor = "default"; 
            noMessage = false;
            $find('ahSavePopupBehavior').hide();
            fireAfterPostbackComplete();
            LoadPopupPos();
//UpdateProgress1
//            $get('UpdateProgress1').style.display = 'none'; 
        }

        var styleToSelect;
        
//        function ChangeStyle() {
//            var s = document.getElementById("RptStyle");
//            var i = document.getElementById("StyleSample");
//            i.src = "../ahImages/" + s.options(s.selectedIndex).value;
//        }
        
        Type.registerNamespace('AHScripts');
        AHScripts.ResetCaret = function() {}
        AHScripts.ResetCaret.prototype = {
            resCaret: function() {
                lastCaretPos=null;
                lastCaretStart=null;
                lastCaretEnd=null;
            }
        }
        AHScripts.ResetCaret.registerClass('AHScripts.ResetCaret');
        
        var panelUpdated = new AHScripts.ResetCaret();
        var postbackElement;
        rm.add_beginRequest(beginRequest);
        rm.add_pageLoaded(pageLoaded);
        
        function beginRequest(sender, args) {
            postbackElement = args.get_postBackElement();
            document.body.style.cursor = "wait"; 
//            $find('ahSavePopupBehavior').show();
//            $get('UpdateProgress1').style.display = 'block'; 
            SavePopupPos();
        }
        function pageLoaded(sender, args) {
            panelUpdated.resCaret();
        }
        
        var lastCaretPos;
        var lastCaretStart;
        var lastCaretEnd;

        // Places the symbol at the end of the text in the formula box.
        function appendSymbol( symbol, txtID )
        {
	        // Symbols can be only one character long.
	        if (symbol.length = 1)
	        {
	            if (!txtID) txtID = "txtFormula";
	            var target = document.getElementById( txtID );
		        if (target != null)
		        {
			        insertAtCaret(target, " " + symbol + " ");
		        }
	        }
	        else
	        {
		        alert("Invalid operator.");
	        }
        }

        function appendColumn(txt,bAsIs,txtID,bSummary)
        {
	        //var el = document.getElementById("ColumnPicker");
	        
	        if (!txtID) txtID = "txtFormula";
	        var target = document.getElementById( txtID );
	        if (target != null)
	        {
	            if (bAsIs) {
        	        insertAtCaret(target, " ( " + txt + " ) ");
        	    }
        	    else {
        	        if (bSummary) {
        	            var ddl = document.getElementById("ddlSummaryCalcFunction");
        	            if (ddl != null) {
        	                var sFunc = ddl.options[ddl.selectedIndex].value;
        	                insertAtCaret(target, " " + sFunc + " (�" + txt.replace(".", "�.�") + "�) ");
        	            }
        	        }
        	        else {
        	            insertAtCaret(target, " �" + txt.replace(".", "�.�") + "� ");
        	        }
        	    }
        	}
        }

        // The functions below insert text at a specified position in a textarea.
        // The code below only works for IE, since it relies on createTextRange().
        // Inserts will occur at the end of the textarea for all other browsers.
        // See http://www.faqts.com/knowledge_base/view.phtml/aid/1052/fid/130

        // Adjusts the caret position.
        // Used in conjunction with the [storeCaret] function.
        function setCaretToEnd (el) {
          if (el.createTextRange) {
            var v = el.value;
            var r = el.createTextRange();
            r.moveStart('character', v.length);
            r.select();
          }
        }

        // Inserts text at the current location of a cursor in an html form element.
        // Used in conjunction with the [storeCaret] function.
        function insertAtCaret(el, txt) {
          if (lastCaretPos) {
	        el.range = lastCaretPos;
            el.range.text = el.range.text.charAt(el.range.text.length - 1) != ' ' ? txt : txt + ' ';
            el.range.select();
          }
          else if (lastCaretStart && lastCaretEnd) {
            var s = el.value;
            el.value = s.substring(0, lastCaretStart) + txt + s.substring(lastCaretEnd, s.length);
            lastCaretStart = lastCaretStart + txt.length;
            lastCaretEnd = lastCaretStart;
          }
          else {
            insertAtEnd(el, txt);
          }
        }

        // Stores the current location of a cursor in an html form element.
        function storeCaret(txtID) {
          if (!txtID) txtID = "txtFormula";
          var el = document.getElementById( txtID );
          if (el != null) {
              if (el.createTextRange)
                lastCaretPos = document.selection.createRange().duplicate();
              else {
                lastCaretStart = el.selectionStart;
                lastCaretEnd = el.selectionEnd;
              }
          }
        }

        // Inserts text at the end of an html form element.
        function insertAtEnd(el, txt) {
          el.value += txt;
          setCaretToEnd (el);
        }

        function toggleScale(id) {
            var e = document.getElementById(id);
            var x = document.getElementById('txtHdrScale');
            if (x.value == "px") {
                x.value = '%';
                __doPostBack('txtHdrScale','%');
            } else {
                x.value = 'px';
                __doPostBack('txtHdrScale','px');
            }
        }
        
        function SavePopupPos() {
            SavePopupPosition('mpeDataSourceBehavior');
            SavePopupPosition('ahModalPopupBehavior');
            SavePopupPosition('mpeSettingsBehavior');
            SavePopupPosition('mpeOpenBehavior');
            SavePopupPosition('mpePagingBehavior');            
            SavePopupPosition('mpeCalcColumnBehavior');
            SavePopupPosition('mpeStatColumnBehavior');
            //SavePopupPosition('mpeColumnDetailsBehavior');
            SavePopupPosition('mpeColumnSummaryBehavior');
            SavePopupPosition('mpeVisualizationBehavior');
            SavePopupPosition('mpeAppearanceBehavior');
            SavePopupPosition('mpeParamDetailsBehavior');
            SavePopupPosition('mpeLayerDetailsBehavior');
            SavePopupPosition('mpeCrosstabsAddLabelColsBehavior');
            SavePopupPosition('mpeCrosstabsAddValueColsBehavior');
            SavePopupPosition('mpeCTStyleBehavior');
            SavePopupPosition('mpePickFromServerBehavior');
            SavePopupPosition('mpePickFromClientBehavior');
            SavePopupPosition('mpeCFCalcColumn');
        }
        
        function LoadPopupPos() {
            RestorePopupPosition('mpeDataSourceBehavior');
            RestorePopupPosition('ahModalPopupBehavior');
            RestorePopupPosition('mpeSettingsBehavior');
            RestorePopupPosition('mpeOpenBehavior');
            RestorePopupPosition('mpePagingBehavior');            
            RestorePopupPosition('mpeCalcColumnBehavior');
            RestorePopupPosition('mpeStatColumnBehavior');
            //RestorePopupPosition('mpeColumnDetailsBehavior');
            RestorePopupPosition('mpeColumnSummaryBehavior');
            RestorePopupPosition('mpeVisualizationBehavior');
            RestorePopupPosition('mpeAppearanceBehavior');
            RestorePopupPosition('mpeParamDetailsBehavior');
            RestorePopupPosition('mpeLayerDetailsBehavior');
            RestorePopupPosition('mpeCrosstabsAddLabelColsBehavior');
            RestorePopupPosition('mpeCrosstabsAddValueColsBehavior');
            RestorePopupPosition('mpeCTStyleBehavior');
            RestorePopupPosition('mpePickFromServerBehavior');
            RestorePopupPosition('mpePickFromClientBehavior');
            RestorePopupPosition('mpeCFCalcColumn');
        }
        </script>
        
        <div class="divForm">
            <a id="aHelp" target="_blank"></a>
<%--            <asp:HyperLink ID="hlTest" runat="server" Target="_blank" Text="test" />
--%>            
            <asp:UpdatePanel ID="UPahDirty" runat="Server" UpdateMode="Conditional" RenderMode="Inline">
                <ContentTemplate>
                    <input type="hidden" id="ahDirty" runat="server" />
                    <input type="hidden" id="ahPanelDirty" runat="server" />
                    <%--<input type="hidden" id="whatCase" runat="server" />--%>
                    <input type="hidden" id="ahSectionPreview" runat="server" />
                    <input type="hidden" id="javaGrdColumnFormat" runat="server" />
                </ContentTemplate>
            </asp:UpdatePanel>

            <input id="errMsg" runat="server" type="hidden" />
            <asp:Button runat="server" ID="btnFDataSource" Style="display: none" meta:resourcekey="btnFDataSourceResource1" />
            <ajaxToolkit:ModalPopupExtender runat="server" ID="mpeDataSource" BehaviorID="mpeDataSourceBehavior"
                TargetControlID="btnFDataSource" PopupControlID="pnlDataSourcePopup" BackgroundCssClass="modalBackground" PopupDragHandleControlID="pnlDragHandle1" RepositionMode="None" DynamicServicePath="" Enabled="True">
            </ajaxToolkit:ModalPopupExtender>
            
            <asp:Panel runat="server" CssClass="modalPopup" ID="pnlDataSourcePopup" Style="display: none; width: 806;" meta:resourcekey="pnlDataSourcePopupResource1">
                <asp:Panel ID="pnlDragHandle1" runat="server" Style="cursor: hand;" meta:resourcekey="pnlDragHandle1Resource1">
                <div class="modalPopupHandle" style="width: 800px;">
                    <table cellpadding="0" cellspacing="0" style="width: 100%"><tr><td>
                    <asp:Localize ID="locDataSource" runat="server" Text="Select or Modify Data Source" meta:resourcekey="locDataSourceResource1" />
                        </td>
                        <td style="width: 20px;">
                            <asp:ImageButton ID="ImageButton4" runat="server" 
                                OnClick="CloseDataPopup" CausesValidation="False"
                                SkinID="imgbClose" ImageUrl="../ahImages/remove.gif" 
                                AlternateText="<%$ Resources:LogiAdHoc, CloseWindow %>" />
                    </td></tr></table>
                </div>
                </asp:Panel>
                <div>
                    <asp:UpdatePanel ID="UPDataSource" runat="server" UpdateMode="Conditional" RenderMode="Inline">
                        <ContentTemplate>             
                        <asp:Button ID="btnDataSourceDone" runat="server" CssClass="NoShow" CausesValidation ="False"
                            OnClick="DataSourceDone" Text="Done" meta:resourcekey="btnDataSourceDoneResource1" /> 
                            <div id="divIFrameDS" runat="server">
                            </div>                                   
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
            </asp:Panel>
            
            <asp:UpdatePanel ID="UPMenu" runat="Server" UpdateMode="Conditional" RenderMode="Inline">
                <ContentTemplate>
                    <div class="tab_xp tab_default" style="width:800px; visibility: visible;" id="tbs">
                    <div id="divTabs" class="tab_header">
                        <span id="spTFile" runat="server" class="" onmouseover="applyMenuHover(this);" onmouseout="applyMenuNone(this);" onclick="applyMenuActive(this);"><span class="tab_outer"><span class="tab_inner"><span class="tab_tab"><asp:Literal runat="server" Text="File" ID="litFile" meta:resourceKey="litFile"></asp:Literal></span></span></span></span>
                        <span id="spTInsert" runat="server" class="" onmouseover="applyMenuHover(this);" onmouseout="applyMenuNone(this);" onclick="applyMenuActive(this);"><span class="tab_outer"><span class="tab_inner"><span class="tab_tab"><asp:Literal runat="server" Text="Insert" ID="litInsert" meta:resourceKey="litInsert"></asp:Literal></span></span></span></span><span id="spTSettings" runat="server" class="" onmouseover="applyMenuHover(this);" onmouseout="applyMenuNone(this);" onclick="applyMenuActive(this);"><span class="tab_outer"><span class="tab_inner"><span class="tab_tab"><asp:Literal runat="server" Text="Settings" ID="litSettings" meta:resourceKey="litSettings"></asp:Literal></span></span></span></span>
                        <asp:ImageButton ID="imgbQMobile" runat="server" OnCommand="MenuActions" CommandName="mobile" ImageUrl="~/ahImages/MobileOff.png" AlternateText="<%$ Resources:LogiAdHoc, Mobile %>" ToolTip="<%$ Resources:LogiAdHoc, MobileOn %>" />
                        <asp:ImageButton ID="imgbQSave" runat="server" OnCommand="MenuActions" CommandName="save" ImageUrl="~/ahImages/Save.png" AlternateText="<%$ Resources:LogiAdHoc, Save %>" ToolTip="<%$ Resources:LogiAdHoc, Save %>" />
                        <asp:UpdatePanel ID="UPQBtns" runat="server" UpdateMode="Conditional" RenderMode="Inline"><ContentTemplate>
                           <asp:ImageButton ID="imgbQPreview" runat="server" OnCommand="MenuActions" CommandName="preview" ImageUrl="~/ahImages/Preview.png" AlternateText="<%$ Resources:LogiAdHoc, Preview %>" ToolTip="<%$ Resources:LogiAdHoc, Preview %>" />
                            <asp:ImageButton ID="imgbQPaste" runat="server" OnCommand="MenuActions" CommandName="paste" ImageUrl="~/ahImages/Paste.png" Visible="False" meta:resourcekey="imgbQPasteResource1" />
                        </ContentTemplate></asp:UpdatePanel>
                    </div>
                    <input type="Hidden" id="ihSelectedTab" runat="server" value="divTFile" />
                    <div style="height: 50px; margin:0;" id="tbsbody" class="tab_body">    
                        <div id="divTFile" runat="server" class="tab_panel" style="display: none; margin:0;">
                        <table>
                        <tr>
                            <td class="tdSelectChartType"><asp:ImageButton ID="imgbNew" runat="server" OnCommand="MenuActions" CommandName="new" ImageUrl="~/ahImages/wiNew.png" AlternateText="New" ToolTip="New" meta:resourcekey="imgbNewResource1" />
                            <br /><asp:Localize ID="locNew" runat="server" Text="New" meta:resourcekey="locNewResource1" /></td>
                            <td class="tdSelectChartType"><asp:ImageButton ID="imgbOpen" runat="server" OnCommand="MenuActions" CommandName="open" ImageUrl="~/ahImages/wiOpen.png" AlternateText="Open" ToolTip="Open" meta:resourcekey="imgbOpenResource1" />
                            <br /><asp:Localize ID="locOpen" runat="server" Text="Open" meta:resourcekey="locOpenResource1" /></td>
                            <td class="tdSelectChartType"><asp:ImageButton ID="imgbSave" runat="server" OnCommand="MenuActions" CommandName="save" ImageUrl="~/ahImages/wiSave.png" AlternateText="<%$ Resources:LogiAdHoc, Save %>" ToolTip="<%$ Resources:LogiAdHoc, Save %>" />
                            <br /><asp:Localize ID="locSave" runat="server" Text="Save" meta:resourcekey="locSaveResource1" /></td>
                            <td class="tdSelectChartType"><asp:ImageButton ID="imgbSaveAs" runat="server" OnCommand="MenuActions" CommandName="saveas" ImageUrl="~/ahImages/wiSaveAs.png" AlternateText="<%$ Resources:LogiAdHoc, SaveAs %>" ToolTip="<%$ Resources:LogiAdHoc, SaveAs %>" />
                            <br /><asp:Localize ID="locSaveAs" runat="server" Text="Save As" meta:resourcekey="locSaveAsResource1" /></td>
                            <td class="tdSelectChartType"><asp:ImageButton ID="imgbPreview" runat="server" OnCommand="MenuActions" CommandName="preview" ImageUrl="~/ahImages/wiPreview.png" AlternateText="<%$ Resources:LogiAdHoc, Preview %>" ToolTip="<%$ Resources:LogiAdHoc, Preview %>" />
                            <br /><asp:Localize ID="locPreview" runat="server" Text="Preview" meta:resourcekey="locPreviewResource1" /></td>
                        </tr>
                        </table>
                        </div>
                        <div id="divTEdit" runat="server" class="tab_panel" style="display: none; margin:0;">
                        <asp:UpdatePanel ID="UPTEdit" runat="server" UpdateMode="Conditional" RenderMode="Inline"><ContentTemplate>
                        <table>
                        <tr>
                            <td class="tdSelectChartType"><asp:ImageButton ID="imgbCut" runat="server" OnCommand="MenuActions" CommandName="cut" ImageUrl="~/ahImages/wiCut.png" AlternateText="Cut" ToolTip="Cut" meta:resourcekey="imgbCutResource1" />
                            <br /><asp:Localize ID="locCut" runat="server" Text="Cut" meta:resourcekey="locCutResource1" /></td>
                            <td class="tdSelectChartType"><asp:ImageButton ID="imgbCopy" runat="server" OnCommand="MenuActions" CommandName="copy" ImageUrl="~/ahImages/wiCopy.png" AlternateText="Copy" ToolTip="Copy" meta:resourcekey="imgbCopyResource1" />
                            <br /><asp:Localize ID="locCopy" runat="server" Text="Copy" meta:resourcekey="locCopyResource1" /></td>
                            <td class="tdSelectChartType"><asp:ImageButton ID="imgbPaste" runat="server" OnCommand="MenuActions" CommandName="paste" ImageUrl="~/ahImages/wiPaste.png" AlternateText="Paste" ToolTip="Paste" meta:resourcekey="imgbPasteResource1" />
                            <br /><asp:Localize ID="locPaste" runat="server" Text="Paste" meta:resourcekey="locPasteResource1" /></td>
                        </tr>
                        </table>
                        </ContentTemplate></asp:UpdatePanel>
                        </div>
                        <div id="divTInsert" runat="server" class="tab_panel" style="display: none; margin:0;">
                        <asp:UpdatePanel ID="UPTInsert" runat="server" UpdateMode="Conditional" RenderMode="Inline"><ContentTemplate>
                        <table>
                        <tr id="iconBar" runat="server"></tr>
                        </table>
                        <div id="divExtraInsertIcons" runat="server">
                            <table id="tbExtraInsertIcons" runat="server">
                            </table>
                        </div>
                        </ContentTemplate></asp:UpdatePanel>
                        </div>
                        <div id="divTSettings" runat="server" class="tab_panel" style="display: none; margin:0;">
                        <table>
                        <tr>
                            <td id="tdPagingMenuOption" runat="server" class="tdSelectChartType"><asp:ImageButton ID="imgbPaging" runat="server" OnCommand="MenuActions" CommandName="paging" ImageUrl="~/ahImages/wiPaging.png" AlternateText="Paging" ToolTip="Paging" meta:resourcekey="imgbPagingResource1" />
                            <br /><asp:Localize ID="Localize1" runat="server" Text="Paging" meta:resourcekey="Localize1Resource1" /></td>
                            <td class="tdSelectChartType"><asp:ImageButton ID="imgbStyle" runat="server" OnCommand="MenuActions" CommandName="style" ImageUrl="~/ahImages/wiStyle.png" AlternateText="Style" ToolTip="Style" meta:resourcekey="imgbStyleResource1" />
                            <br /><asp:Localize ID="Localize2" runat="server" Text="Style" meta:resourcekey="Localize2Resource1" /></td>
                        </tr>
                        <tr>
                            <td></td>
                            <td></td>
                        </tr>
                        </table>
                        </div>
                    </div>
                    </div>

<!-- Save As popup -->
                    <asp:Button runat="server" ID="Button1" style="display:none" meta:resourcekey="Button1Resource1"/>
                    <ajaxToolkit:ModalPopupExtender runat="server" ID="ahModalPopup" BehaviorID="ahModalPopupBehavior"
                        TargetControlID="Button1" PopupControlID="ahPopup" BackgroundCssClass="modalBackground" PopupDragHandleControlID="pnlDragHandle" RepositionMode="None" DynamicServicePath="" Enabled="True">
                    </ajaxToolkit:ModalPopupExtender>
                    
                    <asp:Panel runat="server" CssClass="modalPopup" ID="ahPopup" style="display:none;width:406;" meta:resourcekey="ahPopupResource1">
                        <asp:Panel ID="pnlDragHandle" runat="server" Style="cursor: hand;" meta:resourcekey="pnlDragHandleResource1">
                        <div class="modalPopupHandle" style="width: 400px;">
                            <table cellpadding="0" cellspacing="0" style="width: 100%"><tr><td>
                                    <asp:Localize ID="PopupHeaderSaveAs" runat="server" meta:resourcekey="SaveAsPopupHeader" Text="Save As"></asp:Localize>
                                </td>
                                <td style="width: 20px;">
                                    <asp:ImageButton ID="imgClosePopup" runat="server" 
                                        OnClick="imgClosePopup_Click" CausesValidation="False"
                                        SkinID="imgbClose" ImageUrl="../ahImages/remove.gif" 
                                        AlternateText="<%$ Resources:LogiAdHoc, CloseWindow %>" />
                            </td></tr></table>
                        </div>
                        </asp:Panel>
                        <div id="divSaveAsContent" runat="server" class="modalDiv">                        
                            <asp:UpdatePanel ID="upSaveAs" runat="server" UpdateMode="Conditional" RenderMode="Inline">
                                <ContentTemplate>
                                    <table>
                                        <tr>
                                            <td width="100px">
                                                <asp:Label ID="Label139" runat="server" Text="<%$ Resources:LogiAdHoc, ReportName %>" AssociatedControlID="SaveAsReportName" meta:resourcekey="Label139Resource1"></asp:Label>:
                                            </td>
                                            <td>
                                                <input id="SaveAsReportName" runat="server" type="text" size="45" maxlength="200" />
                                                <asp:RequiredFieldValidator ID="rfvtxtSaveAsReportName" ControlToValidate="SaveAsReportName" ValidationGroup="SaveAs"
                                                    ErrorMessage="Report Name is required." runat="server" meta:resourcekey="rfvtxtSaveAsReportNameResource">*</asp:RequiredFieldValidator>
                                                <asp:CustomValidator ID="cvValidSaveAsReportName" runat="server" ControlToValidate="SaveAsReportName"
                                                    ErrorMessage="This report name already exists." meta:resourcekey="cvValidSaveAsReportNameResource1"
                                                    OnServerValidate="IsReportSaveAsNameValid" ValidationGroup="SaveAs">*</asp:CustomValidator>
                                                <input id="SaveAsErrMsg" runat="server" type="hidden" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="100px">
                                                <asp:Label ID="Lbl40" runat="server" meta:resourcekey="LiteralResource4" Text="Report Description" AssociatedControlID="txtSaveAsReportDescription"></asp:Label>
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtSaveAsReportDescription" TextMode="MultiLine" Columns="50" Rows="5" MaxLength="255"
                                                    runat="server" meta:resourcekey="txtSaveAsReportDescriptionResource1" />
                                                <asp:CustomValidator ID="cvtxtSaveAsReportDescription" ControlToValidate="txtSaveAsReportDescription" ValidationGroup="SaveAs"
                                                    ErrorMessage="Report Description can be no more than 255 characters." OnServerValidate="ReachedMaxLength"
                                                    runat="server" meta:resourcekey="rtvReportDescriptionResource1">*</asp:CustomValidator>
                                            </td>
                                        </tr>
                                        <tr id="trSaveAsExpirationDate" runat="server">
                                            <td id="Td1" width="100px" runat="server">
                                                <asp:Localize ID="Localize142" runat="server" meta:resourcekey="LiteralResource138" Text="Report Expiration Date:"></asp:Localize>
                                            </td>
                                            <td id="Td2" runat="server">
                                                <wizard:datebox1 id="dbSaveAsExpirationDate" runat="server" Required="false" />
                                                <asp:CustomValidator ID="cvSaveAsExpirationDate" ControlToValidate="txtSaveAsReportDescription"
                                                    ErrorMessage="Report Expiration Date should be greater than today." OnServerValidate="IsSaveAsReportExpirationDateValid"
                                                    runat="server" ValidateEmptyText="True" meta:resourcekey="cvReportExpDateResource1" ValidationGroup="SaveAs">*</asp:CustomValidator>
                                            </td>
                                        </tr>
                                    </table>
                                    
                                    <AdHoc:PickFolder ID="pfSaveAs" runat="server" ShowAllUserGroup="true" />
                                    
                                    <table>
                                        <tr>
                                            <td>
                                                <AdHoc:LogiButton ID="btnSPA_OK" runat="server" OnClick="OK_SPA" ValidationGroup="SaveAs"
                                                ToolTip="<%$ Resources:LogiAdHoc, SaveTooltip1 %>" Text="<%$ Resources:LogiAdHoc, OKWithSpaces %>" UseSubmitBehavior="False" />
                                                <AdHoc:LogiButton ID="btnSPA_Cancel" runat="server" OnClick="Cancel_SPA"
                                                ToolTip="<%$ Resources:LogiAdHoc, CancelTooltip1 %>" Text="<%$ Resources:LogiAdHoc, Cancel %>" CausesValidation="False" />
                                            </td>
                                        </tr>
                                    </table>
                                    <asp:UpdatePanel ID="UPSaveAsError" runat="server" UpdateMode="Conditional" RenderMode="Inline">
                                        <ContentTemplate>
                                            <div id="divSaveAsError" runat="server" class="errorWithImage">
                                            </div>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                    <asp:ValidationSummary ID="ValidationSummary13" ValidationGroup="SaveAs" runat="server" meta:resourcekey="vsummaryResource13" />
                                    <asp:Label ID="lblSemicolon13" runat="server" OnPreRender="lblSemicolon_PreRender" meta:resourcekey="lblSemicolon13Resource1" />
                                </ContentTemplate>
                                <Triggers>
                                    <asp:AsyncPostBackTrigger ControlID="imgbSave" />
                                    <asp:AsyncPostBackTrigger ControlID="imgbSaveAs" />
                                    <asp:AsyncPostBackTrigger ControlID="imgbQSave" />
                                </Triggers>
                            </asp:UpdatePanel>
                        </div>
                    </asp:Panel>

<!-- Report Settings popup -->             
            <asp:Button runat="server" ID="Button2" style="display:none" meta:resourcekey="Button2Resource1"/>
            <ajaxToolkit:ModalPopupExtender runat="server" ID="mpeSettings" BehaviorID="mpeSettingsBehavior"
                TargetControlID="Button2" PopupControlID="popSettings" BackgroundCssClass="modalBackground" PopupDragHandleControlID="pnlDHSettings" RepositionMode="None" DynamicServicePath="" Enabled="True">
            </ajaxToolkit:ModalPopupExtender>
                
            <asp:Panel runat="server" CssClass="modalPopup" ID="popSettings" style="display:none;width:256;" meta:resourcekey="popSettingsResource1">
                <asp:Panel ID="pnlDHSettings" runat="server" Style="cursor: hand;" meta:resourcekey="pnlDHSettingsResource1">
                <div class="modalPopupHandle" style="width: 250px;">
                    <table cellpadding="0" cellspacing="0" style="width: 100%">
                    <tr><td>
                        <asp:Localize ID="Localize4" runat="server" Text="Report Style" meta:resourcekey="Localize4Resource1"></asp:Localize>
                        </td>
                        <td style="width: 50px;">
                            <asp:ImageButton ID="ImageButton2" runat="server" OnClientClick="goHelp('23'); return false;" 
                                CausesValidation="False" SkinID="imgbWinHelp" ImageUrl="../ahImages/iconHelpText.gif" 
                                ToolTip="Get help with report style." meta:resourcekey="ImageButton2Resource1" 
                                AlternateText="Help" />
                            <asp:ImageButton ID="ImageButton3" runat="server" 
                                OnClick="CloseSettingsPopup" CausesValidation="False"
                                SkinID="imgbClose" ImageUrl="../ahImages/remove.gif" 
                                AlternateText="<%$ Resources:LogiAdHoc, CloseWindow %>" />
                    </td></tr></table>
                </div>
                </asp:Panel>
                <div id="Div1" runat="server" class="modalDiv">
                    <asp:UpdatePanel ID="UPRptStyle" runat="server" UpdateMode="Conditional" RenderMode="Inline">
                        <ContentTemplate>
                            <table>
                                <tr>
                                    <td width="125px">
                                    <asp:Label ID="Label15" runat="server" meta:resourcekey="LiteralResource15" Text="Pick a Style:" AssociatedControlID="RptStyle"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:DropDownList ID="RptStyle" runat="server" AutoPostBack="True" meta:resourcekey="RptStyleResource1">
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                            </table>
                            <div>
                                <asp:Image ID="StyleSample" runat="server" meta:resourcekey="StyleSampleResource1" AlternateText="Style Image" />
                            </div>
                            <!-- Buttons -->
                        <br />
                        <table><tr><td>
                                <AdHoc:LogiButton ID="btnRPTStyleOK" OnClick="SettingsOK_OnClick" runat="server" 
                                    Text="<%$ Resources:LogiAdHoc, OKWithSpaces %>" CausesValidation="False" meta:resourcekey="btnRPTStyleOKResource1" />
                                <AdHoc:LogiButton ID="btnRPTStyleCancel" OnClick="SettingsCancel_OnClick" runat="server"
                                    Text="<%$ Resources:LogiAdHoc, Cancel %>" CausesValidation="False" meta:resourcekey="btnRPTStyleCancelResource1" />
                        </td></tr></table>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
            </asp:Panel>

<!-- Open popup -->             
            <asp:Button runat="server" ID="Button5" style="display:none" meta:resourcekey="Button5Resource1"/>
            <ajaxToolkit:ModalPopupExtender runat="server" ID="mpeOpen" BehaviorID="mpeOpenBehavior"
                TargetControlID="Button5" PopupControlID="pnlOpen" BackgroundCssClass="modalBackground" PopupDragHandleControlID="pnlDHOpen" RepositionMode="None" DynamicServicePath="" Enabled="True">
            </ajaxToolkit:ModalPopupExtender>
                
            <asp:Panel ID="pnlOpen" runat="server" CssClass="modalPopup" style="display:none;width:636;" meta:resourcekey="pnlOpenResource1">
                <asp:Panel ID="pnlDHOpen" runat="server" Style="cursor: hand;" meta:resourcekey="pnlDHOpenResource1">
                <div class="modalPopupHandle" style="width: 630px;">
                    <table cellpadding="0" cellspacing="0" style="width: 100%"><tr><td>
                        <asp:Localize ID="Localize5" runat="server" Text="Open Report" meta:resourcekey="Localize5Resource1"></asp:Localize>
                    </td><td style="width: 20px;">
                        <asp:ImageButton ID="imgCloseOpen" runat="server" 
                            OnClick="imgCloseOpen_Click" CausesValidation="False"
                            SkinID="imgbClose" ImageUrl="../ahImages/remove.gif" 
                            AlternateText="<%$ Resources:LogiAdHoc, CloseWindow %>" />
                    </td></tr></table>
                </div>
                </asp:Panel>
                <div id="Div2" runat="server" class="modalDiv">
                <asp:UpdatePanel ID="UPOpen" runat="server" UpdateMode="Conditional" RenderMode="Inline">
                    <ContentTemplate>
                        <AdHoc:SelectReportControl ID="ucSelectReports" DisAllowDashboards="true" runat="server" ShowAdHocReportsOnly="True" />
                        <br />
                        <table><tr><td>
                        <AdHoc:LogiButton ID="btnOKOpen" runat="server" Text="<%$ Resources:LogiAdHoc, OKWithSpaces %>"
                            OnClick="OKOpen_Click" CausesValidation="False" meta:resourcekey="btnOKOpenResource1" />
                        <AdHoc:LogiButton ID="btnCancelOpen" runat="server" Text="<%$ Resources:LogiAdHoc, Cancel %>"
                            OnClick="CancelOpen_Click" CausesValidation="False" meta:resourcekey="btnCancelOpenResource1" />
                        </td></tr></table>
                    </ContentTemplate>
                    <Triggers>
                        <asp:PostBackTrigger ControlID="btnOKOpen" />
                    </Triggers>
                </asp:UpdatePanel>
                </div>
            </asp:Panel>

<!-- Paging popup -->             
            <asp:Button runat="server" ID="Button4" style="display:none" meta:resourcekey="Button4Resource2"/>
            <ajaxToolkit:ModalPopupExtender runat="server" ID="mpePaging" BehaviorID="mpePagingBehavior"
                TargetControlID="Button4" PopupControlID="popPaging" BackgroundCssClass="modalBackground" PopupDragHandleControlID="pnlDHPaging" RepositionMode="None" DynamicServicePath="" Enabled="True">
            </ajaxToolkit:ModalPopupExtender>
                
            <asp:Panel runat="server" CssClass="modalPopup" ID="popPaging" style="display:none;width:286;" meta:resourcekey="popPagingResource1">
                <asp:Panel ID="pnlDHPaging" runat="server" Style="cursor: hand;" meta:resourcekey="pnlDHPagingResource1">
                <div class="modalPopupHandle" style="width: 280px;">
                    <table cellpadding="0" cellspacing="0" style="width: 100%">
                    <tr><td>
                        <asp:Localize ID="Localize7" runat="server" Text="<%$ Resources:LogiAdHoc, PrintablePaging %>"></asp:Localize>
                        </td>
                        <td style="width: 20px;">
                            <asp:ImageButton ID="ImageButton1" runat="server" 
                                OnClick="ClosePagingPopup" CausesValidation="False"
                                SkinID="imgbClose" ImageUrl="../ahImages/remove.gif" 
                                AlternateText="<%$ Resources:LogiAdHoc, CloseWindow %>" />
                    </td></tr></table>
                </div>
                </asp:Panel>
                <div id="Div3" runat="server" class="modalDiv">
                    <asp:UpdatePanel ID="UPPageSize" runat="server" UpdateMode="Conditional" RenderMode="Inline">
                        <ContentTemplate>
                    <table>
                        <tr>
                            <td width="125px">
                            <asp:Label ID="Label7" runat="server" meta:resourcekey="LiteralResource7" Text="Page Size" AssociatedControlID="PrintablePageType"></asp:Label>:
                            </td>
                            <td>
                                <asp:DropDownList ID="PrintablePageType" AutoPostBack="True" DataTextField="Name" OnSelectedIndexChanged="Settings_Changed"
                                    DataValueField="Value" runat="server" meta:resourcekey="PrintablePageTypeResource1" /></td>
                        </tr>
                    </table>
                            <table>
                                <tr>
                                    <td width="125px">
                                        <asp:Localize ID="Localize8" runat="server" EnableViewState="False" Text="<%$ Resources:LogiAdHoc, Width %>"></asp:Localize>:
                                    </td>
                                    <td>
                                        <span id="pagewidth" runat="server"></span>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Localize ID="Localize9" runat="server" EnableViewState="False" Text="<%$ Resources:LogiAdHoc, Height %>"></asp:Localize>:
                                    </td>
                                    <td>
                                        <span id="pageheight" runat="server"></span>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Localize ID="Localize10" runat="server" EnableViewState="False" Text="<%$ Resources:LogiAdHoc, Orientation %>"></asp:Localize>:
                                    </td>
                                    <td>
                                        <asp:RadioButton ID="PrintablePageOrientation1" GroupName="PrintablePagingOrientation" 
                                            runat="server" Text="<%$ Resources:LogiAdHoc, Portrait %>" AutoPostBack="True"
                                            OnCheckedChanged="Settings_Changed" meta:resourcekey="PrintablePageOrientation1Resource1" />
                                        <asp:RadioButton ID="PrintablePageOrientation2" GroupName="PrintablePagingOrientation" 
                                            runat="server" Text="<%$ Resources:LogiAdHoc, Landscape %>" AutoPostBack="True" 
                                            OnCheckedChanged="Settings_Changed" meta:resourcekey="PrintablePageOrientation2Resource1" />                                                        
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                    <asp:Label ID="Label13" runat="server" EnableViewState="False" meta:resourcekey="LiteralResource13" Text="Show Page Number" AssociatedControlID="ShowPrintablePageNr" ></asp:Label>:
                                    </td>
                                    <td>
                                        <input type="checkbox" id="ShowPrintablePageNr" runat="server" EnableViewState="False" />
                                    </td>
                                </tr>
                            </table>
                            
                            <!-- Buttons -->
                        <br />
                        <table><tr><td>
                                <AdHoc:LogiButton ID="btnPagingOK" OnClick="PagingOK_OnClick" runat="server" 
                                    Text="<%$ Resources:LogiAdHoc, OKWithSpaces %>" CausesValidation="False" meta:resourcekey="btnPagingOKResource1" />
                                <AdHoc:LogiButton ID="btnPagingCancel" OnClick="PagingCancel_OnClick" runat="server"
                                    Text="<%$ Resources:LogiAdHoc, Cancel %>" CausesValidation="False" meta:resourcekey="btnPagingCancelResource1" />
                        </td></tr></table>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
            </asp:Panel>
                </ContentTemplate>
            </asp:UpdatePanel>

            <asp:UpdatePanel ID="UPMain" runat="server" UpdateMode="Conditional" RenderMode="Inline">
                <ContentTemplate>
                
                    <asp:UpdatePanel ID="UPError" runat="server" UpdateMode="Conditional" RenderMode="Inline">
                        <ContentTemplate>
                            <div id="divError" runat="server" class="errorWithImage">
                            </div>
                            <div id="divWarning" runat="server" class="warning">
                            </div>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                         
                    <asp:Button ID="btnFakeAddContent" runat="server" CssClass="NoShow" 
                         OnClick="AddContentObject" Text="Add" meta:resourcekey="btnFakeAddContentResource1" />
                    <asp:Button ID="btnFakeMoveDataSection" runat="server" CssClass="NoShow" 
                         OnClick="MoveDataSection" Text="Add" meta:resourcekey="btnFakeMoveDataSectionResource1" />
                    <asp:Button ID="btnFakeMoveReportContent" runat="server" CssClass="NoShow" 
                         OnClick="MoveReportContent" Text="Add" meta:resourcekey="btnFakeMoveReportContentResource1" />

                    <table cellspacing="0" cellpadding="0" border="0" style="table-layout: auto; margin-top:10px;">
                        <tr>
                            <td style="vertical-align: top; border: solid 1px DarkGray; background-color:#fff;" width="212px" id="tdLeftMenu" runat="server">
                                <asp:Panel ID="pnlRightTemplate" runat="server" Width="100%" meta:resourcekey="pnlRightTemplateResource1">
                                    <input type="hidden" id="ihAddContentPosition" runat="server" />
                                    <input type="hidden" id="ihAddContentID" runat="server" />
                                    <input type="hidden" id="ihAddChartTypeID" runat="server" />
                                    <input type="hidden" id="ihActiveDataID" runat="server" />
                                    <input type="hidden" id="ihNewPostbackID" runat="server" />
                                    
                                <div id="divReportAreaHdr" class="groupHeader" style="width:191px;">
                                    <asp:UpdatePanel ID="UPReportDetails" runat="server" UpdateMode="Conditional" RenderMode="Inline">
                                        <ContentTemplate>
                                            <table cellpadding="0" cellspacing="0">
                                                <tr>
                                                    <td width="100%">
                                                        <asp:Label ID="lblReportName" runat="server" Text="Report" meta:resourcekey="lblReportNameResource1" />
                                                        <asp:Panel ID="pnlReportSettings" runat="server" CssClass="settingsPopupControl" meta:resourcekey="pnlReportSettingsResource1">
                                                            <div style="border: 1px outset white;">
                                                            <table style="padding: 5px;" cellpadding="5" cellspacing="5">
                                                                <tr>
                                                                    <td>
                                                                        <asp:Label ID="Localize3" runat="server" Text="Report Name:" AssociatedControlID="txtReportName" meta:resourcekey="Localize3Resource1"></asp:Label>
                                                                    </td>
                                                                    <td>
                                                                        <asp:TextBox ID="txtReportName" runat="server" meta:resourcekey="txtReportNameResource1"></asp:TextBox>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <asp:Label ID="Localize6" runat="server" Text="Description:" AssociatedControlID="txtReportDescription" meta:resourcekey="Localize6Resource1"></asp:Label>
                                                                    </td>
                                                                    <td>
                                                                        <asp:TextBox ID="txtReportDescription" runat="server" meta:resourcekey="txtReportDescriptionResource1"></asp:TextBox>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                            </div>
                                                        </asp:Panel>
                                                    </td>
                                                    <td valign="top">
                                                        <img id="imgExpandReportDetails" onclick="ExpandCollapse('divReportContents','imgExpandReportDetails');" src="../ahImages/collapse_blue.jpg" 
                                                            title="<%$ Resources:LogiAdHoc, CollapseExpandPanel %>" alt="<%$ Resources:LogiAdHoc, CollapseExpand %>" runat="server" /> 
                                                    </td>
                                                </tr>
                                            </table>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </div>
                                <div id="divReportContents" runat="server">
                                    <div style="background-image:url(../ahImages/ReportTop.png); width:210px; height:17px;"></div>
                                    <div style="background-image:url(../ahImages/ReportBac.png); width:210px;">
                                    <table id="tbReportContents" runat="server" cellpadding="0" cellspacing="0" width="180px" style="margin-left:13px;" ahDDGroup="content;content1">
                                    </table>
                                    </div>
                                    <div style="background-image:url(../ahImages/ReportBot.png); width:210px; height:16px;"></div>
                                    <br />
                                </div>
                                <asp:UpdatePanel ID="UPDataTree" runat="server" UpdateMode="Conditional" RenderMode="Inline">
                                    <ContentTemplate>
                                    <div id="divDataAreaHdr" class="groupHeader" style="width:191px;">
                                        <asp:UpdatePanel ID="UPDateTree" runat="server" UpdateMode="Conditional" RenderMode="Inline">
                                            <ContentTemplate>
                                                <table cellpadding="0" cellspacing="0">
                                                    <tr>
                                                        <td width="100%" align="left">
                                                            <input type="hidden" id="ihLastDataSourceID" runat="server" />
                                                            <asp:Label ID="lblDataSourceName" runat="server" Text="Data Source" meta:resourcekey="lblDataSourceNameResource1" />
                                                            <asp:DropDownList ID="ddlDataSource" runat="server" AutoPostBack="True" meta:resourcekey="ddlDataSourceResource1" />
                                                        </td>
                                                        <td valign="top">
                                                            <img id="imgDataTree" onclick="ExpandCollapse('divDataTreeArea','imgDataTree');" src="../ahImages/collapse_blue.jpg" 
                                                                title="<%$ Resources:LogiAdHoc, CollapseExpandPanel %>" alt="<%$ Resources:LogiAdHoc, CollapseExpand %>" runat="server" /> 
                                                        </td>
                                                    </tr>
                                                </table>
                                                
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </div>                            
                                    <div id="divDataTreeArea" runat="server" style="width:210px; overflow:auto; min-height:65px;">
                                        <asp:TreeView ID="trvData" runat="server" meta:resourcekey="trvDataResource1" >
                                        </asp:TreeView>
                                    </div>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                                <br />
                                &nbsp; <AdHoc:LogiButton ID="Button3" Text="Modify Data Source" CausesValidation="False" runat="server" meta:resourcekey="Button3Resource2" />
                                </asp:Panel>
                            </td>
                            <td id="tdWizardBody" runat="server" class="tdWizardBody" style="z-index:-1;">
                                <asp:UpdatePanel ID="UPScrollTab" runat="server" UpdateMode="Conditional" RenderMode="Inline">
                                    <ContentTemplate>
                                        <wizard:ScrollTab id="scTabWiz" runat="server">
                                        </wizard:ScrollTab>                                
                                <div id="wizBod" runat="server" class="wizardBody">
                                <asp:UpdatePanel ID="UPPanels" runat="server" UpdateMode="Conditional" RenderMode="Inline">
                                <ContentTemplate>
                                    <asp:Panel ID="pnlNoColumn" runat="server" Height="350px" Width="100%" meta:resourcekey="pnlNoColumnResource1">
                                        <div id="divNoColumn" runat="server" class="centered">
                                            <br />
                                            <br />
                                            <br />
                                            <h2>
                                            <asp:Label ID="lblNoColumn" runat="server" Text="<%$ Resources:Errors, Err_SelectColumn %>" meta:resourcekey="lblNoColumnResource1" />
                                            </h2>
                                            <br />
                                            <br />
                                            <br />
                                        </div>
                                    </asp:Panel>
                                    <asp:Panel ID="pnlTblSettings" runat="server" Height="350px" Width="100%" meta:resourcekey="pnlTblSettingsResource1">
                                        <asp:ImageButton ID="ImageButton5" runat="server" OnClientClick="goHelp('14'); return false;" style="float:right;" AlternateText="Help" 
                                            CausesValidation="False" SkinID="imgbPnlHelp" ImageUrl="../ahImages/iconHelpText.gif" ToolTip="Get help with table settings." meta:resourcekey="ImageButton5Resource1" />
                                        <table>
                                            <tr>
                                                <td width="150px">
                                                <asp:Label ID="Label30" runat="server" Text="<%$ Resources:LogiAdHoc, Title %>" AssociatedControlID="TableTitle" meta:resourcekey="Label30Resource1"></asp:Label>:
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="TableTitle" runat="server" AutoPostBack="True" OnTextChanged="TableSettings_Changed" Width="250px" meta:resourcekey="TableTitleResource1"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                <asp:Label ID="Label156" runat="server" Text="Show record count:" meta:resourcekey="Localize156Resource1" AssociatedControlID="chkRecordCount"></asp:Label>
                                                </td>
                                                <td>
                                                <asp:CheckBox ID="chkRecordCount" runat="server" AutoPostBack="True" OnCheckedChanged="TableSettings_Changed" meta:resourcekey="chkRecordCountResource1" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                <asp:Label ID="Label157" runat="server" Text="Include row number:" meta:resourcekey="Localize157Resource1" AssociatedControlID="chkRowNum"></asp:Label>
                                                </td>
                                                <td>
                                                <asp:CheckBox ID="chkRowNum" runat="server" AutoPostBack="True" OnCheckedChanged="TableSettings_Changed" meta:resourcekey="chkRowNumResource1" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                <%--<asp:Localize ID="Localize31" runat="server" meta:resourcekey="LiteralResource31" Text="Interactive Paging:"></asp:Localize>--%>
                                                <asp:Localize ID="Localize31" runat="server" meta:resourcekey="LiteralResource31" Text="Paging Style:"></asp:Localize>
                                                </td>
                                                <td>
                                                     <asp:DropDownList ID="ddlPagingStyle" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlPagingStyle_SelectedIndexChanged" >
                                                        <asp:ListItem Value="1" meta:resourcekey="ListItemResource034" Text="Interactive Paging" />
                                                        <asp:ListItem Value="2" meta:resourcekey="ListItemResource035" Text="Numbered List" />
                                                        <asp:ListItem Value="3" meta:resourcekey="ListItemResource036" Text="Append Rows" />
                                                        <asp:ListItem Value="0" meta:resourcekey="ListItemResource037" Text="None" />
                                                    </asp:DropDownList>
                                                </td>
                                            </tr>
                                            <tr id="trPagingLocation" runat="server">
                                                <td>
                                                    <asp:Localize ID="Localize19" runat="server" meta:resourcekey="LiteralResource192" Text="Paging Location:"></asp:Localize>
                                                </td>
                                                <td>
                                                     <asp:DropDownList ID="ddlPagingLocation" runat="server" AutoPostBack="true" OnSelectedIndexChanged="TableSettings_Changed" >
                                                        <asp:ListItem Value="0" meta:resourcekey="ListItemResource40" Text="Top" />
                                                        <asp:ListItem Value="1" meta:resourcekey="ListItemResource41" Text="Bottom" />
                                                        <asp:ListItem Value="2" meta:resourcekey="ListItemResource42" Text="Both" />
                                                    </asp:DropDownList>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                <asp:Label ID="Label32" runat="server" meta:resourcekey="LiteralResource32" Text="Rows Per Page:"
                                                    AssociatedControlID="InteractiveRows"></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="InteractiveRows" Width="30px" MaxLength="3" runat="server" AutoPostBack="True" OnTextChanged="TableSettings_Changed" meta:resourcekey="InteractiveRowsResource1" />
                                                    <asp:RangeValidator ID="rvInteractiveRows" runat="server" ValidationGroup="ReportSetting"
                                                        ControlToValidate="InteractiveRows" Type="Integer" MinimumValue="1" MaximumValue="999"
                                                        ErrorMessage="Number of rows can only be set to integer values between 1 and 999."
                                                        meta:resourcekey="rvInteractiveRowsResource1">*</asp:RangeValidator>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:Label ID="Label34" runat="server" meta:resourcekey="LiteralResource34" 
                                                        Text="Rows Per Sub-Report Page:" AssociatedControlID="InteractiveRowsSub"></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="InteractiveRowsSub" Width="30px" MaxLength="3" runat="server" AutoPostBack="True" OnTextChanged="TableSettings_Changed" meta:resourcekey="InteractiveRowsSubResource1" />
                                                    <asp:RangeValidator ID="rvInteractiveRowsSub" runat="server" ValidationGroup="ReportSetting"
                                                        ControlToValidate="InteractiveRowsSub" Type="Integer" MinimumValue="1" MaximumValue="999"
                                                        ErrorMessage="Number of rows can only be set to integer values between 1 and 999."
                                                        meta:resourcekey="rvInteractiveRowsSubResource1">*</asp:RangeValidator>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:Localize ID="Localize164" runat="server" meta:resourcekey="LiteralResource164"
                                                        Text="Summary Row Location:"></asp:Localize>
                                                </td>
                                                <td>
                                                    <asp:RadioButton ID="rbtnSumRowLocationTop" runat="server" AutoPostBack="True" GroupName="SummaryRowLocation"
                                                        meta:resourcekey="rbtnSumRowLocationTopResource1" OnCheckedChanged="TableSettings_Changed"
                                                        Text="Top" TextAlign="Left" />
                                                    <asp:RadioButton ID="rbtnSumRowLocationBottom" runat="server" AutoPostBack="True"
                                                        Checked="True" GroupName="SummaryRowLocation" meta:resourcekey="rbtnSumRowLocationBottomResource1"
                                                        OnCheckedChanged="TableSettings_Changed" Text="Bottom" TextAlign="Left" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:Localize ID="Localize165" runat="server" meta:resourcekey="LiteralResource165"
                                                        Text="Show Summary Row:"></asp:Localize>
                                                </td>
                                                <td>
                                                    <asp:RadioButton ID="rbtnShowSumRowOnce" runat="server" AutoPostBack="True" Checked="True"
                                                        GroupName="ShowSummaryRow" meta:resourcekey="rbtnShowSumRowOnceResource1" 
                                                        OnCheckedChanged="TableSettings_Changed" Text="Once" TextAlign="Left" />
                                                    <asp:RadioButton ID="rbtnShowSumRowAllPages" runat="server" AutoPostBack="True" 
                                                        GroupName="ShowSummaryRow" meta:resourcekey="rbtnShowSumRowAllPagesResource1" 
                                                        OnCheckedChanged="TableSettings_Changed" Text="All Pages" TextAlign="Left" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                <asp:Label ID="Label12" runat="server" meta:resourcekey="LiteralResource193" Text="Table Width:"
                                                    AssociatedControlID="txtTableWidth"></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtTableWidth" Width="30px" MaxLength="5" runat="server" AutoPostBack="True" OnTextChanged="TableSettings_Changed" meta:resourcekey="InteractiveRowsResource1" />
                                                    <asp:RangeValidator ID="rvTableWidth" runat="server" ValidationGroup="ReportSetting"
                                                        ControlToValidate="txtTableWidth" Type="Integer" MinimumValue="1" MaximumValue="99999"
                                                        ErrorMessage="Table width can only be set to integer values between 1 and 99999."
                                                        meta:resourcekey="rvTableWidthResource1">*</asp:RangeValidator>
                                                    <asp:DropDownList ID="ddlTableWidthScale" runat="server" AutoPostBack="True" OnSelectedIndexChanged="TableSettings_Changed">
                                                        <asp:ListItem Text="Pixels" Value="px" />
                                                        <asp:ListItem Text="Percent" Value="%" />
                                                    </asp:DropDownList>
                                                </td>
                                            </tr>
                                        </table>
                                        <asp:ValidationSummary ID="ValidationSummary7" runat="server" ValidationGroup="ReportSetting"
                                            meta:resourcekey="ValidationSummary7Resource1" />
                                        <asp:Label ID="lblSemicolon7" runat="server" OnPreRender="lblSemicolon_PreRender" meta:resourcekey="lblSemicolon7Resource1" />
                                    </asp:Panel>
                                    <asp:Panel ID="pnlTblColumns" runat="server" Height="350px" Width="100%" meta:resourcekey="pnlTblColumnsResource1">
                                        <asp:UpdatePanel ID="UpTblColumns" runat="server" UpdateMode="Conditional" RenderMode="Inline">
                                            <ContentTemplate>                                                        
                                                <table>
                                                    <tr>
                                                        <td colspan="2"><asp:Localize ID="Localize35" runat="server" meta:resourcekey="LiteralResource35" Text="Available Columns"></asp:Localize></td>
                                                        <td colspan="2"><asp:Localize ID="Localize37" runat="server" meta:resourcekey="LiteralResource37" Text="Assigned Columns"></asp:Localize></td>
                                                    </tr>
                                                    <tr>
			                                            <td valign="top" >
			                                               
                                                                <div style="font-size: 90%;">
                                                                    <AdHoc:ScrollableListBox ID="ColAvailable" runat="server" DblClickFunction="__doPostBack('moveColRight','');"
                                                                        MultipleSelection="True" OnChangeFunction="getDescription(this.id);" SelectHeight="202"
                                                                        SelectWidth="300" />
                                                                </div>
                                                                
			                                            </td>
			                                            <td>
                                                            <asp:ImageButton ID="moveColRight" runat="server" AlternateText="Move Columns Right"
                                                                CausesValidation="False" CssClass="mslbutton" ImageUrl="../ahImages/arrowRight.gif"
                                                                meta:resourcekey="imgMoveRightResource1" OnClick="MoveColRight_Click" ToolTip="Move Columns Right" />
                                                            <asp:ImageButton ID="moveColLeft" runat="server" AlternateText="Move Columns Left"
                                                                CausesValidation="False" CssClass="mslbutton" ImageUrl="../ahImages/arrowLeft.gif"
                                                                meta:resourcekey="imgMoveLeftResource1" OnClick="MoveColLeft_Click" ToolTip="Move Columns Left" />
                                                        </td>
                                                        <td valign="top">
                                                            
                                                                    <div style="font-size: 90%;">
                                                                    <AdHoc:ScrollableListBox ID="ColAssigned" runat="server" DblClickFunction="__doPostBack('moveColLeft','');"
                                                                        MultipleSelection="True" SelectHeight="202" SelectWidth="300" />
                                                                    </div>
                                                            
                                                        </td>
                                                        <td>
                                                            <asp:ImageButton ID="movColUp" runat="server" AlternateText="Move Columns Up" CausesValidation="False"
                                                                CssClass="mslbutton" ImageUrl="../ahImages/arrowUp.gif" meta:resourcekey="imgMoveupResource1"
                                                                OnClick="MoveColUp_Click" ToolTip="Move Columns Up" />
                                                            <asp:ImageButton ID="movColDown" runat="server" AlternateText="Move Columns Down"
                                                                CausesValidation="False" CssClass="mslbutton" ImageUrl="../ahImages/arrowDown.gif"
                                                                meta:resourcekey="imgMovedownResource1" OnClick="MoveColDown_Click" ToolTip="Move Columns Down" />
                                                        </td>
                                                        <td valign="top" >
        			                                        <div id="DivDesc" class="DescriptionField" runat="server">&nbsp;</div>
			                                            </td>
		                                            </tr>
                                                </table>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                        
                                    </asp:Panel>
                                    <asp:Panel ID="pnlColFormat" runat="server" Height="350px" Width="100%" meta:resourcekey="pnlColFormatResource1">
                                        <asp:UpdatePanel ID="UPCFGrid" runat="server" UpdateMode="Conditional" RenderMode="Inline">
                                        <ContentTemplate>
                                        <asp:ImageButton ID="ImageButton6" runat="server" OnClientClick="goHelp('12'); return false;" style="float:right;" AlternateText="Help" 
                                            CausesValidation="False" SkinID="imgbPnlHelp" ImageUrl="../ahImages/iconHelpText.gif" ToolTip="Get help configuring columns." meta:resourcekey="ImageButton6Resource1" />
                                        <span style="font-weight:bold;">
                                            <asp:ImageButton ID="imgOVColFormat" runat="server" CausesValidation="False" ImageUrl="../ahImages/right_blue.jpg"
                                                OnClientClick="javascript: noMessage = true;" OnCommand="ExpandCollapseOptions"
                                                ToolTip="<%$ Resources:LogiAdHoc, AdvancedOptionsTooltip %>" AlternateText="<%$ Resources:LogiAdHoc, AdvancedOptionsTooltip %>" />
                                            &nbsp;
                                            <asp:Localize ID="lOVColFormat" runat="server" Text="<%$ Resources:LogiAdHoc, ShowExtra %>" />
                                            <br />
                                        </span>
                                        <table>
                                            <tr>
                                                <td>
                                                    <asp:Label ID="lbltxtHdrScale" runat="server" CssClass="NoShow" Text="HdrScale" AssociatedControlID="txtHdrScale" meta:resourcekey="lbltxtHdrScaleResource1"></asp:Label>
                                                    <asp:TextBox ID="txtHdrScale" runat="server" CssClass="NoShow" AutoPostBack="True" OnTextChanged="X_Changed" meta:resourcekey="txtHdrScaleResource2" />
                                                    <div id="divGrdColumnFormat" runat="server" style="height: 297px; overflow:auto;">
                                                        <asp:UpdatePanel ID="UPGrdColumnFormat" runat="server" UpdateMode="conditional" RenderMode="Inline">
                                                            <ContentTemplate>
                                                                <asp:Button ID="btnFakeGrdColumnFormatReorder" runat="server" CssClass="NoShow" OnClick="btnFakeGrdColumnFormatReorder_Click" meta:resourcekey="btnFakeGrdColumnFormatReorderResource1" />
                                                                <%--<AdHoc:DDGridView ID="grdColumnFormat" runat="server" AutoGenerateColumns="False" OnRowDataBound="OnCFItemDataBoundHandler"
                                                                    CssClass="gridWline" meta:resourcekey="grdColumnFormatResource1" DDDivID="divDragHandle" PostbackButtonID="btnFakeGrdColumnFormatReorder">--%>
                                                                <asp:GridView ID="grdColumnFormat" runat="server" AutoGenerateColumns="False" OnRowDataBound="OnCFItemDataBoundHandler"
                                                                    CssClass="gridWline" meta:resourcekey="grdColumnFormatResource1" >
                                                                    <Columns>
                                                                        <asp:TemplateField meta:resourcekey="TemplateFieldResource1">
                                                                            <HeaderTemplate>
                                                                                <asp:Label runat="server" AssociatedControlID="CheckAll" 
                                                                                    Text="<%$ Resources:LogiAdHoc, SelectDeselectAll %>" 
                                                                                    CssClass="NoShow" ID="lblChkAll" meta:resourcekey="lblChkAllResource1"></asp:Label>
                                                                                <asp:CheckBox runat="server" ToolTip="<%$ Resources:LogiAdHoc, SelectDeselectAll %>" 
                                                                                    ID="CheckAll" meta:resourcekey="CheckAllResource1"></asp:CheckBox>
                                                                            </HeaderTemplate>
                                                                            <ItemTemplate>
                                                                                <input runat="server" ID="CFRowOrder" type="hidden" />
                                                                                <input runat="server" ID="RowIndex" type="hidden" />
                                                                                <table cellpadding="0" cellspacing="0">
                                                                                    <tr>
                                                                                        <%--<td>
                                                                                            <div runat="server" ID="divDragHandle" class="dragHandle">
                                                                                            </div>
                                                                                        </td>--%>
                                                                                        <td>
                                                                                            <asp:Label runat="server" AssociatedControlID="chk_Select" 
                                                                                                Text="<%$ Resources:LogiAdHoc, SelectDeselect %>" 
                                                                                                CssClass="NoShow" ID="lblChk" meta:resourcekey="lblChkResource1"></asp:Label>
                                                                                            <asp:CheckBox runat="server" ToolTip="<%$ Resources:LogiAdHoc, SelectDeselect %>" 
                                                                                                ID="chk_Select" meta:resourcekey="chk_SelectResource1"></asp:CheckBox>
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField HeaderText="<%$ Resources:LogiAdHoc, Column %>" meta:resourcekey="TemplateFieldResource2">
                                                                            <ItemTemplate>
                                                                                <input runat="server" ID="hColumnKey" type="hidden" />
                                                                                <asp:Label runat="server" ID="Column" meta:resourceKey="ColumnResource2"></asp:Label>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField HeaderText="<%$ Resources:LogiAdHoc, Header %>" meta:resourcekey="TemplateFieldResource3">
                                                                            <ItemTemplate>
                                                                                <asp:Label runat="server" AssociatedControlID="header" 
                                                                                    Text="<%$ Resources:LogiAdHoc, Header %>" CssClass="NoShow" 
                                                                                    ID="lblHeader" meta:resourcekey="lblHeaderResource1"></asp:Label>
                                                                                <asp:TextBox runat="server" AutoPostBack="True" ID="header" 
                                                                                    meta:resourceKey="headerResource1" OnTextChanged="CFChanged"></asp:TextBox>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField HeaderText="Linkable" meta:resourcekey="grdColumnFormatLinkable" HeaderStyle-Width="80px">
                                                                            <ItemTemplate>
                                                                                <asp:Label runat="server" AssociatedControlID="Linked" Text="Linkable" 
                                                                                    CssClass="NoShow" ID="lblLinked" meta:resourceKey="lblLinkedResource1"></asp:Label>
                                                                                <asp:CheckBox runat="server" AutoPostBack="True" ID="Linked" 
                                                                                    meta:resourceKey="LinkedResource1" OnCheckedChanged="CFChanged"></asp:CheckBox>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField HeaderText="Sortable" meta:resourcekey="TemplateFieldResource4">
                                                                            <HeaderTemplate>
                                                                               <table>
                                                                                    <tr>
                                                                                        <td>
                                                                                            <asp:Label runat="server" AssociatedControlID="SortableCheckAll" 
                                                                                                Text="<%$ Resources:LogiAdHoc, SelectDeselectAll %>" CssClass="NoShow" 
                                                                                                ID="lblSortableCheckAll" meta:resourcekey="lblSortableCheckAllResource1"></asp:Label>
                                                                                            <asp:CheckBox runat="server" AutoPostBack="True" 
                                                                                                ToolTip="<%$ Resources:LogiAdHoc, SelectDeselectAll %>" 
                                                                                                ID="SortableCheckAll" meta:resourcekey="SortableCheckAllResource2" 
                                                                                                OnCheckedChanged="CFChanged"></asp:CheckBox>
                                                                                        </td>
                                                                                        <td>
                                                                                            <asp:Label runat="server" Text="Sortable" ID="lblSortableHeader" CssClass="gridheader" meta:resourceKey="grdColumnFormatSortable" />
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                            </HeaderTemplate>
                                                                            <ItemTemplate>
                                                                                <asp:Label runat="server" AssociatedControlID="sort" 
                                                                                    Text="<%$ Resources:LogiAdHoc, SelectDeselect %>" CssClass="NoShow" 
                                                                                    ID="lblsort" meta:resourcekey="lblsortResource1"></asp:Label>
                                                                                <asp:CheckBox runat="server" AutoPostBack="True" 
                                                                                    ToolTip="<%$ Resources:LogiAdHoc, SelectDeselect %>" ID="sort" 
                                                                                    meta:resourceKey="sortResource1" OnCheckedChanged="CFChanged"></asp:CheckBox>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField HeaderText="Summary" meta:resourcekey="grdColumnFormatSummary" HeaderStyle-Width="50px">
                                                                            <ItemTemplate>
                                                                                <asp:ImageButton runat="server" CausesValidation="False" AlternateText="Modify Summary Row" 
                                                                                    ImageUrl="../ahImages/iconSummaryRowOff.gif" ToolTip="Modify Summary Row" 
                                                                                    ID="ModifySummary" meta:resourceKey="ModifySummaryResource1" OnCommand="ModifySummaryRow"></asp:ImageButton>
                                                                            </ItemTemplate>
                                                                            <HeaderStyle Width="50px"></HeaderStyle>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField HeaderText="Format" meta:resourcekey="grdColumnFormatFormat">
                                                                            <ItemTemplate>
                                                                                <asp:Label runat="server" AssociatedControlID="format" Text="Format" 
                                                                                    CssClass="NoShow" ID="lblFormat" meta:resourceKey="lblFormatResource1"></asp:Label>
                                                                                <asp:DropDownList runat="server" AutoPostBack="True" ID="format" 
                                                                                    meta:resourceKey="formatResource1" OnSelectedIndexChanged="CFChanged"></asp:DropDownList>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField HeaderText="Visualization" meta:resourcekey="grdColumnFormatVisualization" HeaderStyle-Width="80px">
                                                                            <ItemTemplate>
                                                                                <asp:ImageButton runat="server" CausesValidation="False" AlternateText="Modify Visualization Options" ImageUrl="../ahImages/iconVisualizationOff.gif" ToolTip="Modify Visualization Options" ID="ModifyVisualization" meta:resourceKey="ModifyVisualizationResource1" OnCommand="ModifyVisualization"></asp:ImageButton>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField meta:resourcekey="TemplateFieldResource5" HeaderStyle-Width="65px">
                                                                            <HeaderTemplate>
                                                                                <asp:Label runat="server" Text="<%$ Resources:LogiAdHoc, Width %>" ID="hdrWidth" meta:resourcekey="hdrWidthResource1"></asp:Label>
                                                                                (
                                                                                <span runat="server" ID="hdrScale" onclick="javascript: document.getElementById('ahDirty').value=1; toggleScale(this.id);" title="Click to toggle">px</span>
                                                                                )
                                                                            </HeaderTemplate>
                                                                            <ItemTemplate>
                                                                                <span style="white-space: nowrap">
                                                                                    <asp:Label runat="server" AssociatedControlID="width" Text="<%$ Resources:LogiAdHoc, Width %>" CssClass="NoShow" ID="lblwidth" meta:resourcekey="lblwidthResource1"></asp:Label>
                                                                                    <asp:TextBox runat="server" AutoPostBack="True" CssClass="smallText" ID="width" meta:resourceKey="widthResource1" OnTextChanged="CFChanged"></asp:TextBox>
                                                                                </span>
                                                                                <asp:RangeValidator runat="server" MaximumValue="64000" MinimumValue="0" Type="Integer" ControlToValidate="width" ErrorMessage="Width accepts only integer values." ValidationGroup="ColumnFormatGroup" ID="rvWidth" meta:resourceKey="rvWidthResource1">*</asp:RangeValidator>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField HeaderText="Alignment" meta:resourcekey="grdColumnFormatAlignment">
                                                                            <ItemTemplate>
                                                                                <asp:Label runat="server" AssociatedControlID="alignment" Text="Alignment" CssClass="NoShow" ID="lblAlignment" meta:resourceKey="lblAlignmentResource1"></asp:Label>
                                                                                <asp:DropDownList runat="server" AutoPostBack="True" ID="alignment" meta:resourceKey="alignmentResource1" OnSelectedIndexChanged="CFChanged">
                                                                                    <asp:ListItem Value="Left" meta:resourceKey="ListItemResource6">Left</asp:ListItem>
                                                                                    <asp:ListItem Value="Right" meta:resourceKey="ListItemResource7">Right</asp:ListItem>
                                                                                    <asp:ListItem Value="Center" meta:resourceKey="ListItemResource8">Center</asp:ListItem>
                                                                                </asp:DropDownList>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField HeaderText="<%$ Resources:LogiAdHoc, Style %>" meta:resourcekey="TemplateFieldResource6">
                                                                            <ItemTemplate>
                                                                                <asp:ImageButton runat="server" CausesValidation="False" AlternateText="Modify Style" ImageUrl="../ahImages/modify.gif" ToolTip="Modify Style" ID="Modify" meta:resourceKey="ModifyResource1" OnCommand="ModifyAppearance"></asp:ImageButton>
                                                                            </ItemTemplate>
                                                                            <HeaderStyle Width="50px"></HeaderStyle>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField HeaderText="<%$ Resources:LogiAdHoc, Actions %>" meta:resourcekey="TemplateFieldResource7">
                                                                            <ItemTemplate>
                                                                                <asp:ImageButton runat="server" ID="EditColumn" SkinID="imgSingleAction" CommandName="EditColumn" OnCommand="ModifyCFCalcColumn" 
                                                                                    AlternateText="Edit Column" ToolTip="Edit Column" CausesValidation="False" meta:resourceKey="EditColumnResource1"></asp:ImageButton>
                                                                            </ItemTemplate>
                                                                            <HeaderStyle Width="60px" Wrap="False"></HeaderStyle>
                                                                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Top" Wrap="False"></ItemStyle>
                                                                        </asp:TemplateField>
                                                                    </Columns>
                                                                    <HeaderStyle CssClass="gridheader" />
                                                                <%--</AdHoc:DDGridView>--%>
                                                                </asp:GridView>
                                                            </ContentTemplate>
                                                        </asp:UpdatePanel>
                                                    </div>
                                                    <div style="padding-top:8px;">
                                                    <AdHoc:LogiButton ID="AddCalculatedColumn" OnClick="AddCalculatedColumn_OnClick" Text="Add a Custom Column"
                                                        runat="server" meta:resourcekey="AddCalculatedColumnResource1" />
                                                    </div>
                                                </td>
                                                <td width="30">
                                                    <div id="divColumnMove" runat="server">
                                                        <asp:ImageButton ID="imgColumnMoveup" runat="server" AlternateText="Move Columns Up"
                                                            CssClass="mslbutton" ImageUrl="../ahImages/arrowUp.gif" meta:resourcekey="imgMoveupResource1"
                                                            OnCommand="ColumnMoveUp" ToolTip="Move Columns Up" />
                                                        <asp:ImageButton ID="imgColumnMovedown" runat="server" AlternateText="Move Columns Down"
                                                            CssClass="mslbutton" ImageUrl="../ahImages/arrowDown.gif" meta:resourcekey="imgMovedownResource1"
                                                            OnCommand="ColumnMoveDown" ToolTip="Move Columns Down" />
                                                     </div>
                                                     <div id="divColumnRemove" runat="server">
                                                        <asp:ImageButton ID="imgColumnDelete" runat="server" AlternateText="Remove Columns From Report"
                                                            CausesValidation="False" CssClass="mslbutton" ImageUrl="../ahImages/remove.gif"
                                                            meta:resourcekey="imgDeleteResource2" OnCommand="DeleteColumns" ToolTip="Remove Columns From Report" />
                                                    </div>
                                                </td>
                                            </tr>
                                        </table>
                                        <asp:ValidationSummary ID="ValidationSummary14" runat="server" ValidationGroup="ColumnFormatGroup"
                                            meta:resourcekey="ValidationSummary2Resource1" />
                                        <asp:Label ID="lblSemicolon14" runat="server" OnPreRender="lblSemicolon14_PreRender" meta:resourcekey="lblSemicolon14Resource1" />
                                        </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </asp:Panel>
                                    <asp:Panel ID="pnlGrouping" runat="server" Height="350px" Width="100%" meta:resourcekey="pnlGroupingResource1">
                                        <asp:ImageButton ID="ImageButton7" runat="server" OnClientClick="goHelp('13'); return false;" style="float:right;" AlternateText="Help"
                                            CausesValidation="False" SkinID="imgbPnlHelp" ImageUrl="../ahImages/iconHelpText.gif" ToolTip="Get help picking a grouping style." meta:resourcekey="ImageButton7Resource1" />
                                        <p>
                                        <asp:Localize ID="Localize71" runat="server" meta:resourcekey="LiteralResource71" Text="Do you want this to be a grouped report? Please choose the grouping style:"></asp:Localize>
                                        </p>
                                        <h2><asp:Localize ID="Localize72" runat="server" meta:resourcekey="LiteralResource72" Text="Grouping Options:"></asp:Localize></h2>
                                        <asp:UpdatePanel ID="UP1" UpdateMode="Conditional" RenderMode="Inline" runat="server">
                                            <ContentTemplate>
                                                <asp:RadioButton ID="GroupNone" GroupName="GroupingMethod" Text="Not Grouped" Checked="True"
                                                    AutoPostback="True" runat="server" OnCheckedChanged="GroupingMethod_Changed"
                                                    meta:resourcekey="GroupNoneResource1" />
                                                <asp:RadioButton ID="GroupFlat" GroupName="GroupingMethod" Text="Grouped Flat-Table"
                                                    AutoPostback="True" runat="server" OnCheckedChanged="GroupingMethod_Changed"
                                                    meta:resourcekey="GroupFlatResource1" />
                                                <asp:RadioButton ID="GroupDrill" GroupName="GroupingMethod" Text="Grouped Drill-Down"
                                                    AutoPostback="True" runat="server" OnCheckedChanged="GroupingMethod_Changed"
                                                    meta:resourcekey="GroupDrillResource1" />
                                                <asp:Panel ID="pnlExpandAll" runat="server" GroupingText="Layer Expansion" meta:resourcekey="pnlExpandAllResource1">
                                                    <asp:CheckBox ID="chkExpandAll" runat="server" AutoPostBack="True" Text="Add option to expand all layers"
                                                        meta:resourcekey="chkExpandAllResource1"></asp:CheckBox> &nbsp; &nbsp; &nbsp; 
                                                    <AdHoc:LogiButton ID="btnRecCnt" OnClick="btnRecCnt_OnClick" Text="Get Record Count"
                                                        runat="server" meta:resourcekey="btnRecCntResource1" />
                                                    <div class="warning">
                                                        <asp:Localize ID="Localize104" runat="server" meta:resourcekey="LiteralResource104" Text="Using this option in the report can bear the risk of overloading your server, depending on the size of data. Click on Get Record Count button to get an approximate idea of the current size of data."></asp:Localize>
                                                    </div>
                                                </asp:Panel>
                                                <div id="DivGrouping" runat="server">
                                                    <table cellpadding="5">
                                                        <tr>
                                                            <td valign="top">
                                                                <AdHoc:DDGridView ID="MainGrid" runat="server" AutoGenerateColumns="False" OnRowDataBound="OnMainItemDataBoundHandler"
                                                                    OnRowCommand="OnMainItemCommandHandler" CssClass="gridWline" meta:resourcekey="MainGridResource1">
                                                                    <Columns>
                                                                        <asp:TemplateField HeaderText="Grouped Columns" meta:resourcekey="GroupingGroupedColumns">
                                                                            <ItemTemplate>
                                                                                <input runat="server" ID="hLayer" type="hidden" />
                                                                                <div runat="server" ID="divGroupedColumns">
                                                                                </div>
                                                                            </ItemTemplate>
                                                                            <HeaderStyle Width="200px"></HeaderStyle>
                                                                            <ItemStyle VerticalAlign="Top" />
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField HeaderText="Aggregate Columns" meta:resourcekey="GroupingAggregateColumns">
                                                                            <ItemTemplate>
                                                                                <div runat="server" ID="divAggregatedColumns">
                                                                                </div>
                                                                            </ItemTemplate>
                                                                            <HeaderStyle Width="200px"></HeaderStyle>
                                                                            <ItemStyle VerticalAlign="Top" />
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField HeaderText="<%$ Resources:LogiAdHoc, Actions %>" meta:resourcekey="TemplateFieldResource7">
                                                                            <ItemTemplate>
                                                                                <asp:ImageButton runat="server" CommandName="GroupMoveUp" CausesValidation="False" AlternateText="Move Layer Up" ImageUrl="../ahImages/SmallArrowUp.gif" ToolTip="Move Layer Up" ID="imgMoveupGroup" meta:resourceKey="imgMoveupGroupResource1"></asp:ImageButton>
                                                                                <asp:ImageButton runat="server" CommandName="GroupMoveDown" CausesValidation="False" AlternateText="Move Layer Down" ImageUrl="../ahImages/SmallArrowDown.gif" ToolTip="Move Layer Down" ID="imgMovedownGroup" meta:resourceKey="imgMovedownGroupResource1"></asp:ImageButton>
                                                                                &nbsp;
                                                                                <asp:ImageButton runat="server" CommandName="EditLayer" CausesValidation="False" AlternateText="Edit Layer" ImageUrl="../ahImages/modify.gif" ToolTip="Edit Layer" ID="EditLayer" meta:resourceKey="EditLayerResource1"></asp:ImageButton>
                                                                                <asp:ImageButton runat="server" CommandName="RemoveLayer" CausesValidation="False" AlternateText="Remove Layer" ImageUrl="../ahImages/remove.gif" ToolTip="Remove Layer" ID="RemoveLayer" meta:resourceKey="RemoveLayerResource1"></asp:ImageButton>
                                                                            </ItemTemplate>
                                                                            <HeaderStyle Width="120px" Wrap="False"></HeaderStyle>
                                                                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Top" Wrap="False"></ItemStyle>
                                                                        </asp:TemplateField>
                                                                    </Columns>
                                                                    <FooterStyle CssClass="gridfooter" />
                                                                    <HeaderStyle CssClass="gridheader" />
                                                                </AdHoc:DDGridView>
                                                                <AdHoc:LogiButton ID="AddLayer" OnClick="AddLayer_OnClick" Text="Add Grouping Layer"  OnClientClick="javascript: ResizePopupWindow()"
                                                                    runat="server" meta:resourcekey="AddLayerResource1" />
                                                            </td>
                                                            <td valign="top">
                                                                
                                                            </td>
                                                        </tr>
                                                        <tr id="trFlatGroupingRollUpDetails" runat="server">
                                                            <td>
                                                                <table>
                                                                    <tr>
                                                                        <td>
                                                                            <asp:Label ID="lblchkRollUpDetails" runat="server" meta:resourcekey="lblchkRollUpDetailsResource"
                                                                                Text="Roll up last layer:" AssociatedControlID="chkRollUpDetails"></asp:Label>
                                                                        </td>
                                                                        <td>
                                                                            <asp:CheckBox ID="chkRollUpDetails" runat="server" AutoPostBack="True" meta:resourcekey="chkRollUpDetailsResource1" />
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </div>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </asp:Panel>
                                    <asp:Panel ID="pnlHeaderInfo" runat="server" Height="350px" Width="100%" meta:resourcekey="pnlHeaderInfoResource1">
                                        <table>
                                            <tr>
                                                <td width="125px">
                                                    <asp:Label ID="Label52" runat="server" Text="<%$ Resources:LogiAdHoc, Res_Date %>" AssociatedControlID="ShowDate" meta:resourcekey="Label52Resource1"></asp:Label>:
                                                </td>
                                                <td>
                                                    <asp:CheckBox ID="ShowDate" runat="server" AutoPostBack="True" OnCheckedChanged="Header_Changed" meta:resourcekey="ShowDateResource1" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:Label ID="Label53" runat="server" Text="<%$ Resources:LogiAdHoc, Time %>" AssociatedControlID="ShowTime" meta:resourcekey="Label53Resource1"></asp:Label>:</td>
                                                <td>
                                                    <asp:CheckBox ID="ShowTime" runat="server" AutoPostBack="True" OnCheckedChanged="Header_Changed" meta:resourcekey="ShowTimeResource1" />
                                                </td>
                                            </tr>
                                        </table>
                                    </asp:Panel>
                                    <asp:Panel ID="pnlExport" runat="server" Height="350px" Width="100%" meta:resourcekey="pnlExportResource1">
                                        <asp:ImageButton ID="ImageButton8" runat="server" OnClientClick="goHelp('20'); return false;" style="float:right;" AlternateText="Help"
                                            CausesValidation="False" SkinID="imgbPnlHelp" ImageUrl="../ahImages/iconHelpText.gif" ToolTip="Get help with export options." meta:resourcekey="ImageButton8Resource1" />
                                        <table>
                                            <tr>
                                                <td width="200px">
                                                    <asp:Label ID="Label75" runat="server" meta:resourcekey="LiteralResource75" Text="Searchable Report:" AssociatedControlID="LinkSearchable"></asp:Label></td>
                                                <td>
                                                    <asp:CheckBox ID="LinkSearchable" runat="server" AutoPostBack="True" OnCheckedChanged="Export_Changed" meta:resourcekey="LinkSearchableResource1" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:Label ID="Label6" runat="server" Text="<%$ Resources:LogiAdHoc, PrintablePaging %>" AssociatedControlID="LinkPrintable" meta:resourcekey="Label6Resource1"></asp:Label>:
                                                </td>
                                                <td>
                                                    <asp:CheckBox ID="LinkPrintable" runat="server" AutoPostBack="True" OnCheckedChanged="Export_Changed" meta:resourcekey="LinkPrintableResource1" />
                                                </td>
                                            </tr>
                                            <tr id="rowXLS" runat="server">
                                                <td id="Td3" runat="server">
                                                    <asp:Label ID="Label76" runat="server" meta:resourcekey="LiteralResource76" Text="Export to Excel:" AssociatedControlID="LinkExcel"></asp:Label></td>
                                                <td id="Td4" runat="server">
                                                    <asp:CheckBox ID="LinkExcel" runat="server" AutoPostBack="True" OnCheckedChanged="Export_Changed" />
                                                </td>
                                            </tr>
                                            <tr id="rowWRD" runat="server">
                                                <td id="Td5" runat="server">
                                                    <asp:Label ID="Label77" runat="server" meta:resourcekey="LiteralResource77" Text="Export to Word:" AssociatedControlID="LinkWord"></asp:Label></td>
                                                <td id="Td6" runat="server">
                                                <asp:CheckBox ID="LinkWord" runat="server" AutoPostBack="True" OnCheckedChanged="Export_Changed" />
                                                </td>
                                            </tr>
                                            <tr id="rowPPT" runat="server">
                                                <td id="Td7" runat="server">
                                                    <asp:Label ID="Label159" runat="server" meta:resourcekey="LiteralResource159" Text="Export to PowerPoint:" AssociatedControlID="LinkPPT"></asp:Label></td>
                                                <td id="Td8" runat="server">
                                                <asp:CheckBox ID="LinkPPT" runat="server" AutoPostBack="True" OnCheckedChanged="Export_Changed" />
                                                </td>
                                            </tr>
                                            <tr id="rowPDF" runat="server">
                                                <td id="Td9" runat="server">
                                                    <asp:Label ID="Label78" runat="server" meta:resourcekey="LiteralResource78" Text="Export to PDF:" AssociatedControlID="LinkPDF"></asp:Label></td>
                                                <td id="Td10" runat="server">
                                                <asp:CheckBox ID="LinkPDF" runat="server" AutoPostBack="True" OnCheckedChanged="Export_Changed" />
                                                </td>
                                            </tr>
                                            <tr id="rowCSV" runat="server">
                                                <td id="Td11" runat="server">
                                                    <asp:Label ID="Label79" runat="server" meta:resourcekey="LiteralResource79" Text="Export to CSV:" AssociatedControlID="LinkCSV"></asp:Label>
                                                    <asp:Label ID="lblCSVStar" runat="server" Text="*" />
                                                </td>
                                                <td id="Td12" runat="server">
                                                <asp:CheckBox ID="LinkCSV" runat="server" AutoPostBack="True" OnCheckedChanged="Export_Changed" />
                                                </td>
                                            </tr>
                                            <tr id="rowXML" runat="server">
                                                <td id="Td13" runat="server">
                                                    <asp:Label ID="Label80" runat="server" meta:resourcekey="LiteralResource80" Text="Export to XML:" AssociatedControlID="LinkXML"></asp:Label>
                                                    <asp:Label ID="lblXMLStar" runat="server" Text="*" />
                                                </td>
                                                <td id="Td14" runat="server">
                                                <asp:CheckBox ID="LinkXML" runat="server" AutoPostBack="True" OnCheckedChanged="Export_Changed" />
                                                </td>
                                            </tr>
                                            <tr id="rowArchive" runat="server">
                                                <td id="Td15" runat="server">
                        				            <asp:Label ID="Label1" runat="server" Text="<%$ Resources:LogiAdHoc, AddToArchive %>" AssociatedControlID="LinkArchive"></asp:Label>:
                                                </td>
                                                <td id="Td16" runat="server">
                                                <asp:CheckBox ID="LinkArchive" runat="server" AutoPostBack="True" OnCheckedChanged="Export_Changed" />
                                                </td>
                                            </tr>
                                            <tr id="rowSendMail" runat="server">
                                                <td id="Td17" runat="server">
                                                    <asp:Label ID="Label81" runat="server" meta:resourcekey="LiteralResource81" Text="Send Report By eMail:" AssociatedControlID="SendMail"></asp:Label>
                                                    &nbsp;
                                                    <asp:Label ID="lblddlSendEmailExportType" runat="server" CssClass="NoShow" Text="Export Type" AssociatedControlID="ddlSendEmailExportType" meta:resourcekey="lblddlSendEmailExportTypeResource1"></asp:Label>
                                                    <asp:DropDownList ID="ddlSendEmailExportType" runat="server" OnSelectedIndexChanged="Export_Changed">
                                                    </asp:DropDownList>
                                                </td>
                                                <td id="Td18" runat="server">
                                                <asp:CheckBox ID="SendMail" runat="server" AutoPostBack="True" OnCheckedChanged="Export_Changed" />
                                                </td>
                                            </tr>
                                        </table>
                                        <div id="divExportWarning" runat="server" class="info">
                                            <asp:Localize ID="Localize11" runat="server" meta:resourcekey="LiteralResource99" Text="* Exporting a grouped report to CSV or XML may not export all data."></asp:Localize>
                                        </div>
                                    </asp:Panel>
                                    <asp:Panel ID="pnlCrosstabSettings" runat="server" Height="350px" Width="100%" meta:resourcekey="pnlCrosstabSettingsResource1">
                                        <asp:ImageButton ID="ImageButton9" runat="server" OnClientClick="goHelp('16'); return false;" style="float:right;" AlternateText="Help" 
                                            CausesValidation="False" SkinID="imgbPnlHelp" ImageUrl="../ahImages/iconHelpText.gif" ToolTip="Get help with crosstab settings." meta:resourcekey="ImageButton9Resource1" />
                                        <table>
                                            <tr>
                                                <td width="150">
                                                    <asp:Label ID="Label54" runat="server" Text="<%$ Resources:LogiAdHoc, Title %>" AssociatedControlID="CrosstabTitle" meta:resourcekey="Label54Resource1"></asp:Label>:</td>
                                                <td>
                                                    <asp:TextBox ID="CrosstabTitle" runat="server" AutoPostBack="True" OnTextChanged="CrosstabSettings_Changed" Width="250px" meta:resourcekey="CrosstabTitleResource1"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td><asp:Localize ID="Localize82" runat="server" meta:resourcekey="LiteralResource31" Text="Paging Style:"></asp:Localize></td>
                                                <td>
                                                   <asp:DropDownList ID="ddlCTPagingStyle" runat="server" AutoPostBack="true" OnSelectedIndexChanged="CrosstabSettings_Changed" >
                                                        <asp:ListItem Value="1" meta:resourcekey="ListItemResource034" Text="Interactive Paging" />
                                                        <asp:ListItem Value="2" meta:resourcekey="ListItemResource035" Text="Numbered List" />
                                                        <asp:ListItem Value="3" meta:resourcekey="ListItemResource036" Text="Append Rows" />
                                                        <asp:ListItem Value="0" meta:resourcekey="ListItemResource037" Text="None" />
                                                    </asp:DropDownList>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td><asp:Localize ID="Localize20" runat="server" meta:resourcekey="LiteralResource192" Text="Paging Location:"></asp:Localize></td>
                                                <td>
                                                   <asp:DropDownList ID="ddlCTPagingLocation" runat="server" AutoPostBack="true" OnSelectedIndexChanged="CrosstabSettings_Changed" >
                                                        <asp:ListItem Value="0" meta:resourcekey="ListItemResource40" Text="Top" />
                                                        <asp:ListItem Value="1" meta:resourcekey="ListItemResource41" Text="Bottom" />
                                                        <asp:ListItem Value="2" meta:resourcekey="ListItemResource42" Text="Both" />
                                                    </asp:DropDownList>
                                                </td>
                                            </tr>
                                            <tr id="trCTRowsPerPage" runat="server">
                                                <td id="Td19" runat="server">
                                                    <asp:Label ID="Label33" runat="server" meta:resourcekey="LiteralResource32" 
                                                    Text="Rows Per Page:" AssociatedControlID="CTInteractiveRows"></asp:Label>
                                                </td>
                                                <td id="Td20" runat="server">
                                                    <asp:TextBox ID="CTInteractiveRows" Width="30px" MaxLength="3" runat="server" AutoPostBack="True" OnTextChanged="CrosstabSettings_Changed" />
                                                    <asp:RangeValidator ID="rvCTInteractiveRows" runat="server" ValidationGroup="CTSetting"
                                                        ControlToValidate="CTInteractiveRows" Type="Integer" MinimumValue="1" MaximumValue="999"
                                                        ErrorMessage="Number of rows can only be set to integer values between 1 and 999.">*</asp:RangeValidator>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:Label ID="Label198" runat="server" meta:resourcekey="LiteralResource34" 
                                                        Text="Rows Per Sub-Report Page:" AssociatedControlID="CTInteractiveRowsSub"></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="CTInteractiveRowsSub" Width="30px" MaxLength="3" runat="server" AutoPostBack="True" OnTextChanged="CrosstabSettings_Changed" meta:resourcekey="CTInteractiveRowsSubResource1" />
                                                    <asp:RangeValidator ID="rvCTInteractiveRowsSub" runat="server" ValidationGroup="CTSetting"
                                                        ControlToValidate="CTInteractiveRowsSub" Type="Integer" MinimumValue="1" MaximumValue="999"
                                                        ErrorMessage="Number of rows can only be set to integer values between 1 and 999."
                                                        meta:resourcekey="rvInteractiveRowsSubResource1">*</asp:RangeValidator>
                                                </td>
                                            </tr>
                                        </table>
                                        <asp:ValidationSummary ID="ValidationSummary8" runat="server" ValidationGroup="CTSetting"
                                            meta:resourcekey="ValidationSummary8Resource1" />
                                        <asp:Label ID="lblSemicolon8" runat="server" OnPreRender="lblSemicolon_PreRender" meta:resourcekey="lblSemicolon8Resource1" />
                                    </asp:Panel>
                                    <asp:Panel ID="pnlCrossTab" runat="server" Height="350px" Width="100%" meta:resourcekey="pnlCrossTabResource1">
                                        <asp:ImageButton ID="ImageButton10" runat="server" OnClientClick="goHelp('15'); return false;" style="float:right;" AlternateText="Help"
                                            CausesValidation="False" SkinID="imgbPnlHelp" ImageUrl="../ahImages/iconHelpText.gif" ToolTip="Get help with crosstab tables." meta:resourcekey="ImageButton10Resource1" />
                                        <span style="font-weight:bold;">
                                            <asp:ImageButton ID="imgOVCrosstab" runat="server" CausesValidation="False" ImageUrl="../ahImages/expand_blue.jpg"
                                                OnClientClick="javascript: noMessage = true;" OnCommand="ExpandCollapseOptions"
                                                ToolTip="<%$ Resources:LogiAdHoc, AdvancedOptionsTooltip %>" AlternateText="<%$ Resources:LogiAdHoc, AdvancedOptionsTooltip %>" />
                                            &nbsp;
                                            <asp:Localize ID="lOVCrosstab" runat="server" Text="<%$ Resources:LogiAdHoc, ShowExtra %>" />
                                            <br />
                                        </span>
                                        <asp:Panel ID="pnlCrosstabHeaderColumn" GroupingText="Header Values Column" runat="server" meta:resourcekey="pnlCrosstabHeaderColumnResource1" >
                                            <table>
                                                <tr>
                                                    <td width="135px">
                                                        <asp:Label ID="Label83" runat="server" Text="<%$ Resources:LogiAdHoc, Column %>" AssociatedControlID="CrosstabColumn" meta:resourcekey="Label83Resource1"></asp:Label>:
                                                    </td>
                                                    <td width="100">
                                                        <asp:DropDownList ID="CrosstabColumn" runat="server" AutoPostBack="True" meta:resourcekey="CrosstabColumnResource1" />
                                                    </td>
                                                    <td id="tdCTHeaderTimePeriod" runat="server" style="padding: 0;">
                                                        <table>
                                                            <tr>
                                                                <td width="135px">
                                                                   <asp:Label ID="Label5" runat="server" Text="Apply Time Period:" AssociatedControlID="ddlCTHeaderTPC" meta:resourcekey="Label5Resource1"></asp:Label>
                                                                </td>
                                                                <td width="100">
                                                                    <asp:DropDownList ID="ddlCTHeaderTPC" runat="server" AutoPostBack="True" meta:resourcekey="ddlCTHeaderTPCResource1" >
                                                                        <asp:ListItem Text="<%$ Resources:LogiAdHoc, TimePeriodNone %>" Value="" />
                                                                        <asp:ListItem Text="<%$ Resources:LogiAdHoc, TimePeriodYear %>" Value="Year" />
                                                                        <asp:ListItem Text="<%$ Resources:LogiAdHoc, TimePeriodQuarter %>" Value="FirstDayOfQuarter" />
                                                                        <asp:ListItem Text="<%$ Resources:LogiAdHoc, TimePeriodFiscalQuarter %>" Value="FirstDayOfFiscalQuarter" />
                                                                        <asp:ListItem Text="<%$ Resources:LogiAdHoc, TimePeriodMonth %>" Value="FirstDayOfMonth" />
                                                                    </asp:DropDownList>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </table>
                                            <asp:UpdatePanel ID="UPCrosstabHeaderColumn" runat="server" UpdateMode="Conditional" RenderMode="Inline">
                                                <ContentTemplate>
                                                    <table id="tbCrosstabHeaderColumn" runat="server">
                                                        <tr id="Tr1" runat="server">
                                                            <td id="Td21" width="135px" runat="server">
                                                                <asp:Label ID="Label199" runat="server" Text="Header:" AssociatedControlID="txtCTHeaderColHeader" 
                                                                    meta:resourcekey="LiteralResource189"></asp:Label>
                                                            </td>
                                                            <td id="Td22" width="100" runat="server">
                                                                <asp:TextBox ID="txtCTHeaderColHeader" runat="server" AutoPostBack="True" OnTextChanged="Crosstab_Changed" Width="200px"></asp:TextBox>
                                                            </td>
                                                        </tr>
                                                    </table>        
                                                </ContentTemplate>
                                            </asp:UpdatePanel>
                                        </asp:Panel>
                                        <asp:Panel ID="pnlCrosstabLabelColumn" GroupingText="Label Values Column" runat="server" meta:resourcekey="pnlCrosstabLabelColumnResource1" >
                                            <input type="hidden" id="hdLabelColumnDisplay" runat="server" />
                                            <div id="divCTSingleLabelColumn" runat="server">
                                                <table>
                                                    <tr>
                                                        <td  width="135px">
                                                            <asp:Label ID="Label84" runat="server" Text="<%$ Resources:LogiAdHoc, Column %>" AssociatedControlID="LabelColumn" meta:resourcekey="Label84Resource1"></asp:Label>:</td>
                                                        <td width="100">
                                                            <asp:DropDownList ID="LabelColumn" runat="server" AutoPostBack="True"  meta:resourcekey="LabelColumnResource1" />
                                                        </td>
                                                        <%--<td id="tdCTLabelTimePeriod" runat="server" style="padding: 0;">
                                                            <table>
                                                                <tr>
                                                                    <td width="135px">
                                                                       <asp:Label ID="Label10" runat="server" Text="Apply Time Period:" AssociatedControlID="ddlCTLabelTPC" meta:resourcekey="Label5Resource1"></asp:Label>
                                                                    </td>
                                                                    <td width="100">
                                                                        <asp:DropDownList ID="ddlCTLabelTPC" runat="server" AutoPostBack="True" meta:resourcekey="ddlCTLabelTPCResource1" >
                                                                            <asp:ListItem Text="<%$ Resources:LogiAdHoc, TimePeriodNone %>" Value="" />
                                                                            <asp:ListItem Text="<%$ Resources:LogiAdHoc, TimePeriodYear %>" Value="Year" />
                                                                            <asp:ListItem Text="<%$ Resources:LogiAdHoc, TimePeriodQuarter %>" Value="FirstDayOfQuarter" />
                                                                            <asp:ListItem Text="<%$ Resources:LogiAdHoc, TimePeriodFiscalQuarter %>" Value="FirstDayOfFiscalQuarter" />
                                                                            <asp:ListItem Text="<%$ Resources:LogiAdHoc, TimePeriodMonth %>" Value="FirstDayOfMonth" />
                                                                        </asp:DropDownList>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>--%>
                                                    </tr>
                                                </table>
                                                <table id="tblCT1" runat="server">
                                                    <tr id="Tr2" runat="server">
                                                        <td id="Td23" runat="server" style="padding: 0;">
                                                            <asp:UpdatePanel ID="UPLabelColumn1" runat="server" UpdateMode="Conditional" RenderMode="Inline">
                                                                <ContentTemplate>
                                                                    <table>
                                                                        <tr>
                                                                            <td width="135px">
                                                                                <asp:Label ID="Label189" runat="server" meta:resourcekey="LiteralResource189" 
                                                                                    Text="Header:" AssociatedControlID="txtCrosstabLabelHeader"></asp:Label></td>
                                                                            <td>
                                                                                <asp:TextBox ID="txtCrosstabLabelHeader" runat="server" AutoPostBack="true" OnTextChanged="Crosstab_Changed" Width="200px"></asp:TextBox>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td width="135px">
                                                                                <asp:Label ID="Label143" runat="server" meta:resourcekey="LiteralResource143" 
                                                                                    Text="Sortable:" AssociatedControlID="chkLabelColumnSort"></asp:Label>
                                                                            </td>
                                                                            <td>
                                                                                <asp:CheckBox ID="chkLabelColumnSort" AutoPostBack="true" OnCheckedChanged="Crosstab_Changed" runat="server" />
                                                                            </td>
                                                                        </tr>
                                                                        <tr id="trCrosstabLabelColLinked" runat="server">
                                                                            <td width="135px">
                                                                                <asp:Label ID="Label190" runat="server" meta:resourcekey="LiteralResource190" 
                                                                                    Text="Linked:" AssociatedControlID="chkCrosstabLabelColLinked"></asp:Label>
                                                                            </td>
                                                                            <td>
                                                                                <asp:CheckBox ID="chkCrosstabLabelColLinked" AutoPostBack="true" OnCheckedChanged="Crosstab_Changed" runat="server" />
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>
                                                                                <asp:Label ID="Label144" runat="server" meta:resourcekey="LiteralResource144" 
                                                                                    Text="Format:" AssociatedControlID="ddlLabelColumnFormat"></asp:Label>
                                                                            </td>
                                                                            <td>
                                                                                <asp:DropDownList ID="ddlLabelColumnFormat" runat="server" AutoPostBack="true" OnSelectedIndexChanged="Crosstab_Changed" />
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>    
                                                                                <asp:Label ID="Label39" runat="server" meta:resourcekey="LiteralResource146" 
                                                                                    Text="Alignment:" AssociatedControlID="ddlLabelColAlignment"></asp:Label>
                                                                            </td>
                                                                            <td>
                                                                                <asp:DropDownList ID="ddlLabelColAlignment" runat="server" AutoPostBack="true" OnSelectedIndexChanged="Crosstab_Changed" meta:resourcekey="alignmentResource1">
                                                                                    <asp:ListItem Value="Left" meta:resourcekey="ListItemResource6">Left</asp:ListItem>
                                                                                    <asp:ListItem Value="Right" meta:resourcekey="ListItemResource7">Right</asp:ListItem>
                                                                                    <asp:ListItem Value="Center" meta:resourcekey="ListItemResource8">Center</asp:ListItem>
                                                                                </asp:DropDownList>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </ContentTemplate>
                                                                <Triggers>
                                                                    <asp:AsyncPostBackTrigger ControlID="LabelColumn" EventName="SelectedIndexChanged" />
                                                                </Triggers>
                                                            </asp:UpdatePanel>
                                                        </td>
                                                        <td id="Td24" runat="server" >
                                                            <AdHoc:LogiButton ID="btnAddLabelColumns" runat="server" CausesValidation="False"
                                                                OnClick="btnAddLabelColumns_Click" Text="Add a Layer" meta:resourcekey="btnAddLabelColumnsResource1"/>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </div>
                                            <div id="divCTMultipleLabelColumns" runat="server">
                                                 <asp:Button ID="btnFakeGrdCTLabelColsReorderRows" runat="server" CssClass="NoShow" 
                                                    OnClick="btnFakeGrdCTLabelColsReorderRows_Click" meta:resourcekey="btnFakeGrdCTLabelColsReorderRowsResource1" />
                                                 <AdHoc:DDGridView ID="grdCTLabelCols" runat="server" AutoGenerateColumns="False" CssClass="gridWline"
                                                    meta:resourcekey="grdCTLabelColsResource1" OnRowCommand="OnCrosstabLabelItemCommandHandler"
                                                    OnRowDataBound="OnCrosstabLabelItemDataBoundHandler" DDDivID="divCTLDragHandle" 
                                                    PostbackButtonID="btnFakeGrdCTLabelColsReorderRows">
                                                    <Columns>
                                                        <asp:TemplateField HeaderText="Column Name" meta:resourcekey="grdCTLabelColsHeaderResource1">
                                                            <ItemTemplate>
<input runat="server" ID="CTLRowOrder" type="hidden" />

                                                                <table cellpadding="0" cellspacing="0">
                                                                    <tr>
                                                                        <td>
                                                                            <div runat="server" ID="divCTLDragHandle" class="dragHandle"></div>

                                                                        </td>
                                                                        <td>
                                                                            <input runat="server" ID="hdLabelColumnID" type="hidden" />

                                                                            <asp:Label runat="server" ID="lblCTLColumnName" meta:resourcekey="lblCTLColumnNameResource1"></asp:Label>

                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            
</ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Header" meta:resourcekey="grdCTLabelColsHeaderResource2">
                                                            <ItemTemplate>
<asp:Label runat="server" ID="lblCTLHeader" meta:resourcekey="lblCTLHeaderResource1"></asp:Label>

                                                            
</ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Sortable" meta:resourcekey="grdCTLabelColsHeaderResource3">
                                                            <ItemTemplate>
<asp:Label runat="server" ID="lblCTLSortable" meta:resourcekey="lblCTLSortableResource1"></asp:Label>

                                                            
</ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Linked" meta:resourcekey="grdCTLabelColsHeaderResource4">
                                                            <ItemTemplate>
<asp:Label runat="server" ID="lblCTLLinked" meta:resourcekey="lblCTLLinkedResource1"></asp:Label>

                                                            
</ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Format" meta:resourcekey="grdCTLabelColsHeaderResource5">
                                                            <ItemTemplate>
<asp:Label runat="server" ID="lblCTLFormat" meta:resourcekey="lblCTLFormatResource1"></asp:Label>

                                                            
</ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Alignment" meta:resourcekey="grdCTLabelColsHeaderResource6">
                                                            <ItemTemplate>
<asp:Label runat="server" ID="lblCTLAlignment" meta:resourcekey="lblCTLAlignmentResource1"></asp:Label>

                                                            
</ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="<%$ Resources:LogiAdHoc, Actions %>" meta:resourcekey="TemplateFieldResource8">
                                                            <ItemTemplate>
<asp:ImageButton runat="server" CommandName="LabelColumnMoveUp" CausesValidation="False" AlternateText="Move Label Column Up" ImageUrl="../ahImages/SmallArrowUp.gif" ToolTip="Move Label Column Up" ID="imgMoveupLabelColumn" meta:resourceKey="imgMoveupLabelColumnResource1"></asp:ImageButton>

                                                                <asp:ImageButton runat="server" CommandName="LabelColumnMoveDown" CausesValidation="False" AlternateText="Move Label Column Down" ImageUrl="../ahImages/SmallArrowDown.gif" ToolTip="Move Label Column Down" ID="imgMovedownLabelColumn" meta:resourceKey="imgMovedownLabelColumnResource1"></asp:ImageButton>

                                                                &nbsp;
                                                                <asp:ImageButton runat="server" CommandName="EditCTLabelCol" CausesValidation="False" AlternateText="Edit Item" ImageUrl="../ahImages/modify.gif" ToolTip="Edit" ID="EditLabelColumn" meta:resourceKey="EditItemResource1"></asp:ImageButton>

                                                                <asp:ImageButton runat="server" CommandName="RemoveCTLabelCol" AlternateText="<%$ Resources:LogiAdHoc, RemoveItem %>" ImageUrl="../ahImages/remove.gif" ToolTip="<%$ Resources:LogiAdHoc, RemoveItem %>" ID="RemoveLabelColumn"></asp:ImageButton>

                                                            
</ItemTemplate>
                                                            <FooterStyle VerticalAlign="Top" />
                                                            <HeaderStyle Width="50px" />
                                                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Top" Wrap="False" />
                                                        </asp:TemplateField>
                                                    </Columns>
                                                    <FooterStyle CssClass="gridfooter" />
                                                    <HeaderStyle CssClass="gridheader" />
                                                </AdHoc:DDGridView>
                                                <AdHoc:LogiButton ID="btnAddLabelColumns1" runat="server" OnClick="btnAddLabelColumns_Click"
                                                    Text="Add a Layer" meta:resourcekey="btnAddLabelColumnsResource1"/>
                                                <br />
                                                <br />
                                                <div id="divCTShowExpandCollapseAllOption" runat="server">
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                <asp:Label ID="Label200" runat="server" meta:resourcekey="ShowExpandCollapseAllResource"
                                                                    Text="Show an option to Expand/Collapse all drill-down rows:" AssociatedControlID="chkCTShowExpandCollapseOption"></asp:Label>
                                                            </td>
                                                            <td>
                                                                <asp:CheckBox ID="chkCTShowExpandCollapseOption" AutoPostBack="True" OnCheckedChanged="Crosstab_Changed" runat="server" meta:resourcekey="chkCTShowExpandCollapseOptionResource1" />
                                                            </td>
                                                        </tr>
                                                    </table>
                                                    <p>
                                                    </p>
                                                </div>
                                                <div id="divCTAutoExpandDrillDownRows" runat="server">
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                <asp:Label ID="Label18" runat="server" meta:resourcekey="AutoExpandDrillDownRowsResource"
                                                                    Text="Auto Expand all drill-down rows:" AssociatedControlID="chkCTAutoExpandDrillDownRows"></asp:Label>
                                                            </td>
                                                            <td>
                                                                <asp:CheckBox ID="chkCTAutoExpandDrillDownRows" AutoPostBack="True" OnCheckedChanged="Crosstab_Changed" runat="server" meta:resourcekey="chkCTAutoExpandDrillDownRowsResource1" />
                                                            </td>
                                                        </tr>
                                                    </table>
                                                    <p>
                                                    </p>
                                                </div>
                                            </div>
                                            
                                        </asp:Panel>
                                        <asp:Panel ID="pnlCrosstabValuesColumn" GroupingText="Values Column" runat="server" meta:resourcekey="pnlCrosstabValuesColumnResource1" >
                                            <input type="hidden" id="hdValueColumnDisplay" runat="server" />
                                            <div id="divCTSingleValueColumn" runat="server">
                                                <table>
                                                    <tr>
                                                        <td  width="135">
                                                            <asp:Label ID="Label85" runat="server" Text="<%$ Resources:LogiAdHoc, Column %>"
                                                                 AssociatedControlID="ValueColumn" meta:resourcekey="Label85Resource1"></asp:Label>:
                                                        </td>
                                                        <td width="100">
                                                            <asp:DropDownList ID="ValueColumn" runat="server" AutoPostBack="True" meta:resourcekey="ValueColumnResource1" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <asp:Label ID="Label86" runat="server" meta:resourcekey="LiteralResource86" 
                                                                Text="Aggregate Function:" AssociatedControlID="AggregateFunction"></asp:Label>
                                                        </td>
                                                        <td>
                                                            <asp:DropDownList ID="AggregateFunction" runat="server" AutoPostBack="True" OnSelectedIndexChanged="Crosstab_Changed" meta:resourcekey="AggregateFunctionResource1">
                                                                <asp:ListItem Value="Sum" Text="<%$ Resources:LogiAdHoc, Sum %>" />
                                                                <asp:ListItem Value="Average" Text="<%$ Resources:LogiAdHoc, Average %>" />
                                                                <asp:ListItem Value="STDEV" Text="<%$ Resources:LogiAdHoc, StandardDeviation %>" />
                                                                <asp:ListItem Value="Count" Text="<%$ Resources:LogiAdHoc, Count %>" />
                                                            </asp:DropDownList>
                                                        </td>
                                                    </tr>
                                                </table>
                                                <table id="tblCT2" runat="server">
                                                    <tr id="Tr3" runat="server">
                                                        <td id="Td25" runat="server" style="padding: 0;">
                                                            <table>
                                                                <tr>
                                                                    <td width="135">
                                                                        <asp:Label ID="Label145" runat="server" meta:resourcekey="LiteralResource143" 
                                                                            Text="Sortable:" AssociatedControlID="chkValueColumnSort"></asp:Label>
                                                                    </td>
                                                                    <td>
                                                                        <asp:CheckBox ID="chkValueColumnSort" runat="server" AutoPostBack="True" OnCheckedChanged="Crosstab_Changed" />
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <asp:Label ID="Label146" runat="server" meta:resourcekey="LiteralResource144" 
                                                                            Text="Format:" AssociatedControlID="ddlValueColumnFormat"></asp:Label>
                                                                    </td>
                                                                    <td>
                                                                        <asp:UpdatePanel ID="upValueColFormat" runat="server" UpdateMode="Conditional" RenderMode="Inline">
                                                                            <ContentTemplate>
                                                                                <asp:DropDownList ID="ddlValueColumnFormat" runat="server" AutoPostBack="true" OnSelectedIndexChanged="Crosstab_Changed" />
                                                                            </ContentTemplate>
                                                                            <Triggers>
                                                                                <asp:AsyncPostBackTrigger ControlID="ValueColumn" EventName="SelectedIndexChanged" />
                                                                            </Triggers>
                                                                        </asp:UpdatePanel>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <asp:Label ID="Label43" runat="server" meta:resourcekey="LiteralResource146" 
                                                                            Text="Alignment:" AssociatedControlID="ddlValuesColAlignment"></asp:Label>
                                                                    </td>
                                                                    <td>
                                                                        <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional" >
                                                                            <ContentTemplate>
                                                                                <asp:DropDownList ID="ddlValuesColAlignment" runat="server" AutoPostBack="true" OnSelectedIndexChanged="Crosstab_Changed" meta:resourcekey="alignmentResource1">
                                                                                    <asp:ListItem Value="Left" meta:resourcekey="ListItemResource6">Left</asp:ListItem>
                                                                                    <asp:ListItem Value="Right" meta:resourcekey="ListItemResource7">Right</asp:ListItem>
                                                                                    <asp:ListItem Value="Center" meta:resourcekey="ListItemResource8">Center</asp:ListItem>
                                                                                </asp:DropDownList>
                                                                            </ContentTemplate>
                                                                            <Triggers>
                                                                                <asp:AsyncPostBackTrigger ControlID="ValueColumn" EventName="SelectedIndexChanged" />
                                                                            </Triggers>
                                                                        </asp:UpdatePanel>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <asp:Localize ID="Localize183" runat="server" meta:resourcekey="LiteralResource183"
                                                                            Text="Style:"></asp:Localize>
                                                                    </td>
                                                                    <td>
                                                                        <asp:ImageButton ID="imgCrosstabStyle" runat="server" AlternateText="Modify Style" CausesValidation="False"
                                                                            ImageUrl="../ahImages/modify.gif" meta:resourcekey="ModifyResource1" OnClick="ModifyCrosstabStyle" 
                                                                            ToolTip="Modify Style" />
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                        <td id="Td26" runat="server" >
                                                            <AdHoc:LogiButton ID="btnAddValueColumns" runat="server" CausesValidation="False"
                                                                OnClick="btnAddValueColumns_Click" Text="Add Extra Value Columns" meta:resourcekey="btnAddValueColumnsResource1"/>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </div>
                                            <div id="divCTMultipleValueColumns" runat="server">
                                                <asp:Button ID="btnFakeGrdCTValueColsReorderRows" runat="server" CssClass="NoShow" 
                                                    OnClick="btnFakeGrdCTValueColsReorderRows_Click" meta:resourcekey="btnFakeGrdCTValueColsReorderRowsResource1" />
                                                <AdHoc:DDGridView ID="grdCTValueCols" runat="server" AutoGenerateColumns="False" CssClass="gridWline"
                                                    OnRowCommand="OnCrosstabValueItemCommandHandler" OnRowDataBound="OnCrosstabValueItemDataBoundHandler"
                                                    DDDivID="divCTVDragHandle" PostbackButtonID="btnFakeGrdCTValueColsReorderRows" meta:resourcekey="grdCTValueColsResource1">
                                                    <Columns>
                                                        <asp:TemplateField HeaderText="Column Name" meta:resourcekey="grdCTValueColsResource1">
                                                            <ItemTemplate>
<input runat="server" ID="CTVRowOrder" type="hidden" />

                                                                <table cellpadding="0" cellspacing="0">
                                                                    <tr>
                                                                        <td>
                                                                            <div runat="server" ID="divCTVDragHandle" class="dragHandle"></div>

                                                                        </td>
                                                                        <td>
                                                                            <input runat="server" ID="hdValueColumnID" type="hidden" />

                                                                            <asp:Label runat="server" ID="lblCTVColumnName" meta:resourcekey="lblCTVColumnNameResource1"></asp:Label>

                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            
</ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Aggregate Function" meta:resourcekey="grdCTValueColsResource2">
                                                            <ItemTemplate>
<asp:Label runat="server" ID="lblAggregateFunction" meta:resourcekey="lblAggregateFunctionResource1"></asp:Label>

                                                            
</ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Header" meta:resourcekey="grdCTValueColsResource3">
                                                            <ItemTemplate>
<asp:Label runat="server" ID="lblCTVHeader" meta:resourcekey="lblCTVHeaderResource1"></asp:Label>

                                                            
</ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Sortable" meta:resourcekey="grdCTValueColsResource4">
                                                            <ItemTemplate>
<asp:Label runat="server" ID="lblCTVSortable" meta:resourceKey="lblValueResource1"></asp:Label>

                                                            
</ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Format" meta:resourcekey="grdCTValueColsResource5">
                                                            <ItemTemplate>
<asp:Label runat="server" ID="lblCTVFormat" meta:resourcekey="lblCTVFormatResource1"></asp:Label>

                                                            
</ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Alignment" meta:resourcekey="grdCTValueColsResource6">
                                                            <ItemTemplate>
<asp:Label runat="server" ID="lblCTVAlignment" meta:resourcekey="lblCTVAlignmentResource1"></asp:Label>

                                                            
</ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Summary Row" meta:resourcekey="grdCTValueColsResource7">
                                                            <ItemTemplate>
<asp:ImageButton runat="server" CommandName="AddSummaryRow" CausesValidation="False" AlternateText="Modify Summary Row" ImageUrl="../ahImages/SetSummaryRowOff.gif" ToolTip="Modify Summary Row" ID="imgSummaryRow" meta:resourceKey="imgSummaryRowResource1"></asp:ImageButton>

                                                            
</ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Summary Column" meta:resourcekey="grdCTValueColsResource8">
                                                            <ItemTemplate>
<asp:ImageButton runat="server" CommandName="AddSummaryColumn" CausesValidation="False" AlternateText="Modify Summary Column" ImageUrl="../ahImages/SetSummaryColumnoff.gif" ToolTip="Modify Summary Column" ID="imgSummaryColumn" meta:resourceKey="imgSummaryColumnResource1"></asp:ImageButton>

                                                            
</ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Style" meta:resourcekey="grdCTValueColsResource9">
                                                            <ItemTemplate>
<asp:ImageButton runat="server" CommandName="AddValueColumnStyle" CausesValidation="False" AlternateText="Modify Value Column Style" ImageUrl="../ahImages/modify.gif" ToolTip="Modify Value Column Style" ID="imgValueColumnStyle" meta:resourceKey="imgValueColumnStyleResource1"></asp:ImageButton>

                                                            
</ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="<%$ Resources:LogiAdHoc, Actions %>" meta:resourcekey="TemplateFieldResource9">
                                                            <ItemTemplate>
<asp:ImageButton runat="server" CommandName="ValueColumnMoveUp" CausesValidation="False" AlternateText="Move Value Column Up" ImageUrl="../ahImages/SmallArrowUp.gif" ToolTip="Move Value Column Up" ID="imgMoveupValueColumn" meta:resourceKey="imgMoveupValueColumnResource1"></asp:ImageButton>

                                                                <asp:ImageButton runat="server" CommandName="ValueColumnMoveDown" CausesValidation="False" AlternateText="Move Value Column Down" ImageUrl="../ahImages/SmallArrowDown.gif" ToolTip="Move Value Column Down" ID="imgMovedownValueColumn" meta:resourceKey="imgMovedownValueColumnResource1"></asp:ImageButton>

                                                                &nbsp;
                                                                <asp:ImageButton runat="server" CommandName="ModifyValueColumn" CausesValidation="False" AlternateText="Edit Item" ImageUrl="../ahImages/modify.gif" ToolTip="Edit" ID="imgModifyValueColumn" meta:resourceKey="EditItemResource1"></asp:ImageButton>

                                                                <asp:ImageButton runat="server" CommandName="RemoveValueColumn" AlternateText="<%$ Resources:LogiAdHoc, RemoveItem %>" ImageUrl="../ahImages/remove.gif" ToolTip="<%$ Resources:LogiAdHoc, RemoveItem %>" ID="imgRemoveValueColumn"></asp:ImageButton>

                                                            
</ItemTemplate>
                                                            <FooterStyle VerticalAlign="Top" />
                                                            <HeaderStyle Width="50px" />
                                                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Top" Wrap="False" />
                                                        </asp:TemplateField>
                                                    </Columns>
                                                    <FooterStyle CssClass="gridfooter" />
                                                    <HeaderStyle CssClass="gridheader" />
                                                </AdHoc:DDGridView>
                                                <AdHoc:LogiButton ID="btnAddValueColumns1" runat="server" OnClick="btnAddValueColumns_Click"
                                                    Text="Add Extra Value Columns" meta:resourcekey="btnAddValueColumnsResource1"/>
                                                <br />
                                                <br />
                                            </div>
                                        </asp:Panel>
                                        <div id="divCrosstabSummary" runat="server">
                                            <asp:Panel ID="pnlCrosstabSummaryRow" GroupingText="Summary Row &#160;" runat="server" meta:resourcekey="pnlCrosstabSummaryRowResource1" >
                                                <table>
                                                    <tr>
                                                        <td width="135px">
                                                            <asp:Label ID="Label87" runat="server" meta:resourcekey="LiteralResource87" 
                                                                Text="Include:" AssociatedControlID="chkSummaryRow"></asp:Label>
                                                        </td>
                                                        <td>
                                                            <asp:CheckBox ID="chkSummaryRow" AutoPostBack="True" runat="server" OnCheckedChanged="SummaryRow_Changed"
                                                                meta:resourcekey="chkSummaryRowResource1"></asp:CheckBox>
                                                        </td>
                                                    </tr>
                                                    <tr id="trSumRowCaption" runat="server">
                                                        <td id="Td27" runat="server">
                                                            <asp:Label ID="Label88" runat="server" meta:resourcekey="LiteralResource88" 
                                                                Text="Caption:" AssociatedControlID="txtSumRowCaption"></asp:Label>
                                                        </td>
                                                        <td id="Td28" runat="server">
                                                            <asp:TextBox ID="txtSumRowCaption" runat="server" AutoPostBack="True" OnTextChanged="Crosstab_Changed" Text="Summary" meta:resourcekey="SummaryResource1"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                    <tr id="trSumRowFunc" runat="server">
                                                        <td id="Td29" runat="server">
                                                        <asp:Label ID="Label89" runat="server" meta:resourcekey="LiteralResource89" 
                                                                Text="Aggregate Function:" AssociatedControlID="ddlSumRowFunc"></asp:Label>
                                                        </td>
                                                        <td id="Td30" runat="server">
                                                            <asp:DropDownList ID="ddlSumRowFunc" runat="server" AutoPostBack="True" OnSelectedIndexChanged="Crosstab_Changed">
                                                                <asp:ListItem Value="Sum" Text="<%$ Resources:LogiAdHoc, Sum %>" />
                                                                <asp:ListItem Value="Average" Text="<%$ Resources:LogiAdHoc, Average %>" />
                                                                <asp:ListItem Value="STDEV" Text="<%$ Resources:LogiAdHoc, StandardDeviation %>" />
                                                                <asp:ListItem Value="Count" Text="<%$ Resources:LogiAdHoc, Count %>" />
                                                                <asp:ListItem Value="DistinctCount" Text="<%$ Resources:LogiAdHoc, DistinctCount %>" />
                                                                <asp:ListItem Value="AverageOfAllRows" Text="<%$ Resources:LogiAdHoc, AverageOfAllRows %>" />
                                                                <asp:ListItem Value="CountOfAllRows" Text="<%$ Resources:LogiAdHoc, CountOfAllRows %>" />
                                                            </asp:DropDownList>
                                                        </td>
                                                    </tr>
                                                    <tr id="trSumRowFormat" runat="server">
                                                        <td id="Td31" runat="server">
                                                            <asp:Label ID="Label150" runat="server" meta:resourcekey="LiteralResource144" 
                                                                Text="Format:" AssociatedControlID="ddlSumRowFormat"></asp:Label>
                                                        </td>
                                                        <td id="Td32" runat="server">
                                                            <asp:DropDownList ID="ddlSumRowFormat" runat="server" AutoPostBack="True" OnSelectedIndexChanged="Crosstab_Changed" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </asp:Panel>
                                            <asp:Panel ID="pnlCrosstabSummaryCol" GroupingText="Summary Column" runat="server" meta:resourcekey="pnlCrosstabSummaryColResource1" >
                                                <table>
                                                    <tr>
                                                        <td width="135px">
                                                            <asp:Label ID="Label90" runat="server" meta:resourcekey="LiteralResource90" 
                                                                Text="Include:" AssociatedControlID="chkSummaryColumn"></asp:Label>
                                                        </td>
                                                        <td>
                                                            <asp:CheckBox ID="chkSummaryColumn" AutoPostBack="True" runat="server" OnCheckedChanged="SummaryColumn_Changed"
                                                                meta:resourcekey="chkSummaryColumnResource1"></asp:CheckBox>
                                                        </td>
                                                    </tr>
                                                    <tr id="trSumColHeader" runat="server">
                                                        <td id="Td33" runat="server">
                                                            <asp:Label ID="Label91" runat="server" meta:resourcekey="LiteralResource91" 
                                                                Text="Header:" AssociatedControlID="txtSumColHeader"></asp:Label>
                                                        </td>
                                                        <td id="Td34" runat="server">
                                                            <asp:TextBox ID="txtSumColHeader" runat="server" AutoPostBack="True" OnTextChanged="Crosstab_Changed" Text="Summary" meta:resourcekey="SummaryResource1"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                    <tr id="trSumColFunc" runat="server">
                                                        <td id="Td35" runat="server">
                                                            <asp:Label ID="Label92" runat="server" meta:resourcekey="LiteralResource92" 
                                                                Text="Aggregate Function:" AssociatedControlID="ddlSumColFunc"></asp:Label>
                                                        </td>
                                                        <td id="Td36" runat="server">
                                                            <asp:DropDownList ID="ddlSumColFunc" runat="server" AutoPostBack="True" OnSelectedIndexChanged="Crosstab_Changed">
                                                                <asp:ListItem Value="Sum" Text="<%$ Resources:LogiAdHoc, Sum %>" />
                                                                <asp:ListItem Value="Average" Text="<%$ Resources:LogiAdHoc, Average %>" />
                                                                <asp:ListItem Value="STDEV" Text="<%$ Resources:LogiAdHoc, StandardDeviation %>" />
                                                                <asp:ListItem Value="Count" Text="<%$ Resources:LogiAdHoc, Count %>" />
                                                                <asp:ListItem Text="<%$ Resources:LogiAdHoc, AverageOfAllRows %>" Value="AverageOfAllRows" />
                                                                <asp:ListItem Text="<%$ Resources:LogiAdHoc, CountOfAllRows %>" Value="CountOfAllRows" />
                                                            </asp:DropDownList>
                                                        </td>
                                                    </tr>
                                                    <tr id="trSumColFormat" runat="server">
                                                        <td id="Td37" runat="server">
                                                            <asp:Label ID="Label151" runat="server" meta:resourcekey="LiteralResource144" 
                                                                Text="Format:" AssociatedControlID="ddlSumColFormat"></asp:Label>
                                                        </td>
                                                        <td id="Td38" runat="server">
                                                            <asp:DropDownList ID="ddlSumColFormat" runat="server" AutoPostBack="True" OnSelectedIndexChanged="Crosstab_Changed" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </asp:Panel>
                                        </div>
                                    </asp:Panel>
                                    <asp:Panel ID="pnlHeatmap" runat="server" Height="350px" Width="100%" meta:resourcekey="pnlHeatmapResource1">
                                        <asp:ImageButton ID="ImageButton11" runat="server" OnClientClick="goHelp('19'); return false;" style="float:right;" AlternateText="Help"
                                            CausesValidation="False" SkinID="imgbPnlHelp" ImageUrl="../ahImages/iconHelpText.gif" ToolTip="Get help creating heat maps." meta:resourcekey="ImageButton11Resource1" />
                                        <span style="font-weight:bold;">
                                            <asp:ImageButton ID="imgOVHeatmap" runat="server" CausesValidation="False" ImageUrl="../ahImages/expand_blue.jpg"
                                                OnClientClick="javascript: noMessage = true;" OnCommand="ExpandCollapseOptions"
                                                ToolTip="<%$ Resources:LogiAdHoc, AdvancedOptionsTooltip %>" AlternateText="<%$ Resources:LogiAdHoc, AdvancedOptionsTooltip %>" />
                                            &nbsp;
                                            <asp:Localize ID="lOVHeatmap" runat="server" Text="<%$ Resources:LogiAdHoc, ShowExtra %>" />
                                            <br />
                                        </span>
                                        <table>
                                            <tr id="trHMTitle" runat="server">
                                                <td id="Td39" width="135" runat="server">
                                                <asp:Label ID="Label55" runat="server" Text="<%$ Resources:LogiAdHoc, Title %>" AssociatedControlID="txtHMTitle"></asp:Label>:</td>
                                                <td id="Td40" runat="server">
                                                    <asp:TextBox ID="txtHMTitle" runat="server" AutoPostBack="True" OnTextChanged="Heatmap_Changed" Width="250px" meta:resourcekey="txtHMTitleResource1"></asp:TextBox>
                                                </td>
                                            </tr>
                                        </table>
                                        <asp:Panel ID="pnlHMLabelColumn" runat="server" GroupingText="Label Column" meta:resourcekey="pnlHMLabelColumnResource1">
                                            <asp:UpdatePanel ID="UPHeatmapLabelColumn" runat="server" UpdateMode="Conditional">
                                                <ContentTemplate>
                                                    <table>
                                                        <tr>
                                                            <td width="135">
                                                                <asp:Label ID="Label56" runat="server" Text="<%$ Resources:LogiAdHoc, Column %>" AssociatedControlID="ddlHMLabelColumn" meta:resourcekey="Label56Resource1"></asp:Label>:</td>
                                                            <td>
                                                                <asp:DropDownList ID="ddlHMLabelColumn" runat="server" AutoPostBack="True" meta:resourcekey="ddlHMLabelColumnResource1">
                                                                </asp:DropDownList>
                                                            </td>
                                                        </tr>
                                                        <tr id="trHMLabelCaption" runat="server">
                                                            <td id="Td41" runat="server">
                                                            <asp:Label ID="Label57" runat="server" Text="<%$ Resources:LogiAdHoc, Caption %>" AssociatedControlID="txtHMLabelCaption"></asp:Label>:</td>
                                                            <td id="Td42" runat="server">
                                                                <asp:TextBox ID="txtHMLabelCaption" runat="server" AutoPostBack="True" OnTextChanged="Heatmap_Changed" meta:resourcekey="txtHMLabelCaptionResource1"></asp:TextBox>
                                                            </td>
                                                        </tr>
                                                        <tr id="trHMLabelColumnLinked" runat="server">
                                                            <td id="tdHMLabelColumnLinkedCaption" runat="server">
                                                                <asp:Localize ID="Localize201" runat="server" meta:resourcekey="LiteralResource190"
                                                                    Text="Linked:"></asp:Localize>
                                                            </td>
                                                            <td id="tdHMLabelColumnLinkSettings" runat="server">
                                                                <asp:CheckBox ID="chkHeatmapLinked" runat="server" AutoPostBack="True" />
                                                                <asp:DropDownList ID="ddlHeatmapLink" runat="server" AutoPostBack="True" meta:resourcekey="ddlHeatmapLinkResource1"
                                                                    OnSelectedIndexChanged="Heatmap_Changed">
                                                                </asp:DropDownList>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </ContentTemplate>
                                            </asp:UpdatePanel>
                                        </asp:Panel>
                                        <asp:Panel ID="pnlHMColorColumn" runat="server" GroupingText="Cell Color Column"
                                            meta:resourcekey="pnlHMColorColumnResource1">
                                            <table>
                                                <tr>
                                                    <td width="135">
                                                    <asp:Label ID="Label58" runat="server" Text="<%$ Resources:LogiAdHoc, Column %>" AssociatedControlID="ddlHMColorColumn" meta:resourcekey="Label58Resource1"></asp:Label>:</td>
                                                    <td>
                                                        <asp:DropDownList ID="ddlHMColorColumn" AutoPostBack="True" runat="server" meta:resourcekey="ddlHMColorColumnResource1" />
                                                    </td>
                                                </tr>
                                                <tr id="trHMColorCaption" runat="server">
                                                    <td id="Td43" runat="server">
                                                    <asp:Label ID="Label59" runat="server" Text="<%$ Resources:LogiAdHoc, Caption %>" AssociatedControlID="txtHMColorCaption"></asp:Label>:</td>
                                                    <td id="Td44" runat="server">
                                                        <asp:TextBox ID="txtHMColorCaption" runat="server" AutoPostBack="True" OnTextChanged="Heatmap_Changed" meta:resourcekey="txtHMColorCaptionResource1"></asp:TextBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                    <asp:Label ID="Label60" runat="server" Text="<%$ Resources:LogiAdHoc, Aggregate %>" AssociatedControlID="ddlColorAggregate" meta:resourcekey="Label60Resource1"></asp:Label>:</td>
                                                    <td>
                                                        <asp:DropDownList ID="ddlColorAggregate" runat="server" AutoPostBack="True" OnSelectedIndexChanged="Heatmap_Changed" meta:resourcekey="ddlColorAggregateResource1" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </asp:Panel>
                                        <asp:Panel ID="pnlHMSizeColumn" runat="server" GroupingText="Cell Size Column" meta:resourcekey="pnlHMSizeColumnResource1">
                                            <table>
                                                <tr>
                                                    <td width="135">
                                                    <asp:Label ID="Label64" runat="server" Text="<%$ Resources:LogiAdHoc, Column %>" AssociatedControlID="ddlHMSizeColumn" meta:resourcekey="Label64Resource1"></asp:Label>:</td>
                                                    <td>
                                                        <asp:DropDownList ID="ddlHMSizeColumn" AutoPostBack="True" runat="server" meta:resourcekey="ddlHMSizeColumnResource1" />
                                                    </td>
                                                </tr>
                                                <tr id="trHMSizeCaption" runat="server">
                                                    <td id="Td45" runat="server">
                                                    <asp:Label ID="Label67" runat="server" Text="<%$ Resources:LogiAdHoc, Caption %>" AssociatedControlID="txtHMSizeCaption"></asp:Label>:</td>
                                                    <td id="Td46" runat="server">
                                                        <asp:TextBox ID="txtHMSizeCaption" runat="server" AutoPostBack="True" OnTextChanged="Heatmap_Changed" meta:resourcekey="txtHMSizeCaptionResource1"></asp:TextBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                     <asp:Label ID="Label61" runat="server" Text="<%$ Resources:LogiAdHoc, Aggregate %>" AssociatedControlID="ddlSizeAggregate" meta:resourcekey="Label61Resource1"></asp:Label>:</td>
                                                    <td>
                                                        <asp:DropDownList ID="ddlSizeAggregate" AutoPostBack="True" OnSelectedIndexChanged="Heatmap_Changed" runat="server" meta:resourcekey="ddlSizeAggregateResource1" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </asp:Panel>
                                        <asp:Panel ID="pnlHMStyle" runat="server" GroupingText="Color Slider" meta:resourcekey="pnlHMStyleResource1">
                                            <table>
                                                <tr>
                                                    <td width="135">
                                                        <asp:Label ID="Label93" runat="server" meta:resourcekey="LiteralResource93" Text="Low Value Color:" AssociatedControlID="ddlHMLeftColor"></asp:Label></td>
                                                    <td>
                                                        <asp:DropDownList ID="ddlHMLeftColor" runat="server" AutoPostBack="True" OnSelectedIndexChanged="Heatmap_Changed" meta:resourcekey="ddlHMLeftColorResource1" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <asp:Label ID="Label94" runat="server" meta:resourcekey="LiteralResource94" Text="Middle Value Color:" AssociatedControlID="ddlHMCenterColor"></asp:Label></td>
                                                    <td>
                                                        <asp:DropDownList ID="ddlHMCenterColor" runat="server" AutoPostBack="True" OnSelectedIndexChanged="Heatmap_Changed" meta:resourcekey="ddlHMCenterColorResource1" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <asp:Label ID="Label95" runat="server" meta:resourcekey="LiteralResource95" 
                                                            Text="High Value Color:" AssociatedControlID="ddlHMRightColor"></asp:Label></td>
                                                    <td>
                                                        <asp:DropDownList ID="ddlHMRightColor" runat="server" AutoPostBack="True" OnSelectedIndexChanged="Heatmap_Changed" meta:resourcekey="ddlHMRightColorResource1" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </asp:Panel>
                                        <table id="tbHMSize" runat="server">
                                            <tr id="Tr4" runat="server">
                                                <td id="Td47" width="135" runat="server">
                                                    <asp:Label ID="lblHMSize" runat="server" Text="Size:" AssociatedControlID="ddlHMSize" meta:resourcekey="lblHMSizeResource1"></asp:Label></td>
                                                <td id="Td48" runat="server">
                                                    <asp:DropDownList ID="ddlHMSize" runat="server" AutoPostBack="True" OnSelectedIndexChanged="Heatmap_Changed" meta:resourcekey="ddlHMSizeResource1">
                                                        <asp:ListItem Value="Small" meta:resourcekey="ListItemResource17">Small</asp:ListItem>
                                                        <asp:ListItem Value="Medium" meta:resourcekey="ListItemResource18">Medium</asp:ListItem>
                                                        <asp:ListItem Value="Large" meta:resourcekey="ListItemResource19">Large</asp:ListItem>
                                                    </asp:DropDownList>
                                                </td>
                                            </tr>
                                        </table>
                                    </asp:Panel>
                                    <asp:Panel ID="pnlChart" runat="server" Height="350px" Width="100%" meta:resourcekey="pnlChartResource1">
                                        <asp:ImageButton ID="imgbChartHelp" runat="server" OnClientClick="goHelp('17'); return false;" style="float:right;" AlternateText="Help"
                                            CausesValidation="False" SkinID="imgbPnlHelp" ImageUrl="../ahImages/iconHelpText.gif" ToolTip="Get help creating charts." meta:resourcekey="imgbChartHelpResource1" />
                                        <input type="hidden" id="ChartID" runat="server" />
                                        <table>
                                        <tr>
                                        <td valign="top">
                                        <span style="font-weight:bold;">
                                            <asp:ImageButton ID="imgOVChart" runat="server" CausesValidation="False" ImageUrl="../ahImages/expand_blue.jpg"
                                                OnClientClick="javascript: noMessage = true;" OnCommand="ExpandCollapseOptions"
                                                ToolTip="<%$ Resources:LogiAdHoc, AdvancedOptionsTooltip %>" AlternateText="<%$ Resources:LogiAdHoc, AdvancedOptionsTooltip %>" />
                                            &nbsp;
                                            <asp:Localize ID="lOVChart" runat="server" Text="<%$ Resources:LogiAdHoc, ShowExtra %>" />
                                            <br />
                                        </span>
                                        <table>
                                            <tr id="trChartTitle" runat="server">
                                                <td id="Td49" width="150" runat="server">
                                                    <asp:Label ID="Label105" runat="server" Text="<%$ Resources:LogiAdHoc, Title %>" 
                                                        AssociatedControlID="ChartTitle"></asp:Label>:
                                                </td>
                                                <td id="Td50" runat="server">
                                                    <asp:TextBox ID="ChartTitle" runat="server"  OnTextChanged="ChartSizeAffected" Width="200px" meta:resourcekey="TitleResource2"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <%--<tr id="trChartType" runat="server">
                                                <td width="150" runat="server">
                                                    <asp:Label ID="Label96" runat="server" meta:resourcekey="LiteralResource96" 
                                                        Text="Chart Type:" AssociatedControlID="ChartType"></asp:Label></td>
                                                <td runat="server">
                                                    <asp:DropDownList ID="ChartType" runat="server" AutoPostBack="True" OnSelectedIndexChanged="Chart_Changed">
                                                        <asp:ListItem Value="Line" meta:resourcekey="ListItemResource28">Line</asp:ListItem>
                                                        <asp:ListItem Value="Spline" meta:resourcekey="ListItemResource26">Spline</asp:ListItem>
                                                        <asp:ListItem Value="Area" meta:resourcekey="ListItemResource27">Area</asp:ListItem>
                                                    </asp:DropDownList>
                                                </td>
                                            </tr>--%>
                                        </table>
                                        <asp:Panel ID="pnlLabelColumn" runat="server" GroupingText="Label Column (x-axis)" meta:resourcekey="pnlLabelColumnResource1">
                                            <asp:UpdatePanel ID="UPLabelColumn" runat="server" UpdateMode="Conditional">
                                                <ContentTemplate>
                                                    <table>
                                                        <tr>
                                                            <td width="150">
                                                                <asp:Label ID="Label65" runat="server" Text="<%$ Resources:LogiAdHoc, Column %>" AssociatedControlID="ChartLabelColumn" meta:resourcekey="Label65Resource1"></asp:Label>:</td>
                                                            <td>
                                                                <asp:DropDownList ID="ChartLabelColumn" runat="server" AutoPostBack="True" meta:resourcekey="ChartLabelColumnResource1">
                                                                </asp:DropDownList>
                                                            </td>
                                                            <td id="tdChartLabelTimePeriod" runat="server" style="padding: 0;">
                                                                <table>
                                                                    <tr>
                                                                        <td width="135px">
                                                                           <asp:Label ID="Label14" runat="server" Text="Apply Time Period:" AssociatedControlID="ddlChartLabelColTPC" meta:resourcekey="Label5Resource1"></asp:Label>
                                                                        </td>
                                                                        <td width="100">
                                                                            <asp:DropDownList ID="ddlChartLabelColTPC" runat="server" AutoPostBack="True" meta:resourcekey="ddlChartLabelColTPCResource1" >
                                                                                <asp:ListItem Text="<%$ Resources:LogiAdHoc, TimePeriodNone %>" Value="" />
                                                                                <asp:ListItem Text="<%$ Resources:LogiAdHoc, TimePeriodYear %>" Value="Year" />
                                                                                <asp:ListItem Text="<%$ Resources:LogiAdHoc, TimePeriodQuarter %>" Value="FirstDayOfQuarter" />
                                                                                <asp:ListItem Text="<%$ Resources:LogiAdHoc, TimePeriodFiscalQuarter %>" Value="FirstDayOfFiscalQuarter" />
                                                                                <asp:ListItem Text="<%$ Resources:LogiAdHoc, TimePeriodMonth %>" Value="FirstDayOfMonth" />
                                                                            </asp:DropDownList>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                        <tr id="trLabelCaption" runat="server">
                                                            <td id="Td51" runat="server">
                                                                <asp:Label ID="Label68" runat="server" Text="<%$ Resources:LogiAdHoc, Caption %>" AssociatedControlID="txtLabelColumnCaption"></asp:Label>:</td>
                                                            <td id="Td52" runat="server">
                                                                <asp:TextBox ID="txtLabelColumnCaption" runat="server" AutoPostBack="True" OnTextChanged="Chart_Changed" meta:resourcekey="txtLabelColumnCaptionResource1"></asp:TextBox>
                                                            </td>
                                                        </tr>
                                                        <tr id="trLabelColFormat" runat="server">
                                                            <td id="Td53" runat="server">
                                                                <asp:Label ID="Label123" runat="server" Text="<%$ Resources:LogiAdHoc, Format %>"
                                                                    AssociatedControlID="ddlLabelColFormat"></asp:Label>:
                                                            </td>
                                                            <td id="Td54" runat="server">
                                                                <asp:DropDownList ID="ddlLabelColFormat" runat="server" AutoPostBack="True" OnSelectedIndexChanged="Chart_Changed">
                                                                    <%--<asp:ListItem Text="<%$ Resources:LogiAdHoc, None_enclosed %>" />
                                                                    <asp:ListItem Value="Short Date" Text="<%$ Resources:LogiAdHoc, ShortDate %>" />
                                                                    <asp:ListItem Value="General Number" Text="<%$ Resources:LogiAdHoc, GeneralNumber %>" />
                                                                    <asp:ListItem Value="Currency" Text="<%$ Resources:LogiAdHoc, Currency %>" />
                                                                    <asp:ListItem Value="2-digit place holder" Text="<%$ Resources:LogiAdHoc, TwoDigit %>" />
                                                                    <asp:ListItem Value="3-digit place holder" Text="<%$ Resources:LogiAdHoc, ThreeDigit %>" />--%>
                                                                </asp:DropDownList>
                                                            </td>
                                                        </tr>
                                                        <tr id="trLabelColLinked" runat="server">
                                                            <td id="tdLabelColLinkedCaption" runat="server">
                                                                <asp:Localize ID="Localize202" runat="server" meta:resourcekey="LiteralResource190"
                                                                    Text="Linked:"></asp:Localize>
                                                            </td>
                                                            <td id="tdLabelColLinkedSettings" runat="server">
                                                                <asp:CheckBox ID="chkLabelColLinked" runat="server" AutoPostBack="True" OnCheckedChanged="Chart_Changed" />
                                                                <asp:DropDownList ID="ddlLabelColLink" runat="server" AutoPostBack="True" meta:resourcekey="ChartLabelColLinkResource1"
                                                                    OnSelectedIndexChanged="Chart_Changed">
                                                                </asp:DropDownList>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </ContentTemplate>
                                            </asp:UpdatePanel>
                                        </asp:Panel>
                                        <asp:Panel ID="pnlDataColumn" runat="server" GroupingText="Data Column (y-axis)"
                                            meta:resourcekey="pnlDataColumnResource1">
                                            <table>
                                                <tr>
                                                    <td width="150">
                                                    <asp:Label ID="Label66" runat="server" Text="<%$ Resources:LogiAdHoc, Column %>"
                                                         AssociatedControlID="ChartDataColumn" meta:resourcekey="Label66Resource1"></asp:Label>:</td>
                                                    <td>
                                                        <asp:DropDownList ID="ChartDataColumn" runat="server" AutoPostBack="True" OnSelectedIndexChanged="Chart_Changed" meta:resourcekey="ChartDataColumnResource1">
                                                        </asp:DropDownList>
                                                    </td>
                                                </tr>
                                                <tr id="trDataCaption" runat="server">
                                                    <td id="Td55" runat="server">
                                                    <asp:Label ID="Label69" runat="server" Text="<%$ Resources:LogiAdHoc, Caption %>"
                                                         AssociatedControlID="txtDataColumnCaption"></asp:Label>:</td>
                                                    <td id="Td56" runat="server">
                                                        <asp:TextBox ID="txtDataColumnCaption" runat="server" AutoPostBack="True" OnTextChanged="Chart_Changed" meta:resourcekey="txtDataColumnCaptionResource1"></asp:TextBox>
                                                    </td>
                                                </tr>
                                                <tr id="trChartDataFormat" runat="server">
                                                    <td id="Td57" runat="server">
                                                        <asp:Label ID="Label124" runat="server" Text="<%$ Resources:LogiAdHoc, Format %>" 
                                                            AssociatedControlID="ddlDataColFormat"></asp:Label>:
                                                    </td>
                                                    <td id="Td58" runat="server">
                                                        <asp:DropDownList ID="ddlDataColFormat" runat="server" AutoPostBack="True" OnSelectedIndexChanged="Chart_Changed">
                                                            <asp:ListItem Value="" Text="<%$ Resources:LogiAdHoc, None_enclosed %>" />
                                                            <asp:ListItem Value="General Number" Text="<%$ Resources:LogiAdHoc, GeneralNumber %>" />
                                                            <asp:ListItem Value="Currency" Text="<%$ Resources:LogiAdHoc, Currency %>" />
                                                            <asp:ListItem Value="2-digit place holder" Text="<%$ Resources:LogiAdHoc, TwoDigit %>" />
                                                            <asp:ListItem Value="3-digit place holder" Text="<%$ Resources:LogiAdHoc, ThreeDigit %>" />
                                                        </asp:DropDownList>
                                                    </td>
                                                </tr>
                                                <tr id="trChartAggregate" runat="server">
                                                    <td id="Td59" runat="server">
                                                    <asp:Label ID="lblChartAggregateFunction" runat="server" Text="<%$ Resources:LogiAdHoc, Aggregate %>"
                                                         AssociatedControlID="ddlAggregateFunction"></asp:Label>:</td>
                                                    <td id="Td60" runat="server">
                                                        <asp:DropDownList ID="ddlAggregateFunction" runat="server" AutoPostBack="True" OnSelectedIndexChanged="Chart_Changed">
                                                            <asp:ListItem Value="SUM" Text="<%$ Resources:LogiAdHoc, Sum %>" />
                                                            <asp:ListItem Value="AVG" Text="<%$ Resources:LogiAdHoc, Average %>" />
                                                            <asp:ListItem Value="MIN" Text="<%$ Resources:LogiAdHoc, Minimum %>"></asp:ListItem>
                                                            <asp:ListItem Value="MAX" Text="<%$ Resources:LogiAdHoc, Maximum %>"></asp:ListItem>
                                                            <asp:ListItem Value="COUNT" Text="<%$ Resources:LogiAdHoc, Count %>" />
                                                            <asp:ListItem Value="DISTINCTCOUNT" Text="<%$ Resources:LogiAdHoc, DistinctCount %>" />
                                                        </asp:DropDownList>
                                                        <asp:DropDownList ID="ValueFunction" runat="server" AutoPostBack="True" OnSelectedIndexChanged="Chart_Changed">
                                                            <asp:ListItem Value="Sum" Text="<%$ Resources:LogiAdHoc, Sum %>" />
                                                            <asp:ListItem Value="Average" Text="<%$ Resources:LogiAdHoc, Average %>" />
                                                            <asp:ListItem Value="STDEV" Text="<%$ Resources:LogiAdHoc, StandardDeviation %>" />
                                                            <asp:ListItem Value="Count" Text="<%$ Resources:LogiAdHoc, Count %>" />
                                                        </asp:DropDownList>
                                                    </td>
                                                </tr>
                                                <tr id="trPercent" runat="server">
                                                    <td id="Td61" runat="server">
                                                        <asp:Label ID="Label158" runat="server" Text="Use Percentage:" AssociatedControlID="chkUsePercentage" 
                                                            meta:resourcekey="Localize158Resource1"></asp:Label>
                                                    </td>
                                                    <td id="Td62" runat="server">
                                                        <asp:CheckBox ID="chkUsePercentage" AutoPostBack="True" runat="server" OnCheckedChanged="Chart_Changed" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </asp:Panel>
                                        <asp:Panel ID="pnlExtraChartLayers" runat="server" GroupingText="Extra Chart Layer"
                                            meta:resourcekey="pnlExtraChartLayersResource1">
                                           <%--<table id="tblShowExtraLayer" runat="server">
                                                <tr>
                                                    <td width="150px">
                                                        <asp:Label ID="Label5" runat="server" meta:resourcekey="LiteralResource190" 
                                                            Text="Show Extra Layer:" AssociatedControlID="chkShowExtraLayer"></asp:Label></td>
                                                    <td>
                                                        <asp:CheckBox ID="chkShowExtraLayer" AutoPostBack="True" runat="server"></asp:CheckBox>
                                                    </td>
                                                </tr>
                                            </table>--%>
                                            <asp:UpdatePanel ID="UPShowExtraLayer" runat="server" UpdateMode="Conditional">
                                                <ContentTemplate>
                                                    <div id="divExtraLayerSettings" runat="server">
                                                        <table id="tbExtraChartLayer" runat="server">
                                                            <tr>
                                                                <td width="150px">
                                                                    <asp:Label ID="Label3" runat="server" Text="Chart Type"
                                                                         AssociatedControlID="ddlExtraLayerChartType" meta:resourcekey="Label3Resource1"></asp:Label>:
                                                                </td>
                                                                <td>
                                                                    <asp:DropDownList ID="ddlExtraLayerChartType" AutoPostBack="True" OnSelectedIndexChanged="Chart_Changed" runat="server" 
                                                                        meta:resourcekey="ddlExtraLayerChartTypeResource1">
                                                                        <asp:ListItem Value="Bar" meta:resourcekey="ListItemResource038" Text="Bar" />
                                                                        <asp:ListItem Value="Line" meta:resourcekey="ListItemResource28" Text="Line" />
                                                                        <asp:ListItem Value="Spline" meta:resourcekey="ListItemResource026" Text="Spline" />
                                                                        <asp:ListItem Value="Area" meta:resourcekey="ListItemResource027" Text="Area" />
                                                                    </asp:DropDownList>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td  width="150px">
                                                                    <asp:Label ID="Label2" runat="server" Text="<%$ Resources:LogiAdHoc, Column %>"
                                                                         AssociatedControlID="ddlExtraLayerColumn" meta:resourcekey="Label2Resource1"></asp:Label>:
                                                                </td>
                                                                <td>
                                                                    <asp:DropDownList ID="ddlExtraLayerColumn" AutoPostBack="True" runat="server" meta:resourcekey="ddlExtraLayerColumnResource1" />
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <asp:Label ID="Label8" runat="server" Text="<%$ Resources:LogiAdHoc, Aggregate %>"
                                                                        AssociatedControlID="ddlExtraLayerAggregateFunction"></asp:Label>:</td>
                                                                <td>
                                                                    <asp:DropDownList ID="ddlExtraLayerAggregateFunction" runat="server" AutoPostBack="True" OnSelectedIndexChanged="Chart_Changed">
                                                                        <asp:ListItem Value="SUM" Text="<%$ Resources:LogiAdHoc, Sum %>" />
                                                                        <asp:ListItem Value="AVG" Text="<%$ Resources:LogiAdHoc, Average %>" />
                                                                        <asp:ListItem Value="MIN" Text="<%$ Resources:LogiAdHoc, Minimum %>"></asp:ListItem>
                                                                        <asp:ListItem Value="MAX" Text="<%$ Resources:LogiAdHoc, Maximum %>"></asp:ListItem>
                                                                        <asp:ListItem Value="COUNT" Text="<%$ Resources:LogiAdHoc, Count %>" />
                                                                        <asp:ListItem Value="DISTINCTCOUNT" Text="<%$ Resources:LogiAdHoc, DistinctCount %>" />
                                                                    </asp:DropDownList>
                                                                </td>
                                                            </tr>
                                                            <tr id="trExtraLayerColor" runat="server">
                                                                <td width="150px">
                                                                    <asp:Label ID="Label4" runat="server" meta:resourcekey="LiteralResource106" 
                                                                        Text="Color:" AssociatedControlID="ddlColor"></asp:Label></td>
                                                                <td>
                                                                    <asp:DropDownList ID="ddlExtraLayerColor" runat="server" AutoPostBack="True" OnSelectedIndexChanged="Chart_Changed" />
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td width="150px">
                                                                    <asp:Label ID="Label9" runat="server" meta:resourcekey="LiteralResource191" 
                                                                        Text="Show Secondary Y-Axis:" AssociatedControlID="chkShowSecondaryAxis"></asp:Label></td>
                                                                <td>
                                                                    <asp:CheckBox ID="chkShowSecondaryAxis" runat="server" AutoPostBack="True" OnCheckedChanged="Chart_Changed"></asp:CheckBox>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </div>
                                                </ContentTemplate>
                                            </asp:UpdatePanel>
                                        </asp:Panel>
                                        <asp:Panel ID="pnlUseCrosstab" runat="server" GroupingText="Crosstab Column" meta:resourcekey="pnlUseCrosstabResource1">
                                            <%--<table id="tblUseCrosstab" runat="server">
                                                <tr id="trUseCrosstab" runat="server">
                                                    <td width="150" >
                                                        <asp:Label ID="Label97" runat="server" meta:resourcekey="LiteralResource97" 
                                                            Text="Use Crosstab Filter:" AssociatedControlID="chkUseCrosstab"></asp:Label></td>
                                                    <td>
                                                        <asp:CheckBox ID="chkUseCrosstab" AutoPostBack="True" runat="server"></asp:CheckBox>
                                                    </td>
                                                </tr>
                                            </table>
                                            <asp:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Conditional">
                                                <ContentTemplate>--%>
                                                    <table id="tblCrosstab" runat="server">
                                                        <tr id="Tr5" runat="server">
                                                            <td id="Td63" width="150" runat="server">
                                                            <asp:Label ID="Label63" runat="server" Text="<%$ Resources:LogiAdHoc, Column %>"
                                                                 AssociatedControlID="ChartCrosstabColumn"></asp:Label>:</td>
                                                            <td id="Td64" runat="server">
                                                                <asp:DropDownList ID="ChartCrosstabColumn" runat="server" AutoPostBack="True" >
                                                                </asp:DropDownList>
                                                            </td>
                                                            <td id="tdChartCTTimePeriod" runat="server" style="padding: 0;">
                                                                <table>
                                                                    <tr>
                                                                        <td width="135px">
                                                                           <asp:Label ID="Label10" runat="server" Text="Apply Time Period:" AssociatedControlID="ddlChartCTColTPC" meta:resourcekey="Label10Resource1"></asp:Label>
                                                                        </td>
                                                                        <td width="100">
                                                                            <asp:DropDownList ID="ddlChartCTColTPC" runat="server" AutoPostBack="True" meta:resourcekey="ddlChartCTColTPCResource1" >
                                                                                <asp:ListItem Text="<%$ Resources:LogiAdHoc, TimePeriodNone %>" Value="" />
                                                                                <asp:ListItem Text="<%$ Resources:LogiAdHoc, TimePeriodYear %>" Value="Year" />
                                                                                <asp:ListItem Text="<%$ Resources:LogiAdHoc, TimePeriodQuarter %>" Value="FirstDayOfQuarter" />
                                                                                <asp:ListItem Text="<%$ Resources:LogiAdHoc, TimePeriodFiscalQuarter %>" Value="FirstDayOfFiscalQuarter" />
                                                                                <asp:ListItem Text="<%$ Resources:LogiAdHoc, TimePeriodMonth %>" Value="FirstDayOfMonth" />
                                                                            </asp:DropDownList>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                    <%--<table id="tblChartDVT" runat="server">
                                                        <tr runat="server">
                                                            <td width="150" runat="server">
                                                                <asp:Label ID="Label98" runat="server" meta:resourcekey="LiteralResource98" 
                                                                    Text="Data View Type:" AssociatedControlID="ExtraBarOptions"></asp:Label></td>
                                                            <td runat="server">
                                                                <asp:DropDownList ID="ExtraBarOptions" runat="server" AutoPostBack="True" OnSelectedIndexChanged="Chart_Changed">
                                                                    <asp:ListItem Value="SideBySide" Text="Side By Side" meta:resourcekey="ListItemResource22" />
                                                                    <asp:ListItem Value="Stacked" Text="Stacked" meta:resourcekey="ListItemResource25" />
                                                                </asp:DropDownList>
                                                            </td>
                                                        </tr>
                                                    </table>--%>
                                                    <table id="tblIDVDVT" runat="server">
                                                        <tr id="Tr6" runat="server">
                                                            <td id="Td65" width="150" runat="server">
                                                                <asp:Label ID="Label99" runat="server" meta:resourcekey="LiteralResource98" 
                                                                    Text="Data View Type:" AssociatedControlID="IDVType"></asp:Label></td>
                                                            <td id="Td66" runat="server">
                                                                <asp:DropDownList ID="IDVType" runat="server" AutoPostBack="True" OnSelectedIndexChanged="Chart_Changed">
                                                                    <asp:ListItem Value="Line" meta:resourcekey="ListItemResource28">Line</asp:ListItem>
                                                                    <asp:ListItem Value="ClusteredBar" meta:resourcekey="ListItemResource29">Clustered Bar</asp:ListItem>
                                                                    <asp:ListItem Value="StackedBar" meta:resourcekey="ListItemResource30">Stacked Bar</asp:ListItem>
                                                                    <asp:ListItem Value="StackedArea" meta:resourcekey="ListItemResource31">Stacked Area</asp:ListItem>
                                                                    <asp:ListItem Value="Pie" meta:resourcekey="ListItemResource32">Pie</asp:ListItem>
                                                                </asp:DropDownList>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                <%--</ContentTemplate>
                                                <Triggers>
                                                    <asp:AsyncPostBackTrigger ControlID="chkUseCrosstab" />
                                                </Triggers>
                                            </asp:UpdatePanel>--%>
                                        </asp:Panel>
                                        <asp:Panel ID="pnlUseLegend" runat="server" GroupingText="Legend" meta:resourcekey="pnlUseLegendResource1">
                                            <table>
                                                <tr>
                                                    <td width="150">
                                                        <asp:Label ID="Label100" runat="server" meta:resourcekey="LiteralResource100" 
                                                            Text="Include Legend:" AssociatedControlID="chkUseLegend"></asp:Label></td>
                                                    <td>
                                                        <asp:CheckBox ID="chkUseLegend" AutoPostBack="True" runat="server" meta:resourcekey="chkUseLegendResource1">
                                                        </asp:CheckBox>
                                                    </td>
                                                </tr>
                                            </table>
                                            <asp:UpdatePanel ID="UpdatePanel5" runat="server" UpdateMode="Conditional">
                                                <ContentTemplate>
                                                    <table id="tblLegend" runat="server">
                                                        <tr id="Tr7" runat="server">
                                                            <td id="Td67" width="150" runat="server">
                                                                <asp:Label ID="Label109" runat="server" Text="<%$ Resources:LogiAdHoc, Position %>"
                                                                     AssociatedControlID="ddlLegendPosition"></asp:Label>:</td>
                                                            <td id="Td68" runat="server">
                                                                <asp:DropDownList ID="ddlLegendPosition" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ChartSizeAffected">
                                                                    <asp:ListItem Value="Side" Selected="True" meta:resourcekey="ListItemResource033" Text="Side" />
                                                                    <asp:ListItem Value="Bottom" meta:resourcekey="ListItemResource039" Text="Bottom" />
                                                                </asp:DropDownList>
                                                            </td>
                                                        </tr>
                                                        <tr id="trLegendLabel" runat="server">
                                                            <td id="Td69" width="150" runat="server">
                                                                <asp:Label ID="Label101" runat="server" meta:resourcekey="LiteralResource101" 
                                                                    Text="Legend Label:" AssociatedControlID="txtLegendLabel"></asp:Label></td>
                                                            <td id="Td70" runat="server">
                                                                <asp:TextBox ID="txtLegendLabel" runat="server" AutoPostBack="True" OnTextChanged="Chart_Changed"></asp:TextBox>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </ContentTemplate>
                                                <Triggers>
                                                    <asp:AsyncPostBackTrigger ControlID="chkUseLegend" />
                                                </Triggers>
                                            </asp:UpdatePanel>
                                        </asp:Panel>
                                        <asp:Panel ID="pnlUseTrend" runat="server" GroupingText="Trend Line" meta:resourcekey="pnlUseTrendResource1">
                                            <table>
                                                <tr>
                                                    <td width="150">
                                                        <asp:Label ID="Label118" runat="server" meta:resourcekey="LiteralResource118" 
                                                            Text="Include Trend Line:" AssociatedControlID="chkUseTrend"></asp:Label></td>
                                                    <td>
                                                        <asp:CheckBox ID="chkUseTrend" AutoPostBack="True" runat="server" meta:resourcekey="chkUseTrendResource1" />
                                                    </td>
                                                </tr>                                                
                                            </table>
                                            <asp:UpdatePanel ID="UpTrendLine" runat="server" UpdateMode="Conditional">
                                                <ContentTemplate>
                                                    <table id="tblTrend" runat="server">
                                                        <tr id="Tr8" runat="server">
                                                            <td id="Td71" width="150" runat="server">
                                                                <asp:Label ID="Label154" runat="server" meta:resourcekey="LiteralResource106" 
                                                                    Text="Color:" AssociatedControlID="ddlTrendColor"></asp:Label>
                                                            </td>
                                                            <td id="Td72" runat="server">
                                                                <asp:DropDownList ID="ddlTrendColor" runat="server" AutoPostBack="True" OnSelectedIndexChanged="Chart_Changed" />
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </ContentTemplate>
                                                <Triggers>
                                                    <asp:AsyncPostBackTrigger ControlID="chkUseTrend" />
                                                </Triggers>
                                            </asp:UpdatePanel>
                                        </asp:Panel>
                                        <asp:Panel ID="pnlChartStyle" runat="server" GroupingText="Style" meta:resourcekey="pnlChartStyleResource1">
                                            <table>
                                                <%--<tr id="trAnimated" runat="server">
                                                    <td width="150" runat="server">
                                                        <asp:Label ID="Label12" runat="server" Text="<%$ Resources:LogiAdHoc, Animated %>"
                                                             AssociatedControlID="chkAnimated"></asp:Label>:</td>
                                                    <td runat="server">
                                                        <asp:CheckBox ID="chkAnimated" runat="server" AutoPostBack="True" />
                                                    </td>
                                                    <td colspan=2 runat="server"></td>
                                                </tr>--%>
                                                <tr>
                                                    <td width="150">
                                                        <asp:Label ID="Label110" runat="server" Text="<%$ Resources:LogiAdHoc, Size %>"
                                                             AssociatedControlID="Size" meta:resourcekey="Label110Resource1"></asp:Label>:</td>
                                                    <td>
                                                    <asp:UpdatePanel ID="UPChartSize" runat="server" UpdateMode="Conditional">
                                                    <ContentTemplate>
                                                        <asp:DropDownList ID="Size" runat="server" AutoPostBack="True" meta:resourcekey="SizeResource2">
                                                            <asp:ListItem Value="Small" meta:resourcekey="ListItemResource17">Small</asp:ListItem>
                                                            <asp:ListItem Value="Medium" meta:resourcekey="ListItemResource18">Medium</asp:ListItem>
                                                            <asp:ListItem Value="Large" meta:resourcekey="ListItemResource19">Large</asp:ListItem>
                                                        </asp:DropDownList>
                                                    </ContentTemplate>
                                                    </asp:UpdatePanel>
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="lbl3D" runat="server" meta:resourcekey="LiteralResource111" 
                                                            Text="3D:" AssociatedControlID="Is3D"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:CheckBox ID="Is3D" runat="server" AutoPostBack="True" OnCheckedChanged="Chart_Changed" meta:resourcekey="Is3DResource1"></asp:CheckBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <asp:Label ID="Label16" runat="server" Text="<%$ Resources:LogiAdHoc, Width %>"
                                                             AssociatedControlID="txtChartWidth" meta:resourcekey="Label16Resource1"></asp:Label>:
                                                    </td>
                                                    <td>
                                                        <asp:TextBox ID="txtChartWidth" runat="server" AutoPostBack="True" OnTextChanged="ChartSizeChanged" meta:resourcekey="txtChartWidthResource1"></asp:TextBox>
                                                        <asp:RequiredFieldValidator ValidationGroup="ChartGroup" ID="reqChartWidth" runat="server" ControlToValidate="txtChartWidth"
                                                            ErrorMessage="Chart Width is required." meta:resourcekey="reqChartWidthResource">*</asp:RequiredFieldValidator>
                                                        <asp:RangeValidator ValidationGroup="ChartGroup" ID="rvChartWidth" runat="server" ControlToValidate="txtChartWidth"
                                                            Type="Integer" MinimumValue="10" MaximumValue="3000" ErrorMessage="Chart Width accepts only integer values between 10 and 3000." meta:resourcekey="rvChartWidthResource">*</asp:RangeValidator>
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="Label27" runat="server" Text="<%$ Resources:LogiAdHoc, Height %>"
                                                             AssociatedControlID="txtChartHeight" meta:resourcekey="Label27Resource1"></asp:Label>:
                                                    </td>
                                                    <td>
                                                        <asp:TextBox ID="txtChartHeight" runat="server" AutoPostBack="True" OnTextChanged="ChartSizeChanged" meta:resourcekey="txtChartHeightResource1"></asp:TextBox>
                                                        <asp:RequiredFieldValidator ValidationGroup="ChartGroup" ID="reqChartHeight" runat="server" ControlToValidate="txtChartHeight"
                                                            ErrorMessage="Chart Height is required." meta:resourcekey="reqChartHeightResource">*</asp:RequiredFieldValidator>
                                                        <asp:RangeValidator ValidationGroup="ChartGroup" ID="rvChartHeight" runat="server" ControlToValidate="txtChartHeight"
                                                            Type="Integer" MinimumValue="10" MaximumValue="3000" ErrorMessage="Chart Height accepts only integer values between 10 and 3000." meta:resourcekey="rvChartHeightResource">*</asp:RangeValidator>
                                                    </td>
                                                </tr>
                                                <tr id="trStyleBorderFixed" runat="server">
                                                    <td id="Td73" runat="server">
                                                        <asp:Label ID="Label116" runat="server" meta:resourcekey="LiteralResource116" 
                                                            Text="Left Border:" AssociatedControlID="txtLeftBorder"></asp:Label>
                                                    </td>
                                                    <td id="Td74" runat="server">
                                                        <asp:TextBox ID="txtLeftBorder" runat="server" AutoPostBack="True" OnTextChanged="ChartSizeChanged"></asp:TextBox>
                                                        <asp:RequiredFieldValidator ValidationGroup="ChartGroup" ID="reqLeftBorder" runat="server" ControlToValidate="txtLeftBorder"
                                                            ErrorMessage="Left Border is required." meta:resourcekey="reqLeftBorderResource">*</asp:RequiredFieldValidator>
                                                        <asp:RangeValidator ValidationGroup="ChartGroup" ID="rvLeftBorder" runat="server" ControlToValidate="txtLeftBorder"
                                                            Type="Integer" MinimumValue="10" MaximumValue="3000" ErrorMessage="Left Border accepts only integer values between 10 and 3000." meta:resourcekey="rvLeftBorderResource">*</asp:RangeValidator>
                                                    </td>
                                                    <td id="Td75" runat="server">
                                                        <asp:Label ID="Label119" runat="server" meta:resourcekey="LiteralResource119" 
                                                            Text="Top Border:" AssociatedControlID="txtTopBorder"></asp:Label>
                                                    </td>
                                                    <td id="Td76" runat="server">
                                                        <asp:TextBox ID="txtTopBorder" runat="server" AutoPostBack="True" OnTextChanged="ChartSizeChanged"></asp:TextBox>
                                                        <asp:RequiredFieldValidator ValidationGroup="ChartGroup" ID="reqTopBorder" runat="server" ControlToValidate="txtTopBorder"
                                                            ErrorMessage="Top Border is required." meta:resourcekey="reqTopBorderResource">*</asp:RequiredFieldValidator>
                                                        <asp:RangeValidator ValidationGroup="ChartGroup" ID="rvTopBorder" runat="server" ControlToValidate="txtTopBorder"
                                                            Type="Integer" MinimumValue="10" MaximumValue="3000" ErrorMessage="Top Border accepts only integer values between 10 and 3000." meta:resourcekey="rvTopBorderResource">*</asp:RangeValidator>
                                                    </td>
                                                </tr>
                                                <tr id="trStyleBorder" runat="server">
                                                    <td id="Td77" runat="server">
                                                        <asp:Label ID="Label117" runat="server" meta:resourcekey="LiteralResource117" 
                                                            Text="Right Border:" AssociatedControlID="txtRightBorder"></asp:Label>
                                                    </td>
                                                    <td id="Td78" runat="server">
                                                        <asp:TextBox ID="txtRightBorder" runat="server" AutoPostBack="True" OnTextChanged="ChartSizeChanged"></asp:TextBox>
                                                        <asp:RequiredFieldValidator ValidationGroup="ChartGroup" ID="reqRightBorder" runat="server" ControlToValidate="txtRightBorder"
                                                            ErrorMessage="Right Border is required." meta:resourcekey="reqRightBorderResource">*</asp:RequiredFieldValidator>
                                                        <asp:RangeValidator ValidationGroup="ChartGroup" ID="rvRightBorder" runat="server" ControlToValidate="txtRightBorder"
                                                            Type="Integer" MinimumValue="10" MaximumValue="3000" ErrorMessage="Right Border accepts only integer values between 10 and 3000." meta:resourcekey="rvRightBorderResource">*</asp:RangeValidator>
                                                    </td>
                                                    <td id="Td79" runat="server">
                                                        <asp:Label ID="Label120" runat="server" meta:resourcekey="LiteralResource120" 
                                                            Text="Bottom Border:" AssociatedControlID="txtBottomBorder"></asp:Label>
                                                    </td>
                                                    <td id="Td80" runat="server">
                                                        <asp:TextBox ID="txtBottomBorder" runat="server" AutoPostBack="True" OnTextChanged="ChartSizeChanged"></asp:TextBox>
                                                        <asp:RequiredFieldValidator ValidationGroup="ChartGroup" ID="reqBottomBorder" runat="server" ControlToValidate="txtBottomBorder"
                                                            ErrorMessage="Bottom Border is required." meta:resourcekey="reqBottomBorderResource">*</asp:RequiredFieldValidator>
                                                        <asp:RangeValidator ValidationGroup="ChartGroup" ID="rvBottomBorder" runat="server" ControlToValidate="txtBottomBorder"
                                                            Type="Integer" MinimumValue="10" MaximumValue="3000" ErrorMessage="Bottom Border accepts only integer values between 10 and 3000." meta:resourcekey="rvBottomBorderResource">*</asp:RangeValidator>
                                                    </td>
                                                </tr>
                                                <tr id="trPieRadius" runat="server">
                                                    <td id="Td81" runat="server">
                                                        <asp:Label ID="Label121" runat="server" meta:resourcekey="LiteralResource121" 
                                                            Text="Pie Radius:" AssociatedControlID="txtPieRadius"></asp:Label>
                                                    </td>
                                                    <td id="Td82" runat="server">
                                                        <asp:TextBox ID="txtPieRadius" runat="server" AutoPostBack="True" OnTextChanged="ChartSizeChanged"></asp:TextBox>
                                                        <asp:RequiredFieldValidator ValidationGroup="ChartGroup" ID="reqPieRadius" runat="server" ControlToValidate="txtPieRadius"
                                                            ErrorMessage="Pie Radius is required." meta:resourcekey="reqPieRadiusResource">*</asp:RequiredFieldValidator>
                                                        <asp:RangeValidator ValidationGroup="ChartGroup" ID="rvPieRadius" runat="server" ControlToValidate="txtPieRadius"
                                                            Type="Integer" MinimumValue="10" MaximumValue="3000" ErrorMessage="Pie Radius accepts only integer values between 10 and 3000." meta:resourcekey="rvPieRadiusResource">*</asp:RangeValidator>
                                                    </td>
                                                    <td id="Td83" runat="server">
                                                        <asp:Label ID="Label122" runat="server" meta:resourcekey="LiteralResource122" 
                                                            Text="Label Layout:" AssociatedControlID="ddlLabelLayout"></asp:Label>
                                                    </td>
                                                    <td id="Td84" runat="server">
                                                        <asp:DropDownList ID="ddlLabelLayout" runat="server" AutoPostBack="True" OnSelectedIndexChanged="Chart_Changed">
                                                            <asp:ListItem Value="ExternalLayout" meta:resourcekey="LLExternal">External</asp:ListItem>
                                                            <asp:ListItem Value="InternalLayout" meta:resourcekey="LLInternal">Internal</asp:ListItem>
                                                            <asp:ListItem Value="SideLayout" meta:resourcekey="LLSide">Side</asp:ListItem>
                                                            <asp:ListItem Value="NoLabels" meta:resourcekey="LLNoLabels">No Labels</asp:ListItem>
                                                        </asp:DropDownList>
                                                    </td>
                                                </tr>                                                    
                                                <tr id="trColor" runat="server">
                                                    <td id="Td85" width="150" runat="server">
                                                        <asp:Label ID="Label106" runat="server" meta:resourcekey="LiteralResource106" 
                                                            Text="Color:" AssociatedControlID="ddlColor"></asp:Label></td>
                                                    <td id="Td86" runat="server">
                                                        <asp:DropDownList ID="ddlColor" runat="server" AutoPostBack="True" OnSelectedIndexChanged="Chart_Changed" />
                                                    </td>
                                                    <td id="Td87" colspan=2 runat="server"></td>
                                                </tr>
                                            </table>
                                            <table>
                                                <tr id="trColorSequence" runat="server">
                                                    <td id="Td88" width="150" runat="server">
                                                    <asp:Localize ID="Localize107" runat="server" meta:resourcekey="LiteralResource107" Text="Color Sequence:"></asp:Localize>
                                                    </td>
                                                    <td id="Td89" runat="server">
                                                        <asp:UpdatePanel ID="UPColorSequence" runat="server" UpdateMode="Conditional">
                                                            <ContentTemplate>
                                                                <AdHoc:ColorSequence ID="csPieChartImage" runat="Server"  />
                                                            </ContentTemplate>
                                                        </asp:UpdatePanel>
                                                    </td>
                                                </tr>
                                            </table>
                                        </asp:Panel>
                                        <asp:Panel ID="pnlAdvancedXScale" runat="server" GroupingText="Label (x-axis) Scaling" meta:resourcekey="pnlXScaleResource1">
                                            <table>
                                            <tr>
                                                <td>
                                                    <asp:Label ID="Label125" runat="server" meta:resourcekey="LiteralResource125" 
                                                        Text="Lower Bound:" AssociatedControlID="txtLowerBoundX"></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtLowerBoundX" runat="server" AutoPostBack="True" OnTextChanged="Chart_Changed" meta:resourcekey="txtLowerBoundXResource1"></asp:TextBox>
                                                    <asp:CustomValidator ValidationGroup="ChartGroup" ID="cvLowerBoundX" runat="server" ControlToValidate="txtLowerBoundX" ErrorMessage="Lower Bound accepts only numbers."
                                                        OnServerValidate="IsValueNumeric" meta:resourcekey="cvLowerBoundResource">*</asp:CustomValidator>
                                                </td>
                                                <td>
                                                    <asp:Label ID="Label126" runat="server" meta:resourcekey="LiteralResource126" 
                                                        Text="Upper Bound:" AssociatedControlID="txtUpperBoundX"></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtUpperBoundX" runat="server" AutoPostBack="True" OnTextChanged="Chart_Changed" meta:resourcekey="txtUpperBoundXResource1"></asp:TextBox>
                                                    <asp:CustomValidator ValidationGroup="ChartGroup" ID="cvUpperBoundX" runat="server" ControlToValidate="txtUpperBoundX" ErrorMessage="Upper Bound accepts only numbers."
                                                        OnServerValidate="IsValueNumeric" meta:resourcekey="cvUpperBoundResource">*</asp:CustomValidator>
                                                </td>
                                            </tr>
                                            <tr id="trTimeScale" runat="server">
                                                <td id="Td90" runat="server">
                                                    <asp:Label ID="Label132" runat="server" meta:resourcekey="LiteralResource132" 
                                                        Text="Linear Time:" AssociatedControlID="chkLinearTime"></asp:Label>
                                                </td>
                                                <td id="Td91" runat="server">
                                                    <asp:CheckBox ID="chkLinearTime" runat="server" AutoPostBack="True" OnCheckedChanged="Chart_Changed" />
                                                </td>
                                                <td id="Td92" runat="server"></td>
                                                <td id="Td93" runat="server"></td>
                                            </tr>
                                            <tr id="trNumScale" runat="server">
                                                <td id="Td94" runat="server">
                                                    <asp:Label ID="Label134" runat="server" meta:resourcekey="LiteralResource134" 
                                                        Text="Linear Numeric:" AssociatedControlID="chkLinearNumeric"></asp:Label>
                                                </td>
                                                <td id="Td95" runat="server">
                                                    <asp:CheckBox ID="chkLinearNumeric" runat="server" AutoPostBack="True" OnCheckedChanged="Chart_Changed" />
                                                </td>
                                                <td id="Td96" runat="server"></td>
                                                <td id="Td97" runat="server"></td>
                                            </tr>
                                            </table>
                                        </asp:Panel>
                                        <asp:Panel ID="pnlAdvancedYScale" runat="server" GroupingText="Data (y-axis) Scaling" meta:resourcekey="pnlYScaleResource1">
                                            <table>
                                            <tr>
                                                <td>
                                                    <asp:Label ID="Label127" runat="server" meta:resourcekey="LiteralResource125" 
                                                        Text="Lower Bound:" AssociatedControlID="txtLowerBoundY"></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtLowerBoundY" runat="server" AutoPostBack="True" OnTextChanged="Chart_Changed" meta:resourcekey="txtLowerBoundYResource1"></asp:TextBox>
                                                    <asp:CustomValidator ValidationGroup="ChartGroup" ID="cvLowerBoundY" runat="server" ControlToValidate="txtLowerBoundY" ErrorMessage="Lower Bound accepts only numbers."
                                                        OnServerValidate="IsValueNumeric" meta:resourcekey="cvLowerBoundResource">*</asp:CustomValidator>
                                                </td>
                                                <td>
                                                    <asp:Label ID="Label128" runat="server" meta:resourcekey="LiteralResource126" 
                                                        Text="Upper Bound:" AssociatedControlID="txtUpperBoundY"></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtUpperBoundY" runat="server" AutoPostBack="True" OnTextChanged="Chart_Changed" meta:resourcekey="txtUpperBoundYResource1"></asp:TextBox>
                                                    <asp:CustomValidator ValidationGroup="ChartGroup" ID="cvUpperBoundY" runat="server" ControlToValidate="txtUpperBoundY" ErrorMessage="Upper Bound accepts only numbers."
                                                        OnServerValidate="IsValueNumeric" meta:resourcekey="cvUpperBoundResource">*</asp:CustomValidator>
                                                </td>
                                            </tr>
                                            </table>
                                        </asp:Panel>
                                        <!-- Type-specific fields -->
                                        <%--<asp:UpdatePanel ID="UpdatePanel3" runat="server" UpdateMode="Conditional">
                                            <ContentTemplate>--%>
                                                <asp:Panel ID="pnlRelevance" runat="server" GroupingText="Relevance" meta:resourcekey="pnlRelevanceResource1">
                                                    <table>
                                                        <tr>
                                                            <td width="150">
                                                                <asp:Label ID="Label102" runat="server" meta:resourcekey="LiteralResource102" 
                                                                    Text="Use Relevance Values:" AssociatedControlID="UseRelevance"></asp:Label></td>
                                                            <td>
                                                                <asp:CheckBox ID="UseRelevance" runat="server" AutoPostBack="True" meta:resourcekey="UseRelevanceResource1">
                                                                </asp:CheckBox>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td width="150">
                                                            </td>
                                                            <td>
                                                                <asp:UpdatePanel ID="UPRelevance" runat="server" UpdateMode="Conditional">
                                                                    <ContentTemplate>
                                                                        <asp:Label ID="lblRelevanceValue" runat="server" CssClass="NoShow" Text="Relevance Value" AssociatedControlID="RelevanceValue" meta:resourcekey="lblRelevanceValueResource1"></asp:Label>
                                                                        <asp:TextBox ID="RelevanceValue" runat="server" AutoPostBack="True" OnTextChanged="Chart_Changed" CssClass="smallText" meta:resourcekey="RelevanceValueResource1"></asp:TextBox>
                                                                        <asp:RangeValidator ID="rvRelevanceValue" runat="server" ControlToValidate="RelevanceValue"
                                                                            ValidationGroup="ChartGroup" Type="Integer" MinimumValue="1" MaximumValue="99"
                                                                            ErrorMessage="Relevance Value accepts only integer values between 1 and 99."
                                                                            meta:resourcekey="rvRelevanceValueResource1">*</asp:RangeValidator>
                                                                        <asp:RadioButton ID="rbTypeRows" runat="server" GroupName="RelevanceType" Text="Top N rows" meta:resourcekey="LiteralResource103" AutoPostBack="True" OnCheckedChanged="Chart_Changed" />
                                                                        <asp:RadioButton ID="rbTypePercent" runat="server" GroupName="RelevanceType" Text="Percentage" meta:resourcekey="LiteralResource108" AutoPostBack="True" OnCheckedChanged="Chart_Changed" />
                                                                    </ContentTemplate>
                                                                    <Triggers>
                                                                        <asp:AsyncPostBackTrigger ControlID="UseRelevance" />
                                                                    </Triggers>
                                                                </asp:UpdatePanel>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </asp:Panel>
                                                <br />
                                             <%--</ContentTemplate>
                                            <Triggers>
                                                <asp:AsyncPostBackTrigger ControlID="chkUseCrosstab" />
                                            </Triggers>
                                        </asp:UpdatePanel>--%>
                                        <table id="tbDataValue" runat="server">
                                            <tr id="Tr9" runat="server">
                                                <td id="Td98" width="150" runat="server">
                                                    <asp:Label ID="Label42" runat="server" meta:resourcekey="LiteralResource41" 
                                                        Text="Show Data Values:" AssociatedControlID="chkShowChartValues"></asp:Label></td>
                                                <td id="Td99" runat="server">
                                                    <asp:CheckBox ID="chkShowChartValues" runat="server" AutoPostBack="True" OnCheckedChanged="Chart_Changed" />
                                                </td>
                                                <td id="Td100" width="100px" runat="server" />
                                                <td id="Td101" width="150" runat="server">
                                                    <asp:Label ID="Label162" runat="server" meta:resourcekey="LiteralResource162" 
                                                        Text="Allow Resizing:" AssociatedControlID="chkAllowResizing"></asp:Label></td>
                                                <td id="Td102" runat="server">
                                                    <asp:CheckBox ID="chkAllowResizing" runat="server" AutoPostBack="True" OnCheckedChanged="Chart_Changed" />
                                                </td>
                                            </tr>
                                        </table>
                                        </td>
                                        </tr>
                                        </table>

                                        <asp:ValidationSummary ID="ValidationSummary4" runat="server" ValidationGroup="ChartGroup"
                                            meta:resourcekey="ValidationSummary4Resource1" />
                                        <asp:Label ID="lblSemicolon4" runat="server" OnPreRender="lblSemicolon_PreRender" meta:resourcekey="lblSemicolon4Resource1" />
                                    </asp:Panel>
                                    <asp:Panel ID="pnlLabel" runat="server" Height="350px" Width="100%" meta:resourcekey="pnlLabelResource1">
                                        <asp:ImageButton ID="ImageButton12" runat="server" OnClientClick="goHelp('21'); return false;" style="float:right;" AlternateText="Help" 
                                            CausesValidation="False" SkinID="imgbPnlHelp" ImageUrl="../ahImages/iconHelpText.gif" ToolTip="Get help creating labels." meta:resourcekey="ImageButton12Resource1" />
                                        <table>
                                            <tr>
                                                <td width="125px">
                                                    <asp:Label ID="Label112" runat="server" Text="<%$ Resources:LogiAdHoc, Label %>"
                                                         AssociatedControlID="txtaLabel" meta:resourcekey="Label112Resource1"></asp:Label>:</td>
                                                <td>
                                                    <asp:TextBox ID="txtaLabel" runat="server" AutoPostBack="True" OnTextChanged="Label_Changed" Columns="40" Rows="3" TextMode="MultiLine" meta:resourcekey="txtaLabelResource1" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="125px">
                                                <asp:Label ID="Label113" runat="server" meta:resourcekey="LiteralResource113" 
                                                    Text="Label Type:" AssociatedControlID="LabelType"></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:DropDownList ID="LabelType" runat="server" AutoPostBack="True" OnSelectedIndexChanged="Label_Changed" meta:resourcekey="LabelTypeResource1">
                                                        <asp:ListItem Value="Simple" Text="Simple" meta:resourcekey="ListItemResource23"></asp:ListItem>
                                                        <asp:ListItem Value="FullWidth" Text="Full Width" meta:resourcekey="ListItemResource24"></asp:ListItem>
                                                    </asp:DropDownList>
                                                </td>
                                            </tr>
                                            <tr id="trLabelAppearance" runat="server">
                                                <td id="Td103" width="125px" runat="server">
                                                <asp:Label ID="Label114" runat="server" meta:resourcekey="LiteralResource183" 
                                                    Text="Style:" AssociatedControlID="LabelClass"></asp:Label>
                                                </td>
                                                <td id="Td104" runat="server">
                                                    <asp:DropDownList ID="LabelClass" runat="server" AutoPostBack="True" OnSelectedIndexChanged="Label_Changed" meta:resourcekey="LabelClassResource1" />
                                                </td>
                                            </tr>
                                        </table>
                                    </asp:Panel>
                                    <asp:Panel ID="pnlImage" runat="server" Height="350px" Width="100%" meta:resourcekey="pnlImageResource1">
                                        <asp:ImageButton ID="ImageButton13" runat="server" OnClientClick="goHelp('22'); return false;" style="float:right;" AlternateText="Help"
                                            CausesValidation="False" SkinID="imgbPnlHelp" ImageUrl="../ahImages/iconHelpText.gif" ToolTip="Get help creating an image." meta:resourcekey="ImageButton13Resource1" />
                                        <asp:UpdatePanel ID="UpdatePanel9" runat="server" UpdateMode="Conditional">
                                            <ContentTemplate>
                                                <table>
                                                    <tr>
                                                        <td width="125px">
                                                            <asp:Localize ID="Localize152" runat="server" meta:resourcekey="LiteralResource147" Text="Source:"></asp:Localize>
                                                        </td>
                                                        <td>
                                                            <asp:RadioButton ID="rbtnFileImage" runat="server" GroupName="ImageCondition"
                                                                Text="File" meta:resourcekey="rbtnFileImageResource1" AutoPostBack="True" OnCheckedChanged="ImageMethod_Changed"/>
                                                            <asp:RadioButton ID="rbtnURLImage" runat="server" GroupName="ImageCondition"
                                                                Text="URL" meta:resourcekey="rbtnURLImageResource1" AutoPostBack="True" OnCheckedChanged="ImageMethod_Changed" />                                                    
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td width="125px">
                                                            <asp:Localize ID="Localize115" runat="server" meta:resourcekey="LiteralResource115" Text="Image:"></asp:Localize>
                                                        </td>
                                                        <td>
                                                            <table>
                                                                <tr>
                                                                    <td>
                                                                        
                                                                        
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td id="tdFile" runat="server">
                                                                        <asp:UpdatePanel ID="UPPickImageFromFile" runat="server" UpdateMode="Conditional">
                                                                            <ContentTemplate>
                                                                                <input type="hidden" id="imgFileURL" runat="server" />
                                                                                <asp:Image ID="imgThumbnail" runat="server" meta:resourcekey="imgThumbnailResource1" />
                                                                                <br />
                                                                                <AdHoc:LogiButton ID="btnPickFromClient" OnClick="btnPickFromClient_Click"
                                                                                    Text="From My Computer" CausesValidation="False" runat="server" meta:resourcekey="btnPickFromClientResource1" />
                                                                                <AdHoc:LogiButton ID="btnPickFromServer" OnClick="btnPickFromServer_Click"
                                                                                    Text="From Server" CausesValidation="False" runat="server" meta:resourcekey="btnPickFromServerResource1" />
                                                                                                   
                                                                                
                                                                                
                                                                            </ContentTemplate>
                                                                        </asp:UpdatePanel>
                                                                    </td>
                                                                    <td id="tdURL" runat="server">
                                                                        <asp:Label ID="Label153" runat="server" meta:resourcekey="LiteralResource148" Text="URL:" AssociatedControlID="txImage"></asp:Label>
                                                                        <asp:TextBox ID="txImage" runat="server" AutoPostBack="True" OnTextChanged="Image_Changed" Width="350px" meta:resourcekey="txImageResource1" />
                                                                        <asp:RequiredFieldValidator EnableClientScript="False" ID="rtvImageURL" runat="server" ControlToValidate="txImage"
                                                                            ErrorMessage="Image URL is required." meta:resourcekey="rtvImageURLResource1" ValidationGroup="Image">*</asp:RequiredFieldValidator>
                                                                        <asp:RegularExpressionValidator ID="revImageURL" runat="server" ControlToValidate="txImage" EnableClientScript="False"
                                                                            ErrorMessage="Image URL must be a valid URL pointing to an image." meta:resourcekey="revImageURLResource1"
                                                                            ValidationExpression="^http\:\/\/[a-zA-Z0-9\-\.]+(?:\/\S*)?(?:[a-zA-Z0-9_])+\.(?:jpg|jpeg|gif|bmp)$" ValidationGroup="Image">*</asp:RegularExpressionValidator>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <asp:UpdatePanel ID="UPImageError" runat="server" UpdateMode="Conditional"><ContentTemplate>
                                                                        <ul runat="server" EnableViewState="False" id="imgErrorList" class="validation_error">
                                                                            <li>
                                                                                <asp:Label runat="server" EnableViewState="False" ID="lblImgErrMessage" meta:resourcekey="lblImgErrMessageResource1"></asp:Label>
                                                                            </li>
                                                                        </ul>
                                                                        </ContentTemplate></asp:UpdatePanel>
                                                                    </td>
                                                                </tr>
                                                           </table>
                                                        </td>
                                                    </tr>
                                                    <tr id="trImageAppearance" runat="server">
                                                        <td id="Td105" runat="server">
                                                            <asp:Label ID="Label129" runat="server" meta:resourcekey="LiteralResource183" 
                                                                Text="Style:" AssociatedControlID="ddlImageClass"></asp:Label>
                                                        </td>
                                                        <td id="Td106" runat="server">
                                                            <asp:DropDownList ID="ddlImageClass" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ImageMethod_Changed" meta:resourcekey="ImageClassResource1" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </ContentTemplate>
                                        </asp:UpdatePanel> 
                                        <asp:ValidationSummary ID="ValidationSummary11" runat="server" ValidationGroup="Image" meta:resourcekey="ValidationSummary11Resource1" />
                                        <asp:Label ID="lblSemicolon11" runat="server" OnPreRender="lblSemicolon11_PreRender" meta:resourcekey="lblSemicolon11Resource1" />
                                    </asp:Panel>
                                </ContentTemplate>
                                </asp:UpdatePanel>
                                </div>
                                
                                <wizard:navbtns ID="myNavButtons" runat="server" />
                                </ContentTemplate>
                                </asp:UpdatePanel>
                                
                                <br />

                                <asp:Button ID="btnClosePopupPreview" runat="server" OnClick="CloseLivePreviewPopup" Style="display: none" meta:resourcekey="btnClosePopupPreviewResource1" />
                                    
                                <asp:UpdatePanel ID="UPPreviewMain" runat="server" UpdateMode="Conditional" RenderMode="Inline">
                                <ContentTemplate>
                                <div id="divPreviewMain" runat="server" class="searchDiv">
                                    <div class="collapsePanelHeader">
                                        <table width="100%">
                                            <tr>
                                                <td align="right" valign="middle">
                                                <asp:Localize ID="Localize141" runat="server" Text="Live Preview" meta:resourcekey="LiteralResource141"></asp:Localize>
                                                    &nbsp;
                                                <asp:ImageButton ID="imgExpand2" runat="server" CausesValidation="False" ImageUrl="../ahImages/collapse_blue.jpg"
                                                    meta:resourcekey="imgExpandCollapsePreviewResource1" OnClientClick="javascript: noMessage = true;"
                                                    OnCommand="ExpandCollapsePreview" ToolTip="Click to collapse/expand Live Preview." AlternateText="Collapse/Expand" />
                                                <asp:ImageButton ID="imgPopupPreview" runat="server" CausesValidation="False" ImageUrl="../ahImages/right_blue.jpg"
                                                    meta:resourcekey="imgPopupPreviewResource1" OnClientClick="javascript: noMessage=true;"
                                                    OnCommand="PopupLivePreview" ToolTip="Click to open Live Preview in a popup window." AlternateText="Popup" />
                                                </td>
                                            </tr>
                                        </table>
                                        
                                    </div>
                                    <div id="divPreview" runat="server">
                                        <table width="100%">
                                            <tr>
                                                <td valign="top">

                                 <asp:UpdateProgress ID="UpdateProgress1" runat="server" 
                                    AssociatedUpdatePanelID="UPPreview">
                                    <ProgressTemplate>
                                   loading...
                                    </ProgressTemplate>
                                </asp:UpdateProgress>

                                                <asp:UpdatePanel ID="UPPreview" runat="server" UpdateMode="Conditional" >
                                                <ContentTemplate>
                                                <iframe width="100%" height="500px" id="ifPreview" runat="server" />
                                                </ContentTemplate>
                                                </asp:UpdatePanel>
                                
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                                </ContentTemplate>
                                </asp:UpdatePanel>
                            </td>
                        </tr>
                    </table>
                    <div id="divCFModalPopups">
                        <asp:Button runat="server" ID="btnFakeSummary" Style="display: none" meta:resourcekey="btnFakeSummaryResource1" />
                        <ajaxToolkit:ModalPopupExtender runat="server" ID="mpeColumnSummary" BehaviorID="mpeColumnSummaryBehavior"
                            TargetControlID="btnFakeSummary" PopupControlID="pnlSummaryPopup" BackgroundCssClass="modalBackground" 
                            PopupDragHandleControlID="pnlDragHandle5" RepositionMode="None" DynamicServicePath="" Enabled="True">
                        </ajaxToolkit:ModalPopupExtender>
                        <asp:Panel runat="server" CssClass="modalPopup" ID="pnlSummaryPopup" Style="display: none; width: 706px;" meta:resourcekey="pnlSummaryPopupResource1">
                            <asp:Panel ID="pnlDragHandle5" runat="server" Style="cursor: hand;" meta:resourcekey="pnlDragHandle5Resource1">
                            <div class="modalPopupHandle" style="width: 700px;">
                                <table cellpadding="0" cellspacing="0" style="width: 100%"><tr><td>
                                        <asp:UpdatePanel ID="UPSummaryPopup" runat="server" UpdateMode="Conditional" RenderMode="Inline"><ContentTemplate>
                                            <asp:Label ID="lblSummaryPopupHeader" runat="server" meta:resourcekey="lblSummaryPopupHeaderResource1" /></ContentTemplate></asp:UpdatePanel>
                                    </td>
                                    <td style="width: 20px;">
                                        <asp:ImageButton ID="imgClosePopup5" runat="server" 
                                            OnClick="imgClosePopup5_Click" CausesValidation="False"
                                            SkinID="imgbClose" ImageUrl="../ahImages/remove.gif" 
                                            AlternateText="<%$ Resources:LogiAdHoc, CloseWindow %>" />
                                </td></tr></table>
                            </div>
                            </asp:Panel>
                            <div class="modalDiv">
                                <asp:UpdatePanel ID="UPColumnSummaryPopup" runat="server" UpdateMode="Conditional" RenderMode="Inline">
                                    <ContentTemplate>
                                        <div id="divColumnSummaryPopup" runat="server">
                                            <asp:UpdatePanel ID="UpdatePanel4" runat="server" UpdateMode="Conditional" RenderMode="Inline">
                                                <ContentTemplate>
                                                    <div id="divSummaryGrid" runat="server" style="height: 140px; overflow: auto;">
                                                        <asp:Button ID="btnFakeGrdSummaryReorderRows" runat="server" CssClass="NoShow" 
                                                            OnClick="btnFakeGrdSummaryReorderRows_Click" meta:resourcekey="btnFakeGrdSummaryReorderRowsResource1" />
                                                        <asp:UpdatePanel ID="UPGrdSummary" runat="server" UpdateMode="Conditional" RenderMode="inline">
                                                            <ContentTemplate>
                                                                <%--<AdHoc:DDGridView ID="grdSummary" runat="server" AutoGenerateColumns="False" 
                                                                    OnRowDataBound="OnSummaryItemDataBoundHandler" CssClass="gridWline" 
                                                                    DDDivID="divSGDragHandle" PostbackButtonID="btnFakeGrdSummaryReorderRows" meta:resourcekey="grdSummaryResource1">--%>
                                                                <asp:GridView ID="grdSummary" runat="server" AutoGenerateColumns="False" 
                                                                    OnRowDataBound="OnSummaryItemDataBoundHandler" CssClass="gridWline" 
                                                                    meta:resourcekey="grdSummaryResource1">
                                                                    <Columns>
                                                                        <asp:TemplateField HeaderText="<%$ Resources:LogiAdHoc, Name %>" meta:resourcekey="TemplateFieldResource10">
                                                                            <ItemTemplate>
                                                                                <input runat="server" ID="SMRowOrder" type="hidden" />
                                                                                <input type="hidden" id="ihSummaryRowDefinition" runat="server" />
                                                                                <input type="hidden" id="ihSummaryRowPresentation" runat="server" />
                                                                                <table cellpadding="0" cellspacing="0">
                                                                                    <tr>
                                                                                        <%--<td>
                                                                                            <div runat="server" ID="divSGDragHandle" class="dragHandle"></div>
                                                                                        </td>--%>
                                                                                        <td>
                                                                                            <asp:Label runat="server" AssociatedControlID="txtSummaryName" Text="<%$ Resources:LogiAdHoc, Name %>" CssClass="NoShow" ID="lbltxtSummaryName" ></asp:Label>
                                                                                            <input runat="server" ID="txtSummaryName" type="text" style="width: 100px;" />
                                                                                            <asp:RequiredFieldValidator ID="rfvSummaryName" runat="server" ControlToValidate="txtSummaryName"
                                                                                                    EnableClientScript="false" ErrorMessage="Name is required" ValidationGroup="Summary"
                                                                                                    meta:resourcekey="rfvSummaryNameResource1">*</asp:RequiredFieldValidator>
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                            </ItemTemplate>
                                                                            <HeaderStyle Width="130px"></HeaderStyle>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField HeaderText="<%$ Resources:LogiAdHoc, Label %>">
                                                                            <ItemTemplate>
                                                                                <asp:Label runat="server" AssociatedControlID="txtSummaryLabel" Text="<%$ Resources:LogiAdHoc, Label %>" CssClass="NoShow" ID="lbltxtSummaryLabel"></asp:Label>
                                                                                <input runat="server" ID="txtSummaryLabel" type="text" style="width: 100px;" />
                                                                            </ItemTemplate>
                                                                            <HeaderStyle Width="120px"></HeaderStyle>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField HeaderText="<%$ Resources:LogiAdHoc, Summary %>">
                                                                            <ItemTemplate>
                                                                                <asp:Label runat="server" AssociatedControlID="summary" Text="<%$ Resources:LogiAdHoc, Summary %>" CssClass="NoShow" ID="lblsummary" meta:resourcekey="lblsummaryResource1"></asp:Label>
                                                                                <asp:DropDownList runat="server" ID="summary" AutoPostback="True" OnSelectedIndexChanged="Summary_SelectedIndexChanged" meta:resourcekey="summaryResource2" ></asp:DropDownList>
                                                                            </ItemTemplate>
                                                                            <HeaderStyle Width="50px"></HeaderStyle>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField HeaderText="Format" meta:resourcekey="grdSummaryFormat">
                                                                            <ItemTemplate>
                                                                                <asp:Label runat="server" AssociatedControlID="ddlSummaryFormat" Text="Format" CssClass="NoShow" ID="lblddlSummaryFormat" meta:resourceKey="lblddlSummaryFormatResource1"></asp:Label>
                                                                                <asp:DropDownList runat="server" ID="ddlSummaryFormat" meta:resourcekey="ddlSummaryFormatResource2"></asp:DropDownList>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField HeaderText="<%$ Resources:LogiAdHoc, Actions %>" meta:resourcekey="TemplateFieldResource12">
                                                                            <ItemTemplate>
                                                                                <asp:ImageButton runat="server" CommandName="SummaryAggregateMoveUp" CausesValidation="False" AlternateText="Move Aggregate Up" ImageUrl="../ahImages/SmallArrowUp.gif" ID="imgMoveupSummaryAggregate" meta:resourceKey="imgMoveupSummaryAggregateResource1" OnCommand="SummaryModifications"></asp:ImageButton>
                                                                                <asp:ImageButton runat="server" CommandName="SummaryAggregateMoveDown" CausesValidation="False" AlternateText="Move Aggregate Down" ImageUrl="../ahImages/SmallArrowDown.gif" ID="imgMovedownSummaryAggregate" meta:resourceKey="imgMovedownSummaryAggregateResource1" OnCommand="SummaryModifications"></asp:ImageButton>
                                                                                &nbsp;
                                                                                <asp:ImageButton ID="imgModifySpacer" ImageUrl="../ahImages/spacer.gif" AlternateText="" CausesValidation="False"
                                                                                    ToolTip="" runat="server" Enabled="false" />
                                                                                <asp:ImageButton ID="imgModify" SkinID="imgSingleAction" AlternateText="Modify Aggregate" CausesValidation="False"
                                                                                    ToolTip="Modify Aggregate" runat="server" OnCommand="SummaryModifications" meta:resourcekey="EditItemResource1" CommandName="ModifyAggregate" />
                                                                                <asp:ImageButton runat="server" CommandName="RemoveAggregate" AlternateText="Remove Aggregate" ImageUrl="../ahImages/remove.gif" 
                                                                                    ID="RemoveAggregate" meta:resourceKey="RemoveAggregateResource1" OnCommand="SummaryModifications"></asp:ImageButton>
                                                                            </ItemTemplate>
                                                                            <HeaderStyle Width="90px" />
                                                                            <ItemStyle HorizontalAlign="Center" Wrap="False" />
                                                                        </asp:TemplateField>
                                                                    </Columns>
                                                                    <FooterStyle CssClass="gridfooter" />
                                                                    <HeaderStyle CssClass="gridheader" />
                                                                <%--</AdHoc:DDGridView>--%>
                                                                </asp:GridView>
                                                            </ContentTemplate>
                                                        </asp:UpdatePanel>
                                                    </div>
                                                    <br /><table><tr><td>
                                                    <AdHoc:LogiButton ID="btnAddSummary" OnClick="AddSummary_OnClick" Text="Add a Summary"
                                                        runat="server" meta:resourcekey="btnAddSummaryResource1" />
                                                    </td></tr></table>
                                                    
                                                    <asp:UpdatePanel ID="UPSummaryCalc" runat="server" UpdateMode="Conditional">
                                                        <ContentTemplate>
                                                            <asp:Button runat="server" ID="btnFakeSummaryCalcDetails" Style="display: none" />
                                                            <ajaxToolkit:ModalPopupExtender runat="server" ID="mpeSummaryCalcDetails" BehaviorID="mpeSummaryCalcDetailsBehavior"
                                                                TargetControlID="btnFakeSummaryCalcDetails" PopupControlID="pnlSummaryCalcDetails" BackgroundCssClass="modalBackground" >
                                                            </ajaxToolkit:ModalPopupExtender>
                                                            <asp:Panel runat="server" CssClass="modalPopup" ID="pnlSummaryCalcDetails" Style="display: none; width: 736;" meta:resourcekey="pnlParamDetailsResource1">
                                                                <asp:Panel ID="pnlDragHandle15" runat="server">
                                                                    <div class="modalPopupHandle" style="width: 730px; cursor: default;">
                                                                        <table cellpadding="0" cellspacing="0" style="width: 100%"><tr><td>
                                                                                <asp:Label ID="lblSummaryCalcDetailsHeader" runat="server" />
                                                                            </td>
                                                                            <td style="width: 20px;">
                                                                                <asp:ImageButton ID="imgClosePopup15" runat="server" 
                                                                                    OnClick="imgClosePopup15_Click" CausesValidation="False"
                                                                                    SkinID="imgbClose" ImageUrl="../ahImages/remove.gif" 
                                                                                    AlternateText="<%$ Resources:LogiAdHoc, CloseWindow %>" />
                                                                        </td></tr></table>
                                                                    </div>
                                                                </asp:Panel>
                                                                <div id="divCalcColumnFuncDetails" runat="server" class="modalDiv">
                                                                    <asp:UpdatePanel ID="UpdatePanel16" runat="server" UpdateMode="Conditional" RenderMode="inline">
                                                                        <ContentTemplate>
                                                                            <input type="hidden" id="ihSummaryCalcColumnIdx" runat="server" />
                                                                            <table>
                                                                                <tr>
                                                                                    <td width="250px">
                                                                                        <div id="divSummCalcAggrCol" runat="server">
                                                                                            <asp:Localize ID="Localize15" runat="server" Text="Use an existing aggregate: "></asp:Localize>
                                                                                            
                                                                                            <div id="divSummCalcAggrTreeView" runat="server" style="width:240px; height: 100px;" class="divColumnTreeView">
                                                                                                <cc1:DataSourceTreeView ID="trvSumCalcAggrs" runat="server" >
                                                                                                </cc1:DataSourceTreeView>
                                                                                            </div>
                                                                                            <div id="test" runat="server">
                                                                                            </div>
                                                                                            
                                                                                            <asp:Localize ID="Localize18" runat="server" Text="Or an aggregate of a column: "></asp:Localize>
                                                                                        </div>
                                                                                        <div id="divSummCalcFuncCol" runat="server">
                                                                                            <table><tr><td>
                                                                                                    <asp:Localize ID="Localize192" runat="server" Text="Function:"></asp:Localize>
                                                                                                </td>
                                                                                                <td>
                                                                                                    <asp:DropDownList ID="ddlSummaryCalcFunction" runat="server" AutoPostback="True" OnSelectedIndexChanged="ddlSummaryCalcFunction_SelectedIndexChanged" />
                                                                                            </td></tr></table>
                                                                                            
                                                                                            <div id="divColumnTreeView" runat="server" style="width:240px; height: 150px;" class="divColumnTreeView">
                                                                                                <asp:UpdatePanel ID="UPTrvSumCalcColumns" runat="server" UpdateMode="conditional" RenderMode="inline">
                                                                                                    <ContentTemplate>
                                                                                                        <cc1:DataSourceTreeView ID="trvSumCalcColumns" runat="server" >
                                                                                                        </cc1:DataSourceTreeView>
                                                                                                    </ContentTemplate>
                                                                                                    <Triggers>
                                                                                                        <asp:AsyncPostBackTrigger ControlID="ddlSummaryCalcFunction" EventName="SelectedIndexChanged" />
                                                                                                    </Triggers>
                                                                                                </asp:UpdatePanel>
                                                                                            </div>
                                                                                        </div>
                                                                                    </td>
                                                                                    <td>
                                                                                        <table><tr><td>
                                                                                                <asp:Localize ID="Localize195" runat="server" Text="<%$ Resources:LogiAdHoc, Name %>"></asp:Localize>:
                                                                                            </td>
                                                                                            <td>
                                                                                                <asp:TextBox ID="txtSummaryCalcName" runat="server" />
                                                                                                <asp:RequiredFieldValidator ID="rfvSummaryCalcName" runat="server" ControlToValidate="txtSummaryCalcName"
                                                                                                    EnableClientScript="false" ErrorMessage="Name is required" ValidationGroup="SummaryCalc"
                                                                                                    meta:resourcekey="rfvSummaryCalcNameResource1">*</asp:RequiredFieldValidator>
                                                                                        </td></tr></table>
                                                                                        <div id="divSummaryLabel" runat="server">
                                                                                            <table><tr><td>
                                                                                                <asp:Localize ID="Localize12" runat="server" Text="<%$ Resources:LogiAdHoc, Label %>"></asp:Localize>:
                                                                                            </td>
                                                                                            <td>
                                                                                                <asp:TextBox ID="txtSummaryCalcLabel" runat="server" />
                                                                                            </td></tr></table>
                                                                                        </div>
                                                                                        <asp:Label ID="locDef" runat="server" Text="Definition:" AssociatedControlID="txtFormula" meta:resourceKey="locDef"></asp:Label>
                                                                                        <br />
                                                                                        <textarea id="txtFormula" runat="server" cols="92" rows="5" 
                                                                                            onclick="storeCaret('txtFormula')" onkeyup="storeCaret('txtFormula')" >
                                                                                        </textarea>
                                                                                        <br />
                                                                                        <%--<div style="float:right; padding-top:5px;">
                                                                                            <AdHoc:LogiButton ID="btnTestCalcColumn" runat="server" Text=" Test " 
                                                                                                OnClick="TestColumn_OnClick" ValidationGroup="Calculation" meta:resourcekey="LogiButton6Resource1" />
                                                                                        </div>--%>
                                                                                        <br />
                                                                                        <br />
                                                                                        <asp:Localize ID="locOp" runat="server" Text="Operators:" meta:resourceKey="locOp"></asp:Localize>
                                                                                        <AdHoc:LogiButton ID="btnP" runat="server" OnClientClick="appendSymbol('+','txtFormula');return false;" Text="+" CausesValidation="false" UseSubmitBehavior="False" />
                                                                                        <AdHoc:LogiButton ID="btnM" runat="server" OnClientClick="appendSymbol('-','txtFormula');return false;" Text="-" CausesValidation="false" UseSubmitBehavior="False" />
                                                                                        <AdHoc:LogiButton ID="btnL" runat="server" OnClientClick="appendSymbol('*','txtFormula');return false;" Text="x" CausesValidation="false" UseSubmitBehavior="False" />
                                                                                        <AdHoc:LogiButton ID="btnD" runat="server" OnClientClick="appendSymbol('/','txtFormula');return false;" Text="/" CausesValidation="false" UseSubmitBehavior="False" />
                                                                                        <AdHoc:LogiButton ID="btnO" runat="server" OnClientClick="appendSymbol('(','txtFormula');return false;" Text="(" CausesValidation="false" UseSubmitBehavior="False" />
                                                                                        <AdHoc:LogiButton ID="btnC" runat="server" OnClientClick="appendSymbol(')','txtFormula');return false;" Text=")" CausesValidation="false" UseSubmitBehavior="False" />
                                                                                        <br />
                                                                                        <br />
                                                                                        <%--<table>
                                                                                             <tr>
                                                                                                <td colspan="2">
                                                                                                    <br />
                                                                                                    <AdHoc:LogiButton ID="btnSaveCalc" runat="server" Text="Save" OnClick="SaveCalculation" 
                                                                                                        meta:resourcekey="LogiButton3Resource1" ValidationGroup="Calculation"/>
                                                                                                    <AdHoc:LogiButton ID="btnNewCalc" runat="server" Text=" New " OnClick="NewCalculation" 
                                                                                                        meta:resourcekey="LogiButton2Resource1" CausesValidation="false" UseSubmitBehavior="false"/>
                                                                                                </td>
                                                                                            </tr>
                                                                                        </table>--%>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                            <div id="divSummaryCalcButtons" runat="server">
                                                                                <table><tr><td>
                                                                                    <AdHoc:LogiButton ID="btnSummaryCalcDetailsDone" OnClick="SummaryCalcDetailsDone_OnClick" runat="server" 
                                                                                        Text="<%$ Resources:LogiAdHoc, OKWithSpaces %>" CausesValidation="False" meta:resourcekey="btnSummaryCalcDetailsDoneResource1" />
                                                                                    <AdHoc:LogiButton ID="btnSummaryCalcDetailsCancel" OnClick="SummaryCalcDetailsCancel_OnClick" runat="server"
                                                                                        Text="<%$ Resources:LogiAdHoc, Cancel %>" CausesValidation="False" meta:resourcekey="btnSummaryCalcDetailsCancelResource1" />
                                                                                </td></tr></table>
                                                                            </div>
                                                                        </ContentTemplate>
                                                                    </asp:UpdatePanel>
                                                                </div>
                                                            </asp:Panel>
                                                        </ContentTemplate>
                                                    </asp:UpdatePanel>
                                                </ContentTemplate>
                                            </asp:UpdatePanel>
                                            <!-- Buttons -->
                                        <br /><table><tr><td>
                                                <asp:ValidationSummary ID="ValidationSummary1" runat="server" ValidationGroup="Summary" />
                                                <AdHoc:LogiButton ID="btnSummaryOK" OnClick="SummaryDone_OnClick" runat="server" ValidationGroup="Summary"
                                                    Text="<%$ Resources:LogiAdHoc, OKWithSpaces %>"  meta:resourcekey="btnSummaryOKResource1" />
                                                <AdHoc:LogiButton ID="btnSummaryCancel" OnClick="SummaryCancel_OnClick" runat="server"
                                                    Text="<%$ Resources:LogiAdHoc, Cancel %>" CausesValidation="False" meta:resourcekey="btnSummaryCancelResource1" />
                                        </td></tr></table>
                                        </div>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </div>
                        </asp:Panel>
                        <asp:Button runat="server" ID="btnFakeVisualization" Style="display: none" meta:resourcekey="btnFakeVisualizationResource1" />
                        <ajaxToolkit:ModalPopupExtender runat="server" ID="mpeVisualization" BehaviorID="mpeVisualizationBehavior"
                            TargetControlID="btnFakeVisualization" PopupControlID="pnlVisualizationPopup" BackgroundCssClass="modalBackground" PopupDragHandleControlID="pnlDragHandle6" RepositionMode="None" DynamicServicePath="" Enabled="True" >
                        </ajaxToolkit:ModalPopupExtender>
                        <asp:Panel runat="server" CssClass="modalPopup" ID="pnlVisualizationPopup" Style="display: none; width: 326;" meta:resourcekey="pnlVisualizationPopupResource1">
                            <asp:Panel ID="pnlDragHandle6" runat="server" Style="cursor: hand;" meta:resourcekey="pnlDragHandle6Resource1">
                            <div class="modalPopupHandle" style="width: 320px;">
                                <table cellpadding="0" cellspacing="0" ><tr><td>
                                        <asp:UpdatePanel ID="UPVisuallizationPopup" runat="server" UpdateMode="Conditional" RenderMode="Inline"><ContentTemplate>
                                            <asp:Label ID="lblVisualizationPopupHeader" runat="server" meta:resourcekey="lblVisualizationPopupHeaderResource1" /></ContentTemplate></asp:UpdatePanel>
                                    </td>
                                    <td style="width: 20px;">
                                        <asp:ImageButton ID="imgClosePopup6" runat="server" 
                                            OnClick="imgClosePopup6_Click" CausesValidation="False"
                                            SkinID="imgbClose" ImageUrl="../ahImages/remove.gif" 
                                            AlternateText="<%$ Resources:LogiAdHoc, CloseWindow %>" />
                                </td></tr></table>
                            </div>
                            </asp:Panel>
                            <div class="modalDiv">
                                <asp:UpdatePanel ID="UPColumnVisualization" runat="server" UpdateMode="Conditional" RenderMode="Inline">
                                    <ContentTemplate>
                                        <div id="divColumnVisualization" runat="server">
                                            <table>
                                                <tr>
                                                    <td><asp:Label ID="Label40" runat="server" meta:resourcekey="LiteralResource40" Text="Visualization Style:" AssociatedControlID="Visualization"></asp:Label></td>
                                                    <td>
                                                        <asp:DropDownList ID="Visualization" runat="server" AutoPostBack="True" meta:resourcekey="VisualizationResource2" />
                                                    </td>
                                                </tr>
                                                <tr id="trShowData" runat="server">
                                                    <td id="Td107" valign="top" runat="server"><asp:Localize ID="Localize41" runat="server" meta:resourcekey="LiteralResource41" Text="Show Data Values:"></asp:Localize></td>
                                                    <td id="Td108" runat="server">
                                                        <asp:RadioButtonList ID="VizShowData" runat="server" meta:resourcekey="VizShowDataResource1">
                                                            <asp:ListItem Value="0" Text="<%$ Resources:LogiAdHoc, No %>" />
                                                            <asp:ListItem Selected="True" Value="1" Text="<%$ Resources:LogiAdHoc, Yes %>" />
                                                        </asp:RadioButtonList>
                                                    </td>
                                                </tr>
                                            </table>
                                            <!-- Buttons -->
                                        <br /><table><tr><td>
                                                <AdHoc:LogiButton ID="btnVizOK" OnClick="VisualizationDone_OnClick" runat="server" 
                                                    Text="<%$ Resources:LogiAdHoc, OKWithSpaces %>" CausesValidation="False" meta:resourcekey="btnVizOKResource1" />
                                                <AdHoc:LogiButton ID="btnVizCancel" OnClick="VisualizationCancel_OnClick" runat="server"
                                                    Text="<%$ Resources:LogiAdHoc, Cancel %>" CausesValidation="False" meta:resourcekey="btnVizCancelResource1" />
                                        </td></tr></table>
                                        </div>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </div>
                        </asp:Panel>
                        <asp:Button runat="server" ID="btnFakeAppearance" Style="display: none" meta:resourcekey="btnFakeAppearanceResource1" />
                        <ajaxToolkit:ModalPopupExtender runat="server" ID="mpeAppearance" BehaviorID="mpeAppearanceBehavior"
                            TargetControlID="btnFakeAppearance" PopupControlID="pnlAppearancePopup" BackgroundCssClass="modalBackground" PopupDragHandleControlID="pnlDragHandle7" RepositionMode="None" DynamicServicePath="" Enabled="True">
                        </ajaxToolkit:ModalPopupExtender>
                        <asp:Panel runat="server" CssClass="modalPopup" ID="pnlAppearancePopup" Style="display: none; width: 656px;" meta:resourcekey="pnlAppearancePopupResource1">
                            <asp:Panel ID="pnlDragHandle7" runat="server" Style="cursor: hand;" meta:resourcekey="pnlDragHandle7Resource1">
                            <div class="modalPopupHandle" style="width: 650px;">
                                <table cellpadding="0" cellspacing="0" style="width: 100%"><tr><td>
                                        <asp:UpdatePanel ID="UPAppearancePopup" runat="server" UpdateMode="Conditional" RenderMode="Inline"><ContentTemplate>
                                            <asp:Label ID="lblAppearancePopupHeader" runat="server" meta:resourcekey="lblAppearancePopupHeaderResource1" /></ContentTemplate></asp:UpdatePanel>
                                    </td>
                                    <td style="width: 20px;">
                                        <asp:ImageButton ID="imgClosePopup7" runat="server" 
                                            OnClick="imgClosePopup7_Click" CausesValidation="False"
                                            SkinID="imgbClose" ImageUrl="../ahImages/remove.gif" 
                                            AlternateText="<%$ Resources:LogiAdHoc, CloseWindow %>" />
                                </td></tr></table>
                            </div>
                            </asp:Panel>
                            <div class="modalDiv">
                                <asp:UpdatePanel ID="UPColumnAppearance" runat="server" UpdateMode="Conditional" RenderMode="Inline">
                                    <ContentTemplate>
                                        <div id="divColumnAppearance" runat="server">
                                        <asp:UpdatePanel ID="UPPresentation" runat="server" UpdateMode="Conditional" RenderMode="Inline">
                                            <ContentTemplate>
                                                <div id="divConditionGrid" runat="server" style="height: 130px; overflow: auto;">
                                                    <asp:Button ID="btnFakeGrdConditionReorderRows" runat="server" CssClass="NoShow" 
                                                        OnClick="btnFakeGrdConditionReorderRows_Click" meta:resourcekey="btnFakeGrdConditionReorderRowsResource1" />
                                                    <%--<AdHoc:DDGridView ID="grdCondition" runat="server" AutoGenerateColumns="False" 
                                                        OnRowDataBound="OnCondParamItemDataBoundHandler" CssClass="gridWline" 
                                                        DDDivID="divCGDragHandle" PostbackButtonID="btnFakeGrdConditionReorderRows" meta:resourcekey="grdConditionResource1">--%>
                                                    <asp:GridView ID="grdCondition" runat="server" AutoGenerateColumns="False" 
                                                        OnRowDataBound="OnCondParamItemDataBoundHandler" CssClass="gridWline" 
                                                        meta:resourcekey="grdConditionResource1">
                                                        <Columns>
                                                            <asp:TemplateField HeaderText="<%$ Resources:LogiAdHoc, Column %>" meta:resourcekey="TemplateFieldResource13">
                                                                <ItemTemplate>
                                                                    <input runat="server" ID="CFCondRowOrder" type="hidden" />
                                                                    <table cellpadding="0" cellspacing="0">
                                                                        <tr>
                                                                            <%--<td>
                                                                                <div runat="server" ID="divCGDragHandle" class="dragHandle"></div>
                                                                            </td>--%>
                                                                            <td>
                                                                                <input runat="server" ID="CGCondParamKey" type="hidden" />
                                                                                <asp:Label runat="server" ID="lblCondColumnName" meta:resourcekey="lblCondColumnNameResource1"></asp:Label>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </ItemTemplate>
                                                                <HeaderStyle Width="120px"></HeaderStyle>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="<%$ Resources:LogiAdHoc, Op_Operator %>" meta:resourcekey="TemplateFieldResource14" >
                                                                <ItemTemplate>
                                                                    <asp:Label runat="server" ID="lblCondOperator" meta:resourcekey="lblCondOperatorResource1"></asp:Label>
                                                                </ItemTemplate>
                                                                <HeaderStyle Width="50px"></HeaderStyle>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="<%$ Resources:LogiAdHoc, Value %>" meta:resourcekey="TemplateFieldResource15">
                                                                <ItemTemplate>
                                                                    <asp:Label runat="server" ID="lblCondParamValue" meta:resourcekey="lblCondParamValueResource1"></asp:Label>
                                                                </ItemTemplate>
                                                                <HeaderStyle Width="150px"></HeaderStyle>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="<%$ Resources:LogiAdHoc, Style %>" meta:resourcekey="TemplateFieldResource16" >
                                                                <ItemTemplate>
                                                                    <asp:Label runat="server" ID="lblClassName" meta:resourcekey="lblClassNameResource1"></asp:Label>
                                                                </ItemTemplate>
                                                                <HeaderStyle Width="100px"></HeaderStyle>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="<%$ Resources:LogiAdHoc, Actions %>" meta:resourcekey="TemplateFieldResource17">
                                                                <ItemTemplate>
                                                                    <asp:ImageButton runat="server" CommandName="ConditionParamMoveUp" CausesValidation="False" AlternateText="Move Condition Up" ImageUrl="../ahImages/SmallArrowUp.gif" ID="imgMoveupOrder" meta:resourceKey="imgMoveupConditionrResource1" OnCommand="CondParamModifications"></asp:ImageButton>
                                                                    <asp:ImageButton runat="server" CommandName="ConditionParamMoveDown" CausesValidation="False" AlternateText="Move Condition Down" ImageUrl="../ahImages/SmallArrowDown.gif" ID="imgMovedownOrder" meta:resourceKey="imgMovedownConditionResource1" OnCommand="CondParamModifications"></asp:ImageButton>
                                                                    &nbsp;
                                                                    <asp:ImageButton runat="server" CommandName="ModifyParam" AlternateText="Modify Condition" ImageUrl="../ahImages/modify.gif" ID="ModifyCondParam" meta:resourceKey="ModifyCondParamResource1" OnCommand="CondParamModifications"></asp:ImageButton>
                                                                    <asp:ImageButton runat="server" CommandName="RemoveParam" AlternateText="Remove Condition" ImageUrl="../ahImages/remove.gif" ID="RemoveCondParam" meta:resourceKey="RemoveCondParamResource1" OnCommand="CondParamModifications"></asp:ImageButton>
                                                                </ItemTemplate>
                                                                <HeaderStyle Width="90px" />
                                                                <ItemStyle HorizontalAlign="Center" Wrap="False" />
                                                            </asp:TemplateField>
                                                        </Columns>
                                                        <HeaderStyle CssClass="gridheader" />
                                                    <%--</AdHoc:DDGridView>--%>
                                                    </asp:GridView>
                                                </div>
                                        <br /><table><tr><td>
                                                <AdHoc:LogiButton ID="LogiButton1" OnClick="AddCondParam_OnClick" Text="Add a Condition"
                                                        runat="server" meta:resourcekey="AddConditionResource1" />
                                        </td></tr></table>
                                                <asp:Panel ID="pnlCondStyleDetails" CssClass="detailpanel" runat="server" meta:resourcekey="pnlCondStyleDetailsResource1">
                                                    <asp:UpdatePanel ID="UpdatePanel20" runat="server" UpdateMode="Conditional" RenderMode="Inline">
                                                        <ContentTemplate>
                                                            <input type="hidden" id="CondParamKey" runat="server" />
                                                            <table cellspacing="0" cellpadding="1" border="0">
                                                                <tr>
                                                                    <td valign="top" width="100px">
                                                                        <asp:Label ID="Label135" runat="server" Text="<%$ Resources:LogiAdHoc, Column %>" AssociatedControlID="ddlStyleColumnKey" meta:resourcekey="Label135Resource1"></asp:Label>:
                                                                    </td>
                                                                    <td>
                                                                        <asp:DropDownList ID="ddlStyleColumnKey" runat="server" AutoPostBack="True" OnSelectedIndexChanged="StyleColumnID_OnChangeHandler"
                                                                            meta:resourcekey="ColumnIDResource1" />
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td valign="top" width="100px">
                                                                        <asp:Label ID="Label136" runat="server" Text="<%$ Resources:LogiAdHoc, Op_Operator %>" AssociatedControlID="ddlStyleOperator" meta:resourcekey="Label136Resource1"></asp:Label>:
                                                                    </td>
                                                                    <td valign="top">
                                                                        <asp:DropDownList ID="ddlStyleOperator" runat="server" AutoPostBack="True" OnSelectedIndexChanged="StyleOperator_OnChangeHandler"
                                                                            meta:resourcekey="ddlOperatorResource1" />
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                            <div id="divStyleValues" runat="server">
                                                                <table cellspacing="0" cellpadding="1" border="0">
                                                                    <tr>
                                                                        <td id="tdStyleValuesLabel" runat="server" valign="top" width="100px">
                                                                            <asp:Localize ID="Localize138" runat="server" Text="<%$ Resources:LogiAdHoc, Value %>"></asp:Localize>:
                                                                        </td>
                                                                        <td valign="top">
                                                                            <span id="spnStyle1" runat="server">
                                                                                <wizard:SpecialValue id="svStyle1" runat="server" />
                                                                                <asp:ImageButton ID="StylePickFromDB1" AlternateText="<%$ Resources:LogiAdHoc, PickValueFromDB %>" ImageUrl="../ahImages/iconFind.gif"
                                                                                    OnClick="StylePickFromDatabase" runat="server" CausesValidation="False" meta:resourcekey="StylePickFromDB1Resource1" />
                                                                            </span>
                                                                            <asp:Label ID="lblStyleAnd" runat="server" Text="<%$ Resources:LogiAdHoc, Res_And %>" meta:resourcekey="lblStyleAndResource1"></asp:Label>
                                                                            <div id="spnStyle2" runat="server">
                                                                                <wizard:SpecialValue ID="svStyle2" runat="server" />
                                                                                <asp:ImageButton ID="StylePickFromDB2" AlternateText="<%$ Resources:LogiAdHoc, PickValueFromDB %>" ImageUrl="../ahImages/iconFind.gif"
                                                                                    OnClick="StylePickFromDatabase" runat="server" CausesValidation="False" meta:resourcekey="StylePickFromDB2Resource1" />
                                                                            </div>
                                                                            <asp:Label ID="lblchkStyleBit" runat="server" CssClass="NoShow" Text="<%$ Resources:LogiAdHoc, BitValue %>" AssociatedControlID="chkStyleBit" meta:resourcekey="lblchkStyleBitResource1"></asp:Label>
                                                                            <asp:CheckBox ID="chkStyleBit" runat="server" meta:resourcekey="chkBitValueResource1" />
                                                                            <asp:Label ID="lblfakeStyleColumnID" runat="server" CssClass="NoShow" Text="<%$ Resources:LogiAdHoc, Column %>" AssociatedControlID="fakeStyleColumnID" meta:resourcekey="lblfakeStyleColumnIDResource1" ></asp:Label>
                                                                            <asp:TextBox style="display: none;" id="fakeStyleColumnID" runat="server" meta:resourcekey="fakeStyleColumnIDResource2"/>
                                                                            <asp:CustomValidator ID="cvStyleParamValid" OnServerValidate="IsStyleParameterValid" ControlToValidate="fakeStyleColumnID"
                                                                                ValidationGroup="StyleGroup" ErrorMessage="This parameter leads to an invalid query." ValidateEmptyText="True"
                                                                                runat="server" meta:resourcekey="cvValidParamResource1">*</asp:CustomValidator>
                                                                        </td>
                                                                        <td id="tdFindStyleValues" valign="top" runat="server">
                                                                            <table>
                                                                                <tr>
                                                                                    <td>
                                                                                        <wizard:DatabaseValues id="dvStyleDBValues" runat="server" />
                                                                                    </td>
                                                                                    <td>
                                                                                        <AdHoc:LogiButton ID="btnDBValuesOK" Width="50px" runat="server" Text="<%$ Resources:LogiAdHoc, OKWithSpaces %>"
                                                                                            OnClick="btnStyleFVOk_Click" CausesValidation="False" meta:resourcekey="btnDBValuesOKResource1" />
                                                                                        <br />
                                                                                        <AdHoc:LogiButton ID="btnDBValuesCancel" Width="50px" runat="server" Text="<%$ Resources:LogiAdHoc, Cancel %>"
                                                                                            OnClick="btnStyleFVCancel_Click" CausesValidation="False" meta:resourcekey="btnDBValuesCancelResource1" />
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </div>
                                                        </ContentTemplate>
                                                    </asp:UpdatePanel>
                                                    
                                                    <table cellspacing="0" cellpadding="1" border="0">
                                                        <tr>
                                                            <td valign="top" width="100px">
                                                                <asp:Label ID="Label133" runat="server" Text="<%$ Resources:LogiAdHoc, Style %>" AssociatedControlID="ConditionalStyleClass" meta:resourcekey="Label133Resource1"></asp:Label>:
                                                            </td>
                                                            <td>
                                                                <asp:DropDownList ID="ConditionalStyleClass" runat="server" meta:resourcekey="ConditionalStyleClassResource1" />
                                                            </td>
                                                        </tr>
                                                    </table>
                                                    
                                                    
                                                    <!-- Buttons -->
                                                    <table>
                                                        <tr>
                                                            <td colspan="2">
                                                                <AdHoc:LogiButton ID="btnStyleOK" OnClick="SaveStyle_OnClick" runat="server"
                                                                    Text="<%$ Resources:LogiAdHoc, OKWithSpaces %>" ValidationGroup="StyleGroup" meta:resourcekey="btnStyleOKResource1"/>
                                                                <AdHoc:LogiButton ID="btnStyleCancel" OnClick="CancelStyle_OnClick" runat="server"
                                                                    Text="<%$ Resources:LogiAdHoc, Cancel %>" CausesValidation="False" meta:resourcekey="btnStyleCancelResource1" />
                                                            </td>
                                                            <td width="250px" align="right">
                                                                <AdHoc:LogiButton ID="btnTestStyleParam" OnClick="TestStyle_OnClick" Text="Validate Parameter"
                                                                    runat="server" meta:resourcekey="TestStyleParamResource1" />
                                                            </td>
                                                        </tr>
                                                    </table>
                                                    <asp:ValidationSummary ID="ValidationSummary12" runat="server" ValidationGroup="StyleGroup"
                                                        meta:resourcekey="ValidationSummary12Resource1" />
                                                    <asp:Label ID="lblSemicolon12" runat="server" OnPreRender="lblSemicolon_PreRender" meta:resourcekey="lblSemicolon12Resource1" />
                                                    <p id="pCondStyleValidationDone" runat="server">
                                                        <asp:Label ID="lblCondStyleValidationDone" runat="server" meta:resourcekey="lblCondStyleValidationDoneResource1"></asp:Label>
                                                    </p>
                                                </asp:Panel>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                        
                                        <!-- Buttons -->
                                        <table>
                                            <tr>
                                                <td>
                                                    <asp:CheckBox ID="ApplyToAll" runat="server" Text="Apply this style to all columns."
                                                        meta:resourcekey="ApplyToAllResource1"></asp:CheckBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <AdHoc:LogiButton ID="SaveCondition" OnClick="SaveCondParam_OnClick" runat="server" 
                                                        Text="<%$ Resources:LogiAdHoc, OKWithSpaces %>" meta:resourcekey="SaveConditionResource1" />
                                                    <AdHoc:LogiButton ID="CancelCondition" OnClick="CancelCondParam_OnClick" runat="server"
                                                        Text="<%$ Resources:LogiAdHoc, Cancel %>" CausesValidation="False" meta:resourcekey="CancelConditionResource1" />
                                                </td>
                                            </tr>
                                        </table>
                                        </div>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </div>
                        </asp:Panel>
                        <asp:Button runat="server" ID="btnFakeCFCalcColumn" Style="display: none" />
                        <ajaxToolkit:ModalPopupExtender runat="server" ID="mpeCFCalcColumn" BehaviorID="mpeCFCalcColumnBehavior"
                            TargetControlID="btnFakeCFCalcColumn" PopupControlID="pnlCFCalcColumn" BackgroundCssClass="modalBackground" 
                            PopupDragHandleControlID="pnlDragHandle16" RepositionMode="None" DynamicServicePath="" Enabled="True">
                        </ajaxToolkit:ModalPopupExtender>
                        <asp:Panel runat="server" CssClass="modalPopup" ID="pnlCFCalcColumn" Style="display: none; width: 766;" meta:resourcekey="pnlCFCalcColumnResource1">
                            <asp:Panel ID="pnlDragHandle16" runat="server">
                                <div class="modalPopupHandle" style="width: 760px; cursor: default;">
                                    <table cellpadding="0" cellspacing="0" style="width: 100%"><tr><td>
                                            <asp:Label ID="lblCFCalcColumnHeader" runat="server" Text="Custom Column" meta:resourceKey="lblCFCalcColumnHeaderResource1" />
                                        </td>
                                        <td style="width: 20px;">
                                            <asp:ImageButton ID="ImageButton17" runat="server" 
                                                OnClick="imgClosePopup17_Click" CausesValidation="False"
                                                SkinID="imgbClose" ImageUrl="../ahImages/remove.gif" 
                                                AlternateText="<%$ Resources:LogiAdHoc, CloseWindow %>" />
                                    </td></tr></table>
                                </div>
                            </asp:Panel>
                            <div id="divCFCalcColumnDetails" runat="server" class="modalDiv">
                                <asp:UpdatePanel ID="UPCFCalcColumnDetails" runat="server" UpdateMode="Conditional" RenderMode="inline">
                                    <ContentTemplate>
                                        <input type="hidden" id="ihCFCalcColumnID" runat="server" />
                                        <table>
                                            <tr>
                                                <td width="250px">
                                                    <div id="divCFCalcColumnTreeView" runat="server" style="width:240px; height: 290px;" class="divColumnTreeView">
                                                        <asp:UpdatePanel ID="UPtrvCFCalcColumn" runat="server" UpdateMode="conditional" RenderMode="inline">
                                                            <ContentTemplate>
                                                                <cc1:DataSourceTreeView ID="trvCFCalcColumns" runat="server" >
                                                                </cc1:DataSourceTreeView>
                                                            </ContentTemplate>
                                                        </asp:UpdatePanel>
                                                    </div>
                                                </td>
                                                <td>
                                                    <asp:Localize ID="Localize14" runat="server" Text="<%$ Resources:LogiAdHoc, Name %>"></asp:Localize>:
                                                    <asp:TextBox ID="txtCFCalcColumnName" runat="server" />
                                                    <asp:RequiredFieldValidator ID="rfvCFCalcColumnName" runat="server" ControlToValidate="txtCFCalcColumnName"
                                                        EnableClientScript="false" ErrorMessage="Name is required" ValidationGroup="CFCalcColumn"
                                                        meta:resourcekey="rfvCFCalcColumnNameResource1">*</asp:RequiredFieldValidator>
                                                    <br />
                                                    <asp:Label ID="Label11" runat="server" Text="Definition:" AssociatedControlID="txtFormula" meta:resourceKey="locDef"></asp:Label>
                                                    <br />
                                                    <textarea id="txtCFCalcColumnFormula" runat="server" cols="92" rows="5" 
                                                        onclick="storeCaret('txtCFCalcColumnFormula')" onkeyup="storeCaret('txtCFCalcColumnFormula')" >
                                                    </textarea>
                                                    <asp:RequiredFieldValidator ID="rfvCFCalcColumnFormula" runat="server" ControlToValidate="txtCFCalcColumnFormula"
                                                        EnableClientScript="false" ErrorMessage="Formula is required" ValidationGroup="CFCalcColumn"
                                                        meta:resourcekey="rfvCFCalcColumnFormulaResource1">*</asp:RequiredFieldValidator>
                                                    <br />
                                                    <%--<div style="float:right; padding-top:5px;">
                                                        <AdHoc:LogiButton ID="btnTestCalcColumn" runat="server" Text=" Test " 
                                                            OnClick="TestColumn_OnClick" ValidationGroup="Calculation" meta:resourcekey="LogiButton6Resource1" />
                                                    </div>--%>
                                                    <br />
                                                    <br />
                                                    <asp:Localize ID="Localize16" runat="server" Text="Operators:" meta:resourceKey="locOp"></asp:Localize>
                                                    <AdHoc:LogiButton ID="LogiButton2" runat="server" OnClientClick="appendSymbol('+','txtCFCalcColumnFormula');return false;" Text="+" CausesValidation="false" UseSubmitBehavior="False" />
                                                    <AdHoc:LogiButton ID="LogiButton3" runat="server" OnClientClick="appendSymbol('-','txtCFCalcColumnFormula');return false;" Text="-" CausesValidation="false" UseSubmitBehavior="False" />
                                                    <AdHoc:LogiButton ID="LogiButton4" runat="server" OnClientClick="appendSymbol('*','txtCFCalcColumnFormula');return false;" Text="x" CausesValidation="false" UseSubmitBehavior="False" />
                                                    <AdHoc:LogiButton ID="LogiButton5" runat="server" OnClientClick="appendSymbol('/','txtCFCalcColumnFormula');return false;" Text="/" CausesValidation="false" UseSubmitBehavior="False" />
                                                    <AdHoc:LogiButton ID="LogiButton6" runat="server" OnClientClick="appendSymbol('(','txtCFCalcColumnFormula');return false;" Text="(" CausesValidation="false" UseSubmitBehavior="False" />
                                                    <AdHoc:LogiButton ID="LogiButton7" runat="server" OnClientClick="appendSymbol(')','txtCFCalcColumnFormula');return false;" Text=")" CausesValidation="false" UseSubmitBehavior="False" />
                                                    <br />
                                                    <br />
                                                    <%--<table>
                                                         <tr>
                                                            <td colspan="2">
                                                                <br />
                                                                <AdHoc:LogiButton ID="btnSaveCalc" runat="server" Text="Save" OnClick="SaveCalculation" 
                                                                    meta:resourcekey="LogiButton3Resource1" ValidationGroup="Calculation"/>
                                                                <AdHoc:LogiButton ID="btnNewCalc" runat="server" Text=" New " OnClick="NewCalculation" 
                                                                    meta:resourcekey="LogiButton2Resource1" CausesValidation="false" UseSubmitBehavior="false"/>
                                                            </td>
                                                        </tr>
                                                    </table>--%>
                                                </td>
                                            </tr>
                                        </table>
                                        <br />
                                        <asp:ValidationSummary ID="vSummary16" runat="server" ValidationGroup="CFCalcColumn" meta:resourcekey="vsummary16Resource1" />
                                        <asp:Label ID="lblSemicolon16" runat="server" OnPreRender="lblSemicolon16_PreRender" meta:resourcekey="lblSemicolon16Resource1" />
                                        <div id="div7" runat="server">
                                            <table><tr><td>
                                                <AdHoc:LogiButton ID="btnCFCalcColumnDetailsDone" OnClick="CFCalcColumnDetailsDone_OnClick" runat="server" 
                                                    Text="<%$ Resources:LogiAdHoc, OKWithSpaces %>" CausesValidation="True" ValidationGroup="CFCalcColumn" 
                                                    meta:resourcekey="btnCFCalcColumnDetailsDoneResource1" />
                                                <AdHoc:LogiButton ID="btnCFCalcColumnDetailsCancel" OnClick="CFCalcColumnDetailsCancel_OnClick" runat="server"
                                                    Text="<%$ Resources:LogiAdHoc, Cancel %>" CausesValidation="False" meta:resourcekey="btnCFCalcColumnDetailsCancelResource1" />
                                            </td></tr></table>
                                        </div>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </div>
                        </asp:Panel>
                    </div>
                                    
                    <div id="divImageModalPopups">
                        <asp:Button runat="server" ID="btnFakePickFromServer" Style="display: none" meta:resourcekey="btnFakePickFromServerResource1" />
                        <ajaxToolkit:ModalPopupExtender runat="server" ID="mpePickFromServer" BehaviorID="mpePickFromServerBehavior"
                            TargetControlID="btnFakePickFromServer" PopupControlID="pnlPickFromServerPopup" BackgroundCssClass="modalBackground" PopupDragHandleControlID="pnlDragHandle13" RepositionMode="None" DynamicServicePath="" Enabled="True">
                        </ajaxToolkit:ModalPopupExtender>
                        <asp:Panel runat="server" CssClass="modalPopup" ID="pnlPickFromServerPopup" Style="display: none; width: 506;" meta:resourcekey="pnlPickFromServerPopupResource1">
                            <asp:Panel ID="pnlDragHandle13" runat="server" Style="cursor: hand;" meta:resourcekey="pnlDragHandle13Resource1">
                            <div class="modalPopupHandle" style="width: 500px;">
                                <table cellpadding="0" cellspacing="0" style="width: 100%"><tr><td>
                                        <asp:Localize ID="Localize44" runat="server" meta:resourcekey="SelectFileResource" Text="Select an Image File" />
                                    </td>
                                    <td style="width: 20px;">
                                        <asp:ImageButton ID="imgClosePopup13" runat="server" 
                                            OnClick="imgClosePopup13_Click" CausesValidation="False"
                                            SkinID="imgbClose" ImageUrl="../ahImages/remove.gif" 
                                            AlternateText="<%$ Resources:LogiAdHoc, CloseWindow %>" />
                                </td></tr></table>
                            </div>
                            </asp:Panel>
                            <div class="modalDiv">
                                <asp:UpdatePanel ID="UPImgPickFromServer" runat="server" UpdateMode="Conditional">
                                    <ContentTemplate>
                                        <div id="divImgPickFromServer" runat="server">
                                        <input type="hidden" id="SelectedImgURL" runat="server" />
                                        <div id="ServerImagesDiv" runat="server">
                                            <asp:DataList ID="dlImages" runat="server" OnItemCommand="dlImages_ItemCommand" OnItemDataBound="dlImages_ItemDataBound" RepeatColumns="3" meta:resourcekey="dlImagesResource1">
                                                <ItemTemplate>
                                                    <asp:ImageButton ID="imgControl" runat="server" CommandName="Select" meta:resourcekey="imgControlResource1" />
                                                </ItemTemplate>
                                                <ItemStyle BorderColor="Gray" BorderWidth="1px" Height="100px" HorizontalAlign="Center"
                                                    VerticalAlign="Middle" Width="100px" />
                                                <SelectedItemStyle BorderColor="Blue" BorderWidth="2px" />
                                            </asp:DataList>
                                        </div>
                                        <br /><table><tr><td>
                                        <AdHoc:LogiButton ID="btnPFSOK" Text="<%$ Resources:LogiAdHoc, OKWithSpaces %>"
                                                    CausesValidation="False" runat="server" OnClick="btnPickFromServerOK_Click" meta:resourcekey="btnPFSOKResource1" />
                                        <AdHoc:LogiButton ID="btnPFSCancel" Text="<%$ Resources:LogiAdHoc, Cancel %>"
                                                    CausesValidation="False" runat="server" OnClick="btnPickFromServerCancel_Click" meta:resourcekey="btnPFSCancelResource1" />
                                        </td></tr></table>
                                        </div>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </div>
                        </asp:Panel>
                        
                        <asp:Button runat="server" ID="btnFakePickFromClient" Style="display: none" meta:resourcekey="btnFakePickFromClientResource1" />
                        <ajaxToolkit:ModalPopupExtender runat="server" ID="mpePickFromClient" BehaviorID="mpePickFromClientBehavior"
                            TargetControlID="btnFakePickFromClient" PopupControlID="pnlPickFromClientPopup" BackgroundCssClass="modalBackground" 
                            PopupDragHandleControlID="pnlDragHandle14" RepositionMode="None" DynamicServicePath="" Enabled="True">
                        </ajaxToolkit:ModalPopupExtender>
                        <asp:Panel runat="server" CssClass="modalPopup" ID="pnlPickFromClientPopup" Style="display: none; width: 506;" meta:resourcekey="pnlPickFromClientPopupResource1">
                            <asp:Panel ID="pnlDragHandle14" runat="server" Style="cursor: hand;" meta:resourcekey="pnlDragHandle14Resource1">
                            <div class="modalPopupHandle" style="width: 500px;">
                                <table cellpadding="0" cellspacing="0" style="width: 100%"><tr><td>
                                        <asp:Localize ID="Localize149" runat="server" meta:resourcekey="UploadFileResource" Text="Upload an Image File" />
                                    </td>
                                    <td style="width: 20px;">
                                        <asp:ImageButton ID="imgClosePopup14" runat="server" 
                                            OnClick="imgClosePopup14_Click" CausesValidation="False"
                                            SkinID="imgbClose" ImageUrl="../ahImages/remove.gif" 
                                            AlternateText="<%$ Resources:LogiAdHoc, CloseWindow %>" />
                                </td></tr></table>
                            </div>
                            </asp:Panel>
                            <div class="modalDiv">
                                <asp:UpdatePanel ID="UPImgPickFromClient" runat="server" UpdateMode="Conditional">
                                    <ContentTemplate>
                                        <div id="divImgPickFromClient" runat="server">
                                        <asp:Button ID="btnUploaded" style="display:none;" UseSubmitBehavior="False" 
                                            runat="server" OnClick="btnUploaded_Click" meta:resourcekey="btnUploadedResource1" />
                                        <iframe id="ifrmImageUpload" runat="server" src="ImageUpload.aspx" width="470px" height="100px" frameborder="0" >
                                        </iframe>
                                        <div id="divUploadInfo" runat="server" class="info">
                                            <asp:Localize ID="Localize163" runat="server" meta:resourcekey="LiteralResource163"
                                                Text="Image files must be 1 megabyte or smaller and be one of the following file types: JPG, JPEG, GIF, BMP"></asp:Localize>
                                        </div>
                                        </div>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </div>
                        </asp:Panel>
                    </div>
                    
                    <div id="divGroupingModalPopups">
                        <asp:Button runat="server" ID="btnFakeLayerDetails" Style="display: none" meta:resourcekey="btnFakeLayerDetailsResource1" />
                        <ajaxToolkit:ModalPopupExtender runat="server" ID="mpeLayerDetails" BehaviorID="mpeLayerDetailsBehavior"
                            TargetControlID="btnFakeLayerDetails" PopupControlID="pnlLayerDetails" BackgroundCssClass="modalBackground" PopupDragHandleControlID="pnlDragHandle9" RepositionMode="None" DynamicServicePath="" Enabled="True">
                        </ajaxToolkit:ModalPopupExtender>
                        <asp:Panel runat="server" CssClass="modalPopup" ID="pnlLayerDetails" Style="display: none; width: 806;" meta:resourcekey="pnlLayerDetailsResource1">
                            <asp:Panel ID="pnlDragHandle9" runat="server" Style="cursor: hand;" meta:resourcekey="pnlDragHandle9Resource1">
                            <div class="modalPopupHandle" style="width: 800px;">
                                <table cellpadding="0" cellspacing="0" style="width: 100%"><tr>
                                    <td>
                                        <asp:UpdatePanel ID="UPGroupingDetailsHeader" runat="server" UpdateMode="Conditional" RenderMode="Inline">
                                            <ContentTemplate>
                                                <asp:Label ID="lblGroupingLayerDetailsHeader" runat="server" meta:resourcekey="lblGroupingLayerDetailsHeaderResource1" />
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </td>
                                    <td style="width: 35px;">
                                        <asp:ImageButton ID="imgbHelpPopup9" runat="server" OnClientClick="goHelp('13'); return false;" 
                                            CausesValidation="False" SkinID="imgbWinHelp" ImageUrl="../ahImages/iconHelpText.gif" 
                                            ToolTip="Get help with grouping data." meta:resourcekey="imgbHelpPopup9Resource1"
                                            AlternateText="Help" />
                                        <asp:ImageButton ID="imgClosePopup9" runat="server" 
                                            OnClick="imgClosePopup9_Click" CausesValidation="False"
                                            SkinID="imgbClose" ImageUrl="../ahImages/remove.gif" 
                                            AlternateText="<%$ Resources:LogiAdHoc, CloseWindow %>" />
                                </td></tr></table>
                            </div>
                            </asp:Panel>
                            <div id="divGroupingDetailsPopup" runat="server" class="modalDiv">
                                <asp:UpdatePanel ID="UPGroupingDetails" runat="server" UpdateMode="Conditional" RenderMode="Inline">
                                    <ContentTemplate>
                                        <div id="divGroupingDetails" runat="server">
                                            <asp:UpdatePanel ID="UPGroupingLayerDetails" runat="server" UpdateMode="Conditional" RenderMode="Inline">
                                                <ContentTemplate>
                                                    <span style="font-weight:bold;">
                                                        <asp:ImageButton ID="imgOVGroup" runat="server" CausesValidation="False" ImageUrl="../ahImages/expand_blue.jpg"
                                                            OnClientClick="javascript: noMessage = true;" OnCommand="ExpandCollapseOptions"
                                                            ToolTip="<%$ Resources:LogiAdHoc, AdvancedOptionsTooltip %>" AlternateText="<%$ Resources:LogiAdHoc, AdvancedOptionsTooltip %>" />
                                                        &nbsp;
                                                        <asp:Localize ID="lOVGroup" runat="server" Text="<%$ Resources:LogiAdHoc, ShowExtra %>" />
                                                        <br />
                                                        <br />
                                                    </span>
                                                    <table id="MSL" runat="server">
                                                        <tr id="Tr10" runat="server">
                                                            <td id="Td109" colspan="2" runat="server">
                                                            <asp:Localize ID="Localize36" runat="server" meta:resourcekey="LiteralResource35" Text="Available Columns"></asp:Localize></td>
                                                            <td id="Td110" colspan="2" runat="server">
                                                            <asp:Localize ID="Localize73" runat="server" meta:resourcekey="LiteralResource73" Text="Grouped Columns"></asp:Localize>
                                                            </td>
                                                        </tr>
                                                        <tr id="Tr11" runat="server">
                                                            <td id="Td111" runat="server">
                                                                <AdHoc:ScrollableListBox ID="availableGrouping" runat="server" DblClickFunction="__doPostBack('moveright','');"
                                                                    MultipleSelection="True" SelectHeight="210" SelectWidth="300" />
                                                            </td>
                                                            <td id="Td112" runat="server">
                                                                <asp:ImageButton ID="moveright" runat="server" AlternateText="Move Column Right" CausesValidation="False"
                                                                    CssClass="mslbutton" ImageUrl="../ahImages/arrowRight.gif" meta:resourcekey="imgMoveRightResource2"
                                                                    OnClick="MoveRight_Click" ToolTip="Move Column Right" />
                                                                <asp:ImageButton ID="moveleft" runat="server" AlternateText="Move Column Left" CausesValidation="False"
                                                                    CssClass="mslbutton" ImageUrl="../ahImages/arrowLeft.gif" meta:resourcekey="imgMoveLeftResource2"
                                                                    OnClick="MoveLeft_Click" ToolTip="Move Column Left" />
                                                            </td>
                                                            <td id="Td113" runat="server">
                                                                <AdHoc:ScrollableListBox ID="assignedGrouping" runat="server" MultipleSelection="True"
                                                                    SelectHeight="210" SelectWidth="300" />
                                                            </td>
                                                            <td id="Td114" runat="server">
                                                                <asp:ImageButton ID="moveUp" runat="server" CausesValidation="False" CssClass="mslbutton"
                                                                    ImageUrl="../ahImages/arrowUp.gif" meta:resourcekey="imgMoveUpResource2"
                                                                    OnClick="MoveUp_Click" ToolTip="Move Column Up" AlternateText="Move Column Up" />
                                                                <asp:ImageButton ID="moveDown" runat="server" CausesValidation="False" CssClass="mslbutton"
                                                                    ImageUrl="../ahImages/arrowDown.gif" meta:resourcekey="imgMoveDownResource2"
                                                                    OnClick="MoveDown_Click" ToolTip="Move Column Down" AlternateText="Move Column Down" />
                                                            </td>
                                                            <td id="Td115" runat="server">
                                                                <asp:CustomValidator ID="cvGroupingColumns" runat="server" ControlToValidate="SummaryRowCaption" ValidateEmptyText="True" 
                                                                     OnServerValidate="IsGroupingLayerValid" ValidationGroup="GroupingGroup" EnableClientScript="False" 
                                                                     ErrorMessage="At least one column must be selected for grouping." meta:resourcekey="cvGroupingColumnsResource1">*</asp:CustomValidator>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                    <div id="divGroupedColumnsViewOnly" runat="server">
                                                    </div>
                                                    <div id="divInfo" runat="server">
                                                    </div>
                                                    <div id="divGroupMore1" runat="server">
                                                    <h2><asp:Localize ID="Localize74" runat="server" meta:resourcekey="LiteralResource74" Text="Group Aggregates:"></asp:Localize></h2>
                                                    <div id="divAggregateGrid" runat="server" style="height: 130px; overflow: auto;">
                                                        <AdHoc:DDGridView ID="AggregateGrid" runat="server" AutoGenerateColumns="False" OnRowDataBound="OnAggregateItemDataBoundHandler"
                                                             DataKeyNames="ColumnAlias" CssClass="gridWline" meta:resourcekey="AggregateGridResource1">
                                                            <Columns>
                                                                <asp:TemplateField HeaderText="Index" meta:resourcekey="GroupingAggregateIndex">
                                                                    <ItemTemplate>
    <asp:Label runat="server" ID="lblRowIndex" meta:resourceKey="LongLabelResource3"></asp:Label>

                                                                    
    </ItemTemplate>
                                                                    <HeaderStyle Width="50px"></HeaderStyle>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="<%$ Resources:LogiAdHoc, Column %>" meta:resourcekey="TemplateFieldResource18">
                                                                    <ItemTemplate>
    <asp:Label runat="server" AssociatedControlID="NewColumnAlias" Text="<%$ Resources:LogiAdHoc, Column %>" CssClass="NoShow" ID="lblNewColumnAlias" meta:resourcekey="lblNewColumnAliasResource1"></asp:Label>

                                                                        <asp:DropDownList runat="server" AutoPostBack="True" ID="NewColumnAlias" meta:resourceKey="NewColumnAliasResource1" OnSelectedIndexChanged="OnChangeHandler"></asp:DropDownList>

                                                                    
    </ItemTemplate>
                                                                    <HeaderStyle Width="200px"></HeaderStyle>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Aggregate function" meta:resourcekey="GroupingAggregateFunction1">
                                                                    <ItemTemplate>
    <asp:Label runat="server" AssociatedControlID="Aggregation" Text="<%$ Resources:LogiAdHoc, Aggregation %>" CssClass="NoShow" ID="lblAggregation" meta:resourcekey="lblAggregationResource1"></asp:Label>

                                                                        <asp:DropDownList runat="server" ID="Aggregation" meta:resourceKey="AggregationResource2"></asp:DropDownList>

                                                                    
    </ItemTemplate>
                                                                    <HeaderStyle Width="100px"></HeaderStyle>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="<%$ Resources:LogiAdHoc, Header %>" meta:resourcekey="TemplateFieldResource19">
                                                                    <ItemTemplate>
    <asp:Label runat="server" AssociatedControlID="AggregationLabel" Text="<%$ Resources:LogiAdHoc, Header %>" CssClass="NoShow" ID="lblAggregationLabel" meta:resourcekey="lblAggregationLabelResource1"></asp:Label>

                                                                        <asp:TextBox runat="server" ID="AggregationLabel" meta:resourceKey="AggregationLabelResource2"></asp:TextBox>

                                                                    
    </ItemTemplate>
                                                                    <HeaderStyle Width="100px"></HeaderStyle>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Include in sub-report" meta:resourcekey="GroupingIncludeInSubReport">
                                                                    <ItemTemplate>
    <asp:Label runat="server" AssociatedControlID="Detail" Text="Include in sub-report" CssClass="NoShow" ID="lblDetail" meta:resourceKey="lblDetailResource1"></asp:Label>

                                                                        <asp:CheckBox runat="server" ID="Detail" meta:resourceKey="DetailResource2"></asp:CheckBox>

                                                                    
    </ItemTemplate>
                                                                    <HeaderStyle Width="100px"></HeaderStyle>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="<%$ Resources:LogiAdHoc, Actions %>" meta:resourcekey="TemplateFieldResource20">
                                                                    <ItemTemplate>
    <asp:ImageButton runat="server" CommandName="GroupAggregateMoveUp" CausesValidation="False" AlternateText="Move Aggregate Up" ImageUrl="../ahImages/SmallArrowUp.gif" ToolTip="Move Aggregate Up" ID="imgMoveupGroupAggregate" meta:resourceKey="imgMoveupGroupAggregateResource1" OnCommand="AggregateModifications"></asp:ImageButton>

                                                                        <asp:ImageButton runat="server" CommandName="GroupAggregateMoveDown" CausesValidation="False" AlternateText="Move Aggregate Down" ImageUrl="../ahImages/SmallArrowDown.gif" ToolTip="Move Aggregate Down" ID="imgMovedownGroupAggregate" meta:resourceKey="imgMovedownGroupAggregateResource1" OnCommand="AggregateModifications"></asp:ImageButton>

                                                                        &nbsp;
                                                                        <asp:ImageButton runat="server" CommandName="RemoveAggregate" AlternateText="<%$ Resources:LogiAdHoc, RemoveItem %>" ImageUrl="../ahImages/remove.gif" ToolTip="<%$ Resources:LogiAdHoc, RemoveItem %>" ID="RemoveGroupAggregate" OnCommand="AggregateModifications"></asp:ImageButton>

                                                                    
    </ItemTemplate>
                                                                    <HeaderStyle Width="90px" />
                                                                    <ItemStyle HorizontalAlign="Center" Wrap="False" />
                                                                </asp:TemplateField>
                                                            </Columns>
                                                            <HeaderStyle CssClass="gridheader" />
                                                        </AdHoc:DDGridView>
                                                    </div>
                                            <br /><table><tr><td>
                                                    <AdHoc:LogiButton ID="AddAggregate" OnClick="AddAggregate_OnClick" Text="Add an Aggregate Column"
                                                        runat="server" meta:resourcekey="AddAggregateResource1" />
                                            </td></tr></table>
                                                    </div>
                                                </ContentTemplate>
                                            </asp:UpdatePanel>
                                            <asp:UpdatePanel ID="UPGroupMore2" runat="server" UpdateMode="Conditional" RenderMode="Inline">
                                                <ContentTemplate>
                                                    <div id="divGroupMore2" runat="server">
                                                    <div id="divFeatures" runat="server">
                                                        <p>
                                                        </p>
                                                        <h3><asp:Localize ID="Localize17" runat="server" Text="Summary Column Options:" meta:resourcekey="DetailsColumnHeaderResource"></asp:Localize></h3>
                                                        <table>
                                                            <tr>
                                                                <td>
                                                                    <asp:RadioButton ID="NoDetailColumn" runat="server" AutoPostBack="True" GroupName="DrillDown"
                                                                        meta:resourcekey="NoDetailColumnResource1" OnCheckedChanged="DetailColumn_Changed"
                                                                        Text="No summary" />
                                                                </td>
                                                                <td>
                                                                    <asp:RadioButton ID="NoDrillDown" runat="server" AutoPostBack="True" GroupName="DrillDown"
                                                                        meta:resourcekey="NoDrillDownResource1" OnCheckedChanged="DetailColumn_Changed"
                                                                        Text="Auto-generated column (drill-down disabled)" />
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <asp:RadioButton ID="WithAll" runat="server" AutoPostBack="True" Checked="True" GroupName="DrillDown"
                                                                        meta:resourcekey="WithAllResource1" OnCheckedChanged="DetailColumn_Changed" Text="Auto-generated column (drill-down enabled)" />
                                                                </td>
                                                                <td>
                                                                    <div id="divColumnDrillDown" runat="server">
                                                                        <asp:RadioButton ID="ColumnDrillDown" runat="server" AutoPostBack="True" GroupName="DrillDown"
                                                                            meta:resourcekey="ColumnDrillDownResource1" OnCheckedChanged="DetailColumn_Changed"
                                                                            Text="Grouping / Aggregate column (drill-down enabled)" />
                                                                        <asp:Label ID="lblddlColumnDrillDown" runat="server" CssClass="NoShow" Text="<%$ Resources:LogiAdHoc, Column %>" AssociatedControlID="ddlColumnDrillDown" meta:resourcekey="lblddlColumnDrillDownResource1" ></asp:Label>
                                                                        <asp:DropDownList ID="ddlColumnDrillDown" runat="server" meta:resourcekey="ddlColumnDrillDownResource2">
                                                                        </asp:DropDownList>
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                        <p>
                                                        </p>
                                                        <div id="divDrilldownSummaryRow" runat="server" >
                                                            <h3><asp:Localize ID="Localize38" runat="server" Text="Auto-generated summary column settings:" meta:resourcekey="ShowColumnHeaderResource"></asp:Localize></h3>
                                                            <table>
                                                                <tr>
                                                                    <td>
                                                                        <asp:Label ID="Label147" runat="server" meta:resourcekey="DrillDownColumnHeaderResource" Text="Column header (i.e.,  Records vs Details):" AssociatedControlID="DrilldownColHeader"></asp:Label>
                                                                    </td>
                                                                    <td>
                                                                        <input id="DrilldownColHeader" runat="server" type="text" />
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <asp:Label ID="Label148" runat="server" meta:resourcekey="DrillDownSuffixResource" Text="Suffix (i.e., Parts vs Rows):" AssociatedControlID="DrilldownSuffix"></asp:Label>
                                                                    </td>
                                                                    <td>
                                                                        <input id="DrilldownSuffix" runat="server" type="text" />
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <asp:CheckBox ID="IncludeSummaryRow" runat="server" Text="Include summary row for Summary column"
                                                                            meta:resourcekey="IncludeSummaryRowResource1" />
                                                                    </td>
                                                                    <td>
                                                                        &nbsp;&nbsp;&nbsp;&nbsp;
                                                                        <asp:Label ID="Label103" runat="server" Text="<%$ Resources:LogiAdHoc, Caption %>" AssociatedControlID="SummaryRowCaption" meta:resourcekey="Label103Resource1"></asp:LAbel>:
                                                                        <input id="SummaryRowCaption" runat="server" size="50" type="text" />
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </div>
                                                        <p>
                                                        </p>
                                                        <div id="divShowExpandCollapseAllOption" runat="server">
                                                            <table>
                                                                <tr>
                                                                    <td>
                                                                        <asp:Label ID="Label166" runat="server" meta:resourcekey="ShowExpandCollapseAllResource"
                                                                            Text="Show an option to Expand/Collapse all drill-down rows:" AssociatedControlID="chkShowExpandCollapseOption"></asp:Label>
                                                                    </td>
                                                                    <td>
                                                                        <asp:CheckBox ID="chkShowExpandCollapseOption" runat="server" meta:resourcekey="chkShowExpandCollapseOptionResource1" />
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                            <p>
                                                            </p>
                                                        </div>
                                                        <div id="divAutoExpandDrillDownRows" runat="server">
                                                            <table>
                                                                <tr>
                                                                    <td>
                                                                        <asp:Label ID="Label17" runat="server" meta:resourcekey="AutoExpandDrillDownRowsResource"
                                                                            Text="Auto Expand all drill-down rows:" AssociatedControlID="chkAutoExpandDrillDownRows"></asp:Label>
                                                                    </td>
                                                                    <td>
                                                                        <asp:CheckBox ID="chkAutoExpandDrillDownRows" runat="server" meta:resourcekey="chkAutoExpandDrillDownRowsResource1" />
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                            <p>
                                                            </p>
                                                        </div>
                                                    </div>
                                                    <div id="divFlatGroupFeatures" runat="server">
                                                        <p>
                                                        </p>
                                                        <table>
                                                            <tr>
                                                                <td>
                                                                    <asp:Label ID="Label108" runat="server" meta:resourcekey="FlatGroupPageBreakResource"
                                                                        Text="Insert a page break between groups on PDF Export:" AssociatedControlID="chkGroupPageBreak"></asp:Label>
                                                                </td>
                                                                <td>
                                                                    <asp:CheckBox ID="chkGroupPageBreak" runat="server" meta:resourcekey="chkGroupPageBreakResource1" />
                                                                </td>
                                                            </tr>
                                                        </table>
                                                        <p>
                                                        </p>
                                                    </div>
                                                    </div>
                                                </ContentTemplate>
                                            </asp:UpdatePanel>
                                            <!-- Buttons -->
                                            <p></p>
                                            <table>
                                                <tr>
                                                    <td colspan="2">
                                                        <AdHoc:LogiButton ID="SaveLayer" OnClick="SaveLayer_OnClick" runat="server"
                                                            Text="<%$ Resources:LogiAdHoc, OKWithSpaces %>" meta:resourcekey="SaveLayerResource1" />
                                                        <AdHoc:LogiButton ID="CancelLayer" OnClick="CancelLayer_OnClick" runat="server"
                                                            Text="<%$ Resources:LogiAdHoc, Cancel %>" CausesValidation="False" meta:resourcekey="CancelLayerResource1" />
                                                    </td>
                                                </tr>
                                            </table>
                                       
                                            <asp:ValidationSummary ID="ValidationSummary9" runat="server" ValidationGroup="GroupingGroup"
                                                meta:resourcekey="ValidationSummary9Resource1" />
                                            <asp:Label ID="lblSemicolon9" runat="server" OnPreRender="lblSemicolon_PreRender" meta:resourcekey="lblSemicolon9Resource1" />
                                        </div>
                                    </ContentTemplate>
                                </asp:UpdatePanel>    
                            </div>
                        </asp:Panel>
                    </div> 
                    
                    <div id="divCrosstabModalPopups">
                        <asp:Button ID="btnFakeAddLabelColumns" runat="server" Style="display: none" meta:resourcekey="btnFakeAddLabelColumnsResource1" />
                        <ajaxToolkit:ModalPopupExtender ID="mpeCrosstabsAddLabelCols" runat="server" BackgroundCssClass="modalBackground"
                            BehaviorID="mpeCrosstabsAddLabelColsBehavior" PopupControlID="pnlCrosstabsAddLabelCols"
                            TargetControlID="btnFakeAddLabelColumns" PopupDragHandleControlID="pnlDragHandle10" RepositionMode="None" DynamicServicePath="" Enabled="True">
                        </ajaxToolkit:ModalPopupExtender>
                        <asp:Panel ID="pnlCrosstabsAddLabelCols" runat="server" CssClass="modalPopup" Style="display: none;
                            width: 506;" meta:resourcekey="pnlCrosstabsAddLabelColsResource1">
                            <asp:Panel ID="pnlDragHandle10" runat="server" Style="cursor: hand;" meta:resourcekey="pnlDragHandle10Resource1">
                            <div class="modalPopupHandle" style="width: 500px;">
                                <table cellpadding="0" cellspacing="0" style="width: 100%"><tr><td>
                                        <asp:Localize ID="Localize191" runat="server" meta:resourcekey="CrosstabLabelResource"
                                            Text="Crosstab Label Column"></asp:Localize>
                                    </td>
                                    <td style="width: 20px;">
                                        <asp:ImageButton ID="imgClosePopup10" runat="server" 
                                            OnClick="imgClosePopup10_Click" CausesValidation="False"
                                            SkinID="imgbClose" ImageUrl="../ahImages/remove.gif" 
                                            AlternateText="<%$ Resources:LogiAdHoc, CloseWindow %>" />
                                </td></tr></table>
                            </div>
                            </asp:Panel>
                            <div class="modalDiv">
                                <asp:UpdatePanel ID="UPCTLabelColumns" runat="server" UpdateMode="Conditional" RenderMode="Inline">
                                    <ContentTemplate>
                                        <div id="divCTLabelColumns" runat="server">
                                        <input id="hdCrosstabLabelColumnID" runat="server" type="hidden" />
                                        <div id="divCrosstabLabelColumnSettings" runat="server">
                                            <table>
                                                <tr>
                                                    <td  width="135px">
                                                        <asp:Label ID="Label192" runat="server" Text="<%$ Resources:LogiAdHoc, Column %>"
                                                             AssociatedControlID="LabelColumn1" meta:resourcekey="Label192Resource1"></asp:Label>:
                                                    </td>
                                                    <td width="100">
                                                        <asp:DropDownList ID="LabelColumn1" AutoPostBack="True" runat="server" meta:resourcekey="LabelColumnResource1" />
                                                        <asp:Label ID="lblLabelColumn" runat="server" meta:resourcekey="lblLabelColumnResource1" />
                                                    </td>
                                                    <%--<td id="tdCTLabelTimePeriod1" runat="server" style="padding: 0;">
                                                        <table>
                                                            <tr>
                                                                <td width="135px">
                                                                   <asp:Label ID="Label12" runat="server" Text="Apply Time Period:" AssociatedControlID="ddlCTLabelTPC1" meta:resourcekey="Label5Resource1"></asp:Label>
                                                                </td>
                                                                <td width="100">
                                                                    <asp:DropDownList ID="ddlCTLabelTPC1" runat="server" AutoPostBack="True" meta:resourcekey="ddlCTLabelTPC1Resource1" >
                                                                        <asp:ListItem Text="<%$ Resources:LogiAdHoc, TimePeriodNone %>" Value="" />
                                                                        <asp:ListItem Text="<%$ Resources:LogiAdHoc, TimePeriodYear %>" Value="Year" />
                                                                        <asp:ListItem Text="<%$ Resources:LogiAdHoc, TimePeriodQuarter %>" Value="FirstDayOfQuarter" />
                                                                        <asp:ListItem Text="<%$ Resources:LogiAdHoc, TimePeriodFiscalQuarter %>" Value="FirstDayOfFiscalQuarter" />
                                                                        <asp:ListItem Text="<%$ Resources:LogiAdHoc, TimePeriodMonth %>" Value="FirstDayOfMonth" />
                                                                    </asp:DropDownList>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>--%>
                                                </tr>
                                                <tr>
                                                    <td width="135px">
                                                        <asp:Label ID="Label193" runat="server" meta:resourcekey="LiteralResource189" 
                                                            Text="Header:" AssociatedControlID="txtCrosstabLabelHeader1"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:TextBox ID="txtCrosstabLabelHeader1" runat="server" Width="200px" meta:resourcekey="txtCrosstabLabelHeader1Resource1"></asp:TextBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td width="135px">
                                                        <asp:Label ID="Label194" runat="server" meta:resourcekey="LiteralResource143" 
                                                            Text="Sortable:" AssociatedControlID="chkLabelColumnSort1"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:CheckBox ID="chkLabelColumnSort1" runat="server" meta:resourcekey="chkLabelColumnSort1Resource1" />
                                                    </td>
                                                </tr>
                                                <tr id="trCrosstabLabelColLinked1" runat="server">
                                                    <td id="Td116" width="135px" runat="server">
                                                        <asp:Label ID="Label195" runat="server" meta:resourcekey="LiteralResource190" 
                                                            Text="Linked:" AssociatedControlID="chkCrosstabLabelColLinked1"></asp:Label>
                                                    </td>
                                                    <td id="Td117" runat="server">
                                                        <asp:CheckBox ID="chkCrosstabLabelColLinked1" AutoPostBack="True" OnCheckedChanged="Crosstab_Changed" runat="server" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td><asp:Label ID="Label196" runat="server" meta:resourcekey="LiteralResource144" 
                                                            Text="Format:" AssociatedControlID="ddlLabelColumnFormat1"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:DropDownList ID="ddlLabelColumnFormat1" runat="server" meta:resourcekey="ddlLabelColumnFormat1Resource1" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td><asp:Label ID="Label197" runat="server" meta:resourcekey="LiteralResource146" 
                                                            Text="Alignment:" AssociatedControlID="ddlLabelColAlignment1"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:DropDownList ID="ddlLabelColAlignment1" runat="server" meta:resourcekey="alignmentResource1">
                                                            <asp:ListItem Value="Left" meta:resourcekey="ListItemResource6">Left</asp:ListItem>
                                                            <asp:ListItem Value="Right" meta:resourcekey="ListItemResource7">Right</asp:ListItem>
                                                            <asp:ListItem Value="Center" meta:resourcekey="ListItemResource8">Center</asp:ListItem>
                                                        </asp:DropDownList>
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                        <br /><table><tr><td>
                                        <AdHoc:LogiButton ID="btnAddCrosstabLabelColumnOK" runat="server" CausesValidation="False"
                                            OnClick="btnAddCrosstabLabelColumnOK_Click" Text="<%$ Resources:LogiAdHoc, OKWithSpaces %>" meta:resourcekey="btnAddCrosstabLabelColumnOKResource1" />
                                        <AdHoc:LogiButton ID="btnAddCrosstabLabelColumnCancel" runat="server" CausesValidation="False"
                                            OnClick="btnAddCrosstabLabelColumnCancel_Click" Text="<%$ Resources:LogiAdHoc, Cancel %>" meta:resourcekey="btnAddCrosstabLabelColumnCancelResource1" />
                                        </td></tr></table>
                                        </div>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </div>
                        </asp:Panel>
                        
                        <asp:Button ID="btnFakeAddValueColumns" runat="server" Style="display: none" meta:resourcekey="btnFakeAddValueColumnsResource1" />
                        <ajaxToolkit:ModalPopupExtender ID="mpeCrosstabsAddValueCols" runat="server" BackgroundCssClass="modalBackground"
                            BehaviorID="mpeCrosstabsAddValueColsBehavior" PopupControlID="pnlCrosstabsAddValueCols"
                            TargetControlID="btnFakeAddValueColumns" PopupDragHandleControlID="pnlDragHandle11" RepositionMode="None" DynamicServicePath="" Enabled="True">
                        </ajaxToolkit:ModalPopupExtender>
                        <asp:Panel ID="pnlCrosstabsAddValueCols" runat="server" CssClass="modalPopup" Style="display: none;
                            width: 506;" meta:resourcekey="pnlCrosstabsAddValueColsResource1">
                            <asp:Panel ID="pnlDragHandle11" runat="server" Style="cursor: hand;" meta:resourcekey="pnlDragHandle11Resource1">
                            <div class="modalPopupHandle" style="width: 500px;">
                                <table cellpadding="0" cellspacing="0" style="width: 100%"><tr><td>
                                        <asp:Localize ID="Localize168" runat="server" meta:resourcekey="CrosstabValueResource"
                                            Text="Crosstab Value Column"></asp:Localize>
                                    </td>
                                    <td style="width: 20px;">
                                        <asp:ImageButton ID="imgClosePopup11" runat="server" 
                                            OnClick="imgClosePopup11_Click" CausesValidation="False"
                                            SkinID="imgbClose" ImageUrl="../ahImages/remove.gif" 
                                            AlternateText="<%$ Resources:LogiAdHoc, CloseWindow %>" />
                                </td></tr></table>
                            </div>
                            </asp:Panel>
                            <div class="modalDiv">
                                <asp:UpdatePanel ID="UPCTValueColumns" runat="server" UpdateMode="Conditional">
                                    <ContentTemplate>
                                        <div id="divCTValueColumns" runat="server">
                                        <input id="hdCrosstabValueColumnID" runat="server" type="hidden" />
                                        <input id="hdCrosstabPopupType" runat="server" type="hidden" />
                                        <div id="divCrosstabValueColumnSettings" runat="server">
                                            <table>
                                                <tr>
                                                    <td style="width: 135px">
                                                        <asp:Label ID="Label169" runat="server" Text="<%$ Resources:LogiAdHoc, Column %>"
                                                             AssociatedControlID="ValueColumn1" meta:resourcekey="Label169Resource1"></asp:Label>:
                                                    </td>
                                                    <td width="100">
                                                        <asp:DropDownList ID="ValueColumn1" runat="server" AutoPostBack="True" meta:resourcekey="ValueColumnResource1">
                                                        </asp:DropDownList>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="width: 135px">
                                                        <asp:Label ID="Label170" runat="server" meta:resourcekey="LiteralResource86"
                                                            Text="Aggregate Function:" AssociatedControlID="AggregateFunction1"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:DropDownList ID="AggregateFunction1" runat="server" AutoPostBack="True" meta:resourcekey="AggregateFunctionResource1">
                                                            <asp:ListItem Value="Sum" Text="<%$ Resources:LogiAdHoc, Sum %>"></asp:ListItem>
                                                            <asp:ListItem Value="Average" Text="<%$ Resources:LogiAdHoc, Average %>"></asp:ListItem>
                                                            <asp:ListItem Value="STDEV" Text="<%$ Resources:LogiAdHoc, StandardDeviation %>"></asp:ListItem>
                                                            <asp:ListItem Value="Count" Text="<%$ Resources:LogiAdHoc, Count %>"></asp:ListItem>
                                                        </asp:DropDownList>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="width: 135px">
                                                        <asp:Label ID="Label174" runat="server" Text="<%$ Resources:LogiAdHoc, Header %>"
                                                             AssociatedControlID="txtCrosstabValueColumnHeader" meta:resourcekey="Label174Resource1"></asp:Label>:
                                                    </td>
                                                    <td width="100">
                                                        <asp:UpdatePanel ID="UpdatePanel26" runat="server" UpdateMode="Conditional">
                                                            <ContentTemplate>
                                                                <asp:TextBox ID="txtCrosstabValueColumnHeader" runat="server" meta:resourcekey="txtCrosstabValueColumnHeaderResource1"></asp:TextBox>
                                                            </ContentTemplate>
                                                            <Triggers>
                                                                <asp:AsyncPostBackTrigger ControlID="ValueColumn1" EventName="SelectedIndexChanged" />
                                                                <asp:AsyncPostBackTrigger ControlID="AggregateFunction1" EventName="SelectedIndexChanged" />
                                                            </Triggers>
                                                        </asp:UpdatePanel>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="width: 135px">
                                                        <asp:Label ID="Label171" runat="server" meta:resourcekey="LiteralResource143"
                                                            Text="Sortable:" AssociatedControlID="chkValueColumnSort1"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:CheckBox ID="chkValueColumnSort1" runat="server" meta:resourcekey="chkValueColumnSort1Resource1" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="width: 135px">
                                                        <asp:Label ID="Label172" runat="server" meta:resourcekey="LiteralResource144"
                                                            Text="Format:" AssociatedControlID="ddlValueColumnFormat1"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:UpdatePanel ID="UpdatePanel21" runat="server" UpdateMode="Conditional">
                                                            <ContentTemplate>
                                                                <asp:DropDownList ID="ddlValueColumnFormat1" runat="server" meta:resourcekey="ddlValueColumnFormat1Resource1">
                                                                </asp:DropDownList>
                                                            </ContentTemplate>
                                                            <Triggers>
                                                                <asp:AsyncPostBackTrigger ControlID="ValueColumn1" EventName="SelectedIndexChanged" />
                                                            </Triggers>
                                                        </asp:UpdatePanel>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="width: 135px">
                                                        <asp:Label ID="Label173" runat="server" meta:resourcekey="LiteralResource146"
                                                            Text="Alignment:" AssociatedControlID="ddlValuesColAlignment1"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:UpdatePanel ID="UpdatePanel22" runat="server" UpdateMode="Conditional">
                                                            <ContentTemplate>
                                                                <asp:DropDownList ID="ddlValuesColAlignment1" runat="server" meta:resourcekey="alignmentResource1">
                                                                    <asp:ListItem meta:resourcekey="ListItemResource6" Value="Left">Left</asp:ListItem>
                                                                    <asp:ListItem meta:resourcekey="ListItemResource7" Value="Right">Right</asp:ListItem>
                                                                    <asp:ListItem meta:resourcekey="ListItemResource8" Value="Center">Center</asp:ListItem>
                                                                </asp:DropDownList>
                                                            </ContentTemplate>
                                                            <Triggers>
                                                                <asp:AsyncPostBackTrigger ControlID="ValueColumn1" EventName="SelectedIndexChanged" />
                                                            </Triggers>
                                                        </asp:UpdatePanel>
                                                    </td>
                                                </tr>
                                                
                                            </table>
                                        </div>
                                        <div id="divCrosstabValueColumnSummaryRow" runat="server">
                                            <table>
                                                <tr>
                                                    <td width="135px">
                                                       <asp:Label ID="Label175" runat="server" meta:resourcekey="LiteralResource87"
                                                            Text="Include:" AssociatedControlID="chkSummaryRow1"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:CheckBox ID="chkSummaryRow1" runat="server" AutoPostBack="True" meta:resourcekey="chkSummaryRowResource1"
                                                            OnCheckedChanged="SummaryRow_Changed1" />
                                                    </td>
                                                </tr>
                                                <tr id="trSumRowCaption1" runat="server">
                                                    <td id="Td118" runat="server">
                                                        <asp:Label ID="Label176" runat="server" meta:resourcekey="LiteralResource88"
                                                            Text="Caption:" AssociatedControlID="txtSumRowCaption1"></asp:Label>
                                                    </td>
                                                    <td id="Td119" runat="server">
                                                        <asp:TextBox ID="txtSumRowCaption1" runat="server" meta:resourcekey="SummaryResource1"
                                                            Text="Summary"></asp:TextBox>
                                                    </td>
                                                </tr>
                                                <tr id="trSumRowFunc1" runat="server">
                                                    <td id="Td120" runat="server">
                                                        <asp:Label ID="Label177" runat="server" meta:resourcekey="LiteralResource89"
                                                            Text="Aggregate Function:" AssociatedControlID="ddlSumRowFunc1"></asp:Label>
                                                    </td>
                                                    <td id="Td121" runat="server">
                                                        <asp:DropDownList ID="ddlSumRowFunc1" runat="server">
                                                            <asp:ListItem Value="Sum" Text="<%$ Resources:LogiAdHoc, Sum %>" />
                                                            <asp:ListItem Value="Average" Text="<%$ Resources:LogiAdHoc, Average %>" />
                                                            <asp:ListItem Value="STDEV" Text="<%$ Resources:LogiAdHoc, StandardDeviation %>" />
                                                            <asp:ListItem Value="Count" Text="<%$ Resources:LogiAdHoc, Count %>" />
                                                            <asp:ListItem Value="DistinctCount" Text="<%$ Resources:LogiAdHoc, DistinctCount %>" />
                                                            <asp:ListItem Value="AverageOfAllRows" Text="<%$ Resources:LogiAdHoc, AverageOfAllRows %>" />
                                                            <asp:ListItem Value="CountOfAllRows" Text="<%$ Resources:LogiAdHoc, CountOfAllRows %>" />
                                                        </asp:DropDownList>
                                                    </td>
                                                </tr>
                                                <tr id="trSumRowFormat1" runat="server">
                                                    <td id="Td122" runat="server">
                                                        <asp:Label ID="Label178" runat="server" meta:resourcekey="LiteralResource144"
                                                            Text="Format:" AssociatedControlID="ddlSumRowFormat1"></asp:Label>
                                                    </td>
                                                    <td id="Td123" runat="server">
                                                        <asp:DropDownList ID="ddlSumRowFormat1" runat="server" >
                                                        </asp:DropDownList>
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                        <div id="divCrosstabValueColumnSummaryColumn" runat="server">
                                            <table>
                                                <tr>
                                                    <td width="135px">
                                                        <asp:Label ID="Label179" runat="server" meta:resourcekey="LiteralResource90"
                                                            Text="Include:" AssociatedControlID="chkSummaryColumn1"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:CheckBox ID="chkSummaryColumn1" runat="server" AutoPostBack="True" meta:resourcekey="chkSummaryColumnResource1"
                                                            OnCheckedChanged="SummaryColumn_Changed1" />
                                                    </td>
                                                </tr>
                                                <tr id="trSumColHeader1" runat="server">
                                                    <td id="Td124" runat="server">
                                                        <asp:Label ID="Label180" runat="server" meta:resourcekey="LiteralResource91"
                                                            Text="Header:" AssociatedControlID="txtSumColHeader1"></asp:Label>
                                                    </td>
                                                    <td id="Td125" runat="server">
                                                        <asp:TextBox ID="txtSumColHeader1" runat="server" meta:resourcekey="SummaryResource1"
                                                            Text="Summary"></asp:TextBox>
                                                    </td>
                                                </tr>
                                                <tr id="trSumColFunc1" runat="server">
                                                    <td id="Td126" runat="server">
                                                        <asp:Label ID="Label181" runat="server" meta:resourcekey="LiteralResource92"
                                                            Text="Aggregate Function:" AssociatedControlID="ddlSumColFunc1"></asp:Label>
                                                    </td>
                                                    <td id="Td127" runat="server">
                                                        <asp:DropDownList ID="ddlSumColFunc1" runat="server" >
                                                            <asp:ListItem Value="Sum" Text="<%$ Resources:LogiAdHoc, Sum %>" />
                                                            <asp:ListItem Value="Average" Text="<%$ Resources:LogiAdHoc, Average %>" />
                                                            <asp:ListItem Value="STDEV" Text="<%$ Resources:LogiAdHoc, StandardDeviation %>" />
                                                            <asp:ListItem Value="Count" Text="<%$ Resources:LogiAdHoc, Count %>" />
                                                            <asp:ListItem Value="AverageOfAllRows" Text="<%$ Resources:LogiAdHoc, AverageOfAllRows %>" />
                                                            <asp:ListItem Value="CountOfAllRows" Text="<%$ Resources:LogiAdHoc, CountOfAllRows %>" />
                                                        </asp:DropDownList>
                                                    </td>
                                                </tr>
                                                <tr id="trSumColFormat1" runat="server">
                                                    <td id="Td128" runat="server">
                                                        <asp:Label ID="Label182" runat="server" meta:resourcekey="LiteralResource144"
                                                            Text="Format:" AssociatedControlID="ddlSumColFormat1"></asp:Label>
                                                    </td>
                                                    <td id="Td129" runat="server">
                                                        <asp:DropDownList ID="ddlSumColFormat1" runat="server">
                                                        </asp:DropDownList>
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                        <br /><table><tr><td>
                                        <AdHoc:LogiButton ID="btnAddCrosstabValueColumnOK" runat="server" CausesValidation="False"
                                            OnClick="btnAddCrosstabValueColumnOK_Click" Text="<%$ Resources:LogiAdHoc, OKWithSpaces %>" meta:resourcekey="btnAddCrosstabValueColumnOKResource1" />
                                        <AdHoc:LogiButton ID="btnAddCrosstabValueColumnCancel" runat="server" CausesValidation="False"
                                            OnClick="btnAddCrosstabValueColumnCancel_Click" Text="<%$ Resources:LogiAdHoc, Cancel %>" meta:resourcekey="btnAddCrosstabValueColumnCancelResource1" />
                                        </td></tr></table>
                                        </div>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </div>
                        </asp:Panel>
                        
                        <asp:Button ID="btnFakeCTStyle" runat="server" Style="display: none" meta:resourcekey="btnFakeCTStyleResource1" />
                        <ajaxToolkit:ModalPopupExtender ID="mpeCTStyle" runat="server" BackgroundCssClass="modalBackground"
                            BehaviorID="mpeCTStyleBehavior" PopupControlID="pnlCTStyle"
                            TargetControlID="btnFakeCTStyle" PopupDragHandleControlID="pnlDragHandle12" RepositionMode="None" DynamicServicePath="" Enabled="True">
                        </ajaxToolkit:ModalPopupExtender>
                        <asp:Panel ID="pnlCTStyle" runat="server" CssClass="modalPopup" Style="display: none; width: 656px;" meta:resourcekey="pnlCTStyleResource1">
                            <asp:Panel ID="pnlDragHandle12" runat="server" Style="cursor: hand;" meta:resourcekey="pnlDragHandle12Resource1">
                            <div class="modalPopupHandle" style="width: 650px;">
                                <table cellpadding="0" cellspacing="0" style="width: 100%"><tr><td>
                                        <asp:Localize ID="Localize184" runat="server" meta:resourcekey="CrosstabStyleResource"
                                            Text="Crosstab Value Column Style"></asp:Localize>
                                    </td>
                                    <td style="width: 20px;">
                                        <asp:ImageButton ID="imgClosePopup12" runat="server" 
                                            OnClick="imgClosePopup12_Click" CausesValidation="False"
                                            SkinID="imgbClose" ImageUrl="../ahImages/remove.gif" 
                                            AlternateText="<%$ Resources:LogiAdHoc, CloseWindow %>" />
                                </td></tr></table>
                            </div>
                            </asp:Panel>
                            <div class="modalDiv">
                                <asp:UpdatePanel ID="UPCTStyleValueColumns" runat="server" UpdateMode="Conditional">
                                    <ContentTemplate>
                                        <div id="divCTStyleValueColumns" runat="server">
                                        <input id="hdCTStyleValueColumnID" runat="server" type="hidden" />
                                        
                                        <asp:UpdatePanel ID="UPCTPresentation" runat="server">
                                            <ContentTemplate>
                                                <div id="divCTConditionGrid" runat="server" style="height: 130px; overflow: auto;">
                                                    <asp:Button ID="btnFakeGrdCTConditionReorderRows" runat="server" CssClass="NoShow" 
                                                        OnClick="btnFakeGrdCTConditionReorderRows_Click" meta:resourcekey="btnFakeGrdCTConditionReorderRowsResource1" />
                                                    <AdHoc:DDGridView ID="grdCTCondition" runat="server" AutoGenerateColumns="False" CssClass="gridWline"
                                                         OnRowDataBound="OnCTCondParamItemDataBoundHandler" DDDivID="divCTCDragHandle"
                                                         PostbackButtonID="btnFakeGrdCTConditionReorderRows" meta:resourcekey="grdCTConditionResource1">
                                                        <Columns>
                                                            <asp:TemplateField HeaderText="<%$ Resources:LogiAdHoc, Column %>" meta:resourcekey="TemplateFieldResource21">
                                                                <ItemTemplate>
<input runat="server" ID="RowOrder" type="hidden" />

                                                                    <table cellpadding="0" cellspacing="0">
                                                                        <tr>
                                                                            <td>
                                                                                <div runat="server" ID="divCTCDragHandle" class="dragHandle"></div>

                                                                            </td>
                                                                            <td>
                                                                                <input runat="server" ID="CTCondParamKey" type="hidden" />

                                                                                <asp:Label runat="server" ID="lblColumnName" meta:resourcekey="lblColumnNameResource1"></asp:Label>

                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                
</ItemTemplate>
                                                                <HeaderStyle Width="120px" />
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="<%$ Resources:LogiAdHoc, Op_Operator %>" meta:resourcekey="TemplateFieldResource22">
                                                                <ItemTemplate>
<asp:Label runat="server" ID="lblCTCondOperator" meta:resourcekey="lblCTCondOperatorResource1"></asp:Label>

                                                                
</ItemTemplate>
                                                                <HeaderStyle Width="50px" />
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="<%$ Resources:LogiAdHoc, Value %>" meta:resourcekey="TemplateFieldResource23">
                                                                <ItemTemplate>
<asp:Label runat="server" ID="lblCTCondParamValue" meta:resourcekey="lblCTCondParamValueResource1"></asp:Label>

                                                                
</ItemTemplate>
                                                                <HeaderStyle Width="150px" />
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="<%$ Resources:LogiAdHoc, Style %>" meta:resourcekey="TemplateFieldResource24">
                                                                <ItemTemplate>
<asp:Label runat="server" ID="lblCTClassName" meta:resourcekey="lblCTClassNameResource1"></asp:Label>

                                                                
</ItemTemplate>
                                                                <HeaderStyle Width="100px" />
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="<%$ Resources:LogiAdHoc, Actions %>" meta:resourcekey="TemplateFieldResource25">
                                                                <ItemTemplate>
<asp:ImageButton runat="server" CommandName="ConditionParamMoveUp" CausesValidation="False" AlternateText="Move Condition Up" ImageUrl="../ahImages/SmallArrowUp.gif" ID="imgCTMoveupOrder" meta:resourceKey="imgMoveupCTCondResource1" OnCommand="CTCondParamModifications"></asp:ImageButton>

                                                                    <asp:ImageButton runat="server" CommandName="ConditionParamMoveDown" CausesValidation="False" AlternateText="Move Condition Down" ImageUrl="../ahImages/SmallArrowDown.gif" ID="imgCTMovedownOrder" meta:resourceKey="imgMovedownCTCondResource1" OnCommand="CTCondParamModifications"></asp:ImageButton>

                                                                    &nbsp;
                                                                    <asp:ImageButton runat="server" CommandName="ModifyParam" AlternateText="Modify Condition" ImageUrl="../ahImages/modify.gif" ID="ModifyCTParam" meta:resourceKey="ModifyCondParamResource1" OnCommand="CTCondParamModifications"></asp:ImageButton>

                                                                    <asp:ImageButton runat="server" CommandName="RemoveParam" AlternateText="Remove Condition" ImageUrl="../ahImages/remove.gif" ID="RemoveCTParam" meta:resourceKey="RemoveCondParamResource1" OnCommand="CTCondParamModifications"></asp:ImageButton>

                                                                
</ItemTemplate>
                                                                <HeaderStyle Width="90px" />
                                                                <ItemStyle HorizontalAlign="Center" Wrap="False" />
                                                            </asp:TemplateField>
                                                        </Columns>
                                                        <HeaderStyle CssClass="gridheader" />
                                                    </AdHoc:DDGridView>
                                                </div>
                                        <br /><table><tr><td>
                                                <AdHoc:LogiButton ID="btnAddCTCondition" runat="server" meta:resourcekey="AddConditionResource1"
                                                    OnClick="AddCTCondParam_OnClick" Text="Add a Condition" />
                                        </td></tr></table>
                                                    
                                                <asp:Panel ID="pnlCTCondStyleDetails" runat="server" CssClass="detailpanel" meta:resourcekey="pnlCTCondStyleDetailsResource1">
                                                    <asp:UpdatePanel ID="UpdatePanel25" runat="server" UpdateMode="Conditional">
                                                        <ContentTemplate>
                                                            <input id="hdCTCondParamKey" runat="server" type="hidden" />
                                                            <table border="0" cellpadding="1" cellspacing="0">
                                                                <tr>
                                                                    <td valign="top" width="100px">
                                                                        <asp:Label ID="Label185" runat="server" Text="<%$ Resources:LogiAdHoc, Column %>"
                                                                             AssociatedControlID="ddlCTStyleColumnKey" meta:resourcekey="Label185Resource1"></asp:Label>:
                                                                    </td>
                                                                    <td>
                                                                        <asp:DropDownList ID="ddlCTStyleColumnKey" runat="server" AutoPostBack="True" meta:resourcekey="ColumnIDResource1"
                                                                            OnSelectedIndexChanged="CTStyleColumnID_OnChangeHandler">
                                                                        </asp:DropDownList>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td valign="top" width="100px">
                                                                        <asp:Label ID="Label186" runat="server" Text="<%$ Resources:LogiAdHoc, Op_Operator %>"
                                                                             AssociatedControlID="ddlCTStyleOperator" meta:resourcekey="Label186Resource1"></asp:Label>:
                                                                    </td>
                                                                    <td valign="top">
                                                                        <asp:DropDownList ID="ddlCTStyleOperator" runat="server" AutoPostBack="True" meta:resourcekey="ddlOperatorResource1"
                                                                            OnSelectedIndexChanged="CTStyleOperator_OnChangeHandler">
                                                                        </asp:DropDownList>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                            <div id="divCTStyleValues" runat="server">
                                                                <table border="0" cellpadding="1" cellspacing="0">
                                                                    <tr>
                                                                        <td id="tdCTStyleValuesLabel" runat="server" valign="top" width="100px">
                                                                            <asp:Localize ID="Localize187" runat="server" Text="<%$ Resources:LogiAdHoc, Value %>"></asp:Localize>:
                                                                        </td>
                                                                        <td valign="top">
                                                                            <span id="spnCTStyle1" runat="server">
                                                                                <wizard:SpecialValue ID="svCTStyle1" runat="server" />
                                                                            </span>
                                                                            <asp:Label ID="lblCTStyleAnd" runat="server" Text="<%$ Resources:LogiAdHoc, Res_And %>" meta:resourcekey="lblCTStyleAndResource1"></asp:Label>
                                                                            <div id="spnCTStyle2" runat="server">
                                                                                <wizard:SpecialValue ID="svCTStyle2" runat="server" />
                                                                            </div>
                                                                            <asp:Label ID="lblchkCTStyleBit" runat="server" CssClass="NoShow" Text="<%$ Resources:LogiAdHoc, BitValue %>" AssociatedControlID="chkCTStyleBit" meta:resourcekey="lblchkCTStyleBitResource1" ></asp:Label>
                                                                            <asp:CheckBox ID="chkCTStyleBit" runat="server" meta:resourcekey="chkBitValueResource1" />
                                                                            <asp:Label ID="lblfakeCTStyleColumnID" runat="server" CssClass="NoShow" Text="<%$ Resources:LogiAdHoc, Column %>" AssociatedControlID="fakeCTStyleColumnID" meta:resourcekey="lblfakeCTStyleColumnIDResource1" ></asp:Label>
                                                                            <asp:TextBox ID="fakeCTStyleColumnID" runat="server" Style="display: none;" meta:resourcekey="fakeCTStyleColumnIDResource2"></asp:TextBox>
                                                                            <asp:CustomValidator ID="cvCTStyleParamValid" runat="server" ControlToValidate="fakeCTStyleColumnID"
                                                                                ErrorMessage="This parameter leads to an invalid query." meta:resourcekey="cvValidParamResource1"
                                                                                OnServerValidate="IsCTStyleParameterValid" ValidateEmptyText="True" ValidationGroup="CTStyleGroup">*</asp:CustomValidator>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </div>
                                                        </ContentTemplate>
                                                    </asp:UpdatePanel>
                                                    <table border="0" cellpadding="1" cellspacing="0">
                                                        <tr>
                                                            <td valign="top" width="100px">
                                                                <asp:Label ID="Label188" runat="server" Text="<%$ Resources:LogiAdHoc, Style %>"
                                                                     AssociatedControlID="CTConditionalStyleClass" meta:resourcekey="Label188Resource1"></asp:Label>:
                                                            </td>
                                                            <td>
                                                                <asp:DropDownList ID="CTConditionalStyleClass" runat="server" meta:resourcekey="CTConditionalStyleClassResource1">
                                                                </asp:DropDownList>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                    <!-- Buttons -->
                                                    <table>
                                                        <tr>
                                                            <td colspan="2">
                                                                <AdHoc:LogiButton ID="btnCTStyleOK" runat="server" OnClick="SaveCTStyle_OnClick"
                                                                    Text="<%$ Resources:LogiAdHoc, OKWithSpaces %>" ValidationGroup="CTStyleGroup" meta:resourcekey="btnCTStyleOKResource1" />
                                                                <AdHoc:LogiButton ID="btnCTStyleCancel" runat="server" CausesValidation="False"
                                                                    OnClick="CTCancelStyle_OnClick" Text="<%$ Resources:LogiAdHoc, Cancel %>" meta:resourcekey="btnCTStyleCancelResource1" />
                                                            </td>
                                                            
                                                        </tr>
                                                    </table>
                                                    <asp:ValidationSummary ID="ValidationSummary2" runat="server" meta:resourcekey="ValidationSummary3Resource1"
                                                        ValidationGroup="CTStyleGroup" />
                                                    <asp:Label ID="lblSemicolon15" runat="server" OnPreRender="lblSemicolon_PreRender" meta:resourcekey="lblSemicolon15Resource1"></asp:Label>
                                                    <p id="pCTCondStyleValidationDone" runat="server">
                                                        <asp:Label ID="lblCTCondStyleValidationDone" runat="server" meta:resourcekey="lblCTCondStyleValidationDoneResource1"></asp:Label>
                                                    </p>
                                                </asp:Panel>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                        
                                        <br /><table><tr><td>
                                        <AdHoc:LogiButton ID="btnCrosstabStyleOK" runat="server" CausesValidation="False"
                                            OnClick="btnCrosstabStyleOK_Click" Text="<%$ Resources:LogiAdHoc, OKWithSpaces %>" meta:resourcekey="btnCrosstabStyleOKResource1" />
                                        <AdHoc:LogiButton ID="btnCrosstabStyleCancel" runat="server" CausesValidation="False"
                                            OnClick="btnCrosstabStyleCancel_Click" Text="<%$ Resources:LogiAdHoc, Cancel %>" meta:resourcekey="btnCrosstabStyleCancelResource1" />
                                        </td></tr></table>
                                        </div>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </div>
                        </asp:Panel>                  
                                            
                    </div>
                    
                    <div id="divChartModalPopups">
                        <asp:Panel ID="pnlTest" runat="server">
                        </asp:Panel>
                        <asp:Button ID="btnFakeCSModifyChartType" runat="server" Style="display: none" meta:resourcekey="btnFakeCSModifyChartTypeResource1" />
                        <ajaxToolkit:ModalPopupExtender ID="mpeCSModifyChartType" runat="server" BackgroundCssClass="modalBackground"
                            BehaviorID="mpeCSModifyChartTypeBehavior" PopupControlID="pnlCSModifyChartType"
                            TargetControlID="btnFakeCSModifyChartType" PopupDragHandleControlID="pnlDragHandle18" 
                            RepositionMode="None" DynamicServicePath="" Enabled="True">
                        </ajaxToolkit:ModalPopupExtender>
                        <asp:Panel ID="pnlCSModifyChartType" runat="server" CssClass="modalPopup" Style="display: none; width: 556px;" meta:resourcekey="pnlCSModifyChartTypeResource1">
                            <asp:Panel ID="pnlDragHandle18" runat="server" Style="cursor: hand;" meta:resourcekey="pnlDragHandle18Resource1">
                            <div class="modalPopupHandle" style="width: 550px;">
                                <table cellpadding="0" cellspacing="0" style="width: 100%"><tr><td>
                                        <asp:Localize ID="Localize13" runat="server" meta:resourcekey="lblSelectChartTypeResource"
                                            Text="Select Chart Type"></asp:Localize>
                                    </td>
                                    <td style="width: 20px;">
                                        <asp:ImageButton ID="ImageButton18" runat="server" 
                                            OnClick="imgClosePopup18_Click" CausesValidation="False"
                                            SkinID="imgbClose" ImageUrl="../ahImages/remove.gif" 
                                            AlternateText="<%$ Resources:LogiAdHoc, CloseWindow %>" />
                                </td></tr></table>
                            </div>
                            </asp:Panel>
                            <div class="modalDiv">
                                <asp:UpdatePanel ID="UPSelectChartType" runat="server" UpdateMode="Conditional">
                                    <ContentTemplate>
                                        <div id="divModifyChartType" runat="server">
                                            <input type="hidden" id="ihSelectedChartType" runat="server" />
                                            <input type="hidden" id="ihSCTReportContentID" runat="server" />
                                            <asp:Panel id="pnlSelectChartType" runat="server">
                                                
                                            </asp:Panel>
                                            <br /><table><tr><td>
                                            <AdHoc:LogiButton ID="btnCSSelectChartTypeOK" runat="server" CausesValidation="False"
                                                OnClick="btnCSSelectChartTypeOK_Click" Text="<%$ Resources:LogiAdHoc, OKWithSpaces %>" meta:resourcekey="btnCSSelectChartTypeOKResource1" />
                                            <AdHoc:LogiButton ID="btnCSSelectChartTypeCancel" runat="server" CausesValidation="False"
                                                OnClick="btnCSSelectChartTypeCancel_Click" Text="<%$ Resources:LogiAdHoc, Cancel %>" meta:resourcekey="btnCSSelectChartTypeCancelResource1" />
                                            </td></tr></table>
                                        </div>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </div>
                        </asp:Panel>     
                    </div>
                    
                    <asp:ValidationSummary ID="vsummary" runat="server" ValidationGroup="Report" meta:resourcekey="vsummaryResource1" />
                    <asp:Label ID="lblSemicolon" runat="server" OnPreRender="lblSemicolon_PreRender" meta:resourcekey="lblSemicolonResource1" />
                    <ul class="validation_error" id="ulErr" runat="server">
                        <li>
                            <asp:Label ID="liErr" runat="server" meta:resourcekey="liErrResource1"></asp:Label>
                        </li>
                    </ul>
            
                </ContentTemplate>
            </asp:UpdatePanel>

            <asp:UpdatePanel ID="UPInsertChartOptions" runat="server" UpdateMode="Conditional" RenderMode="Inline">
                <ContentTemplate>
                    <asp:Panel ID="pnlChartOptions" runat="server">
                        <asp:Panel ID="pnlInsertChartOptions" runat="server"></asp:Panel>
                    </asp:Panel>
                </ContentTemplate>
            </asp:UpdatePanel>
            <%--<ajaxToolkit:UpdatePanelAnimationExtender ID="UpdatePanelAnimationExt1" runat="server"
                TargetControlID="UPMain" BehaviorID="UpdatePanelAnimationExt1" Enabled="True">
                <Animations>
                    <OnUpdating>
                            <EnableAction Enabled="false" />
                    </OnUpdating>
                    <OnUpdated>
                            <EnableAction Enabled="true" />
                    </OnUpdated></Animations>
            </ajaxToolkit:UpdatePanelAnimationExtender>
            </ContentTemplate>
            </asp:UpdatePanel>--%>
            
            <asp:Panel ID="pnlHelpDefaultValue" runat="server" CssClass="popupControl" Width="300px" meta:resourcekey="pnlHelpDefaultValueResource1">
                <asp:Localize ID="Localize137" runat="server" meta:resourcekey="LiteralResource137" Text="
                When this checkbox is selected, the default value will be interpreted as True, Yes or On."></asp:Localize>
            </asp:Panel>
            
            <div id="divDataS">
            </div>
            
            <div id="divSaveAnimation">
                <asp:Button ID="btnSaveAnimation" runat="server" Style="display: none" meta:resourcekey="btnSaveAnimationResource1" />
                <ajaxToolkit:ModalPopupExtender ID="ahSavePopup" runat="server" BackgroundCssClass="modalBackground"
                    BehaviorID="ahSavePopupBehavior" PopupControlID="pnlSaving"
                    TargetControlID="btnSaveAnimation" DynamicServicePath="" Enabled="True">
                </ajaxToolkit:ModalPopupExtender>
                <asp:Panel ID="pnlSaving" runat="server" CssClass="savePopup" Style="display: none;" meta:resourcekey="pnlSavingResource1">
                    <table>
                        <tr>
                            <td>
                                <asp:Image ID="imgSave" runat="server" EnableViewState="False" SkinID="imgSaving" AlternateText="<%$ Resources:LogiAdHoc, SaveImageAltText %>" meta:resourcekey="imgSaveResource1" />
                            </td>
                            <td valign="middle">
                                <asp:Label ID="lblSaveText" runat="server" CssClass="lblSavePopup" EnableViewState="False"
                                    Text="<%$ Resources:LogiAdHoc, PleaseWait %>" meta:resourcekey="lblSaveTextResource1"></asp:Label>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </div>
        </div>
        
        <script type="text/javascript">

            function RebuildColorPicker(myInputElement)
            {
                var myColor = new jscolor.color(myInputElement)
                jscolor.install();
                jscolor.bind();        
            }

            function ResizePopupWindow()
            {
                var t=setTimeout("ResizePopupListbox()",10);
            }
            
            function ResizePopupListbox()
            {
                ScrollableListBoxRefineHeightAndWidth(document.getElementById('availableGrouping_ScrollableSelect'), 210, 300);
                ScrollableListBoxRefineHeightAndWidth(document.getElementById('assignedGrouping_ScrollableSelect'), 210, 300);
            }
        </script>
    </form>
</body>
</html>