<%@ Page AutoEventWireup="false" Codebehind="LivePreviewPopup.aspx.vb" Inherits="LogiAdHoc.ahWizard_LivePreviewPopup" Language="vb" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Home Page</title>
    <link rel="shortcut icon" href="../ahImages/flav.ico" />
    <%--<script language="JavaScript" type="text/javascript" src="../ahScripts/ahChange.js"></script>--%>
    <script type="text/javascript">
        function calcHeight()
        {
            try
            {
              //find the height of the internal page
              var the_height = document.getElementById('frm').contentWindow.document.body.scrollHeight;

              //change the height of the iframe
              document.getElementById('frm').height = the_height+50;
              //document.getElementById('frm').height = window.screen.availHeight;
            }
            catch(err)
            {
                //document.getElementById('frm').height = 5000;
                document.getElementById('frm').height = window.screen.availHeight+50;
            }
        }
        var Reloading = false;
        function SetReload(bRel)
        {
            Reloading = bRel;
        }
        
        function CloseWindow()
        {
            if (!Reloading) 
            {
                try
                {
                    //alert('unload');
                    window.opener.ClosePreviewPopup(false);
                }
                catch(err)
                {
                    //document.getElementById('frm').height = 5000;
                    //document.getElementById('frm').height = window.screen.availHeight+50;
                }
            }
        }
    </script> 
        
</head>
<body onunload="CloseWindow();">
    <form id="form1" runat="server">
        <%--<span>
            <iframe id="frm" runat="server" frameborder="0" width="100%"></iframe>
        </span>--%>
        <%--<asp:UpdatePanel ID="UPPreviewMain" runat="server" UpdateMode="Conditional">
            <ContentTemplate>--%>
                <div id="divPreviewMain" runat="server" class="searchDiv">
                    <div class="collapsePanelHeader">
                        <table width="100%">
                            <tr>
                                <td align="right">
                                    <asp:Localize ID="Localize141" runat="server" meta:resourcekey="LiteralResource141"
                                        Text="Live Preview"></asp:Localize>
                                    &nbsp;
                                    <asp:ImageButton ID="imgPopupPreview" runat="server" CausesValidation="False" ImageUrl="../ahImages/collapse_blue.jpg"
                                        OnClientClick="javascript: noMessage=true; window.opener.ClosePreviewPopup(true);" />
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div id="divPreview" runat="server">
                        <table width="100%">
                            <tr>
                                <td valign="top">
                                    <%--<asp:UpdateProgress ID="UpdateProgress1" runat="server" AssociatedUpdatePanelID="UPPreview">
                                        <ProgressTemplate>
                                            loading...
                                        </ProgressTemplate>
                                    </asp:UpdateProgress>--%>
                                    <%--<asp:UpdatePanel ID="UPPreview" runat="server" UpdateMode="Conditional">
                                        <ContentTemplate>--%>
                                            <iframe id="frm" runat="server" width="100%" title="<%$ Resources:LogiAdHoc, LivePreview %>"></iframe>
                                        <%--</ContentTemplate>
                                    </asp:UpdatePanel>--%>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
            <%--</ContentTemplate>
        </asp:UpdatePanel>--%>
    </form>
</body>
</html>
