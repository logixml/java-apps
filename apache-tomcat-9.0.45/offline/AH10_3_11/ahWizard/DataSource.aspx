<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="DataSource.aspx.vb" Inherits="LogiAdHoc.DataSource" Culture="auto"
    UICulture="auto" %>

<%@ Register TagPrefix="cc1" Assembly="LGXAHWCL" Namespace="LGXAHWCL" %>
<%@ Register TagPrefix="wizard" TagName="SpecialValue" Src="../ahControls/SpecialValue.ascx" %>
<%@ Register TagPrefix="wizard" TagName="DatabaseValues" Src="../ahControls/DatabaseValues.ascx" %>
<%@ Register TagPrefix="AdHoc" TagName="DatabaseControl" Src="~/ahControls/DatabaseControl.ascx" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7">
    <script language="JavaScript" type="text/javascript" src="../ahScripts/ahChange.js"></script>
    <script type="text/javascript">
<!--
/*
    var SelectRowIndex = -1;
    
    var lastPanel1 = "";
    var lastPanel2 = "";
    function MouseOver(select, pnlID1, pnlID2)
    {
        var pnl1 = document.getElementById(pnlID1);
        var pnl2 = document.getElementById(pnlID2);
        if(select) {
            if ((lastPanel1 != "") && (lastPanel2 != "")) {
                var lpnl1 = document.getElementById(lastPanel1);
                if (lpnl1)
                    lpnl1.style.display="none";
                
                var lpnl2 = document.getElementById(lastPanel2);
                if (lpnl2)
                lpnl2.style.display="none";
            }
            
            if (pnl1)
                pnl1.style.display="";
            if (pnl2)
                pnl2.style.display="";
            
            lastPanel1 = pnlID1;
            lastPanel2 = pnlID2;
        }
        else {
            if (pnl1)
                pnl1.style.display="none";
            if (pnl2)
                pnl2.style.display="none";
            //div.style.display="";
        }
    }
    */
    function ShowInfo(id) {
        var o = document.getElementById(id);
        var t = "";
        if (o) t = o.innerHTML;
        document.getElementById("divColumns").innerHTML = t;
    }
    
    function BlankInfo() {
        document.getElementById("divColumns").innerHTML = "";
    }
    
    function ShowDataForObject(objectID)
    {
        var ih = document.getElementById('ihShowDataObjectID');
        ih.value = objectID;
        
        document.getElementById('btnShowDataFake').click();
    }
    
    //var activeMenuTab;
    
    function applyMenuHover(o) {
        if (o.className != 'tab_active') o.className="tab_hover";
    }
    
    function applyMenuActive(o) {
        o.className="tab_active";
        //if (activeMenuTab) {
        //    activeMenuTab.className="";
            //document.getElementById('div'+activeMenuTab.id.substring(2)).style.display="none";
        //}
        //activeMenuTab=o;
        //document.getElementById('div'+activeMenuTab.id.substring(2)).style.display="block";
        
        var btn = document.getElementById('btn'+o.id.substring(2));
        if (btn)
            btn.click();
    }
    
    function applyMenuNone(o) {
        //if (o != activeMenuTab) o.className="";
        if (o.className != 'tab_active') o.className="";
    }
    function DataSourceSelect_OnChange(parentID)
    {
        if(!confirm('Are you sure you want to change the data source for active content?'))
        {
            document.getElementById(parentID).selectedIndex = document.getElementById('ihLastDataSourceID').value
            return false;
        }
        
        document.getElementById('ihLastDataSourceID').value = document.getElementById(parentID).selectedIndex
        return true;
    }
    -->
    </script>
    <script type="text/javascript">
    
    function ModifyLibraryColumn(columnID) {
        var ih = document.getElementById("ihModifyColumnID");
        ih.value = columnID;
        
        var btn = document.getElementById("btnModifyLibraryColumn");
        btn.click();
    }
    function RemoveLibraryColumn(columnID) {
        var ih = document.getElementById("ihModifyColumnID");
        ih.value = columnID;
        
        var btn = document.getElementById("btnRemoveLibraryColumn");
        btn.click();
    }
    function RemoveCalculatedColumn1(columnID) {
        var ih = document.getElementById("ihRemoveCalculatedColumn1");
        ih.value = columnID;
        
        var btn = document.getElementById("btnRemoveCalculatedColumn1");
        btn.click();
    }
    
    //Statistical Columns
    function ModifyStatColumn(columnID) {
        var ih = document.getElementById("ihModifyStatColumnID");
        ih.value = columnID;
        
        var btn = document.getElementById("btnModifyStatColumn");
        btn.click();
    }
    function RemoveStatColumn(columnID) {
        var ih = document.getElementById("ihModifyStatColumnID");
        ih.value = columnID;
        
        var btn = document.getElementById("btnRemoveStatColumn");
        btn.click();
    }
    function RemoveStatisticalColumn1(columnID) {
        var ih = document.getElementById("ihRemoveStatColumnID1");
        ih.value = columnID;
        
        var btn = document.getElementById("btnRemoveStatColumn1");
        btn.click();
    }
    function pickStatisticalColumn( colKey, colName )
    {
        var targetLbl = document.getElementById("lblStatSelectedColumn1");
        var targetKey = document.getElementById("txtStatSelectedColumn");
        var mName = document.getElementById("StatName");
    		
        var txt = colName;
        targetKey.value = colKey;
		targetLbl.value = "�" + txt.replace(".", "�.�") + "�";
		var pos = txt.lastIndexOf(".");
		mName.value = txt.substring(pos + 1, txt.length + 1) + getSuffix();
    }
    function getSuffix() {
        var ft = document.getElementById( "FunctionType" );
        var res = "Rank";
        switch (ft.value) {
	        case "1":
		        res = "Rank";
		        break;
	        case "2":
		        res = "RvrsRank";
		        break;
	        case "3":
		        res = "Percentile";
		        break;
	        case "4":
		        res = "RngTotal";
		        break;
	        case "5":
		        res = "Difference";
		        break;
	        default:
		        res = "Rank";
		        break;
        }
        
//        var dbt = document.getElementById("whatCase");
//        switch (dbt.value) {
//	        case "l":
//		        res = res.toLowerCase();
//		        break;
//	        case "u":
//		        res = res.toUpperCase();
//		        break;
//        }
        
        return res;
    }
    </script>
    
    <script src="../rdTemplate/yui/build/yahoo-dom-event/yahoo-dom-event.js" type="text/javascript"></script>
    <script src="../rdTemplate/yui/build/animation/animation-min.js" type="text/javascript"></script>
    <script src="../rdTemplate/yui/build/dragdrop/dragdrop-min.js" type="text/javascript"></script>
    <script language="JavaScript" type="text/javascript" src="../ahScripts/ahReorderGrid.js"></script>
    
</head>
<body class="modalPopupBackground">
    <form id="form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnablePartialRendering="true" >
        <Scripts>
            <asp:ScriptReference Path="~/ahScripts/webkit.js" />
        </Scripts>
    </asp:ScriptManager>
    
    <script type="text/javascript">
        Sys.WebForms.PageRequestManager.getInstance().add_endRequest(EndRequestHandler);
        Sys.WebForms.PageRequestManager.getInstance().add_beginRequest(BeginRequestHandler);
        function EndRequestHandler(sender, args) {
            RestorePopupPosition('ahModalPopupBehavior');
            RestorePopupPosition('ahSaveAsPopupBehavior');            
        }
        function BeginRequestHandler(sender, args) {
            SavePopupPosition('ahModalPopupBehavior');
            SavePopupPosition('ahSaveAsPopupBehavior');
        }
    </script>
    <script type="text/javascript">
    
        var lastCaretPos;
        var lastCaretStart;
        var lastCaretEnd;
        
        var rm = Sys.WebForms.PageRequestManager.getInstance();
        rm.add_pageLoaded(pageLoaded);
         function pageLoaded(sender, args) {
            lastCaretPos=null;
            lastCaretStart=null;
            lastCaretEnd=null;
        }
        
     // Stores the current location of a cursor in an html form element.
        function storeCaret() {
          var el = document.getElementById( "txtFormula" );
          if (el != null) {
              if (el.createTextRange)
                lastCaretPos = document.selection.createRange().duplicate();
              else {
                lastCaretStart = el.selectionStart;
                lastCaretEnd = el.selectionEnd;
              }
          }
        }
        
        // Places the symbol at the end of the text in the formula box.
        function appendSymbol( symbol )
        {
	        // Symbols can be only one character long. 14612
	        if (symbol.length == 1)   
	        {
		        var target = document.getElementById( "txtFormula" );
		        if (target != null)
		        {
			        //target.value += symbol;		
			        insertAtCaret(target, " " + symbol + " ");
			        document.getElementById('ihCalcColDirty').value=1;
		        }
	        }
	        else
	        {
		        alert("Invalid operator.");
	        }
        }

        function appendColumn(txt,bAsIs)
        {
            //var el = document.getElementById("ColumnPicker");
	        var target = document.getElementById( "txtFormula" );
	        if (target != null)
	        {
	            if (bAsIs) {
        	        insertAtCaret(target, " ( " + txt + " ) ");
        	        document.getElementById('ihCalcColDirty').value=1;
        	    }
        	    else {
        	        insertAtCaret(target, " �" + txt.replace(".", "�.�") + "� ");
        	        document.getElementById('ihCalcColDirty').value=1;
        	    }
        	}
//	        if (el != null )
//	        {
//		        for (i=0; i<el.options.length; i++) 
//		        {
//			        if (el.options[i].selected) 
//			        {
//				        // Add the column name to the formula. Enclose it in square brackets.
//				        //target.value += "[" + el.options[i].text + "]";
//				        //insertAtCaret(target, " [" + el.options[i].text + "] ");
//				        var txt = el.options[i].text;
//				        insertAtCaret(target, " �" + txt.replace(".", "�.�") + "� ");
//				        //document.getElementById('ahDirty').value=1;
//			        }
//		        }
//	        }
        }
        
        function appendFunction()
        {
	        var target = document.getElementById( "txtFormula" );
		    if (target != null)
		    {
			    var ih = document.getElementById("ihCalcColFormula");
			    ih.value = target.value;
			    insertAtCaret(target, "�");
		    }
        }
        
        // The functions below insert text at a specified position in a textarea.
        // The code below only works for IE, since it relies on createTextRange().
        // Inserts will occur at the end of the textarea for all other browsers.
        // See http://www.faqts.com/knowledge_base/view.phtml/aid/1052/fid/130

        // Adjusts the caret position.
        // Used in conjunction with the [storeCaret] function.
        function setCaretToEnd (el) {
          if (el.createTextRange) {
            var v = el.value;
            var r = el.createTextRange();
            r.moveStart('character', v.length);
            r.select();
          }
        }
        
        // Inserts text at the current location of a cursor in an html form element.
        // Used in conjunction with the [storeCaret] function.
        function insertAtCaret(el, txt) {
          if (lastCaretPos) {
	        el.range = lastCaretPos;
            el.range.text = el.range.text.charAt(el.range.text.length - 1) != ' ' ? txt : txt + ' ';
            el.range.select();
          }
          else if (lastCaretStart && lastCaretEnd) {
            var s = el.value;
            el.value = s.substring(0, lastCaretStart) + txt + s.substring(lastCaretEnd, s.length);
            lastCaretStart = lastCaretStart + txt.length;
            lastCaretEnd = lastCaretStart;
          }
          else {
            insertAtEnd(el, txt);
          }
        }
        
        // Inserts text at the end of an html form element.
        function insertAtEnd(el, txt) {
          el.value += txt;
          setCaretToEnd (el);
        }
        
        function ApplyChangesToCalcColumn() {
            var ih = document.getElementById('ihCalcColDirty');
            if (ih)
            {
                if (ih.value == 1) {
                    if(!confirm('You may lose any unsaved changes to this column.\n\nAre you sure you want to continue?'))
                        return false;
                    else
                        ih.value = 0;
                }
            }    
            return true;
        }
    </script>
    <script type="text/javascript">
        //var focusEl = "";
        function setFunctionParameterValue(txt,bAsIs)
        {
//            if (focusEl == "") {
//                var ih = document.getElementById("ihDefTxtCtrl");
//                focusEl = ih.value;
//            }
            var ih = document.getElementById("ihDefTxtCtrl");
            if (ih.value != "") {
                var focusTxt = document.getElementById(ih.value);
                if (focusTxt != null)
                {
                    if (bAsIs) {
                        focusTxt.value = txt;
                    } else {
                        focusTxt.value = "�" + txt.replace(".", "�.�") + "�";
                    }
                }
            }
        }
        
        function setFocusEl(el)
        {
            //focusEl = el;
            var ih = document.getElementById("ihDefTxtCtrl");
            ih.value = el;
        }
//        function resetFocusEl()
//        {
//            focusEl = "";
//        }
    </script>
    
    <div>
        <asp:UpdatePanel ID="UPInfo" runat="server" UpdateMode="Conditional" RenderMode="inline">
            <ContentTemplate>
            <div id="divInfo" runat="server" class="NoShow"></div>
            </ContentTemplate>
        </asp:UpdatePanel>
        
        <asp:UpdatePanel ID="UPMain" runat="server" UpdateMode="Conditional" RenderMode="inline">
            <ContentTemplate>
                <div class="tab_xp tab_default" style="width:100%; height:100%">
                    <asp:Button ID="btnTData" runat="server" CssClass="NoShow" OnClick="btnShowData_OnClick" />
                    <asp:Button ID="btnTCalc" runat="server" CssClass="NoShow" OnClick="btnShowCalc_OnClick" />
                    <asp:Button ID="btnTStat" runat="server" CssClass="NoShow" OnClick="btnShowStat_OnClick" />
                    <asp:Button ID="btnTSort" runat="server" CssClass="NoShow" OnClick="btnShowSort_OnClick" />
                    <asp:Button ID="btnTFilter" runat="server" CssClass="NoShow" OnClick="btnShowFilter_OnClick" />        
                    <asp:UpdatePanel ID="UPDataTabs" runat="server" UpdateMode="Conditional" RenderMode="inline">
                        <ContentTemplate>
                <div id="divDataTabs" class="tab_header">
<span id="spTData" runat="server" onmouseover="applyMenuHover(this);" onmouseout="applyMenuNone(this);" onclick="applyMenuActive(this);"><span class="tab_outer"><span class="tab_inner"><span class="tab_tab"><asp:Literal runat="server" Text="Add/Remove" ID="litAdd" meta:resourceKey="litAdd"></asp:Literal></span></span></span></span><span id="spTCalc" runat="server" onmouseover="applyMenuHover(this);" onmouseout="applyMenuNone(this);" onclick="applyMenuActive(this);"><span class="tab_outer"><span class="tab_inner"><span class="tab_tab"><asp:Literal runat="server" Text="Calculated Columns" ID="litCalc" meta:resourceKey="litCalc"></asp:Literal></span></span></span></span><span id="spTStat" runat="server" onmouseover="applyMenuHover(this);" onmouseout="applyMenuNone(this);" onclick="applyMenuActive(this);"><span class="tab_outer"><span class="tab_inner"><span class="tab_tab"><asp:Literal runat="server" Text="Statistical Columns" ID="litStat" meta:resourceKey="litStat"></asp:Literal></span></span></span></span><span id="spTSort" runat="server" onmouseover="applyMenuHover(this);" onmouseout="applyMenuNone(this);" onclick="applyMenuActive(this);"><span class="tab_outer"><span class="tab_inner"><span class="tab_tab"><asp:Literal runat="server" Text="Sort" ID="litSort" meta:resourceKey="litSort"></asp:Literal></span></span></span></span><span id="spTFilter" runat="server" onmouseover="applyMenuHover(this);" onmouseout="applyMenuNone(this);" onclick="applyMenuActive(this);"><span class="tab_outer"><span class="tab_inner"><span class="tab_tab"><asp:Literal runat="server" Text="Filter" ID="litFilter" meta:resourceKey="litFilter"></asp:Literal></span></span></span></span>
                </div>	
                        </ContentTemplate>
                    </asp:UpdatePanel>
                    <div id="divTData" runat="server" class="tab_data">
                        <asp:Panel ID="pnlSelectDataControl" runat="server">
                            <asp:UpdatePanel ID="UP1" runat="server" UpdateMode="Conditional" RenderMode="inline">
                                <ContentTemplate>
                                    <table cellspacing="10" width="100%">
                                        <tr id="trCategories" runat="server">
                                            <td colspan="2">
                                                <table>
                                                    <tr>
                                                        <td id="tdlblDatabaseList" runat="server">
                                                            <asp:Label ID="lblDatabases" runat="server" Text="" AssociatedControlID="ddlDatabases" />
                                                        </td>
                                                        <td id="tdDdlDatabaseList" runat="server">
                                                            <asp:DropDownList ID="ddlDatabases" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlDatabases_SelectedIndexChanged">
                                                            </asp:DropDownList> 
                                                        </td>
                                                        <td>
                                                            <div id="divCategories" runat="server" >
                                                                <table>
                                                                    <tr>
                                                                        <td>
                                                                            <asp:Label ID="Localize1" runat="server" Text="Data Objects in:" AssociatedControlID="ddlCategories" meta:resourcekey="Localize1Resource1"></asp:Label>
                                                                        </td>
                                                                        <td>
                                                                            <asp:DropDownList ID="ddlCategories" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlCategories_SelectedIndexChanged" meta:resourcekey="ddlCategoriesResource1">
                                                                            </asp:DropDownList>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </div>
                                                        </td>
                                                        <td>
                                                            <asp:CheckBox ID="chkDistinct" runat="server" />
                                                            <asp:Label ID="Label26" runat="server" meta:resourcekey="LiteralResource26" Text="Exclude duplicate rows" AssociatedControlID="chkDistinct"></asp:Label>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td id="tdPrimary" valign="top" runat="server"  style="height:300px; background-color:White; border: solid 1px DarkGray;">
                                                <div style="height: 296px; width: 100%; overflow: auto;"> 
                                                    <asp:UpdatePanel ID="upTreeView" runat="server" UpdateMode="Conditional" RenderMode="inline">
                                                        <ContentTemplate>
                                                            <cc1:DataSourceTreeView ID="tvDataSource" runat="server" ShowCheckBoxes="Parent" ShowLines="True" 
                                                                ExpandDepth="1" EnableClientScript="false" meta:resourcekey="tvDataSourceResource1">
                                                            </cc1:DataSourceTreeView>
                                                            <asp:Button ID="btnFakeObjectSelected" runat="server" CssClass="NoShow" />
                                                            <input type="hidden" id="ihRemoveCalculatedColumn1" runat="server" />
                                                            <asp:Button ID="btnRemoveCalculatedColumn1" runat="server" CssClass="NoShow" CausesValidation="false" 
                                                                 OnClick="RemoveCalculatedColumn1" meta:resourcekey="btnRemoveCalculatedColumn1Resource1" />
                                                            <input type="hidden" id="ihRemoveStatColumnID1" runat="server" />
                                                            <asp:Button ID="btnRemoveStatColumn1" runat="server" CssClass="NoShow" CausesValidation="false" 
                                                                 OnClick="RemoveStatisticalColumn1" meta:resourcekey="btnRemoveStatColumn1Resource1" />
                                                        </ContentTemplate>
                                                    </asp:UpdatePanel>
                                                </div>
                                            </td>
                                            <td id="tdColumns" valign="top" runat="server" style="height:320px; background-color: White; border: solid 1px DarkGray;" width="300px">
                                                <div id="divInfHeader" class="groupHeader">
                                                    <asp:Localize ID="Localize2" runat="server" Text="Information" meta:resourcekey="Localize2Resource1" />
                                                </div>
                                                <div id="divColumns" runat="server" style="height: 275px; overflow: auto; padding:5px;">
                                                </div>
                                                <asp:Panel ID="pnlHelp" runat="server" meta:resourcekey="pnlHelpResource1">
                                                </asp:Panel>
                                            </td>
                                        </tr>
                                    </table>
                                
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </asp:Panel>
                    </div>
                    <div id="divTCalc" runat="server" class="tab_data">
                        <asp:Panel ID="pnlCalc" runat="server" meta:resourcekey="pnlCalcResource1">
                            <asp:UpdatePanel ID="UPCalcColumn" runat="server" UpdateMode="Conditional" RenderMode="inline">
                                <ContentTemplate>
                                    <input type="hidden" id="ihCalculatedColumnID" runat="server" />
                                    <input type="hidden" id="ihCalcColFormula" runat="server" />
                                    <input type="hidden" id="ihCalcColDirty" runat="server" />
                                    <asp:Localize ID="LocFunctions" runat="server" Text="Functions:" meta:resourceKey="locFunc"></asp:Localize>
                                    <br />
                                    <div id="divFunctionsMenu" runat="server">
                                    </div>
                                    <div id="divMenuFunctions" runat="server" style="width: 100px; height: 10px;" >
                                    </div>
                                    <div id="divMoreMenu" >
                                        <asp:Panel CssClass="popupMenu" ID="PopupMenu" runat="server" style="display: none;">
                                            <%--<div style="border:1px outset white;padding:2px;">--%>
                                                <div><asp:LinkButton ID="btnCreateReport" runat="server" Text="<%$ Resources:LogiAdHoc, Res_And %>" 
                                                             OnClientClick="appendSymbol('AND');return false;" CausesValidation="false"/></div>
                                                <div><asp:LinkButton ID="btnCreateDashboard" runat="server" Text="<%$ Resources:LogiAdHoc, Res_Or %>" 
                                                            OnClientClick="appendSymbol('OR');return false;" CausesValidation="false"/></div>
                                                <div><asp:LinkButton ID="btnCreateFolder" runat="server" Text="<%$ Resources:LogiAdHoc, Res_Not %>" 
                                                            OnClientClick="appendSymbol('NOT');return false;" CausesValidation="false"/></div>
                                            <%--</div>--%>
                                        </asp:Panel>
                                        <ajaxToolkit:HoverMenuExtender ID="hme" runat="Server"
                                            PopupControlID="PopupMenu"
                                            PopupPosition="Bottom" 
                                            TargetControlID="spBtnMore"
                                            PopDelay="25" />
                                    </div>
                                    <table>
                                        <tr>
                                            <td valign="top" width="250px">
                                                <asp:Localize ID="locColumn" runat="server" Text="Columns:" meta:resourceKey="locColumn"></asp:Localize>
                                                <br />
                                                <div class="divColumnTreeView" style="width:240px; height: 290px;">
                                                    <cc1:DataSourceTreeView ID="trvColumns" runat="server" meta:resourcekey="trvColumnsResource1">
                                                    </cc1:DataSourceTreeView>
                                                </div>
                                                <input type="hidden" id="ihModifyColumnID" runat="server" />
                                                <asp:Button ID="btnModifyLibraryColumn" runat="server" CssClass="NoShow" CausesValidation="false" 
                                                     OnClick="ModifyLibraryColumn" meta:resourcekey="btnModifyLibraryColumnResource1" />
                                                <asp:Button ID="btnRemoveLibraryColumn" runat="server" CssClass="NoShow" CausesValidation="false" 
                                                     OnClick="RemoveLibraryColumn" meta:resourcekey="btnRemoveLibraryColumnResource1" />
                                            </td>
                                            <td valign="top" width="480px" style="padding-top:3px;">
                                                <table>
                                                    <tr>
                                                        <td>
                                                            <asp:Label ID="locName" runat="server" Text="Name:" AssociatedControlID="txtName" meta:resourceKey="locName"></asp:Label>
                                                        </td>
                                                        <td>
                                                            <input id="txtName" runat="server" size="41" type="text" />
                                                            <asp:RequiredFieldValidator ID="rtvCalcName" ValidationGroup="Calculation" ControlToValidate="txtName" 
                                                                ErrorMessage="Calculated Column Name is required." runat="server" meta:resourcekey="rtvCalcNameResource1">*</asp:RequiredFieldValidator>
                                                            <asp:CustomValidator ID="cvUniqueCalcName" ValidationGroup="Calculation" runat="server"
                                                                ErrorMessage="Another calculation already uses this name. Please choose a new name."
                                                                ControlToValidate="txtName" OnServerValidate="IsCalcNameUnique" meta:resourcekey="cvUniqueCalcNameResource1">*</asp:CustomValidator>
                                                        </td>
                                                     </tr>
                                                </table>
                                                <br />
                                                <asp:Label ID="locDef" runat="server" Text="Definition:" AssociatedControlID="txtFormula" meta:resourceKey="locDef"></asp:Label>
                                                <br />
                                                <textarea id="txtFormula" runat="server" cols="92" rows="5" 
                                                    onclick="storeCaret()" onkeyup="storeCaret()" >
                                                </textarea>
                                                <br />
                                                <%--<textarea id="txtReplaceCalcColDef" runat="server" class="NoShow" />--%>
                                                <div style="float:right; padding-top:5px;">
                                                    <AdHoc:LogiButton ID="btnTestCalcColumn" runat="server" Text=" Test " 
                                                        OnClick="TestColumn_OnClick" ValidationGroup="Calculation" meta:resourcekey="LogiButton6Resource1" />
                                                </div>
                                                <br />
                                                <br />
                                                <asp:Localize ID="locOp" runat="server" Text="Operators:" meta:resourceKey="locOp"></asp:Localize>
                                                <AdHoc:LogiButton ID="btnP" runat="server" OnClientClick="appendSymbol('+');return false;" Text="+" CausesValidation="false" UseSubmitBehavior="False" />
                                                <AdHoc:LogiButton ID="btnM" runat="server" OnClientClick="appendSymbol('-');return false;" Text="-" CausesValidation="false" UseSubmitBehavior="False" />
                                                <AdHoc:LogiButton ID="btnL" runat="server" OnClientClick="appendSymbol('*');return false;" Text="x" CausesValidation="false" UseSubmitBehavior="False" />
                                                <AdHoc:LogiButton ID="btnD" runat="server" OnClientClick="appendSymbol('/');return false;" Text="/" CausesValidation="false" UseSubmitBehavior="False" />
                                                <AdHoc:LogiButton ID="btnO" runat="server" OnClientClick="appendSymbol('(');return false;" Text="(" CausesValidation="false" UseSubmitBehavior="False" />
                                                <AdHoc:LogiButton ID="btnC" runat="server" OnClientClick="appendSymbol(')');return false;" Text=")" CausesValidation="false" UseSubmitBehavior="False" />
                                                <span id="spBtnMore" runat="server"><AdHoc:LogiMenuBox ID="btnMore" runat="server" Text="More" meta:resourceKey="btnMore" /></span>
                                                <br />
                                                <br />
                                                <asp:Label ID="locType" runat="server" Text="Type:" AssociatedControlID="ddlDataType" meta:resourceKey="locType"></asp:Label>
                                                <asp:DropDownList ID="ddlDataType" runat="server">
                                                     <asp:ListItem Value="-1" Text="<%$ Resources:LogiAdHoc, Automatic %>" />
                                                     <asp:ListItem Value="2" Text="<%$ Resources:LogiAdHoc, Text %>" />
                                                     <asp:ListItem Value="1" Text="<%$ Resources:LogiAdHoc, Number %>" />
                                                     <asp:ListItem Value="4" Text="<%$ Resources:LogiAdHoc, Res_Date %>" />
                                                </asp:DropDownList>&nbsp;  
                                                <AdHoc:LogiButton ID="btnDetermineType" runat="server" Text="Determine Type" 
                                                    meta:resourcekey="LogiButton5Resource1" ValidationGroup="Calculation" 
                                                    OnClick="btnDetermineType_OnClick" />
                                                <br />
                                                <br />
                                                <table>
                                                     <tr>
                                                        <td colspan="2">
                                                            <br />
                                                            <AdHoc:LogiButton ID="btnSaveCalc" runat="server" Text="Save" OnClick="SaveCalculation" 
                                                                meta:resourcekey="LogiButton3Resource1" ValidationGroup="Calculation"/>
                                                            <%--<AdHoc:LogiButton ID="btnClearCalc" runat="server" Text="Clear" OnClick="ClearCalculation" 
                                                                meta:resourcekey="LogiButton1Resource1" CausesValidation="false"/>--%>
                                                            <AdHoc:LogiButton ID="btnNewCalc" runat="server" Text=" New " OnClick="NewCalculation" 
                                                                meta:resourcekey="LogiButton2Resource1" CausesValidation="false" UseSubmitBehavior="false"/>
                                                        </td>
                                                    </tr>
                                                </table>
                                                 
                                                <asp:ValidationSummary ID="ValidationSummary5" runat="server" ValidationGroup="Calculation" meta:resourcekey="ValidationSummary5Resource1" />
                                                <%--Adding this label as a temporary workaround to fix a bug in AJAXcontroltoolkit.--%>
                                                <asp:Label ID="lblSemicolon5" runat="server" OnPreRender="lblSemicolon_PreRender" />
                                                <ul class="validation_error" id="calcErrorList" runat="server">
                                                    <li>
                                                        <asp:Label ID="calcError" runat="server" meta:resourcekey="calcErrorResource1"></asp:Label>
                                                    </li>
                                                </ul>
                                            </td>
                                        </tr>
                                    </table>
                                    
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </asp:Panel>
                    </div>
                    <div id="divTStat" runat="server" class="tab_data">
                        <asp:Panel ID="pnlStat" runat="server" meta:resourcekey="pnlCalcResource1">
                            <asp:UpdatePanel ID="UPStatColumn" runat="server" UpdateMode="Conditional" RenderMode="inline">
                                <ContentTemplate>
                                    <input type="hidden" id="ihStatisticalColumnID" runat="server" />
                                    <%--<input type="hidden" id="whatCase" name="whatCase" runat="server" />--%>
                                    <table>
                                        <tr>
                                            <td valign="top" width="250px">
                                                <asp:Localize ID="locColumnStat" runat="server" Text="Columns:" meta:resourceKey="locColumn"></asp:Localize> <br />
                                                <div class="divColumnTreeView" style="width:240px; height:339px;">
                                                    <cc1:DataSourceTreeView ID="trvStatColumns" runat="server">
                                                    </cc1:DataSourceTreeView>
                                                </div>
                                                <input type="hidden" id="ihModifyStatColumnID" runat="server" />
                                                <asp:Button ID="btnModifyStatColumn" runat="server" CssClass="NoShow" CausesValidation="false" 
                                                     OnClick="ModifyStatColumn" />
                                                <asp:Button ID="btnRemoveStatColumn" runat="server" CssClass="NoShow" CausesValidation="false" 
                                                     OnClick="RemoveStatColumn" />
                                            </td>
                                            <td valign="top">
                                                <table>
                                                    <tr style="height:24px;">
                                                        <td>
                                                            <asp:Label ID="Localize25" runat="server" AssociatedControlID="lblStatSelectedColumn1" meta:resourcekey="LiteralResource25" Text="Selected Column:"></asp:Label>
                                                        </td>
                                                        <td>
                                                            <!--<asp:Label ID="lblStatSelectedColumn" runat="server"></asp:Label>-->
                                                            <asp:TextBox ID="lblStatSelectedColumn1" runat="server" Enabled="False" BorderStyle="None" Width="350px" />
                                                            <asp:Label ID="Label4" runat="server" AssociatedControlID="txtStatSelectedColumn" CssClass="NoShow" Text="Selected Column:"></asp:Label>
                                                            <asp:TextBox ID="txtStatSelectedColumn" runat="server" CssClass="NoShow"></asp:TextBox>
                                                            <asp:RequiredFieldValidator ID="rfvStatSelectedColumn" ValidationGroup="Statistical" 
                                                                runat="server" ErrorMessage="Please select a Column." ControlToValidate="txtStatSelectedColumn"
                                                                meta:resourcekey="Requiredtextvalidator2Resource1">*</asp:RequiredFieldValidator>
                                                        </td>
                                                    </tr>
                                                    <tr style="height:24px;">
                                                        <td>
                                                            <asp:Label ID="Label23" runat="server" meta:resourcekey="LiteralResource23" Text="Function Type:" AssociatedControlID="FunctionType"></asp:Label>
                                                        </td>
                                                        <td>
                                                            <asp:DropDownList ID="FunctionType" runat="server" AutoPostBack="True" meta:resourcekey="FunctionTypeResource1">
                                                                <asp:ListItem Value="1" Text="<%$ Resources:LogiAdHoc, RW_Rank %>" />
                                                                <asp:ListItem Value="2" Text="<%$ Resources:LogiAdHoc, ST_RevRank %>" />
                                                                <asp:ListItem Value="3" Text="<%$ Resources:LogiAdHoc, ST_Percentile %>" />
                                                                <asp:ListItem Value="4" Text="<%$ Resources:LogiAdHoc, ST_RunTotal %>" />
                                                                <asp:ListItem Value="5" Text="<%$ Resources:LogiAdHoc, ST_DifFromPrev %>" />
                                                            </asp:DropDownList>
                                                        </td>
                                                    </tr>
                                                    <tr style="height:24px;">
                                                        <td>
                                                        <asp:Label ID="Label24" runat="server" Text="<%$ Resources:LogiAdHoc, Name %>" AssociatedControlID="StatName"></asp:Label>:
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="StatName" Width="150px" runat="server" />
                                                            <asp:RequiredFieldValidator ID="rtvStatName" ValidationGroup="Statistical" 
                                                                runat="server" ErrorMessage="Name is required." ControlToValidate="StatName"
                                                                meta:resourcekey="Requiredtextvalidator2Resource1">*</asp:RequiredFieldValidator>
                                                            <asp:CustomValidator ID="cvUniqueStatName" ValidationGroup="Statistical" runat="server"
                                                                ErrorMessage="Another Statistical function already uses this name. Please choose a new name."
                                                                ControlToValidate="StatName" OnServerValidate="IsStatNameUnique" meta:resourcekey="cvUniqueStatNameResource1">*</asp:CustomValidator>
                                                        </td>
                                                     </tr>
                                                     <tr style="height:24px;">
                                                        <td colspan="2">
                                                            <br />
                                                            <AdHoc:LogiButton ID="btnSaveStat" runat="server" Text="Save" OnClick="SaveStatisticalColumn" 
                                                                meta:resourcekey="LogiButton3Resource1" ValidationGroup="Statistical"/>
                                                            <AdHoc:LogiButton ID="btnClearStat" runat="server" Text="Clear" OnClick="ClearStatisticalColumn" 
                                                                meta:resourcekey="LogiButton1Resource1" CausesValidation="false"/>
                                                            <AdHoc:LogiButton ID="btnNewStat" runat="server" Text=" New " OnClick="NewStatisticalColumn" 
                                                                meta:resourcekey="LogiButton2Resource1" ValidationGroup="Statistical"/>
                                                        </td>
                                                    </tr>
                                                </table>
                                                <asp:ValidationSummary ID="ValidationSummary4" runat="server" ValidationGroup="Statistical"
                                                    meta:resourcekey="ValidationSummary6Resource1" />
                                                <asp:Label ID="Label2" runat="server" OnPreRender="lblSemicolon_PreRender" />
                                            </td>
                                        </tr>
                                    </table>
                                        
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </asp:Panel>
                    </div>
                    <div id="divTSort" runat="server" class="tab_data">
                        <asp:Panel ID="pnlOrder" runat="server" Height="100%" Width="100%" meta:resourcekey="pnlOrderResource1">
                            <asp:UpdatePanel ID="UP3" runat="server" UpdateMode="Conditional" RenderMode="inline">
                                <ContentTemplate>
                                    <asp:Button ID="btnFakeGrdOrderReorderRows" runat="server" CssClass="NoShow" 
                                        OnClick="btnFakeGrdOrderReorderRows_Click" />
                                    <%--<div style="background-color: White; border: solid 1px DarkGray;">--%>
                                        <div style="height: 260px; width: 100%; overflow: auto;">
                                    <AdHoc:DDGridView ID="grdOrder" runat="server" AutoGenerateColumns="False" 
                                        OnRowDataBound="OnOrderItemDataBoundHandler" OnRowCommand="OnOrderItemCommandHandler" 
                                        CssClass="grid" meta:resourcekey="grdOrderResource1" DDDivID="divDragHandle"
                                        PostbackButtonID="btnFakeGrdOrderReorderRows" >
                                        <Columns>
                                            <asp:TemplateField HeaderText="<%$ Resources:LogiAdHoc, Column %>">
                                                <HeaderStyle Width="200px"></HeaderStyle>
                                                <ItemTemplate>
                                                    <input type="hidden" runat="server" id="RowOrder" />
                                                    <input type="hidden" runat="server" id="RowIndex" />
                                                    <table cellpadding="0" cellspacing="0">
                                                        <tr>
                                                            <td>
                                                                <div id="divDragHandle" runat="server" class="dragHandle"></div>
                                                            </td>
                                                            <td>
                                                                <input type="hidden" id="Key" runat="server" />
                                                                <%--<asp:Label ID="LongLabel" runat="server" meta:resourcekey="LongLabelResource1" />--%>
                                                                <asp:DropDownList ID="NewKey" runat="server" meta:resourcekey="NewKeyResource1" title="Column" />
                                                                <asp:TextBox id="txtFakeValidOrderCol" runat="server" CssClass="NoShow" title="Column"></asp:TextBox>
                                                                <asp:CustomValidator ID="cvtxtFakeValidOrderCol" runat="server" ValidationGroup="OrderGroup"
                                                                    OnServerValidate="IsOrderingColumnValid" ControlToValidate="txtFakeValidOrderCol" 
                                                                    EnableClientScript="False" ErrorMessage="Order by Column must be unique." ValidateEmptyText="True">
                                                                    *</asp:CustomValidator>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </ItemTemplate>
                                                <%--<FooterTemplate>
                                                    <asp:Label ID="lblNewKey" runat="server" CssClass="NoShow" Text="<%$ Resources:LogiAdHoc, Column %>" AssociatedControlID="NewKey" meta:resourcekey="lblNewKeyResource1" ></asp:Label>
                                                    <asp:DropDownList ID="NewKey" runat="server" meta:resourcekey="NewKeyResource1" />
                                                </FooterTemplate>--%>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Direction" meta:resourcekey="grdOrderDirection">
                                                <HeaderStyle Width="180px"></HeaderStyle>
                                                <ItemTemplate>
                                                    <asp:RadioButton ID="SortOrder1" GroupName="SortOrder" Text="Ascending" runat="server"
                                                        AutoPostBack="True" OnCheckedChanged="Order_Changed" meta:resourcekey="SortOrder1Resource1" />
                                                    <asp:RadioButton ID="SortOrder2" GroupName="SortOrder" Text="Descending" runat="server"
                                                        AutoPostBack="True" OnCheckedChanged="Order_Changed" meta:resourcekey="SortOrder2Resource1" />
                                                </ItemTemplate>
                                                <%--<FooterTemplate>
                                                    <asp:RadioButton ID="NewSort1" GroupName="SortOrder" Text="Ascending" Checked="True"
                                                        runat="server" meta:resourcekey="NewSort1Resource1" />
                                                    <asp:RadioButton ID="NewSort2" GroupName="SortOrder" Text="Descending" runat="server"
                                                        meta:resourcekey="SortOrder2Resource1" />
                                                </FooterTemplate>--%>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="<%$ Resources:LogiAdHoc, Actions %>">
                                                <ItemStyle HorizontalAlign="Center" Wrap="False" />
                                                <ItemTemplate>
                                                    <%--<asp:ImageButton ID="imgMoveupOrder" runat="server" AlternateText="Move Order Column Up"
                                                        CausesValidation="False" CommandName="OrderMoveUp" ImageUrl="../ahImages/SmallArrowUp.gif"
                                                        meta:resourcekey="imgMoveupOrderResource1" ToolTip="Move Order Column Up" />
                                                    <asp:ImageButton ID="imgMovedownOrder" runat="server" AlternateText="Move Order Column Down"
                                                        CausesValidation="False" CommandName="OrderMoveDown" ImageUrl="../ahImages/SmallArrowDown.gif"
                                                        meta:resourcekey="imgMovedownOrderResource1" ToolTip="Move Order Column Down" />
                                                    &nbsp;--%>
                                                    <asp:ImageButton ID="RemoveOrder" runat="server" AlternateText="<%$ Resources:LogiAdHoc, RemoveItem %>"
                                                        CommandName="Remove" ImageUrl="../ahImages/remove.gif" ToolTip="<%$ Resources:LogiAdHoc, RemoveItem %>" />
                                                </ItemTemplate>
                                                <%--<FooterStyle HorizontalAlign="Center"></FooterStyle>
                                                <FooterTemplate>
                                                    <asp:ImageButton ID="AddItem" runat="server" AlternateText="<%$ Resources:LogiAdHoc, SaveItem %>"
                                                        CausesValidation="False" CommandName="Add" ImageUrl="../ahImages/save.gif" ToolTip="<%$ Resources:LogiAdHoc, SaveItem %>" />
                                                    <asp:ImageButton ID="Cancel" runat="server" AlternateText="<%$ Resources:LogiAdHoc, Cancel %>"
                                                        CausesValidation="False" CommandName="Cancel" ImageUrl="../ahImages/cancel.gif"
                                                        ToolTip="<%$ Resources:LogiAdHoc, Cancel %>" />
                                                </FooterTemplate>--%>
                                            </asp:TemplateField>
                                        </Columns>
                                        <%--<FooterStyle CssClass="gridfooter" />--%>
                                        <HeaderStyle CssClass="gridheader" />
                                        <RowStyle CssClass="gridrow" Height="29px"/>
                                        <AlternatingRowStyle CssClass="gridalternaterow"  Height="29px" ></AlternatingRowStyle>
                                    </AdHoc:DDGridView>
                                        </div>
                                    <%--</div>--%>
                                    <AdHoc:LogiButton ID="AddOrderRow" OnClick="AddOrderRow_OnClick" Text="Add a Column"
                                        runat="server" meta:resourcekey="AddOrderRowResource1" />
                                    <p class="info">
                                        <asp:Label ID="lblOrderMaxRow" runat="server" meta:resourcekey="lblOrderMaxRowResource1" />
                                    </p>
                                    <br />
                                    <p>
                                        <asp:Label ID="Label28" runat="server" meta:resourcekey="LiteralResource28" Text="Return first" AssociatedControlID="TopN"></asp:Label>
                                        <asp:TextBox ID="TopN" runat="server" MaxLength="7" AutoPostBack="True" meta:resourcekey="TopNResource1" />
                                        <asp:Localize ID="Localize29" runat="server" meta:resourcekey="LiteralResource29" Text="rows."></asp:Localize>
                                        <asp:RangeValidator ID="rvMaxRecords" runat="server" ValidationGroup="OrderGroup" ErrorMessage="Max Records accepts only integer values."
                                            ControlToValidate="TopN" Type="Integer" MinimumValue="-1" MaximumValue="9999999"
                                            meta:resourcekey="rvMaxRecordsResource1">*</asp:RangeValidator>
                                    </p>
                                    <asp:ValidationSummary ID="ValidationSummary1" runat="server" ValidationGroup="OrderGroup"
                                        meta:resourcekey="ValidationSummary1Resource1" />
                                    <asp:Label ID="lblSemicolon1" runat="server" OnPreRender="lblSemicolon_PreRender" />
                                    <%--<p class="help">
                                    <a href="../Help.aspx?src=6" target="_blank">
                                    <asp:Localize ID="GetHelpOrder" runat="server" meta:resourcekey="GetHelpOrder" Text="Get help ordering data."></asp:Localize>
                                    </a></p>--%>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </asp:Panel>
                    </div>
                    <div id="divTFilter" runat="server" class="tab_data">
                        <asp:Panel ID="pnlFilters" runat="server" Height="100%" Width="100%" meta:resourcekey="pnlFiltersResource1">
                            <asp:UpdatePanel ID="UP4" runat="server" UpdateMode="Conditional" RenderMode="inline">
                                <ContentTemplate>
                                    <%--<div style="background-color: White; border: solid 1px DarkGray;">--%>
                                    <div style="height: 300px; width: 100%; overflow: auto;">
                                        <asp:GridView ID="grdParameter" runat="server" AutoGenerateColumns="False" OnRowDataBound="OnParamItemDataBoundHandler"
                                        OnRowCommand="OnParamItemCommandHandler" CssClass="grid" meta:resourcekey="grdParameterResource1">
                                        <HeaderStyle CssClass="gridheader"></HeaderStyle>
                                        <RowStyle CssClass="gridrow" Height="29px" Wrap="false"/>
                                        <AlternatingRowStyle CssClass="gridalternaterow"  Height="29px" Wrap="false" ></AlternatingRowStyle>
                                        <Columns>
                                            <asp:TemplateField HeaderText="Move" meta:resourcekey="grdParameterMove">
                                                <ItemTemplate>
                                                    <input type="hidden" id="ParamID" runat="server" />
                                                    <input type="hidden" id="ParamType" runat="server" />
                                                    <table>
                                                        <tr>
                                                            <td style="border: 0px; padding: 0px;">
                                                                <asp:Label ID="Spacer" runat="server" meta:resourcekey="SpacerResource1"></asp:Label>
                                                            </td>
                                                            <td style="border: 0px; padding: 0px;">
                                                                <table cellspacing="0" cellpadding="0" border="0">
                                                                    <tr>
                                                                        <td rowspan="2" style="border: 0px; padding: 0px;">
                                                                            <asp:ImageButton ID="moveleft" runat="server" AlternateText="Move Parameter Left"
                                                                                CausesValidation="False" CommandName="left" ImageUrl="../ahImages/arrowLeft.gif"
                                                                                meta:resourcekey="moveleftResource1" ToolTip="Move Parameter Left" />
                                                                        </td>
                                                                        <td style="border: 0px; padding: 1px 0px;">
                                                                            <asp:ImageButton ID="moveup" runat="server" AlternateText="Move Parameter Up" CausesValidation="False"
                                                                                CommandName="up" ImageUrl="../ahImages/arrowUp.gif" meta:resourcekey="moveupResource1"
                                                                                ToolTip="Move Parameter Up" />
                                                                        </td>
                                                                        <td rowspan="2" style="border: 0px; padding: 0px;">
                                                                            <asp:ImageButton ID="moveright" runat="server" AlternateText="Move Parameter Right"
                                                                                CausesValidation="False" CommandName="right" ImageUrl="../ahImages/arrowRight.gif"
                                                                                meta:resourcekey="moverightResource1" ToolTip="Move Parameter Right" />
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td style="border: 0px; padding: 1px 0px;">
                                                                            <asp:ImageButton ID="movedown" runat="server" AlternateText="Move Parameter Down"
                                                                                CausesValidation="False" CommandName="down" ImageUrl="../ahImages/arrowDown.gif"
                                                                                meta:resourcekey="movedownResource1" ToolTip="Move Parameter Down" />
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                            <td style="border: 0px; padding: 0px;">
                                                                <span>&nbsp;&nbsp;<asp:Label ID="lblBitCmd1" runat="server" meta:resourcekey="lblBitCmdResource1"></asp:Label>
                                                                </span>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </ItemTemplate>
                                                <ItemStyle VerticalAlign="Top" />
                                                <FooterStyle VerticalAlign="Top" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Column Name" meta:resourcekey="grdParameterColumnName">
                                                <HeaderStyle Width="200px"></HeaderStyle>
                                                <ItemTemplate>
                                                    <asp:Label ID="lblOpenParen" runat="server" meta:resourcekey="lblOpenParenResource1"></asp:Label>
                                                    <asp:Label ID="lblColumnName" runat="server" meta:resourcekey="lblColumnNameResource1"></asp:Label>
                                                </ItemTemplate>
                                                <ItemStyle VerticalAlign="Top" />
                                                <FooterStyle VerticalAlign="Top" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="<%$ Resources:LogiAdHoc, Op_Operator %>">
                                                <HeaderStyle Width="170px"></HeaderStyle>
                                                <ItemTemplate>
                                                    <asp:Label ID="lblOperator" runat="server" meta:resourcekey="lblOperatorResource1"></asp:Label>
                                                </ItemTemplate>
                                                <ItemStyle VerticalAlign="Top" />
                                                <FooterStyle VerticalAlign="Top" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="<%$ Resources:LogiAdHoc, Value %>">
                                                <HeaderStyle Width="200px"></HeaderStyle>
                                                <ItemTemplate>
                                                    <asp:Label ID="lblValue" runat="server" meta:resourcekey="lblValueResource1"></asp:Label>
                                                    <asp:Label ID="lblCloseParen" runat="server" meta:resourcekey="lblCloseParenResource1"></asp:Label>
                                                </ItemTemplate>
                                                <ItemStyle VerticalAlign="Top" />
                                                <FooterStyle VerticalAlign="Top" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Ask" meta:resourcekey="grdParameterAsk">
                                                <HeaderStyle Width="20px"></HeaderStyle>
                                                <ItemTemplate>
                                                    <asp:Label ID="lblDoAsk" runat="server" meta:resourcekey="lblDoAskResource1"></asp:Label>
                                                </ItemTemplate>
                                                <ItemStyle VerticalAlign="Top" />
                                                <FooterStyle VerticalAlign="Top" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="<%$ Resources:LogiAdHoc, Caption %>">
                                                <HeaderStyle Width="100px"></HeaderStyle>
                                                <ItemTemplate>
                                                    <asp:Label ID="lblParamCaption" runat="server" meta:resourcekey="lblParamCaptionResource1"></asp:Label>
                                                </ItemTemplate>
                                                <ItemStyle VerticalAlign="Top" />
                                                <FooterStyle VerticalAlign="Top" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="<%$ Resources:LogiAdHoc, Actions %>">
                                                <HeaderStyle Width="50px"></HeaderStyle>
                                                <ItemStyle HorizontalAlign="Center" VerticalAlign="Top" Wrap="False" />
                                                <ItemTemplate>
                                                    <asp:ImageButton AlternateText="Edit Item" ImageUrl="../ahImages/modify.gif" ID="EditItem"
                                                        ToolTip="Edit" CommandName="EditParam" CausesValidation="False" runat="server" meta:resourcekey="EditItemResource1" />
                                                    <asp:ImageButton AlternateText="<%$ Resources:LogiAdHoc, RemoveItem %>" ImageUrl="../ahImages/remove.gif" ID="RemoveParam"
                                                        ToolTip="Remove" CommandName="RemoveParam" CausesValidation="False" runat="server" meta:resourcekey="RemoveItemResource1" />
                                                </ItemTemplate>
                                                <FooterStyle VerticalAlign="Top" />
                                            </asp:TemplateField>
                                        </Columns>
                                        <FooterStyle CssClass="gridfooter" />
                                        <HeaderStyle CssClass="gridheader" />
                                    </asp:GridView>
                                    </div>
                                    <%--</div>--%>
                                    <br />
                                    <AdHoc:LogiButton ID="AddParamRow" OnClick="AddParamRow_OnClick" Text="Add a Parameter"
                                        runat="server" meta:resourcekey="AddParamRowResource1" />
                                    <%--<p class="help">
                                    <a href="../Help.aspx?src=7" target="_blank">
                                    <asp:Localize ID="GetHelpSettingParams" runat="server" meta:resourcekey="GetHelpSettingParams" Text="Get help with setting parameters."></asp:Localize>
                                    </a></p>--%>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </asp:Panel>
                    </div>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
        
        <asp:UpdatePanel ID="UPShowDataPopup" runat="server" UpdateMode="Conditional" RenderMode="inline">
            <ContentTemplate>
                <input type="hidden" id="ihShowDataObjectID" runat="server" value="0" />
                <asp:Button ID="btnShowDataFake" runat="server" Style="display: none" 
                    OnClick="ShowDataForObject" CausesValidation="False" meta:resourcekey="btnShowDataFakeResource1"/>
                <asp:Button runat="server" ID="btnShowDataPopup" Style="display: none" 
                    CausesValidation="False" meta:resourcekey="btnShowDataPopupResource1"/>
                <asp:Button runat="server" ID="Button3" Style="display: none" meta:resourcekey="Button1Resource1" />
                <ajaxToolkit:ModalPopupExtender runat="server" ID="ahShowDataPopup" BehaviorID="ahShowDataPopupBehavior"
                    TargetControlID="Button3" PopupControlID="pnlShowDataPopup" BackgroundCssClass="modalBackground" 
                    Enabled="True">
                </ajaxToolkit:ModalPopupExtender>
                
                <asp:Panel runat="server" CssClass="modalPopup" ID="pnlShowDataPopup" Style="display: none; width: 756px;" meta:resourcekey="ahPopupResource1">
                    <asp:Panel ID="pnlDragHandle" runat="server" meta:resourcekey="pnlDragHandleResource1">
                    <div class="modalPopupHandle" style="width: 750px; cursor: default;">
                        <table cellpadding="0" cellspacing="0" style="width: 100%;"><tr><td>
                            <asp:UpdatePanel ID="UPShowDataHeader" runat="server" UpdateMode="Conditional" RenderMode="Inline">
                                <ContentTemplate>
                                    <asp:Label ID="lblShowDataHeader" runat="server" Text="Data" ></asp:Label>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                            </td>
                            <td style="width: 20px;">
                                <asp:ImageButton ID="imgClosePopup" runat="server" 
                                    OnClick="imgClosePopup_Click" CausesValidation="False"
                                    SkinID="imgbClose" ImageUrl="../ahImages/remove.gif" 
                                    AlternateText="<%$ Resources:LogiAdHoc, CloseWindow %>" />
                        </td></tr></table>
                    </div>
                    </asp:Panel>
                    <div class="modalDiv">
                    <div id="divPreviewInfo" runat="server">
                    <p class="info">
                        <asp:Localize ID="localize10" runat="server" Text="<%$ Resources:Errors, Wrn_LimitedPreview %>"></asp:Localize>
                    </p>
                    </div>
                    
                    <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional" RenderMode="inline">
                        <ContentTemplate>
                            <p></p>
                            <% 
                                Dim browserType As System.Web.HttpBrowserCapabilities = Request.Browser
                                Dim overflowText As String = "auto"
                                
                                If (browserType.Type = "IE9") Then
                                    overflowText = "scroll"
                                End If
                            %>
                            <div class="showDataPopup" style="width: 725px; overflow:<%= overflowText %>;">
                                <asp:GridView ID="grdData" runat="server" CssClass="grid" 
                                    meta:resourcekey="grdDataResource1" >
                                    <HeaderStyle CssClass="gridheader"></HeaderStyle>
                                    <RowStyle CssClass="gridrow" Height="29px" Wrap="false"/>
                                    <AlternatingRowStyle CssClass="gridalternaterow"  Height="29px" Wrap="false" ></AlternatingRowStyle>
                                </asp:GridView>
                            </div>
                            <div id="divDataError" runat="server">
                                <ul class="validation_error" id="UlDatError" runat="server">
                                    <li>
                                        <asp:Label ID="lblDataError" runat="server" ></asp:Label>
                                    </li>
                                </ul>
                            </div>
                            <div id="divValidationMessage" runat="server">
                                <asp:Label ID="lblTestValidationMessage" runat="server"></asp:Label>
                            </div>
                            <p></p>
                            <!-- Buttons -->
                            <table>
                                <tr>
                                    <td colspan="2">
                                        <AdHoc:LogiButton ID="btnShowDataSelectNCont" OnClick="btnShowDataSelectAndCont_OnClick" runat="server"
                                            Text="Select and Continue" UseSubmitBehavior="False" CausesValidation="False" meta:resourcekey="btnShowDataOKResource1" />
                                        <AdHoc:LogiButton ID="btnShowDataClose" OnClick="btnShowDataClose_OnClick"
                                            runat="server" Text="Close" CausesValidation="False" meta:resourcekey="btnShowDataCancelResource1" />
                                    </td>
                                </tr>
                            </table>
                        </ContentTemplate>
                        <Triggers>
                            <asp:PostBackTrigger ControlID="btnShowDataSelectNCont" />
                        </Triggers>
                    </asp:UpdatePanel>
                    </div>
                </asp:Panel>
            </ContentTemplate>
        </asp:UpdatePanel>

        <asp:UpdatePanel ID="UPParamDetails" runat="server" UpdateMode="Conditional" RenderMode="inline">
            <ContentTemplate>
                 <asp:Button runat="server" ID="btnFakeParamDetails" Style="display: none" meta:resourcekey="btnFakeParamDetailsResource1" />
                <ajaxToolkit:ModalPopupExtender runat="server" ID="mpeParamDetails" BehaviorID="mpeParamDetailsBehavior"
                    TargetControlID="btnFakeParamDetails" PopupControlID="pnlParamDetails" BackgroundCssClass="modalBackground" 
                    DynamicServicePath="" Enabled="True">
                </ajaxToolkit:ModalPopupExtender>
                <asp:Panel runat="server" CssClass="modalPopup" ID="pnlParamDetails" Style="display: none; width: 606;" meta:resourcekey="pnlParamDetailsResource1">
                    <asp:Panel ID="pnlDragHandle8" runat="server" meta:resourcekey="pnlDragHandle8Resource1">
                        <div class="modalPopupHandle" style="width: 600px; cursor: default;">
                            <table cellpadding="0" cellspacing="0" style="width: 100%"><tr><td>
                                    <asp:Localize ID="Localize22" runat="server" Text="<%$ Resources:LogiAdHoc, ParameterDetails %>"></asp:Localize>
                                </td>
                                <td style="width: 20px;">
                                    <asp:ImageButton ID="imgClosePopup8" runat="server" 
                                        OnClick="imgClosePopup8_Click" CausesValidation="False"
                                        SkinID="imgbClose" ImageUrl="../ahImages/remove.gif" 
                                        AlternateText="<%$ Resources:LogiAdHoc, CloseWindow %>" />
                            </td></tr></table>
                        </div>
                        </asp:Panel>
                    <div id="divParamDetails" runat="server" class="modalDiv">
                        <asp:UpdatePanel ID="UpdatePanel15" runat="server" RenderMode="inline">
                            <ContentTemplate>
                                <asp:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Conditional" RenderMode="inline">
                                    <ContentTemplate>
                                        <input type="hidden" id="NewParamID" runat="server" />
                                        <input type="hidden" id="ihParamType" runat="server" />
                                        <input type="hidden" id="ihParamDataTypeCategory" runat="server" />
                                        <input type="hidden" id="ConfirmParamModify" runat="server" />
                                        <div id="divFixedParamSettings" runat="server">
                                        <table cellspacing="0" cellpadding="1" border="0">
                                            <tr>
                                                <td width="100px">
                                                </td>
                                                <td>
                                                    <asp:Label ID="lblBitCmd" runat="server" CssClass="NoShow" Text="Boolean Operator" AssociatedControlID="BitCmd" meta:resourcekey="lblBitCmdResource1"></asp:Label>
                                                    <asp:DropDownList ID="BitCmd" runat="server" meta:resourcekey="BitCmdResource2">
                                                        <asp:ListItem Value="And" meta:resourcekey="ListItemResource1"></asp:ListItem>
                                                        <asp:ListItem Value="Or" meta:resourcekey="ListItemResource2"></asp:ListItem>
                                                    </asp:DropDownList>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:Label ID="Label46" runat="server" Text="<%$ Resources:LogiAdHoc, Column %>" AssociatedControlID="ColumnID" meta:resourcekey="Label46Resource1"></asp:Label>:
                                                </td>
                                                <td>
                                                    <asp:DropDownList ID="ColumnID" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ColumnID_OnChangeHandler"
                                                        meta:resourcekey="ColumnIDResource1" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td valign="top">
                                                    <asp:Label ID="Label45" runat="server" Text="<%$ Resources:LogiAdHoc, Op_Operator %>" AssociatedControlID="ddlOperator" meta:resourcekey="Label45Resource1"></asp:Label>:
                                                </td>
                                                <td valign="top">
                                                    <asp:DropDownList ID="ddlOperator" runat="server" AutoPostBack="True" OnSelectedIndexChanged="Operator_OnChangeHandler"
                                                        meta:resourcekey="ddlOperatorResource1" />
                                                </td>
                                            </tr>
                                        </table>
                                        </div>
                                        <div id="divReqFilterSettings" runat="server">
                                            <table>
                                                <tr>
                                                    <td width="100px">
                                                        <asp:Localize ID="Localize52" runat="server" Text="Parameter Name"></asp:Localize>:
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="lblReqParameterName" runat="server" ></asp:Label>
                                                    </td>        
                                                </tr>
                                            </table>
                                        </div>
                                        <div id="divValues" runat="server">
                                            <table cellspacing="0" cellpadding="1" border="0">
                                                <tr>
                                                    <td id="tdValueLabel" runat="server" valign="top" width="100px">
                                                        <asp:Localize ID="Localize47" runat="server" Text="<%$ Resources:LogiAdHoc, Value %>"></asp:Localize>:
                                                    </td>
                                                    <td valign="top">
                                                        <span id="spFixedParamValues" runat="server">
                                                        <span id="spnFV1" runat="server">
                                                            <wizard:SpecialValue ID="sv1" runat="server" />
                                                            <asp:ImageButton ID="btnFV1" runat="server" AlternateText="<%$ Resources:LogiAdHoc, PickValueFromDB %>"
                                                                CausesValidation="False" ImageUrl="../ahImages/iconFind.gif" OnClick="PickFromDatabase"
                                                                ToolTip="<%$ Resources:LogiAdHoc, PickValueFromDB %>" />
                                                        </span>
                                                        <asp:Label ID="lblTo" runat="server" Text="<%$ Resources:LogiAdHoc, Res_And %>" meta:resourcekey="lblToResource1"></asp:Label>
                                                        <div id="spnFV2" runat="server">
                                                            <wizard:SpecialValue ID="sv2" runat="server" />
                                                            <asp:ImageButton ID="btnFV2" runat="server" AlternateText="<%$ Resources:LogiAdHoc, PickValueFromDB %>"
                                                                CausesValidation="False" ImageUrl="../ahImages/iconFind.gif" OnClick="PickFromDatabase"
                                                                ToolTip="<%$ Resources:LogiAdHoc, PickValueFromDB %>" />
                                                        </div>
                                                        <span id="spnFV3" runat="server">
                                                            <wizard:SpecialValue ID="sv3" runat="server" ShowAsList="True" />
                                                            <%--<asp:Label ID="lbltxaTextArea" runat="server" CssClass="NoShow" Text="Values" AssociatedControlID="txaTextArea" meta:resourcekey="lbltxaTextAreaResource1"></asp:Label>
                                                            <asp:TextBox ID="txaTextArea" runat="server" TextMode="MultiLine" Rows="8" meta:resourcekey="txaTextAreaResource1" />--%>
                                                            <asp:ImageButton ID="btnFV3" runat="server" AlternateText="<%$ Resources:LogiAdHoc, PickValuesFromDB %>"
                                                                CausesValidation="False" ImageUrl="../ahImages/iconFind.gif" OnClick="PickFromDatabase"
                                                                ToolTip="<%$ Resources:LogiAdHoc, PickValueFromDB %>" />
                                                        </span>
                                                        <asp:Label ID="lblChkBitValue" runat="server" CssClass="NoShow" Text="<%$ Resources:LogiAdHoc, BitValue %>" AssociatedControlID="chkBitValue" meta:resourcekey="lblChkBitValueResource1"></asp:Label>
                                                        <asp:CheckBox ID="chkBitValue" runat="server" meta:resourcekey="chkBitValueResource1" />
                                                        <asp:Label ID="lblddlFilter" runat="server" CssClass="NoShow" Text="Cascading Filter" AssociatedControlID="ddlFilter" meta:resourcekey="lblddlFilterResource1"></asp:Label>
                                                        <asp:DropDownList ID="ddlFilter" runat="server" meta:resourcekey="ddlFilterResource1" />
                                                        </span>
                                                        <asp:CustomValidator ID="cvValidParam" OnServerValidate="IsParameterValid" ControlToValidate="ColumnID"
                                                            ValidationGroup="ParamGroup" EnableClientScript="False" ErrorMessage="This parameter leads to an invalid query."
                                                            runat="server" meta:resourcekey="cvValidParamResource1">*</asp:CustomValidator>
                                                        <span id="spRequiredFilterValues" runat="server">
                                                            <wizard:SpecialValue ID="svReqFiltVal" runat="server" AllowColumnSelection="False" AllowAllValuesSelection="False" AllowFilterSelection="False" />
                                                        </span>
                                                    </td>
                                                    <td id="tdFindValues" valign="top" runat="server">
                                                        <table>
                                                            <tr>
                                                                <td>
                                                                    <wizard:DatabaseValues id="dbValues" runat="server" />
                                                                </td>
                                                                <td>
                                                                    <AdHoc:LogiButton ID="btnFVOk" Width="55px" runat="server" Text="<%$ Resources:LogiAdHoc, OKWithSpaces %>"
                                                                        OnClick="btnFVOk_Click" CausesValidation="False" meta:resourcekey="btnFVOkResource1" />
                                                                    <br />
                                                                    <AdHoc:LogiButton ID="btnFVCancel" Width="55px" runat="server" Text="<%$ Resources:LogiAdHoc, Cancel %>"
                                                                        OnClick="btnFVCancel_Click" CausesValidation="False" meta:resourcekey="btnFVCancelResource1" />
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td width="100px">
                                                        <asp:Label ID="Label48" runat="server" meta:resourcekey="LiteralResource48" 
                                                            Text="Ask in Report:" AssociatedControlID="chkAsk"></asp:Label></td>
                                                    <td>
                                                        <asp:CheckBox ID="chkAsk" runat="server" AutoPostBack="True" meta:resourcekey="chkAskResource1" />                                                                    
                                                    </td>
                                                </tr>
                                            </table>
                                            <table id="tbAsk" runat="server" cellspacing="0" cellpadding="1" border="0">
                                                <tr runat="server">
                                                    <td width="100px" runat="server">
                                                        <asp:Label ID="Label51" runat="server" Text="<%$ Resources:LogiAdHoc, Caption %>" AssociatedControlID="txtParamCaption"></asp:Label>:
                                                    </td>
                                                    <td width="200px" runat="server">
                                                        <asp:TextBox ID="txtParamCaption" runat="server"></asp:TextBox>
                                                    </td>
                                                </tr>
                                                <tr runat="server">
                                                    <td width="100px" runat="server">
                                                        <asp:Label ID="Label49" runat="server" meta:resourcekey="LiteralResource49" Text="Control Type:" AssociatedControlID="ddlControlType"></asp:Label>
                                                    </td>
                                                    <td width="200px" runat="server">
                                                        <asp:DropDownList ID="ddlControlType" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlControlType_OnChangeHandler">
                                                        </asp:DropDownList>
                                                    </td>
                                                </tr>
                                                <tr id="trParamOfferAllOption" runat="server">
                                                    <td width="100px" runat="server">
                                                        <asp:Label ID="Label167" runat="server" meta:resourcekey="LiteralResource167"
                                                            Text="Offer &quot;All&quot; option:" AssociatedControlID="chkParamOfferAllOption"></asp:Label>
                                                    </td>
                                                    <td width="300px" runat="server">
                                                        <asp:CheckBox ID="chkParamOfferAllOption" runat="server" />
                                                    </td>
                                                </tr>
                                            </table>
                                            <div id="divParamDisplayMode" runat="server">
                                                <table id="tbParamDispMode" runat="server" cellspacing="0" cellpadding="1" border="0">
                                                    <tr id="trParamDisplayMode" runat="server">
                                                        <td id="Td1" width="100px" runat="server">
                                                        <asp:Localize ID="Localize50" runat="server" meta:resourcekey="LiteralResource50" Text="Display this parameter:"></asp:Localize>
                                                        </td>
                                                        <td id="Td2" width="300px" runat="server">
                                                            <asp:RadioButtonList ID="ParamDisplayMode" runat="server">
                                                                <asp:ListItem Text="Next to the previous parameter" Value="0" meta:resourcekey="pdmNext"></asp:ListItem>
                                                                <asp:ListItem Selected="True" Text="In a new line" Value="1" meta:resourcekey="pdmLine"></asp:ListItem>
                                                            </asp:RadioButtonList>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </div>
                                        </div>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                                <p class="info" id="info" runat="server">
                                <asp:Localize ID="Localize70" runat="server" Text="<%$ Resources:LogiAdHoc, UseEnterKey %>"></asp:Localize>
                                </p>
                                <!-- Buttons -->
                                <table>
                                    <tr>
                                        <td colspan="2">
                                            <AdHoc:LogiButton ID="SaveParam" OnClick="SaveParam_OnClick" runat="server"
                                                Text="<%$ Resources:LogiAdHoc, OKWithSpaces %>" meta:resourcekey="SaveParamResource1" />
                                            <AdHoc:LogiButton ID="CancelParam" OnClick="CancelParam_OnClick" runat="server"
                                                Text="<%$ Resources:LogiAdHoc, Cancel %>" CausesValidation="False" meta:resourcekey="CancelParamResource1" />
                                        </td>
                                        <td width="250px" align="right">
                                            <AdHoc:LogiButton ID="TestParam" OnClick="TestParam_OnClick" Text="Validate Parameter"
                                                runat="server" meta:resourcekey="TestParamResource1" />
                                        </td>
                                    </tr>
                                </table>
                                <ul class="validation_error" id="ErrorList" runat="server">
                                    <li>
                                        <asp:Label ID="lblValidationMessage" runat="server" meta:resourcekey="lblValidationMessageResource1"></asp:Label>
                                    </li>
                                </ul>
                                <asp:ValidationSummary ID="ValidationSummary3" runat="server" ValidationGroup="ParamGroup"
                                    meta:resourcekey="ValidationSummary3Resource1" />
                                <asp:Label ID="lblSemicolon3" runat="server" OnPreRender="lblSemicolon_PreRender" />
                                <p id="pParamValidationDone" runat="server">
                                    <asp:Label ID="lblParamValidationDone" runat="server" meta:resourcekey="lblParamValidationDoneResource1"></asp:Label>
                                </p>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </div>
                </asp:Panel>
            </ContentTemplate>
        </asp:UpdatePanel>
    
        <asp:UpdatePanel Id="UPCalcColAddFunction" runat="server" UpdateMode="Conditional" RenderMode="inline">
            <ContentTemplate>
                <asp:Button runat="server" ID="btnFakeCalcColFuncDetails" Style="display: none" meta:resourcekey="btnFakeParamDetailsResource1" />
                <ajaxToolkit:ModalPopupExtender runat="server" ID="mpeCalcColFuncDetails" BehaviorID="mpeCalcColFuncDetailsBehavior"
                    TargetControlID="btnFakeCalcColFuncDetails" PopupControlID="pnlCalcColFuncDetails" BackgroundCssClass="modalBackground" >
                </ajaxToolkit:ModalPopupExtender>
                <asp:Panel runat="server" CssClass="modalPopup" ID="pnlCalcColFuncDetails" Style="display: none; width: 736;" meta:resourcekey="pnlParamDetailsResource1">
                    <asp:Panel ID="pnlDragHandle9" runat="server" meta:resourcekey="pnlDragHandle8Resource1">
                        <div class="modalPopupHandle" style="width: 730px; cursor: default;">
                            <table cellpadding="0" cellspacing="0" style="width: 100%"><tr><td>
                                    <asp:Label ID="lblCalcColFuncDetails" runat="server" />
                                </td>
                                <td style="width: 20px;">
                                    <asp:ImageButton ID="imgClosePopup9" runat="server" 
                                        OnClick="imgClosePopup9_Click" CausesValidation="False"
                                        SkinID="imgbClose" ImageUrl="../ahImages/remove.gif" 
                                        AlternateText="<%$ Resources:LogiAdHoc, CloseWindow %>" />
                            </td></tr></table>
                        </div>
                    </asp:Panel>
                    <div id="divCalcColumnFuncDetails" runat="server" class="modalDiv">
                        <asp:UpdatePanel ID="UpdatePanel16" runat="server" UpdateMode="COnditional" RenderMode="inline">
                            <ContentTemplate>
                                <p>
                                    <asp:Label ID="lblFunctionInfoText" runat="server" />
                                </p>
                                <table>
                                    <tr>
                                        <td valign="top" width="150px">
                                            <div class="divColumnTreeView" style="width:140px; height: 200px;" >
                                                <cc1:DataSourceTreeView ID="trvCalcFuncColumns" runat="server" meta:resourcekey="trvColumnsResource1">
                                                </cc1:DataSourceTreeView>
                                            </div>
                                        </td>
                                        <td valign="top">
                                            <input type="hidden" id="ihDefTxtCtrl" runat="server" />
                                             <div style="padding: 4px; margin-right:10px; height: 200px; overflow: auto;">
                                                <asp:GridView ID="grdCalcColParams" runat="server" AutoGenerateColumns="False" 
                                                    OnRowDataBound="OnRowDataBoundHandler" GridLines="None" ShowFooter="False" 
                                                    ShowHeader="False" CellPadding="5" CellSpacing="5" >
                                                    <Columns>
                                                        <asp:TemplateField HeaderText="Parameter">
                                                            <ItemTemplate>
                                                                <input type="hidden" id="ihParameterID" runat="server" />
                                                                <input type="hidden" id="ihParameterType" runat="server" />
                                                                <asp:Label ID="lblParamName" runat="server"></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <%--<asp:TemplateField HeaderText="ValueType">
                                                            <ItemTemplate>
                                                                <asp:DropDownList ID="ddlValueType" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlValueType_SelectedIndexChanged">
                                                                    <asp:ListItem Text="Column" Value="1"></asp:ListItem>
                                                                    <asp:ListItem Text="Specific Value" Value="0"></asp:ListItem>
                                                                </asp:DropDownList>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>--%>
                                                        <asp:TemplateField HeaderText="Value">
                                                            <ItemTemplate>
                                                                <div id="divTxtValue" runat="server">
                                                                    <asp:TextBox ID="txtValue" runat="server" columns="30" ></asp:TextBox>
                                                                    <asp:HyperLink ID="Calendar" runat="server" ImageUrl="../_Images/calendar.gif" 
                                                                        ToolTip="<%$ Resources:LogiAdHoc, CalendarLinkTooltip %>" 
                                                                        Text="<%$ Resources:LogiAdHoc, CalendarLinkTooltip %>"></asp:HyperLink>
                                                                    <asp:RequiredFieldValidator ID="rtvValidValue" ValidationGroup="CalcColParams" ControlToValidate="txtValue" EnableClientScript="false" 
                                                                        ErrorMessage="Parameter Value is required." runat="server" meta:resourcekey="rtvCalcNameResource1">*</asp:RequiredFieldValidator>
                                                                    <asp:CustomValidator ID="cvValidValue" ValidationGroup="CalcColParams" runat="server" EnableClientScript="false"
                                                                        ErrorMessage="Please enter a valid value for this parameter." ControlToValidate="txtValue" 
                                                                        OnServerValidate="IsCalcParamValueValid" meta:resourcekey="cvUniqueCalcNameResource1">*</asp:CustomValidator>
                                                                </div>
                                                                <div id="divPredefinedValues" runat="server">
                                                                    <asp:DropDownList ID="ddlColumn" runat="server"></asp:DropDownList>
                                                                </div>
                                                                <div id="divRangeValues" runat="server">
                                                                    <asp:UpdatePanel ID="UPRangeVals" runat="server" UpdateMode="Conditional" RenderMode="inline">
                                                                        <ContentTemplate>
                                                                            <asp:GridView ID="grdRangeVals" runat="server" AutoGenerateColumns="False"
                                                                                OnRowDataBound="OnRangeRowDataBoundHandler" CssClass="grid">
                                                                                <Columns>
                                                                                    <asp:TemplateField HeaderText="Operator">
                                                                                        <ItemTemplate>
                                                                                            <input type="hidden" id="ihIsNew" runat="server" />
                                                                                            <asp:Label ID="lblRangeOperator" runat="server" Style="display: block; width: 150px; padding:2px; padding-right: 30px;" />
                                                                                            <asp:Panel ID="pnlOperators" runat="server" Style="display :none; visibility: hidden;">
                                                                                            </asp:Panel>
                                                                                            <ajaxToolkit:DropDownExtender runat="server" ID="DDE"
                                                                                                TargetControlID="lblRangeOperator"
                                                                                                DropDownControlID="pnlOperators" />
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateField>
                                                                                    <asp:TemplateField HeaderText="Value">
                                                                                        <ItemTemplate>
                                                                                            <asp:TextBox ID="txtParamValue" runat="server" columns="20" ></asp:TextBox>
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateField>
                                                                                    <asp:TemplateField HeaderText="Maps to">
                                                                                        <ItemTemplate>
                                                                                            <asp:TextBox ID="txtMapsTo" runat="server" columns="20" ></asp:TextBox>
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateField>
                                                                                    <asp:TemplateField HeaderText="<%$ Resources:LogiAdHoc, Actions %>">
                                                                                        <ItemTemplate>
                                                                                            <asp:ImageButton ID="Remove" ImageURL="../ahImages/remove.gif" AlternateText="Remove"
                                                                                                ToolTip="Remove" runat="server" OnCommand="RangeParamAction" 
                                                                                                CausesValidation="false" meta:resourcekey="ModifyResource1" />
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateField>
                                                                                </Columns>
                                                                            </asp:GridView>
                                                                        </ContentTemplate>
                                                                    </asp:UpdatePanel>
                                                                </div>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                    </Columns>
                                                </asp:GridView>
                                            </div>
                                            <asp:ValidationSummary ID="ValidationSummary6" runat="server" ValidationGroup="CalcColParams" meta:resourcekey="ValidationSummary5Resource1" />
                                            <asp:Label ID="lblSemicolon6" runat="server" OnPreRender="lblSemicolon_PreRender" />
                                        </td>
                                    </tr>
                                </table>
                                <p></p>
                                <br /><table><tr><td>
                                <AdHoc:LogiButton ID="btnCalcColFuncOK" OnClick="btnCalcColFuncOK_OnClick" runat="server" ValidationGroup="CalcColParams"
                                    Text="<%$ Resources:LogiAdHoc, OKWithSpaces %>" UseSubmitBehavior="False" />
                                <AdHoc:LogiButton ID="btnCalcColFuncCancel" OnClick="btnCalcColFuncCancel_OnClick"
                                    runat="server" Text="<%$ Resources:LogiAdHoc, Cancel %>" CausesValidation="False" />
                                </td></tr></table>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </div>
                </asp:Panel>
            </ContentTemplate>
        </asp:UpdatePanel>
        <%--<table>
            <tr>
                <td>
                    <asp:Localize ID="Localize3" runat="server" meta:resourcekey="LiteralResource3" Text="Apply to Empty Elements:"></asp:Localize>
                </td>
                <td>
                    <asp:CheckBox ID="chkApplyChangesToEmptyElements" runat="server" meta:resourcekey="chkApplyChangesToEmptyElementsResource1" />
                </td>
            </tr>
        </table>--%>
        
        <div id="divButtons" class="divButtons">
            <table width="100%">
            <tr>
            <td>
            <AdHoc:LogiButton ID="btnContinue" runat="server" ToolTip="<%$ Resources:LogiAdHoc, OK %>"
                Text="<%$ Resources:LogiAdHoc, OKWithSpaces %>" UseSubmitBehavior="False" />
            <AdHoc:LogiButton ID="btnCancel" runat="server" 
                ToolTip="<%$ Resources:LogiAdHoc, CancelTooltip1 %>"
                Text="<%$ Resources:LogiAdHoc, Cancel %>" CausesValidation="False" />
            </td>
            <td id="tdDSName" runat="server" align="right">
                    <input type="hidden" id="ihLastDataSourceID" runat="server" />
                    <asp:Label ID="lblDataSourceName" runat="server" />
                    <asp:Label ID="Label3" runat="server" CssClass="NoShow" Text="Select Data Source" AssociatedControlID="ddlDataSourceName" />
                    <asp:DropDownList ID="ddlDataSourceName" runat="server" AutoPostBack="True">
                    </asp:DropDownList>
                    &nbsp;
                    <AdHoc:LogiButton ID="btnSaveAs" runat="server" Text="Save As New" CausesValidation="false" meta:resourcekey="btnSaveAs" />
                    <ajaxToolkit:ModalPopupExtender runat="server" ID="mpeSaveAs" BehaviorID="ahSaveAsPopupBehavior" 
                        TargetControlID="btnSaveAs" OkControlID="btnOKSaveAs" CancelControlID="btnCancelSaveAs" 
                        PopupControlID="pnlSaveAs" BackgroundCssClass="modalBackground" RepositionMode="None" 
                        DynamicServicePath="" Enabled="True">
                    </ajaxToolkit:ModalPopupExtender>                                                
                    <asp:Panel runat="server" CssClass="modalPopup" ID="pnlSaveAs" Style="display:none; width:300px;">
                        <div class="modalDiv">
                        <p align="left">
                        <asp:Localize ID="Localize3" runat="server" Text="You are about to save this data source as a new source. It will automatically apply to the element that you are designing and identified with the name that you can give to it." meta:resourcekey="Localize3Resource"></asp:Localize>                        
                        </p>
                        <br />
                        <asp:Label ID="Localize4" runat="server" Text="Name:" AssociatedControlID="txtFriendlyName" meta:resourcekey="Localize4Resource"></asp:Label>
                        <asp:TextBox ID="txtFriendlyName" runat="server" Text="Data Source" Columns="44" />
<%--                        
                        <asp:RequiredFieldValidator ID="rvFriendlyName" ValidationGroup="SaveAs" ControlToValidate="txtFriendlyName"
                            ErrorMessage="Data Source has to have a unique name." runat="server">*</asp:RequiredFieldValidator>
                        <asp:CustomValidator ID="cvFriendlyName" ValidationGroup="SaveAs" runat="server"
                            ErrorMessage="Another data source already uses this name. Please choose a new name."
                            ControlToValidate="txtFriendlyName" OnServerValidate="IsDSNameUnique">*</asp:CustomValidator>
--%>                            
                        <br />
                        <br />
                        <table width="100%"><tr><td align="center">
                        <AdHoc:LogiButton ID="btnOKSaveAs" runat="server"
                            Text="<%$ Resources:LogiAdHoc, OKWithSpaces %>" UseSubmitBehavior="False" CausesValidation="False" />
                        <AdHoc:LogiButton ID="btnCancelSaveAs" runat="server" Text="<%$ Resources:LogiAdHoc, Cancel %>" CausesValidation="False" />
                        </td></tr></table>
                                                 
                        <asp:ValidationSummary ID="ValidationSummary2" runat="server" ValidationGroup="SaveAs" />
                        <%--Adding this label as a temporary workaround to fix a bug in AJAXcontroltoolkit.--%>
                        <asp:Label ID="Label1" runat="server" OnPreRender="lblSemicolon_PreRender" />
                        <ul class="validation_error" id="saveasErrorList" runat="server">
                            <li>
                                <asp:Label ID="saveasError" runat="server"></asp:Label>
                            </li>
                        </ul>
                        </div>
                    </asp:Panel>
            </td>
            <td align="right">
            <AdHoc:LogiButton ID="btnPreviewData" runat="server" Text="Preview Selected Data" CausesValidation="false"
                 OnClick="btnPreviewData_OnClick" UseSubmitBehavior="false" meta:resourcekey="btnPreview" />
            </td>
            </tr>
            </table>
        </div>
    </div>
    </form>
</body>
</html>
