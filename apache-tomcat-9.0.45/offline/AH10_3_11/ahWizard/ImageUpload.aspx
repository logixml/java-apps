<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="ImageUpload.aspx.vb" Inherits="LogiAdHoc.ImageUpload" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Image Upload</title>
</head>
<%--<body style="background-color:#ddd;">--%>
<body class="modalPopupBckgndColor">
    <form id="form2" runat="server" enctype="multipart/form-data">
        <div>
            <asp:Panel ID="pnlTest" runat="server" >
                <asp:Label ID="lblfImageUpload" runat="server" CssClass="NoShow" Text="<%$ Resources:LogiAdHoc, FileUpload %>" AssociatedControlID="fImageUpload" ></asp:Label>
                <asp:FileUpload ID="fImageUpload" runat="server" size="50" /><br /><br />
                <AdHoc:LogiButton ID="btnUploadImage" Text="<%$ Resources:LogiAdHoc, OKWithSpaces %>"
                            CausesValidation="False" runat="server" OnClick="UploadFile" />
                <AdHoc:LogiButton ID="btnCancel" Text="<%$ Resources:LogiAdHoc, Cancel %>"
                            CausesValidation="False" runat="server" OnClick="CancelUpload" />
                <ul runat="server" EnableViewState="false" id="ErrorList" class="validation_error">
                    <li>
                        <asp:Label runat="server" EnableViewState="false" ID="lblErrMessage"></asp:Label>
                    </li>
                </ul>
            </asp:Panel>
        </div>
    </form>
</body>
</html>
