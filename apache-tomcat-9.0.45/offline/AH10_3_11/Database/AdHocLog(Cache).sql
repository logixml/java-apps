CREATE TABLE AdHocLog (
	ActionName varchar (25) NOT NULL ,
	ActionTimeStamp datetime NOT NULL ,
	ActionDuration int NULL ,
	HostAddress varchar (40) NULL ,
	SessionGUID varchar (36) NULL ,
	UserName varchar (50) NULL ,
	GroupName varchar (100) NULL ,
	AffectedUserName varchar (50) NULL ,
	Affected varchar (500) NULL ,
	AppCode varchar (500) NULL ,
	ConnectionLabel varchar (255) NULL ,
	ReportID varchar (255) NULL ,
	ReportName varchar (200) NULL ,
	ReportType varchar (1) NULL ,
	Query text NULL ,
	SessionDuration int NULL 
) 
