SET SCAN OFF;

CREATE OR REPLACE PACKAGE sqlserver_utilities AS
identity NUMBER(10);
END sqlserver_utilities;

/

CREATE SEQUENCE  FolderRole_FolderRoleID_SEQ  
  MINVALUE 1 MAXVALUE 999999999999999999999999 INCREMENT BY 1  NOCYCLE ;

CREATE SEQUENCE  UserScheduleSubscription_Subsc  
  MINVALUE 1 MAXVALUE 999999999999999999999999 INCREMENT BY 1  NOCYCLE ;

CREATE SEQUENCE  Users_UserID_SEQ  
  MINVALUE 1 MAXVALUE 999999999999999999999999 INCREMENT BY 1  NOCYCLE ;

CREATE SEQUENCE  JoinRelation_JoinRelationID_SE  
  MINVALUE 1 MAXVALUE 999999999999999999999999 INCREMENT BY 1  NOCYCLE ;

CREATE SEQUENCE  SessionParameters_SessionParam  
  MINVALUE 1 MAXVALUE 999999999999999999999999 INCREMENT BY 1  NOCYCLE ;

CREATE SEQUENCE  UserGroupSessions_UserGroupSes  
  MINVALUE 1 MAXVALUE 999999999999999999999999 INCREMENT BY 1  NOCYCLE ;

CREATE SEQUENCE  UserSessions_UserSessionID_SEQ  
  MINVALUE 1 MAXVALUE 999999999999999999999999 INCREMENT BY 1  NOCYCLE ;

CREATE SEQUENCE  DatabaseRole_DatabaseRoleID_SE  
  MINVALUE 1 MAXVALUE 999999999999999999999999 INCREMENT BY 1  NOCYCLE ;

CREATE SEQUENCE  UserReport_ID_SEQ  
  MINVALUE 1 MAXVALUE 999999999999999999999999 INCREMENT BY 1  NOCYCLE ;

CREATE SEQUENCE  Templates_TemplateID_SEQ  
  MINVALUE 1 MAXVALUE 999999999999999999999999 INCREMENT BY 1  NOCYCLE ;

CREATE SEQUENCE  Classes_ClassID_SEQ  
  MINVALUE 1 MAXVALUE 999999999999999999999999 INCREMENT BY 1  NOCYCLE ;

CREATE SEQUENCE  ReportSchedules_ReportSchedule  
  MINVALUE 1 MAXVALUE 999999999999999999999999 INCREMENT BY 1  NOCYCLE ;

CREATE SEQUENCE  ReportSessionParameters_Report  
  MINVALUE 1 MAXVALUE 999999999999999999999999 INCREMENT BY 1  NOCYCLE ;

CREATE SEQUENCE  Role_RoleID_SEQ  
  MINVALUE 1 MAXVALUE 999999999999999999999999 INCREMENT BY 1  NOCYCLE ;

CREATE SEQUENCE  JoinRelationDetails_JoinRelati  
  MINVALUE 1 MAXVALUE 999999999999999999999999 INCREMENT BY 1  NOCYCLE ;

CREATE SEQUENCE  Objects_ObjectID_SEQ  
  MINVALUE 1 MAXVALUE 999999999999999999999999 INCREMENT BY 1  NOCYCLE ;

CREATE SEQUENCE  Folder_FolderID_SEQ  
  MINVALUE 1 MAXVALUE 999999999999999999999999 INCREMENT BY 1  NOCYCLE ;

CREATE SEQUENCE  ReportParameterValues_ReportPa  
  MINVALUE 1 MAXVALUE 999999999999999999999999 INCREMENT BY 1  NOCYCLE ;

CREATE SEQUENCE  UserGroups_GroupID_SEQ  
  MINVALUE 1 MAXVALUE 999999999999999999999999 INCREMENT BY 1  NOCYCLE ;

CREATE SEQUENCE  Columns_ColumnID_SEQ  
  MINVALUE 1 MAXVALUE 999999999999999999999999 INCREMENT BY 1  NOCYCLE ;

CREATE SEQUENCE  UserProfile_UserProfileID_SEQ  
  MINVALUE 1 MAXVALUE 999999999999999999999999 INCREMENT BY 1  NOCYCLE ;

CREATE SEQUENCE  ParameterValues_ParameterValue  
  MINVALUE 1 MAXVALUE 999999999999999999999999 INCREMENT BY 1  NOCYCLE ;

CREATE SEQUENCE  ReportParameters_ReportParamet  
  MINVALUE 1 MAXVALUE 999999999999999999999999 INCREMENT BY 1  NOCYCLE ;

CREATE SEQUENCE  CascadeFilterDetails_FilterDet  
  MINVALUE 1 MAXVALUE 999999999999999999999999 INCREMENT BY 1  NOCYCLE ;

CREATE SEQUENCE  RolePermissions_RolePermission  
  MINVALUE 1 MAXVALUE 999999999999999999999999 INCREMENT BY 1  NOCYCLE ;

CREATE SEQUENCE  LinkParameters_LinkParameterID  
  MINVALUE 1 MAXVALUE 999999999999999999999999 INCREMENT BY 1  NOCYCLE ;

CREATE SEQUENCE  UserGroupRoles_UserGroupRoleID  
  MINVALUE 1 MAXVALUE 999999999999999999999999 INCREMENT BY 1  NOCYCLE ;

CREATE SEQUENCE  SourceDatabase_DatabaseID_SEQ  
  MINVALUE 1 MAXVALUE 999999999999999999999999 INCREMENT BY 1  NOCYCLE ;

CREATE SEQUENCE  ColumnExplanation_ExplanationI  
  MINVALUE 1 MAXVALUE 999999999999999999999999 INCREMENT BY 1  NOCYCLE ;

CREATE SEQUENCE  CascadeFilters_FilterID_SEQ  
  MINVALUE 1 MAXVALUE 999999999999999999999999 INCREMENT BY 1  NOCYCLE ;

CREATE SEQUENCE  Categories_CategoryID_SEQ  
  MINVALUE 1 MAXVALUE 999999999999999999999999 INCREMENT BY 1  NOCYCLE ;

CREATE SEQUENCE  DataFormats_FormatID_SEQ  
  MINVALUE 1 MAXVALUE 999999999999999999999999 INCREMENT BY 1  NOCYCLE ;

CREATE SEQUENCE  ObjectParameters_ObjectParamet  
  MINVALUE 1 MAXVALUE 999999999999999999999999 INCREMENT BY 1  NOCYCLE ;

CREATE SEQUENCE  PERMISSIONRIGHTS_PERMISSIONR_1  
  MINVALUE 1 MAXVALUE 999999999999999999999999 INCREMENT BY 1  NOCYCLE ;

CREATE TABLE DataFormats (
  FormatID NUMBER(10,0) NOT NULL,
  FormatKey VARCHAR2(50 CHAR),
  FormatName VARCHAR2(50 CHAR) NOT NULL,
  Format VARCHAR2(255 CHAR) NOT NULL,
  Internal NUMBER(3,0) DEFAULT (0) NOT NULL,
  AppliesTo NUMBER(10,0) DEFAULT (0) NOT NULL,
  Explanation VARCHAR2(255 CHAR),
  ExampleBefore VARCHAR2(50 CHAR),
  ExampleAfter VARCHAR2(50 CHAR),
  IsAvailable NUMBER(3,0) DEFAULT (1) NOT NULL,
  SortOrder NUMBER(10,0) DEFAULT (0)
);


ALTER TABLE DataFormats
ADD CONSTRAINT PK_DataFormats PRIMARY KEY
(
  FormatID
)
ENABLE
;
CREATE INDEX DataFormats_SortOrderIdx ON DataFormats
(
  SortOrder
) 
;

CREATE TABLE CascadeFilterDetails (
  FilterDetailID NUMBER(10,0) NOT NULL,
  FilterID NUMBER(10,0) DEFAULT (0) NOT NULL,
  ObjectID NUMBER(10,0) DEFAULT (0) NOT NULL,
  FilterColumnID NUMBER(10,0),
  DisplayColumnID NUMBER(10,0) NOT NULL,
  ValueColumnID NUMBER(10,0) NOT NULL,
  FilterOrder NUMBER(3,0) DEFAULT (0) NOT NULL
);


ALTER TABLE CascadeFilterDetails
ADD CONSTRAINT PK_CascadeFilterDetails PRIMARY KEY
(
  FilterDetailID
)
ENABLE
;

CREATE TABLE Categories (
  CategoryID NUMBER(10,0) NOT NULL,
  DatabaseID NUMBER(10,0) DEFAULT (0) NOT NULL,
  CategoryName VARCHAR2(100 CHAR) NOT NULL,
  Description VARCHAR2(255 CHAR)
);


ALTER TABLE Categories
ADD CONSTRAINT PK_Categories PRIMARY KEY
(
  CategoryID
)
ENABLE
;

CREATE TABLE CategoryObjects (
  CategoryID NUMBER(10,0) NOT NULL,
  ObjectID NUMBER(10,0) NOT NULL
);


ALTER TABLE CategoryObjects
ADD CONSTRAINT PK_CategoryObjects PRIMARY KEY
(
  CategoryID,
  ObjectID
)
ENABLE
;
CREATE INDEX CategoryObjects_CategoryIDIdx ON CategoryObjects
(
  CategoryID
) 
;
CREATE INDEX CategoryObjects_ObjectIDIdx ON CategoryObjects
(
  ObjectID
) 
;

CREATE TABLE FolderRole (
  FolderRoleID NUMBER(10,0) NOT NULL,
  FolderID NUMBER(10,0) NOT NULL,
  RoleID NUMBER(10,0) NOT NULL
);


ALTER TABLE FolderRole
ADD CONSTRAINT PK_FolderRole PRIMARY KEY
(
  FolderRoleID
)
ENABLE
;

CREATE TABLE JoinRelationDetails (
  JoinRelationDetailsID NUMBER(10,0) NOT NULL,
  JoinRelationID NUMBER(10,0) NOT NULL,
  ColumnID1 NUMBER(10,0) NOT NULL,
  ColumnID2 NUMBER(10,0) NOT NULL
);


ALTER TABLE JoinRelationDetails
ADD CONSTRAINT PK_JoinRelationDetails PRIMARY KEY
(
  JoinRelationDetailsID
)
ENABLE
;
CREATE INDEX JoinRelationDetails_JoinRelati ON JoinRelationDetails
(
  JoinRelationID
) 
;

CREATE TABLE JoinRelation (
  JoinRelationID NUMBER(10,0) NOT NULL,
  ObjectID1 NUMBER(10,0) NOT NULL,
  Relation VARCHAR2(50 CHAR) NOT NULL,
  ObjectID2 NUMBER(10,0) NOT NULL,
  RelationName VARCHAR2(100 CHAR),
  Description VARCHAR2(255 CHAR), 
  DatabaseID NUMBER(10,0),
  ObjectLabel1 VARCHAR2(100 CHAR),
  ObjectLabel2 VARCHAR2(100 CHAR),
  HideJoin NUMBER(3,0) DEFAULT (0),
  AutoReverse NUMBER(3,0) DEFAULT (1),
  Automatic NUMBER(3,0) DEFAULT (0)
);


ALTER TABLE JoinRelation
ADD CONSTRAINT PK_JoinRelation PRIMARY KEY
(
  JoinRelationID
)
ENABLE
;

CREATE TABLE LinkParameters (
  LinkParameterID NUMBER(10,0) NOT NULL,
  ColumnID NUMBER(10,0) DEFAULT (0) NOT NULL,
  ParameterName VARCHAR2(50 CHAR) NOT NULL,
  ParamDisplayName VARCHAR2(50 CHAR) NOT NULL,
  ParameterSourceType NUMBER(3,0) DEFAULT (0) NOT NULL,
  ParameterSource VARCHAR2(100 CHAR) NOT NULL
);


ALTER TABLE LinkParameters
ADD CONSTRAINT PK_LinkParameters PRIMARY KEY
(
  LinkParameterID
)
ENABLE
;
CREATE INDEX LP_ColumnIDIdx ON LinkParameters
(
  ColumnID
) 
;
CREATE INDEX LinkParameters_ParameterNameId ON LinkParameters
(
  ParameterName
) 
;

CREATE TABLE ObjectAccess (
  AccessID NUMBER(10,0) NOT NULL,
  AccessName VARCHAR2(10 CHAR) NOT NULL
);


ALTER TABLE ObjectAccess
ADD CONSTRAINT PK_ObjectAccess PRIMARY KEY
(
  AccessID
)
ENABLE
;

CREATE TABLE ObjectParameters (
  ObjectParameterID NUMBER(10,0) NOT NULL,
  ObjectID NUMBER(10,0) DEFAULT (0) NOT NULL,
  ColumnID NUMBER(10,0) DEFAULT (0) NOT NULL,
  Operator VARCHAR2(25 CHAR),
  ParamValue VARCHAR2(50 CHAR),
  BitCmd VARCHAR2(10 CHAR),
  ParamOrder NUMBER(10,0),
  ParamLevel NUMBER(10,0),
  ParamType NUMBER(3,0) DEFAULT (0),
  ParamName VARCHAR2(128 CHAR),
  ParamCaption VARCHAR2(128 CHAR),
  ParamDataType VARCHAR2(20 CHAR),
  ParamDirection NUMBER(3,0) DEFAULT (0)
);


ALTER TABLE ObjectParameters
ADD CONSTRAINT PK_ObjectParameters PRIMARY KEY
(
  ObjectParameterID
)
ENABLE
;
CREATE INDEX ObjectParameters_ColumnIDIdx ON ObjectParameters
(
  ColumnID
) 
;
CREATE INDEX ObjectParameters_ObjectIDIdx ON ObjectParameters
(
  ObjectID
) 
;

CREATE TABLE CascadeFilters (
  FilterID NUMBER(10,0) NOT NULL,
  FilterName VARCHAR2(100 CHAR) NOT NULL,
  Description VARCHAR2(255 CHAR),
  ObjectID NUMBER(10,0) NOT NULL,
  ColumnID NUMBER(10,0) NOT NULL,
  DatabaseID NUMBER(10,0) NOT NULL
);


ALTER TABLE CascadeFilters
ADD CONSTRAINT PK_CascadeFilters PRIMARY KEY
(
  FilterID
)
ENABLE
;

CREATE TABLE Classes (
  ClassID NUMBER(10,0) NOT NULL,
  Class VARCHAR2(100 CHAR) NOT NULL,
  FriendlyName VARCHAR2(100 CHAR) NOT NULL
);

ALTER TABLE Classes
ADD CONSTRAINT PK_Classes PRIMARY KEY
(
  ClassID
)
ENABLE
;
ALTER TABLE Classes
ADD CONSTRAINT Classes_FriendlyNameIdx UNIQUE (
  FriendlyName
)
ENABLE
;

CREATE TABLE ColumnAccess (
  AccessID NUMBER(10,0) NOT NULL,
  AccessName VARCHAR2(10 CHAR) NOT NULL
);

ALTER TABLE ColumnAccess
ADD CONSTRAINT PK_ColumnAccess PRIMARY KEY
(
  AccessID
)
ENABLE
;

CREATE TABLE Columns (
  ColumnID NUMBER(10,0) NOT NULL,
  ColumnName VARCHAR2(100 CHAR) NOT NULL,
  ColumnAlias VARCHAR2(100 CHAR),
  Description VARCHAR2(100 CHAR),
  ColumnType VARCHAR2(2 CHAR) DEFAULT 'D',
  ObjectID NUMBER(10,0) NOT NULL,
  OrdinalPosition NUMBER(10,0),
  ColumnOrder NUMBER(10,0) DEFAULT (0),
  DataType VARCHAR2(20 CHAR),
  CharacterMaxLen NUMBER(10,0),
  NumericPrecision NUMBER(10,0),
  NumericScale NUMBER(10,0),
  DisplayFormat VARCHAR2(50 CHAR),
  Alignment VARCHAR2(15 CHAR),
  NativeDataType VARCHAR2(20 CHAR),
  Definition VARCHAR2(4000 CHAR),
  ExplanationID NUMBER(10,0) DEFAULT (0) NOT NULL,
  LinkRptID NUMBER(10,0) DEFAULT (0),
  LinkURL VARCHAR2(255 CHAR),
  FrameID VARCHAR2(25 CHAR), 
  HideColumn NUMBER(3,0) DEFAULT (0)
);


ALTER TABLE Columns
ADD CONSTRAINT PK_Columns PRIMARY KEY
(
  ColumnID
)
ENABLE
;
CREATE UNIQUE INDEX IX_Columns ON Columns
(
  ObjectID,
  ColumnName
) 
;

CREATE TABLE ColumnExplanation (
  ExplanationID NUMBER(10,0) NOT NULL,
  Explanation CLOB NOT NULL
);


ALTER TABLE ColumnExplanation
ADD CONSTRAINT PK_ColumnExplanation PRIMARY KEY
(
  ExplanationID
)
ENABLE
;

CREATE TABLE DatabaseRole (
  DatabaseRoleID NUMBER(10,0) NOT NULL,
  DatabaseID NUMBER(10,0) NOT NULL,
  RoleID NUMBER(10,0) NOT NULL
);


ALTER TABLE DatabaseRole
ADD CONSTRAINT PK_DatabaseRole PRIMARY KEY
(
  DatabaseRoleID
)
ENABLE
;

CREATE TABLE UserGroups (
  GroupID NUMBER(10,0) NOT NULL,
  GroupName VARCHAR2(100 CHAR) NOT NULL,
  Description VARCHAR2(255 CHAR),
  DefaultTheme VARCHAR2(255 CHAR)
);

ALTER TABLE UserGroups
ADD CONSTRAINT PK_UserGroups PRIMARY KEY
(
  GroupID
)
ENABLE
;
ALTER TABLE UserGroups
ADD CONSTRAINT SP_GroupNameIdx UNIQUE (
  GroupName
)
ENABLE
;

CREATE TABLE Folder (
  FolderID NUMBER(10,0) NOT NULL,
  FolderName VARCHAR2(100 CHAR) NOT NULL,
  OwnerUserID NUMBER(10,0) NOT NULL,
  ModifiedUserID NUMBER(10,0),
  FolderType NUMBER(10,0) DEFAULT (1) NOT NULL,
  ParentFolderID NUMBER(10,0) DEFAULT (0) NOT NULL,
  Description VARCHAR2(255 CHAR),
  AllRolesAccess NUMBER(10,0) DEFAULT (1) NOT NULL,
  TimeCreated DATE DEFAULT SYSDATE NOT NULL,
  TimeSaved DATE,
  DatabaseID NUMBER(10,0),
  GroupID NUMBER(10,0) DEFAULT (0)
);


ALTER TABLE Folder
ADD CONSTRAINT PK_Folder PRIMARY KEY
(
  FolderID
)
ENABLE
;
CREATE INDEX Folder_GroupIDIdx ON Folder
(
  GroupID
) 
;

CREATE TABLE Objects (
  ObjectID NUMBER(10,0) NOT NULL,
  ObjectSchema VARCHAR2(255 CHAR),
  ObjectName VARCHAR2(128 CHAR) NOT NULL,
  ObjectAlias VARCHAR2(128 CHAR),
  Description VARCHAR2(128 CHAR),
  Type VARCHAR2(2 CHAR),
  DatabaseID NUMBER(10,0),
  Definition VARCHAR2(4000 CHAR),
  HideObject NUMBER(3,0) DEFAULT (0), 
  ExplanationID NUMBER(10,0) DEFAULT (0), 
  IsCatalogue NUMBER(3,0) DEFAULT (1)  
);


ALTER TABLE Objects
ADD CONSTRAINT PK_Objects PRIMARY KEY
(
  ObjectID
)
ENABLE
;

CREATE TABLE ParameterValues (
  ParameterValuesID NUMBER(10,0) NOT NULL,
  ObjectParameterID NUMBER(10,0) NOT NULL,
  ParamValue VARCHAR2(255 CHAR) NOT NULL,
  ParamValueType NUMBER(3,0) DEFAULT (0)
);


ALTER TABLE ParameterValues
ADD CONSTRAINT PK_ParameterValues PRIMARY KEY
(
  ParameterValuesID
)
ENABLE
;

CREATE TABLE PermissionRights (
  PermissionRightID NUMBER(10,0) NOT NULL,
  PermissionID NUMBER(10,0) NOT NULL,
  RightID NUMBER(10,0) NOT NULL
);


ALTER TABLE PermissionRights
ADD CONSTRAINT PK_PermissionRights PRIMARY KEY
(
  PermissionRightID
)
ENABLE
;

CREATE TABLE Permissions (
  PermissionID NUMBER(10,0) NOT NULL,
  Permission VARCHAR2(100 CHAR) NOT NULL,
  Description VARCHAR2(255 CHAR)
);


ALTER TABLE Permissions
ADD CONSTRAINT PK_Permissions PRIMARY KEY
(
  PermissionID
)
ENABLE
;

CREATE TABLE ReportParameters (
  ReportParameterID NUMBER(10,0) NOT NULL,
  ReportID NUMBER(10,0) NOT NULL,
  ColumnID NUMBER(10,0) NOT NULL,
  Operator VARCHAR2(25 CHAR),
  BitCmd VARCHAR2(10 CHAR),
  Ask NUMBER(10,0),
  Scheduler NUMBER(10,0),
  Caption VARCHAR2(100 CHAR),
  ParamType NUMBER(3,0) DEFAULT (0),
  ParamName VARCHAR2(128 CHAR)
);


ALTER TABLE ReportParameters
ADD CONSTRAINT PK_ReportParameters PRIMARY KEY
(
  ReportParameterID
)
ENABLE
;

CREATE TABLE ReportParameterValues (
  ReportParameterValuesID NUMBER(10,0) NOT NULL,
  ReportParameterID NUMBER(10,0) NOT NULL,
  ReportScheduleID NUMBER(10,0) NOT NULL,
  ParamValue VARCHAR2(255 CHAR) NOT NULL
);


ALTER TABLE ReportParameterValues
ADD CONSTRAINT PK_ReportParameterValues PRIMARY KEY
(
  ReportParameterValuesID
)
ENABLE
;
CREATE INDEX ReportParameterValues_ReportSc ON ReportParameterValues
(
  ReportScheduleID
) 
;

CREATE TABLE ReportSchedules (
  ReportScheduleID NUMBER(10,0) NOT NULL,
  ReportID NUMBER(10,0) NOT NULL,
  ModifiedUserID NUMBER(10,0),
  TimeSaved DATE,
  TaskName VARCHAR2(50 CHAR) NOT NULL,
  TaskID NUMBER(10,0), 
  OutputFormat NUMBER(10,0),
  Archive NUMBER(10,0),
  Server VARCHAR2(255 CHAR),
  Method NUMBER(3,0),
  Broken NUMBER(3,0)
);


ALTER TABLE ReportSchedules
ADD CONSTRAINT PK_ReportSchedule PRIMARY KEY
(
  ReportScheduleID
)
ENABLE
;
CREATE INDEX ReportSchedules_ReportIDIdx ON ReportSchedules
(
  ReportID
) 
;
CREATE INDEX ReportSchedules_ModifiedUserID ON ReportSchedules
(
  ModifiedUserID
) 
;

CREATE TABLE ReportSessionParameters (
  ReportSessionParamID NUMBER(10,0) NOT NULL,
  ReportID NUMBER(10,0) DEFAULT (0) NOT NULL,
  UserID NUMBER(10,0) DEFAULT (0) NOT NULL,
  ParamName VARCHAR2(100 CHAR) NOT NULL,
  ParamValue VARCHAR2(4000 CHAR)
);


ALTER TABLE ReportSessionParameters
ADD CONSTRAINT PK_ReportSessionParameters PRIMARY KEY
(
  ReportID,
  UserID,
  ParamName
)
ENABLE
;
ALTER TABLE ReportSessionParameters
ADD CONSTRAINT IX_ReportSessionParameters UNIQUE (
  ReportSessionParamID
)
ENABLE
;
CREATE INDEX ReportSessionParameters_Report ON ReportSessionParameters
(
  ReportID
) 
;
CREATE INDEX ReportSessionParameters_UserID ON ReportSessionParameters
(
  UserID
) 
;

CREATE TABLE Rights (
  RightID NUMBER(10,0) NOT NULL,
  RightName VARCHAR2(100 CHAR) NOT NULL,
  Description VARCHAR2(255 CHAR),
  RightGroup VARCHAR2(50 CHAR) NOT NULL
);


ALTER TABLE Rights
ADD CONSTRAINT PK_Rights PRIMARY KEY
(
  RightID
)
ENABLE
;

CREATE TABLE Role (
  RoleID NUMBER(10,0) NOT NULL,
  RoleName VARCHAR2(64 CHAR) NOT NULL,
  Description VARCHAR2(255 CHAR)
);

ALTER TABLE Role
ADD CONSTRAINT PK_Role PRIMARY KEY
(
  RoleID
)
ENABLE
;
ALTER TABLE Role
ADD CONSTRAINT Role_RoleNameIdx UNIQUE (
  RoleName
)
ENABLE
;

CREATE TABLE RoleColumns (
  RoleID NUMBER(10,0) NOT NULL,
  ColumnID NUMBER(10,0) NOT NULL,
  AccessType NUMBER(10,0) NOT NULL
);


ALTER TABLE RoleColumns
ADD CONSTRAINT PK_RoleColumns PRIMARY KEY
(
  RoleID,
  ColumnID
)
ENABLE
;
CREATE INDEX RoleColumns_RoleIDIdx ON RoleColumns
(
  RoleID
) 
;
CREATE INDEX RoleColumns_ColumnIDIdx ON RoleColumns
(
  ColumnID
) 
;

CREATE TABLE RoleObjects (
  RoleID NUMBER(10,0) NOT NULL,
  ObjectID NUMBER(10,0) NOT NULL,
  AccessType NUMBER(10,0) NOT NULL
);


ALTER TABLE RoleObjects
ADD CONSTRAINT PK_RoleObjects PRIMARY KEY
(
  RoleID,
  ObjectID
)
ENABLE
;
CREATE INDEX RoleObjects_RoleIDIdx ON RoleObjects
(
  RoleID
) 
;
CREATE INDEX RoleObjects_ObjectIDIdx ON RoleObjects
(
  ObjectID
) 
;

CREATE TABLE RolePermissions (
  RolePermissionID NUMBER(10,0) NOT NULL,
  RoleID NUMBER(10,0) NOT NULL,
  PermissionID NUMBER(10,0) NOT NULL
);


ALTER TABLE RolePermissions
ADD CONSTRAINT PK_RolePermissions PRIMARY KEY
(
  RolePermissionID
)
ENABLE
;

CREATE TABLE SessionParameters (
  SessionParameterID NUMBER(10,0) NOT NULL,
  ParameterName VARCHAR2(50 CHAR) NOT NULL,
  DefaultValue VARCHAR2(4000 CHAR), 
  DataTypeCategory NUMBER(10,0) DEFAULT (2) NULL
);


ALTER TABLE SessionParameters
ADD CONSTRAINT PK_SessionParameters PRIMARY KEY
(
  SessionParameterID
)
ENABLE
;
ALTER TABLE SessionParameters
ADD CONSTRAINT SP_ParameterNameIdx UNIQUE (
  ParameterName
)
ENABLE
;

CREATE TABLE SourceDatabase (
  DatabaseID NUMBER(10,0) NOT NULL,
  Description VARCHAR2(255 CHAR)
);


ALTER TABLE SourceDatabase
ADD CONSTRAINT PK_SourceDatabase PRIMARY KEY
(
  DatabaseID
)
ENABLE
;

CREATE TABLE SystemSetting (
  SettingName VARCHAR2(20 CHAR) NOT NULL,
  SettingValue VARCHAR2(20 CHAR),
  Description VARCHAR2(100 CHAR),
  DatabaseID NUMBER(10,0) NOT NULL
);


ALTER TABLE SystemSetting
ADD CONSTRAINT PK_SystemSetting PRIMARY KEY
(
  SettingName,
  DatabaseID
)
ENABLE
;
CREATE INDEX SystemSetting_DatabaseIDIdx ON SystemSetting
(
  DatabaseID
) 
;
CREATE INDEX SystemSetting_SettingNameIdx ON SystemSetting
(
  SettingName
) 
;

CREATE TABLE Templates (
  TemplateID NUMBER(10,0) NOT NULL,
  TemplateName VARCHAR2(255 CHAR),
  TemplateKey VARCHAR2(50 CHAR),
  TemplateType NUMBER(3,0) DEFAULT (0) NOT NULL,
  Description VARCHAR2(255 CHAR),
  OwnerID NUMBER(10,0),
  UsedObjects VARCHAR2(255 CHAR),
  UsedColumns CLOB,
  IsDefault NUMBER(3,0) DEFAULT (0) NOT NULL,
  IsAvailable NUMBER(3,0) DEFAULT (1) NOT NULL,
  SortOrder NUMBER(10,0),
  RightName VARCHAR2(100 CHAR)
);

ALTER TABLE Templates
ADD CONSTRAINT PK_Templates PRIMARY KEY
(
  TemplateID
)
ENABLE
;
CREATE INDEX Templates_TemplateTypeIdx ON Templates
(
  TemplateType
) 
;

CREATE TABLE UserGroupRoles (
  UserGroupRoleID NUMBER(10,0) NOT NULL,
  GroupID NUMBER(10,0) NOT NULL,
  RoleID NUMBER(10,0) NOT NULL
);

ALTER TABLE UserGroupRoles
ADD CONSTRAINT PK_UserGroupRoles PRIMARY KEY
(
  UserGroupRoleID
)
ENABLE
;

CREATE TABLE UserGroupSessions (
  UserGroupSessionID NUMBER(10,0) NOT NULL,
  GroupID NUMBER(10,0) NOT NULL,
  SessionParameterID NUMBER(10,0) NOT NULL,
  SessionParameterValue VARCHAR2(4000 CHAR) NOT NULL
);


ALTER TABLE UserGroupSessions
ADD CONSTRAINT PK_UserGroupSessions PRIMARY KEY
(
  UserGroupSessionID
)
ENABLE
;
CREATE INDEX UGS_GroupIDIdx ON UserGroupSessions
(
  GroupID
) 
;
CREATE INDEX UGS_SessionParameterIDIdx ON UserGroupSessions
(
  SessionParameterID
) 
;

CREATE TABLE UserSessions (
  UserSessionID NUMBER(10,0) NOT NULL,
  UserID NUMBER(10,0) NOT NULL,
  SessionParameterID NUMBER(10,0) NOT NULL,
  SessionParameterValue VARCHAR2(4000 CHAR) NOT NULL
);


ALTER TABLE UserSessions
ADD CONSTRAINT PK_UserSessions PRIMARY KEY
(
  UserSessionID
)
ENABLE
;
CREATE INDEX US_UserIDIdx ON UserSessions
(
  UserID
) 
;
CREATE INDEX US_SessionParameterIDIdx ON UserSessions
(
  SessionParameterID
) 
;

CREATE TABLE UserProfile (
  UserProfileID NUMBER(10,0) NOT NULL,
  UserID NUMBER(10,0) NOT NULL,
  PropertyName VARCHAR2(255 CHAR) NOT NULL,
  PropertyValue VARCHAR2(255 CHAR)
);


ALTER TABLE UserProfile
ADD CONSTRAINT PK_UserProfile PRIMARY KEY
(
  UserProfileID
)
ENABLE
;
CREATE INDEX UserProfile_UserIDIdx ON UserProfile
(
  UserID
) 
;

CREATE TABLE UserReport (
  ID NUMBER(10,0) NOT NULL,
  UserID NUMBER(10,0) NOT NULL,
  FolderType NUMBER(10,0) DEFAULT (1) NOT NULL,
  ReportName VARCHAR2(200 CHAR) DEFAULT 'New',
  Description VARCHAR2(255 CHAR),
  TimeCreated DATE DEFAULT SYSDATE NOT NULL,
  TimeSaved DATE,
  ParentFolderID NUMBER(10,0) DEFAULT (0) NOT NULL,
  ModifiedUserID NUMBER(10,0),
  DatabaseID NUMBER(10,0),
  UsedDatabases VARCHAR2(255 CHAR),
  UsedObjects VARCHAR2(255 CHAR),
  UsedColumns CLOB,
  UsedRelationships VARCHAR2(255 CHAR),
  UsedCascadingFilters VARCHAR2(255 CHAR),
  UsedClasses VARCHAR2(255 CHAR),
  UsedFormats VARCHAR2(255 CHAR),
  GroupID NUMBER(10,0) DEFAULT (0),
  PhysicalName VARCHAR2(200 CHAR),
  Broken NUMBER(3,0),
  BreakReason NUMBER(10,0),
  LastViewDate DATE,
  LastViewUserID NUMBER(10,0),
  ViewCount NUMBER(10,0) DEFAULT (0),
  Dashboard NUMBER(3,0) DEFAULT (0) NOT NULL,
  Mobile NUMBER(3,0) DEFAULT (0) NOT NULL,
  ExpirationDate DATE,
  ReportGUID VARCHAR2(100 CHAR)
);


ALTER TABLE UserReport
ADD CONSTRAINT PK_UserReport PRIMARY KEY
(
  ID
)
ENABLE
;
CREATE INDEX UserReport_GroupIDIdx ON UserReport
(
  GroupID
) 
;
CREATE INDEX UserReport_LastViewUserIDIdx ON UserReport
(
  LastViewUserID
) 
;

CREATE TABLE Users (
  UserID NUMBER(10,0) NOT NULL,
  UserName VARCHAR2(50 CHAR) NOT NULL,
  Password VARCHAR2(100 CHAR) NOT NULL,
  FirstName VARCHAR2(20 CHAR),
  LastName VARCHAR2(20 CHAR),
  Email VARCHAR2(50 CHAR),
  LastDatabaseID NUMBER(10,0),
  GroupID NUMBER(10,0) DEFAULT (0),
  Locked NUMBER(3,0) DEFAULT (0),
  LastPasswordChange DATE
);

ALTER TABLE Users
ADD CONSTRAINT PK_Users PRIMARY KEY
(
  UserID
)
ENABLE
;
ALTER TABLE Users
ADD CONSTRAINT Users_UserNameIdx UNIQUE (
  UserName
)
ENABLE
;
CREATE INDEX Users_GroupIDIdx ON Users
(
  GroupID
) 
;

CREATE TABLE UserRole (
  UserID NUMBER(10,0) NOT NULL,
  RoleID NUMBER(10,0) NOT NULL
);

ALTER TABLE UserRole
ADD CONSTRAINT PK_UserRole PRIMARY KEY
(
  UserID,
  RoleID
)
ENABLE
;
CREATE INDEX UserRole_UserIDIdx ON UserRole
(
  UserID
) 
;
CREATE INDEX UserRole_RoleIDIdx ON UserRole
(
  RoleID
) 
;

CREATE TABLE UserScheduleSubscription (
  SubscriptionID NUMBER(10,0) NOT NULL,
  UserID NUMBER(10,0) NOT NULL,
  ReportScheduleID NUMBER(10,0) NOT NULL,
  Broken NUMBER(1,0) DEFAULT (0)
);


ALTER TABLE UserScheduleSubscription
ADD CONSTRAINT PK_UserScheduleSubscription PRIMARY KEY
(
  SubscriptionID
)
ENABLE
;
CREATE INDEX UserScheduleSubscription_Repor ON UserScheduleSubscription
(
  ReportScheduleID
) 
;

CREATE OR REPLACE PROCEDURE authenticateUser
(
  v_prmUser IN VARCHAR2 DEFAULT NULL ,
  v_prmPass IN VARCHAR2 DEFAULT NULL ,
  cv_1 IN OUT SYS_REFCURSOR
)
AS
BEGIN

   OPEN cv_1 FOR
      SELECT UserName,
             UserID
        FROM Users
         WHERE UPPER(UserName) = UPPER(v_prmUser)
                 AND Password = v_prmPass;

END;
/

CREATE OR REPLACE PROCEDURE GetIdentity
( id out int) AS
BEGIN

	SELECT sqlserver_utilities.identity INTO id FROM dual;

END GetIdentity;

/

CREATE OR REPLACE TRIGGER LINKPARAMETERS_LINKPARAMETER_1 BEFORE INSERT OR UPDATE ON LinkParameters
FOR EACH ROW
DECLARE 
v_newVal NUMBER(12) := 0;
v_incval NUMBER(12) := 0;
BEGIN
  IF INSERTING AND :new.LinkParameterID IS NULL THEN
    SELECT  LinkParameters_LinkParameterID.NEXTVAL INTO v_newVal FROM DUAL;
    -- save this to emulate @@identity
   sqlserver_utilities.identity := v_newVal; 
   -- assign the value from the sequence to emulate the identity column
   :new.LinkParameterID := v_newVal;
  END IF;
END;

/

CREATE OR REPLACE TRIGGER SESSIONPARAMETERS_SESSIONPAR_1 BEFORE INSERT OR UPDATE ON SessionParameters
FOR EACH ROW
DECLARE 
v_newVal NUMBER(12) := 0;
v_incval NUMBER(12) := 0;
BEGIN
  IF INSERTING AND :new.SessionParameterID IS NULL THEN
    SELECT  SessionParameters_SessionParam.NEXTVAL INTO v_newVal FROM DUAL;
    -- save this to emulate @@identity
   sqlserver_utilities.identity := v_newVal; 
   -- assign the value from the sequence to emulate the identity column
   :new.SessionParameterID := v_newVal;
  END IF;
END;

/

CREATE OR REPLACE TRIGGER SourceDatabase_DatabaseID_TRG BEFORE INSERT OR UPDATE ON SourceDatabase
FOR EACH ROW
DECLARE 
v_newVal NUMBER(12) := 0;
v_incval NUMBER(12) := 0;
BEGIN
  IF INSERTING AND :new.DatabaseID IS NULL THEN
    SELECT  SourceDatabase_DatabaseID_SEQ.NEXTVAL INTO v_newVal FROM DUAL;
    -- save this to emulate @@identity
   sqlserver_utilities.identity := v_newVal; 
   -- assign the value from the sequence to emulate the identity column
   :new.DatabaseID := v_newVal;
  END IF;
END;

/

CREATE OR REPLACE TRIGGER PARAMETERVALUES_PARAMETERVAL_1 BEFORE INSERT OR UPDATE ON ParameterValues
FOR EACH ROW
DECLARE 
v_newVal NUMBER(12) := 0;
v_incval NUMBER(12) := 0;
BEGIN
  IF INSERTING AND :new.ParameterValuesID IS NULL THEN
    SELECT  ParameterValues_ParameterValue.NEXTVAL INTO v_newVal FROM DUAL;
    -- save this to emulate @@identity
   sqlserver_utilities.identity := v_newVal; 
   -- assign the value from the sequence to emulate the identity column
   :new.ParameterValuesID := v_newVal;
  END IF;
END;

/

CREATE OR REPLACE TRIGGER Classes_ClassID_TRG BEFORE INSERT OR UPDATE ON Classes
FOR EACH ROW
DECLARE 
v_newVal NUMBER(12) := 0;
v_incval NUMBER(12) := 0;
BEGIN
  IF INSERTING AND :new.ClassID IS NULL THEN
    SELECT  Classes_ClassID_SEQ.NEXTVAL INTO v_newVal FROM DUAL;
    -- save this to emulate @@identity
   sqlserver_utilities.identity := v_newVal; 
   -- assign the value from the sequence to emulate the identity column
   :new.ClassID := v_newVal;
  END IF;
END;

/

CREATE OR REPLACE TRIGGER UserProfile_UserProfileID_TRG BEFORE INSERT OR UPDATE ON UserProfile
FOR EACH ROW
DECLARE 
v_newVal NUMBER(12) := 0;
v_incval NUMBER(12) := 0;
BEGIN
  IF INSERTING AND :new.UserProfileID IS NULL THEN
    SELECT  UserProfile_UserProfileID_SEQ.NEXTVAL INTO v_newVal FROM DUAL;
    -- save this to emulate @@identity
   sqlserver_utilities.identity := v_newVal; 
   -- assign the value from the sequence to emulate the identity column
   :new.UserProfileID := v_newVal;
  END IF;
END;

/

CREATE OR REPLACE TRIGGER FolderRole_FolderRoleID_TRG BEFORE INSERT OR UPDATE ON FolderRole
FOR EACH ROW
DECLARE 
v_newVal NUMBER(12) := 0;
v_incval NUMBER(12) := 0;
BEGIN
  IF INSERTING AND :new.FolderRoleID IS NULL THEN
    SELECT  FolderRole_FolderRoleID_SEQ.NEXTVAL INTO v_newVal FROM DUAL;
    -- save this to emulate @@identity
   sqlserver_utilities.identity := v_newVal; 
   -- assign the value from the sequence to emulate the identity column
   :new.FolderRoleID := v_newVal;
  END IF;
END;

/

CREATE OR REPLACE TRIGGER USERSCHEDULESUBSCRIPTION_SUB_1 BEFORE INSERT OR UPDATE ON UserScheduleSubscription
FOR EACH ROW
DECLARE 
v_newVal NUMBER(12) := 0;
v_incval NUMBER(12) := 0;
BEGIN
  IF INSERTING AND :new.SubscriptionID IS NULL THEN
    SELECT  UserScheduleSubscription_Subsc.NEXTVAL INTO v_newVal FROM DUAL;
    -- save this to emulate @@identity
   sqlserver_utilities.identity := v_newVal; 
   -- assign the value from the sequence to emulate the identity column
   :new.SubscriptionID := v_newVal;
  END IF;
END;

/

CREATE OR REPLACE TRIGGER UserGroups_GroupID_TRG BEFORE INSERT OR UPDATE ON UserGroups
FOR EACH ROW
DECLARE 
v_newVal NUMBER(12) := 0;
v_incval NUMBER(12) := 0;
BEGIN
  IF INSERTING AND :new.GroupID IS NULL THEN
    SELECT  UserGroups_GroupID_SEQ.NEXTVAL INTO v_newVal FROM DUAL;
    -- save this to emulate @@identity
   sqlserver_utilities.identity := v_newVal; 
   -- assign the value from the sequence to emulate the identity column
   :new.GroupID := v_newVal;
  END IF;
END;

/

CREATE OR REPLACE TRIGGER REPORTSESSIONPARAMETERS_REPO_1 BEFORE INSERT OR UPDATE ON ReportSessionParameters
FOR EACH ROW
DECLARE 
v_newVal NUMBER(12) := 0;
v_incval NUMBER(12) := 0;
BEGIN
  IF INSERTING AND :new.ReportSessionParamID IS NULL THEN
    SELECT  ReportSessionParameters_Report.NEXTVAL INTO v_newVal FROM DUAL;
    -- save this to emulate @@identity
   sqlserver_utilities.identity := v_newVal; 
   -- assign the value from the sequence to emulate the identity column
   :new.ReportSessionParamID := v_newVal;
  END IF;
END;

/

CREATE OR REPLACE TRIGGER Role_RoleID_TRG BEFORE INSERT OR UPDATE ON Role
FOR EACH ROW
DECLARE 
v_newVal NUMBER(12) := 0;
v_incval NUMBER(12) := 0;
BEGIN
  IF INSERTING AND :new.RoleID IS NULL THEN
    SELECT  Role_RoleID_SEQ.NEXTVAL INTO v_newVal FROM DUAL;
    -- save this to emulate @@identity
   sqlserver_utilities.identity := v_newVal; 
   -- assign the value from the sequence to emulate the identity column
   :new.RoleID := v_newVal;
  END IF;
END;

/

CREATE OR REPLACE TRIGGER REPORTPARAMETERS_REPORTPARAM_1 BEFORE INSERT OR UPDATE ON ReportParameters
FOR EACH ROW
DECLARE 
v_newVal NUMBER(12) := 0;
v_incval NUMBER(12) := 0;
BEGIN
  IF INSERTING AND :new.ReportParameterID IS NULL THEN
    SELECT  ReportParameters_ReportParamet.NEXTVAL INTO v_newVal FROM DUAL;
    -- save this to emulate @@identity
   sqlserver_utilities.identity := v_newVal; 
   -- assign the value from the sequence to emulate the identity column
   :new.ReportParameterID := v_newVal;
  END IF;
END;

/

CREATE OR REPLACE TRIGGER USERGROUPROLES_USERGROUPROLE_1 BEFORE INSERT OR UPDATE ON UserGroupRoles
FOR EACH ROW
DECLARE 
v_newVal NUMBER(12) := 0;
v_incval NUMBER(12) := 0;
BEGIN
  IF INSERTING AND :new.UserGroupRoleID IS NULL THEN
    SELECT  UserGroupRoles_UserGroupRoleID.NEXTVAL INTO v_newVal FROM DUAL;
    -- save this to emulate @@identity
   sqlserver_utilities.identity := v_newVal; 
   -- assign the value from the sequence to emulate the identity column
   :new.UserGroupRoleID := v_newVal;
  END IF;
END;

/

CREATE OR REPLACE TRIGGER ROLEPERMISSIONS_ROLEPERMISSI_1 BEFORE INSERT OR UPDATE ON RolePermissions
FOR EACH ROW
DECLARE 
v_newVal NUMBER(12) := 0;
v_incval NUMBER(12) := 0;
BEGIN
  IF INSERTING AND :new.RolePermissionID IS NULL THEN
    SELECT  RolePermissions_RolePermission.NEXTVAL INTO v_newVal FROM DUAL;
    -- save this to emulate @@identity
   sqlserver_utilities.identity := v_newVal; 
   -- assign the value from the sequence to emulate the identity column
   :new.RolePermissionID := v_newVal;
  END IF;
END;

/

CREATE OR REPLACE TRIGGER Columns_ColumnID_TRG BEFORE INSERT OR UPDATE ON Columns
FOR EACH ROW
DECLARE 
v_newVal NUMBER(12) := 0;
v_incval NUMBER(12) := 0;
BEGIN
  IF INSERTING AND :new.ColumnID IS NULL THEN
    SELECT  Columns_ColumnID_SEQ.NEXTVAL INTO v_newVal FROM DUAL;
    -- save this to emulate @@identity
   sqlserver_utilities.identity := v_newVal; 
   -- assign the value from the sequence to emulate the identity column
   :new.ColumnID := v_newVal;
  END IF;
END;

/

CREATE OR REPLACE TRIGGER Objects_ObjectID_TRG BEFORE INSERT OR UPDATE ON Objects
FOR EACH ROW
DECLARE 
v_newVal NUMBER(12) := 0;
v_incval NUMBER(12) := 0;
BEGIN
  IF INSERTING AND :new.ObjectID IS NULL THEN
    SELECT  Objects_ObjectID_SEQ.NEXTVAL INTO v_newVal FROM DUAL;
    -- save this to emulate @@identity
   sqlserver_utilities.identity := v_newVal; 
   -- assign the value from the sequence to emulate the identity column
   :new.ObjectID := v_newVal;
  END IF;
END;

/

CREATE OR REPLACE TRIGGER Users_UserID_TRG BEFORE INSERT OR UPDATE ON Users
FOR EACH ROW
DECLARE 
v_newVal NUMBER(12) := 0;
v_incval NUMBER(12) := 0;
BEGIN
  IF INSERTING AND :new.UserID IS NULL THEN
    SELECT  Users_UserID_SEQ.NEXTVAL INTO v_newVal FROM DUAL;
    -- save this to emulate @@identity
   sqlserver_utilities.identity := v_newVal; 
   -- assign the value from the sequence to emulate the identity column
   :new.UserID := v_newVal;
  END IF;
END;

/

CREATE OR REPLACE TRIGGER Folder_FolderID_TRG BEFORE INSERT OR UPDATE ON Folder
FOR EACH ROW
DECLARE 
v_newVal NUMBER(12) := 0;
v_incval NUMBER(12) := 0;
BEGIN
  IF INSERTING AND :new.FolderID IS NULL THEN
    SELECT  Folder_FolderID_SEQ.NEXTVAL INTO v_newVal FROM DUAL;
    -- save this to emulate @@identity
   sqlserver_utilities.identity := v_newVal; 
   -- assign the value from the sequence to emulate the identity column
   :new.FolderID := v_newVal;
  END IF;
END;

/

CREATE OR REPLACE TRIGGER DatabaseRole_DatabaseRoleID_TR BEFORE INSERT OR UPDATE ON DatabaseRole
FOR EACH ROW
DECLARE 
v_newVal NUMBER(12) := 0;
v_incval NUMBER(12) := 0;
BEGIN
  IF INSERTING AND :new.DatabaseRoleID IS NULL THEN
    SELECT  DatabaseRole_DatabaseRoleID_SE.NEXTVAL INTO v_newVal FROM DUAL;
    -- save this to emulate @@identity
   sqlserver_utilities.identity := v_newVal; 
   -- assign the value from the sequence to emulate the identity column
   :new.DatabaseRoleID := v_newVal;
  END IF;
END;

/

CREATE OR REPLACE TRIGGER USERGROUPSESSIONS_USERGROUPS_1 BEFORE INSERT OR UPDATE ON UserGroupSessions
FOR EACH ROW
DECLARE 
v_newVal NUMBER(12) := 0;
v_incval NUMBER(12) := 0;
BEGIN
  IF INSERTING AND :new.UserGroupSessionID IS NULL THEN
    SELECT  UserGroupSessions_UserGroupSes.NEXTVAL INTO v_newVal FROM DUAL;
    -- save this to emulate @@identity
   sqlserver_utilities.identity := v_newVal; 
   -- assign the value from the sequence to emulate the identity column
   :new.UserGroupSessionID := v_newVal;
  END IF;
END;

/

CREATE OR REPLACE TRIGGER USERSESSIONS_USERSESSIONID_1 BEFORE INSERT OR UPDATE ON UserSessions
FOR EACH ROW
DECLARE 
v_newVal NUMBER(12) := 0;
v_incval NUMBER(12) := 0;
BEGIN
  IF INSERTING AND :new.UserSessionID IS NULL THEN
    SELECT  UserSessions_UserSessionID_SEQ.NEXTVAL INTO v_newVal FROM DUAL;
    -- save this to emulate @@identity
   sqlserver_utilities.identity := v_newVal; 
   -- assign the value from the sequence to emulate the identity column
   :new.UserSessionID := v_newVal;
  END IF;
END;

/

CREATE OR REPLACE TRIGGER OBJECTPARAMETERS_OBJECTPARAM_1 BEFORE INSERT OR UPDATE ON ObjectParameters
FOR EACH ROW
DECLARE 
v_newVal NUMBER(12) := 0;
v_incval NUMBER(12) := 0;
BEGIN
  IF INSERTING AND :new.ObjectParameterID IS NULL THEN
    SELECT  ObjectParameters_ObjectParamet.NEXTVAL INTO v_newVal FROM DUAL;
    -- save this to emulate @@identity
   sqlserver_utilities.identity := v_newVal; 
   -- assign the value from the sequence to emulate the identity column
   :new.ObjectParameterID := v_newVal;
  END IF;
END;

/

CREATE OR REPLACE TRIGGER CascadeFilters_FilterID_TRG BEFORE INSERT OR UPDATE ON CascadeFilters
FOR EACH ROW
DECLARE 
v_newVal NUMBER(12) := 0;
v_incval NUMBER(12) := 0;
BEGIN
  IF INSERTING AND :new.FilterID IS NULL THEN
    SELECT  CascadeFilters_FilterID_SEQ.NEXTVAL INTO v_newVal FROM DUAL;
    -- save this to emulate @@identity
   sqlserver_utilities.identity := v_newVal; 
   -- assign the value from the sequence to emulate the identity column
   :new.FilterID := v_newVal;
  END IF;
END;

/

CREATE OR REPLACE TRIGGER Categories_CategoryID_TRG BEFORE INSERT OR UPDATE ON Categories
FOR EACH ROW
DECLARE 
v_newVal NUMBER(12) := 0;
v_incval NUMBER(12) := 0;
BEGIN
  IF INSERTING AND :new.CategoryID IS NULL THEN
    SELECT  Categories_CategoryID_SEQ.NEXTVAL INTO v_newVal FROM DUAL;
    -- save this to emulate @@identity
   sqlserver_utilities.identity := v_newVal; 
   -- assign the value from the sequence to emulate the identity column
   :new.CategoryID := v_newVal;
  END IF;
END;

/

CREATE OR REPLACE TRIGGER COLUMNEXPLANATION_EXPLANATIO_1 BEFORE INSERT OR UPDATE ON ColumnExplanation
FOR EACH ROW
DECLARE 
v_newVal NUMBER(12) := 0;
v_incval NUMBER(12) := 0;
BEGIN
  IF INSERTING AND :new.ExplanationID IS NULL THEN
    SELECT  ColumnExplanation_ExplanationI.NEXTVAL INTO v_newVal FROM DUAL;
    -- save this to emulate @@identity
   sqlserver_utilities.identity := v_newVal; 
   -- assign the value from the sequence to emulate the identity column
   :new.ExplanationID := v_newVal;
  END IF;
END;

/

CREATE OR REPLACE TRIGGER REPORTSCHEDULES_REPORTSCHEDU_1 BEFORE INSERT OR UPDATE ON ReportSchedules
FOR EACH ROW
DECLARE 
v_newVal NUMBER(12) := 0;
v_incval NUMBER(12) := 0;
BEGIN
  IF INSERTING AND :new.ReportScheduleID IS NULL THEN
    SELECT  ReportSchedules_ReportSchedule.NEXTVAL INTO v_newVal FROM DUAL;
    -- save this to emulate @@identity
   sqlserver_utilities.identity := v_newVal; 
   -- assign the value from the sequence to emulate the identity column
   :new.ReportScheduleID := v_newVal;
  END IF;
END;

/

CREATE OR REPLACE TRIGGER REPORTPARAMETERVALUES_REPORT_1 BEFORE INSERT OR UPDATE ON ReportParameterValues
FOR EACH ROW
DECLARE 
v_newVal NUMBER(12) := 0;
v_incval NUMBER(12) := 0;
BEGIN
  IF INSERTING AND :new.ReportParameterValuesID IS NULL THEN
    SELECT  ReportParameterValues_ReportPa.NEXTVAL INTO v_newVal FROM DUAL;
    -- save this to emulate @@identity
   sqlserver_utilities.identity := v_newVal; 
   -- assign the value from the sequence to emulate the identity column
   :new.ReportParameterValuesID := v_newVal;
  END IF;
END;

/

CREATE OR REPLACE TRIGGER JOINRELATIONDETAILS_JOINRELA_1 BEFORE INSERT OR UPDATE ON JoinRelationDetails
FOR EACH ROW
DECLARE 
v_newVal NUMBER(12) := 0;
v_incval NUMBER(12) := 0;
BEGIN
  IF INSERTING AND :new.JoinRelationDetailsID IS NULL THEN
    SELECT  JoinRelationDetails_JoinRelati.NEXTVAL INTO v_newVal FROM DUAL;
    -- save this to emulate @@identity
   sqlserver_utilities.identity := v_newVal; 
   -- assign the value from the sequence to emulate the identity column
   :new.JoinRelationDetailsID := v_newVal;
  END IF;
END;

/

CREATE OR REPLACE TRIGGER UserReport_ID_TRG BEFORE INSERT OR UPDATE ON UserReport
FOR EACH ROW
DECLARE 
v_newVal NUMBER(12) := 0;
v_incval NUMBER(12) := 0;
BEGIN
  IF INSERTING AND :new.ID IS NULL THEN
    SELECT  UserReport_ID_SEQ.NEXTVAL INTO v_newVal FROM DUAL;
    -- save this to emulate @@identity
   sqlserver_utilities.identity := v_newVal; 
   -- assign the value from the sequence to emulate the identity column
   :new.ID := v_newVal;
  END IF;
END;

/

CREATE OR REPLACE TRIGGER Templates_TemplateID_TRG BEFORE INSERT OR UPDATE ON Templates
FOR EACH ROW
DECLARE 
v_newVal NUMBER(12) := 0;
v_incval NUMBER(12) := 0;
BEGIN
  IF INSERTING AND :new.TemplateID IS NULL THEN
    SELECT  Templates_TemplateID_SEQ.NEXTVAL INTO v_newVal FROM DUAL;
    -- save this to emulate @@identity
   sqlserver_utilities.identity := v_newVal; 
   -- assign the value from the sequence to emulate the identity column
   :new.TemplateID := v_newVal;
  END IF;
END;

/

CREATE OR REPLACE TRIGGER JoinRelation_JoinRelationID_TR BEFORE INSERT OR UPDATE ON JoinRelation
FOR EACH ROW
DECLARE 
v_newVal NUMBER(12) := 0;
v_incval NUMBER(12) := 0;
BEGIN
  IF INSERTING AND :new.JoinRelationID IS NULL THEN
    SELECT  JoinRelation_JoinRelationID_SE.NEXTVAL INTO v_newVal FROM DUAL;
    -- save this to emulate @@identity
   sqlserver_utilities.identity := v_newVal; 
   -- assign the value from the sequence to emulate the identity column
   :new.JoinRelationID := v_newVal;
  END IF;
END;

/

CREATE OR REPLACE TRIGGER CASCADEFILTERDETAILS_FILTERD_1 BEFORE INSERT OR UPDATE ON CascadeFilterDetails
FOR EACH ROW
DECLARE 
v_newVal NUMBER(12) := 0;
v_incval NUMBER(12) := 0;
BEGIN
  IF INSERTING AND :new.FilterDetailID IS NULL THEN
    SELECT  CascadeFilterDetails_FilterDet.NEXTVAL INTO v_newVal FROM DUAL;
    -- save this to emulate @@identity
   sqlserver_utilities.identity := v_newVal; 
   -- assign the value from the sequence to emulate the identity column
   :new.FilterDetailID := v_newVal;
  END IF;
END;

/

CREATE OR REPLACE TRIGGER DataFormats_FormatID_TRG BEFORE INSERT OR UPDATE ON DataFormats
FOR EACH ROW
DECLARE 
v_newVal NUMBER(12) := 0;
v_incval NUMBER(12) := 0;
BEGIN
  IF INSERTING AND :new.FormatID IS NULL THEN
    SELECT  DataFormats_FormatID_SEQ.NEXTVAL INTO v_newVal FROM DUAL;
    -- save this to emulate @@identity
   sqlserver_utilities.identity := v_newVal; 
   -- assign the value from the sequence to emulate the identity column
   :new.FormatID := v_newVal;
  END IF;
END;

/

CREATE OR REPLACE TRIGGER PermissionRights_PermissionRig BEFORE INSERT OR UPDATE ON PermissionRights
FOR EACH ROW
DECLARE 
v_newVal NUMBER(12) := 0;
v_incval NUMBER(12) := 0;
BEGIN
  IF INSERTING AND :new.PermissionRightID IS NULL THEN
    SELECT  PERMISSIONRIGHTS_PERMISSIONR_1.NEXTVAL INTO v_newVal FROM DUAL;
   -- save this to emulate @@identity
   sqlserver_utilities.identity := v_newVal; 
   -- assign the value from the sequence to emulate the identity column
   :new.PermissionRightID := v_newVal;
  END IF;
END;

/

INSERT INTO DataFormats (FormatKey, FormatName, Format, Internal, AppliesTo, SortOrder) VALUES 
	('GeneralNumber','General Number', 'General Number', 1, 9, 1);
INSERT INTO DataFormats (FormatKey, FormatName, Format, Internal, AppliesTo, SortOrder) VALUES 
	('Currency','Currency', 'Currency', 1, 9, 2);
INSERT INTO DataFormats (FormatKey, FormatName, Format, Internal, AppliesTo, SortOrder) VALUES
	('IntegerFormat','Integer', '#0', 1, 9, 3);
INSERT INTO DataFormats (FormatKey, FormatName, Format, Internal, AppliesTo, SortOrder) VALUES
	('Fixed','Fixed', 'Fixed', 1, 9, 4);
INSERT INTO DataFormats (FormatKey, FormatName, Format, Internal, AppliesTo, SortOrder) VALUES
	('Standard','Standard', 'Standard', 1, 9, 5);
INSERT INTO DataFormats (FormatKey, FormatName, Format, Internal, AppliesTo, SortOrder) VALUES
	('Percent','Percent', 'Percent', 1, 9, 6);
INSERT INTO DataFormats (FormatKey, FormatName, Format, Internal, AppliesTo, SortOrder) VALUES
	('Scientific','Scientific', 'Scientific', 1, 9, 7);
INSERT INTO DataFormats (FormatKey, FormatName, Format, Internal, AppliesTo, SortOrder) VALUES
	('TwoDigit','2-digit place holder', '2-digit place holder', 1, 9, 8);
INSERT INTO DataFormats (FormatKey, FormatName, Format, Internal, AppliesTo, SortOrder) VALUES
	('ThreeDigit','3-digit place holder', '3-digit place holder', 1, 9, 9);
INSERT INTO DataFormats (FormatKey, FormatName, Format, Internal, AppliesTo, SortOrder) VALUES
	('GeneralDate','General Date', 'General Date', 1, 10, 10);
INSERT INTO DataFormats (FormatKey, FormatName, Format, Internal, AppliesTo, SortOrder) VALUES
	('LongDate','Long Date', 'Long Date', 1, 10, 11);
INSERT INTO DataFormats (FormatKey, FormatName, Format, Internal, AppliesTo, IsAvailable, SortOrder) VALUES
	('MediumDate','Medium Date', 'Medium Date', 1, 10, 0, 12);
INSERT INTO DataFormats (FormatKey, FormatName, Format, Internal, AppliesTo, SortOrder) VALUES
	('ShortDate','Short Date', 'Short Date', 1, 10, 13);
INSERT INTO DataFormats (FormatKey, FormatName, Format, Internal, AppliesTo, SortOrder) VALUES
	('LongTime','Long Time', 'Long Time', 1, 10, 14);
INSERT INTO DataFormats (FormatKey, FormatName, Format, Internal, AppliesTo, IsAvailable, SortOrder) VALUES
	('MediumTime','Medium Time', 'Medium Time', 1, 10, 0, 15);
INSERT INTO DataFormats (FormatKey, FormatName, Format, Internal, AppliesTo, SortOrder) VALUES
	('ShortTime','Short Time', 'Short Time', 1, 10, 16);
INSERT INTO DataFormats (FormatKey, FormatName, Format, Internal, AppliesTo, SortOrder) VALUES
	('YesNo','Yes/No', 'Yes/No', 1, 12, 17);
INSERT INTO DataFormats (FormatKey, FormatName, Format, Internal, AppliesTo, SortOrder) VALUES
	('TrueFalse','True/False', 'True/False', 1, 12, 18);
INSERT INTO DataFormats (FormatKey, FormatName, Format, Internal, AppliesTo, SortOrder) VALUES
	('OnOff','On/Off', 'On/Off', 1, 12, 19);
INSERT INTO DataFormats (FormatKey, FormatName, Format, Internal, AppliesTo, SortOrder) VALUES
	('PreserveLineFeeds','Preserve Line Feeds', 'Preserve Line Feeds', 1, 8, 20);
INSERT INTO DataFormats (FormatKey, FormatName, Format, Internal, AppliesTo, SortOrder) VALUES
	('HTML','HTML', 'HTML', 2, 8, 21);

INSERT INTO Templates (TemplateName, Description, IsDefault, SortOrder, RightName) VALUES ('Tabular Report with Header', 'Start with a tabular report with a header.', 0, 1, '');
INSERT INTO Templates (TemplateName, Description, IsDefault, SortOrder, RightName) VALUES ('Tabular Report without Header', 'Start with a tabular report without a header or style. Perfect as a dashboard panel.', 1, 2, '');
INSERT INTO Templates (TemplateName, Description, IsDefault, SortOrder, RightName) VALUES ('Crosstab Report', 'Start with a crosstab, also known as a pivot table.', 0, 3, 'RB_CTB');
INSERT INTO Templates (TemplateName, Description, IsDefault, SortOrder, RightName) VALUES ('Bar Chart', 'Start with a bar chart without header or style. Perfect as a dashboard panel.', 0, 4, 'RB_BCT');
INSERT INTO Templates (TemplateName, Description, IsDefault, SortOrder, RightName) VALUES ('Tabular Report with Chart', 'Start with a tabular report and a pie chart.', 0, 5, 'RB_PCT');
INSERT INTO Templates (TemplateName, Description, IsDefault, SortOrder, RightName) VALUES ('Tabular Report with Export', 'Start with a tabular report with export to Excel, Word, and PDF.', 0, 6, 'RB_XLS,RB_WRD,RB_PDF');

Insert Into ObjectAccess Values (0, 'None');
Insert Into ObjectAccess Values (1, 'Limited');
Insert Into ObjectAccess Values (2, 'Full');

Insert Into Classes (Class,FriendlyName) Values ('bold', 'Bold');
Insert Into Classes (Class,FriendlyName) Values ('green', 'Green');
Insert Into Classes (Class,FriendlyName) Values ('red', 'Red');
Insert Into Classes (Class,FriendlyName) Values ('AlignLeft', 'Align Text Left');
Insert Into Classes (Class,FriendlyName) Values ('AlignCenter', 'Align Text Center');
Insert Into Classes (Class,FriendlyName) Values ('AlignRight', 'Align Text Right');
Insert Into Classes (Class,FriendlyName) Values ('imageAlignLeft', 'Align Image Left');
Insert Into Classes (Class,FriendlyName) Values ('imageAlignCenter', 'Align Image Center');
Insert Into Classes (Class,FriendlyName) Values ('imageAlignRight', 'Align Image Right');
Insert Into Classes (Class,FriendlyName) Values ('BlackTextYellowBackground', 'Black Text Yellow Background');
Insert Into Classes (Class,FriendlyName) Values ('WhiteTextGreenBackground', 'White Text Green Background');

Insert Into ColumnAccess Values (0, 'None');
Insert Into ColumnAccess Values (1, 'Full');

Insert Into UserGroups (GroupName) values ('Default');

Insert Into Role (RoleName) values ('System Admin');

Insert into RolePermissions (RoleID, PermissionID) Values (1,1);

Insert into UserGroupRoles (GroupID, RoleID) (select GroupID, RoleID from UserGroups, Role);

Insert into Users (UserName, Password, FirstName, GroupID) Values ('Admin', 'password', 'Administrator', 1);

Insert into UserRole (UserID,RoleID) (select UserID, RoleID from Users, Role);

commit;


