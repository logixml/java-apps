importPackage( java.io );
importPackage( java.lang );
var subjectReturn = "subject";
var fileDataReturn = "";

function GetEmail() {
	var fileObject = new File("@Input.Filename~");
        var contents = new StringBuffer();
        var reader = new BufferedReader(new FileReader(fileObject));
        var text;
         // repeat until all lines is read
        while ((text = reader.readLine()) != null)
        {
          contents.append(text)
        }
	reader.close();
	fileDataReturn = contents.toString();
        fileDataReturn = fileDataReturn.replace("~ReportName", "@Input.DynReportName~");
	fileDataReturn = fileDataReturn.replace("~ReportLink", "@Constants.DB-" + "@Input.DBID~" + ".ArchiveWebPath~/ahReport@Request.ReportID~/@Session.GUIDExt~/@Session.GUID~.@Input.FileType~");
	//return fileDataReturn;
}

function GetSubject() {
	var subject = "@Input.Subject~";
	subject = subject.replace("~ReportName", "@Input.DynReportName~");
	subjectReturn = subject;
}
