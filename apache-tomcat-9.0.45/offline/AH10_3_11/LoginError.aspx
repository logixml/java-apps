<%@ Page Language="VB" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title>Welcome to Logi 10 Ad Hoc Reporting</title>
    <link rel="stylesheet" type="text/css" href="Login.css">
</head>
<body background="ahImages/homePattern.gif">
<table width="100%" border="0" cellspacing="0" cellpadding="0" height="100%">
  <tr align="center" valign="middle"> 
    <td> 
      <table width="420" height="250" border="0" align="center" cellpadding="3" cellspacing="0">
        <tr bgcolor="#254974" align="center" valign="middle"> 
          <td> 
            <table width="100%" height="230" border="0" cellpadding="0" cellspacing="0">
              <tr bgcolor="#4A719C"> 
                <td align="center" valign="middle" bgcolor="#FFFFFF">
				 <table width="90%" height="230" border="0" cellpadding="0" cellspacing="0">
                  <tr>
                    <td align="center" valign="middle">
				      <font color="#CC3300"><%=Session("rdLogonFailMessage")%></font>
					</td>
                  </tr>
                </table>
               </td>
              </tr>
            </table>
          </td>
        </tr>
      </table>
     <br>
    </td>
  </tr>
</table>
</body>
</html>

