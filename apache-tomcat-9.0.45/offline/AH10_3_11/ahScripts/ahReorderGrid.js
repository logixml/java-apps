﻿// JScript File

var Dom = YAHOO.util.Dom;
var Event = YAHOO.util.Event;
var DDM = YAHOO.util.DragDropMgr;
//var gridID = "grdMain";
//var myDTDrags = {};
////var LastClass = "";

///*
//     Initialize and render the MenuBar when its elements are ready 
//     to be scripted.

//YAHOO.util.Event.onContentReady("grdMain", function () {
//    initializeTableDragAndDrop();
//});*/

//function initializeTableDragAndDrop(ctrlID, bInit) {

//    gridID = ctrlID;
//    
//    for(i=0; i<myDTDrags.length; i++) {
//        // Clean up any existing Drag instances
//        if (myDTDrags[i]) {
//             myDTDrags[i].unreg();
//             delete myDTDrags[i];
//        }
//    }
//    
//    //DDM.unregAll();
//    
//    if (bInit) {
//        new YAHOO.util.DDTarget(gridID);
//        
//        var cnt = 0;
//        var row = document.getElementById('trGrdMain_' + cnt);
//        while (row) {
//        
//            var dd = new YAHOO.example.DDReorderList(row.id);
//            var hdl = getControlInGrid(row, 'div', 'divDragHandle');
//            dd.setHandleElId(hdl.id);
//            myDTDrags[cnt] = dd;
//            
//            cnt++;
//            row = document.getElementById('trGrdMain_' + cnt);
//        }
//        DDM.refreshCache();
//    }
//}

//function resetRowOrders() {
//    //Find the first row in the grid
//    var grid = document.getElementById(gridID);
//    var rows = grid.getElementsByTagName('tr');
//    var row = rows[0];
//    var cnt = 0;
//    var inp;
//    var l = rows.length;
//    for (var i=0; i<l; i++) {
//        inp = rows[i];
//        if (inp.id.indexOf('trGrdMain_') > -1) {
//           row = inp;
//           break;
//        }
//    }
//     
//    var p = row.parentNode.childNodes;
//    l = p.length;
//    cnt = 0;
//    for (var i=0; i<l; i++) {
//        row = p[i];
//        if (row.tagName.toLowerCase() == 'tr') {
//            inp = getControlInGrid(row, 'input', 'RowOrder');
//            if (inp) {
//               inp.value = cnt;
//               cnt++;
//            }
//        }
//    }   

//    window.setTimeout('document.getElementById("btnFakeReorderRows").click()',10);
//}

function getControlInGrid(row, tagName, ctrlID){
    var oInputs = row.getElementsByTagName(tagName);
    var l = oInputs.length;
    //var row;
    var inp;
    for (var i=0; i<l; i++) {
        inp = oInputs[i];
        if (inp.id.indexOf(ctrlID) > -1) {
            return inp;
        }
    }
    return null;
}

//////////////////////////////////////////////////////////////////////////////
// custom drag and drop implementation
//////////////////////////////////////////////////////////////////////////////

YAHOO.example.DDReorderList = function(id, sGroup, config) {

    YAHOO.example.DDReorderList.superclass.constructor.call(this, id, sGroup, config);

    //this.logger = this.logger || YAHOO;
    var el = this.getDragEl();
    Dom.setStyle(el, "opacity", 0.67); // The proxy is slightly transparent

    this.goingUp = false;
    this.lastY = 0;
    this.multipleRows = false;
    this.preventDefault = false;
    this.resetOrder = new YAHOO.util.CustomEvent("resetOrder");
};

YAHOO.extend(YAHOO.example.DDReorderList, YAHOO.util.DDProxy, {

    startDrag: function(x, y) {
        //this.logger.log(this.id + " startDrag");

        // make the proxy look like the source element
        var dragEl = this.getDragEl();
        var clickEl = this.getEl();
        
        if (this.isRowSelected(clickEl)) {
            Dom.setStyle(clickEl, "visibility", "hidden");
            this.multipleRows = false;
            
            this.firstSelectedEl = clickEl;
            this.lastSelectedEl = clickEl;
            
            //Find the first one selected for this group
            var el = Dom.getPreviousSibling(this.firstSelectedEl);
            while ((el != null) && (this.isRowSelected(el))) {
                this.multipleRows = true;
                Dom.setStyle(el, "visibility", "hidden");
                this.firstSelectedEl = el;
                el = Dom.getPreviousSibling(this.firstSelectedEl);
            }
            
            //Find the last one selected for this group
            el = Dom.getNextSibling(this.lastSelectedEl);
            while ((el != null) && (this.isRowSelected(el))) {
                this.multipleRows = true;
                Dom.setStyle(el, "visibility", "hidden");
                this.lastSelectedEl = el;
                el = Dom.getNextSibling(this.lastSelectedEl);
            }
//            var hgt = this.lastSelectedEl.offsetHeight + (Dom.getY(this.lastSelectedEl) - Dom.getY(this.firstSelectedEl));
//            Dom.setStyle(dragEl, "height", hgt);
//            Dom.setXY(dragEl, Dom.getXY(this.firstSelectedEl));  
//            this.setDelta(0,100);
        }
        else {
            //This row is not selected so we can only move this row.
            this.multipleRows = false;
            Dom.setStyle(clickEl, "visibility", "hidden");
        }
        
        dragEl.innerHTML = "";
        //dragEl.innerHTML = clickEl.innerHTML;
        //dragEl.offsetWidth = 100;
        //dragEl.offsetHeight = 100;
        //Dom.setStyle(dragEl, "height", dragEl.offsetHeight * 3);
        
        Dom.setStyle(dragEl, "color", Dom.getStyle(clickEl, "color"));
        Dom.setStyle(dragEl, "backgroundColor", Dom.getStyle(clickEl, "backgroundColor"));
        Dom.setStyle(dragEl, "border", "2px solid gray");
        Dom.setStyle(dragEl, "z-index", "10005");
    },

    endDrag: function(e) {

        var srcEl = this.getEl();
        var proxy = this.getDragEl();

        // Show the proxy element and animate it to the src element's location
        Dom.setStyle(proxy, "visibility", "");
        var a = new YAHOO.util.Motion( 
            proxy, { 
                points: { 
                    to: Dom.getXY(srcEl)
                }
            }, 
            0.2, 
            YAHOO.util.Easing.easeOut 
        )
        var proxyid = proxy.id;
        var thisid = this.id;
        
        //resetRowOrders();
        this.resetOrder.fire();
        
        // Hide the proxy and show the source element when finished with the animation
        a.onComplete.subscribe(function() {
                Dom.setStyle(proxyid, "visibility", "hidden");
                Dom.setStyle(thisid, "visibility", "");
            });
        a.animate();
    },

    onDragDrop: function(e, id) {

        // If there is one drop interaction, the li was dropped either on the list,
        // or it was dropped on the current location of the source element.
        if (DDM.interactionInfo.drop.length === 1) {

            // The position of the cursor at the time of the drop (YAHOO.util.Point)
            var pt = DDM.interactionInfo.point; 

            // The region occupied by the source element at the time of the drop
            var region = DDM.interactionInfo.sourceRegion; 

            // Check to see if we are over the source element's location.  We will
            // append to the bottom of the list once we are sure it was a drop in
            // the negative space (the area of the list without any list items)
            if (!region.intersect(pt)) {
                var destEl = Dom.get(id);
                var destDD = DDM.getDDById(id);
                destEl.appendChild(this.getEl());
                destDD.isEmpty = false;
                DDM.refreshCache();
            }

        }
    },

    onDrag: function(e) {

        // Keep track of the direction of the drag for use during onDragOver
        var y = Event.getPageY(e);

        if (y < this.lastY) {
            this.goingUp = true;
        } else if (y > this.lastY) {
            this.goingUp = false;
        }

        this.lastY = y;
    },

    onDragOver: function(e, id) {
    
        var srcEl = this.getEl();
        var destEl = Dom.get(id);

        // We are only concerned with list items, we ignore the dragover
        // notifications for the list.
        if (destEl.nodeName.toLowerCase() == "tr") {
            var orig_p = srcEl.parentNode;
            var p = destEl.parentNode;

            if (this.multipleRows) {
                var bMove = true;
                var el = this.firstSelectedEl;
                while ((el != null) && (this.isRowSelected(el))) {
                    if (el.id == destEl.id) {
                        bMove = false;
                        break;
                    }   
                    el = el.nextSibling;
                }
                
                if (bMove) {
                    el = this.firstSelectedEl;
                    while ((el != null) && (this.isRowSelected(el))) {
                        var nextEl = Dom.getNextSibling(el);
                        if (this.goingUp) {
                            p.insertBefore(el, destEl); // insert above
                            //destEl = el.nextSibling;
                        } else {
                            p.insertBefore(el, destEl.nextSibling); // insert below
                            destEl = el;
                        }
                        el = nextEl;
                    }
                }
            }
            else {
                if (this.goingUp) {
                    p.insertBefore(srcEl, destEl); // insert above
                } else {
                    p.insertBefore(srcEl, destEl.nextSibling); // insert below
                }
            }

            DDM.refreshCache();
        }
    }, 
    
    isRowSelected: function(row) {
        var oInputs = row.getElementsByTagName('input');
        var l = oInputs.length;
        var row;
        var inp;
        for (var i=0; i<l; i++) {
            inp = oInputs[i];
            if (inp.id.indexOf("chk_Select") > -1) {
                if (inp.checked)
                    return true;
                else
                    return false;
            }
        }
        return false;
    }
});