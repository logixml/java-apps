﻿// JScript File

//Drag and Drop functions start here................
var Dom = YAHOO.util.Dom;
var Event = YAHOO.util.Event;
var DDM = YAHOO.util.DragDropMgr;

var nextAvailableRow = 1;
var destPanelID;
var srcPanelID;
var totalActualRows = 1;
var totalActualCols = 3;
var numPanels = 1;
var proxyCnt = 0;
var myDTDrags = new Array();

function initializeMenuDragDrop(containerID) {
    for(i=0; i<myDTDrags.length; i++) {
        // Clean up any existing Drag instances
        if (myDTDrags[i]) {
             myDTDrags[i].unreg();
             delete myDTDrags[i];
        }
    }
    
    DDM.unregAll();

//    var sUniquePBCode = document.getElementById("ihNewPostbackID").value;
    
    //Set Drag and Drop Targets
    var div = document.getElementById(containerID);
    var menuDiv;
    var idx = 0;
    if (div) {
        //Targets are pre-defined
        var tb = document.getElementById("tbReportContents");
        var trs = tb.getElementsByTagName("tr"); 
        for (var i=0;i<trs.length;i++) {
            var tr = trs[i];
            var trID = tr.getAttribute("id");
            switch (trID.substr(0, 7)) {
                case "trHeade": 
                    var strGrp = tr.getAttribute("ahDDGroup");
                    if (strGrp != null && strGrp != "") {
                        var bx = new YAHOO.util.DDTarget(trID);
                        myDTDrags[idx] = bx;
                        bx.addToGroup('header');
                        idx++;
                    }
                    break;
                case "trConte":
                    var strGrp = tr.getAttribute("ahDDGroup");
                    if (strGrp != null && strGrp != "") {
                        myDTDrags[idx] = new YAHOO.example.DDList(trID, "content1");
                        idx++;
                         
                         // Also make these targets
                        var bx = new YAHOO.util.DDTarget(trID);
                        myDTDrags[idx] = bx;
                        bx.addToGroup('content');
                        bx.addToGroup('content1');
                        idx++;
                    }
                    break;
                case "trEmpty":
                    var strGrp = tr.getAttribute("ahDDGroup");
                    if (strGrp != null && strGrp != "") {
                        var bx = new YAHOO.util.DDTarget(trID);
                        myDTDrags[idx] = bx;
                        bx.addToGroup('content');
                        bx.addToGroup('content1');
                        idx++;
                    }
                    break;
                case "trFoote":
                    var strGrp = tr.getAttribute("ahDDGroup");
                    if (strGrp != null && strGrp != "") {
                        var bx = new YAHOO.util.DDTarget(trID);
                        myDTDrags[idx] = bx;
                        bx.addToGroup('footer');
                        idx++;
                    }
                    break;
            }
        }

        //Lists
        var cells = div.getElementsByTagName("div"); 
        for (var i = 0; i < cells.length; i++) { 
            menuDiv = cells[i];
            var divID = menuDiv.getAttribute("id").substr(0, 8);
            
            if (divID != 'disabled') { 
                var strGrp = menuDiv.getAttribute("ahDDGroup");
                myDTDrags[idx] = new YAHOO.example.DDList(menuDiv.getAttribute("id"), strGrp);
                idx++;
            }
        }
    }   
    
}

function addContentAnimationDone() {
   addContentToTemplate(srcPanelID, destPanelID);
   //nextAvailableRow = nextAvailableRow + 1;
   var el = this.getEl();
   el.parentNode.removeChild(el);
}

function AddContent(imgID) {
    //Create a proxy element that will be moved as animation.

    var xy = YAHOO.util.Dom.getXY(imgID);
    var origDiv = document.getElementById(imgID);
    var proxyEle = getCopyElement(origDiv);

    //get the next available container here.
    //var pnl = document.getElementById('pnlRightTemplate');
    //pnl.appendChild(proxyEle);
    var sGroup = origDiv.getAttribute("ahDDGroup");
    if (sGroup == 'content') {
        var ih = document.getElementById("ihActiveDataID");
        sGroup = sGroup + ih.value;
    }
    
    var pnl = getLastAvailablePanel(sGroup);
    destPanelID = pnl.id;
    srcPanelID = imgID;
    pnl.appendChild(proxyEle);
    
    pnl.style.height="";
    
    //Set the position of proxy element on top of current element.
    YAHOO.util.Dom.setXY(proxyEle.id, xy);
    
    //Get the absolute position of final container element.
    var pnlXY = YAHOO.util.Dom.getXY(pnl.id);
    var x = YAHOO.util.Dom.getX(pnl.id);
    var y = YAHOO.util.Dom.getY(pnl.id);
    
    //animate the proxy element to move from orig position to new pos.
    var attributes = { 
        points: { to: [x, y] }
    };
    var anim = new YAHOO.util.Motion(proxyEle.id, attributes); 
    anim.duration = 0.5; 
    anim.onComplete.subscribe(addContentAnimationDone);  
    anim.animate();
    //proxyEle.style.display = 'none';
}

function getCopyElement(ele) {
    var copy = ele.cloneNode(true);
    resetElementID(copy);
    proxyCnt = proxyCnt + 1;
    return copy;
}
function resetElementID(ele) {
    if (ele.id) {
        ele.id = ele.id + '_' + proxyCnt;
    }
    for (var i=0;i<ele.childNodes.length;i++) {
        resetElementID(ele.childNodes[i]);
    }
}

function getLastAvailablePanel(sGroup){    
    var pnl;
//    var sUniquePBCode = document.getElementById("ihNewPostbackID").value;

    switch (sGroup) {
        case "header":
            pnl = document.getElementById('divHeader');
            break;
        case "footer":
            pnl = document.getElementById('divFooter');
            break;
        default:
            pnl = document.getElementById('divEmpty');
            // Now set the position of the new element
            var ih = document.getElementById("ihAddContentPosition");
            ih.value = pnl.getAttribute("ahDDPosition");
    }

    return pnl;
}

function addContentToTemplate(srcDivID, destDivID) {
    var div = document.getElementById(srcDivID);
    var img = div.firstChild;
    var destDiv = document.getElementById(destDivID);
    if ((img) && (destDiv)) {        
        //var newPnl = document.createElement("div");
        //newPnl.innerText = "Adding Content...";
        var newPnl = getCopyElement(img);
        destDiv.appendChild(newPnl);
        destDiv.style.height="";
//        destDiv.style.backgroundColor = "#c0c0c0";
//        destDiv.style.border="solid 1px gray";
        var contID = document.getElementById("ihAddContentID");
        contID.value = div.getAttribute("ahContentID");
                
        var cTypeID = document.getElementById("ihAddChartTypeID");
        cTypeID.value = div.getAttribute("ahChartTypeID");
        
        if (cTypeID.value == "0") {
            newPnl.setAttribute('src','../ahImages/thumb_' + contID.value + '.png');
        }
        else {
            newPnl.setAttribute('src','../ahImages/ChartTypes/Chart' + cTypeID.value + '.png');
        }
        
        window.setTimeout('document.getElementById("btnFakeAddContent").click()',10);
        //document.getElementById("btnFakeAddContent").click();
        //img.click();
    }      
}

function moveReportContent(srcDivID) {
    var srcDiv = document.getElementById(srcDivID);
    if (srcDiv) {        
        var contID = document.getElementById("ihAddContentID");
        contID.value = srcDiv.getAttribute("ahContentIndex");
        var ih = document.getElementById("ihAddContentPosition");
        ih.value = srcDiv.getAttribute("ahNewPos");
        
        window.setTimeout('document.getElementById("btnFakeMoveReportContent").click()',10);
    }
}

YAHOO.example.DDList = function(id, sGroup, config) {
    YAHOO.example.DDList.superclass.constructor.call(this, id, sGroup, config);

    //this.logger = this.logger || YAHOO;
    var el = this.getDragEl();
    Dom.setStyle(el, "opacity", 0.67); // The proxy is slightly transparent
    this.dropTargetID = null;
};

YAHOO.extend(YAHOO.example.DDList, YAHOO.util.DDProxy, {

    startDrag: function(x, y) {
        //this.logger.log(this.id + " startDrag");

        // make the proxy look like the source element
        var dragEl = this.getDragEl();
        var clickEl = this.getEl();
        Dom.setStyle(dragEl, "opacity", 0.67);

        dragEl.innerHTML = clickEl.innerHTML;

//        var s = clickEl.id.substr(0,6);
//        if (s == 'btnAdd') {
//            dragEl.src = clickEl.src;
//        }
        
        Dom.setStyle(dragEl, "color", Dom.getStyle(clickEl, "color"));
        Dom.setStyle(dragEl, "backgroundColor", Dom.getStyle(clickEl, "backgroundColor"));
        Dom.setStyle(dragEl, "border", "1px solid gray");
        
        var targets = YAHOO.util.DDM.getRelated(this, true);
        //YAHOO.log(targets.length + " targets", "info", "example");
        for (var i=0; i<targets.length; i++) {
            
            var targetEl = targets[i].getEl();
            var s = targetEl.id.substr(0,6);
            if ((s == "trCont") || (s == "trHead") || (s == "trFoot")) {
                targetEl.style.backgroundColor = "#9CBD5A"
//              if (!this.originalStyles[targetEl.id]) {
//                    this.originalStyles[targetEl.id] = targetEl.className;
//              }
//              targetEl.className = "target";
            }
        }
    },

    endDrag: function(e) {
        this.resetTargets();

        var srcEl = this.getEl();
        var proxy = this.getDragEl();

        // Show the proxy element and animate it to the src element's location
        Dom.setStyle(proxy, "visibility", "");
        var a = new YAHOO.util.Motion( 
            proxy, { 
                points: { 
                    to: Dom.getXY(srcEl)
                }
            }, 
            0.2, 
            YAHOO.util.Easing.easeOut 
        )
        
        var proxyid = proxy.id;
        var thisid = this.id;

        // Hide the proxy and show the source element when finished with the animation
        a.onComplete.subscribe(function() {
                Dom.setStyle(proxyid, "visibility", "hidden");
                Dom.setStyle(thisid, "visibility", "");
            });
        a.animate();
    },

    onDragDrop: function(e, id) {
//        this.highlightDropTarget(null);
        // If there is one drop interaction, we are dropping a new content on the report.
        if (DDM.interactionInfo.drop.length === 1) {
            var clickEl = this.getEl();
            var srcID = clickEl.getAttribute('id');
            var s = srcID.substr(0,6);
                
            if ((s == 'trCont') && (clickEl.getAttribute("ahNewPos") != clickEl.getAttribute("ahDDPosition"))) {
                moveReportContent(srcID);
                DDM.refreshCache();
            } else {
                if (s == 'btnAdd') {
                    var destDD = DDM.getDDById(id);

//                    var ih = document.getElementById("ihAddContentPosition");
//                    ih.value = srcDiv.getAttribute("ahNewPos");
//                    var eleDest = document.getElementById(id);
                    var ih = document.getElementById("ihAddContentPosition");
                    ih.value = document.getElementById(id).getAttribute("ahDDPosition");
                    //We are adding a new content using drag and drop.
                    addContentToTemplate(srcID, id);

                    destDD.isEmpty = false;
                    DDM.refreshCache();
                }
            }

            Dom.setStyle(clickEl, "opacity", 1);
        }
        this.resetTargets();
    },

//    onDragEnter: function(e, id) {
//        var src = this.getEl();
//        var el;

//        // this is called anytime we drag over
//        // a potential valid target
//        // highlight the target in red
//        if ("string" == typeof id) {
//            el = YAHOO.util.DDM.getElement(id);		
//        } else {
//            el = YAHOO.util.DDM.getBestMatch(id).getEl();
//        }
//    	//el.style.backgroundColor = "#ff0000";
//    	this.highlightDropTarget(el);
//    },

//    onDragOut: function(e, id) {
//        var el;

//        // this is called anytime we drag out of
//        // a potential valid target
//        // remove the highlight
//        if ("string" == typeof id) {
//            el = YAHOO.util.DDM.getElement(id);
//        } else {
//            el = YAHOO.util.DDM.getBestMatch(id).getEl();
//        }
//        //el.style.backgroundColor = "";
//        this.highlightDropTarget(null);
//    }, 
    
//    highlightDropTarget: function (el) {
//        if (this.dropTargetID) {
//            var a = document.getElementById(this.dropTargetID);
//            a.style.backgroundColor = "#9999AA";
//            this.dropTargetID = null;
//        }
//        if (el) {
//            var s = el.id.substr(0,6);
//            if (s == "tbRepo") {
//                el.style.backgroundColor = "#00FF00";
//                this.dropTargetID = el.id;
//            }
//        }
//    },

    onDrag: function(e) {
        // Keep track of the direction of the drag for use during onDragOver
        var y = Event.getPageY(e);

        if (y < this.lastY) {
            this.goingUp = true;
        } else if (y > this.lastY) {
            this.goingUp = false;
        }

        this.lastY = y;
    },

    onDragOver: function(e, id) {
        var srcEl = this.getEl();
        var s = srcEl.id.substr(0,6);
        if (s == 'trCont') {
            var destEl = Dom.get(id);
            
            // We are only concerned with list items, we ignore the dragover
            // notifications for the list.
//            if (destEl.nodeName.toLowerCase() == "img") {
            if (id.substr(0,6) == "trCont") {
                var p = destEl.parentNode;
                
                // swap the elements' position
                var tmp = srcEl.getAttribute("ahNewPos");
                srcEl.setAttribute("ahNewPos", destEl.getAttribute("ahNewPos"));
                destEl.setAttribute("ahNewPos", tmp);
                
                if (this.goingUp) {
                    p.insertBefore(srcEl, destEl); // insert above
                } else {
                    p.insertBefore(srcEl, destEl.nextSibling); // insert below
                }

                DDM.refreshCache();
            }
        } else {
            if ((s == 'btnAdd')) {
                var destEl = Dom.get(id);
                
                // We are only concerned with list items, we ignore the dragover
                // notifications for the list.
                if ((id.substr(0,6) == "trCont") || (id == "trEmpty")) {
                    var cells = destEl.getElementsByTagName("div");
                    var eleChild = cells[0];
                    var p = eleChild.parentNode;
                    p.insertBefore(srcEl, eleChild); // insert above
                    DDM.refreshCache();
                }
            }
        }
    },
    
    resetTargets: function() {

        // reset the target styles
        var targets = YAHOO.util.DDM.getRelated(this, true);
        for (var i=0; i<targets.length; i++) {
            var targetEl = targets[i].getEl();
            targetEl.style.backgroundColor = "";
        }
    }
});
