<script type="text/javascript">

   function validateMultEmails(str, validateEmpty)
   {
      if (str == '' && validateEmpty) return true;

      var email = str.split(';');
      for (var i = 0; i < email.length; i++) {
         if (!validateEmail(TrimString(email[i]), 0)) {
            //alert('One or more email addresses entered is invalid');
            return false;
         }
      }
      return true;
   } 

   function validateEmail(str)
   {
      var emailpat = /^([a-zA-Z0-9])+([\.a-zA-Z0-9_-])*@([a-zA-Z0-9])+(\.[a-zA-Z0-9_-]+)+$/;
      //if( !emailpat.test(str) && promptMsg ) alert( 'Please enter a valid email address in the From field' );
      if( !emailpat.test(str) ) return false;
      return true;
   }

   function TrimString(sInString) {
      sInString = sInString.replace( /^\s+/g, "" );// strip leading
      return sInString.replace( /\s+$/g, "" );// strip trailing
   }

</script>
