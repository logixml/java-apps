<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="ReportRename.aspx.vb" Inherits="LogiAdHoc.ahReport_ReportRename" Culture="auto" meta:resourcekey="PageResource1" UICulture="auto" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>

<%@ Register TagPrefix="AdHoc" TagName="MainMenu" Src="~/ahControls/MainMenu.ascx" %>
<%@ Register TagPrefix="AdHoc" TagName="NavMenu" Src="~/ahControls/NavMenu.ascx" %>
<%@ Register TagPrefix="AdHoc" TagName="BreadCrumbTrail" Src="~/ahControls/BreadCrumbTrail.ascx" %>
<%@ Register tagprefix="wizard" tagname="datebox1" src="../ahControls/DateBox.ascx" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title><asp:Localize ID="Localize6" runat="server" Text="<%$ Resources:LogiAdHoc, RenameReport %>"></asp:Localize></title>
    <link rel="shortcut icon" href="../ahImages/flav.ico" />
    <script language="JavaScript" type="text/javascript" src="../ahScripts/ahChange.js"></script>
    <script type="text/javascript">
        window.onbeforeunload = AlertOnExit;
    </script>

</head>
<body id="bod">
    <AdHoc:MainMenu ID="menu" runat="server" />
    
    <form id="form1" runat="server">
    	<div id="submenu"><AdHoc:NavMenu id="subnav" runat="server" /></div>	
        <AdHoc:BreadCrumbTrail ID="bct" runat="server" Key="RenameReport" />
        <asp:ScriptManager ID="ScriptManager1" EnablePartialRendering="true" runat="server" />
        
        <script type="text/javascript">
        Sys.WebForms.PageRequestManager.getInstance().add_endRequest(EndRequestHandler);
        function EndRequestHandler(sender, args) {
            noMessage=false;
            $find('ahSavePopupBehavior').hide();
        }
        </script>
        
        <div class="divForm">
        <input type="hidden" id="ahDirty" name="ahDirty" runat="server" />
        <input type="hidden" id="DatabaseID" runat="server" />
        <input type="hidden" id="PhysicalName" name="PhysicalName" runat="server" />
        <asp:UpdatePanel ID="UP1" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <table>
                    <tr>
                        <td width="100px">
                            <asp:Label ID="Label1" runat="server" AssociatedControlID="reportname" Text="<%$ Resources:LogiAdHoc, ReportName %>"></asp:Label>:
                        </td>
                        <td>
                            <input type="text" id="reportname" size="45" maxlength="200" runat="server" name="reportname">
                            <asp:RequiredFieldValidator ID="rtvreportname" runat="server" ErrorMessage="Report Name is required."
                                ControlToValidate="reportname" meta:resourcekey="rtvreportnameResource1">*</asp:RequiredFieldValidator>
                            <asp:CustomValidator ID="cvValidReportName" runat="server" ErrorMessage="<%$ Resources:Errors, Err_ReportNameUnique %>"
                                ControlToValidate="reportname" OnServerValidate="IsReportNameValid" meta:resourcekey="cvValidReportNameResource1">*</asp:CustomValidator>
                            <%--<asp:CustomValidator ID="cvValidReportName" runat="server" ErrorMessage="Invalid report filename."
                                ControlToValidate="reportname" OnServerValidate="ValidReportName" meta:resourcekey="cvValidReportNameResource1">*</asp:CustomValidator>--%>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label ID="Label2" runat="server" AssociatedControlID="Description" Text="<%$ Resources:LogiAdHoc, Description %>"></asp:Label>:
                        </td>
                        <td>
                            <asp:TextBox ID="Description" TextMode="MultiLine" Columns="45" Rows="3" runat="server" />
                            <asp:CustomValidator ID="cvMaxLength" runat="server" ErrorMessage="<%$ Resources:Errors, Err_Description255 %>"
                                ControlToValidate="Description" OnServerValidate="ReachedMaxLength">*</asp:CustomValidator>
                        </td>
                    </tr>
                    <tr id="trExpirationDate" runat="server">
                        <td width="100px">
                            <asp:Literal ID="Literal7" runat="server" Text="<%$ Resources:LogiAdHoc, ReportExpirationDate %>"></asp:Literal>:
                            <%--<label for="ReportExpirationDate_DateValue"><asp:Literal runat="server" Text="<%$ Resources:LogiAdHoc, ReportExpirationDate %>"></asp:Literal>:</label>--%>
                            
                        </td>
                        <td>
                            <wizard:datebox1 id="ReportExpirationDate" runat="server" Required="false" />
                            <asp:CustomValidator ID="cvReportExpirationDate" ControlToValidate="Description"
                                ErrorMessage="Report Expiration Date should be greater than today." OnServerValidate="IsReportExpirationDateValid"
                                runat="server" ValidateEmptyText="true" meta:resourcekey="cvReportExpDateResource1">*</asp:CustomValidator>
                        </td>
                    </tr>
                </table>
                <div id="divButtons" class="divButtons">
                    <AdHoc:LogiButton ID="Save" Text="<%$ Resources:LogiAdHoc, Save %>" OnClick="Save_OnClick" ToolTip="<%$ Resources:LogiAdHoc, SaveReportListTooltip %>"
                        runat="server" UseSubmitBehavior="false" />
                    <AdHoc:LogiButton ID="Cancel" Text="<%$ Resources:LogiAdHoc, BackToReportsList %>" CausesValidation="False"
                        OnClick="Cancel_OnClick" ToolTip="<%$ Resources:LogiAdHoc, BackToReportsListTooltip %>"
                        runat="server" />
                    <asp:ValidationSummary ID="vsummary" runat="server" meta:resourcekey="vsummaryResource1" />
                </div>
            </ContentTemplate>
            <Triggers>
                <asp:PostBackTrigger ControlID="Cancel" />
            </Triggers>
        </asp:UpdatePanel>
<br />
        <div id="divSaveAnimation">
            <asp:Button runat="server" ID="btnSaveAnimation" Style="display: none" />
            <ajaxToolkit:ModalPopupExtender runat="server" ID="ahSavePopup" BehaviorID="ahSavePopupBehavior"
                TargetControlID="btnSaveAnimation" PopupControlID="pnlSaving" BackgroundCssClass="modalBackground"
                DropShadow="False">
            </ajaxToolkit:ModalPopupExtender>
            <asp:Panel id="pnlSaving" CssClass="savePopup" runat="server" style="display:none;">
                <table><tr><td>
                <asp:Image ID="imgSave" runat="server" SkinID="imgSaving" EnableViewState="false" AlternateText="<%$ Resources:LogiAdHoc, SaveImageAltText %>" />
                </td>
                <td valign="middle">
                <asp:Label ID="lblSaveText" CssClass="lblSavePopup" runat="server" EnableViewState="false" Text="<%$ Resources:LogiAdHoc, PleaseWait %>"/>
                </td></tr></table>
            </asp:Panel>
        </div>
        </div>
    </form>
</body>
</html>
