<%@ Page Language="vb" AutoEventWireup="false" Codebehind="ReportScheduleList.aspx.vb"
    Inherits="LogiAdHoc.ahReport_ReportScheduleList" Culture="auto" meta:resourcekey="PageResource1" UICulture="auto" %>

<%@ Register TagPrefix="AdHoc" TagName="MainMenu" Src="~/ahControls/MainMenu.ascx" %>
<%@ Register TagPrefix="AdHoc" TagName="NavMenu" Src="~/ahControls/NavMenu.ascx" %>
<%@ Register TagPrefix="AdHoc" TagName="BreadCrumbTrail" Src="~/ahControls/BreadCrumbTrail.ascx" %>
<%@ Register TagPrefix="AdHoc" TagName="PagingControl" Src="~/ahControls/PagingControl.ascx" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Schedules</title>
    <link rel="shortcut icon" href="../ahImages/flav.ico" />
    <script language="JavaScript" type="text/javascript" src="../ahScripts/ahChange.js"></script>
</head>
<body>
    <AdHoc:MainMenu ID="menu" runat="server" />
    <form id="form1" runat="server">
    <div id="submenu">
        <AdHoc:NavMenu ID="subnav" runat="server" />
    </div>
        <AdHoc:BreadCrumbTrail ID="bct" runat="server" Key="Schedules" />
        
        <asp:ScriptManager ID="ScriptManager1" runat="server"
            ScriptMode="Release" />

        <script type="text/javascript">
            Sys.WebForms.PageRequestManager.getInstance().add_endRequest(EndRequestHandler);
            function EndRequestHandler(sender, args) {
                $find('ahSavePopupBehavior').hide();
            }
        </script>

<table class="limiting"><tr><td>
        <table class="gridForm"><tr><td>
            <input type="hidden" id="ReportName" name="ReportName" runat="server" />
            <input type="hidden" id="PhysicalName" name="PhysicalName" runat="server" />
            <asp:UpdatePanel ID="UP1" UpdateMode="Conditional" runat="server">
                <ContentTemplate>
                    <div id="NoSchedule" runat="server">
                        <asp:Localize ID="Localize1" runat="server" meta:resourcekey="LiteralResource1" Text="No schedules were found for this report."></asp:Localize>                
                        <br />
                        <br />
                    </div>
                <div id="data_main">
                    <div id="activities">
                        <table width="100%" cellpadding="0" cellspacing="0">
                        <tr width="100%">
                        <td align="left" valign="top">
                    <AdHoc:LogiButton ID="btnCreateScheduleTop" runat="server" CausesValidation="False" 
                        OnClick="CreateSchedule" Text="<%$ Resources:LogiAdHoc, AddWithSpaces %>"  />
                    <AdHoc:LogiButton ID="btnRemoveScheduleTop" runat="server" CausesValidation="False" 
                        OnClick="RemoveSchedule" Text="<%$ Resources:LogiAdHoc, Delete %>"  />
                        </td>
                        </tr>
                        </table>
                    </div>
                    <asp:GridView id="grdMain" runat="server" CssClass="grid" AllowSorting="True" 
                    AutoGenerateColumns="False" AllowPaging="True" OnRowDataBound="OnItemDataBoundHandler" 
                    OnSorting="OnSortCommandHandler" DataKeyNames="ReportScheduleID" meta:resourcekey="grdMainResource1" >
                        <Columns>
                            <asp:TemplateField>
                                <HeaderStyle Width="30px"></HeaderStyle>
                                <HeaderTemplate>
                                    <asp:Label ID="lblChkAll" runat="server" AssociatedControlID="CheckAll" Text="<%$ Resources:LogiAdHoc, SelectDeselectAll %>" CssClass="NoShow"></asp:Label>
                                    <asp:CheckBox runat="server" ToolTip="<%$ Resources:LogiAdHoc, SelectDeselectAll %>" ID="CheckAll" />
                                </HeaderTemplate>
                                <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblChk" runat="server" AssociatedControlID="chk_Select" Text="<%$ Resources:LogiAdHoc, SelectDeselect %>" CssClass="NoShow"></asp:Label>
                                    <asp:CheckBox runat="server" ID="chk_Select" ToolTip="<%$ Resources:LogiAdHoc, SelectDeselect %>" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField SortExpression="FrequencyDesc" HeaderText="Frequency" meta:resourcekey="TemplateFieldResource2">
                                <ItemTemplate>
                                    <asp:Image ID="ObjectType" runat="server" meta:resourcekey="ObjectTypeResource1" />
                                    <asp:Label ID="lblFrequency" runat="server" meta:resourcekey="lblFrequencyResource1" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField SortExpression="Schedule" HeaderText="Schedule" meta:resourcekey="TemplateFieldResource3">
                                <ItemTemplate>
                                    <INPUT id="TaskName" type="hidden" runat="server" />
                                    <INPUT id="TaskID" type="hidden" runat="server" />
                                    <INPUT id="ReportScheduleID" type="hidden" runat="server" />
                                    <asp:Label id="Schedule" Runat="server" meta:resourcekey="ScheduleResource1"/><BR />
                                    <asp:Label id="LastRun" Runat="server" meta:resourcekey="LastRunResource1" /><BR />
                                    <asp:Label id="NextRun" Runat="server" meta:resourcekey="NextRunResource1" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="<%$ Resources:LogiAdHoc, Actions %>">
                                <HeaderStyle Width="50px" />
                                <ItemStyle HorizontalAlign="Center" />
                                <ItemTemplate>
                                    <asp:Image ID="imgActions" AlternateText="Actions" runat="server" ToolTip="Actions"
                                        ImageUrl="~/ahImages/arrowStep.gif" SkinID="imgActions" />
                                        
                                    <asp:Panel ID="pnlActionsMenu" runat="server" CssClass="popupMenu" 
                                        HorizontalAlign="Left" Wrap="false" style="display:none;">
                                        <div id="divModify" runat="server" class="hoverMenuActionLink" >
                                            <asp:LinkButton ID="lnkModify" runat="server" Text="Modify Schedule" meta:resourcekey="ModifyResource1"
                                                OnCommand="ModifySchedule" ></asp:LinkButton>
                                        </div>
                                        <div id="divSubscription" runat="server" class="hoverMenuActionLink" >
                                            <asp:LinkButton ID="lnkSubscription" runat="server" Text="Change Subscription" meta:resourcekey="SubscriptionResource1"
                                                OnCommand="ChangeSubscription" ></asp:LinkButton>
                                        </div>
                                        <div id="divRunSchedule" runat="server" class="hoverMenuActionLink" >
                                            <asp:LinkButton ID="lnkRunSchedule" runat="server" Text="Run Schedule" meta:resourcekey="RunResource1"
                                                OnCommand="RunSchedule" ></asp:LinkButton>
                                        </div>
 				                    </asp:Panel>
                                    <ajaxToolkit:HoverMenuExtender ID="hme2" runat="server"
                                        PopupControlID="pnlActionsMenu" PopupPosition="right" 
                                        TargetControlID="imgActions" PopDelay="25" />
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <PagerTemplate>
                            <AdHoc:PagingControl id="pageCtrl" runat="server" OnGotoNextPage="ChangePageIndex" />
                        </PagerTemplate>
                        <PagerStyle HorizontalAlign="Center"></PagerStyle>
                        <HeaderStyle CssClass="gridheader"></HeaderStyle>
                        <RowStyle CssClass="gridrow" />
                        <AlternatingRowStyle CssClass="gridalternaterow"></AlternatingRowStyle>
                    </asp:GridView> 
                </div>
                    <br />
                    <AdHoc:LogiButton ID="btnCancelTop" runat="server" CausesValidation="False" 
                        OnClick="Cancel_OnClick" Text="<%$ Resources:LogiAdHoc, BackToReportsList %>" ToolTip="<%$ Resources:LogiAdHoc, BackToReportsListTooltip %>" />
                </contenttemplate>
                <Triggers>
                    <asp:PostBackTrigger ControlID="btnCancelTop" />
                </Triggers>
            </asp:UpdatePanel>
            <div id="divSaveAnimation">
                <asp:Button runat="server" ID="btnSaveAnimation" Style="display: none" />
                <ajaxToolkit:ModalPopupExtender runat="server" ID="ahSavePopup" BehaviorID="ahSavePopupBehavior"
                    TargetControlID="btnSaveAnimation" PopupControlID="pnlSaving" BackgroundCssClass="modalBackground"
                    DropShadow="False">
                </ajaxToolkit:ModalPopupExtender>
                <asp:Panel id="pnlSaving" CssClass="savePopup" runat="server" style="display:none;">
                    <table><tr><td>
                    <asp:Image ID="imgSave" runat="server" SkinID="imgSaving" EnableViewState="false" AlternateText="<%$ Resources:LogiAdHoc, SaveImageAltText %>"/>
                    </td>
                    <td valign="middle">
                    <asp:Label ID="lblSaveText" CssClass="lblSavePopup" runat="server" EnableViewState="false" Text="<%$ Resources:LogiAdHoc, PleaseWait %>"/>
                    </td></tr></table>
                </asp:Panel>
            </div>
        </td></tr></table>
</td></tr></table>
<br />
    </form>
</body>
</html>
