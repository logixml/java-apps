<%@ Page Language="vb" AutoEventWireup="false" Codebehind="About.aspx.vb" Inherits="LogiAdHoc.About" Culture="auto" UICulture="auto" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>About Logi AdHoc</title>
</head>
<body class="AboutBody" onkeypress="window.close()" onclick="window.close()" >
    <form name="frmVersion">
        <table width="360" border="0" cellspacing="0" cellpadding="0" height="100" align="right">
            <tr height="95">
            <td>&nbsp;</td>
            </tr>
            <tr align="Right" valign="middle">
                
                <td>
                    <table width="360" border="0" cellpadding="3" cellspacing="0">
                        <tr>
                            <td width="70%" align="right" valign="middle">
                                <b><asp:Label ID="lblAdHocVersionTitle" runat="server"></asp:Label></b>
                            </td>
                            <td width="30%" height="25" valign="middle">
                                <b><asp:Label ID="lblAdHocVersion" runat="server"></asp:Label></b>
                            </td>
                        </tr>
                        <tr>
                            <td width="70%" align="right" valign="middle">
                                <b><asp:Label ID="lblEngineVersionLabel" runat="server" Text="Engine version:" meta:resourcekey="lblEngineVersionLabelResource1"></asp:Label></b>
                            </td>
                            <td width="30%" height="25" valign="middle">
                                <b><asp:Label ID="lblEngineVersion" runat="server"></asp:Label></b>
                            </td>
                        </tr>
                        
                    </table>
                </td>
            </tr>
            <tr id="trTrialMessage" runat="server">
                <td>
                    <asp:Label ID="lblTrialNumDaysLeft" runat="server"></asp:Label>
                </td>
            </tr>
            <tr align="Right" valign="middle">
            <td><br /><br /><br /><br /><br /><br />Copyright � 2000-<%=Year(Date.Now)%> by LogiXML, Inc. &nbsp; &nbsp;</td>
            </tr>
        </table>
    </form>
</body>
</html>
