<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="ArchivedReports.aspx.vb" Inherits="LogiAdHoc.ahReport_ArchivedReports" Culture="auto" meta:resourcekey="PageResource1" UICulture="auto" %>

<%@ Register TagPrefix="AdHoc" TagName="MainMenu" Src="~/ahControls/MainMenu.ascx" %>
<%@ Register TagPrefix="AdHoc" TagName="NavMenu" Src="~/ahControls/NavMenu.ascx" %>
<%@ Register TagPrefix="AdHoc" TagName="BreadCrumbTrail" Src="~/ahControls/BreadCrumbTrail.ascx" %>
<%@ Register TagPrefix="AdHoc" TagName="Search" Src="~/ahControls/Search.ascx" %>
<%@ Register TagPrefix="AdHoc" TagName="PagingControl" Src="~/ahControls/PagingControl.ascx" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title>Archived Reports</title>
    <link rel="shortcut icon" href="../ahImages/flav.ico" />
    <script language="JavaScript" type="text/javascript" src="../ahScripts/ahChange.js"></script>
</head>
<body>
    <AdHoc:MainMenu ID="menu" runat="server" />
    
    <form id="form1" runat="server">
        <div id="submenu">
            <AdHoc:NavMenu ID="subnav" runat="server" />
        </div>
        <AdHoc:BreadCrumbTrail ID="bct" runat="server" Key="ArchivedReports" ParentLevels="0" />
        
        <asp:ScriptManager ID="ScriptManager1" runat="server" ScriptMode="Release" />

        <script type="text/javascript">
        Sys.WebForms.PageRequestManager.getInstance().add_endRequest(EndRequestHandler);
        function EndRequestHandler(sender, args) {
            $find('ahSavePopupBehavior').hide();
        }
        </script>

<table class="limiting"><tr><td>
        <table class="gridForm"><tr><td>
            <div id=NoReport runat=server>
 		        <br />
                <asp:Localize ID="Localize1" runat="server" meta:resourcekey="LiteralResource1" Text="No archived reports were found."></asp:Localize>                         		
                <br />
 		    </div>
            <asp:UpdatePanel ID="UP1" UpdateMode="Conditional" runat="server">
                <ContentTemplate>
                <div id="data_main">
                    <div id="activities">
                        <table width="100%" cellpadding="0" cellspacing="0">
                        <tr width="100%">
                        <td align="left" valign="top">
                        <AdHoc:LogiButton ID="btnRemoveArchiveTop" OnClick="RemoveArchive" 
                            CausesValidation="False" ToolTip="Click to remove selected archives." 
                            Text="Delete Archives" Runat="server" meta:resourcekey="btnRemoveArchiveTopResource1" />
                        </td>
                        <td align="right" valign="top">
                        <AdHoc:Search ID="srch" runat="server" Title="Find Archives" Width="400" meta:resourcekey="AdHocSearch" />
                        </td>
                        </tr>
                        </table>
                    </div>
                <asp:GridView ID="grdMain" runat="server" AllowPaging="True" AllowSorting="True"
                    AutoGenerateColumns="False" CssClass="grid" DataKeyNames="ReportID"
                    OnRowDataBound="OnItemDataBoundHandler" OnSorting="OnSortCommandHandler" meta:resourcekey="grdMainResource1" >
                        <HeaderStyle CssClass="gridheader"></HeaderStyle>
                    <PagerStyle HorizontalAlign="Center" />
                    <PagerTemplate>
                        <AdHoc:PagingControl ID="pageCtrl" runat="server" OnGotoNextPage="ChangePageIndex" />
                    </PagerTemplate>
                        <RowStyle CssClass="gridrow" />
                        <AlternatingRowStyle CssClass="gridalternaterow"></AlternatingRowStyle>
                    <Columns>
                        <asp:TemplateField meta:resourcekey="TemplateFieldResource1">
                            <HeaderStyle Width="30px" />
                            <HeaderTemplate>
                                <asp:Label ID="lblChkAll" runat="server" AssociatedControlID="CheckAll" Text="<%$ Resources:LogiAdHoc, SelectDeselectAll %>" CssClass="NoShow"></asp:Label>
                                <asp:CheckBox ID="CheckAll" runat="server" ToolTip="<%$ Resources:LogiAdHoc, SelectDeselectAll %>" />
                            </HeaderTemplate>
                            <ItemStyle HorizontalAlign="Center" />
                            <ItemTemplate>
                                <asp:Label ID="lblChk" runat="server" AssociatedControlID="chk_Select" Text="<%$ Resources:LogiAdHoc, SelectDeselect %>" CssClass="NoShow"></asp:Label>
                                <asp:CheckBox id="chk_Select" runat="server" ToolTip="<%$ Resources:LogiAdHoc, SelectDeselect %>"></asp:CheckBox>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="<%$ Resources:LogiAdHoc, Name %>" SortExpression="ReportName">
                            <ItemTemplate>
                                <input type="hidden" id="ReportID" runat="server" />
                                <asp:HyperLink CssClass="ReportLink" ID="ReportName" Runat="server" meta:resourcekey="ReportNameResource1" />
                                <div>
                                    <asp:Label ID="Description" Runat="server" meta:resourcekey="DescriptionResource1" /></div>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="<%$ Resources:LogiAdHoc, Owner %>" SortExpression="UserName">
                            <ItemTemplate>
                                <asp:Label ID="OwnedBy" Runat="server" meta:resourcekey="OwnedByResource1" />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField HeaderText="<%$ Resources:LogiAdHoc, UserGroup %>" DataField="GroupName" SortExpression="GroupName">
                        </asp:BoundField>
                        <asp:BoundField HeaderText="Folder" DataField="FolderName" meta:resourcekey="BoundFieldResource2" >
                        </asp:BoundField>
                        <asp:BoundField HeaderText="Last Archive" DataField="ArchivedDateTime" meta:resourcekey="BoundFieldResource3" >
                        </asp:BoundField>
                        <asp:TemplateField HeaderText="Archive Count" meta:resourcekey="TemplateFieldResource4">
                            <ItemTemplate>
                                <asp:HyperLink CssClass="ReportLink" ID="ArchivedCount" Runat="server" meta:resourcekey="ArchivedCountResource1" />
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>
                </div>
                <br />
                <div id="divOrphanedArchives" runat="server">
                <h2 id="GridHeader" runat="server">
                    <asp:Localize ID="Localize6" runat="server" meta:resourcekey="LiteralResource6" Text="Archives for Deleted Reports"></asp:Localize>
                </h2>
                <table><tr><td>
                    <div id="data_main2">
                <asp:GridView ID="grdOrphanedArchives" runat="server" AllowPaging="True" 
                    AutoGenerateColumns="False" CssClass="gridDetail" DataKeyNames="ArchivedID" meta:resourcekey="grdOrphanedArchivesResource1"
                    OnRowDataBound="OnItemDataBoundHandler1">
                    <Columns>
                        <asp:TemplateField meta:resourcekey="TemplateFieldResource1">
                            <HeaderStyle Width="30px" />
                            <HeaderTemplate>
                                <asp:Label ID="lblChkAll" runat="server" AssociatedControlID="CheckAll" Text="<%$ Resources:LogiAdHoc, SelectDeselectAll %>" CssClass="NoShow"></asp:Label>
                                <asp:CheckBox ID="CheckAll" runat="server" ToolTip="<%$ Resources:LogiAdHoc, SelectDeselectAll %>" />
                            </HeaderTemplate>
                            <ItemStyle HorizontalAlign="Center" />
                            <ItemTemplate>
                                <asp:Label ID="lblChk" runat="server" AssociatedControlID="chk_Select" Text="<%$ Resources:LogiAdHoc, SelectDeselect %>" CssClass="NoShow"></asp:Label>
                                <asp:CheckBox ID="chk_Select" runat="server" ToolTip="<%$ Resources:LogiAdHoc, SelectDeselect %>" />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="ArchivedDateTime" HeaderText="Last Archive" meta:resourcekey="BoundFieldResource3">
                        </asp:BoundField>
                        <asp:TemplateField HeaderText="Archive Count" meta:resourcekey="TemplateFieldResource4">
                            <ItemTemplate>
                                <input id="ReportID" runat="server" type="hidden" />
                                <asp:HyperLink ID="ArchivedCount" runat="server" CssClass="ReportLink" meta:resourcekey="ArchivedCountResource1">
                                </asp:HyperLink>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <PagerStyle HorizontalAlign="Center" />
                    <HeaderStyle CssClass="gridheader" />
                    <RowStyle CssClass="gridrow" />
                    <AlternatingRowStyle CssClass="gridalternaterow" />
                    <PagerTemplate>
                        <AdHoc:PagingControl ID="pageCtrl1" runat="server" OnGotoNextPage="ChangePageIndex1" />
                    </PagerTemplate>
                </asp:GridView>
                    </div>
                </td></tr></table>
<br />
                <AdHoc:LogiButton ID="btnRemoveOrphanedArchives" runat="server" CausesValidation="False"
                    meta:resourcekey="btnRemoveOrphanedArchivesResource1" OnClick="RemoveOrphanedArchive"
                    Text="Delete Archives for Deleted Reports" ToolTip="Click to remove selected archives for deleted reports." />
<br />
                </div>
                <div id="divSaveAnimation">
                    <asp:Button runat="server" ID="btnSaveAnimation" Style="display: none" />
                    <ajaxToolkit:ModalPopupExtender runat="server" ID="ahSavePopup" BehaviorID="ahSavePopupBehavior"
                        TargetControlID="btnSaveAnimation" PopupControlID="pnlSaving" BackgroundCssClass="modalBackground"
                        DropShadow="False">
                    </ajaxToolkit:ModalPopupExtender>
                    <asp:Panel id="pnlSaving" CssClass="savePopup" runat="server" style="display:none;">
                        <table><tr><td>
                        <asp:Image ID="imgSave" runat="server" SkinID="imgSaving" EnableViewState="false" AlternateText="<%$ Resources:LogiAdHoc, SaveImageAltText %>" />
                        </td>
                        <td valign="middle">
                        <asp:Label ID="lblSaveText" CssClass="lblSavePopup" runat="server" EnableViewState="false" Text="<%$ Resources:LogiAdHoc, PleaseWait %>"/>
                        </td></tr></table>
                    </asp:Panel>
                </div>
                
                </ContentTemplate>
                <Triggers>
                    <asp:AsyncPostBackTrigger ControlID="srch" EventName="DoSearch" />
                </Triggers>
            </asp:UpdatePanel>
        </td></tr></table>
</td></tr></table>
    </form>
</body>
</html>
