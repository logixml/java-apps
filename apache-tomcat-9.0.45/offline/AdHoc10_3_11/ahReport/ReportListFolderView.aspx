<%@ Page EnableTheming="true" Language="vb" AutoEventWireup="false" Codebehind="ReportListFolderView.aspx.vb"
    Inherits="LogiAdHoc.ahReport_ReportListFolderView" Culture="auto" meta:resourcekey="PageResource1"
    UICulture="auto" %>

<%@ Register TagPrefix="AdHoc" TagName="MainMenu" Src="~/ahControls/MainMenu.ascx" %>
<%@ Register TagPrefix="AdHoc" TagName="NavMenu" Src="~/ahControls/NavMenu.ascx" %>
<%@ Register TagPrefix="AdHoc" TagName="BreadCrumbTrail" Src="~/ahControls/BreadCrumbTrail.ascx" %>
<%@ Register TagPrefix="AdHoc" TagName="Search" Src="~/ahControls/Search.ascx" %>
<%@ Register TagPrefix="AdHoc" TagName="PagingControl" Src="../ahControls/PagingControl.ascx" %>
<%@ Register TagPrefix="AdHoc" TagName="SelectTemplate" Src="~/ahControls/SelectTemplateControl.ascx" %>
<%@ Register TagPrefix="AdHoc" TagName="DatabaseControl" Src="~/ahControls/DatabaseControl.ascx" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Report Management</title>
    <link rel="shortcut icon" href="../ahImages/flav.ico" />
    
    <script language="JavaScript" type="text/javascript" src="../ahScripts/ahChange.js"></script>

    <script type="text/javascript">
        function ShowSelectedObjectData() {
            //alert('Show Data');
            document.getElementById('btnShowDataPopup').click();
        }
        function SelectTemplate(id) {
            //alert("Template Selected " + id);
            var ih = document.getElementById("ihSelectedTemplate");
            var btn = document.getElementById("btnSelTmpltDblClickHidden");
            if (ih && btn)
            {
                document.body.style.cursor = 'wait';

                ih.value = id;
                btn.click();
            }
        }
    </script>
<!--[if !IE 8]>
<link rel="stylesheet" type="text/css" href="../Test.css" />
<![endif]-->
</head>
<body id="bod">
    <AdHoc:MainMenu ID="menu" runat="server" EnableViewState="false" />
    <form id="form1" runat="server">
	<div id="submenu"><AdHoc:NavMenu id="subnav" runat="server" /></div>	
        <AdHoc:BreadCrumbTrail ID="bct" runat="server" Key="ReportList" />
        <asp:ScriptManager ID="ScriptManager1" runat="server" EnablePageMethods="true" EnablePartialRendering="true" ScriptMode="Release" />

        <script type="text/javascript">
            Sys.WebForms.PageRequestManager.getInstance().add_beginRequest(BeginRequestHandler);
            Sys.WebForms.PageRequestManager.getInstance().add_endRequest(EndRequestHandler);
            function EndRequestHandler(sender, args) {
                $find('ahSavePopupBehavior').hide();
                RestorePopupPosition('ahModalPopupBehavior');
            }
            function BeginRequestHandler(sender, args) {
                SavePopupPosition('ahModalPopupBehavior');
            }
        </script>
<table class="limiting"><tr><td>
        <table class="gridForm"><tr><td>
            
            <input type="hidden" id="PrivateFolderID" runat="server" name="PrivateFolderID" EnableViewState="false" />
            <input type="hidden" id="PublicFolderID" runat="server" name="PublicFolderID" EnableViewState="false" />
            
            <Adhoc:DatabaseControl ID="ucDatabaseControl" runat="server" ShowAllDatabasesOption="True" />
            <br /><br />
            
            <table width="100%" cellspacing="0">
                <tr>
                <td align="right" valign="top">            
                    <AdHoc:LogiButton id="btnOpenWebStudioTop" Runat="server" Text="Open Web Studio" OnCommand="OpenWebStudio" 
                    CommandName="OpenWebStudio" meta:resourcekey="btnOpenWebStudioTopResource1" EnableViewState="false" ></AdHoc:LogiButton>
                </td>
                </tr>
            </table>
            <asp:UpdatePanel ID="UP1" UpdateMode="Conditional" runat="server">
                <ContentTemplate>
                    <div id="data_main">
                    <div id="activities">                    
                        <table width="100%" cellpadding="0" cellspacing="0">
                        <tr width="100%">
                        <td align="left" valign="top">
                        <span id="spBtnAdd" runat="server">
                            <AdHoc:LogiMenuBox ID="btnAdd" runat="server" Text="<%$ Resources:LogiAdHoc, Add %>" />
                        </span>
                        <asp:Panel CssClass="AddpopupMenu" ID="PopupMenu" runat="server">
                            <div><asp:LinkButton ID="btnCreateReport" runat="server" Text="<%$ Resources:LogiAdHoc, Report %>" 
                                        CommandName="CreateReport" OnCommand="DoActions"/></div>
                            <div><asp:LinkButton ID="btnCreateDashboard" runat="server" Text="<%$ Resources:LogiAdHoc, Dashboard %>" 
                                        CommandName="CreateDashboard" OnCommand="DoActions" /></div>
                            <div><asp:LinkButton ID="btnCreateFolder" runat="server" Text="<%$ Resources:LogiAdHoc, Folder %>" 
                                        CommandName="CreateFolder" OnCommand="DoActions" /></div>
                        </asp:Panel>
                        <ajaxToolkit:HoverMenuExtender ID="hme" runat="Server"
                            PopupControlID="PopupMenu"
                            PopupPosition="Bottom" 
                            TargetControlID="spBtnAdd"
                            PopDelay="25" />
                        
                        <asp:Button ID="btnDelete" runat="server" EnableViewState="false" CommandName="RemoveItems" OnCommand="RemoveItems" CssClass="NoShow" />
                        <AdHoc:LogiButton ID="btnRemoveItems" runat="server" EnableViewState="false" 
                            Text="<%$ Resources:LogiAdHoc, Delete %>" CommandName="RemoveItems" OnCommand="DoActions" />
                        <AdHoc:LogiButton ID="btnCopyReports" runat="server" EnableViewState="false" 
                            Text="<%$ Resources:LogiAdHoc, Copy %>" CommandName="CopyReports" OnCommand="DoActions" />
                        <AdHoc:LogiButton ID="btnMoveReports" runat="server" EnableViewState="false" 
                            Text="<%$ Resources:LogiAdHoc, Move %>" CommandName="MoveReports" OnCommand="DoActions" />
                        </td>

                        <td align="right" valign="top">
                            <AdHoc:Search ID="srch" runat="server" Title="Find Reports" meta:resourcekey="AdHocSearch" />
                        </td></tr></table>
                    </div>
                    <ul runat="server" EnableViewState="false" id="ErrorListTop" class="errorWithImage">
                        <li>
                            <asp:Label runat="server" EnableViewState="false" ID="lblValidationMessageTop" meta:resourcekey="lblValidationMessageTopResource1"></asp:Label>
                        </li>
                    </ul>
                    <asp:GridView runat="server" ID="grdMain" AllowSorting="True" CssClass="grid" AutoGenerateColumns="False"
                        AllowPaging="True" DataKeyNames="ID" meta:resourcekey="grdMainResource1" OnSorting="OnSortCommandHandler">
                        <HeaderStyle CssClass="gridheader"></HeaderStyle>
                        <PagerTemplate>
                            <AdHoc:PagingControl ID="pageCtrl" runat="server" OnGotoNextPage="ChangePageIndex" />
                        </PagerTemplate>
                        
                        <PagerStyle HorizontalAlign="Center"></PagerStyle>
                        
                        <RowStyle CssClass="gridrow" />
                        <AlternatingRowStyle CssClass="gridalternaterow"></AlternatingRowStyle>
                        <Columns>
                            <asp:TemplateField>
                                <HeaderStyle Width="30px"></HeaderStyle>
                                <HeaderTemplate>
                                    <asp:Label ID="lblChkAll" runat="server" AssociatedControlID="CheckAll" Text="<%$ Resources:LogiAdHoc, SelectDeselectAll %>" CssClass="NoShow"></asp:Label>
                                    <asp:CheckBox runat="server" ToolTip="<%$ Resources:LogiAdHoc, SelectDeselectAll %>" ID="CheckAll" />
                                </HeaderTemplate>
                                <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblChk" runat="server" AssociatedControlID="chk_Select" Text="<%$ Resources:LogiAdHoc, SelectDeselect %>" CssClass="NoShow"></asp:Label>
                                    <asp:CheckBox runat="server" ID="chk_Select" ToolTip="<%$ Resources:LogiAdHoc, SelectDeselect %>" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField SortExpression="ObjectTypeID,ObjectName" HeaderText="<%$ Resources:LogiAdHoc, Name %>">
                                <ItemTemplate>
                                    <input runat="server" id="MainID" type="hidden"/>
                                    <input runat="server" id="ObjectTypeID" type="hidden"/>
                                    <input runat="server" id="FolderID" type="hidden"/>
                                    <input runat="server" id="ReportID" type="hidden"/>
                                    <%--<input runat="server" id="TaskName" type="hidden"/>--%>
                                    <input runat="server" id="PhysicalName" type="hidden"/>
                                    <%--<input runat="server" id="DoArchive" type="hidden"/>--%>
                                    <input runat="server" id="ArchiveExists" type="hidden"/>
                                    <input runat="server" id="AllowDelete" type="hidden"/>
                                    <input runat="server" id="AllowCopy" type="hidden"/>
                                    <input runat="server" id="AllowMove" type="hidden"/>
                                    <asp:Image runat="server" ID="ObjectType" meta:resourcekey="ObjectTypeResource1"></asp:Image>
                                    <asp:Label runat="server" CssClass="broken" ID="BrokenReport" meta:resourcekey="BrokenReportResource1"></asp:Label>
                                    <asp:HyperLink runat="server" CssClass="ReportLink" ID="ObjectName" meta:resourcekey="ObjectNameResource1"></asp:HyperLink>
                                    <asp:LinkButton runat="server" ID="FolderLink" meta:resourcekey="FolderLinkResource1"
                                        OnCommand="DoModifications"></asp:LinkButton>
                                    <div>
                                        <asp:Label runat="server" ID="Description" meta:resourcekey="DescriptionResource1"></asp:Label>
                                    </div>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField SortExpression="ObjectTypeID,UserName" HeaderText="<%$ Resources:LogiAdHoc, Owner %>">
                                <ItemTemplate>
                                    <asp:Label runat="server" ID="OwnedBy" meta:resourcekey="OwnedByResource1"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField SortExpression="ObjectTypeID,GroupName" HeaderText="<%$ Resources:LogiAdHoc, Group %>">
                                <ItemTemplate>
                                    <asp:Label runat="server" ID="GroupName" meta:resourcekey="GroupNameResource1"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField SortExpression="ObjectTypeID,FolderType" HeaderText="<%$ Resources:LogiAdHoc, FolderType %>">
                                <ItemTemplate>
                                    <asp:Label runat="server" ID="lblFolderType"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField SortExpression="ObjectTypeID,LastModified" HeaderText="Last Modified"
                                meta:resourcekey="TemplateFieldResource5">
                                <ItemTemplate>
                                    <asp:Label runat="server" ID="Modified" meta:resourcekey="ModifiedResource1"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="<%$ Resources:LogiAdHoc, Actions %>">
                                <HeaderStyle Width="50px" />
                                <ItemStyle HorizontalAlign="center" />
                                <ItemTemplate>
                                    <%--<asp:Image ID="imgActions" AlternateText="Actions" runat="server" ToolTip="Actions"
                                        ImageUrl="~/ahImages/arrowStep.gif" SkinID="imgActions" />--%>
                                    <asp:Image ID="imgActions" AlternateText="<%$ Resources:LogiAdHoc, Actions %>" runat="server" ToolTip="<%$ Resources:LogiAdHoc, Actions %>"
                                         SkinID="imgActions" />
                                    <%--<asp:ImageButton ID="Modify" SkinID="imgSingleAction" AlternateText="<%$ Resources:LogiAdHoc, ModifyFolder %>" ToolTip="<%$ Resources:LogiAdHoc, ModifyFolder %>"
                                        runat="server" OnCommand="DoModifications" CausesValidation="false" meta:resourcekey="ModifyResource1" />--%>
                                        
                                    <asp:Panel ID="pnlActionsMenu" runat="server" CssClass="popupMenu" 
                                        HorizontalAlign="Left" Wrap="false" style="display:none;">
                                        <div id="divModify" runat="server" class="hoverMenuActionLink" >
                                            <asp:LinkButton ID="lnkModify" runat="server" OnCommand="DoModifications" 
                                                Text="Modify" meta:resourcekey="lnkModifyResource1"></asp:LinkButton>
                                        </div>
                                        <div id="divWSModify" runat="server" class="hoverMenuActionLink">
                                            <asp:LinkButton ID="lnkWSModify" runat="server" OnCommand="DoModifications" 
                                                Text="Modify in Web Studio" meta:resourcekey="lnkWSModifyResource1"></asp:LinkButton>
                                        </div>
                                        <div id="divRename" runat="server" class="hoverMenuActionLink">
                                            <asp:LinkButton ID="lnkRename" runat="server" OnCommand="DoModifications" 
                                                Text="Rename" meta:resourcekey="lnkRenameResource1"></asp:LinkButton>
                                        </div>
                                        <div id="divCopy" runat="server" class="hoverMenuActionLink">
                                            <asp:LinkButton ID="lnkCopy" runat="server" OnCommand="DoModifications" 
                                                Text="Copy" meta:resourcekey="lnkCopyResource1"></asp:LinkButton>
                                        </div>
                                        <div id="divMove" runat="server" class="hoverMenuActionLink">
                                            <asp:LinkButton ID="lnkMove" runat="server" OnCommand="DoModifications" 
                                                Text="Move" meta:resourcekey="lnkMoveResource1"></asp:LinkButton>
                                        </div>
                                        <div id="divSchedule" runat="server" class="hoverMenuActionLink">
                                            <asp:LinkButton ID="lnkSchedule" runat="server" OnCommand="DoModifications" 
                                                Text="Schedule" meta:resourcekey="lnkScheduleResource1"></asp:LinkButton>
                                        </div>
                                        <div id="divArchive" runat="server" class="hoverMenuActionLink">
                                            <asp:LinkButton ID="lnkArchive" runat="server" OnCommand="DoModifications" 
                                                Text="View Archives" meta:resourcekey="lnkArchiveResource1"></asp:LinkButton>
                                        </div>
                                        <div id="divDependencies" runat="server" class="hoverMenuActionLink">
                                            <asp:LinkButton ID="lnkDependencies" runat="server" OnCommand="DoModifications" 
                                                Text="<%$ Resources:LogiAdHoc, ViewDependencyInfo %>"></asp:LinkButton>
                                        </div>
                                        <div id="divDelete" runat="server" class="hoverMenuActionLink">
                                            <asp:LinkButton ID="lnkDelete" runat="server" OnCommand="DoModifications"
                                                Text="Delete" meta:resourcekey="DeleteResource1"></asp:LinkButton>
                                        </div>
                                    </asp:Panel>
                                    
                                    <ajaxToolkit:HoverMenuExtender ID="hme2" runat="server"
                                        PopupControlID="pnlActionsMenu" PopupPosition="Right" 
                                        TargetControlID="imgActions" PopDelay="25" />
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                    <div id="pnlHelp" runat="server" class="hlp" style="width:500px;"></div>
                </div>
                    <p runat="server" id="NoReports" class="info">
                        <asp:Localize ID="locNoReports" runat="server" EnableViewState="false" Text="<%$ Resources:Errors, Err_NoReports %>"></asp:Localize>                        
                    </p>
                    
                    <asp:Button runat="server" ID="Button1" Style="display: none" meta:resourcekey="Button1Resource1" />
                    <ajaxToolkit:ModalPopupExtender runat="server" ID="ahModalPopup" BehaviorID="ahModalPopupBehavior"
                        TargetControlID="Button1" PopupControlID="ahPopup" BackgroundCssClass="modalBackground" 
                        DropShadow="False" PopupDragHandleControlID="pnlDragHandle" RepositionMode="None">
                    </ajaxToolkit:ModalPopupExtender>
                    
                    <asp:Panel runat="server" CssClass="modalPopup" ID="ahPopup" Style="display: none; width: 670;" meta:resourcekey="ahPopupResource1">
                        <asp:Panel ID="pnlDragHandle" runat="server">
                        <div class="modalPopupHandle">
                            <table cellpadding="0" cellspacing="0" style="width: 100%"><tr><td>
                                    <asp:Localize ID="PopupHeader" runat="server" Text="Report Templates" meta:resourcekey="PopupHeaderResource1"></asp:Localize>
                                </td>
                                <td style="width: 20px;">
                                    <asp:ImageButton ID="imgClosePopup" runat="server" 
                                        OnClick="imgClosePopup_Click" CausesValidation="false"
                                        SkinID="imgbClose" ImageUrl="../ahImages/remove.gif" 
                                        AlternateText="<%$ Resources:LogiAdHoc, CloseWindow %>"/>
                            </td></tr></table>
                        </div>
                        </asp:Panel>
                        <div class="modalDiv">
                        <asp:UpdatePanel ID="upPopup" runat="server" UpdateMode="Conditional">
                            <ContentTemplate>
                                <input type="hidden" id="ihSelectedTemplate" runat="server" />
                                <AdHoc:SelectTemplate ID="ucSelectTemplate" runat="server" />
                                
                                <!-- Buttons -->
                                <table>
                                    <tr>
                                        <td colspan="2">
                                            <asp:Button ID="btnSelTmpltDblClickHidden" runat="server" OnClick="btnSelTmpltDblClickHidden_OnClick" 
                                                CausesValidation="false" CssClass="NoShow" UseSubmitBehavior="False" />
                                            <AdHoc:LogiButton ID="btnSelectTemplateOK" OnClick="btnSelectTemplateOK_OnClick" runat="server"
                                                Text="<%$ Resources:LogiAdHoc, OKWithSpaces %>" UseSubmitBehavior="False" CausesValidation="False" meta:resourcekey="btnSelectTemplateOKResource1" />
                                            <AdHoc:LogiButton ID="btnSelectTemplateCancel" OnClick="btnSelectTemplateCancel_OnClick"
                                                runat="server" Text="<%$ Resources:LogiAdHoc, Cancel %>" CausesValidation="False" meta:resourcekey="btnSelectTemplateCancelResource1" />
                                        </td>
                                    </tr>
                                </table>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                        </div>
                    </asp:Panel>
                </ContentTemplate>
                <Triggers>
                    <asp:AsyncPostBackTrigger ControlID="srch" EventName="DoSearch" />
                </Triggers>
            </asp:UpdatePanel>
            <div id="divSaveAnimation">
                <asp:Button runat="server" ID="btnSaveAnimation" Style="display: none" />
                <ajaxToolkit:ModalPopupExtender runat="server" ID="ahSavePopup" BehaviorID="ahSavePopupBehavior"
                    TargetControlID="btnSaveAnimation" PopupControlID="pnlSaving" BackgroundCssClass="modalBackground"
                    DropShadow="False">
                </ajaxToolkit:ModalPopupExtender>
                <asp:Panel id="pnlSaving" CssClass="savePopup" runat="server" style="display:none;">
                    <table><tr><td>
                    <asp:Image ID="imgSave" runat="server" SkinID="imgSaving" EnableViewState="false" AlternateText="<%$ Resources:LogiAdHoc, SaveImageAltText %>"/>
                    </td>
                    <td valign="middle">
                    <asp:Label ID="lblSaveText" CssClass="lblSavePopup" runat="server" EnableViewState="false" Text="<%$ Resources:LogiAdHoc, PleaseWait %>"/>
                    </td></tr></table>
                </asp:Panel>
            </div>
        </td></tr></table>
</td></tr></table>
<br /><br /><br /><br /><br /><br />
    </form>
</body>
</html>
