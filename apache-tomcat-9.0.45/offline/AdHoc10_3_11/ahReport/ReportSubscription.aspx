<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="ReportSubscription.aspx.vb" Inherits="LogiAdHoc.ahReport_ReportSubscription" Culture="auto" meta:resourcekey="PageResource1" UICulture="auto" %>

<%@ Register Src="../ahControls/PagingControl.ascx" TagName="PagingControl" TagPrefix="uc1" %>

<%@ Register TagPrefix="AdHoc" TagName="MainMenu" Src="~/ahControls/MainMenu.ascx" %>
<%@ Register tagprefix="AdHoc" tagname="NavMenu" Src="~/ahControls/NavMenu.ascx" %>
<%@ Register TagPrefix="AdHoc" TagName="BreadCrumbTrail" Src="~/ahControls/BreadCrumbTrail.ascx" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Subscribe to Report</title>
    <link rel="shortcut icon" href="../ahImages/flav.ico" />
    <script language="JavaScript" type="text/javascript" src="../ahScripts/ahChange.js"></script>
    <script type="text/javascript">
        window.onbeforeunload = AlertOnExit;
    </script>
</head>
<body id="bod">
    <AdHoc:MainMenu ID="menu" runat="server" />
    
    <form id="form1" runat="server">
	<div id="submenu"><AdHoc:NavMenu id="subnav" runat="server" /></div>
        <AdHoc:BreadCrumbTrail ID="bct" runat="server" Key="SubscribeToReport" />
        
        <asp:ScriptManager ID="ScriptManager1" EnablePartialRendering="true" runat="server"  />
        <script type="text/javascript">
            Sys.WebForms.PageRequestManager.getInstance().add_endRequest(EndRequestHandler);
            function EndRequestHandler(sender, args) {
                noMessage=false;
                $find('ahSavePopupBehavior').hide();
            }
        </script> 
        
        <div class="divForm">
        <input type="hidden" id="ahDirty" name="ahDirty" runat="server" />
        <input id="UserID" type="hidden" runat="server" name="UserID">
        <input type="hidden" id="PhysicalName" name="PhysicalName" runat="server" />
        
            <div id="descriptionDiv" runat="server">
                <table>
                    <tr>
                        <td width="130px">
                            <asp:Localize ID="Localize1" runat="server" Text="<%$ Resources:LogiAdHoc, ReportName %>"></asp:Localize>:
                        </td>
                        <td>
                            <asp:Label ID="reportname" runat="server" meta:resourcekey="reportnameResource1" /></td>
                    </tr>
                    <tr>
                        <td width="130px">
                            <asp:Localize ID="Localize2" runat="server" Text="<%$ Resources:LogiAdHoc, Frequency %>"></asp:Localize>:
                        </td>
                        <td>
                            <asp:Label ID="Frequency" runat="server" meta:resourcekey="FrequencyResource1" /></td>
                    </tr>
                    <tr>
                        <td width="130px">
                            <asp:Localize ID="Localize3" runat="server" Text="<%$ Resources:LogiAdHoc, Schedule %>"></asp:Localize>:
                        </td>
                        <td>
                            <asp:Label ID="Schedule" runat="server" meta:resourcekey="ScheduleResource1" /></td>
                    </tr>
                    <tr>
                        <td width="130px">
                            <asp:Localize ID="Localize4" runat="server" Text="Last Run:" meta:resourcekey="LastRun"></asp:Localize>
                        </td>
                        <td>
                            <asp:Label ID="LastRun" runat="server" meta:resourcekey="LastRunResource1" />
                        </td>
                    </tr>
                    <tr>
                        <td width="130px">
                            <asp:Localize ID="Localize5" runat="server" Text="Next Run:" meta:resourcekey="NextRun"></asp:Localize>
                        </td>
                        <td>
                            <asp:Label ID="NextRun" runat="server" meta:resourcekey="NextRunResource1" /></td>
                    </tr>
                    <tr id="trEmail" runat="server">
                        <td width="130px">
                            <asp:Label ID="Label6" runat="server" Text="Subscriber's Email Address:" AssociatedControlID="txtEmail" meta:resourcekey="LocalizeResource6"></asp:Label>
                        </td>
                        <td>
                            <asp:Label ID="email" runat="server" meta:resourcekey="emailResource1" /><asp:TextBox ID="txtEmail" runat="server" CssClass="hidden" meta:resourcekey="txtEmailResource1" />
                            <asp:RequiredFieldValidator ID="rtvEmail" ControlToValidate="txtEmail" ErrorMessage="Email is required. Please enter it in Profile screen."
                                runat="server" EnableClientScript="False" meta:resourcekey="rtvEmailResource1">*</asp:RequiredFieldValidator>
                        </td>
                    </tr>
                </table>
            </div>
            <div id="divSingleUserSubscription" runat="server">
                <AdHoc:LogiButton ID="btnSubscribe" Text="Subscribe" OnClick="Subscribe_OnClick"
                    ToolTip="Click to subscribe to the report and return to the Schedules list." runat="server" meta:resourcekey="TopSubscribeResource1" />
                <AdHoc:LogiButton ID="btnUnsubscribe" Text="Unsubscribe" OnClick="Unsubscribe_OnClick"
                    ToolTip="Click to unsubscribe from the report and return to the Schedules list."
                    runat="server" meta:resourcekey="TopUnsubscribeResource1" />
                <AdHoc:LogiButton ID="btnCancel" Text="Back to Schedules" CausesValidation="False"
                    OnClick="Cancel_OnClick" ToolTip="Click to return to the Schedules list."
                    runat="server" meta:resourcekey="BackToSchedulesResource1"  />
            </div>
            			
		<div id="divSubscribedUsers" runat=server>
			<h2>
			<asp:Localize ID="Localize8" runat="server" Text="Subscribe / Unsubscribe Users" meta:resourcekey="LocalizeResource8"></asp:Localize>
			</h2>

            <div id="errorDiv" runat="server" style="color: red">
                <br>
                <asp:Localize ID="Localize7" runat="server" Text="* This schedule has been removed manually and is no longer available. It will be
                cleaned-up once you click the OK button." meta:resourcekey="LocalizeResource7"></asp:Localize>                
            </div>
<table class="limiting"><tr><td>
        <table class="gridForm"><tr><td>
        <div id="data_main">
            <div id="activities">
                <table width="100%" cellpadding="0" cellspacing="0">
                <tr width="100%">
                <td align="left" valign="top">
                <AdHoc:LogiButton ID="Delete" Text="OK" OnClick="Delete_OnClick" ToolTip="Click to delete the orphaned schedule and return to the schedule list."
                    runat="server" meta:resourcekey="DeleteResource1" />
                <AdHoc:LogiButton ID="TopSubscribe" Text="Subscribe" OnClick="Subscribe_OnClick"
                    ToolTip="Click to subscribe to the report and return to the Schedules list." runat="server" meta:resourcekey="TopSubscribeResource1" />
                <AdHoc:LogiButton ID="TopUnsubscribe" Text="Unsubscribe" OnClick="Unsubscribe_OnClick"
                    ToolTip="Click to unsubscribe from the report and return to the Schedules list."
                    runat="server" meta:resourcekey="TopUnsubscribeResource1" />
                </td>
                </tr>
                </table>
            </div>
            <asp:UpdatePanel ID="UP1" UpdateMode="Conditional" runat="server">
                <ContentTemplate>
            <asp:GridView ID="grdMain" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                CssClass="grid" OnRowDataBound="OnItemDataBoundHandler" meta:resourcekey="grdMainResource1">
                <Columns>
                    <asp:TemplateField>
                        <HeaderStyle Width="36px"></HeaderStyle>
                        <HeaderTemplate>
                            <asp:Label ID="lblChkAll" runat="server" AssociatedControlID="CheckAll" Text="<%$ Resources:LogiAdHoc, SelectDeselectAll %>" CssClass="NoShow"></asp:Label>
                            <asp:CheckBox runat="server" ToolTip="<%$ Resources:LogiAdHoc, SelectDeselectAll %>" ID="CheckAll" />
                        </HeaderTemplate>
                        <ItemStyle HorizontalAlign="Center" Width="44px"></ItemStyle>
                        <ItemTemplate>
                            <asp:Label ID="lblChk" runat="server" AssociatedControlID="chk_Select" Text="<%$ Resources:LogiAdHoc, SelectDeselect %>" CssClass="NoShow"></asp:Label>
                            <asp:CheckBox runat="server" ID="chk_Select" ToolTip="<%$ Resources:LogiAdHoc, SelectDeselect %>" />
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="User" meta:resourcekey="TemplateFieldResource2">
                        <ItemTemplate>
                            <input type="hidden" id="UserID" runat="server"/>
                            <asp:Label ID="UserName" Runat="server" meta:resourcekey="UserNameResource1" />
                        </ItemTemplate>
                        <HeaderStyle Width="200px" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Email" meta:resourcekey="TemplateFieldResource3">
                        <ItemTemplate>
                            <asp:Label ID="Email" Runat="server" meta:resourcekey="EmailResource2" />
                        </ItemTemplate>
                        <HeaderStyle Width="200px" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Is Subscribed" meta:resourcekey="TemplateFieldResource4">
                        <ItemTemplate>
                            <asp:Label ID="IsSubscribed" Runat="server" meta:resourcekey="IsSubscribedResource1" />
                        </ItemTemplate>
                        <HeaderStyle Width="50px" />
                    </asp:TemplateField>
                </Columns>
                <PagerTemplate>
                    <uc1:PagingControl ID="pageCtrl" runat="server" OnGotoNextPage="ChangePageIndex" />
                </PagerTemplate>
                <PagerStyle HorizontalAlign="Center" />
                <HeaderStyle CssClass="gridheader" />
                <RowStyle CssClass="gridrow" />
                <AlternatingRowStyle CssClass="gridalternaterow" />
            </asp:GridView>
            </ContentTemplate>
            </asp:UpdatePanel>
		</div>
        </td></tr></table>
</td></tr></table>
            <br />
                <AdHoc:LogiButton ID="TopCancel" Text="Back to Schedules" CausesValidation="False"
                    OnClick="Cancel_OnClick" ToolTip="Click to return to the Schedules list."
                    runat="server" meta:resourcekey="BackToSchedulesResource1"  />
            <br />
		</div>
        <ul class="validation_error" id="ErrorList" runat="server">
            <li>
                <asp:Label ID="lblValidationMessage" runat="server" meta:resourcekey="lblValidationMessageResource1"></asp:Label></li>
        </ul>

        <asp:ValidationSummary ID="vsummary" runat="server" meta:resourcekey="vsummaryResource1" />
        <div id="divSaveAnimation">
            <asp:Button runat="server" ID="btnSaveAnimation" Style="display: none" />
            <ajaxToolkit:ModalPopupExtender runat="server" ID="ahSavePopup" BehaviorID="ahSavePopupBehavior"
                TargetControlID="btnSaveAnimation" PopupControlID="pnlSaving" BackgroundCssClass="modalBackground"
                DropShadow="False">
            </ajaxToolkit:ModalPopupExtender>
            <asp:Panel id="pnlSaving" CssClass="savePopup" runat="server" style="display:none;">
                <table><tr><td>
                <asp:Image ID="imgSave" runat="server" SkinID="imgSaving" EnableViewState="false" AlternateText="<%$ Resources:LogiAdHoc, SaveImageAltText %>"/>
                </td>
                <td valign="middle">
                <asp:Label ID="lblSaveText" CssClass="lblSavePopup" runat="server" EnableViewState="false" Text="<%$ Resources:LogiAdHoc, PleaseWait %>"/>
                </td></tr></table>
            </asp:Panel>
        </div>
        </div>
    </form>
</body>
</html>
