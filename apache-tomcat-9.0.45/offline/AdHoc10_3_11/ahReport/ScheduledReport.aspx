<%@ Page AutoEventWireup="false" Codebehind="ScheduledReport.aspx.vb" Culture="auto"
    Inherits="LogiAdHoc.ahReport_ScheduledReport" Language="vb" meta:resourcekey="PageResource1"
    UICulture="auto" %>

<%@ Register TagPrefix="AdHoc" TagName="MainMenu" Src="~/ahControls/MainMenu.ascx" %>
<%@ Register TagPrefix="AdHoc" TagName="NavMenu" Src="~/ahControls/NavMenu.ascx" %>
<%@ Register TagPrefix="AdHoc" TagName="BreadCrumbTrail" Src="~/ahControls/BreadCrumbTrail.ascx" %>
<%@ Register tagprefix="wizard" tagname="datebox" src="../ahControls/DateBox.ascx" %>
<%@ Register TagPrefix="wizard" TagName="SpecialValue" Src="../ahControls/SpecialValue.ascx" %>
<%@ Register TagPrefix="wizard" TagName="DatabaseValues" Src="../ahControls/DatabaseValues.ascx" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN">

<html xmlns="http://www.w3.org/1999/xhtml">

<head id="Head1" runat="server">
    <title>Schedule Report</title>
    <link rel="shortcut icon" href="../ahImages/flav.ico" />
    <script language="JavaScript" type="text/javascript" src="../ahScripts/ahChange.js">
    </script>
    <script type="text/javascript">
        window.onbeforeunload = AlertOnExit;
    </script>
</head>

<body id="bod">
    <AdHoc:MainMenu ID="menu" runat="server" />

    <form id="form1" runat="server">
        <div id="submenu">
            <AdHoc:NavMenu id="subnav" runat="server" />
        </div>
        <AdHoc:BreadCrumbTrail ID="bct" runat="server" Key="ScheduledReport" />

        <asp:ScriptManager ID="ScriptManager1" runat="server" ScriptMode="Release" />

        <script type="text/javascript">
            Sys.WebForms.PageRequestManager.getInstance().add_beginRequest(BeginRequestHandler);
            Sys.WebForms.PageRequestManager.getInstance().add_endRequest(EndRequestHandler);
            function EndRequestHandler(sender, args) {
                //            if (args.get_error() != undefined && args.get_error().httpStatusCode == '500') {
                //                var errorMessage = args.get_error().message;
                //                args.set_errorHandled(true);
                //            }
                noMessage = false;
                $find('ahSavePopupBehavior').hide();
                RestorePopupPosition('ahModalPopupBehavior');
                RestorePopupPosition('mpeSaveAsBehavior');
            }
            function BeginRequestHandler(sender, args) {
                SavePopupPosition('ahModalPopupBehavior');
                SavePopupPosition('mpeSaveAsBehavior');
            }
        </script>

        <div class="divForm">
            <input type="hidden" id="ahDirty" name="ahDirty" runat="server" />
            <input type="hidden" id="PhysicalName" name="PhysicalName" runat="server" />
            <h2>
                <asp:Localize ID="Localize11" runat="server" meta:resourcekey="LiteralResource11"
                    Text="Task Scheduling"></asp:Localize>
            </h2>

            <asp:UpdatePanel ID="UP1" UpdateMode="Conditional" runat="server">
                <ContentTemplate>
                    <table class="tbTB">
                        <tr>
                            <td width="140px">
                                <asp:Localize ID="Localize1" runat="server"
                                    Text="<%$ Resources:LogiAdHoc, ReportName %>"></asp:Localize>:
                            </td>
                            <td>
                                <asp:Label ID="reportname" Runat="server" meta:resourcekey="reportnameResource1" />
                            </td>
                        </tr>
                        <tr id="rowTaskName" Runat="server">
                            <td width="140px" runat="server">
                                <asp:Localize ID="Localize2" runat="server" meta:resourcekey="LiteralResource2"
                                    Text="Task Name:"></asp:Localize>
                            </td>
                            <td runat="server">
                                <asp:Label ID="taskNameEdit" Runat="server" />
                            </td>
                        </tr>
                    </table>

                    <div id="TaskInfo" runat="server">
                        <table class="tbTB">
                            <tr>
                                <td width="140px">
                                    <asp:Label ID="Label5" runat="server" AssociatedControlID="OutputFormat"
                                        meta:resourcekey="LiteralResource3" Text="Output Format:"></asp:Label>
                                </td>
                                <td>
                                    <asp:DropDownList ID="OutputFormat" runat="server" AutoPostBack="True"
                                        meta:resourcekey="OutputFormatResource1">
                                        <asp:ListItem Value="0" meta:resourcekey="ListItemResource1">HTML</asp:ListItem>
                                        <asp:ListItem Value="1" meta:resourcekey="ListItemResource2">PDF</asp:ListItem>
                                        <asp:ListItem Value="2" meta:resourcekey="ListItemResource3">EXCEL
                                        </asp:ListItem>
                                        <asp:ListItem Value="3" meta:resourcekey="ListItemResource4">WORD</asp:ListItem>
                                        <asp:ListItem Value="4" meta:resourcekey="ListItemResource5">CSV</asp:ListItem>
                                    </asp:DropDownList>
                                </td>
                            </tr>
                            <tr id="rowArchival" runat=server>
                                <td width="150px" runat="server">
                                    <asp:Label ID="Label4" runat="server" AssociatedControlID="Archive"
                                        Text="<%$ Resources:LogiAdHoc, AddToArchive %>"></asp:Label>:
                                </td>
                                <td runat="server">
                                    <asp:CheckBox ID="Archive" Runat="server" AutoPostBack=True />
                                </td>
                            </tr>
                            <tr id="rowAttachmentFileName" runat="server">
                                <td runat="server" width="140px">
                                    <asp:Label ID="Label21" runat="server" meta:resourcekey="LiteralResource21"
                                        Text="Attachment file name:" AssociatedControlID="txtAttachFilename">
                                    </asp:Label>
                                </td>
                                <td runat="server">
                                    <asp:TextBox ID="txtAttachFilename" runat="server" size="50" MaxLength="200">
                                    </asp:TextBox>
                                    <asp:CustomValidator ID="cvValidAttachFilename" runat="Server"
                                        ControlToValidate="txtAttachFilename"
                                        ErrorMessage="<%$ Resources:Errors, Err_AttachFilenameInvalidChars %>"
                                        OnServerValidate="IsAttachFilenameValid" ValidationGroup="Schedule">*
                                    </asp:CustomValidator>
                                </td>
                            </tr>
                        </table>

                        <p id="rowInfo" runat=server class="info">
                            <asp:Label ID="lblArchiveInfo" runat="server" meta:resourcekey="lblArchiveInfoResource1" />
                        </p>

                        <table cellpadding="5">
                            <tr>
                                <td valign="top" width="340">
                                    <h2>
                                        <asp:Localize ID="Localize5" runat="server" meta:resourcekey="LiteralResource5"
                                            Text="Scheduling Information"></asp:Localize>
                                    </h2>
                                    <DIV id="rangeGroup">
                                        <table class="tbTB">
                                            <tr>
                                                <td width="140px">
                                                    <asp:Label ID="Label6" runat="server"
                                                        meta:resourcekey="LiteralResource6"
                                                        AssociatedControlID="recurrenceCombo" Text="Schedule Task:">
                                                    </asp:Label>
                                                </td>
                                                <td>
                                                    <asp:DropdownList ID="recurrenceCombo" Runat="server"
                                                        AutoPostBack="True" meta:resourcekey="recurrenceComboResource1">
                                                        <asp:ListItem Value="0" meta:resourcekey="ListItemResource6"
                                                            Text="Once"></asp:ListItem>
                                                        <asp:ListItem Value="1"
                                                            Text="<%$ Resources:LogiAdHoc, Daily %>"></asp:ListItem>
                                                        <asp:ListItem Value="2"
                                                            Text="<%$ Resources:LogiAdHoc, Weekly %>"></asp:ListItem>
                                                        <asp:ListItem Value="3"
                                                            Text="<%$ Resources:LogiAdHoc, Monthly %>"></asp:ListItem>
                                                    </asp:DropDownList>
                                                </td>
                                            </tr>
                                            <tr id="rowStartTime" runat=server>
                                                <td width=140px runat="server">
                                                    <asp:Label ID="Label11" runat="server"
                                                        AssociatedControlID="ddlStartTimeHour"
                                                        meta:resourcekey="LiteralResource10" Text="Start Time:">
                                                    </asp:Label>
                                                </td>
                                                <td runat="server">
                                                    <%--<asp:TextBox id="startTime" runat="server" Width="88px"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="reqStartTime" runat="server" ErrorMessage="Start time is required."
                                                ControlToValidate="startTime" ValidationGroup="Schedule">*</asp:RequiredFieldValidator>
                                            <asp:RegularExpressionValidator ID="regStartTime" runat="server" ErrorMessage="Start time must be in HH:MM:SS format." ValidationGroup="Schedule"
                                                ControlToValidate="startTime" ValidationExpression="(\b(0[0-9])|\b(1[0-9])|\b(2[0-3])):[0-5][0-9]:[0-5][0-9]\b">*</asp:RegularExpressionValidator>--%>

                                                    <asp:DropDownList ID="ddlStartTimeHour" runat="server"
                                                        meta:resourcekey="ddlStartTimeHourResource1">
                                                        <asp:ListItem Text="00" Value="00"></asp:ListItem>
                                                        <asp:ListItem Text="01" Value="01"></asp:ListItem>
                                                        <asp:ListItem Text="02" Value="02"></asp:ListItem>
                                                        <asp:ListItem Text="03" Value="03"></asp:ListItem>
                                                        <asp:ListItem Text="04" Value="04"></asp:ListItem>
                                                        <asp:ListItem Text="05" Value="05"></asp:ListItem>
                                                        <asp:ListItem Text="06" Value="06"></asp:ListItem>
                                                        <asp:ListItem Text="07" Value="07"></asp:ListItem>
                                                        <asp:ListItem Text="08" Value="08"></asp:ListItem>
                                                        <asp:ListItem Text="09" Value="09"></asp:ListItem>
                                                        <asp:ListItem Text="10" Value="10"></asp:ListItem>
                                                        <asp:ListItem Text="11" Value="11"></asp:ListItem>
                                                        <asp:ListItem Text="12" Value="12"></asp:ListItem>
                                                        <asp:ListItem Text="13" Value="13"></asp:ListItem>
                                                        <asp:ListItem Text="14" Value="14"></asp:ListItem>
                                                        <asp:ListItem Text="15" Value="15"></asp:ListItem>
                                                        <asp:ListItem Text="16" Value="16"></asp:ListItem>
                                                        <asp:ListItem Text="17" Value="17"></asp:ListItem>
                                                        <asp:ListItem Text="18" Value="18"></asp:ListItem>
                                                        <asp:ListItem Text="19" Value="19"></asp:ListItem>
                                                        <asp:ListItem Text="20" Value="20"></asp:ListItem>
                                                        <asp:ListItem Text="21" Value="21"></asp:ListItem>
                                                        <asp:ListItem Text="22" Value="22"></asp:ListItem>
                                                        <asp:ListItem Text="23" Value="23"></asp:ListItem>
                                                    </asp:DropDownList>
                                                    <asp:Label ID="lblTimeSeparator"
                                                        AssociatedControlID="ddlStartTimeMinute" runat="server"
                                                        Text=":" />
                                                    <asp:DropDownList ID="ddlStartTimeMinute" runat="server"
                                                        meta:resourcekey="ddlStartTimeMinuteResource1">
                                                        <asp:ListItem Text="00" Value="00"></asp:ListItem>
                                                        <asp:ListItem Text="01" Value="01"></asp:ListItem>
                                                        <asp:ListItem Text="02" Value="02"></asp:ListItem>
                                                        <asp:ListItem Text="03" Value="03"></asp:ListItem>
                                                        <asp:ListItem Text="04" Value="04"></asp:ListItem>
                                                        <asp:ListItem Text="05" Value="05"></asp:ListItem>
                                                        <asp:ListItem Text="06" Value="06"></asp:ListItem>
                                                        <asp:ListItem Text="07" Value="07"></asp:ListItem>
                                                        <asp:ListItem Text="08" Value="08"></asp:ListItem>
                                                        <asp:ListItem Text="09" Value="09"></asp:ListItem>
                                                        <asp:ListItem Text="10" Value="10"></asp:ListItem>
                                                        <asp:ListItem Text="11" Value="11"></asp:ListItem>
                                                        <asp:ListItem Text="12" Value="12"></asp:ListItem>
                                                        <asp:ListItem Text="13" Value="13"></asp:ListItem>
                                                        <asp:ListItem Text="14" Value="14"></asp:ListItem>
                                                        <asp:ListItem Text="15" Value="15"></asp:ListItem>
                                                        <asp:ListItem Text="16" Value="16"></asp:ListItem>
                                                        <asp:ListItem Text="17" Value="17"></asp:ListItem>
                                                        <asp:ListItem Text="18" Value="18"></asp:ListItem>
                                                        <asp:ListItem Text="19" Value="19"></asp:ListItem>
                                                        <asp:ListItem Text="20" Value="20"></asp:ListItem>
                                                        <asp:ListItem Text="21" Value="21"></asp:ListItem>
                                                        <asp:ListItem Text="22" Value="22"></asp:ListItem>
                                                        <asp:ListItem Text="23" Value="23"></asp:ListItem>
                                                        <asp:ListItem Text="24" Value="24"></asp:ListItem>
                                                        <asp:ListItem Text="25" Value="25"></asp:ListItem>
                                                        <asp:ListItem Text="26" Value="26"></asp:ListItem>
                                                        <asp:ListItem Text="27" Value="27"></asp:ListItem>
                                                        <asp:ListItem Text="28" Value="28"></asp:ListItem>
                                                        <asp:ListItem Text="29" Value="29"></asp:ListItem>
                                                        <asp:ListItem Text="30" Value="30"></asp:ListItem>
                                                        <asp:ListItem Text="31" Value="31"></asp:ListItem>
                                                        <asp:ListItem Text="32" Value="32"></asp:ListItem>
                                                        <asp:ListItem Text="33" Value="33"></asp:ListItem>
                                                        <asp:ListItem Text="34" Value="34"></asp:ListItem>
                                                        <asp:ListItem Text="35" Value="35"></asp:ListItem>
                                                        <asp:ListItem Text="36" Value="36"></asp:ListItem>
                                                        <asp:ListItem Text="37" Value="37"></asp:ListItem>
                                                        <asp:ListItem Text="38" Value="38"></asp:ListItem>
                                                        <asp:ListItem Text="39" Value="39"></asp:ListItem>
                                                        <asp:ListItem Text="40" Value="40"></asp:ListItem>
                                                        <asp:ListItem Text="41" Value="41"></asp:ListItem>
                                                        <asp:ListItem Text="42" Value="42"></asp:ListItem>
                                                        <asp:ListItem Text="43" Value="43"></asp:ListItem>
                                                        <asp:ListItem Text="44" Value="44"></asp:ListItem>
                                                        <asp:ListItem Text="45" Value="45"></asp:ListItem>
                                                        <asp:ListItem Text="46" Value="46"></asp:ListItem>
                                                        <asp:ListItem Text="47" Value="47"></asp:ListItem>
                                                        <asp:ListItem Text="48" Value="48"></asp:ListItem>
                                                        <asp:ListItem Text="49" Value="49"></asp:ListItem>
                                                        <asp:ListItem Text="50" Value="50"></asp:ListItem>
                                                        <asp:ListItem Text="51" Value="51"></asp:ListItem>
                                                        <asp:ListItem Text="52" Value="52"></asp:ListItem>
                                                        <asp:ListItem Text="53" Value="53"></asp:ListItem>
                                                        <asp:ListItem Text="54" Value="54"></asp:ListItem>
                                                        <asp:ListItem Text="55" Value="55"></asp:ListItem>
                                                        <asp:ListItem Text="56" Value="56"></asp:ListItem>
                                                        <asp:ListItem Text="57" Value="57"></asp:ListItem>
                                                        <asp:ListItem Text="58" Value="58"></asp:ListItem>
                                                        <asp:ListItem Text="59" Value="59"></asp:ListItem>
                                                    </asp:DropDownList>
                                                    <asp:CustomValidator ID="cvStartDateTime" runat="Server"
                                                        ErrorMessage="<%$ Resources:Errors, Err_StartDateLessThanNow %>"
                                                        ControlToValidate="recurrenceCombo"
                                                        OnServerValidate="IsStartDateValid" ValidationGroup="Schedule">*
                                                    </asp:CustomValidator>
                                                    <asp:CustomValidator ID="cvDuplicateSchedule" runat="Server"
                                                        ErrorMessage="<%$ Resources:Errors, Err_DuplicateScheduledTask %>"
                                                        ControlToValidate="recurrenceCombo"
                                                        OnServerValidate="IsScheduleValid" ValidationGroup="Schedule">*
                                                    </asp:CustomValidator>
                                                </td>
                                            </tr>
                                            <tr id="rowStartDate" runat=server>
                                                <td width=140px runat="server">
                                                    <asp:Label id="lblStartDate" runat=server
                                                        Text="<%$ Resources:LogiAdHoc, ScheduledReport_StartDate %>">
                                                    </asp:Label>
                                                </td>
                                                <td runat="server">
                                                    <wizard:datebox id="startDate" runat="server" />
                                                </td>
                                            </tr>
                                            <tr id="rowEndDate" runat=server>
                                                <td width=140px runat="server">
                                                    <asp:CheckBox ID="hasEndDate" Runat=server AutoPostBack=True>
                                                    </asp:CheckBox>
                                                    <asp:Label ID="endDateLabel" runat=server
                                                        AssociatedControlID="hasEndDate"
                                                        Text="<%$ Resources:LogiAdHoc, ScheduledReport_EndDate %>">
                                                    </asp:Label>
                                                </td>
                                                <td runat="server">
                                                    <wizard:datebox id="endDate" runat="server" />
                                                    <asp:CustomValidator ID="cvEndDateValid" runat="Server"
                                                        ErrorMessage="<%$ Resources:Errors, Err_EndDateLessThenStartDate %>"
                                                        ControlToValidate="recurrenceCombo"
                                                        OnServerValidate="IsEndDateValid" ValidationGroup="Schedule">*
                                                    </asp:CustomValidator>
                                                </td>
                                            </tr>
                                            <tr id="trRepeat" runat="server">
                                                <td id="Td2" runat="server" width="100px">
                                                    <asp:CheckBox ID="chkRepeatTask" runat="server"
                                                        AutoPostBack="True" />
                                                    <asp:Label ID="lblRepeatTask" runat="server"
                                                        meta:resourcekey="lblRepeatTaskResource1" Text="Repeat Task"
                                                        AssociatedControlID="chkRepeatTask"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr id="trRepeatTask" runat="server">
                                                <td>
                                                </td>
                                                <td>
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                <asp:Label ID="Label22" runat="server"
                                                                    meta:resourcekey="LiteralResource22"
                                                                    Text="Interval:"
                                                                    AssociatedControlID="txtIntervalMinutes">
                                                                </asp:Label>&nbsp;
                                                            </td>
                                                            <td>
                                                                <asp:TextBox ID="txtIntervalMinutes" runat="server"
                                                                    MaxLength="10" Width="30">
                                                                </asp:TextBox>
                                                                <asp:Label ID="lblMinutes1" runat="server"
                                                                    Text="<%$ Resources:LogiAdHoc, Minutes %>">
                                                                </asp:Label>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <asp:Label ID="Label23" runat="server"
                                                                    meta:resourcekey="LiteralResource23"
                                                                    Text="Duration:"
                                                                    AssociatedControlID="txtDurationMinutes">
                                                                </asp:Label>&nbsp;
                                                            </td>
                                                            <td>
                                                                <asp:TextBox ID="txtDurationMinutes" runat="server"
                                                                    MaxLength="10" Width="30">
                                                                </asp:TextBox>
                                                                <asp:Label ID="lblMinutes2" runat="server"
                                                                    Text="<%$ Resources:LogiAdHoc, Minutes %>">
                                                                </asp:Label>
                                                                <asp:CustomValidator ID="cvValidRepeatTask"
                                                                    runat="server"
                                                                    ControlToValidate="txtDurationMinutes"
                                                                    ErrorMessage="Duration minutes should be greater than or equal to interval minutes."
                                                                    OnServerValidate="IsRepeatTaskTimeValid"
                                                                    ValidateEmptyText="true" ValidationGroup="Schedule">
                                                                    *</asp:CustomValidator>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </DIV>
                                </td>
                                <td>
                                    <DIV id="recurrenceGroup" runat=server>
                                        <asp:Panel id="dailyPanel" CssClass="detailpanel" runat="server" Width="424px"
                                            Height="200px" ms_positioning="GridLayout"
                                            meta:resourcekey="dailyPanelResource1">
                                            <h2>
                                                <asp:Localize ID="Localize7" runat="server"
                                                    meta:resourcekey="LiteralResource7" Text="Schedule Task Daily">
                                                </asp:Localize>
                                            </h2>
                                            <asp:Label id="Label1" AssociatedControlID="dailyEveryNthDayEdit"
                                                style="margin-left:10px;" runat="server"
                                                meta:resourcekey="Label1Resource1">Every</asp:Label>
                                            <asp:TextBox id="dailyEveryNthDayEdit" runat="server" Width="56px"
                                                meta:resourcekey="dailyEveryNthDayEditResource1"></asp:TextBox>
                                            <asp:Label id="Label3" runat="server" meta:resourcekey="Label3Resource1">
                                                day(s)</asp:Label>
                                            <asp:CustomValidator ID="cvNthDay" runat="server"
                                                ErrorMessage="<%$ Resources:Errors, Err_ScheduleNthDayNumber %>"
                                                ControlToValidate="dailyEveryNthDayEdit"
                                                OnServerValidate="IsNthDayValid" ValidationGroup="Daily">*
                                            </asp:CustomValidator>
                                            <asp:ValidationSummary ID="ValidationSummary1" runat="server"
                                                ValidationGroup="Daily" meta:resourcekey="vsummaryResource1" />
                                            <asp:Label ID="lblSemicolon1" runat="server"
                                                OnPreRender="lblSemicolon_PreRender" />
                                        </asp:Panel>
                                        <asp:Panel id="weeklyPanel" CssClass="detailpanel" runat="server" Width="424px"
                                            ms_positioning="GridLayout" Height="200px"
                                            meta:resourcekey="weeklyPanelResource1">
                                            <h2>
                                                <asp:Localize ID="Localize8" runat="server"
                                                    meta:resourcekey="LiteralResource8" Text="Schedule Task Weekly">
                                                </asp:Localize>
                                            </h2>
                                            <asp:Label id="Label8" style="margin-left:10px;" runat="server"
                                                AssociatedControlID="weeklyEveryNthWeekEdit"
                                                meta:resourcekey="Label1Resource1">Every</asp:Label>
                                            <asp:TextBox id="weeklyEveryNthWeekEdit" runat="server" Width="56px"
                                                meta:resourcekey="weeklyEveryNthWeekEditResource1"></asp:TextBox>
                                            <asp:Label id="Label7" runat="server" meta:resourcekey="Label7Resource1">
                                                week(s) on:</asp:Label>
                                            <asp:RangeValidator ID="dtvNthWeek" runat="server"
                                                ErrorMessage="Number of weeks must be a number."
                                                ValidationGroup="Weekly" Type="Integer"
                                                ControlToValidate="weeklyEveryNthWeekEdit" MinimumValue="1"
                                                MaximumValue="32000" meta:resourcekey="dtvNthWeekResource1">*
                                            </asp:RangeValidator>
                                            <asp:RequiredFieldValidator ID="reqNthWeek" runat="server"
                                                ControlToValidate="weeklyEveryNthWeekEdit" ValidationGroup="Weekly"
                                                ErrorMessage="Number of weeks is required."
                                                meta:resourcekey="reqNthWeekResource1">*</asp:RequiredFieldValidator>

                                            <asp:CheckBoxList id="weekDaysBtns" runat="server"
                                                RepeatDirection=Horizontal RepeatColumns="5"
                                                meta:resourcekey="weekDaysBtnsResource1">
                                                <asp:ListItem Value="Monday" meta:resourcekey="ListItemResource10">
                                                    Monday</asp:ListItem>
                                                <asp:ListItem Value="Tuesday" meta:resourcekey="ListItemResource11">
                                                    Tuesday</asp:ListItem>
                                                <asp:ListItem Value="Wednesday" meta:resourcekey="ListItemResource12">
                                                    Wednesday</asp:ListItem>
                                                <asp:ListItem Value="Thursday" meta:resourcekey="ListItemResource13">
                                                    Thursday</asp:ListItem>
                                                <asp:ListItem Value="Friday" meta:resourcekey="ListItemResource14">
                                                    Friday</asp:ListItem>
                                                <asp:ListItem Value="Saturday" meta:resourcekey="ListItemResource15">
                                                    Saturday</asp:ListItem>
                                                <asp:ListItem Value="Sunday" meta:resourcekey="ListItemResource16">
                                                    Sunday</asp:ListItem>
                                            </asp:CheckBoxList>
                                            <asp:ValidationSummary ID="ValidationSummary3" runat="server"
                                                ValidationGroup="Weekly" />
                                            <asp:Label ID="lblSemicolon3" runat="server"
                                                OnPreRender="lblSemicolon_PreRender" />
                                        </asp:Panel>
                                        <asp:Panel id="monthlyPanel" CssClass="detailpanel" runat="server" Width="424px"
                                            ms_positioning="GridLayout" Height="200px"
                                            meta:resourcekey="monthlyPanelResource1">
                                            <h2>
                                                <asp:Localize ID="Localize9" runat="server"
                                                    meta:resourcekey="LiteralResource9" Text="Schedule Task Monthly">
                                                </asp:Localize>
                                            </h2>
                                            <table>
                                                <tr>
                                                    <td>
                                                        <asp:RadioButtonList id="monthlyRadio" runat="server"
                                                            AutoPostBack="True"
                                                            meta:resourcekey="monthlyRadioResource1">
                                                            <asp:ListItem Value="0"
                                                                meta:resourcekey="ListItemResource17">Day</asp:ListItem>
                                                            <asp:ListItem Value="1"
                                                                meta:resourcekey="ListItemResource18">The</asp:ListItem>
                                                        </asp:RadioButtonList>
                                                    </td>
                                                    <td>
                                                        <asp:TextBox id="monthlyDayEdit" runat="server" Width="56px"
                                                            meta:resourcekey="monthlyDayEditResource1"></asp:TextBox>
                                                        <asp:Label id="Label9" runat="server"
                                                            AssociatedControlID="monthlyDayEdit"
                                                            meta:resourcekey="Label9Resource1"> of the month(s)
                                                        </asp:Label>
                                                        <asp:RangeValidator ID="dtvMonthlyDay" runat="server"
                                                            ErrorMessage="Day of month must be a number between 1 and 31."
                                                            ValidationGroup="Monthly" Type="Integer"
                                                            ControlToValidate="monthlyDayEdit" MinimumValue="1"
                                                            MaximumValue="31" meta:resourcekey="dtvMonthlyDayResource1">
                                                            *</asp:RangeValidator>
                                                        <asp:RequiredFieldValidator ID="reqMonthlyDay" runat="server"
                                                            ControlToValidate="monthlyDayEdit" ValidationGroup="Monthly"
                                                            ErrorMessage="Day of month is required."
                                                            meta:resourcekey="reqMonthlyDayResource1">*
                                                        </asp:RequiredFieldValidator>
                                                        <asp:CustomValidator ID="cvMonthlyDay" runat="server"
                                                            ErrorMessage="Day of month is not valid for selected month."
                                                            ValidationGroup="Monthly" ControlToValidate="monthlyDayEdit"
                                                            OnServerValidate="IsMonthlyDayValid"
                                                            meta:resourcekey="cvMonthlyDayResource1">*
                                                        </asp:CustomValidator>
                                                        <br>
                                                        <asp:Label ID="lblMonthlyNthOccurence" runat="server"
                                                            CssClass="NoShow" Text="Occurence"
                                                            AssociatedControlID="monthlyNthOccurrenceCombo"
                                                            meta:resourcekey="lblMonthlyNthOccurrenceResource1" />
                                                        <asp:Label ID="lblMonthlyDayOfWeek" runat="server"
                                                            CssClass="NoShow" Text="Day of Week"
                                                            AssociatedControlID="monthlyDayOfWeekCombo"
                                                            meta:resourcekey="lblMonthlyDayOfWeekResource1" />
                                                        <asp:DropDownList id="monthlyNthOccurrenceCombo" runat="server"
                                                            Width="96px"
                                                            meta:resourcekey="monthlyNthOccurrenceComboResource1">
                                                        </asp:DropDownList>
                                                        <asp:DropDownList id="monthlyDayOfWeekCombo" runat="server"
                                                            Width="96px"
                                                            meta:resourcekey="monthlyDayOfWeekComboResource1">
                                                        </asp:DropDownList>
                                                        <asp:Label id="Label10" runat="server"
                                                            meta:resourcekey="Label10Resource1"> of the month(s)
                                                        </asp:Label>
                                                    </td>
                                                </tr>
                                            </table>
                                            <asp:CheckBoxList id="monthBtns" runat="server" RepeatColumns="4"
                                                meta:resourcekey="monthBtnsResource1">
                                                <asp:ListItem Value="January" meta:resourcekey="ListItemResource19">
                                                    January</asp:ListItem>
                                                <asp:ListItem Value="February" meta:resourcekey="ListItemResource20">
                                                    February</asp:ListItem>
                                                <asp:ListItem Value="March" meta:resourcekey="ListItemResource21">March
                                                </asp:ListItem>
                                                <asp:ListItem Value="April" meta:resourcekey="ListItemResource22">April
                                                </asp:ListItem>
                                                <asp:ListItem Value="May" meta:resourcekey="ListItemResource23">May
                                                </asp:ListItem>
                                                <asp:ListItem Value="June" meta:resourcekey="ListItemResource24">June
                                                </asp:ListItem>
                                                <asp:ListItem Value="July" meta:resourcekey="ListItemResource25">July
                                                </asp:ListItem>
                                                <asp:ListItem Value="August" meta:resourcekey="ListItemResource26">
                                                    August</asp:ListItem>
                                                <asp:ListItem Value="September" meta:resourcekey="ListItemResource27">
                                                    September</asp:ListItem>
                                                <asp:ListItem Value="October" meta:resourcekey="ListItemResource28">
                                                    October</asp:ListItem>
                                                <asp:ListItem Value="November" meta:resourcekey="ListItemResource29">
                                                    November</asp:ListItem>
                                                <asp:ListItem Value="December" meta:resourcekey="ListItemResource30">
                                                    December</asp:ListItem>
                                            </asp:CheckBoxList>
                                            <asp:ValidationSummary ID="ValidationSummary4" runat="server"
                                                ValidationGroup="Monthly" />
                                            <asp:Label ID="lblSemicolon4" runat="server"
                                                OnPreRender="lblSemicolon_PreRender" />
                                        </asp:Panel>
                                    </DIV>
                                </td>
                            </tr>
                        </table>
                        <div id="ParameterGroup" runat=server>
                            <h2>
                                <asp:Localize ID="Localize12" runat="server" meta:resourcekey="LiteralResource12"
                                    Text="Report Input Parameters"></asp:Localize>
                            </h2>
                            <asp:UpdatePanel ID="UPParametersGrid" runat="server" UpdateMode="Conditional">
                                <ContentTemplate>
                                    <table class="limiting">
                                        <tr>
                                            <td>
                                                <table class="gridForm">
                                                    <tr>
                                                        <td>
                                                            <div id="data_main">
                                                                <asp:GridView ID="ParameterGrid" Runat="server"
                                                                    AutoGenerateColumns="False"
                                                                    OnRowCommand="OnItemCommandHandler"
                                                                    OnRowDataBound="OnItemDataBoundHandler"
                                                                    CssClass="grid"
                                                                    meta:resourcekey="ParameterGridResource1">
                                                                    <Columns>
                                                                        <asp:TemplateField
                                                                            HeaderText="<%$ Resources:LogiAdHoc, Parameter %>">
                                                                            <ItemTemplate>
                                                                                <input type="hidden" id="hDataStructID"
                                                                                    runat="server" />
                                                                                <input type="hidden" id="hParamID"
                                                                                    runat="server" />
                                                                                <span>&nbsp;&nbsp;<asp:Label
                                                                                        ID="lblBitCmd" Runat="server"
                                                                                        meta:resourcekey="lblBitCmdResource1">
                                                                                    </asp:Label></span>
                                                                                <asp:Label ID="Spacer" Runat="server"
                                                                                    meta:resourcekey="SpacerResource1">
                                                                                </asp:Label>
                                                                                <asp:Label ID="lblOpenParen"
                                                                                    Runat="server"
                                                                                    meta:resourcekey="lblOpenParenResource1">
                                                                                </asp:Label>
                                                                                <asp:Label ID="ColumnName"
                                                                                    Runat="server"
                                                                                    meta:resourcekey="ColumnNameResource1" />
                                                                            </ItemTemplate>
                                                                            <ItemStyle VerticalAlign="Top" />
                                                                        </asp:TemplateField>

                                                                        <asp:TemplateField HeaderText="Operator"
                                                                            meta:resourcekey="TemplateFieldResource2">
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="Operator" Runat="server"
                                                                                    meta:resourcekey="OperatorResource1" />
                                                                            </ItemTemplate>
                                                                            <ItemStyle VerticalAlign="Top" />
                                                                        </asp:TemplateField>

                                                                        <asp:TemplateField HeaderText="Value"
                                                                            meta:resourcekey="TemplateFieldResource3">
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="lblValue" runat="server"
                                                                                    meta:resourcekey="lblValueResource1">
                                                                                </asp:Label>
                                                                                <asp:Label ID="lblCloseParen"
                                                                                    Runat="server"
                                                                                    meta:resourcekey="lblCloseParenResource1">
                                                                                </asp:Label>
                                                                            </ItemTemplate>
                                                                            <ItemStyle VerticalAlign="Top" />
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField
                                                                            HeaderText="<%$ Resources:LogiAdHoc, Actions %>"
                                                                            meta:resourcekey="TemplateFieldResource5">
                                                                            <HeaderStyle Width="50px" />
                                                                            <ItemStyle HorizontalAlign="Center" />
                                                                            <ItemTemplate>
                                                                                <asp:ImageButton ID="imgModify"
                                                                                    SkinID="imgSingleAction"
                                                                                    AlternateText="Edit Parameter"
                                                                                    ToolTip="Edit Parameter"
                                                                                    runat="server"
                                                                                    CommandName="EditItem"
                                                                                    CausesValidation="False"
                                                                                    meta:resourcekey="EditItemResource1" />

                                                                                <%--<asp:Image ID="imgActions" AlternateText="Actions" runat="server" ToolTip="Actions"
                                                            ImageUrl="~/ahImages/arrowStep.gif" SkinID="imgActions" />
                                                        <asp:Panel ID="pnlActionsMenu" runat="server" CssClass="popupMenu" 
                                                            HorizontalAlign="Left" Wrap="false" style="display:none;">
                                                            <div id="divModify" runat="server" class="hoverMenuActionLink" >
                                                                <asp:LinkButton ID="lnkModify" runat="server" CausesValidation="False"
                                                                    CommandName="EditItem" Text="Edit Parameter" meta:resourcekey="EditItemResource1"></asp:LinkButton>
                                                            </div>
 				                                        </asp:Panel>
                                                        <ajaxToolkit:HoverMenuExtender ID="hme2" runat="server"
                                                            PopupControlID="pnlActionsMenu" PopupPosition="right" 
                                                            TargetControlID="imgActions" PopDelay="25" />--%>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                    </Columns>
                                                                    <HeaderStyle CssClass="gridheader" />
                                                                    <RowStyle CssClass="gridrow" />
                                                                    <AlternatingRowStyle CssClass="gridalternaterow" />
                                                                </asp:GridView>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                            <asp:CustomValidator ID="cvValidateAllParams" runat="server"
                                ControlToValidate="recurrenceCombo" EnableClientScript="False"
                                ErrorMessage="Some parameters do not have a value specified."
                                OnServerValidate="ValidateAllParameters" ValidationGroup="Schedule">*
                            </asp:CustomValidator>
                            <br />

                            <%--<table>
				            <tr>
				                <td>
				                    <asp:Localize ID="Localize14" runat="server" meta:resourcekey="LiteralResource17" Text="Run with Parameter Values shown in header"></asp:Localize>
				                </td>
				                <td>
				                    <asp:CheckBox ID="chkRunWithParams" runat="server" />
				                </td>
				            </tr>
				        </table>--%>

                            <asp:Button runat="server" ID="Button1" style="display:none" />
                            <ajaxToolkit:ModalPopupExtender runat="server" ID="ahModalPopup"
                                BehaviorID="ahModalPopupBehavior" TargetControlID="Button1" PopupControlID="ahPopup"
                                BackgroundCssClass="modalBackground" DropShadow="False"
                                PopupDragHandleControlID="pnlDragHandle" RepositionMode="None">
                            </ajaxToolkit:ModalPopupExtender>

                            <asp:Panel runat="server" CssClass="modalPopup" ID="ahPopup"
                                style="display:none;width:806;">
                                <%--<asp:Panel ID="ParamDetails" CssClass="detailpanel" runat="server" meta:resourcekey="ParamDetailsResource1">--%>
                                <asp:Panel ID="pnlDragHandle" runat="server" Style="cursor: hand;">
                                    <div class="modalPopupHandle" style="width: 800px;">
                                        <table cellpadding="0" cellspacing="0" style="width: 100%">
                                            <tr>
                                                <td>
                                                    <asp:Localize ID="Localize17" runat="server"
                                                        Text="<%$ Resources:LogiAdHoc, ParameterDetails %>">
                                                    </asp:Localize>:
                                                </td>
                                                <td style="width: 20px;">
                                                    <asp:ImageButton ID="imgClosePopup" runat="server"
                                                        OnClick="imgClosePopup_Click" CausesValidation="false"
                                                        SkinID="imgbClose" ImageUrl="../ahImages/remove.gif"
                                                        AlternateText="<%$ Resources:LogiAdHoc, CloseWindow %>" />
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </asp:Panel>
                                <div class="modalDiv">
                                    <asp:UpdatePanel ID="upPopup" runat="server" UpdateMode="COnditional">
                                        <ContentTemplate>
                                            <input type="hidden" id="NewParamID" runat="server" />
                                            <input type="hidden" id="NewParamDataStructID" runat="server" />
                                            <input type="hidden" id="ihColumnKey" runat="server" />
                                            <input type="hidden" id="InvOperatorName" runat="server" />
                                            <input type="hidden" id="ihParamDataCategory" runat="server" />
                                            <table cellspacing="0" cellpadding="1" border="0">
                                                <tr>
                                                    <td valign="top">
                                                        <asp:Localize ID="Localize18" runat="server"
                                                            Text="<%$ Resources:LogiAdHoc, Parameter %>"></asp:Localize>
                                                        :
                                                    </td>
                                                    <td valign="top">
                                                        <asp:Label ID="lblParamName" Runat="server"></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td valign="top">
                                                        <asp:Localize ID="Localize19" runat="server"
                                                            Text="<%$ Resources:LogiAdHoc, Op_Operator %>">
                                                        </asp:Localize>:
                                                    </td>
                                                    <td valign="top">
                                                        <asp:Label ID="lblOperatorName" Runat="server"></asp:Label>
                                                    </td>
                                                </tr>
                                            </table>
                                            <div id="divValues" runat="server">
                                                <table cellspacing="0" cellpadding="1" border="0">
                                                    <tr>
                                                        <td id="tdValueLabel" runat="server" valign="top" width="100px">
                                                            Value:</td>
                                                        <td id="Td1" valign="top" runat="server">
                                                            <span id="spnFV1" runat="server">
                                                                <wizard:SpecialValue ID="sv1" runat="server" />
                                                                <asp:ImageButton ID="btnFV1"
                                                                    AlternateText="Pick value from database"
                                                                    CausesValidation="False"
                                                                    ImageUrl="../ahImages/iconFind.gif"
                                                                    OnClick="PickFromDatabase" runat="server"
                                                                    meta:resourcekey="btnFV1Resource1" />
                                                            </span>
                                                            <asp:Label id="lblTo" Runat="server"
                                                                meta:resourcekey="lblToResource1" Text="And">
                                                            </asp:Label>
                                                            <div id="spnFV2" runat="server">
                                                                <wizard:SpecialValue ID="sv2" runat="server" />
                                                                <asp:ImageButton ID="btnFV2"
                                                                    AlternateText="Pick value from database"
                                                                    CausesValidation="False"
                                                                    ImageUrl="../ahImages/iconFind.gif"
                                                                    OnClick="PickFromDatabase" runat="server"
                                                                    meta:resourcekey="btnFV2Resource1" />
                                                            </div>
                                                            <span id="spnFV3" runat="server">
                                                                <asp:Label ID="lbltxaTextArea" runat="server"
                                                                    CssClass="NoShow" Text="Values"
                                                                    AssociatedControlID="txaTextArea"
                                                                    meta:resourcekey="lbltxaTextAreaResource1">
                                                                </asp:Label>
                                                                <asp:Textbox ID="txaTextArea" Runat="server"
                                                                    TextMode="MultiLine" Rows="8"
                                                                    meta:resourcekey="txaTextAreaResource1" />
                                                                <asp:ImageButton ID="btnFV3"
                                                                    AlternateText="Pick values from database"
                                                                    CausesValidation="False"
                                                                    ImageUrl="../ahImages/iconFind.gif"
                                                                    OnClick="PickFromDatabase" runat="server"
                                                                    meta:resourcekey="btnFV3Resource1" />
                                                            </span>
                                                            <asp:Label ID="lblchkBitValue" runat="server"
                                                                CssClass="NoShow"
                                                                Text="<%$ Resources:LogiAdHoc, BitValue %>"
                                                                AssociatedControlID="chkBitValue"></asp:Label>
                                                            <asp:CheckBox ID="chkBitValue" Runat="server"
                                                                meta:resourcekey="chkBitValueResource1" />
                                                            <%--<asp:CustomValidator ID="cvValidParam" OnServerValidate="IsParameterValid" ControlToValidate="txaTextArea"
                                                        EnableClientScript="False" ErrorMessage="This parameter leads to an invalid query."
                                                        runat="server" meta:resourcekey="cvValidParamResource1">*</asp:CustomValidator>--%>
                                                        </td>

                                                        <td id="tdFindValues" valign="top" runat="server">
                                                            <table>
                                                                <tr>
                                                                    <td>
                                                                        <%--<asp:ListBox ID="lstList" SelectionMode="Multiple" Rows="8" Runat="server" meta:resourcekey="lstListResource1" />--%>
                                                                        <wizard:DatabaseValues id="dbValues"
                                                                            runat="server" />
                                                                    </td>
                                                                    <td>
                                                                        <AdHoc:LogiButton ID="btnFVOk" Width="50px"
                                                                            runat="server"
                                                                            Text="<%$ Resources:LogiAdHoc, OKWithSpaces %>"
                                                                            OnClick="btnFVOk_Click"
                                                                            CausesValidation="False" />
                                                                        <br />
                                                                        <AdHoc:LogiButton ID="btnFVCancel" Width="50px"
                                                                            runat="server"
                                                                            Text="<%$ Resources:LogiAdHoc, Cancel %>"
                                                                            OnClick="btnFVCancel_Click"
                                                                            CausesValidation="False" />
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </div>
                                            <p class="info" id="P1" runat="server">
                                                <asp:Localize ID="Localize20" runat="server"
                                                    Text="<%$ Resources:LogiAdHoc, UseEnterKey %>"></asp:Localize>
                                            </p>
                                            <!-- Buttons -->
                                            <br />
                                            <table>
                                                <tr>
                                                    <td colspan="2">
                                                        <AdHoc:LogiButton ID="btnSaveParam" OnClick="SaveParam_OnClick"
                                                            runat="server"
                                                            Text="<%$ Resources:LogiAdHoc, SaveParameter %>" />
                                                        <AdHoc:LogiButton ID="btnCancelParam"
                                                            OnClick="CancelParam_OnClick" runat="server"
                                                            Text="<%$ Resources:LogiAdHoc, Cancel %>"
                                                            CausesValidation="False" />
                                                    </td>
                                                    <td width="250px" align="right">
                                                        <AdHoc:LogiButton ID="TestParam" OnClick="TestParam_OnClick"
                                                            Text="Validate Parameter" runat="server"
                                                            meta:resourcekey="TestParamResource1" />
                                                    </td>
                                                </tr>
                                            </table>
                                            <ul class="validation_error" id="ErrorList" runat="server">
                                                <li>
                                                    <asp:Label ID="lblValidationMessage" runat="server"
                                                        meta:resourcekey="lblValidationMessageResource1"></asp:Label>
                                                </li>
                                            </ul>
                                            <p id="pParamValidationDone" runat="server">
                                                <asp:Label ID="lblParamValidationDone" runat="server"></asp:Label>
                                                <br />
                                            </p>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </div>
                            </asp:Panel>
                        </div>
                        <div id="SubscribedUsersDiv" runat="server">
                            <h2>
                                <asp:Localize ID="Localize13" runat="server" meta:resourcekey="LiteralResource13"
                                    Text="Subscribed Users"></asp:Localize>
                            </h2>
                            <table>
                                <tr>
                                    <td>
                                        <div id="data_main2">
                                            <asp:GridView ID="SubscriberGrid" Runat="server" AutoGenerateColumns="False"
                                                CssClass="gridDetail" meta:resourcekey="SubscriberGridResource1">
                                                <Columns>
                                                    <asp:BoundField DataField="Username" HeaderText="User"
                                                        meta:resourcekey="BoundFieldResource1">
                                                        <ItemStyle VerticalAlign="Top" />
                                                        <HeaderStyle Width="100px" />
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="Email" HeaderText="Email"
                                                        meta:resourcekey="BoundFieldResource2">
                                                        <ItemStyle VerticalAlign="Top" />
                                                        <HeaderStyle Width="100px" />
                                                    </asp:BoundField>
                                                </Columns>
                                                <HeaderStyle CssClass="gridheader" />
                                                <RowStyle CssClass="gridrow" />
                                                <AlternatingRowStyle CssClass="gridalternaterow" />
                                            </asp:GridView>
                                        </div>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
            <div id="errorDiv" runat="server" style="color:red">
                <br>
                <asp:Localize ID="Localize15" runat="server" meta:resourcekey="LiteralResource15"
                    Text="* This schedule has been removed manually and is no longer available. It will be cleaned-up once you click the OK button.">
                </asp:Localize>
            </div>
            <div id="errorDiv2" runat="server" style="color:red">
                <br>
                <asp:Localize ID="Localize16" runat="server" meta:resourcekey="LiteralResource16"
                    Text="* You don't have enough permission to view this scheduled task."></asp:Localize>
            </div>
            <div id="divSaveAnimation">
                <asp:Button runat="server" ID="btnSaveAnimation" Style="display: none" />
                <ajaxToolkit:ModalPopupExtender runat="server" ID="ahSavePopup" BehaviorID="ahSavePopupBehavior"
                    TargetControlID="btnSaveAnimation" PopupControlID="pnlSaving" BackgroundCssClass="modalBackground"
                    DropShadow="False">
                </ajaxToolkit:ModalPopupExtender>
                <asp:Panel id="pnlSaving" CssClass="savePopup" runat="server" style="display:none;">
                    <table>
                        <tr>
                            <td>
                                <asp:Image ID="imgSave" runat="server" SkinID="imgSaving" EnableViewState="false"
                                    AlternateText="<%$ Resources:LogiAdHoc, SaveImageAltText %>" />
                            </td>
                            <td valign="middle">
                                <asp:Label ID="lblSaveText" CssClass="lblSavePopup" runat="server"
                                    EnableViewState="false" Text="<%$ Resources:LogiAdHoc, PleaseWait %>" />
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </div>

            <div id="divButtons" class="divButtons">
                <AdHoc:LogiButton id="Delete" Text="OK" OnClick="Delete_OnClick"
                    Tooltip="Click to delete the orphaned schedule and return to the schedule list." runat="server"
                    meta:resourcekey="DeleteResource1" />
                <AdHoc:LogiButton id="Save" Text="<%$ Resources:LogiAdHoc, Save %>" OnClick="Save_OnClick"
                    Tooltip="<%$ Resources:LogiAdHoc, SaveTooltip %>" runat="server" UseSubmitBehavior="false"
                    ValidationGroup="Schedule" />
                <AdHoc:LogiButton id="SaveAs" Text="Save As New" OnClick="SaveAs_OnClick"
                    Tooltip="Click to save your changes as a new Schedule." runat="server" ValidationGroup="Schedule"
                    UseSubmitBehavior="false" meta:resourcekey="SaveAsResource1" />
                <AdHoc:LogiButton id="Unschedule" Text="Un-schedule" OnClick="Unschedule_OnClick"
                    Tooltip="Click to delete this schedule and return to the schedule list." runat="server"
                    meta:resourcekey="UnscheduleResource1" />
                <AdHoc:LogiButton id="Cancel" Text="<%$ Resources:LogiAdHoc, Back %>" CausesValidation="False"
                    OnClick="Cancel_OnClick" Tooltip="<%$ Resources:LogiAdHoc, BackToObjectsTooltip %>"
                    runat="server" />

                <asp:UpdatePanel ID="UPSaveAs" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <asp:Button runat="server" ID="Button2" Style="display: none" />
                        <ajaxToolkit:ModalPopupExtender runat="server" ID="mpeSaveAs" BehaviorID="mpeSaveAsBehavior"
                            TargetControlID="Button2" PopupControlID="pnlSaveAs" BackgroundCssClass="modalBackground"
                            DropShadow="False" PopupDragHandleControlID="pnlDragHandle2" RepositionMode="None">
                        </ajaxToolkit:ModalPopupExtender>
                        <asp:Panel runat="server" CssClass="modalPopup" ID="pnlSaveAs"
                            Style="display: none; width: 250;">
                            <asp:Panel ID="pnlDragHandle2" runat="server" Style="cursor: hand;">
                                <div class="modalPopupHandle">
                                    <table cellpadding="0" cellspacing="0" style="width: 100%">
                                        <tr>
                                            <td>
                                                <asp:Localize ID="PopupHeader" runat="server"
                                                    Text="Save As New Schedule" meta:resourcekey="SaveAsHeader">
                                                </asp:Localize>
                                            </td>
                                            <td style="width: 20px;">
                                                <asp:ImageButton ID="imgClosePopup2" runat="server"
                                                    OnClick="imgClosePopup2_Click" CausesValidation="false"
                                                    SkinID="imgbClose" ImageUrl="../ahImages/remove.gif"
                                                    AlternateText="<%$ Resources:LogiAdHoc, CloseWindow %>" />
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </asp:Panel>
                            <div class="modalDiv">
                                <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                    <ContentTemplate>
                                        <%--<asp:Panel ID="pnlSaveAs" runat="server" CssClass="detailpanel">--%>
                                        <table>
                                            <tr>
                                                <td>
                                                    <asp:Localize ID="Localize14" runat="server"
                                                        meta:resourcekey="CopySubscriptions" Text="Copy Subscriptions:">
                                                    </asp:Localize>
                                                </td>
                                                <td>
                                                    <asp:RadioButton ID="rbtnCpySubsYes" runat="server"
                                                        GroupName="CopySubscribers"
                                                        Text="<%$ Resources:LogiAdHoc, Yes %>" /><br />
                                                    <asp:RadioButton ID="rbtnCpySubsNo" runat="server" Checked="True"
                                                        GroupName="CopySubscribers"
                                                        Text="<%$ Resources:LogiAdHoc, No %>" /><br />
                                                </td>
                                            </tr>
                                        </table>
                                        <br />
                                        <table>
                                            <tr>
                                                <td>
                                                    <AdHoc:LogiButton ID="btnSPA_OK" runat="server" OnClick="OK_SPA"
                                                        ToolTip="<%$ Resources:LogiAdHoc, SaveTooltip1 %>"
                                                        Text="<%$ Resources:LogiAdHoc, OKWithSpaces %>"
                                                        ValidationGroup="SaveAs" UseSubmitBehavior="false" />
                                                    <AdHoc:LogiButton ID="btnSPA_Cancel" runat="server"
                                                        OnClick="Cancel_SPA"
                                                        ToolTip="<%$ Resources:LogiAdHoc, CancelTooltip1 %>"
                                                        Text="<%$ Resources:LogiAdHoc, Cancel %>"
                                                        CausesValidation="false" />
                                                </td>
                                            </tr>
                                        </table>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </div>
                        </asp:Panel>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
            <br />
            <asp:ValidationSummary id="vsummary" runat="server" ValidationGroup="Schedule"
                meta:resourcekey="vsummaryResource1" />
            <asp:Label ID="lblSemicolon" runat="server" OnPreRender="lblSemicolon_PreRender" />
        </div>
    </form>
</body>

</html>