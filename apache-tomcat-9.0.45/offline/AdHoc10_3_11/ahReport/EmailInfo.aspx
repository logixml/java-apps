<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="EmailInfo.aspx.vb" Inherits="LogiAdHoc.EmailInfo" Culture="auto" UICulture="auto"  meta:resourcekey="PageResource1" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register Src="../ahControls/PagingControl.ascx" TagName="PagingControl" TagPrefix="uc1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Logi 10 Ad Hoc Reporting</title>
    <link rel="shortcut icon" href="../ahImages/flav.ico" />
    <script language="JavaScript" type="text/javascript" src="../ahScripts/ahChange.js"></script>
    <%--<LINK rel="stylesheet" type="text/css" href="../_StyleSheets/ahEngineSpecific.css" />--%>
    <script type="text/javascript">
	
	function validateMultEmails(sender, args)
    {
        var email = args.Value.split(';');
        for (var i = 0; i < email.length; i++) {
           var addr = TrimString(email[i])
           if (addr != '') 
           {
               if (!validateEmail(addr)) {
                  //alert('One or more email addresses entered is invalid');
                  args.IsValid = false;
                  return;
               }
           }
        }
        args.IsValid = true;
    }
    
    function validateSingleEmail(sender, args)
    {
        args.IsValid = validateEmail(args.Value);
    }

    function validateEmail(str)
    {
        var emailpat = /^([a-zA-Z0-9])+([\.a-zA-Z0-9_-])*@([a-zA-Z0-9_-])+(\.[a-zA-Z0-9_-]+)+$/;
        //if( !emailpat.test(str) && promptMsg ) alert( 'Please enter a valid email address in the From field' );
        if( !emailpat.test(str) ) return false;
        return true;
    }

    function TrimString(sInString) 
    {
        sInString = sInString.replace( /^\s+/g, "" );// strip leading
        return sInString.replace( /\s+$/g, "" );// strip trailing
    }
	</script>
</head>
<body id="bod">
    <form id="form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server" />
     
     <script type="text/javascript">
            Sys.WebForms.PageRequestManager.getInstance().add_endRequest(EndRequestHandler);
            Sys.WebForms.PageRequestManager.getInstance().add_beginRequest(BeginRequestHandler);
            function EndRequestHandler(sender, args) {
                RestorePopupPosition('ahModalPopupBehavior');
            }
            function BeginRequestHandler(sender, args) {
                SavePopupPosition('ahModalPopupBehavior');
            }
        </script>
        
    <div id="menu">
    <table id="tbMenu" runat="server">
        <tr id="trMenu" runat="server">
            <td>
                <asp:HyperLink ID="SiteLogo" runat="server" SkinID="SiteLogo" />
            </td>
        </tr>
    </table>
</div>
    <div  class="divForm">
        
        <asp:UpdatePanel ID="UPMain" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <table style="width:100%;" class="tblBody">
                    <tr class="trBody">
                        <td>
                            <span class="lbl">
                                <asp:Label ID="Label1" runat="server" Text="From:" AssociatedControlID="txtFrom" meta:resourcekey="LiteralResource1"></asp:Label>
                            </span>
                        </td>
                        <td class="tdBody">
                            <asp:TextBox ID="txtFrom" runat="server" Size="120" CssClass="txt" meta:resourcekey="txtFromResource1"/></td>
                        <td>
                            <%--<asp:ImageButton ID="btnFV1" AlternateText="Pick Email address" CausesValidation="False"
                                ImageUrl="../ahImages/iconFind.gif" OnClick="PickEmailAddress" runat="server" meta:resourcekey="btnFV1Resource1"/>--%>
                        </td>
                        <td>
                            <asp:RequiredFieldValidator ID="rfvFrom" runat="server" ErrorMessage="'From' field is required."
                                ControlToValidate="txtFrom" meta:resourcekey="rfvFromResource1">*</asp:RequiredFieldValidator></td>
                        <td>
                            <asp:CustomValidator ID="cvFromEmail" runat="server" ErrorMessage="'From' field should be a valid email address."
                                 ControlToValidate="txtFrom" ClientValidationFunction="validateSingleEmail" meta:resourcekey="cvFromEmailResource1">*</asp:CustomValidator></td>
                    </tr>
                    <tr class="trBody">
                        <td>
                            <span class="lbl">
                                <asp:Label ID="Label2" runat="server" Text="To:" AssociatedControlID="txtTo" meta:resourcekey="LiteralResource2"></asp:Label>
                            </span>
                        </td>
                        <td class="tdBody">
                            <asp:TextBox ID="txtTo" runat="server" Size="120" CssClass="txt" meta:resourcekey="txtToResource1" /></td>
                        <td>
                            <asp:ImageButton ID="btnFV2" AlternateText="Pick Email address" CausesValidation="False"
                                ImageUrl="../ahImages/iconFind.gif" OnClick="PickEmailAddress" runat="server" meta:resourcekey="btnFV2Resource1"/></td>
                        <td>
                            <asp:RequiredFieldValidator ID="rfvTo" runat="server" ErrorMessage="'To' field is required."
                                ControlToValidate="txtTo" meta:resourcekey="rfvToResource1">*</asp:RequiredFieldValidator></td>
                        <td>
                            <asp:CustomValidator ID="cvToEmail" runat="server" ErrorMessage="'To' field should contain valid email addresses. Please use semicolon(;) to separate multiple email addresses."
                                 ControlToValidate="txtTo" ClientValidationFunction="validateMultEmails" meta:resourcekey="cvToEmailResource1">*</asp:CustomValidator></td>
                    </tr>
                    <tr class="trBody">
                        <td>
                            <span class="lbl">
                                <asp:Label ID="Label3" runat="server" Text="Cc:" AssociatedControlID="txtCc" meta:resourcekey="LiteralResource3"></asp:Label>
                            </span>
                        </td>
                        <td class="tdBody">
                            <asp:TextBox ID="txtCc" runat="server" Size="120" CssClass="txt" meta:resourcekey="txtCcResource1" /></td>
                        <td>
                            <asp:ImageButton ID="btnFV3" AlternateText="Pick Email address" CausesValidation="False"
                                ImageUrl="../ahImages/iconFind.gif" OnClick="PickEmailAddress" runat="server" meta:resourcekey="btnFV3Resource1"/></td>
                        <td>
                            <asp:CustomValidator ID="cvCcEmail" runat="server" ErrorMessage="'Cc' field should contain valid email addresses. Please use semicolon(;) to separate multiple email addresses."
                                 ControlToValidate="txtCc" ClientValidationFunction="validateMultEmails" meta:resourcekey="cvCcEmailResource1">*</asp:CustomValidator></td>
                    </tr>
                    <tr class="trBody">
                        <td>
                            <span class="lbl">
                                <asp:Label ID="Label4" runat="server" Text="Bcc:" AssociatedControlID="txtBcc" meta:resourcekey="LiteralResource4"></asp:Label>
                            </span>
                        </td>
                        <td class="tdBody">
                            <asp:TextBox ID="txtBcc" runat="server" Size="120" CssClass="txt" meta:resourcekey="txtBccResource1" /></td>
                        <td>
                            <asp:ImageButton ID="btnFV4" AlternateText="Pick Email address" CausesValidation="False"
                                ImageUrl="../ahImages/iconFind.gif" OnClick="PickEmailAddress" runat="server" meta:resourcekey="btnFV4Resource1"/></td>
                        <td>
                            <asp:CustomValidator ID="cvBccEmail" runat="server" ErrorMessage="'Bcc' field should contain valid email addresses. Please use semicolon(;) to separate multiple email addresses."
                                 ControlToValidate="txtBcc" ClientValidationFunction="validateMultEmails" meta:resourcekey="cvBccEmailResource1">*</asp:CustomValidator></td>
                    </tr>
                    <tr class="trBody">
                        <td>
                            <span class="lbl">
                                <asp:Label ID="Label5" runat="server" Text="Subject:" AssociatedControlID="txtSubject" meta:resourcekey="LiteralResource5"></asp:Label>
                            </span>
                        </td>
                        <td class="tdBody">
                            <asp:TextBox ID="txtSubject" runat="server" Size="120" CssClass="txt" meta:resourcekey="txtSubjectResource1" /></td>
                        <td>
                            <asp:RequiredFieldValidator ID="rfvSubject" runat="server" ErrorMessage="'Subject' field is required."
                                ControlToValidate="txtSubject" meta:resourcekey="rfvSubjectResource1">*</asp:RequiredFieldValidator></td>
                    </tr>
                    <tr class="trBody">
                        <td colspan="2">
                            <asp:TextBox ID="txtBody" TextMode="MultiLine" CssClass="txtBody" Rows="20" runat="server" meta:resourcekey="txtBodyResource1" title="Body"/></td>
                        <td>
                            <asp:RequiredFieldValidator ID="rfvBody" runat="server" ErrorMessage="'Body' field is required."
                                ControlToValidate="txtBody" meta:resourcekey="rfvBodyResource1">*</asp:RequiredFieldValidator></td>
                    </tr>
                </table>
                    
                <asp:Button runat="server" ID="Button1" Style="display: none" meta:resourcekey="Button1Resource1" />
                <ajaxToolkit:ModalPopupExtender runat="server" ID="ahModalPopup" BehaviorID="ahModalPopupBehavior"
                    TargetControlID="Button1" PopupControlID="ahPopup" BackgroundCssClass="modalBackground" 
                    DropShadow="false" PopupDragHandleControlID="pnlDragHandle" RepositionMode="None">
                </ajaxToolkit:ModalPopupExtender>
                
                <asp:Panel runat="server" CssClass="modalPopup" ID="ahPopup" Style="display: none; width: 500;" meta:resourcekey="ahPopupResource1">
                    <asp:Panel ID="pnlDragHandle" runat="server" Style="cursor: hand;">
                    <div class="modalPopupHandle">
                        <table cellpadding="0" cellspacing="0" style="width: 100%"><tr><td>
                                <asp:Localize ID="PopupHeader" runat="server" Text="Add Email Address:" meta:resourcekey="PopupHeaderResource1"></asp:Localize>
                            </td>
                            <td style="width: 20px;">
                                <asp:ImageButton ID="imgClosePopup" runat="server" 
                                    OnClick="imgClosePopup_Click" CausesValidation="false"
                                    SkinID="imgbClose" ImageUrl="../ahImages/remove.gif" 
                                    AlternateText="<%$ Resources:LogiAdHoc, CloseWindow %>" />
                        </td></tr></table>
                    </div>
                    </asp:Panel>
                    <div class="modalDiv">
                    <asp:UpdatePanel ID="upPopup" runat="server">
                        <ContentTemplate>
                            <div id="divUsersList" runat="server" style="overflow: auto; height: 300px;">
                                <asp:GridView ID="grdMain" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                                    CssClass="grid" OnRowDataBound="OnItemDataBoundHandler" meta:resourcekey="grdMainResource1" >
                                    <Columns>
                                        <asp:TemplateField meta:resourcekey="TemplateFieldResource1" >
                                            <HeaderStyle Width="30px" />
                                            <HeaderTemplate>
                                                <asp:Label ID="lblChkAll" runat="server" AssociatedControlID="CheckAll" Text="<%$ Resources:LogiAdHoc, SelectDeselectAll %>" CssClass="NoShow"></asp:Label>
                                                <asp:CheckBox ID="CheckAll" runat="server" ToolTip="<%$ Resources:LogiAdHoc, SelectDeselectAll %>" meta:resourcekey="CheckAllResource1" />
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <asp:Label ID="lblChk" runat="server" AssociatedControlID="chk_Select" Text="<%$ Resources:LogiAdHoc, SelectDeselect %>" CssClass="NoShow"></asp:Label>
                                                <asp:CheckBox ID="chk_Select" runat="server" ToolTip="<%$ Resources:LogiAdHoc, SelectDeselect %>" />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="User" meta:resourcekey="TemplateFieldResource2">
                                            <ItemTemplate>
                                                <asp:Label ID="ahUserListUsername" EnableViewState="False" runat="server" meta:resourcekey="ahUserListUsernameResource1" />
                                                <input type="hidden" id="UserID" enableviewstate="False" runat="server"/>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Email" meta:resourcekey="TemplateFieldResource3" >
                                            <ItemTemplate>
                                                <asp:Label ID="ahUserListEmail" EnableViewState="False" runat="server" meta:resourcekey="ahUserListEmailResource1" />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                    <PagerTemplate>
                                        <uc1:PagingControl ID="pageCtrl" runat="server" OnGotoNextPage="ChangePageIndex" />
                                    </PagerTemplate>
                                    <PagerStyle HorizontalAlign="Center" />
                                    <HeaderStyle CssClass="gridheader" />
                                    <RowStyle CssClass="gridrow" />
                                    <AlternatingRowStyle CssClass="gridalternaterow" />
                                </asp:GridView>
                            </div>
                            <!-- Buttons -->
                            <table>
                                <tr>
                                    <td colspan="2">
                                        <AdHoc:LogiButton ID="btnAddEmailAddress" OnClick="AddEmail_OnClick" runat="server"
                                            Text="<%$ Resources:LogiAdHoc, OKWithSpaces %>" UseSubmitBehavior="False" CausesValidation="False" meta:resourcekey="btnAddEmailAddressResource1" />
                                        <AdHoc:LogiButton ID="btnCancelEmail" OnClick="CancelEmail_OnClick" runat="server" 
                                            Text="<%$ Resources:LogiAdHoc, Cancel %>" CausesValidation="False" meta:resourcekey="btnCancelEmailResource1" />
                                    </td>
                                </tr>
                            </table>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                    </div>
                </asp:Panel>
            </ContentTemplate>
        </asp:UpdatePanel>
        
        <div id="divButtons" class="divButtons">
            <%--<asp:Button ID="btnSave" CssClass="command" runat="server" OnClick="SendEmail" ToolTip="<%$ Resources:LogiAdHoc, SaveReportListTooltip %>"
                Text="<%$ Resources:LogiAdHoc, Save %>" UseSubmitBehavior="false" />--%>
            <AdHoc:LogiButton ID="btnSendArchiveEmail" runat="server" meta:resourcekey="btnSendArchiveEmailResource1"
                OnClick="btnSendArchiveEmail_Click" Text="Send" ToolTip="Click to send the archive link to specified recipients."
                UseSubmitBehavior="False" />
            <AdHoc:LogiButton ID="btnSave" runat="server" ToolTip="Click to the send the report to specified recipients."
                Text="Send" UseSubmitBehavior="False" meta:resourcekey="btnSaveResource1" />
            <asp:Button ID="btnSaveVal" onclick="btnSaveValidate" runat="server" ToolTip="Click to send the report to specified recipients."
                Text="Send" UseSubmitBehavior="False" meta:resourcekey="btnSaveResource1" />
            <AdHoc:LogiButton ID="btnCancel" runat="server" OnClick="BackToReport"
                ToolTip="<%$ Resources:LogiAdHoc, CancelTooltip %>" Text="<%$ Resources:LogiAdHoc, Cancel %>"
                CausesValidation="False" />
            <asp:ValidationSummary ID="vsummary" runat="server" meta:resourcekey="vsummaryResource1" />
        </div>
<br />
    </div>
    </form>
</body>
</html>
