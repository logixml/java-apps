<%@ Page Language="vb" AutoEventWireup="false" Codebehind="ScheduledReports.aspx.vb"
    Inherits="LogiAdHoc.ahReport_ScheduledReports" Culture="auto" meta:resourcekey="PageResource1" UICulture="auto" %>

<%@ Register TagPrefix="AdHoc" TagName="MainMenu" Src="~/ahControls/MainMenu.ascx" %>
<%@ Register TagPrefix="AdHoc" TagName="NavMenu" Src="~/ahControls/NavMenu.ascx" %>
<%@ Register TagPrefix="AdHoc" TagName="BreadCrumbTrail" Src="~/ahControls/BreadCrumbTrail.ascx" %>
<%@ Register TagPrefix="AdHoc" TagName="Search" Src="~/ahControls/Search.ascx" %>
<%@ Register TagPrefix="AdHoc" TagName="PagingControl" Src="~/ahControls/PagingControl.ascx" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Scheduled Reports</title>
    <link rel="shortcut icon" href="../ahImages/flav.ico" />
    <script language="JavaScript" type="text/javascript" src="../ahScripts/ahChange.js"></script>
</head>
<body>
    <AdHoc:MainMenu ID="menu" runat="server" />
    
    <form id="form1" runat="server">
	<div id="submenu"><AdHoc:NavMenu id="subnav" runat="server" /></div>
        <AdHoc:BreadCrumbTrail ID="bct" runat="server" Key="ScheduledReports" />
        
        <asp:ScriptManager ID="ScriptManager1" runat="server"
            ScriptMode="Release" />

        <script type="text/javascript">
            Sys.WebForms.PageRequestManager.getInstance().add_endRequest(EndRequestHandler);
            function EndRequestHandler(sender, args) {
                $find('ahSavePopupBehavior').hide();
            }
        </script>

<table class="limiting"><tr><td>
        <table class="gridForm"><tr><td>
            <asp:UpdatePanel ID="UP1" UpdateMode="Conditional" runat="server">
                <ContentTemplate>
                <div id="data_main" runat="server">
                    <div id="activities">
                        <table width="100%" cellpadding="0" cellspacing="0">
                        <tr width="100%">
                        <td align="left" valign="top">
                        <AdHoc:LogiButton ID="btnRemoveSchedule" runat="server" CausesValidation="False" OnClick="RemoveSchedule" 
                            Text="Delete Schedules" ToolTip="Click to remove selected schedules." meta:resourcekey="btnRemoveScheduleResource1" />
                        </td>
                        <td align="right" valign="top">
                        <AdHoc:Search id="srch" runat="server" title="Find Scheduled Reports" meta:resourcekey="AdHocSearch" />
                        </td>
                        </tr>
                        </table>
                    </div>
                                           
                    <asp:GridView id="grdMain" runat="server" CssClass="grid" AllowSorting="True" 
                    AutoGenerateColumns="False" AllowPaging="True" OnRowDataBound="OnItemDataBoundHandler" 
                    OnSorting="OnSortCommandHandler" DataKeyNames="ReportID" meta:resourcekey="grdMainResource1" >
                        <HeaderStyle CssClass="gridheader"></HeaderStyle>
                        <RowStyle CssClass="gridrow" />
                        <AlternatingRowStyle CssClass="gridalternaterow"></AlternatingRowStyle>
                        <PagerStyle HorizontalAlign="Center"></PagerStyle>
                        <PagerTemplate>
                            <AdHoc:PagingControl id="pageCtrl" runat="server" OnGotoNextPage="ChangePageIndex" />
                        </PagerTemplate>
                        <Columns>
                            <asp:TemplateField>
                                <HeaderStyle Width="30px"></HeaderStyle>
                                <HeaderTemplate>
                                    <asp:Label ID="lblChkAll" runat="server" AssociatedControlID="CheckAll" Text="<%$ Resources:LogiAdHoc, SelectDeselectAll %>" CssClass="NoShow"></asp:Label>
                                    <asp:CheckBox runat="server" ToolTip="<%$ Resources:LogiAdHoc, SelectDeselectAll %>" ID="CheckAll" />
                                </HeaderTemplate>
                                <ItemStyle HorizontalAlign="Center" Width="44px"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblChk" runat="server" AssociatedControlID="chk_Select" Text="<%$ Resources:LogiAdHoc, SelectDeselect %>" CssClass="NoShow"></asp:Label>
                                    <asp:CheckBox runat="server" ID="chk_Select" ToolTip="<%$ Resources:LogiAdHoc, SelectDeselect %>" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField SortExpression="ReportName" HeaderText="<%$ Resources:LogiAdHoc, Name %>">
                                <ItemTemplate>
                                    <%--<INPUT id="TaskName" type="hidden" runat="server" /> --%>
                                    <INPUT id="ReportID" type="hidden" runat="server" /> 
                                    <INPUT id="PhysicalName" type="hidden" runat="server" /> 
                                    <asp:HyperLink CssClass="ReportLink" ID="ReportName" Runat="server" meta:resourcekey="ReportNameResource1" />
                                    <asp:Label runat="server" CssClass="broken" ID="BrokenReport" meta:resourcekey="BrokenReportResource1"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="<%$ Resources:LogiAdHoc, Folder %>" SortExpression="Folder">
                                <ItemTemplate>
                                    <asp:HyperLink ID="ReportFolder" runat="server" CssClass="ReportLink" meta:resourcekey="ReportFolderResource1">
                                    </asp:HyperLink>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <%--<asp:BoundField SortExpression="FrequencyDesc" DataField="FrequencyDesc" HeaderText="<%$ Resources:LogiAdHoc, Frequency %>">
                                <HeaderStyle Width="60px"></HeaderStyle>
                            </asp:BoundField>
                            <asp:TemplateField SortExpression="Schedule" HeaderText="Schedule" meta:resourcekey="TemplateFieldResource3">
                                <HeaderStyle Width="200px"></HeaderStyle>
                                <ItemTemplate>
                                    <asp:Label id="Schedule" Runat="server" meta:resourcekey="ScheduleResource1"/><BR />
                                    <asp:Label id="LastRun" Runat="server" meta:resourcekey="LastRunResource1" /><BR />
                                    <asp:Label id="NextRun" Runat="server" meta:resourcekey="NextRunResource1" />
                                </ItemTemplate>
                            </asp:TemplateField>--%>
                            <asp:BoundField SortExpression="GroupName" DataField="GroupName" HeaderText="<%$ Resources:LogiAdHoc, UserGroup %>">
                            </asp:BoundField>
                            <%--<asp:TemplateField HeaderText="<%$ Resources:LogiAdHoc, Actions %>">
                                <ItemTemplate>
                                    <asp:ImageButton id="Modify" Runat="server" OnCommand="ModifySchedule" ToolTip="<%$ Resources:LogiAdHoc, ModifySchedulingInfo %>" 
                                    AlternateText="Modify Schedule" ImageUrl="../ahImages/Schedule.gif" meta:resourcekey="ModifyResource1">
                                    </asp:ImageButton> 
                                </ItemTemplate>
                            </asp:TemplateField>--%>
                            <asp:TemplateField HeaderText="Schedules" meta:resourcekey="TemplateFieldResource4" >
                                <HeaderStyle Width="50px"></HeaderStyle>
                                <ItemTemplate>
                                    <asp:LinkButton ID="ScheduleCount" runat="server" OnCommand="ShowScheduleList" />
                                    <asp:Label runat="server" CssClass="broken" ID="lblScheduleCount" meta:resourcekey="BrokenReportResource1"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView> 
                </div>
                    <ul runat="server" id="ErrorList" class="validation_error">
                        <li>
                            <asp:Label runat="server" EnableViewState="false" ID="lblValidationMessage"></asp:Label>
                        </li>
                    </ul>
                <div id="NoReport" runat="server">
     		        <br />
                    <asp:Localize ID="Localize1" runat="server" meta:resourcekey="LiteralResource1" Text="No scheduled reports were found."></asp:Localize>                
                </div>
                </contenttemplate>
                <Triggers>
                    <asp:AsyncPostBackTrigger ControlID="srch" EventName="DoSearch" />
                </Triggers>
            </asp:UpdatePanel>
            <div id="divSaveAnimation">
                <asp:Button runat="server" ID="btnSaveAnimation" Style="display: none" />
                <ajaxToolkit:ModalPopupExtender runat="server" ID="ahSavePopup" BehaviorID="ahSavePopupBehavior"
                    TargetControlID="btnSaveAnimation" PopupControlID="pnlSaving" BackgroundCssClass="modalBackground"
                    DropShadow="False">
                </ajaxToolkit:ModalPopupExtender>
                <asp:Panel id="pnlSaving" CssClass="savePopup" runat="server" style="display:none;">
                    <table><tr><td>
                    <asp:Image ID="imgSave" runat="server" SkinID="imgSaving" EnableViewState="false" AlternateText="<%$ Resources:LogiAdHoc, SaveImageAltText %>"/>
                    </td>
                    <td valign="middle">
                    <asp:Label ID="lblSaveText" CssClass="lblSavePopup" runat="server" EnableViewState="false" Text="<%$ Resources:LogiAdHoc, PleaseWait %>"/>
                    </td></tr></table>
                </asp:Panel>
            </div>
        </td></tr></table>
</td></tr></table>
    </form>
</body>
</html>
