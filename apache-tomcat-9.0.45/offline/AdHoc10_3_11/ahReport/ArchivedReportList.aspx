<%@ Page Language="vb" AutoEventWireup="false" Codebehind="ArchivedReportList.aspx.vb"
    Inherits="LogiAdHoc.ahReport_ArchivedReportList" Culture="auto" meta:resourcekey="PageResource1"
    UICulture="auto" %>

<%@ Register Assembly="System.Web.Extensions" Namespace="System.Web.UI" TagPrefix="asp" %>
<%@ Register TagPrefix="AdHoc" TagName="PagingControl" Src="../ahControls/PagingControl.ascx" %>
<%@ Register TagPrefix="AdHoc" TagName="MainMenu" Src="~/ahControls/MainMenu.ascx" %>
<%@ Register TagPrefix="AdHoc" TagName="NavMenu" Src="~/ahControls/NavMenu.ascx" %>
<%@ Register TagPrefix="AdHoc" TagName="BreadCrumbTrail" Src="~/ahControls/BreadCrumbTrail.ascx" %>
<%@ Register TagPrefix="AdHoc" TagName="Search" Src="~/ahControls/Search.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Archived Report List</title>
    <link rel="shortcut icon" href="../ahImages/flav.ico" />
    <script language="JavaScript" type="text/javascript" src="../ahScripts/ahChange.js"></script>

</head>
<body>
    <AdHoc:MainMenu ID="menu" runat="server" />
    
    <form id="form1" runat="server">
    <div id="submenu">
        <AdHoc:NavMenu ID="subnav" runat="server" />
    </div>
        <AdHoc:BreadCrumbTrail ID="bct" runat="server" Key="ArchRepLst" />
        
        <asp:ScriptManager ID="ScriptManager1" runat="server" />

        <script type="text/javascript">
            Sys.WebForms.PageRequestManager.getInstance().add_endRequest(EndRequestHandler);
            function EndRequestHandler(sender, args) {
                $find('ahSavePopupBehavior').hide();
            }
        </script>

<table class="limiting"><tr><td>
        <table class="gridForm"><tr><td>
            <asp:UpdatePanel ID="UP1" UpdateMode="Conditional" runat="server">
                <ContentTemplate>
                <div id="data_main">
                    <div id="activities">
                        <table width="100%" cellpadding="0" cellspacing="0">
                        <tr width="100%">
                        <td align="left" valign="top">
                        <AdHoc:LogiButton ID="btnRemoveArchives" Text="Delete Archives" CommandName="RemoveArchives"
                            runat="server" meta:resourcekey="btnRemoveArchivesResource1" />
                        <AdHoc:LogiButton ID="Cancel" Text="Back to Archives" CommandName="Cancel" runat="server" meta:resourcekey="btnCancelResource1"/>
                        </td>
                        <td align="right" valign="top">
                        <AdHoc:Search ID="srch" runat="server" Title="Find Archives" Width="400" meta:resourcekey="AdHocSearch" />
                        </td>
                        </tr>
                        </table>
                    </div>
                    <asp:GridView runat="server" ID="grdMain" AllowSorting="True" CssClass="grid" AutoGenerateColumns="False"
                        AllowPaging="True" meta:resourcekey="grdMainResource1"
                        OnSorting="OnSortCommandHandler" OnRowDataBound="OnItemDataBoundHandler">
                        <HeaderStyle CssClass="gridheader"></HeaderStyle>
                        <PagerStyle HorizontalAlign="Center"></PagerStyle>
                        <PagerTemplate>
                            <AdHoc:PagingControl runat="server" ID="pageCtrl" OnGotoNextPage="ChangePageIndex"></AdHoc:PagingControl>
                        </PagerTemplate>
                        <RowStyle CssClass="gridrow" />
                        <AlternatingRowStyle CssClass="gridalternaterow"></AlternatingRowStyle>
                        <Columns>
                            <asp:TemplateField>
                                <HeaderStyle Width="30px"></HeaderStyle>
                                <HeaderTemplate>
                                    <asp:Label ID="lblChkAll" runat="server" AssociatedControlID="CheckAll" Text="<%$ Resources:LogiAdHoc, SelectDeselectAll %>" CssClass="NoShow"></asp:Label>
                                    <asp:CheckBox runat="server" ToolTip="<%$ Resources:LogiAdHoc, SelectDeselectAll %>" ID="CheckAll" />
                                </HeaderTemplate>
                                <ItemStyle HorizontalAlign="Center" Width="44px"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblChk" runat="server" AssociatedControlID="chk_Select" Text="<%$ Resources:LogiAdHoc, SelectDeselect %>" CssClass="NoShow"></asp:Label>
                                    <asp:CheckBox runat="server" ID="chk_Select" ToolTip="<%$ Resources:LogiAdHoc, SelectDeselect %>" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField SortExpression="ArchivedDateTime" HeaderText="Archived Date" meta:resourcekey="TemplateFieldResource2">
                                <ItemTemplate>
                                    <asp:HyperLink runat="server" CssClass="ReportLink" ID="ArchivedDate"
                                        meta:resourcekey="ArchivedDateResource1"></asp:HyperLink>
                                    <input runat="server" id="ArchivedName" type="hidden"/>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField SortExpression="UserName" HeaderText="<%$ Resources:LogiAdHoc, Owner %>">
                                <ItemTemplate>
                                    <asp:Label runat="server" ID="OwnedBy" meta:resourcekey="OwnedByResource1"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="<%$ Resources:LogiAdHoc, Actions %>">
                                <HeaderStyle Width="50px" />
                                <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                <ItemTemplate>
                                    <asp:ImageButton ID="EmailArchive" ImageUrl="~/ahImages/sendEmail.gif" AlternateText="Email Archive" ToolTip="Email Archive"
                                        runat="server" OnCommand="EmailArchiveLink" meta:resourcekey="EmailArchiveResource1" />
                                    
                                    <%--<asp:Image ID="imgActions" AlternateText="Actions" runat="server" 
                                        ToolTip="Actions" ImageUrl="~/ahImages/arrowStep.gif" SkinID="imgActions" />
                                        <asp:Panel ID="pnlActionsMenu" runat="server" CssClass="popupMenu" 
                                            HorizontalAlign="Left" Wrap="false" style="display:none;">
                                            <div id="divModify" runat="server" class="hoverMenuActionLink" >
                                                <asp:LinkButton ID="EmailArchive" runat="server" Text="Email Archive"
                                                    meta:resourcekey="EmailArchiveResource1" OnCommand="EmailArchiveLink" />
                                            </div>
 				                        </asp:Panel>
                                    <ajaxToolkit:HoverMenuExtender ID="hme2" runat="server"
                                        PopupControlID="pnlActionsMenu" PopupPosition="right" 
                                        TargetControlID="imgActions" PopDelay="25" />--%>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                </div>
                    <div runat="server" id="NoReport">
                        <asp:Localize ID="Localize1" runat="server" meta:resourcekey="LiteralResource1" Text="No archived reports were found."></asp:Localize>                        
                    </div>
                </ContentTemplate>
                <Triggers>
                    <asp:PostBackTrigger ControlID="Cancel" />
                    <asp:AsyncPostBackTrigger ControlID="srch" EventName="DoSearch"></asp:AsyncPostBackTrigger>
                </Triggers>
            </asp:UpdatePanel>
            
            <div id="divSaveAnimation">
                <asp:Button runat="server" ID="btnSaveAnimation" Style="display: none" />
                <ajaxToolkit:ModalPopupExtender runat="server" ID="ahSavePopup" BehaviorID="ahSavePopupBehavior"
                    TargetControlID="btnSaveAnimation" PopupControlID="pnlSaving" BackgroundCssClass="modalBackground"
                    DropShadow="False">
                </ajaxToolkit:ModalPopupExtender>
                <asp:Panel id="pnlSaving" CssClass="savePopup" runat="server" style="display:none;">
                    <table><tr><td>
                    <asp:Image ID="imgSave" runat="server" SkinID="imgSaving" EnableViewState="false" AlternateText="<%$ Resources:LogiAdHoc, SaveImageAltText %>" />
                    </td>
                    <td valign="middle">
                    <asp:Label ID="lblSaveText" CssClass="lblSavePopup" runat="server" EnableViewState="false" Text="<%$ Resources:LogiAdHoc, PleaseWait %>"/>
                    </td></tr></table>
                </asp:Panel>
            </div>
        </td></tr></table>
</td></tr></table>
    </form>
</body>
</html>
