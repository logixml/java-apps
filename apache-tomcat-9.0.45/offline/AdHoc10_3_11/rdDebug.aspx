<%@ Page Language="vb" %>
<%
If Not IsNothing(Request("rdContentExtension")) then 
	If Request("rdContentExtension") = "xml" then 
		Response.ContentType="application/xml"
		Response.AddHeader ("content-disposition", "filename=" & Request("rdKey") & ".xml")
	Else
		Response.ContentType="text/plain"
		Response.AddHeader ("content-disposition", "filename=" & Request("rdKey") & ".txt")
	End If

End If

Response.Cache.SetCacheability(HttpCacheability.NoCache)

if isnothing(Session(Request("rdKey"))) then 
	Response.Write ("Sorry, this debug page has expired and was removed to conserve resources.")
else
	Response.Write (Session(Request("rdKey")))
end if
Response.End
%>
<head id="Head1" runat="server" visible=false/>