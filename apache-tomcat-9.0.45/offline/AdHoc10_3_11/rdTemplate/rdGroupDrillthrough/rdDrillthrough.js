﻿
function rdShowDrillthroughIcon(eleContainer,bShow) {
    //Empty cell?
    var sCellContent
    if (eleContainer.textContent != undefined) {
        sCellContent = eleContainer.textContent //Mozilla, Webkit
    } else {
        sCellContent = eleContainer.innerText //IE
    }
    if (sCellContent.replace(/ /g, '').replace(/\u00a0/g, '').length==0) { // \u00a0 is &nbsp;
        bShow = false //No drillthrough for empty cells.
    }

//    for (var i=0; i < eleContainer.childNodes.length; i++) {
//	    if (eleContainer.childNodes[i].tagName=="A") {
//	        if (eleContainer.childNodes[i].href.indexOf("Drillthrough") != -1) {
//	            var eleImage = eleContainer.childNodes[i].firstChild
//	            if (bShow) {
//	                eleImage.className = ""
//	            }else{
//	                eleImage.className = "rdHidden"
//	            }
//            }
//	    }

    var colImgNodes = Y.one(eleContainer).all('a')
    for (var i=0; i < colImgNodes.size(); i++) {
        var eleA = colImgNodes.item(i).getDOMNode()
	    if (eleA.href.indexOf("Drillthrough") != -1) {
            var eleImage = eleA.firstChild
            if (bShow) {
                eleImage.src = "rdTemplate/rdGroupDrillthrough/rdDrillthrough.gif"
            }else{
                eleImage.src = "rdTemplate/rdGroupDrillthrough/rdDrillthroughBlank.gif"
            }
	    }
	}
}

