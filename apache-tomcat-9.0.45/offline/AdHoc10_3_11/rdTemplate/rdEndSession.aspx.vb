Public Partial Class rdEndSession
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
#If JAVA Then '6654 9436
        Try
            Dim servletRequest As javax.servlet.http.HttpServletRequest = vmw.j2ee.J2EEUtils.getHttpServletRequest
            Dim servletSession As javax.servlet.http.HttpSession = servletRequest.getSession
            'Dim sReturn As String = servletSession.getAttribute("rdLogonReturnUrl")
            'If Not String.IsNullOrEmpty(sReturn) Then
            '    servletSession.removeAttribute("rdLogonReturnUrl")
            'End If
            Dim sUsername As String = servletSession.getAttribute("rdUserName")
            If Not String.IsNullOrEmpty(sUsername) Then
                servletSession.removeAttribute("rdUserName")
            End If
            Dim sUsername2 As String = servletSession.getAttribute("lgxUserID")
            If Not String.IsNullOrEmpty(sUsername2) Then
                servletSession.removeAttribute("lgxUserID")
            End If

            Dim sLicense As String = Session.Item("rdLic")
            Dim sProduct As String = Session.Item("rdProduct")
            Dim sJavaRedirect As String = Session.Item("rdJavaRedirect")
            Dim sFormLogon As String = Session.Item("rdFormLogon")
            HttpContext.Current.Session.RemoveAll()

            If Not String.IsNullOrEmpty(sLicense) Then
                Session.Add("rdLic", sLicense)
            End If
            If Not String.IsNullOrEmpty(sProduct) Then
                Session.Add("rdProduct", sProduct)
            End If
            If Not String.IsNullOrEmpty(sJavaRedirect) Then
                Session.Add("rdJavaRedirect", sJavaRedirect)
            End If
            If Not String.IsNullOrEmpty(sFormLogon) Then
                Session.Add("rdFormLogon", sFormLogon)
            End If

        Catch
        End Try
#Else
        Session.Abandon()
#End If
        Response.Cookies.Remove("ASP.NET_SessionId")

    End Sub

End Class