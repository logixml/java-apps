var rdFilterOldComparisonOptionsArray = new Array();    // Array holds the Analysis Grid Filter Comparison options ('Starts With' and 'Contains') in memory.
var rdFilterNewComparisonOptionsArray = new Array();    // Array holds the Analysis Grid Filter Comparison option ('Date Range') in memory.
// Holding the values in memory seems to be a better option to handle the issues that may raise with internationalization.

function rdAgShowMenuTab(sTabName, bCheckForReset) {

    var bOpen = true
    if (sTabName.length==0){
        bOpen=false
    }else{
        var eleSelectedTab = document.getElementById('col' + sTabName)
        var eleSelectedRow = document.getElementById('row' + sTabName)
        if (eleSelectedTab.className.indexOf('rdAgSelectedTab')!=-1) {
            bOpen = false
        }
        if (bCheckForReset){
            if (location.href.indexOf("rdAgReset")!=-1 || location.href.indexOf("rdAgLoadSaved")!=-1){
                bOpen = false
            }
        }
    }

    document.getElementById('rdAgCurrentOpenPanel').value = ''
    rdSetClassNameById('colCalc','rdAgUnselectedTab')
    rdSetClassNameById('colLayout','rdAgUnselectedTab')
    rdSetClassNameById('colSortOrder','rdAgUnselectedTab')
    rdSetClassNameById('colFilter','rdAgUnselectedTab')
    rdSetClassNameById('colGroup','rdAgUnselectedTab')
    rdSetClassNameById('colAggr','rdAgUnselectedTab')
    rdSetClassNameById('colChart','rdAgUnselectedTab')
    rdSetClassNameById('colCrosstab','rdAgUnselectedTab')
    rdSetClassNameById('colPaging','rdAgUnselectedTab')
    
    rdSetDisplayById('rowCalc','none')
    rdSetDisplayById('rowLayout','none')
    rdSetDisplayById('rowSortOrder','none')
    rdSetDisplayById('rowFilter','none')
    rdSetDisplayById('rowGroup','none')
    rdSetDisplayById('rowAggr','none')
    rdSetDisplayById('rowChart','none')
    rdSetDisplayById('rowCrosstab','none')
    rdSetDisplayById('rowPaging','none')
    
    if (bOpen) {
        document.getElementById('rdAgCurrentOpenPanel').value = sTabName
        eleSelectedTab.className = 'rdAgSelectedTab'
//        ShowElement(this.id,'row' + sTabName,'Show');
        if(!bCheckForReset)     // Avoid flicker/fading effect when Paged/Sorted/Postbacks.
            rdFadeElementIn(eleSelectedRow,250);    //#11723.
        eleSelectedRow.style.display=''
    }
    
    rdSetPanelModifiedClass('Calc')
    rdSetPanelModifiedClass('Layout')
    rdSetPanelModifiedClass('SortOrder')
    rdSetPanelModifiedClass('Filter')
    rdSetPanelModifiedClass('Group')
    rdSetPanelModifiedClass('Aggr')
    rdSetPanelModifiedClass('Chart')
    rdSetPanelModifiedClass('Crosstab')
    rdSetPanelModifiedClass('Paging')

    if (typeof window.rdRepositionSliders != 'undefined') {
		//Move CellColorSliders, if there are any.
		rdRepositionSliders()
	}

    if (sTabName=="Filter") {
        rdAgShowPickDistinctButton()
    }
    
    if (sTabName=="Group") {
        rdAgGetGroupByDateOperatorDiv()
    }
    if (sTabName=="Chart") {
        rdAgGetChartsGroupByDateOperatorDiv()
    }
    if (sTabName=="Crosstab") {
        rdAgGetCrosstabHeaderGroupByDateOperatorDiv()
        rdAgGetCrosstabLabelGroupByDateOperatorDiv()
    }
}

function rdSetClassNameById(sId, sClassName) {
    var ele = document.getElementById(sId)
    if(ele) {
        ele.className = sClassName
    }
}
function rdSetDisplayById(sId, sDisplay) {
    var ele = document.getElementById(sId)
    if(ele) {
        ele.style.display = sDisplay
    }
}

function rdSetPanelModifiedClass(sPanel) {
    var eleImage = document.getElementById("img" + sPanel)
    if (eleImage) {
        if (eleImage.className == "rdHighlightOn") {
            var eleButton = document.getElementById("col" + sPanel)
            eleButton.className = eleButton.className + " " + eleButton.className + "On"
        }
    }
}



//function rdAgShowMenuTab(sTabName) {

//    document.getElementById('colCalc').className = (rowCalc.style.display!='none' ? 'rdAgSelectedTab':'')
//    document.getElementById('colLayout').className = (rowCalc.style.display!='none' ? 'rdAgSelectedTab':'')
//    document.getElementById('colSortOrder').className = (rowCalc.style.display!='none' ? 'rdAgSelectedTab':'')
//    document.getElementById('colFilter').className = (rowCalc.style.display!='none' ? 'rdAgSelectedTab':'')
//    document.getElementById('colGroup').className = (rowCalc.style.display!='none' ? 'rdAgSelectedTab':'')
//    document.getElementById('colAggr').className = (rowCalc.style.display!='none' ? 'rdAgSelectedTab':'')
//    document.getElementById('colChart').className = (rowCalc.style.display!='none' ? 'rdAgSelectedTab':'')
//    document.getElementById('colCrosstab').className = (rowCalc.style.display!='none' ? 'rdAgSelectedTab':'')
//    document.getElementById('colPaging').className = (rowCalc.style.display!='none' ? 'rdAgSelectedTab':'')
//}


function rdAgShowChartAdd(sChartType) {
	document.rdForm.rdAgChartType.value=sChartType;
	ShowElement(this.id,'divChartAdd','Show');
	var bForecast = false;
    if(document.getElementById('IslAgForecastType') != null) bForecast = true;
	switch (sChartType) {
			case 'Pie':
			case 'Bar':
				ShowElement(this.id,'lblChartXLabelColumn','Show');
				ShowElement(this.id,'lblChartXAxisColumn','Hide');
				ShowElement(this.id,'lblChartYDataColumn','Show');
				ShowElement(this.id,'lblChartYAxisColumn','Hide');
				ShowElement(this.id,'lblChartSizeColumn','Hide');
				ShowElement(this.id,'rdAgChartXLabelColumn','Show');
				ShowElement(this.id,'rdAgChartXDataColumn','Hide');
				ShowElement(this.id,'rdAgChartXNumberColumn','Hide');
				ShowElement(this.id,'rdAgChartYAggrLabel','Show');
				ShowElement(this.id,'rdAgChartSizeColumnAggrLabel','Hide');
				ShowElement(this.id,'rdAgChartYAggrList','Show');
				ShowElement(this.id,'rdAgChartXLabelTextColumn','Hide');
				ShowElement(this.id,'rowChartExtraDataColumn','Hide');
				ShowElement(this.id,'rowChartExtraAggr','Hide');
				ShowElement(this.id,'rdAgGaugeMinMax','Hide');
				ShowElement(this.id,'rdAgGaugeGoals','Hide');
				ShowElement(this.id,'lblChartYDataAggregation','Hide');
				ShowElement(this.id,'colGaugeRingsColorSelection','Hide');
                rdAgGetChartsGroupByDateOperatorDiv();
                if(bForecast){
                    if(sChartType == "Bar"){
                        document.getElementById('rowChartForecast').style.display = '';
            	        document.getElementById('IslAgForecastType').style.display = '';
                        document.getElementById('rdAgChartForecastLabel').style.display = ''		        
                        rdAgModifyTimeSeriesCycleLengthOptions(document.getElementById('rdAgChartsDateGroupBy'));    
                        rdAgModifyForecastOptions(document.getElementById('rdAgChartXLabelColumn').value);
                        rdAgShowForecastOptions();
                    }else{
                        rdAgHideForecast();
                    }
                }       
				break;
			case 'Heatmap':
			    ShowElement(this.id,'lblChartXLabelColumn','Show');
				ShowElement(this.id,'lblChartXAxisColumn','Hide');
				ShowElement(this.id,'lblChartYDataColumn','Hide');
				ShowElement(this.id,'lblChartYAxisColumn','Hide');
				ShowElement(this.id,'lblChartSizeColumn','Show');
				ShowElement(this.id,'rdAgChartXLabelTextColumn','Show');
				ShowElement(this.id,'rdAgChartXLabelColumn','Hide');
				ShowElement(this.id,'rdAgChartXDataColumn','Hide');
				ShowElement(this.id,'rdAgChartXNumberColumn','Hide');
				ShowElement(this.id,'rdAgChartYAggrLabel','Hide');
			    ShowElement(this.id,'rdAgChartSizeColumnAggrLabel','Show');
				ShowElement(this.id,'rdAgChartYAggrList','Show');
				ShowElement(this.id,'rowChartExtraDataColumn','Show');
				ShowElement(this.id,'rowChartExtraAggr','Show');
				ShowElement(this.id,'divChartsGroupByDateOperator','Hide');
				ShowElement(this.id,'rdAgGaugeMinMax','Hide');
				ShowElement(this.id,'rdAgGaugeGoals','Hide');
				ShowElement(this.id,'lblChartYDataAggregation','Hide');
				ShowElement(this.id,'colGaugeRingsColorSelection','Hide');
				document.rdForm.rdAgChartsDateGroupBy.value='';
				rdAgHideForecast();
				break;
			case 'Scatter':
				ShowElement(this.id,'lblChartXLabelColumn','Hide');
				ShowElement(this.id,'lblChartXAxisColumn','Show');
				ShowElement(this.id,'lblChartYDataColumn','Hide');
				ShowElement(this.id,'lblChartYAxisColumn','Show');
				ShowElement(this.id,'lblChartSizeColumn','Hide');
				ShowElement(this.id,'rdAgChartXLabelColumn','Hide');
				ShowElement(this.id,'rdAgChartXDataColumn','Show');
				ShowElement(this.id,'rdAgChartXNumberColumn','Hide');
				ShowElement(this.id,'rdAgChartYAggrLabel','Hide');
				ShowElement(this.id,'rdAgChartSizeColumnAggrLabel','Hide');
				ShowElement(this.id,'rdAgChartYAggrList','Hide');
				ShowElement(this.id,'divChartsGroupByDateOperator','Hide');
				ShowElement(this.id,'rdAgChartXLabelTextColumn','Hide');
				ShowElement(this.id,'rowChartExtraDataColumn','Hide');
				ShowElement(this.id,'rowChartExtraAggr','Hide');
				ShowElement(this.id,'rdAgGaugeMinMax','Hide');
				ShowElement(this.id,'rdAgGaugeGoals','Hide');
				ShowElement(this.id,'lblChartYDataAggregation','Hide');
				ShowElement(this.id,'colGaugeRingsColorSelection','Hide');
                document.rdForm.rdAgChartsDateGroupBy.value='';
				rdAgHideForecast();
				break;
				case 'Gauge':
			   ShowElement(this.id,'lblChartXLabelColumn','Hide');
				ShowElement(this.id,'lblChartXAxisColumn','Hide');
				ShowElement(this.id,'lblChartYDataColumn','Show');
				ShowElement(this.id,'lblChartYAxisColumn','Hide');
				ShowElement(this.id,'lblChartSizeColumn','Hide');
				ShowElement(this.id,'rdAgChartXLabelColumn','Hide');
				ShowElement(this.id,'rdAgChartXDataColumn','Hide');
				ShowElement(this.id,'rdAgChartXNumberColumn','Hide');
				ShowElement(this.id,'rdAgChartYAggrLabel','Show');
				ShowElement(this.id,'rdAgChartSizeColumnAggrLabel','Hide');
				ShowElement(this.id,'rdAgChartYAggrList','Show');
				ShowElement(this.id,'rdAgChartXLabelTextColumn','Hide');
				ShowElement(this.id,'rowChartExtraDataColumn','Hide');
				ShowElement(this.id,'rowChartExtraAggr','Hide');
				ShowElement(this.id,'rdAgGaugeMinMax','Show');
				ShowElement(this.id,'rdAgGaugeGoals','Show');
				ShowElement(this.id,'lblChartYDataAggregation','Hide');
				ShowElement(this.id,'colGaugeRingsColorSelection','Hide');
				ShowGaugeRingsColorSelection();
				document.rdForm.rdAgChartsDateGroupBy.value='';
				rdAgHideForecast();
				break;
			default:  //Line,Spline
				ShowElement(this.id,'lblChartXLabelColumn','Hide');
				ShowElement(this.id,'lblChartXAxisColumn','Show');
				ShowElement(this.id,'lblChartYDataColumn','Hide');
				ShowElement(this.id,'lblChartYAxisColumn','Show');
				ShowElement(this.id,'lblChartSizeColumn','Hide');
				ShowElement(this.id,'rdAgChartXLabelColumn','Hide');
				ShowElement(this.id,'rdAgChartXDataColumn','Show');
				ShowElement(this.id,'rdAgChartXNumberColumn','Hide');
				ShowElement(this.id,'rdAgChartYAggrLabel','Show');
				ShowElement(this.id,'rdAgChartSizeColumnAggrLabel','Hide');
				ShowElement(this.id,'rdAgChartYAggrList','Show');
				ShowElement(this.id,'rdAgChartXLabelTextColumn','Hide');
				ShowElement(this.id,'rowChartExtraDataColumn','Hide');
				ShowElement(this.id,'rowChartExtraAggr','Hide');
				ShowElement(this.id,'rdAgGaugeMinMax','Hide');
				ShowElement(this.id,'rdAgGaugeGoals','Hide');
				ShowElement(this.id,'lblChartYDataAggregation','Hide');
				ShowElement(this.id,'colGaugeRingsColorSelection','Hide');
                rdAgGetChartsGroupByDateOperatorDiv();
                if(bForecast){
                    document.getElementById('rowChartForecast').style.display = '';
                    document.getElementById('IslAgForecastType').style.display = '';
                    document.getElementById('rdAgChartForecastLabel').style.display = ''		        
                    rdAgModifyTimeSeriesCycleLengthOptions(document.getElementById('rdAgChartsDateGroupBy'));    
                    rdAgModifyForecastOptions(document.getElementById('rdAgChartXDataColumn').value);
                    rdAgShowForecastOptions();
                }
				break;
	}
	//Highlight the selected chart.
	document.getElementById('lblChartAddPie').className = "rdAgCommand"
	document.getElementById('lblChartAddBar').className = "rdAgCommand"
	document.getElementById('lblChartAddLine').className = "rdAgCommand"
	document.getElementById('lblChartAddSpline').className = "rdAgCommand"
	document.getElementById('lblChartAddScatter').className = "rdAgCommand"
	if(document.getElementById('lblChartAddHeatmap') != null){
	    document.getElementById('lblChartAddHeatmap').className = "rdAgCommand"
	}
	var eleSelectedCommand = document.getElementById('lblChartAdd' + sChartType)
	eleSelectedCommand.className = "rdAgCommand rdAgCommandHightlight"
	
}

function rdAgColumnMove(sID, nRow, nDirection) {
	var eleThis = document.getElementById('lblColIndent_Row' + nRow)
	var sThisIndent = eleThis.innerHTML
	var sOtherIndent
	if (nDirection<0) {
		if (sThisIndent.length==0) {
			return
		}
		sOtherIndent = sThisIndent.substr(0,sThisIndent.length-1)  //Take off one ".".
	} else {
		sOtherIndent = sThisIndent + '.'
	}
	//Find the Other column with the new indent.
	var eleOther
	var eleTest
	var i
	for (i=1; i<=100; i++) {
		eleTest = document.getElementById('lblColIndent_Row' + i)
		if (!eleTest) {
			break
		}
		if (eleTest.innerHTML==sOtherIndent) {
			eleOther = eleTest
			break
		}
	}
	
	//Switch indents.
	if (eleOther) {
		var sThis = eleThis.innerHTML 
		var sOther = eleOther.innerHTML 
		eleThis.innerHTML = sOther
		eleOther.innerHTML = sThis
		
		//Save the indent so it goes back to the server.
		var eleColMoves = document.getElementById('rdAgColMoves')
		eleColMoves.value = eleColMoves.value + sID + ',' + nDirection + ';'
	}
	
}

function rdAgShowPickDistinctButton() {       
    // Function gets called on the onchange event of the Filter column dropdown.           
    rdAgRemoveAllWhiteSpaceNodesFromFilterOperatorDropdown()        // Do this to clear the FilterOparator dropdown off all whitespace/text nodes.
    ShowElement(this.id,'divPickDistinct','Show')
    var i = 0;
    if (document.rdForm.rdAgFilterColumn.value!=""){
         //Dates
        if((document.rdForm.rdAgPickDateColumns.value.indexOf(document.rdForm.rdAgFilterColumn.value + ",")!=-1) | (document.rdForm.rdAgPickDateColumns.value.indexOf(document.rdForm.rdAgFilterColumn.value +"-NoCalendar" + ",")!=-1 )){
            // Manipulate the DataColumn Dropdown.       
            if(document.rdForm.rdAgFilterOperator.lastChild.value == 'Date Range'){    // condition specific for a fresh dropdown.
                for(i=1;i<=1;i++){
                    rdFilterNewComparisonOptionsArray.push(document.rdForm.rdAgFilterOperator.lastChild);    // remove the new Comparison option 'Date Range'.
                    document.rdForm.rdAgFilterOperator.removeChild(document.rdForm.rdAgFilterOperator.lastChild); 
                }  
                if(document.rdForm.rdAgFilterOperator.lastChild.value == 'Contains'){    //remove the options 'Starts With' and 'Contains'.
                    for (i=0;i<=1;i++){
                        rdFilterOldComparisonOptionsArray.push(document.rdForm.rdAgFilterOperator.lastChild);
                        document.rdForm.rdAgFilterOperator.removeChild(document.rdForm.rdAgFilterOperator.lastChild); 
                    }
                }
                 for(i=1;i<=1;i++){ // Add the new Comparison option 'Date Range' back.
                    document.rdForm.rdAgFilterOperator.appendChild(rdFilterNewComparisonOptionsArray.pop());
                }  
            }
            else{   // condition specific for an already manipulated dropdown.
               if(document.rdForm.rdAgFilterOperator.lastChild.value == 'Contains'){    //remove the options 'Starts With' and 'Contains'.
                    for (i=0;i<=1;i++){
                        rdFilterOldComparisonOptionsArray.push(document.rdForm.rdAgFilterOperator.lastChild);
                        document.rdForm.rdAgFilterOperator.removeChild(document.rdForm.rdAgFilterOperator.lastChild); 
                    }
                }
                for(i=1;i<=1;i++){ // Add the new Comparison option 'Date Range' back.
                    document.rdForm.rdAgFilterOperator.appendChild(rdFilterNewComparisonOptionsArray.pop());
                }
            }
            rdAgManipulateFilterInputTextBoxValuesForDateColumns(document.rdForm.rdAgFilterColumn.value, document.rdForm.rdAgFilterOperator.value, document.rdForm.rdAgCurrentFilterValue.value, document.rdForm.rdAgCurrentDateType.value);
            return;
        }
        // Distinct values popup.
        else if(document.rdForm.rdAgPickDistinctColumns.value.indexOf(document.rdForm.rdAgFilterColumn.value + ",")!=-1){
            if(document.rdForm.rdAgFilterOperator.lastChild.value == 'Date Range'){
                for(i=1;i<=1;i++){
                    rdFilterNewComparisonOptionsArray.push(document.rdForm.rdAgFilterOperator.lastChild);    // remove the new Comparison option.
                    document.rdForm.rdAgFilterOperator.removeChild(document.rdForm.rdAgFilterOperator.lastChild); 
                }  
                if(document.rdForm.rdAgFilterOperator.lastChild.value != 'Contains'){    // condition specific for an already manipulated dropdown.
                    for (i=0;i<=1;i++){
                        document.rdForm.rdAgFilterOperator.appendChild(rdFilterOldComparisonOptionsArray.pop());
                    }
                }
            }
            var elePopupIFrame = document.getElementById('subPickDistinct')
            var sSrc = elePopupIFrame.getAttribute("HiddenSource")
            //Put the picked column name into the URL.
            var nStart = sSrc.indexOf("&rdAgDataColumn=")
            var nEnd = sSrc.indexOf("&", nStart + 1)
            sSrc = sSrc.substr(0,nStart) + "&rdAgDataColumn=" + encodeURI(document.rdForm.rdAgFilterColumn.value) + sSrc.substr(nEnd)
            elePopupIFrame.setAttribute("HiddenSource", sSrc)
            //elePopupIFrame.setAttribute("HiddenSource", elePopupIFrame.getAttribute("HiddenSource").replace("rdPickDataColumn",encodeURI(document.rdForm.rdAgFilterColumn.value)))
            rdAgHideAllFilterDivs()
            ShowElement(this.id,'divPickDistinct','Show')
            ShowElement(this.id,'divPickDistinctPopUpButton','Show')
            elePopupIFrame.removeAttribute("src") //Clear the list so it's rebuilt when the user clicks.
            //15311
            //rdAgManipulateFilterInputTextBoxValuesForDateColumns(document.rdForm.rdAgFilterColumn.value, document.rdForm.rdAgFilterOperator.value, document.rdForm.rdAgCurrentFilterValue.value, document.rdForm.rdAgCurrentDateType.value);
            return;
        }
        else{                      
            rdAgHideAllFilterDivs()
            ShowElement(this.id,'divPickDistinct','Show')     
            if(document.rdForm.rdAgFilterOperator.lastChild.value != 'Contains'){
                if(document.rdForm.rdAgFilterOperator.lastChild.value == 'Date Range'){
                    for(i=1;i<=1;i++){
                    rdFilterNewComparisonOptionsArray.push(document.rdForm.rdAgFilterOperator.lastChild);    // remove the new Comparison option 'Date Range'.
                    document.rdForm.rdAgFilterOperator.removeChild(document.rdForm.rdAgFilterOperator.lastChild); 
                    } 
                }
                if(rdFilterOldComparisonOptionsArray[0]){                    
                    for(i=0;i<=1;i++){
                        document.rdForm.rdAgFilterOperator.appendChild(rdFilterOldComparisonOptionsArray.pop());
                    }
                }
            }            
        }
    }
    else{   // When no column is selected.
        rdAgHideAllFilterDivs()
        ShowElement(this.id,'divPickDistinct','Show')    
        document.rdForm.rdAgFilterOperator.value = "="; 
    }
}
function rdAgSetPickedFilterValue(nPickListRowNr) {
    var fraPopup = document.getElementById("subPickDistinct")
    var eleValue = fraPopup.contentWindow.document.getElementById("lblFilter_Row" + nPickListRowNr)
    var sValue
    if (eleValue.textContent) {
        sValue = eleValue.textContent //Mozilla
    }else{
         sValue = eleValue.innerText  //IE
    }
    document.rdForm.rdAgFilterValue.value = sValue
}

function rdAgPickProperElementDiv(){
    // Function used to regulate the hiding/unhiding of the Divs containing the InputDate elements, called on the onchange event of the filter operator(values like <, <= etc) dropdown.
    if(document.rdForm.rdAgFilterColumn.value){
        rdAgShowProperElementDiv(document.rdForm.rdAgFilterColumn.value, document.rdForm.rdAgFilterOperator.value)
    } 
}

function rdAgManipulateFilterOptionsDropdownForDateColumns(sFilterColumn, sFilterOperator, sFilterValue){
    // Function gets called when the filter link (with the filter info displayed above the data table) displayed is clicked to set the drop down values.  
    rdAgRemoveAllWhiteSpaceNodesFromFilterOperatorDropdown();      
    document.rdForm.rdAgFilterColumn.value = sFilterColumn;
    var i = 0;
    if(document.rdForm.rdAgFilterColumn.value){
        if((document.rdForm.rdAgPickDateColumns.value.indexOf(document.rdForm.rdAgFilterColumn.value + ",")!=-1)|(document.rdForm.rdAgPickDateColumns.value.indexOf(document.rdForm.rdAgFilterColumn.value +"-NoCalendar" + ",")!=-1)){
            if(document.rdForm.rdAgFilterOperator.lastChild.value == 'Date Range'){
                for(i=1;i<=1;i++){
                    rdFilterNewComparisonOptionsArray.push(document.rdForm.rdAgFilterOperator.lastChild);    // remove the new Comparison option 'Date Range'.
                    document.rdForm.rdAgFilterOperator.removeChild(document.rdForm.rdAgFilterOperator.lastChild); 
                }  
                if(document.rdForm.rdAgFilterOperator.lastChild.value == 'Contains'){    //remove the options 'Starts With' and 'Contains'.
                    for (i=0;i<=1;i++){
                        rdFilterOldComparisonOptionsArray.push(document.rdForm.rdAgFilterOperator.lastChild);
                        document.rdForm.rdAgFilterOperator.removeChild(document.rdForm.rdAgFilterOperator.lastChild); 
                    }
                }
                for(i=1;i<=1;i++){ // Add the new Comparison option 'Date Range' back.
                    document.rdForm.rdAgFilterOperator.appendChild(rdFilterNewComparisonOptionsArray.pop());
                }  
            }
            else{
               if(document.rdForm.rdAgFilterOperator.lastChild.value == 'Contains'){    //remove the options 'Starts With' and 'Contains'.
                    for (i=0;i<=1;i++){
                        rdFilterOldComparisonOptionsArray.push(document.rdForm.rdAgFilterOperator.lastChild);
                        document.rdForm.rdAgFilterOperator.removeChild(document.rdForm.rdAgFilterOperator.lastChild); 
                    }
                }
                for(i=1;i<=1;i++){ // Add the new Comparison option 'Date Range' back.
                    document.rdForm.rdAgFilterOperator.appendChild(rdFilterNewComparisonOptionsArray.pop());
                }  
            }
            document.rdForm.rdAgFilterColumn.value = "Date Range"           
        }
       else if(document.rdForm.rdAgFilterOperator.lastChild.value != 'Contains'){   // Code path executed for putting the original options back.
            if(document.rdForm.rdAgFilterOperator.lastChild.value == 'Date Range'){
                for(i=1;i<=1;i++){
                    rdFilterNewComparisonOptionsArray.push(document.rdForm.rdAgFilterOperator.lastChild);    // remove the new Comparison option.
                    document.rdForm.rdAgFilterOperator.removeChild(document.rdForm.rdAgFilterOperator.lastChild); 
                }  
                if(document.rdForm.rdAgFilterOperator.lastChild.value != 'Contains'){    //Add the original options back.
                    for (i=0;i<=1;i++){
                        document.rdForm.rdAgFilterOperator.appendChild(rdFilterOldComparisonOptionsArray.pop());
                    }
                }
            }  
        }
    }   
}

function rdAgHideAllFilterDivs(){
    // Function hides all the Divs mentioned below used to seperate elements that are used in specific conditions under the Filters section.
        ShowElement(this.id,'divPickDistinct','Hide')                               // Div holds a common TextBox.

        ShowElement(this.id,'divPickDistinctPopUpButton','Hide')                    // Div holds the popup button that pulls up the list of ID's, like CustomerID, OrderID etc. This above div is always hidden for Date Time Columns.   
                                                                                                                                                                                                                                       
        ShowElement(this.id,'divSlidingTimeStartDateFilterOpearator','Hide')        // Div holds a dropdown with the sliding time filter operatior values like Sliding Date etc.
                                                          
        ShowElement(this.id,'divSlidingTimeEndDateFilterOpearator','Hide')          // Div holds a dropdown with the sliding time filter operatior values.
                                                          
        ShowElement(this.id,'divSlidingTimeStartDateFilterOpearatorValues','Hide')  // Div holds a dropdown with the sliding time filter operatior option values like Today, Yesterday etc.
                                                        
        ShowElement(this.id,'divSlidingTimeEndDateFilterOpearatorValues','Hide')    // Div holds a dropdown with the sliding time filter operatior option values like Today, Yesterday etc.
        
        ShowElement(this.id,'divFilterStartDateCalendar','Hide')        // Div holds a calendar control for start date.
        
        ShowElement(this.id,'divFilterEndDateCalendar','Hide')           // Div holds a calendar control for start date.
        
        ShowElement(this.id,'divFilterStartDateTextbox','Hide')          // Div holds a textbox for start date.
        
        ShowElement(this.id,'divFilterEndDateTextbox','Hide')            // Div holds a textbox for end date.
}

function rdAgRemoveAllWhiteSpaceNodesFromFilterOperatorDropdown(){
    // Function removes all the unnecessary text/WhiteSpace nodes from the dropdown which cause issues with different browsers.
    var elerdAgFilterOperator = document.rdForm.rdAgFilterOperator;
    if(elerdAgFilterOperator){
        for(i=0; i<= elerdAgFilterOperator.childNodes.length; i++){
        if(elerdAgFilterOperator.childNodes[i]) 
            if(elerdAgFilterOperator.childNodes[i].nodeName == '#text')
                elerdAgFilterOperator.removeChild(elerdAgFilterOperator.childNodes[i])
        }
    }
}

function rdAgShowProperElementDiv(sFilterColumn, sFilterOperator){
    // Function runs on clicking the filter link with the filter info to show the proper panel/Div.
    if(sFilterColumn){    
        if(document.rdForm.rdAgPickDateColumns.value.indexOf(sFilterColumn + ",")!=-1){
            if(sFilterOperator == 'Date Range'){
                rdAgHideAllFilterDivs()
                ShowElement(this.id,'divSlidingTimeStartDateFilterOpearator','Show')
                ShowElement(this.id,'divSlidingTimeEndDateFilterOpearator','Show')
                if(document.rdForm.rdAgSlidingTimeStartDateFilterOpearator.value == 'Specific Date'){ 
                    ShowElement(this.id,'divFilterStartDateCalendar','Show')
                    ShowElement(this.id,'divSlidingTimeStartDateFilterOpearatorValues','Hide')
                }
                if(document.rdForm.rdAgSlidingTimeEndDateFilterOpearator.value == 'Specific Date'){
                    ShowElement(this.id,'divFilterEndDateCalendar','Show')
                    ShowElement(this.id,'divSlidingTimeEndDateFilterOpearatorValues','Hide')
                }                        
                if(document.rdForm.rdAgSlidingTimeStartDateFilterOpearator.value == 'Sliding Date'){
                    ShowElement(this.id,'divSlidingTimeStartDateFilterOpearatorValues','Show')
                    ShowElement(this.id,'divFilterStartDateCalendar','Hide')
                }
                if(document.rdForm.rdAgSlidingTimeEndDateFilterOpearator.value == 'Sliding Date'){
                    ShowElement(this.id,'divSlidingTimeEndDateFilterOpearatorValues','Show')
                    ShowElement(this.id,'divFilterEndDateCalendar','Hide')
                }                   
            }
            else{
                rdAgHideAllFilterDivs()
                ShowElement(this.id,'divSlidingTimeStartDateFilterOpearator','Show')
                if(document.rdForm.rdAgSlidingTimeStartDateFilterOpearator.value == 'Specific Date'){
                    ShowElement(this.id,'divFilterStartDateCalendar','Show')
                    ShowElement(this.id,'divSlidingTimeStartDateFilterOpearatorValues','Hide')
                }
                else{               
                    ShowElement(this.id,'divFilterStartDateCalendar','Hide')     
                    ShowElement(this.id,'divSlidingTimeStartDateFilterOpearatorValues','Show')
                }
            }   
        }
        else if(document.rdForm.rdAgPickDateColumns.value.indexOf(sFilterColumn +"-NoCalendar" + ",")!=-1){
           if(sFilterOperator == 'Date Range'){
                rdAgHideAllFilterDivs()
                ShowElement(this.id,'divSlidingTimeStartDateFilterOpearator','Show')
                ShowElement(this.id,'divSlidingTimeEndDateFilterOpearator','Show')
                if(document.rdForm.rdAgSlidingTimeStartDateFilterOpearator.value == 'Specific Date'){ 
                    ShowElement(this.id,'divFilterStartDateTextbox','Show')
                    ShowElement(this.id,'divSlidingTimeStartDateFilterOpearatorValues','Hide')
                }
                if(document.rdForm.rdAgSlidingTimeEndDateFilterOpearator.value == 'Specific Date'){
                    ShowElement(this.id,'divFilterEndDateTextbox','Show')
                    ShowElement(this.id,'divSlidingTimeEndDateFilterOpearatorValues','Hide')
                }
                if(document.rdForm.rdAgSlidingTimeStartDateFilterOpearator.value == 'Sliding Date'){
                    ShowElement(this.id,'divSlidingTimeStartDateFilterOpearatorValues','Show')
                    ShowElement(this.id,'divFilterStartDateTextbox','Hide')
                }
                if(document.rdForm.rdAgSlidingTimeEndDateFilterOpearator.value == 'Sliding Date'){
                    ShowElement(this.id,'divSlidingTimeEndDateFilterOpearatorValues','Show')
                    ShowElement(this.id,'divFilterEndDateTextbox','Hide')
                }    
            }
            else{
                rdAgHideAllFilterDivs()
                 ShowElement(this.id,'divSlidingTimeStartDateFilterOpearator','Show')
                 if(document.rdForm.rdAgSlidingTimeStartDateFilterOpearator.value == 'Specific Date'){ 
                    ShowElement(this.id,'divFilterStartDateTextbox','Show')
                    ShowElement(this.id,'divSlidingTimeStartDateFilterOpearatorValues','Hide')
                }
                if(document.rdForm.rdAgSlidingTimeStartDateFilterOpearator.value == 'Sliding Date'){
                    ShowElement(this.id,'divSlidingTimeStartDateFilterOpearatorValues','Show')
                    ShowElement(this.id,'divFilterStartDateTextbox','Hide')
                }
            }
        }
        else{  // When filter column is not a Date column.
            if(document.rdForm.rdAgPickDistinctColumns.value.indexOf(document.rdForm.rdAgFilterColumn.value + ",")!=-1){
                rdAgHideAllFilterDivs()
                ShowElement(this.id,'divPickDistinct','Show')
                ShowElement(this.id,'divPickDistinctPopUpButton','Show')}
            else{
                rdAgHideAllFilterDivs()
                ShowElement(this.id,'divPickDistinct','Show')
            }
        }
    }
}

function rdAgManipulateFilterInputTextBoxValuesForDateColumns(sFilterColumn, sFilterOperator, sFilterValue, sDateType, sSlidingDateName){
    // Function runs to set the values of the filter into the input text boxes.
    document.rdForm.rdAgFilterColumn.value = sFilterColumn;
    if(sFilterOperator.indexOf('&lt;') > -1)  sFilterOperator = sFilterOperator.replace('&lt;','<');    //#17188.
    document.rdForm.rdAgFilterOperator.value = sFilterOperator;
    if(rdAgCheckForElements('rdAgCurrentFilterValue'))
        document.rdForm.rdAgCurrentFilterValue.value = sFilterValue;
    var sDateTypeOperator
    var sSlidingDateValue
    var sInputElementValue = sFilterValue.split('|')[0];
    if (sFilterValue) {
        document.rdForm.rdAgFilterStartDate.value = sInputElementValue;
        document.rdForm.rdAgFilterStartDateTextbox.value = sInputElementValue;
        document.rdForm.rdAgFilterValue.value = sInputElementValue; 
        document.rdForm.rdAgFilterEndDate.value = '';
        document.rdForm.rdAgFilterEndDateTextbox.value = '';
        if(sDateType){
            sDateTypeOperator = sDateType.split(',')[0]
            if(sDateTypeOperator)
                document.rdForm.rdAgSlidingTimeStartDateFilterOpearator.value = sDateTypeOperator
        }
        else{
            document.rdForm.rdAgSlidingTimeStartDateFilterOpearator.value = "Specific Date"
        }
        if(sSlidingDateName){
            sSlidingDateValue = sSlidingDateName.split(',')[0]
            if(sSlidingDateValue)
                document.rdForm.rdAgSlidingTimeStartDateFilterOpearatorOptions.value = sSlidingDateValue
        }  
        if(sFilterValue.split('|')[1]){
            sInputElementValue = sFilterValue.split('|')[1];
            document.rdForm.rdAgFilterEndDate.value = sInputElementValue;
            document.rdForm.rdAgFilterEndDateTextbox.value = sInputElementValue;
            if(sDateType){
                sDateTypeOperator = sDateType.split(',')[1]
                if(sDateTypeOperator)
                    document.rdForm.rdAgSlidingTimeEndDateFilterOpearator.value = sDateTypeOperator
            }
            else{
                document.rdForm.rdAgSlidingTimeEndDateFilterOpearator.value = "Specific Date"
            } 
            if(sSlidingDateName){
                sSlidingDateValue = sSlidingDateName.split(',')[1]
                if(sSlidingDateValue)
                    document.rdForm.rdAgSlidingTimeEndDateFilterOpearatorOptions.value = sSlidingDateValue
            } 
        }
    }
    rdAgShowProperElementDiv(sFilterColumn, sFilterOperator)    // Run through this function to show hide the divs
}

function rdAgGetGroupByDateOperatorDiv(){
    // Function used by the Grouping division for hiding/unhiding the GroupByOperator Div.
    if((document.rdForm.rdAgPickDateColumnsForGrouping.value.indexOf(document.rdForm.rdAgGroupColumn.value + ",")!=-1) && (document.rdForm.rdAgGroupColumn.value.length != 0)){
        if(rdAgCheckForElements('divGroupByDateOperator'))
            ShowElement(this.id,'divGroupByDateOperator','Show');
    }
    else{
        if(rdAgCheckForElements('divGroupByDateOperator')){
            ShowElement(this.id,'divGroupByDateOperator','Hide');
            document.rdForm.rdAgDateGroupBy.value='';
        }
    }
}

function rdAgGetChartsGroupByDateOperatorDiv(){
    // Function used by the Charts division for hiding/unhiding the GroupByOperator Div for the Charts except for Pie and Scatter.
    var sChartType = document.rdForm.rdAgChartType.value;
    if(sChartType == 'Pie' || sChartType == 'Bar'){
        if((document.rdForm.rdAgPickDateColumnsInChartForGrouping.value.indexOf(document.rdForm.rdAgChartXLabelColumn.value + ",")!=-1) && (document.rdForm.rdAgChartXLabelColumn.value.length != 0)){
            ShowElement(this.id,'divChartsGroupByDateOperator','Show');
        }else{
            ShowElement(this.id,'divChartsGroupByDateOperator','Hide');
            document.rdForm.rdAgChartsDateGroupBy.value='';
        }
    }else if(sChartType == 'Line' || sChartType == 'Spline'){
        if((document.rdForm.rdAgPickDateColumnsInChartForGrouping.value.indexOf(document.rdForm.rdAgChartXDataColumn.value + ",")!=-1) && (document.rdForm.rdAgChartXDataColumn.value.length != 0)){
            ShowElement(this.id,'divChartsGroupByDateOperator','Show');
        }else{
            ShowElement(this.id,'divChartsGroupByDateOperator','Hide');
            document.rdForm.rdAgChartsDateGroupBy.value='';
        }
    }else{
        ShowElement(this.id,'divChartsGroupByDateOperator','Hide');
        document.rdForm.rdAgChartsDateGroupBy.value='';
    }    
}

function rdAgGetCrosstabHeaderGroupByDateOperatorDiv(){
 // Function used by the Crosstabs division for hiding/unhiding the GroupByOperator Div for the header Column dropdown.
    if((document.rdForm.rdAgPickDateColumnsInCrossTabForGrouping.value.indexOf(document.rdForm.rdAgCrosstabHeaderColumn.value + ",")!=-1) && (document.rdForm.rdAgCrosstabHeaderColumn.value.length != 0)){
        if(rdAgCheckForElements('divCrosstabHeaderGroupByDateOperator'))
            ShowElement(this.id,'divCrosstabHeaderGroupByDateOperator','Show');
    }
    else{
        if(rdAgCheckForElements('divCrosstabHeaderGroupByDateOperator')){
            ShowElement(this.id,'divCrosstabHeaderGroupByDateOperator','Hide');
            document.rdForm.rdAgCrosstabHeaderDateGroupBy.value='';
        }
    }
}

function rdAgGetCrosstabLabelGroupByDateOperatorDiv(){
 // Function used by the Crosstabs division for hiding/unhiding the GroupByOperator Div for the Label Column dropdown.
    if((document.rdForm.rdAgPickDateColumnsInCrossTabForGrouping.value.indexOf(document.rdForm.rdAgCrosstabLabelColumn.value + ",")!=-1) && (document.rdForm.rdAgCrosstabLabelColumn.value.length != 0)){
        if(rdAgCheckForElements('divCrosstabLabelGroupByDateOperator'))
            ShowElement(this.id,'divCrosstabLabelGroupByDateOperator','Show');
    }
    else{
        if(rdAgCheckForElements('divCrosstabLabelGroupByDateOperator')){
            ShowElement(this.id,'divCrosstabLabelGroupByDateOperator','Hide');
            document.rdForm.rdAgCrosstabLabelDateGroupBy.value='';
        }
    }
}

function rdAgCheckForElements(sElementId){
    // Function to check if the element exists.
    if (document.getElementById(sElementId))
        return true;
    else
        return false;
}

//#Start Region Draggable Panels

function rdInitDraggableAgPanels(){
    var bDraggableAgPanels = false;
    var eleDraggableAgPanels = document.getElementById('rdAgDraggablePanels');
    if (eleDraggableAgPanels!= null) bDraggableAgPanels = true;
//    for (var i=0; i < aDraggableAgPanels.length; i++) {
//        var eleAgPanel = aDraggableAgPanels[i];
//        if(bDraggableAgPanels){
//            var dd = new YAHOO.rd.DDRegion(eleAgPanel.id, '', { cont: 'rdDivAgPanels', panels: aDraggableAgPanels});
//            var elePanelHeaderId = (YAHOO.util.Dom.getElementsByClassName('rdAgContentHeadingRow', 'table', eleAgPanel).length == 0 ? 
//                                    YAHOO.util.Dom.getElementsByClassName('rdAgContentHeading', 'td', eleAgPanel)[0].id : 
//                                    YAHOO.util.Dom.getElementsByClassName('rdAgContentHeadingRow', 'table', eleAgPanel)[0].id)
//            dd.setHandleElId(elePanelHeaderId);
//            document.getElementById(dd.handleElId).style.cursor = 'move';
//            dd.getDragEl().style.cursor = 'auto';
//        }
//    }    

  YUI().use('dd-drop-plugin', 'dd-plugin', 'dd-scroll', 'dd-constrain', function(Y) {	
  		var bIsTouchDevice = false;
        try {
            document.createEvent('TouchEvent');	
            bIsTouchDevice = true;	
        }catch(e){}	
        var aDraggableAgPanels = rdGetDraggableAgPanels();
        for (var i=0; i < aDraggableAgPanels.length; i++) {
            var eleAgPanel = aDraggableAgPanels[i];
            if(bDraggableAgPanels){
            
                var pnlNode =  Y.one('#' + eleAgPanel.id);
				var pnlDD = new Y.DD.Drag( {
				    node: pnlNode
				}).plug(Y.Plugin.DDConstrained, {
					stickY: true
				});				
				var pnlDrop = pnlNode.plug(Y.Plugin.Drop);
				
				var pnlDragged = null;
				var originalPanelPosition = [0,0]; 
				var bDoNothingMore = false; 
				var eleTargetPanel = null;  
				
				pnlDD.on('drag:start', function(e) {
				    pnlDragged = this.get('dragNode').getDOMNode();
                    rdSetDraggableAgPanelsZIndex(pnlDragged, this.panels);
                    Y.DOM.setStyle(pnlDragged, "opacity", '.65');
                    originalPanelPosition = Y.DOM.getXY(pnlDragged);
                    bDoNothingMore = false;
                    eleTargetPanel = null;  
				});		
				
				pnlDD.on('drag:drag', function(e) {                  
				    rdNeutralizeDropZoneColor();       
                    if(eleTargetPanel){               
                        if(eleTargetPanel.id.match('rdDivAgPanelWrap_')) {
                            var regionDraggedPanel = Y.DOM.region(pnlDragged);
                            var regionTargetPanel = Y.DOM.region(eleTargetPanel);
                            var nTargetPanelHeight = regionTargetPanel.height; 
                            eleTargetPanelHandle = eleTargetPanel.nextSibling;
                            if(originalPanelPosition[1] < regionDraggedPanel.top){
                                if(regionDraggedPanel.top > (regionTargetPanel.top + Math.round(nTargetPanelHeight/2))){
                                     eleTargetPanel.nextSibling.firstChild.firstChild.firstChild.className = 'rdAgDropZoneActive';
                                }else{
                                     eleTargetPanel.previousSibling.firstChild.firstChild.firstChild.className = 'rdAgDropZoneActive';
                                }
                            }else{
                                 if(regionDraggedPanel.top < (regionTargetPanel.top + Math.round(nTargetPanelHeight/2))){
                                    eleTargetPanel.previousSibling.firstChild.firstChild.firstChild.className = 'rdAgDropZoneActive';                             
                                }else{
                                    eleTargetPanel.nextSibling.firstChild.firstChild.firstChild.className = 'rdAgDropZoneActive';  
                                }
                            }
                        } 
                    }else{
                        pnlDragged.previousSibling.firstChild.firstChild.firstChild.className = 'rdAgDropZoneActive';
                    }
				});	
				
				pnlDD.on('drag:over', function(e) {
				   eleTargetPanel = e.drop.get('node').getDOMNode();
				});
				
				pnlDD.on('drag:drophit', function(e) {		
				    rdMoveAgPanels(pnlDragged, eleTargetPanel, originalPanelPosition, bDoNothingMore);		    
                    pnlDragged.style.cssText = '';
                    Y.DOM.setStyle(pnlDragged, "opacity", '1');
                    bDoNothingMore = true;
				});	
				
				pnlDD.on('drag:end', function(e) {
				    rdMoveAgPanels(pnlDragged, eleTargetPanel, originalPanelPosition, bDoNothingMore);
                    pnlDragged.style.cssText = '';
                    Y.DOM.setStyle(pnlDragged, "opacity", '1');
                    if(bIsTouchDevice) setTimeout(function(){rdResetAGPanelAfterDDScroll(pnlDragged)}, 1000);  // Do this for the Tablet only, #15364.
				});	
				
				var elePanelHeaderId = (Y.Selector.query('table.rdAgContentHeadingRow', eleAgPanel).length == 0 ?
				                        Y.Selector.query('td.rdAgContentHeading', eleAgPanel)[0].id :
				                        Y.Selector.query('table.rdAgContentHeadingRow', eleAgPanel)[0].id);
				                        
				var pnlTitleNode = Y.one('#' + elePanelHeaderId);						
				pnlDD.addHandle('#' + elePanelHeaderId ).plug(Y.Plugin.DDWinScroll, {horizontal:false, vertical:true, scrollDelay:100, windowScroll:true});					
				pnlTitleNode.setStyle('cursor', 'move');							
            }
        }
    });
}

function rdResetAGPanelAfterDDScroll(elePnlDragged){
    
	var pnlDragged = Y.one(elePnlDragged);
	pnlDragged.setStyles({
		left:0,
		top:0        
	});    
}

function rdMoveAgPanels(eleDraggedPanel, eleTargetPanel, originalPanelPosition, bDoNothing){
    
	if(!bDoNothing){
		if(eleTargetPanel){
			if(eleTargetPanel.id.match('rdDivAgPanelWrap_')) {
				var regionDraggedPanel = Y.DOM.region(eleDraggedPanel);
				var regionTargetPanel = Y.DOM.region(eleTargetPanel);
				var nTargetPanelHeight = regionTargetPanel.height; 
				var eleTargetPanelHandle = eleTargetPanel.nextSibling;
				var eleDraggedPanelHandle = eleDraggedPanel.nextSibling;
				if(originalPanelPosition[1] < regionDraggedPanel.top){
					if(regionDraggedPanel.top > (regionTargetPanel.top + Math.round(nTargetPanelHeight/2))){
						 if(eleTargetPanelHandle.nextSibling){
							eleTargetPanel.parentNode.insertBefore(eleDraggedPanel, eleTargetPanelHandle.nextSibling);
							eleTargetPanel.parentNode.insertBefore(eleDraggedPanelHandle, eleTargetPanelHandle.nextSibling.nextSibling);                                
						}else{
							 eleTargetPanel.parentNode.appendChild(eleDraggedPanel);
							 eleTargetPanel.parentNode.appendChild(eleDraggedPanelHandle);
						}
					}else{
						eleTargetPanel.parentNode.insertBefore(eleDraggedPanel, eleTargetPanel);
						eleTargetPanel.parentNode.insertBefore(eleDraggedPanelHandle, eleTargetPanel);
					}
					rdSaveDraggableAgPanelPositions()
				}else{
					 if(regionDraggedPanel.top < (regionTargetPanel.top + Math.round(nTargetPanelHeight/2))){
						eleTargetPanel.parentNode.insertBefore(eleDraggedPanel, eleTargetPanel);
						eleTargetPanel.parentNode.insertBefore(eleDraggedPanelHandle, eleTargetPanel);                          
					}else{
						if(eleTargetPanelHandle.nextSibling){
							eleTargetPanel.parentNode.insertBefore(eleDraggedPanel, eleTargetPanelHandle.nextSibling);
							eleTargetPanel.parentNode.insertBefore(eleDraggedPanelHandle, eleTargetPanelHandle.nextSibling.nextSibling);
						}else{
							eleTargetPanel.parentNode.appendChild(eleDraggedPanel);
							eleTargetPanel.parentNode.appendChild(eleDraggedPanelHandle);
						}
					} 
					rdSaveDraggableAgPanelPositions()                      
				}
			}
		}
		else{
			var aDraggableAgPanels = rdGetDraggableAgPanels();
			var regionDraggedPanel = Y.DOM.region(eleDraggedPanel);
			if(originalPanelPosition[1] < regionDraggedPanel.top){
				if(eleDraggedPanel.id != aDraggableAgPanels[aDraggableAgPanels.length-1].id){
					if(regionDraggedPanel.top > Y.DOM.region(aDraggableAgPanels[aDraggableAgPanels.length-1]).bottom){
						aDraggableAgPanels[0].parentNode.appendChild(eleDraggedPanel);
						aDraggableAgPanels[0].parentNode.appendChild(eleDraggedPanelHandle);
						rdSaveDraggableAgPanelPositions()
					}
				}
			}else{
				if(eleDraggedPanel.id != aDraggableAgPanels[0].id){
					if(regionDraggedPanel.top < Y.DOM.region(aDraggableAgPanels[0]).top){
						aDraggableAgPanels[0].parentNode.insertBefore(eleDraggedPanel, aDraggableAgPanels[0]);
						aDraggableAgPanels[0].parentNode.insertBefore(eleDraggedPanelHandle, aDraggableAgPanels[0]);
						rdSaveDraggableAgPanelPositions()
					}
				}
			}
		}
		rdNeutralizeDropZoneColor();
		eleDraggedPanel.style.top = '0px';   
		eleDraggedPanel.style.left = '0px';
	}
    
}
  
function rdSaveDraggableAgPanelPositions(){
    var rdPanelParams = "&rdReport=" + document.getElementById("rdAgReportId").value;
    rdPanelParams += "&rdAgPanelOrder="; 
    var aDraggableAgPanels = rdGetDraggableAgPanels();
    for (var i=0; i < aDraggableAgPanels.length; i++){
        var eleAgPnl = aDraggableAgPanels[i];
        rdPanelParams += eleAgPnl.id.replace('rdDivAgPanelWrap_', '') + ',';
    }
    rdPanelParams += "&rdAgId=" + document.getElementById('rdAgId').value;

    window.status = "Saving dashboard panel positions."
    rdAjaxRequestWithFormVars('rdAjaxCommand=rdAjaxNotify&rdNotifyCommand=UpdateAgPanelOrder' + rdPanelParams);
}


function rdNeutralizeDropZoneColor(){
    
	var aDropZoneTDs = Y.Selector.query('td.rdAgDropZoneActive', Y.DOM.byId('rdDivAgPanels'))
	for (var i=0; i < aDropZoneTDs.length; i++){
		var eleDropZoneTD = aDropZoneTDs[i];
		eleDropZoneTD.className = 'rdAgDropZone'
	}
}

function rdSetDraggableAgPanelsZIndex(eleAgPanel, aDraggableAgPanels){
    
	aDraggableAgPanels = rdGetDraggableAgPanels()
	for (var i=0; i < aDraggableAgPanels.length; i++){
		var eleAgPnl = aDraggableAgPanels[i];
		if(eleAgPnl.id == eleAgPanel.id){
			 Y.DOM.setStyle(eleAgPnl, "zIndex", 1000);
		}else{
			Y.DOM.setStyle(eleAgPnl, "zIndex", 0);
		}           
	}    
}

function rdGetDraggableAgPanels(){
        var aDraggableAgPanels = new Array();
        var eleDivAgPanels = document.getElementById('rdDivAgPanels');
        if(eleDivAgPanels == null) return aDraggableAgPanels; //#16596.
        var aDraggableAgDivs = eleDivAgPanels.getElementsByTagName("div")
        for(i=0;i<aDraggableAgDivs.length;i++){
            var eleDraggableAgDiv = aDraggableAgDivs[i];
            if(eleDraggableAgDiv.id){
                if((eleDraggableAgDiv.id.indexOf('rdDivAgPanelWrap_rowTable') > -1) || 
                   (eleDraggableAgDiv.id.indexOf('rdDivAgPanelWrap_rowChart') > -1) ||
                   (eleDraggableAgDiv.id.indexOf('rdDivAgPanelWrap_rowCrosstab_') > -1)){
                    if(eleDraggableAgDiv.firstChild.firstChild.firstChild.style.display != 'none'){
                        aDraggableAgPanels.push(eleDraggableAgDiv);
                    }
                }
            }
        }
        return aDraggableAgPanels;
}

//#End Region Draggable Panels. 

//#Start Region Ag Forecast.

function rdAgShowForecastOptions(){
    if(document.getElementById('IslAgForecastType') == null) return;
    var eleForecastType = document.getElementById('IslAgForecastType');
    if(eleForecastType.value == 'TimeSeriesDecomposition'){
        if(document.getElementById('rdAgChartsDateGroupBy').value == "FirstDayOfYear"){
            document.getElementById('IslAgTimeSeriesCycleLength').style.display = 'none';
            document.getElementById('IslAgTimeSeriesCycleLength' + '-Caption').style.display = 'none';
        }else{
            document.getElementById('IslAgTimeSeriesCycleLength').style.display = '';
            document.getElementById('IslAgTimeSeriesCycleLength' + '-Caption').style.display = '';
        }
        document.getElementById('IslAgRegressionType').style.display = 'none';
        document.getElementById('IslAgRegressionType'+ '-Caption').style.display = 'none';      
        return;
    }
    else if(eleForecastType.value == 'Regression'){
        var eleRegression = document.getElementById('IslAgRegressionType');
        document.getElementById('IslAgRegressionType').style.display = '';
        document.getElementById('IslAgRegressionType' + '-Caption').style.display = '';
        document.getElementById('IslAgTimeSeriesCycleLength').style.display = 'none';
        document.getElementById('IslAgTimeSeriesCycleLength' + '-Caption').style.display = 'none';
        return;
    }
    else{
        document.getElementById('IslAgTimeSeriesCycleLength').style.display = 'none';
        document.getElementById('IslAgTimeSeriesCycleLength' + '-Caption').style.display = 'none';
        document.getElementById('IslAgRegressionType').style.display = 'none';
        document.getElementById('IslAgRegressionType' + '-Caption').style.display = 'none';
    }   
}

function rdAgHideForecast(){
    // Function hides all forecast related elements.
    if(document.getElementById('IslAgForecastType') == null) return;
    document.getElementById('rowChartForecast').style.display = 'none';
    document.getElementById('IslAgForecastType').style.display = 'none';
    document.getElementById('IslAgForecastType').value = '';
    document.getElementById('rdAgChartForecastLabel').style.display = 'none'
    document.getElementById('IslAgTimeSeriesCycleLength').style.display = 'none';
    document.getElementById('IslAgTimeSeriesCycleLength'+ '-Caption').style.display = 'none';
    document.getElementById('IslAgRegressionType').style.display = 'none';
    document.getElementById('IslAgRegressionType' + '-Caption').style.display = 'none';
}

function rdAgResetForecastValues(){
    // Function resets all forecast related element values.
    if(document.getElementById('IslAgForecastType') == null) return;
    document.getElementById('IslAgForecastType').value = '';
    document.getElementById('IslAgTimeSeriesCycleLength').value = '';
    document.getElementById('IslAgRegressionType').value = 'Linear';
}

function rdAgModifyForecastOptions(sColumn){
    // Function modifies the forecast type dropdown based on the X-Axis column value.
    if(document.rdForm.rdAgChartType.value == "Pie" || document.rdForm.rdAgChartType.value == "Scatter" || document.rdForm.rdAgChartType.value == "Heatmap"){rdAgHideForecast(); return;}   //#16559.
    if(document.getElementById('IslAgForecastType') == null) return;
    if(sColumn == ''){rdAgResetForecastValues(); return;}
    var eleAcDataColumnDetails = document.getElementById('rdAgDataColumnDetails');
    var eleDataForecastDropdown = document.getElementById('IslAgForecastType');
    var sForecastValue = eleDataForecastDropdown.value;
    var eleDateGroupByDropdown = document.getElementById('rdAgChartsDateGroupBy');
    var aForecastValues = ['None', 'TimeSeriesDecomposition', 'Regression']; 
    var aForecastOptions = ['', 'Time Series', 'Regression']; 
    if(eleAcDataColumnDetails){
        if(eleAcDataColumnDetails.value != ''){
            var sDataColumnDetails = eleAcDataColumnDetails.value;
            var aDataColumnDetails = sDataColumnDetails.split(',')
            if(aDataColumnDetails.length > 0){
                var i;
                for(i=0;i<aDataColumnDetails.length;i++){
                    var sDataColumnDetail = aDataColumnDetails[i];
                    if(sDataColumnDetail.length > 1 && sDataColumnDetail.indexOf(':') > -1){
                        var sDataColumn = sDataColumnDetail.split(':')[0];
                        if(sDataColumn == sColumn){
                            var sDataColumnType = sDataColumnDetail.split(':')[1];
                            if(sDataColumnType.toLowerCase() == "text"){
                                rdAgHideForecast();
                                return;
                            }
                            if(sDataColumnType.toLowerCase() != "date" && sDataColumnType.toLowerCase() != "datetime"){
                                if(eleDataForecastDropdown.options[1].value == 'TimeSeriesDecomposition'){
                                    eleDataForecastDropdown.remove(1);
                                }
                                document.getElementById('rowChartForecast').style.display = '';                                    
                                document.getElementById('IslAgForecastType').style.display = '';
                                document.getElementById('rdAgChartForecastLabel').style.display = ''
                                document.getElementById('IslAgTimeSeriesCycleLength').style.display = 'none';
                                document.getElementById('IslAgTimeSeriesCycleLength'+ '-Caption').style.display = 'none';
                            }else{
                                if(eleDataForecastDropdown.options.length < 3){ 
                                    var j;
                                    for(j=0;j<4;j++){
                                        if(eleDataForecastDropdown.options.length > 0){
                                            eleDataForecastDropdown.remove(0);
                                        }
                                    }
                                    var k;
                                    for(k=0;k<aForecastOptions.length;k++){
                                        var eleForecastOption = document.createElement('option');
                                        eleForecastOption.text = aForecastOptions[k];
                                        eleForecastOption.value = aForecastValues[k];
                                        eleDataForecastDropdown.add(eleForecastOption);
                                    }
                                    if(sForecastValue.length > 0){
                                        eleDataForecastDropdown.value = sForecastValue;
                                    }                                   
                                }
                                document.getElementById('rowChartForecast').style.display = '';
                                document.getElementById('IslAgForecastType').style.display = '';
                                document.getElementById('rdAgChartForecastLabel').style.display = ''
                            }
                        }
                    }
                }
            
            }
        }
    }
}

function rdAgModifyTimeSeriesCycleLengthOptions(eleColumnGroupByDropdown){
    // Function modifies the forecast cycle length options based on the Groupby dropdown value.
    if(document.getElementById('IslAgForecastType') == null) return;
    if(document.rdForm.rdAgChartType.value == "Pie" || document.rdForm.rdAgChartType.value == "Scatter" || document.rdForm.rdAgChartType.value == "Heatmap") return;
    var eleTimeSeriesCycleLengthDropdown = document.getElementById('IslAgTimeSeriesCycleLength');
    var sTimeSeriesCycleLength = eleTimeSeriesCycleLengthDropdown.value;
    var sColumnGroupByValue = eleColumnGroupByDropdown.value
    var i; var j = 0; var aColumnGroupByOptions = ['Year', 'Quarter', 'Month', 'Week', 'Day', 'Hour']; 
    rdResetTimeSeriesCycleLenthDropdown();
    switch(sColumnGroupByValue){
        case 'FirstDayOfYear':
         for(i=0;i<7;i++){
                var eleTimeSeriesCycleLengthOption = eleTimeSeriesCycleLengthDropdown.options[j]
                if(eleTimeSeriesCycleLengthOption != null){
                    if(eleTimeSeriesCycleLengthOption.value != ''){
                        eleTimeSeriesCycleLengthDropdown.remove(j);
                    }else{
                        if(eleTimeSeriesCycleLengthOption.value == sTimeSeriesCycleLength){
                            eleTimeSeriesCycleLengthDropdown.value = sTimeSeriesCycleLength;
                        }
                        j += 1;
                    }
                }
            }
            document.getElementById('IslAgTimeSeriesCycleLength').style.display = 'none';
            document.getElementById('IslAgTimeSeriesCycleLength' + '-Caption').style.display = 'none';
            break;
        case 'FirstDayOfQuarter':
        for(i=0;i<7;i++){
                var eleTimeSeriesCycleLengthOption = eleTimeSeriesCycleLengthDropdown.options[j]
                if(eleTimeSeriesCycleLengthOption != null){
                    if(eleTimeSeriesCycleLengthOption.value != '' && eleTimeSeriesCycleLengthOption.value != 'Year'){
                        eleTimeSeriesCycleLengthDropdown.remove(j);
                    }else{
                        if(eleTimeSeriesCycleLengthOption.value == sTimeSeriesCycleLength){
                            eleTimeSeriesCycleLengthDropdown.value = sTimeSeriesCycleLength;
                        }
                        j += 1;
                    }
                }
            }
            if(document.getElementById('IslAgForecastType').value == 'TimeSeriesDecomposition'){
                document.getElementById('IslAgTimeSeriesCycleLength').style.display = '';
                document.getElementById('IslAgTimeSeriesCycleLength' + '-Caption').style.display = '';
            }
            break;
        case 'FirstDayOfMonth':
            for(i=0;i<7;i++){
                var eleTimeSeriesCycleLengthOption = eleTimeSeriesCycleLengthDropdown.options[j]
                if(eleTimeSeriesCycleLengthOption != null){
                    if(eleTimeSeriesCycleLengthOption.value != '' && eleTimeSeriesCycleLengthOption.value != 'Year' && eleTimeSeriesCycleLengthOption.value != 'Quarter'){
                        eleTimeSeriesCycleLengthDropdown.remove(j);
                    }else{
                        if(eleTimeSeriesCycleLengthOption.value == sTimeSeriesCycleLength){
                            eleTimeSeriesCycleLengthDropdown.value = sTimeSeriesCycleLength;
                        }
                        j += 1;
                    }
                }
            }
            if(document.getElementById('IslAgForecastType').value == 'TimeSeriesDecomposition'){
                document.getElementById('IslAgTimeSeriesCycleLength').style.display = '';
                document.getElementById('IslAgTimeSeriesCycleLength' + '-Caption').style.display = '';
            }
            break;
        case 'Date':
            for(i=0;i<7;i++){
                var eleTimeSeriesCycleLengthOption = eleTimeSeriesCycleLengthDropdown.options[j]
                if(eleTimeSeriesCycleLengthOption != null){
                    if(eleTimeSeriesCycleLengthOption.value != '' && eleTimeSeriesCycleLengthOption.value != 'Year' && eleTimeSeriesCycleLengthOption.value != 'Quarter' && eleTimeSeriesCycleLengthOption.value != 'Month' && eleTimeSeriesCycleLengthOption.value != 'Week'){
                        eleTimeSeriesCycleLengthDropdown.remove(j);
                    }else{
                        if(eleTimeSeriesCycleLengthOption.value == sTimeSeriesCycleLength){
                            eleTimeSeriesCycleLengthDropdown.value = sTimeSeriesCycleLength;
                        }
                        j += 1;
                    }
                }
            }
            if(document.getElementById('IslAgForecastType').value == 'TimeSeriesDecomposition'){
                document.getElementById('IslAgTimeSeriesCycleLength').style.display = '';
                document.getElementById('IslAgTimeSeriesCycleLength' + '-Caption').style.display = '';
            }
            break;
    }
    if(eleTimeSeriesCycleLengthDropdown.value == ''){
        eleTimeSeriesCycleLengthDropdown.value = eleTimeSeriesCycleLengthDropdown.options[eleTimeSeriesCycleLengthDropdown.options.length -1].value;
        if(eleTimeSeriesCycleLengthDropdown.options[eleTimeSeriesCycleLengthDropdown.options.length -1].value == "Day"){
            eleTimeSeriesCycleLengthDropdown.value = '';
        }        
    }
}

function rdResetTimeSeriesCycleLenthDropdown(){
    // Function resets the forecast cycle length dropdown.
    if(document.getElementById('IslAgForecastType') == null) return;
    var eleTimeSeriesCycleLengthDropdown = document.getElementById('IslAgTimeSeriesCycleLength');
    var i; var aColumnGroupByOptions = ['', 'Year', 'Quarter', 'Month', 'Week', 'Day']; 
    if(eleTimeSeriesCycleLengthDropdown.options.length >5) return;
    for(i=0;i<7;i++){
        if(eleTimeSeriesCycleLengthDropdown.options.length > 0){
            eleTimeSeriesCycleLengthDropdown.remove(0);
        }else{
            break;
        }
    }
    for(i=0;i<aColumnGroupByOptions.length;i++){
        var eleTimeSeriesOption = document.createElement('option');
        eleTimeSeriesOption.text = aColumnGroupByOptions[i];
        eleTimeSeriesOption.value = aColumnGroupByOptions[i];
        eleTimeSeriesCycleLengthDropdown.add(eleTimeSeriesOption);
    }
}

//#End Region Ag Forecast. 

function rdGetAgGaugeAggregation(){
    if(document.rdForm.rdAgChartType.value != "Gauge") return;
    var sRefreshIDs = document.getElementById("rdAgReportId").value + ',lblChartYDataAggregation,rdAgGaugeAggregationValue,rdAgGaugeAggregationRowCount';
    var rdAgRefreshParams = "&rdReport=" + document.getElementById("rdAgReportId").value;
    rdAgRefreshParams += "&rdAgId=" + document.getElementById('rdAgId').value;
    rdAgRefreshParams += "&rdAgAggregateColumnValue=" + document.getElementById('rdAgChartYColumn').value;
    rdAgRefreshParams += "&rdAgAggregation=" + document.getElementById('rdAgChartYAggrList').value;
    rdAgRefreshParams += "&rdDataCache=" + document.getElementById('rdDataTableDiv-dtAnalysisGrid').getAttribute("rdDataCache");
    rdAgRefreshParams += "&rdAgAggregationText=" + document.getElementById('rdAgChartYAggrList').options[document.getElementById('rdAgChartYAggrList').selectedIndex].text;
    rdAgRefreshParams += "&rdAgGetGaugeAggregation=True";
    rdAjaxRequestWithFormVars("rdAjaxCommand=RefreshElement&rdRefreshElementID=" + sRefreshIDs + rdAgRefreshParams, false, "")
}

function ShowGaugeRingsColorSelection(){
//    if(document.rdForm.rdAgChartType.value != "Gauge") return;
    var nGoal1 = document.getElementById('txtGoal1').value;
    var nGoal2 = document.getElementById('txtGoal2').value;
    var eleGaugeRingsColorSequence = document.getElementById('rdAgGaugeRingsColorSequence');
    var imgGoalRings = document.getElementById('imgGaugeRings');
    if(nGoal1 > 0 || nGoal2 > 0){
        ShowElement(this.id,'colGaugeRingsColorSelection','Show');
    }
   if ((nGoal1 == '' && nGoal2 == '') || (nGoal1 == 0 && nGoal2 == 0)){
        ShowElement(this.id,'colGaugeRingsColorSelection','Hide');
        eleGaugeRingsColorSequence.value = "";
    }else if((nGoal1 == '' || nGoal2 == '') || (nGoal1 == 0 || nGoal2 == 0)){
        ShowElement(this.id,'colGaugeRingsColorSelection','Show');
        if((eleGaugeRingsColorSequence.value == 'Red-Yellow-Green') || (eleGaugeRingsColorSequence.value == 'Green-Yellow-Red')) eleGaugeRingsColorSequence.value = 'Red-Green';
        if((eleGaugeRingsColorSequence.value == '') || (eleGaugeRingsColorSequence.value == 'Red-Green')){
            imgGoalRings.src = 'rdTemplate/rdAnalysisGrid/Red-Green.png';
            eleGaugeRingsColorSequence.value = 'Red-Green';
        }else{
            imgGoalRings.src = 'rdTemplate/rdAnalysisGrid/Green-Red.png';
            eleGaugeRingsColorSequence.value = 'Green-Red';
        }
        ShowProperGoalValues();
    }else if(nGoal1 > 0 && nGoal2 > 0){
        ShowElement(this.id,'colGaugeRingsColorSelection','Show');
        if((eleGaugeRingsColorSequence.value == 'Red-Green') || (eleGaugeRingsColorSequence.value == 'Green-Red')) eleGaugeRingsColorSequence.value = 'Red-Yellow-Green';
        if(eleGaugeRingsColorSequence.value == 'Red-Yellow-Green'){
            imgGoalRings.src = 'rdTemplate/rdAnalysisGrid/Red-Yellow-Green.png';
            eleGaugeRingsColorSequence.value = 'Red-Yellow-Green';
        }else{
            imgGoalRings.src = 'rdTemplate/rdAnalysisGrid/Green-Yellow-Red.png';
            eleGaugeRingsColorSequence.value = 'Green-Yellow-Red';
        }
        ShowProperGoalValues();
    }
}

function ApplyGaugeRingColors(imgObj){
//    if(document.rdForm.rdAgChartType.value != "Gauge") return;
    var nGoal1 = document.getElementById('txtGoal1').value;
    var nGoal2 = document.getElementById('txtGoal2').value;
    var eleGaugeRingsColorSequence = document.getElementById('rdAgGaugeRingsColorSequence');
    var sColorSequence = imgObj.src.substring(imgObj.src.lastIndexOf("/") + 1, imgObj.src.indexOf('.png'));
    switch(sColorSequence){
        case "Red-Green":
            imgObj.src = imgObj.src.replace(sColorSequence, "Green-Red");
            eleGaugeRingsColorSequence.value = "Green-Red";
            break;
        case "Green-Red":
            imgObj.src = imgObj.src.replace(sColorSequence, "Red-Green");
            eleGaugeRingsColorSequence.value = "Red-Green";
            break;
        case "Green-Yellow-Red":
            imgObj.src = imgObj.src.replace(sColorSequence, "Red-Yellow-Green");
            eleGaugeRingsColorSequence.value = "Red-Yellow-Green";
            break;
        case "Red-Yellow-Green":
            imgObj.src = imgObj.src.replace(sColorSequence, "Green-Yellow-Red");
            eleGaugeRingsColorSequence.value = "Green-Yellow-Red";
            break;
    } 
}

function ShowProperGoalValues(){
    var nGoal1 = document.getElementById('txtGoal1').value;
    var nGoal2 = document.getElementById('txtGoal2').value;
    if(nGoal1 != '' && nGoal2 != ''){
        if(isNaN(nGoal1)) alert('Goal-1 must be numeric');
        if(isNaN(nGoal2)) alert('Goal-2 must be numeric');
        nGoal1 = parseInt(nGoal1);
        nGoal2 = parseInt(nGoal2);
        if(nGoal1 > nGoal2){
            document.getElementById('txtGoal1').value = nGoal2;
            document.getElementById('txtGoal2').value = nGoal1;
        }
    }else  if(nGoal1 == '' || nGoal2 == ''){
        if(nGoal1 == ''){
            if(isNaN(nGoal2)) alert('Goal-2 must be numeric');
            document.getElementById('txtGoal1').value = nGoal2;
             document.getElementById('txtGoal2').value = '';
        }else{
            if(isNaN(nGoal1)) alert('Goal-1 must be numeric');
        }
    }    
}

function ShowProperMinMaxValues(){
    var nMin = document.getElementById('txtMinValue').value;
    var nMax = document.getElementById('txtMaxValue').value;
    if(nMin != '' && nMax != ''){
        if(isNaN(nMin)) alert('Min must be numeric');
        if(isNaN(nMax)) alert('Max must be numeric');
        nMin = parseInt(nMin);
        nMax = parseInt(nMax);
        if(nMin > nMax){
            document.getElementById('txtMinValue').value = nMax;
            document.getElementById('txtMaxValue').value = nMin;
        }
    }else  if(nMin == '' || nMax == ''){
        if(nMin == ''){
            if(isNaN(nMax)) alert('Max must be numeric');
            //document.getElementById('txtMinValue').value = '0';
        }else{
            if(isNaN(nMin)) alert('Min must be numeric');
        }
    }    
}

//function ShowProperGaugeRingColorSelection(imgObj){
//    var eleGaugeRingsColorSequence = document.getElementById('rdAgGaugeRingsColorSequence');
//    if(eleGaugeRingsColorSequence.value == '') return;
//    var sColorSequence = imgObj.src.substring(imgObj.src.lastIndexOf("/") + 1, imgObj.src.indexOf('.png'));
//    switch(eleGaugeRingsColorSequence.value){
//        case "Red-Green":
//            imgObj.src = imgObj.src.replace(sColorSequence, "Green-Red");
//            break;
//        case "Green-Red":
//            imgObj.src = imgObj.src.replace(sColorSequence, "Red-Green");
//            break;
//        case "Green-Yellow-Red":
//            imgObj.src = imgObj.src.replace(sColorSequence, "Red-Yellow-Green");
//            break;
//        case "Red-Yellow-Green":
//            imgObj.src = imgObj.src.replace(sColorSequence, "Green-Yellow-Red");
//            break;
//    } 
//    imgObj.onload = null;
//    //imgObj.removeEventListener('onload', ShowProperGaugeRingColorSelection, true);
//}
