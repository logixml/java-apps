var rdParentPopupPanel;    // Variable to hold the parent PopUp object. Used in case of a Calendar and AddBookmark.

function ShowElement(sParentId,sElementId,sAction,sEffect) {
    if(sElementId == null) return;
    if(sElementId.tagName) {
        // sElementId is actually an element object.
        rdShowSingleElement(sElementId,sAction,sEffect)

    }else {
	    var sIds = sElementId.split(",")
	    for (k=0; k < sIds.length; k++) {

            var sId = Y.Lang.trim(sIds[k]);
            
            var sCurrAction = sAction
            if(sId.split(":").length==2){
                //The action is in the element ID.
                sCurrAction = sId.split(":")[1]
                sId = sId.split(":")[0]
            }
            
            //When in a data table, the sParentID will have a row number.
            //It gets appended to the ID of the element so that only that row is affected.
            //Adjust the indexOf value to look for the lastIndexOf in case the user has placed _Row
            //as part of the ID.
            if (sParentId) {
                if (sParentId.lastIndexOf("_CtCol") != -1) {
                    //For crosstab columns:
                    var idSuffix = sParentId.substr(sParentId.lastIndexOf("_CtCol"))
                    //idSuffix = idSuffix.substr(0,col.indexOf("_Row")) 
                    if (sId.indexOf(idSuffix) == -1) {
                        sId = sId + idSuffix
                    }
                }else if(sParentId.lastIndexOf("_Row") != -1) {
                    //For rows in tables"
                    var idSuffix = sParentId.substr(sParentId.lastIndexOf("_Row"))
                    if (sId.indexOf(idSuffix) == -1) {
                        sId = sId + idSuffix
                    }
                }
            }
            var c = document.getElementById(sId);
            if(c==null){
                if (sId.indexOf("_Row") != -1){
                    c = document.getElementById(sId.substr(0,sId.lastIndexOf("_Row")));
                    if(c==null){    //#15227.
                        if(sId.indexOf("_CtCol") != -1){
                            c = document.getElementById(sId.substr(0,sId.lastIndexOf("_CtCol")));
                        }
                    }
                }
            }
            if(c){
                if(!sElementId.match('PPDatePickerForInputDate') && !sElementId.match('PPTimePickerForInputTime')){
                    if(c.getAttribute('rdPopupPanel') == 'True') 
                        rdParentPopupPanel = c;    //#11760.
                }
                else{   // Fix to make the new Calendar PopUp from a PopUpPanel, #11924.
                    if(rdParentPopupPanel){
                        var rdCurrPopupPanelObj = c.parentNode;
                        if(rdCurrPopupPanelObj){
                            while(rdCurrPopupPanelObj){
                                if(rdCurrPopupPanelObj != rdParentPopupPanel){
                                    rdCurrPopupPanelObj = rdCurrPopupPanelObj.parentNode;
                                }
                                else{
                                    if(rdCurrPopupPanelObj.firstChild.id.match('rdPopupPanelTable')){
                                        if(rdCurrPopupPanelObj.parentNode)  
                                            rdCurrPopupPanelObj.parentNode.appendChild(c);  // Add the new sibling as a child to the parent of the already popped out Div.                              
                                    }
                                    break;
                                }
                            }                
                        }
                    }
                    if(sAction.toLowerCase() != 'hide'){    //#14844.
                        var eleHiddenParent = c.parentNode;
                        while(eleHiddenParent != null){                        
                            if(eleHiddenParent.style){
                                if(eleHiddenParent.style.display == 'none'){
                                    eleHiddenParent.parentNode.appendChild(c);
                                    break;
                                }
                            }
                            eleHiddenParent = eleHiddenParent.parentNode;
                        }
                    }
                }
            }
  
    		if (c==null && sEffect=="rdDontThrowError") return; //#14064.
		    if (c==null) {
		        if (sId.indexOf("rd")!=0) { //Don't report errors for ReportDev elements.
			        alert ("Unable to find the element with the ID value " + sId + ".")
			        return
			    }
		    }
		    
            if (c != null) { 
   		        rdShowSingleElement(c,sCurrAction,sId,sEffect)
   		    }
    		
	    } //Next ID.
	}
	
	if (typeof window.rdRepositionSliders != 'undefined') {
		//Move CellColorSliders, if there are any.
		rdRepositionSliders()
	}
}


function rdShowSingleElement(c,sAction,sId,sEffect) {
    //Show a single element.  "c" is the element itself.
	if(c.nodeName == "COL" && navigator.product == "Gecko" && navigator.productSub && navigator.productSub > "20041010" && (navigator.userAgent.indexOf("rv:1.8") != -1 || navigator.userAgent.indexOf("rv:1.9") != -1)) {
		//Allow table column hiding for Mozilla.
		c.style.display=""
		if (sAction=="Show"){
			c.style.visibility="";
		}else if (sAction=="Hide") {
			c.style.visibility="collapse";
		} else {
			c.style.visibility=(c.style.visibility=="" ? "collapse":"");  //Toggle.
		}
	} else {
		if (sAction=="Show"){
			c.style.display="";
		}else if (sAction=="Hide") {
			c.style.display="none";
		} else {
            c.style.display=(c.style.display=="" ? "none":"");
		}
	}
	
	if (sId) {
	    if (c.getAttribute("id")!="popupFilter" && c.getAttribute("rdNoElementShowHistory") != "True") { //Special case for this DG element and #14008.
	        var windowCurr = window
	        while (windowCurr) {
	            try {
	                var rdShowElementHistory = windowCurr.document.getElementById("rdShowElementHistory")
	                if (rdShowElementHistory) {
		                rdShowElementHistory.value = rdShowElementHistory.value + sId + "=" + (c.style.display=="" ? "Show":"Hide") + ","
    		        }
    		        try {
                        //If there's a parent, this is running as an IFRAME.  Add this shown element to the parent's ShowElementHistory. #6634
                        if (windowCurr.frameElement) {
                            windowCurr = windowCurr.parent
                        }else{
                            windowCurr = null
                        }
                    }
                    catch(e){
	                    windowCurr = null
	                    }
                    finally {}
	            }
                catch(e){
                    windowCurr = null
                    }
                finally {}
            }
        }
	}
	
	if (c.style.display != "none") {
       if (sEffect=="FadeIn" || typeof rdUseFadeIn != "undefined") {
	        rdFadeElementIn(c,250)
	    }
		
		//Special handling for any IFrame subelements.
		//Set the SRC attribute of all subordinate IFrames so that the requested pages are downloaded now.
		
		cFrames = c.getElementsByTagName("IFRAME");
		for (var i = 0; i < cFrames.length; i++) 
		{
			var cFrame = cFrames[i];
			if (isParentVisible(cFrame,c)) 
			{
				if (true) 
				{
					var sSrc = cFrame.getAttribute("HiddenSource");
					if (sSrc != null) {   //There is no HiddenSource if the element was initially visible.
						if (cFrame.getAttribute("src") == null) {   //For nonIE
							cFrame.setAttribute("src", sSrc + "&rdRnd=" + Math.floor(Math.random() * 100000));
						}										    //For IE.
						if (cFrame.getAttribute("src").indexOf(sSrc) == -1) {
							cFrame.setAttribute("src", sSrc + "&rdRnd=" + Math.floor(Math.random() * 100000));
						}
					}
				} 
				else {
					if (cFrame.height < 2) {
						cFrame.src = cFrame.src;  // The frame hasn't been shown yet.  Refresh it.
					}
				}
			}
		}
		
		if (c.getAttribute("rdPopupPanel")=="True") {
            LogiXML.PopupPanel.rdShowPopupPanel(c)
            if(rdRepositionPopupPanel){ //#12931.
                rdRepositionPopupPanel(c, c.offsetHeight, c.offsetWidth, 1);
            }
        }

		if (typeof rdSliderElements!='undefined') {
            rdShowHiddenInputSliders(c)
        }    

    } else {  //Hiding
    
//        if (sEffect=="FadeIn" || typeof rdUseFadeIn != "undefined") {
//            c.style.display = ""
//	        rdFadeElementOut(c,250)
//	    }
            if (c.getAttribute("rdPopupPanel")=="True") {
                rdHidePopupPanelAndModalShade(c);
            }
        }    

	//More special IFrame handling.  If this page is in a frame,
	//the frame needs to be resized from the parent window.
	try {
	    if (frameElement) {
		    if (frameElement.contentWindow) {
			    if (parent.iframeResize) {
			        if(c.style.display == 'none'){
			            parent.iframeResize(frameElement, "OptionalParam")  //#12347.
			        }else{
			            parent.iframeResize(frameElement)
			        }				    
			    }
		    }
	    }
    }
    catch(e){}

}

function isParentVisible(cChild,cShowing) {
	// See if there are any parent elements, above the element that we're showing,
	// that are invisible.  Don't want to load an IncludeFrame in that case.
	//var cParent = cChild.parentElement
	var cParent = cChild.parentNode
	while (cParent.id != cShowing.id) {
		if (cParent.style.display == "none") {
			return false 
		} 
		cParent = cParent.parentNode
	}
	return true
}



function rdShowElementsFromHistory() {
	var hiddenShowElementHistory = document.getElementById("rdShowElementHistory")
	if (hiddenShowElementHistory) {
		var sHistory = hiddenShowElementHistory.value
		var sEvents = sHistory.split(",")
		for (var i=0; i < sEvents.length; i++) {
			var sElementID = sEvents[i].split("=")[0]
			var sAction = sEvents[i].split("=")[1]
			if (document.getElementById(sElementID)) {
				ShowElement(null,sElementID,sAction)
			}
		}
		hiddenShowElementHistory.value = sHistory
	}
}

function rdColumnDisplayVisibility() {
	if(navigator.product == "Gecko" && navigator.productSub && navigator.productSub > "20041010" && (navigator.userAgent.indexOf("rv:1.8") != -1 || navigator.userAgent.indexOf("rv:1.9") != -1)) {
		var cCols = document.getElementsByTagName("COL")
		for (var i=0; i < cCols.length; i++) {
		    if (cCols[i].style.display == "none") {
			    cCols[i].style.display = null
			    cCols[i].style.visibility = "collapse"
		    }
		}
	}
}


//var counter = 0;
function rdFadeElementIn(ele, nDuration, nOpNext, nOpLast, nBeginTime, nTimeConsumed) {
// function makes the element Opaque.
    if(!nTimeConsumed) nTimeConsumed = 0;
    if(!nBeginTime) nBeginTime = new Date().getTime();  
    if(!nOpNext) nOpNext = 0.1;
    if(!nOpLast) nOpLast = 1; 
    if(nOpNext > 1) return;
    
    rdSetElementOpacity(ele, nOpNext);
    var nValues = FadeElementCommon(nDuration, nBeginTime, nTimeConsumed, nOpNext, nOpLast);
    var nOpFactor = parseFloat(nValues.split(',')[0]);
    nTimeConsumed = parseFloat(nValues.split(',')[1]);
    nOpNext = nOpNext + nOpFactor
    
    if (nOpNext < 0 || nOpNext > 1){ 
        rdSetElementOpacity(ele, 1); 
        if(navigator.appName.indexOf("Microsoft Internet") != -1) rdRemoveFadeInElementOpacity(ele);    // #11772. To deal with the font Clear Type issue in IE. http://mattberseth.com/blog/2007/12/ie7_cleartype_dximagetransform.html
        return;
    }  
    if (nOpNext <= nOpLast) {
        nBeginTime = new Date().getTime() + 1;
        setTimeout(function(){rdFadeElementIn(ele, nDuration, nOpNext, nOpLast, nBeginTime, nTimeConsumed)},1);
    }
}

function rdFadeElementOut(ele, nDuration, nOpNext, nOpLast, nBeginTime, nTimeConsumed) {
// function makes the element Transparent.
    if(!nTimeConsumed) nTimeConsumed = 0;
    if(!nBeginTime) nBeginTime = new Date().getTime();  
    if(!nOpNext) nOpNext = 1;
    if(!nOpLast) nOpLast = 0;
    if(nOpNext < 0) return;
    
    rdSetElementOpacity(ele, nOpNext)
    var nValues = FadeElementCommon(nDuration, nBeginTime, nTimeConsumed, nOpNext, nOpLast);
    var nOpFactor = parseFloat(nValues.split(',')[0]);
    nTimeConsumed = parseFloat(nValues.split(',')[1]);
    nOpNext = nOpNext + nOpFactor
    
    if(nOpNext <= 0 || nOpNext > 1){
        rdSetElementOpacity(ele, 0); 
        ele.style.display = "none"; 
        return;
    }
    if (nOpNext >= nOpLast) {
        nBeginTime = new Date().getTime() + 1;
        setTimeout(function(){rdFadeElementOut(ele, nDuration, nOpNext, nOpLast, nBeginTime, nTimeConsumed)},1);
    }
}

function FadeElementCommon(nDuration, nBeginTime, nTimeConsumed, nOpNext, nOpLast){
    // Common code for the Fade effect.
    if(!nTimeConsumed) nTimeConsumed = 0;
    var nOpFactor = 0;
    var nTimeInterval = (new Date().getTime()) - nBeginTime;
    nTimeConsumed = nTimeConsumed + nTimeInterval;
    if(nTimeConsumed > nDuration) return 100 + ',' + nDuration;
    var nRemainingTime = nDuration - nTimeConsumed;
    var nFrames = nRemainingTime/nTimeInterval
    var nRemainingOp = nOpLast - nOpNext;
    nOpFactor = nRemainingOp/nFrames;
    return nOpFactor + ',' + nTimeConsumed;
}


function rdSetElementOpacity(ele, alpha) {
    try{
        var style = ele.style;
        if( style.MozOpacity != undefined ) { //Moz and older
            style.MozOpacity = alpha;
        }
        else if( style.opacity != undefined ) { //WebKit
            style.opacity = alpha;
        }
        else if( style.filter != undefined ) { //IE
            if (ele.tagName.toUpperCase() == "TR") {
                for (var i=0; i < ele.childNodes.length; i++) {
                    if (ele.childNodes[i].tagName.toUpperCase() == "TD") {
                        rdSetElementOpacity(ele.childNodes[i], alpha);
                    }
                }
            }else{
                style.zoom = 1 //Sets currentStyle.hasLayout=true
                style.filter = "alpha(opacity=" + (alpha * 100) + ")";
                ele.filters.alpha.opacity = ( alpha * 100 );
           }
        }
    }
    catch(e){return;}
}

function rdRemoveFadeInElementOpacity(ele) {
// Function runs only for IE, #13117.
    if(ele.style.filter != undefined){
        ele.style.removeAttribute('filter');        
    }
    for (var i=0; i < ele.childNodes.length; i++) {
        if (ele.childNodes[i].style != undefined) {
            if (ele.childNodes[i].style.filter != undefined) {
                ele.childNodes[i].style.removeAttribute('filter');
            }
        }
    }
}

function rdTabsShowTabContents(sTabsId, sReportId) {

    if(typeof rdSliderElements!='undefined'){rdShowHiddenInputSliders(document.getElementById(sTabsId))};

    var sActiveTabId = document.getElementById("rdActiveTabId_" + sTabsId).value
    
    
    //The tab's contents may need to come from a RefreshElement.
    var eleActiveTab = document.getElementById("rdTabPanel_" + sActiveTabId)
    if (eleActiveTab.innerHTML.indexOf("SubmitForm(") == 0) {
        var sRefreshPageFunction = eleActiveTab.innerHTML
        sRefreshPageFunction = sRefreshPageFunction.replace("&amp;rdRequestForwarding","&rdRequestForwarding") //12458
        eleActiveTab.innerHTML = ""
   		eval(sRefreshPageFunction)
        return 
    }
    var eleTabs = eleActiveTab.parentNode    
    if(eleTabs.parentNode != null){ //#17282.
        if(eleTabs.parentNode.getAttribute("sTabbingStyle") == "Ajax"){
            if (eleActiveTab.innerHTML.length == 0) {
                //Ajax Refresh
                rdAjaxRequestWithFormVars("rdAjaxCommand=RefreshElement&rdRefreshTabPanel=True&rdCurrTabId=" + sActiveTabId + "&rdRefreshElementID=rdTabPanel_" + sActiveTabId + "," + sActiveTabId + "&rdReport=" + sReportId, false, "")
            }
        }
    }
    
    //In some cases, deselected tab panels don't get shown/hidden. Fix this.    
    for (var i=0; i < eleTabs.childNodes.length; i++) {
        if (eleTabs.childNodes[i].id == eleActiveTab.id) {
            eleTabs.childNodes[i].style.display=""
        }else{
            eleTabs.childNodes[i].style.display="none"
        }
    }
    
    rdTabsSetMinWidth(sTabsId)
    
}

function rdTabsSetMinWidth(sTabsId) {  //11744
    if(!document.getElementById("rdActiveTabId_" + sTabsId)) return;    //#12791.
    var sActiveTabId = document.getElementById("rdActiveTabId_" + sTabsId).value
        //Set the width of a hidden table so that the tab's panel cannot be smaller than width of the tabs.
    //Get the current tab's label.
    var eleTabLabel = document.getElementById(sActiveTabId)
    //Get the last tabl's label.
    while (eleTabLabel.nextSibling) {
        eleTabLabel = eleTabLabel.nextSibling
    }
    var nMinTabWidth = eleTabLabel.offsetLeft + eleTabLabel.offsetWidth
    var eleMinWidth = document.getElementById("rdTabMinWidth_" + sActiveTabId)
    if (eleMinWidth) {
        eleMinWidth.style.width = nMinTabWidth + "px"
        eleMinWidth.style.display = "block"
    }
}

function rdFindPosX(obj)
  {
    var curleft = 0;
    if (obj) {
        if(obj.offsetParent)
            while(1) 
            {
              curleft += obj.offsetLeft;
              if(!obj.offsetParent)
                break;
              obj = obj.offsetParent;
            }
        else if(obj.x)
            curleft += obj.x;
    }
    return curleft;
  }

function rdFindPosY(obj)
  {
    var curtop = 0;
    if (obj) {
        if(obj.offsetParent)
            while(1)
            {
              curtop += obj.offsetTop;
              if(!obj.offsetParent)
                break;
              obj = obj.offsetParent;
            }
        else if(obj.y)
            curtop += obj.y;
    }
    return curtop;
  }
