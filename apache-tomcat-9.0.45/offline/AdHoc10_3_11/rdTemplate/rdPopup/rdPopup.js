var rdCurrMenu

function rdShowPopupMenu(sMenuId,sPopupLocation) {

    if (rdCurrMenu) {
//        rdCurrMenu.destroy()
        rdCurrMenu.hide(rdCurrMenu)
        YAHOO.widget.MenuManager.removeMenu(rdCurrMenu);    //#12703.
    }

    var sPopupId  //Add rdPopup to the ID. For tables, it goes befor _Row#, For crosstabs, it needs to go before _Ct# 
    if (sMenuId.indexOf("_CtCol") !=-1) { 
        sPopupId = sMenuId.replace("_CtCol", "_rdPopup_CtCol") 
    } 
     else if (sMenuId.indexOf("_Row") != -1)  { 
        sPopupId = sMenuId.replace("_Row","_rdPopup_Row") 
    } 
     else { 
        sPopupId = sMenuId + "_rdPopup" 
    } 

    
    var sLocation = "bl"
    if (sPopupLocation) {
        if (sPopupLocation.toLowerCase()=="right") {
            sLocation = "tr" // top right
        }
    }
    
    //Special case for empty menu options. 10890
    var elePopup = document.getElementById(sPopupId)
    for (var i=elePopup.firstChild.firstChild.childNodes.length - 1; i > -1; i--) {
        var item = elePopup.firstChild.firstChild.childNodes[i]
        var sText
        if (item.textContent != undefined) {
            sText = item.textContent //Mozilla, Webkit
        } else {
            sText = item.innerText //IE
        }
        if (sText == "") {
            //if (item.innerHTML.indexOf("Blank.gif") != -1) {
                item.parentNode.removeChild(item)
            //}
        }
    }
    if(elePopup.innerHTML.match("rdModalShade") && elePopup.innerHTML.match("rdPopupPanel")){   
        // Move the Modal and the Popup as Siblings to the Menu  for this to work #12652.
        try{
            var eleMenuList = elePopup.firstChild;
            var aMenuListItems;
                if(eleMenuList){
                    while(eleMenuList) {
                        if(!eleMenuList.tagName.toLowerCase().match("ul")){
                            eleMenuList = eleMenuList.firstChild;
                        }
                        else{
                            aMenuListItems = eleMenuList.getElementsByTagName("li")
                            break;
                        }
                    }                
                }
            for(var i = 0; i <= aMenuListItems.length; i++){
                MovePopupPanelAsSiblingToPopupMenu(aMenuListItems[i], elePopup)
            }
        }catch(e){}
    }
    
    rdCurrMenu = YAHOO.widget.MenuManager.getMenu(sPopupId) //9899
//    if(rdCurrMenu != undefined){    //#12703.
//        if(rdCurrMenu.body.innerHTML == ""){ 
//            YAHOO.widget.MenuManager.removeMenu(rdCurrMenu);
//            rdCurrMenu = undefined;
//        }
//    }
    if (rdCurrMenu == undefined) {
        rdCurrMenu = new YAHOO.widget.Menu(sPopupId, { context: [sMenuId, "tl", sLocation] }) //tl:top left  bl:bottom left
        rdCurrMenu.render();
        rdCurrMenu.show(rdCurrMenu)
    } else { 
        //The popup menu already exists.
        rdCurrMenu.show(rdCurrMenu)
    }
}

function MovePopupPanelAsSiblingToPopupMenu(eleMenuListItem, elePopup){
    // Move the Modal and the Popup as Siblings to the Menu.
    if(eleMenuListItem.innerHTML.match("rdModalShade") && eleMenuListItem.innerHTML.match("rdPopupPanel")){
        var eleModalDivs = eleMenuListItem.getElementsByTagName("div")
        for(var x = 0; x <= eleModalDivs.length; x++){
            if(eleModalDivs[x]){
                if(eleModalDivs[x].id){
                    if(eleModalDivs[x].id.toLowerCase().match("rdmodalshade") || eleModalDivs[x].id.toLowerCase().match("ppaddbookmarks") || eleModalDivs[x].id.toLowerCase().match("ppeditbookmarks")){
                        elePopup.parentNode.appendChild(eleModalDivs[x]);
                        x = x-1;
                    }
                }
            }
        }
    }
}

