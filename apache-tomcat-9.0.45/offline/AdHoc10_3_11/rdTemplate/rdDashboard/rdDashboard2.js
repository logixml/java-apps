var rdDropZoneId
var rdAddedRemovedPanels = ""

function rdPreventDragFromRename(sRenameElementId) {
    var eleRename = document.getElementById(sRenameElementId)
    if (eleRename) {
        eleRename.onmousedown =
            function(e) {
                e = e || window.event //Get the event object for all browsers
                e.cancelBubble=true;
                return true;
            }
    }
}

function rdSetDropZone(eleDropZone) {
    if (rdDropZoneId) {
        var eleOldDropZone = document.getElementById(rdDropZoneId)
        if (eleOldDropZone) {
            if (eleOldDropZone.tagName == "TABLE") {
                //Table cell.
                eleOldDropZone.firstChild.firstChild.firstChild.className="rdDashboardDropZone"  //All these children get to the table's cell.
            }else{
                //Tab
                eleOldDropZone.firstChild.firstChild.firstChild.className = eleOldDropZone.firstChild.firstChild.firstChild.className.replace(" rdDashboardDropTabActive","")
            }
        }
    }

    if (eleDropZone) {
        rdDropZoneId = eleDropZone.id
        if (eleDropZone.tagName == "TABLE") {
            //Table cell.
            eleDropZone.firstChild.firstChild.firstChild.className="rdDashboardDropZoneActive"
         }else{
            //Tab
            eleDropZone.firstChild.firstChild.firstChild.className = eleDropZone.firstChild.firstChild.firstChild.className + " rdDashboardDropTabActive"
         }
   } else {
        rdDropZoneId = null
    }
}

function rdGetDbPanelHeight(eleObject) { 
    return(eleObject.offsetParent ? (rdGetDbPanelHeight(eleObject.offsetParent) + eleObject.offsetTop) : eleObject.offsetTop); 
}

function rdSetAppletVisibility(sVis) {
    //Hide objects that will intefere with DnD.
    //applets
    var eleApplets = document.getElementsByTagName("applet")  
    for (var i=0; i < eleApplets.length; i++) {
        var eleApplet = eleApplets[i]
        eleApplet.style.visibility = sVis
    }
    //Flash with IE
    eleApplets = document.getElementsByTagName("object")  
    for (var i=0; i < eleApplets.length; i++) {
        var eleApplet = eleApplets[i]
        eleApplet.style.visibility = sVis
    }
    //Flash with Mozilla.
    eleApplets = document.getElementsByTagName("embed")  
    for (var i=0; i < eleApplets.length; i++) {
        var eleApplet = eleApplets[i]
        eleApplet.style.visibility = sVis
    }
}

function rdInitDashboardPanels() {
    //Is the dashboard adjustable?     
    var bFreeformLayout = false;
	var eleAdjustable =  document.getElementById("rdDashboardAdjustable")
    var eleFreeformLayout = document.getElementById('rdFreeformLayout');
	if (eleFreeformLayout!= null) 
        if(eleFreeformLayout.id.toLowerCase() == 'rdhiddenrequestforwarding') 
            eleFreeformLayout = null;   //#14758.
    if (eleFreeformLayout!= null) bFreeformLayout = true;
    if(!eleAdjustable) return;  //11475 - Does dashboard exist?
	
	//Initilize free form locations one time since the user can't change it
    if (eleAdjustable.innerHTML == 'False') {
        if(bFreeformLayout){
            rdSizeUnAdjustablePanels();
            rdResizeDashboardContainer();
            return;
        }else return;        
    }

	YUI().use('dd-drop-plugin', 'dd-plugin', 'dd-scroll', function(Y) {	
        var bIsTouchDevice = false;
        try {
            document.createEvent('TouchEvent');	
            bIsTouchDevice = true;	
        }catch(e){}	
        
		//Make the panels draggable.
		if(typeof(rdMobileReport)=='undefined'){ //Not for mobile. 13676
			if(bFreeformLayout)				 
				rdCascadeFreeformLayoutPanelsFromOldDashboardFile('rdDivDashboardpanels');  					
			
			var elePanels = Y.all('.rdDashboardPanel')  
			for (var i=0; i < elePanels.size(); i++) {
				var elePanel = elePanels.item(i).getDOMNode();
				if(elePanel.className.indexOf('yui-resize') != -1 || elePanel.id.indexOf('rdDashboardPanel-') != 0) continue //11516,11518,11524.
				
				// Don't unreg the YUI Resize handles.								
				var dashTitleID = elePanel.id.replace("rdDashboardPanel-","rdDashboardPanelTitle-");
				var node = Y.one(elePanel);
				var drag = node.plug(Y.Plugin.Drag);
				
				var pnlContent;
				var pnlDivs = node.all('div');
				for (var j = 0; j < pnlDivs.size(); j++) {
				    if (pnlDivs.item(j).get('id').indexOf('rdDashboard2PanelContent_') == 0) {
				        pnlContent = pnlDivs.item(j);
				        break;
				    }					    
				}			
				var eleEditButton = Y.one('#' + elePanel.id.replace('rdDashboardPanel', 'rdDashboardEdit'));
                if (eleEditButton) eleEditButton.on('click', rdResizePanelContentOnEditCancelSave, eleEditButton, [pnlContent.get('id'), elePanel.id, 'Edit']); 		
                var eleCancelButton = Y.one('#' + elePanel.id.replace('rdDashboardPanel','rdDashboardCancel'));
                if (eleCancelButton) eleCancelButton.on('click', rdResizePanelContentOnEditCancelSave, eleCancelButton, [pnlContent.get('id'), elePanel.id, 'Cancel']);		
                var eleSaveButton = Y.one('#' + elePanel.id.replace('rdDashboardPanel','rdDashboardSave'));
                if (eleSaveButton) eleSaveButton.on('click', rdResizePanelContentOnEditCancelSave, eleSaveButton, [pnlContent.get('id'), elePanel.id, 'Save']);			
				if (bFreeformLayout) {					
					//Init resizers									
					rdInitDashboardPanelResizer(pnlContent, elePanel.id, "rdDivDashboardpanels");
				}				
				//Attach drag-drop events
				drag.dd.on('drag:start', function(e) {
					var pnlDragged = this.get('dragNode');					
					
					if (bFreeformLayout) {						
						freezeDashboardContainer();
						
						rdSetDashboardPanelZIndex(pnlDragged);
						pnlDragged.setStyle('opacity', '.75');
					} else {
						pnlDragged.setStyles({								
							zIndex: 1,
							opacity: .75
						});
					}						
					rdSetAppletVisibility("hidden")
				});					
				drag.dd.on('drag:end', function(e) {
					//endDrag occurs after DragDrop
					var pnlDragged = this.get('dragNode');

					if (bFreeformLayout) {
						unFreezeDashboardContainer();
						
						var pnlTop = pnlDragged.getStyle('top').replace('px', '');
						var pnlLeft = pnlDragged.getStyle('left').replace('px', '');
						if (pnlTop < 0) pnlDragged.setStyle('top', '0px');
						if (pnlLeft < 0) pnlDragged.setStyle('left', '0px');
						pnlDragged.setStyle('opacity', '1');
						rdSaveFreeformLayoutPanelPosition('rdDivDashboardpanels');
					} else {						
						pnlDragged.setStyles({
							zIndex: 0,
							opacity: 1,
							left: 0,
							top: 0
						});				
					}
					var posDashboardPanelFinalCoOrds = pnlDragged.getXY();
					if(bIsTouchDevice) setTimeout(function(){rdResetDashboardPanelAfterDDScroll(pnlDragged.getDOMNode(), posDashboardPanelFinalCoOrds)}, 1000);  // Do this for the Tablet only, #15478.
					rdSetDropZone(null);												
					rdSetAppletVisibility("");
				});
				drag.dd.on('drag:over', function(e) {
					var x = 1;
					
					var target = e.drop.get('node');
					var dragNode = this.get('node');
					
					var eleTarget = target.getDOMNode();
					var pnlDragged = dragNode.getDOMNode();

					if (!bFreeformLayout && target.get('id').indexOf("rdDashboardColumn") == 0) {
						//Find the closest DropZone that's above the current position in the same column.																
						var eleDropZone, eleClosestDropZone, nClosestDistance, elePanelChild;
						
						for (var i=0; i < eleTarget.childNodes.length; i++) {
							if (eleTarget.childNodes[i].id.indexOf("rdDashboardDropZone") != -1) {
								eleDropZone = eleTarget.childNodes[i];
								var yDragged = rdGetDbPanelHeight(pnlDragged);
								var yDropZone = rdGetDbPanelHeight(eleDropZone);
								
								if (!eleClosestDropZone) {
									eleClosestDropZone = eleDropZone;
									nClosestDistance = Math.abs(yDragged - yDropZone);
								} else if (Math.abs(yDragged - yDropZone) < nClosestDistance){
									eleClosestDropZone = eleDropZone;
									nClosestDistance = Math.abs(yDragged - yDropZone);
									
								}
							}
						}
						
						if (eleClosestDropZone) 
							rdSetDropZone(eleClosestDropZone);							
					}
					else {
						//Dragging over a tab?								
						if (eleTarget.tagName == "LI") {							
							if (eleTarget.parentNode.parentNode.id.indexOf('rdTabs-') == 0) {
								rdSetDropZone(eleTarget);
							}
						}
						else
							rdSetDropZone(null);
					}
				});
				
				//Now you can only drag it from the panel title
				var hndNode = node.one('tr[id="' + dashTitleID + '"]');						
				node.dd.addHandle(hndNode);
				if (bFreeformLayout) node.dd.plug(Y.Plugin.DDWinScroll, {scrollDelay:100});
				hndNode.setStyle('cursor', 'move');	
				
			}
			
			if(bFreeformLayout) 
				rdSaveFreeformLayoutPanelPosition('rdDivDashboardpanels');			
		} 
		
		//Make the columns droppable.
		var eleCols = document.getElementsByTagName("TD");  
		for (var i=0; i < eleCols.length; i++) {
			var eleCol = eleCols[i];
			if (eleCol.id.indexOf("rdDashboardColumn") == 0) {				
				var drop = Y.one(eleCol).plug(Y.Plugin.Drop);
				drop.drop.on('drop:hit', function(e) {
					if (!rdDropZoneId)
						return;
					
					//Move the dragged panel
					var eleDropZone = document.getElementById(rdDropZoneId)  //The drop zone where it was dropped.
					if (!eleDropZone) 
						return;
										
					if (eleDropZone.tagName == "TABLE") {
						//Dropped in a drop zone.
						var pnlDragged = e.drag.get('node');
						pnlDragged.setStyles({
							left: 0,
							top: 0
						});
						if (pnlDragged.get('id').replace("rdDashboardPanel","rdDashboardDropZone") == rdDropZoneId) {
							//Dropped on the current panel's drop zone.  Put the panel back.
							rdSetDropZone(null);							
							//rdAnimateHome(pnlDragged.id)
							return
						}
												
						var eleDropZoneBelow = document.getElementById(pnlDragged.get('id').replace("rdDashboardPanel","rdDashboardDropZone")) //The drop zone below the panel.
						var elePanelDragged = document.getElementById(pnlDragged.get('id'));
						//Move the panel and its sibling drop zone.
						if (eleDropZone.nextSibling) {
							eleDropZone.parentNode.insertBefore(eleDropZoneBelow,eleDropZone.nextSibling)
							eleDropZone.parentNode.insertBefore(elePanelDragged,eleDropZone.nextSibling)
						} else {
							eleDropZone.parentNode.appendChild(elePanelDragged)
							eleDropZone.parentNode.appendChild(eleDropZoneBelow)
						}
						
						rdSetDropZone(null)
						rdSaveDashboardOrder()
					}
				});								
			}			
		}
		
		//Make the Tabs droppable.
		var eleTabs = document.getElementsByTagName("LI");
		for (var i = 0; i < eleTabs.length; i++) {
			var eleTab = eleTabs[i];
			if (eleTab.parentNode.parentNode.id.indexOf("rdTabs-") == 0) {
				if (eleTab.title != "active") {
					if (eleTab.id != "rdTabAddNewTab") {					
						var drop = Y.one('li[id="'+ eleTab.id + '"]').plug(Y.Plugin.Drop);
						drop.drop.on('drop:hit', function(e) {
							if (!rdDropZoneId)
								return;
							
							//Move the dragged panel
							var eleDropZone = document.getElementById(rdDropZoneId)  //The drop zone where it was dropped.
							if (!eleDropZone) 
								return;
								
							//Dropped on a Tab
							var pnlDragged = e.drag.get('node');	
							var sPanelId = pnlDragged.get('id');
							// Do not move the panel to a different Tab if the panel is a Single Instance panel.
							var eleSingleInstancePanels = Y.one('#rdDashboardSingleInstanceOnlyPanelsList');
							if(eleSingleInstancePanels != null){
                                var aSingleInstancePanels = eleSingleInstancePanels.getDOMNode().value.split(',');
                                for(i=0;i<aSingleInstancePanels.length;i++){
                                    var sSingleInstancePanelId = aSingleInstancePanels[i];
                                    if(sSingleInstancePanelId == sPanelId.substring(sPanelId.indexOf('rdDashboardPanel-') + 'rdDashboardPanel-'.length, sPanelId.lastIndexOf('_'))){
                                        return;
                                    }
                                }
							}						
							rdMovePanelToTab(sPanelId, e.target.get('node').get('id'), eleDropZone)
						});												
					}
				}
			}
		}
	
	});
    
    //Make the Add New Tab look like a button instead of a tab.
    var eleNewTab = document.getElementById("rdTabAddNewTab")  
    if (eleNewTab) {
      if(typeof(rdMobileReport)=='undefined'){
            var eleA = eleNewTab.firstChild
            eleA.style.backgroundColor="transparent"
            eleA.style.borderLeft=0
            eleA.style.borderRight=0
            eleA.style.borderTop=0
            eleA.style.paddingBottom=0
            eleA.style.backgroundImage="none"
            var eleEm = eleA.firstChild
            eleEm.style.paddingBottom="1px"
            eleEm.style.paddingLeft="1px"
            var eleSpan = eleA.firstChild.firstChild
            eleSpan.className = "rdDashboardCommand"
        }else{
            // Shrink the Tab size to 50% of the screen size.
            //eleNewTab.parentNode.style.whiteSpace=''; This needs to be set in VB.
            var eleMobileDashboardTab = eleNewTab.previousSibling;
            var nTabWidth = 175;
            eleMobileDashboardTab.style.width = nTabWidth + 'px';
            eleMobileDashboardTab.style.wordWrap  = 'break-word';
            eleMobileDashboardTab.firstChild.style.paddingLeft = 2 + 'px';  // anchor tag
            eleMobileDashboardTab.firstChild.style.paddingRight = 2 + 'px';
            eleMobileDashboardTab.firstChild.style.width = (nTabWidth - 4) + 'px';
            eleMobileDashboardTab.firstChild.style.wordWrap  = 'break-word';
            eleMobileDashboardTab.firstChild.style.backgroundRepeat = 'repeat-x'; 
            eleMobileDashboardTab.firstChild.firstChild.style.paddingLeft = 2 + 'px';  // em tag
            eleMobileDashboardTab.firstChild.firstChild.style.paddingRight = 2 + 'px';
            eleMobileDashboardTab.firstChild.firstChild.style.width = (nTabWidth - 8) + 'px';
            eleMobileDashboardTab.firstChild.firstChild.style.wordWrap  = 'break-word';            
        }
    } 
}

function rdResetDashboardPanelAfterDDScroll(elePnlDragged, posDashboardPanelFinalCoOrds){
    
	var pnlDragged = Y.one(elePnlDragged);
	pnlDragged.setXY(posDashboardPanelFinalCoOrds);    
}

function rdSizeUnAdjustablePanels(){
    
	var aDashboardPanels = Y.Selector.query('.rdDashboardPanel', Y.DOM.byId('rdDivDashboardpanels'))    
	for (var i=0; i < aDashboardPanels.length; i++){
		var eleDashboardPanel = aDashboardPanels[i];                                                
		var nHeight = (eleDashboardPanel.offsetHeight-4) + 'px';    // Substracting 4px to compensate the panel growth on every page request.
		var nWidth = (eleDashboardPanel.offsetWidth-4) + 'px';
		
		eleDashboardPanel.style.height = nHeight
		eleDashboardPanel.style.width = nWidth

		var elePanelTable = eleDashboardPanel.firstChild;    
		var elePanelTitle = document.getElementById(eleDashboardPanel.id.replace('rdDashboardPanel', 'rdDashboardPanelTitle'))
		var nTitleHeight = elePanelTitle.offsetHeight;
		var elePanelParams =  elePanelTitle.nextSibling;
		var nParamsHeight
		if(elePanelParams.style.display == 'none'){ 
			nParamsHeight = 0;
		}else{
			nParamsHeight = elePanelParams.offsetHeight;
		}
		var eleContent =  elePanelParams.nextSibling.firstChild.firstChild;
		eleContent.style.overflow = 'auto';
		
		eleContent.style.width = (eleDashboardPanel.offsetWidth-10) + 'px';
		var nContentHeight = (eleDashboardPanel.offsetHeight - nTitleHeight - nParamsHeight -10)
		eleContent.style.height = (nContentHeight < 20 ? 20 : nContentHeight) + 'px';
		
		eleDashboardPanel.style.overflow = 'hidden';
	}    
}

function rdSetDashboardPanelOpacity(eleDashboardPanel, nOpacity) {
	
	var aDashboardPanels = Y.all('.rdDashboardPanel');
	for (var i=0; i < aDashboardPanels.size(); i++){
		var elePnl = aDashboardPanels.item(i);
		if(elePnl.get('id') == eleDashboardPanel.get('id')) continue  // Do not change the opacity of the panel being moved.
		elePnl.setStyle('opacity', nOpacity);
	}	
}

function rdSetDashboardPanelZIndex(eleDashboardPanel) {
	
	var nZindex = 0;
	var aDashboardPanels = Y.Node.all('.rdDashboardPanel'); //Dom.getElementsBy(function(ele){return ele.id.match('rdDashboardPanel-')},"div", 'rdDivDashboardpanels')
	for (var i=0; i < aDashboardPanels.size(); i++){
		var elePnl = aDashboardPanels.item(i);
		if(elePnl.get('id') == eleDashboardPanel.get('id')) continue  // Do not increase the zIndex if this panel is already on top of the stack.
		if(i==0){
			nZindex = parseInt(elePnl.getStyle('zIndex') == 'auto'?0:elePnl.getStyle('zIndex'))
		}else if(nZindex < (elePnl.getStyle('zIndex') == 'auto'?0:elePnl.getStyle('zIndex'))){
			nZindex =  parseInt(elePnl.getStyle('zIndex'));
		}	
	}
	eleDashboardPanel.setStyle('zIndex', (nZindex + 1));
}

function rdResizeDashboardContainer(){
	
	var elePanelContainer = Y.one('#rdDivDashboardpanels');
	if (elePanelContainer == null) return;
	var aDashboardPanels = elePanelContainer.all('.rdDashboardPanel');		
	var nRight = 0; var nBottom = 0;
	
	for (var i = 0; i < aDashboardPanels.size(); i++) {
		var elePanelItem = aDashboardPanels.item(i);
		var regionPanelItem = Y.DOM.region(elePanelItem.getDOMNode());
		if(i == 0){
			nRight = regionPanelItem.right; 
			nBottom = regionPanelItem.bottom;
		}else{
			if(regionPanelItem.right > nRight) nRight = regionPanelItem.right;
			if(regionPanelItem.bottom > nBottom) nBottom = regionPanelItem.bottom;
		}
	}
	
	var regionPanelContainer = Y.DOM.region(elePanelContainer.getDOMNode());
	if(aDashboardPanels.size() > 0){
		var eleTab = Y.Selector.ancestor(aDashboardPanels.item(0), '.rdTabPanel', false)
		elePanelContainer.setStyle('height', (nBottom - regionPanelContainer.top ) + 'px'); // Added 30px more to compensate for the Change Dashboard button table.
		if(nRight < Y.DOM.viewportRegion().right) {
			elePanelContainer.setStyle('width', (Y.DOM.viewportRegion().right - regionPanelContainer.left - 20 + 'px'));
			if (eleTab) eleTab.setStyle('width', (Y.DOM.viewportRegion().right - regionPanelContainer.left - 20 + 'px'));
		}else {
			elePanelContainer.setStyle('width', nRight + 'px');
			if (eleTab) eleTab.setStyle('width', (nRight + 10) + 'px');
		}
	}else{
		elePanelContainer.setStyle('height', 25 + 'px');
		elePanelContainer.setStyle('width', (Y.DOM.viewportRegion().right - regionPanelContainer.left - 20)+ 'px');
	}	
}

function rdCascadeFreeformLayoutPanelsFromOldDashboardFile(sPanelContainerID){
	
	var eleDivDashboardPanel = Y.one('#' + sPanelContainerID);
	if (eleDivDashboardPanel == null) return;
	var aNewDashboardPanels = eleDivDashboardPanel.all('.rdDashboardPanel')		
			
	var eleDashboardPanel;
	for (var i = 0; i < aNewDashboardPanels.size(); i++) {
	
		eleDashboardPanel = aNewDashboardPanels.item(i);      
		if (eleDashboardPanel.getStyle('zIndex') != '') continue;
			
		eleDashboardPanel.setStyle('position', 'absolute');
		if (i == 0) {			
			eleDashboardPanel.setStyle('left', '0px'); 
			eleDashboardPanel.setStyle('top', '0px');
		}
		else {			
			eleDashboardPanel.setStyle('left', (i*25) + 'px'); 
			eleDashboardPanel.setStyle('top', (i*25) + 'px');
		}			
		
		rdSetDashboardPanelZIndex(eleDashboardPanel);    // Bring the panels to the top.
	}	
}

function rdSaveFreeformLayoutPanelPosition(sPanelContainerId) {
	
	rdResizeDashboardContainer();
	var eleHiddenPanelOrder = document.getElementById("rdDashboardPanelOrder")    
	eleHiddenPanelOrder.value = "";
	var eleDashboardTab = Y.one('#' + sPanelContainerId);
	if(!eleDashboardTab) return;
	
	var elePanels = eleDashboardTab.all('.rdDashboardPanel');
	for (var i=0; i < elePanels.size(); i++) {
		var elePanel = elePanels.item(i);
		
		eleHiddenPanelOrder.value += "," + rdGetPanelInstanceId(elePanel.getDOMNode());
		eleHiddenPanelOrder.value += ":0:STYLE=" + elePanel.getStyle('cssText');
		
	}
			
	var rdPanelParams = "&rdReport=" + document.getElementById("rdDashboardDefinition").value;		
	rdPanelParams += "&rdFreeformLayout=True";   
	var eleTab = Y.Selector.ancestor(eleDashboardTab, '.rdTabPanel');
	if (eleTab) {
		eleTab.parentNode.setStyle('overflow', 'hidden');
		rdPanelParams += ("&rdDashboardTabID=" +  eleTab.get('id').substring(eleTab.get('id').lastIndexOf("_") + 1));
	}
	
	var regionTab = Y.DOM.region(eleDashboardTab.getDOMNode());
	var sTabStyle = "Width:" + (regionTab.right - regionTab.left) + 'px';
	sTabStyle += (";Height:" + (regionTab.bottom - regionTab.top) + 'px');
	rdPanelParams += ("&rdDashboardTabStyle=" + sTabStyle); 

	window.status = "Saving dashboard panel positions.";
	rdAjaxRequestWithFormVars('rdAjaxCommand=rdAjaxNotify&rdNotifyCommand=UpdateDashboardPanelOrder' + rdPanelParams);	
}

function rdSaveDashboardOrder() { 
    var eleHiddenPanelOrder = document.getElementById("rdDashboardPanelOrder")
    eleHiddenPanelOrder.value = ""
    var elePanels = document.getElementsByTagName("DIV")  
    for (var i=0; i < elePanels.length; i++) {
        var elePanel = elePanels[i]
        if (elePanel.id.indexOf("rdDashboardPanel") == 0) {
            eleHiddenPanelOrder.value += "," + rdGetPanelInstanceId(elePanel)
            //Add the column number
            var nColNr = elePanel.parentNode.id.replace("rdDashboardColumn","")
            eleHiddenPanelOrder.value += ":" + nColNr                
        }
    }    
    var rdPanelParams = "&rdReport=" + document.getElementById("rdDashboardDefinition").value
    window.status = "Saving dashboard panel positions."
    rdAjaxRequestWithFormVars('rdAjaxCommand=rdAjaxNotify&rdNotifyCommand=UpdateDashboardPanelOrder' + rdPanelParams)
}

var nNewAddedPanelCount=0;
function rdAddDashboardPanel(sPanelID,nRowNr) {
    
	var rdFreeformLayout = document.getElementById('rdFreeformLayout');
	if (rdFreeformLayout!= null) 
		if(rdFreeformLayout.id.toLowerCase() == 'rdhiddenrequestforwarding') 
			rdFreeformLayout = null;   //#14758.
	var rdParams = "&rdReport=" + document.getElementById("rdDashboardDefinition").value
	rdParams += "&PanelID=" + sPanelID
	try { //Skip this if there's no tabs.
		rdParams += "&TabID=" + document.getElementById("rdActiveTabId_rdDashboardTabs").value
	}
	catch (e){}
	if(rdFreeformLayout){
		rdParams += "&rdFreeformLayout=True";
		rdParams += "&rdNewFreeformLayoutPanel=True";
		if(nNewAddedPanelCount == 0){
			rdParams += "&rdFreeformLayoutStyle=Position:absolute;Left:0px;Top:0px;"
			nNewAddedPanelCount = 1;
		}else{
			rdParams += "&rdFreeformLayoutStyle=Position:absolute;" + "Left:" + (nNewAddedPanelCount * 25) + "px;Top:" + (nNewAddedPanelCount * 25) + "px;";
			nNewAddedPanelCount = nNewAddedPanelCount + 1;
		}
		rdParams += ("&rdDashboardTabStyle=Width:" + Y.DOM.winWidth() + "px;Height:" + (Y.DOM.winHeight() - Y.DOM.region(Y.DOM.byId('rdDashboardList')).top - 50) + 'px;');        
	}
	rdAjaxRequestWithFormVars('rdAjaxCommand=rdAjaxNotify&rdNotifyCommand=AddDashboardPanel' + rdParams)
	//Update the count.
	var eleCount = document.getElementById("lblCount_Row" + nRowNr)
	eleCount.innerHTML = parseInt(eleCount.innerHTML) + 1
	var eleCountDiv = document.getElementById("divCount_Row" + nRowNr)
	eleCountDiv.className = "rdDashboardTitleCaption"
	var eleAddedDiv = document.getElementById("divAdded_Row" + nRowNr)
	eleAddedDiv.className = "rdDashboardTitleCaption"
	//Hide the Add button?
	if (document.getElementById("hiddenMultiInstance_Row" + nRowNr).value == "False") {
		var eleAddNowButton = document.getElementById("lblAddPanel_Row" + nRowNr)
		eleAddNowButton.className = "rdDashboardHidden"
		eleCountDiv.className = "rdDashboardHidden"
		var eleDeletePanelButton = document.getElementById("lblDeletePanel_Row" + nRowNr)
		eleDeletePanelButton.className = "rdDashboardHidden"
	}    
}

function rdRemoveDashboardPanel(sPanelElementID,eEvent) {
    var rdFreeformLayout = document.getElementById('rdFreeformLayout');
    if (rdFreeformLayout!= null) 
        if(rdFreeformLayout.id.toLowerCase() == 'rdhiddenrequestforwarding') 
            rdFreeformLayout = null;   //#14758.
    //Remove the panel from the page. 
    var elePanel = document.getElementById(sPanelElementID)
    if (elePanel) {  //If the user clicks a lot on the same button, this may not exist.
        var eleDropZoneBelow = document.getElementById(sPanelElementID.replace("rdDashboardPanel","rdDashboardDropZone")) //The drop zone below the panel.
        elePanel.parentNode.removeChild(elePanel)
        if(eleDropZoneBelow) eleDropZoneBelow.parentNode.removeChild(eleDropZoneBelow)
        
        //Clear the checkbox.
        var sPanelID = sPanelElementID.replace("rdDashboardPanel-","")
        var eleChecks = document.getElementsByTagName("INPUT")  
        for (var i=0; i < eleChecks.length; i++) {
            var eleCheck = eleChecks[i]
            if (eleCheck.parentNode.innerHTML.indexOf('&quot;,' + sPanelID + '&quot;') != -1) {
                eleCheck.checked = false
            }
        }

        var rdPanelParams = "&rdReport=" + document.getElementById("rdDashboardDefinition").value;
        rdPanelParams += '&PanelInstanceID=' + rdGetPanelInstanceId(elePanel)
         if(rdFreeformLayout){
            rdResizeDashboardContainer();
            rdPanelParams += "&rdFreeformLayout=True";
        }        
        rdAjaxRequestWithFormVars('rdAjaxCommand=rdAjaxNotify&rdNotifyCommand=RemoveDashboardPanel' + rdPanelParams)
    }
}

function rdDeleteCustomDashboardPanel(sPanelElementID, nRowNr, eleDeleteButton){
    var rdPanelParams = "&rdReport=" + document.getElementById("rdDashboardDefinition").value
    rdPanelParams += '&sPanelID=' + sPanelElementID
     try { //Skip this if there's no tabs.
        rdPanelParams += "&TabID=" + document.getElementById("rdActiveTabId_rdDashboardTabs").value
    }
    catch (e){}
    rdAjaxRequestWithFormVars('rdAjaxCommand=rdAjaxNotify&rdNotifyCommand=DeleteCustomDashboardPanel' + rdPanelParams)
    var dtPanelList = document.getElementById('dtPanelList');   //#12552.
    dtPanelList.childNodes[0].childNodes[nRowNr-1].style.display='none';
}

var rdPanelParams;
var rdPanelParamNames;
function rdSaveDashboardParams(sPanelElementID) {

	var sErrorMsg = rdValidateForm()
	if (sErrorMsg) {
		alert(sErrorMsg);
		return;
	}
	
	//Hide the Save button.
	var sPanelID = sPanelElementID.replace("rdDashboardPanel-","");
	/*var eleSave = document.getElementById("rdDashboardSave-" + sPanelID);
	eleSave.style.display = "none";*/
	var elePanel = document.getElementById(sPanelElementID);	
	
	//Update the Panel Caption		
	if (document.getElementById('rdDashboardPanelRenameDiv-' + sPanelID)) {
		var divCaption = document.getElementById('rdDashboardPanelCaptionDiv-' + sPanelID);
		var captionEdit = document.getElementById('rdDashboardPanelRenameDiv-' + sPanelID);
		if (captionEdit.style.display != 'none') {		
			if (divCaption.firstChild.innerHTML != captionEdit.firstChild.firstChild.value) divCaption.firstChild.innerHTML = captionEdit.firstChild.firstChild.value;
			ShowElement(this.id, captionEdit.id + ',' + divCaption.id, 'Toggle', '');			
		}
	}	

    //Refresh the panel with updated parameters.
    var elePanel = document.getElementById(sPanelElementID);
    rdPanelParams = "";
    rdPanelParamIDs = "";
    rdGetRecursiveInputValues(elePanel);
    rdPanelParams += "&rdReport=" + document.getElementById("rdDashboardDefinition").value;
    window.status = "Saving dashboard panel parameters.";
    elePanel.style.cursor = "wait";
    rdAjaxRequestWithFormVars('rdAjaxCommand=rdAjaxNotify&rdNotifyCommand=SaveDashboardParams&PanelID=' + sPanelID + rdPanelParams + "&ParamIDs=" + rdPanelParamIDs);
        		
	ShowElement(this.id, sPanelElementID.replace('rdDashboardPanel-','rdDashboardEdit-') + ',' + 
						 sPanelElementID.replace('rdDashboardPanel-','rdDashboardCancel-') + ',' + 
						 sPanelElementID.replace('rdDashboardPanel-','rdDashboard2PanelParams-') + ',' +                              
						 sPanelElementID.replace('rdDashboardPanel-','rdDashboardRemove-') + ',' + 
						 sPanelElementID.replace('rdDashboardPanel-','rdDashboardChangePanel-'),
						 'Toggle', '');
							 
	var rdFreeformLayout = document.getElementById('rdFreeformLayout');
	if(rdFreeformLayout)
        rdResizePanelContentOnEditCancelSave(null, [sPanelElementID.replace('rdDashboardPanel-', 'rdDashboard2PanelContent_'), 
                                                    sPanelElementID, 'Save']);
    
}

function rdDashboardHidePanelParams() {
    //Hide all open parameter panels.
    var eleParams = document.getElementsByTagName("TR")  
    for (var i=0; i < eleParams.length; i++) {
        var eleParam = eleParams[i]
        if (eleParam.id) {
            if (eleParam.id.indexOf("rdDashboard2PanelParams-") == 0) {
                if (eleParam.style.display!='none') {
                    var sId = eleParam.id.substr(24)
                    var sPanelID = sId.substring(0, sId.lastIndexOf('_'));
                    var eleRefreshForCancel = document.getElementById('rdRefreshForCancel-' + sId);
                    if(eleRefreshForCancel != null){
                        var sRefreshElementIds = 'rdDashboardParamsID-' + sId + ',' + 'rdDashboardPanelRename-' + sId + ',' + sId + "," + sPanelID;
                        rdAjaxRequest('rdAjaxCommand=RefreshElement&rdRefreshElementID=' + sRefreshElementIds + '&rdReport=' + document.getElementById("rdDashboardDefinition").value,'false',null)
                    }else{
                        ShowElement(this.id,'rdDashboardEdit-' + sId + ',rdDashboardCancel-' + sId + ',rdDashboard2PanelParams-' + sId + ',rdDashboardRemove-' + sId, 'Toggle');
    					
					    //Handle caption/caption edit separately.  They get out of sync
					    var captionEdit = document.getElementById('rdDashboardPanelRenameDiv-' + sId);
					    if (captionEdit && captionEdit.style.display != 'none') {
						    ShowElement(this.id,'rdDashboardPanelRenameDiv-' + sId, 'Hide');
						    ShowElement(this.id, 'rdDashboardPanelCaptionDiv-' + sId, 'Show');
					    }
					}
//					
					//Reset panel size for freeform dashboard
					rdResizePanelContentOnEditCancelSave(null, ['rdDashboard2PanelContent_' + sId, 
                                                                'rdDashboardPanel-' + sId, 'Save']);
                }
            }
        }
    }
}

function rdEditDashboardPanel(sDashboardPanelID){
    var eleDashboardPanel = document.getElementById(sDashboardPanelID);
    var sId = sDashboardPanelID.substring(17)
    rdDashboardHidePanelParams();
    ShowElement(this.id, 'rdDashboardEdit-' + sId + ',rdDashboardCancel-' + sId + ',rdDashboard2PanelParams-' + sId + ',rdDashboardRemove-' +  sId + ',rdDashboardPanelCaptionDiv-' + sId + ',rdDashboardPanelRenameDiv-' + sId + ',rdDashboardChangePanel-' + sId,'Toggle','FadeIn')	
    var rdDashboardPanelCaptionDiv =  document.getElementById('rdDashboardPanelCaptionDiv-' + sId)
    var rdDashboardPanelEditCancelDiv = document.getElementById('rdDashboardCancel-' + sId)
    try{
        if(rdDashboardPanelCaptionDiv){ //#13683.
            if(rdDashboardPanelEditCancelDiv){
                if(rdDashboardPanelEditCancelDiv.outerHTML){
                    if(rdDashboardPanelEditCancelDiv.outerHTML.indexOf('rdIgnore') != -1)
                        ShowElement(this.id, rdDashboardPanelCaptionDiv, 'Show');
                }
                else if(new XMLSerializer()){ 
                    if(new XMLSerializer().serializeToString(rdDashboardPanelEditCancelDiv).indexOf('rdIgnore') != -1)
                        ShowElement(this.id, rdDashboardPanelCaptionDiv, 'Show');
                }        
            }
        }
    }catch(e){}
   
    rdPreventDragFromRename('rdDashboardPanelRename-' + sId)
    //Code for Gauge from AG.
    var eleDashboardPanelParams = document.getElementById('rdDashboard2PanelParams-' + sId);
    var aDashboardParamsImages = eleDashboardPanelParams.getElementsByTagName("img");
    for(i=0;i<aDashboardParamsImages.length;i++){
        var eleParamsImg = aDashboardParamsImages[i]
        if(eleParamsImg.id == "imgGaugeRings"){
            if(eleParamsImg.src.indexOf("Green") == -1){
                ShowElement(this.id,'colGaugeRingsColorSelection','Hide');
                return;
            }
        }
    }
}

function rdGetRecursiveInputValues(eleParent) {
	for (var i = 0; i < eleParent.childNodes.length; i++) {
	    var eleCurr = eleParent.childNodes[i]
	    //if (eleCurr.nodeName == "INPUT") {
	    switch (eleCurr.type) {
			case 'hidden':  
			case 'text':  
			case 'email':  
			case 'number':  
			case 'tel':  
			case 'textarea':  
			case 'password':  
			case 'select-one':  
			case 'rdRadioButtonGroup':  
			case 'file':  
				var sValue = rdGetFormFieldValue(eleCurr)
				rdPanelParams += '&' + eleCurr.name + "=" + rdAjaxEncodeValue(sValue)
				rdPanelParamIDs += ":" + eleCurr.name
				break;
			case 'select-multiple':
				var selectedItems = new Array(); 
				for (var k = 0; k < eleCurr.length; k++) { 
					if (eleCurr.options[k].selected) {
						selectedItems[selectedItems.length] = eleCurr.options[k].value
					}
				} 
				if (typeof window.rdInputValueDelimiter == 'undefined'){window.rdInputValueDelimiter=','}
				var sValue = selectedItems.join(rdInputValueDelimiter)
				rdPanelParams += '&' + eleCurr.name + "=" + rdAjaxEncodeValue(sValue)
				rdPanelParamIDs += ":" + eleCurr.name
				break;
			case 'checkbox':
				if (eleCurr.checked) {
					var sValue = rdGetFormFieldValue(eleCurr)
					rdPanelParams += '&' + eleCurr.name + "=" + rdAjaxEncodeValue(sValue)
				    rdPanelParamIDs += ":" + eleCurr.name
				}
				break;
			default:			    
			    if(eleCurr.getAttribute){   //#14917.
    	            if(eleCurr.getAttribute("type") == 'rdRadioButtonGroup'){
		                var sValue = rdGetFormFieldValue(eleCurr)
			            rdPanelParams += '&' + eleCurr.getAttribute("name") + "=" + rdAjaxEncodeValue(sValue)
			            rdPanelParamIDs += ":" + eleCurr.getAttribute("name")
			            break;
			        }else{
		                rdGetRecursiveInputValues(eleCurr);
			            break;
		            }
		        }else{
		            //Not an input element.
		            rdGetRecursiveInputValues(eleCurr);
			        break;
		        }
		}
	}
}

function rdRenameDashboardTab() {
    var sTabID = document.getElementById("rdActiveTabId_rdDashboardTabs").value
    var eleTab = document.getElementById(sTabID)
    var sNewName = document.getElementById("txtRenameTab").value
    var sOldName
    if (eleTab.textContent) {
        sOldName = eleTab.textContent
    }else{
        sOldName = eleTab.innerText //IE
    }
    if (sNewName.replace(/ /g, '').length == 0) {
        document.getElementById("txtRenameTab").value = sOldName 
        return
    }
    
    //Update the tab in the browser. Find it
    if (eleTab.textContent) {
        eleTab.firstChild.firstChild.textContent = sNewName
    }else{
        eleTab.firstChild.firstChild.innerText = sNewName //IE
    }
    
    //Report the new name back to the server.
    var rdParams = "&rdReport=" + document.getElementById("rdDashboardDefinition").value
    rdParams += "&TabID=" + sTabID
    rdParams += "&NewName=" + rdAjaxEncodeValue(sNewName)
    bSubmitFormAfterAjax = true //13690
    rdAjaxRequest('rdAjaxCommand=rdAjaxNotify&rdNotifyCommand=RenameDashboardTab' + rdParams)
}

function rdMoveDashboardTab(sDirection) {
    var sTabID = document.getElementById("rdActiveTabId_rdDashboardTabs").value
    var eleTab = document.getElementById(sTabID)
    var bMoveOK = false
	switch (sDirection) {
		case 'Left':
		    var sibling = eleTab.previousSibling
		    if(sibling){
		        eleTab.parentNode.insertBefore(eleTab.parentNode.removeChild(eleTab),sibling)
		        bMoveOK = true
		    }
			break;
		case 'Right':
		    var sibling = eleTab.nextSibling
		    if(sibling){
		        if (sibling.getAttribute("id") != "rdTabAddNewTab") { //Don't move past the "Add Tab".
		            if(sibling.nextSibling){
		                eleTab.parentNode.insertBefore(eleTab.parentNode.removeChild(eleTab),sibling.nextSibling)
		                bMoveOK = true
		            }else{
		                eleTab.parentNode.appendChild(eleTab.parentNode.removeChild(eleTab))
		                bMoveOK = true
		            }
		        }
		    }
			break;
	}

    //Report the new name back to the server.
    if (bMoveOK) {
        var rdParams = "&rdReport=" + document.getElementById("rdDashboardDefinition").value
        rdParams += "&TabID=" + sTabID
        rdParams += "&Direction=" + sDirection
        rdAjaxRequest('rdAjaxCommand=rdAjaxNotify&rdNotifyCommand=MoveDashboardTab' + rdParams)
    }

}

function rdMovePanelToTab(sPanelElementID, sTabID, eleDropZone) {
    var elePanel = document.getElementById(sPanelElementID)
    if (elePanel) {
        //Remove the panel and its drop zone.
        var eleDropZoneBelow = document.getElementById(sPanelElementID.replace("rdDashboardPanel","rdDashboardDropZone")) 
        elePanel.parentNode.removeChild(elePanel)
        if(eleDropZoneBelow)
            eleDropZoneBelow.parentNode.removeChild(eleDropZoneBelow)
        
        //Reset the tab's class.
        eleDropZone.firstChild.firstChild.firstChild.className = eleDropZone.firstChild.firstChild.firstChild.className.replace(" rdDashboardDropTabActive","")

        //Report the new name back to the server.
        var rdParams = "&rdReport=" + document.getElementById("rdDashboardDefinition").value
        rdParams += '&PanelInstanceID=' + rdGetPanelInstanceId(elePanel)
        rdParams += "&TabID=" + sTabID
        rdAjaxRequest('rdAjaxCommand=rdAjaxNotify&rdNotifyCommand=MoveDashboardPanelToTab' + rdParams)
    }      
}

function rdSetDashboardColumns() {
    var sTabID = document.getElementById("rdActiveTabId_rdDashboardTabs").value
    var eleTab = document.getElementById(sTabID)
    var nColumnCount = document.getElementById("lstColumnCount").value
    
    var nWideColumn =  rdGetFormFieldValue(document.getElementById('rdRadioButtonGroupradWideColumn'))
    
    //Report the new column count back to the server.
    var rdParams = "&rdReport=" + document.getElementById("rdDashboardDefinition").value
    rdParams += "&TabID=" + sTabID
    rdParams += "&NewColumnCount=" + nColumnCount;//(nColumnCount == 'Free-form' ? 0 : nColumnCount);
    rdParams += "&NewWideColumn=" + nWideColumn
    rdAjaxRequest('rdAjaxCommand=rdAjaxNotify&rdNotifyCommand=SetDashboardTabColumns' + rdParams)
    
    if(nColumnCount == 'Free-form'){   // Freeform Layout.
    document.getElementById('divFreeformPanels').style.display = ''
    document.getElementById("divWideColumn").style.display = 'none';
    var eleFreeformLayout = document.getElementById('rdFreeformLayout');
    if (eleFreeformLayout!= null) 
        if(eleFreeformLayout.id.toLowerCase() == 'rdhiddenrequestforwarding') 
            eleFreeformLayout = null;   //#14758.
    if(eleFreeformLayout == null){
        eleFreeformLayout = document.createElement("INPUT");
        eleFreeformLayout.setAttribute("TYPE", "HIDDEN")
        eleFreeformLayout.setAttribute("ID", "rdFreeformLayout")
        eleFreeformLayout.setAttribute("NAME", "rdFreeformLayout")
        eleFreeformLayout.style.display = 'none';   //#14663.
        eleTab.appendChild(eleFreeformLayout);
    }
    return;
    }else{
        var eleDivFreeformPanels = document.getElementById('divFreeformPanels');
        if(eleDivFreeformPanels) eleDivFreeformPanels.style.display = 'none';
        var eleFreeformLayout = document.getElementById('rdFreeformLayout');
        if(eleFreeformLayout) eleFreeformLayout.parentNode.removeChild(eleFreeformLayout);
    }
    //Update the UI for the Layout by fixing the image names.
    var eleRadioButtons = document.getElementById("rdRadioButtonGroupradWideColumn").parentNode
    var divRadioButtons = document.getElementById("divWideColumn")
    if (nColumnCount == 1) {
        divRadioButtons.style.display="none"
    } else {
        divRadioButtons.style.display=""
        if (eleRadioButtons.childNodes.length == 1) {
            eleRadioButtons = eleRadioButtons.firstChild.firstChild //For non-IE.
        }
        for (var i=0; i < eleRadioButtons.childNodes.length; i++) {
            var ele = eleRadioButtons.childNodes[i]
                if (typeof ele.tagName != "undefined") {
                if (ele.tagName.toUpperCase() == "IMG") {
                    if (ele.src.indexOf("rdLayout") != -1) {
                        var src = ele.src.substring(0,ele.src.indexOf("rdLayout") + 8)
                        src += nColumnCount
                        src += ele.src.substring(src.indexOf("rdLayout") + 9)
                        ele.src = src
                    }
                }
            }
        }
    }
}

function rdGetPanelInstanceId(elePanel) {
    //The instance ID is the  string after the last "-" from the ID.
    return elePanel.id.substr(elePanel.id.lastIndexOf("_") + 1)
}

function rdRemoveDashboardParams() {
    //Don't pass on values from the dashboard paramers. They are only needed when saving params.13456
    var elePanelParams = document.getElementById("rdDashboardParams")
    while (elePanelParams) {
        elePanelParams.parentNode.removeChild(elePanelParams)
        elePanelParams = document.getElementById("rdDashboardParams")
    }
}

function rdMoveMobileDashboardPanel(sDashboardPanelId, sChangePanelAction, sPopupPanelID){
    // function to move the Mobile Dashboard Panels around.
    ShowElement(this.id, sPopupPanelID, 'Toggle');
    var elePanel = document.getElementById(sDashboardPanelId);
    var elePanelDropZone = elePanel.nextSibling;
    var elePanelPreviousSibling = elePanel.previousSibling;
    var elePanelNextSibling = elePanelDropZone.nextSibling;
    switch(sChangePanelAction){ 
        case "MoveUp":
            if(elePanelPreviousSibling.id.match('rdDashboardDropZone-0-0')){
                break;
            }else{
                var eleNewDropZone = elePanel.previousSibling.previousSibling
                elePanel.parentNode.removeChild(elePanel)
                elePanelDropZone.parentNode.removeChild(elePanelDropZone)
                eleNewDropZone.parentNode.insertBefore(elePanel, eleNewDropZone)
                eleNewDropZone.parentNode.insertBefore(elePanelDropZone, eleNewDropZone)
                rdSaveDashboardOrder();
                break;
            }
        case "MoveDown":
            if(elePanelNextSibling == null){
                break;
            }else{
                var eleNewDropZone = elePanelDropZone.nextSibling.nextSibling.nextSibling;
                if(eleNewDropZone == null){
                    elePanel.parentNode.removeChild(elePanel)
                    elePanelDropZone.parentNode.removeChild(elePanelDropZone)
                    elePanelPreviousSibling.parentNode.appendChild(elePanel)
                    elePanelPreviousSibling.parentNode.appendChild(elePanelDropZone)
                    rdSaveDashboardOrder();
                    break;
                }
                elePanel.parentNode.removeChild(elePanel)
                elePanelDropZone.parentNode.removeChild(elePanelDropZone)
                eleNewDropZone.parentNode.insertBefore(elePanel, eleNewDropZone)
                eleNewDropZone.parentNode.insertBefore(elePanelDropZone, eleNewDropZone)
                rdSaveDashboardOrder();
                break;
            }
        case "Edit":
            rdEditDashboardPanel(sDashboardPanelId)
            break;
        case "Remove":
            if(confirm("Are you sure?"))
                rdRemoveDashboardPanel(sDashboardPanelId);
            break;
        default:
            rdMovePanelToTab(sDashboardPanelId, sChangePanelAction, elePanelDropZone)
            break;        
    } 
}

function rdNavigateBetweenDashboardTabs(sTabIdentifier, sPopupPanelID){
    // function runs on picking a Tab from the Tabs menu.
    ShowElement(this.id, sPopupPanelID, 'Hide');    
    rdTabs_rdDashboardTabs.set('activeIndex',0);    // Set the Index back to 0.
    document.getElementById('rdActiveTabIndex_rdDashboardTabs').value = '0';
    document.getElementById('rdActiveTabId_rdDashboardTabs').value = sTabIdentifier;    // Set this Value to the New Tab Id.
    var sReportName = document.getElementById('rdDashboardDefinition').value;
    SubmitForm('rdPage.aspx?rdReport=' + sReportName + '&rdRequestForwarding=Form','', 'false');
}

function rdShowChangePanelMenu(sPopupPanelID, sDashboardPanelId){
    ShowElement(this.id, sPopupPanelID, 'Toggle');
    try{
        var elePopupPanel = document.getElementById(sPopupPanelID);
        var elePanel = document.getElementById(sDashboardPanelId);
        var elePanelPreviousSibling = elePanel.previousSibling;
        var elePanelNextSibling = elePanel.nextSibling.nextSibling;
        if(elePanelPreviousSibling.id.match('rdDashboardDropZone-0-0')){
            var eleOptionsList = elePopupPanel.getElementsByTagName('td')
            for(i=0; i<eleOptionsList.length; i++){
                var eleTD = eleOptionsList[i]
                if (eleTD.outerHTML.match('MoveUp')){
                    if(eleTD.getAttribute('id')){
                          if(eleTD.getAttribute('id').match('rdDataMenuTable_Row'))
                            eleTD.parentNode.style.display = 'none';
                    }
                }
            }
            
        }else{
            var eleOptionsList = elePopupPanel.getElementsByTagName('td')
            for(i=0; i<eleOptionsList.length; i++){
                var eleTD = eleOptionsList[i]
                if (eleTD.outerHTML.match('MoveUp')){
                    if(eleTD.getAttribute('id')){
                         if(eleTD.getAttribute('id').match('rdDataMenuTable_Row'))
                            eleTD.parentNode.style.display = '';
                    }
                }
            }
        }
        
         if(!elePanelNextSibling){
            var eleOptionsList = elePopupPanel.getElementsByTagName('td')
            for(i=0; i<eleOptionsList.length; i++){
                var eleTD = eleOptionsList[i]
                if (eleTD.outerHTML.match('MoveDown')){
                    if(eleTD.getAttribute('id')){
                          if(eleTD.getAttribute('id').match('rdDataMenuTable_Row'))
                            eleTD.parentNode.style.display = 'none';
                    }
                }
            }
            
        }else{
            var eleOptionsList = elePopupPanel.getElementsByTagName('td')
            for(i=0; i<eleOptionsList.length; i++){
                var eleTD = eleOptionsList[i]
                if (eleTD.outerHTML.match('MoveDown')){
                    if(eleTD.getAttribute('id')){
                          if(eleTD.getAttribute('id').match('rdDataMenuTable_Row'))
                            eleTD.parentNode.style.display = '';
                    }
                }
            }
        }
    }
    catch(e){}
}


function ShowGaugeRingsColorSelection(sTextboxId){
    var eleDashboardPanel = Y.Selector.ancestor(document.getElementById(sTextboxId), '.rdDashboardPanel');
    var sPanelInstanceID = eleDashboardPanel.id.substring(eleDashboardPanel.id.lastIndexOf('_')+1);
    var nGoal1 = document.getElementById('txtGoal1_' + sPanelInstanceID).value;
    var nGoal2 = document.getElementById('txtGoal2_' + sPanelInstanceID).value;
    var eleGaugeRingsColorSequence =  document.getElementById('rdAgGaugeRingsColorSequence_' + sPanelInstanceID);
    var imgGoalRings = document.getElementById('imgGaugeRings');
    if(nGoal1 > 0 || nGoal2 > 0){
        ShowElement(this.id,'colGaugeRingsColorSelection','Show');
    }
    if ((nGoal1 == '' && nGoal2 == '') || (nGoal1 == 0 && nGoal2 == 0)){
        ShowElement(this.id,'colGaugeRingsColorSelection','Hide');
        eleGaugeRingsColorSequence.value = "";
    }else if(nGoal1 == '' || nGoal2 == ''){
        ShowElement(this.id,'colGaugeRingsColorSelection','Show');
        imgGoalRings.src = 'rdTemplate/rdAnalysisGrid/Red-Green.png';
        eleGaugeRingsColorSequence = 'Red-Green';
    }else if(nGoal1 == 0 || nGoal2 == 0){
        imgGoalRings.src = 'rdTemplate/rdAnalysisGrid/Red-Green.png';
        eleGaugeRingsColorSequence = 'Red-Green';
    }else if(nGoal1 > 0 && nGoal2 > 0){
        ShowElement(this.id,'colGaugeRingsColorSelection','Show');
        if(eleGaugeRingsColorSequence.value != 'Red-Yellow-Green'){
            imgGoalRings.src = 'rdTemplate/rdAnalysisGrid/Red-Yellow-Green.png';
            eleGaugeRingsColorSequence.value = 'Red-Yellow-Green';
        }
    }
}

function ApplyGaugeRingColors(imgObj){
//    if(document.rdForm.rdAgChartType.value != "Gauge") return;
    var eleDashboardPanel = Y.Selector.ancestor(imgObj, '.rdDashboardPanel');
    var sPanelInstanceID = eleDashboardPanel.id.substring(eleDashboardPanel.id.lastIndexOf('_')+1);
    var nGoal1 = document.getElementById('txtGoal1_' + sPanelInstanceID).value;
    var nGoal2 = document.getElementById('txtGoal2_' + sPanelInstanceID).value;
    var eleGaugeRingsColorSequence = document.getElementById('rdAgGaugeRingsColorSequence_' + sPanelInstanceID);
    var sColorSequence = imgObj.src.substring(imgObj.src.lastIndexOf("/") + 1, imgObj.src.indexOf('.png'));
    eleGaugeRingsColorSequence.value = sColorSequence;
    switch(sColorSequence){
        case "Red-Green":
            imgObj.src = imgObj.src.replace(sColorSequence, "Green-Red");
            break;
        case "Green-Red":
            imgObj.src = imgObj.src.replace(sColorSequence, "Red-Green");
            break;
        case "Green-Yellow-Red":
            imgObj.src = imgObj.src.replace(sColorSequence, "Red-Yellow-Green");
            break;
        case "Red-Yellow-Green":
            imgObj.src = imgObj.src.replace(sColorSequence, "Green-Yellow-Red");
            break;
    } 
}

