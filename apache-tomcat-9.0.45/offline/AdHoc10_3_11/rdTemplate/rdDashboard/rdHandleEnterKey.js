﻿//Prevent submits when the Enter key is pressed while inside an INPUT.
function rdDashboardHandleEnterKey(evt) {
    var evt  = (evt) ? evt : ((event) ? event : null);
    if (evt.keyCode == 13) {
        var ele = (evt.target) ? evt.target : ((evt.srcElement) ? evt.srcElement : null)
        if (ele.id == "txtRenameTab") {
            rdRenameDashboardTab()
            return false
        }
    }
}
document.onkeypress = rdDashboardHandleEnterKey;