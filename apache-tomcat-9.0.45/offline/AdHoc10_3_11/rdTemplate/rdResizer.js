//General resize yui namespace
YUI.add('rdResize', function(Y) {

	Y.namespace('rdResize').AddYUIResizerHandles = function(sContentHolderID, eleAttrs){			
		// Function includes the common code used to instantiate a YUI object and wrap YUI handles around the Container.		
		var yuiResize,
			resizeeNode = Y.one('#' + sContentHolderID );
		
		if (sContentHolderID.indexOf('rdDashboardPanel-') == 0) {
			yuiResize = new Y.Resize({
				node: resizeeNode,
				handles: 'l,bl,b,br,r'				
			});
			yuiResize.plug(Y.Plugin.ResizeConstrained, {
				minWidth: 200,
				minHeight: 100
			});
		} else {
			yuiResize = new Y.LogiXML.ChartFX.Resize({
				node: resizeeNode,
				wrap: true,
				handles: 'b,br,r'
			});
		}
		
		// Add the node being resized as an EventTarget
		yuiResize.addTarget( resizeeNode );
		
		if (eleAttrs.getAttribute('rdMinWidth') 
		|| eleAttrs.getAttribute('rdMinHeight') 
		|| eleAttrs.getAttribute('rdMaxWidth') 
		|| eleAttrs.getAttribute('rdMaxHeight')) {
		
			yuiResize.plug(Y.Plugin.ResizeConstrained, {
				minWidth: parseInt(eleAttrs.getAttribute('rdMinWidth')),
				minHeight: parseInt(eleAttrs.getAttribute('rdMinHeight')),
				maxWidth: parseInt(eleAttrs.getAttribute('rdMaxWidth')),
				maxHeight: parseInt(eleAttrs.getAttribute('rdMaxHeight'))
			});			
		}		
								
		return yuiResize;	
	}
	
}, '10.1.100', {
	requires:['base', 'resize', 'stylesheet', 'event-custom', 'chartfx-resize']
});

Y.use('rdResize', function(Y) {	
	//Resize function used by rdChart.js
	LogiXML.Resize._rdInitResizer = function(eleContent) {

		var resizeNode = Y.one( eleContent ),
			sIdForAttrs = eleContent.id;
			
		if(sIdForAttrs.lastIndexOf("_Row") != -1) {
			//For rows in tables
			sIdForAttrs = sIdForAttrs.substr(0,sIdForAttrs.lastIndexOf("_Row"))
		}
		
		// Resize already created
		if ( resizeNode.hasClass('yui3-resize') ) {
			return;
		}

		var eleAttrs = document.getElementById("rdResizerAttrs_" + sIdForAttrs)
		if (!Y.Lang.isValue(eleAttrs))
			return;
				
		eleContent.removeAttribute('Height');
		eleContent.removeAttribute('Width');	
		
		var yuiResize = Y.rdResize.AddYUIResizerHandles(eleContent.id, eleAttrs);
		
		yuiResize.on('resize:end', function(e) {
			
			var src = eleContent.src
			if (src.indexOf("rdChart2.aspx")!=-1) {
				if (src.indexOf("&rdResizerNewWidth=")!=-1){
					src = src.substr(0,src.indexOf("&rdResizerNewWidth="))
				}
				src += "&rdResizerNewWidth=" + eleContent.width  + "&rdResizerNewHeight=" + eleContent.height 
				
				if (document.rdForm.rdAgId) {
					src += "&rdAgId=" + document.rdForm.rdAgId.value
				}				
							
				eleContent.src = src + '&rdRequestForwarding=Form'; //#12431.
			} 

			/*if (rdForm.rdAgId) {
				//Update the AG with the new size.
				rdAjaxRequest('rdAjaxCommand=rdAjaxNotify&rdAgId=' + rdForm.rdAgId.value + '&rdNotifyCommand=SaveAgChartSize&ChartID=' + eleContent.id + '&ChartWidth=' + eleContent.width + '&ChartHeight=' + eleContent.height)
			}*/
			//Update the OG with the new size.
			if (document.rdForm.rdOgId)
				rdAjaxRequest('rdAjaxCommand=rdAjaxNotify&rdOgId=' + document.rdForm.rdOgId.value + '&rdNotifyCommand=SaveOgChartSize&ChartWidth=' + eleContent.width + '&ChartHeight=' + eleContent.height);				
			
		});		
	};
	
	//Resize animated charts
	LogiXML.Resize._rdInitAnimatedChartResizer = function(sAnimatedChartMovieType, sAnimatedChartID, sAnimatedChartDataFile, sAnimatedChartOriginalID, sBGColor) {
		// Function runs onLoad to wrap the Yui handles around the Div enclosing the Animated Chart.

		var eleAnimatedChart = document.getElementById(sAnimatedChartID);   // This is an Object, not a regular HTML element.
		if(!eleAnimatedChart) return;
		var width = parseInt(eleAnimatedChart.offsetWidth == 0 ? eleAnimatedChart.getAttribute('Width') : eleAnimatedChart.offsetWidth) // To handle issue under DataTable.
		var height = parseInt(eleAnimatedChart.offsetHeight == 0 ? eleAnimatedChart.getAttribute('Height') : eleAnimatedChart.offsetHeight)
		var eleAnimatedChartContentHolder = document.getElementById('rdAnimatedChart'+ sAnimatedChartID);  // Div that holds the Chart.
		if((eleAnimatedChart.tagName.match('OBJECT'))||(eleAnimatedChart.tagName.match('EMBED'))){               
			eleAnimatedChartContentHolder.style.width = width + 5;
			eleAnimatedChartContentHolder.style.height = height + 5;
			eleAnimatedChartContentHolder.src = 'javascript:void(0)';
		}
		else return;
		
		var eleAttrs = document.getElementById("rdResizerAttrs_" + sAnimatedChartOriginalID);
		if (!Y.Lang.isValue(eleAttrs))
			return;
		
		try{    
			var sReportID = document.getElementById(sAnimatedChartOriginalID + '-Hidden').value; 
		}
		catch(e){   // Under a DataTable.
			if(!sReportID){
				var sRowIdentifier = eleAnimatedChartContentHolder.parentNode.id.substring(eleAnimatedChartContentHolder.parentNode.id.lastIndexOf('_'),eleAnimatedChartContentHolder.parentNode.id.length);
				
				sReportID = document.getElementById(sAnimatedChartOriginalID + '-Hidden' + sRowIdentifier).value;				
			}   
		}    

		if(sAnimatedChartOriginalID.indexOf('_') != -1){
			if(sAnimatedChartOriginalID.substring(sAnimatedChartOriginalID.lastIndexOf('_'), sAnimatedChartOriginalID.length).length > 32)  // Handle Dashboards.
				sAnimatedChartOriginalID = sAnimatedChartOriginalID.substring(0, sAnimatedChartOriginalID.lastIndexOf('_'));    // Take the GUID off and pass the Original ID back.
		}
		
		if (eleAnimatedChartContentHolder.className.indexOf('yui3-resize') == 0)
				return;
		
		var yuiResize = Y.rdResize.AddYUIResizerHandles(eleAnimatedChartContentHolder.id, eleAttrs);
		yuiResize.get('wrapper').setStyles({			
			width: eleAnimatedChart.width,
			height: eleAnimatedChart.height
		});			

		yuiResize.on('resize:end', function(e) {
			var eleAnimatedChartResizing = e.target.get('node').one('embed, object').getDOMNode();
			// event triggered after the resize is done.      
			if(eleAnimatedChartResizing){
				eleAnimatedChartContentHolder.style.backgroundColor = "transparent";
				eleAnimatedChartResizing.style.visibility = '';
			}    
			
			if((eleAnimatedChartContentHolder.style.width != width)|(eleAnimatedChartContentHolder.style.height != height)){
				var currWidth = parseInt(e.target.get('wrapper').getStyle('width')), currHeight = parseInt(e.target.get('wrapper').getStyle('height'));
				eleAnimatedChartResizing.parentNode.removeChild(eleAnimatedChartResizing);           
			   rdAjaxRequest('rdAjaxCommand=RefreshElement&rdRefreshElementID=' + sAnimatedChartOriginalID + ',' + sAnimatedChartID + ',' + eleAnimatedChartContentHolder.id + '&rdAnimatedChartCurrentWidth=' + currWidth + '&rdAnimatedChartCurrentHeight=' + currHeight +  '&rdCurrentAnimatedChartId=' + sAnimatedChartOriginalID + '&rdReport=' + sReportID + '&rdAnimatedChartResizerRefresh=True&rdRequestForwarding=Form');
			}       
		});    
		yuiResize.on('resize:start', function(e) {
			var eleAnimatedChartResizing = e.target.get('node').one('embed, object').getDOMNode();
			// event triggered after the resize has begun.
			//eleAnimatedChartContentHolder.style.backgroundColor = sBGColor;        
			eleAnimatedChartResizing.style.visibility = 'hidden';
		});	
		
	};
	
	//Resize Java Applets
	LogiXML.Resize._rdInitAppletResizer = function(AppletID, sBGColor) {
		// Function runs onLoad to wrap the Yui handles around the Div enclosing the Applet (HeatMap, IDV).

		var eleApplet = document.getElementById(AppletID);  // This is an Object, not a regular HTML element.
		if(!eleApplet) return;
		var width = parseInt(eleApplet.offsetWidth == 0 ? eleApplet.getAttribute('Width') : eleApplet.offsetWidth) // To handle issue under DataTable.
		var height = parseInt(eleApplet.offsetHeight == 0 ? eleApplet.getAttribute('Height') : eleApplet.offsetHeight)
		var eleAppletContentHolder = document.getElementById("Applet_" + AppletID);
		if((eleApplet.tagName.match('APPLET'))||(eleApplet.tagName.match('OBJECT'))){   // #  11632.           
			if(!eleAppletContentHolder){    // Applet needs to be Wrapped in a Div.
				eleAppletContentHolder = document.createElement("Div");
				eleAppletContentHolder.setAttribute("id", "Applet_" + AppletID);
				var eleContentParent = eleApplet.parentNode;
				eleApplet.parentNode.removeChild(eleApplet);
				eleContentParent.appendChild(eleAppletContentHolder);
				eleAppletContentHolder.appendChild(eleApplet);             
			}        
			eleAppletContentHolder.src = 'javascript:void(0)';
		}
		else return;

		var eleAttrs = document.getElementById("rdResizerAttrs_" + AppletID);
		if (!Y.Lang.isValue(eleAttrs))
			return;
				 
		try{    
			var sReportID = document.getElementById(AppletID + '-Hidden').value;  
		}
		catch(e){   // Under a DataTable.
			if(!sReportID){
				var sRowIdentifier = eleAppletContentHolder.parentNode.id.substring(eleAppletContentHolder.parentNode.id.lastIndexOf('_'),eleAppletContentHolder.parentNode.id.length);
				
				sReportID = document.getElementById(AppletID + '-Hidden' + sRowIdentifier).value;				
			}   
		}    

		if(AppletID.substring(AppletID.lastIndexOf('_'), AppletID.length).length > 32)  // To handle Dashboards.
			AppletID = AppletID.substring(0, AppletID.lastIndexOf('_'));                // Cut the ID off the GUID so that the server finds the definiton on Ajax request.
		
		if (eleAppletContentHolder.className.indexOf('yui3-resize') == 0)
				return;
						
		var yuiResize = Y.rdResize.AddYUIResizerHandles(eleAppletContentHolder.id, eleAttrs);	    		
		yuiResize.get('wrapper').setStyles({
			padding: '10',
			width: eleApplet.width,
			height: eleApplet.height
		});		
		
		yuiResize.on('resize:end', function(e) {
			if(eleApplet){
				eleAppletContentHolder.style.backgroundColor = "transparent";        
				eleApplet.style.visibility = '';
			}

			if((eleAppletContentHolder.offsetWidth != eleApplet.offsetWidth)|(eleAppletContentHolder.offsetHeight != eleApplet.offsetHeight)){
				var currWidth = parseInt(e.target.get('wrapper').getStyle('width')), currHeight = parseInt(e.target.get('wrapper').getStyle('height'));
				eleApplet.width = currWidth;
				eleApplet.height =  currHeight;
				rdAjaxRequest('rdAjaxCommand=RefreshElement&rdRefreshElementID=' + AppletID + '&rdAppletCurrentWidth=' + currWidth + '&rdAppletCurrentHeight=' + currHeight + '&rdAppletId=' + AppletID + '&rdReport=' + sReportID +'&rdAppletResizerRefresh=True&rdRequestForwarding=Form');
			}
		});   
		yuiResize.on('resize:start', function(e) {
			eleAppletContentHolder.style.backgroundColor = sBGColor;        
			eleApplet.style.visibility = 'hidden';        
		});
		
	};
	
	//Resize Animated Maps
	LogiXML.Resize._rdInitAnimatedMapResizer = function(sAnimatedMapMovieType, sAnimatedMapID, sAnimatedMapDataFile, sAnimatedMapOriginalID, sBGColor) {
		// Function runs onLoad to wrap the Yui handles around the Div enclosing the Animated Map.

		var eleAnimatedMap = document.getElementById(sAnimatedMapID);   // This is an Object, not a regular HTML element.
		if(!eleAnimatedMap) return;
		var width = parseInt(eleAnimatedMap.offsetWidth == 0 ? eleAnimatedMap.getAttribute('Width') : eleAnimatedMap.offsetWidth) // To handle issue under DataTable.
		var height = parseInt(eleAnimatedMap.offsetHeight == 0 ? eleAnimatedMap.getAttribute('Height') : eleAnimatedMap.offsetHeight)
		var eleAnimatedMapContentHolder = document.getElementById('rdFusionMap'+ sAnimatedMapID);  // Div that holds the Chart.
		if((eleAnimatedMap.tagName.match('OBJECT'))||(eleAnimatedMap.tagName.match('EMBED'))){               
			if(!eleAnimatedMapContentHolder){
				var eleAnimatedMapContentHolder = document.createElement("Div");
				eleAnimatedMapContentHolder.setAttribute("id","rdFusionMap" + sAnimatedMapOriginalID);
				var eleContentParent = eleAnimatedMap.parentNode;   // Span that holds the Div.
				eleAnimatedMap.parentNode.removeChild(eleAnimatedMap);
				eleContentParent.appendChild(eleAnimatedMapContentHolder);
				eleAnimatedMapContentHolder.appendChild(eleAnimatedMap);           
			}        
			eleAnimatedMapContentHolder.src = 'javascript:void(0)';
		}
		else return;

		var eleAttrs = document.getElementById("rdResizerAttrs_" + sAnimatedMapOriginalID);
		if (!Y.Lang.isValue(eleAttrs))
			return;
					 
		try{    
			var sReportID = document.getElementById(sAnimatedMapOriginalID + '-Hidden').value;   
		}
		catch(e){   // Under a DataTable.
			if(!sReportID){
				var sRowIdentifier = eleAnimatedMapContentHolder.parentNode.id.substring(eleAnimatedMapContentHolder.parentNode.id.lastIndexOf('_'),eleAnimatedMapContentHolder.parentNode.id.length);
				
				var sReportID = document.getElementById(sAnimatedMapOriginalID + '-Hidden').value;				
			}   
		}   
		
		if (eleAnimatedMapContentHolder.className.indexOf('yui3-resize') == 0)
				return;
		
		var yuiResize = Y.rdResize.AddYUIResizerHandles(eleAnimatedMapContentHolder.id, eleAttrs);
		yuiResize.get('wrapper').setStyles({
			padding: '10',
			width: eleAnimatedMap.width,
			height: eleAnimatedMap.height
		});			

		yuiResize.on('resize:end', function(e) { 
			var eleAnimatedMapResizing = e.target.get('node').one('embed, object').getDOMNode();		
			// event triggered after the resize is done.      
			if(eleAnimatedMapResizing){
				eleAnimatedMapContentHolder.style.backgroundColor="transparent";
				eleAnimatedMapResizing.style.visibility = '';
			} 
			
			if((eleAnimatedMapContentHolder.style.width != width)|(eleAnimatedMapContentHolder.style.height != height)){
				var currWidth = parseInt(e.target.get('wrapper').getStyle('width')) - 20, currHeight = parseInt(e.target.get('wrapper').getStyle('height'));
				eleAnimatedMapResizing.parentNode.removeChild(eleAnimatedMapResizing);
				rdAjaxRequest('rdAjaxCommand=RefreshElement&rdRefreshElementID=' + sAnimatedMapOriginalID + ',' + sAnimatedMapID + ',' + eleAnimatedMapContentHolder.id + '&rdAnimatedMapCurrentWidth=' + currWidth + '&rdAnimatedMapCurrentHeight=' + currHeight +  '&rdCurrentAnimatedMapId=' + sAnimatedMapOriginalID + '&rdReport=' + sReportID + '&rdAnimatedMapResizerRefresh=True');
			}       
		});    
		yuiResize.on('resize:start', function(e) {
			var eleAnimatedMapResizing = e.target.get('node').one('embed, object').getDOMNode();	
			// event triggered after the resize has begun.     
			eleAnimatedMapResizing.style.visibility = 'hidden';
		});
		yuiResize.on('resize:resize', function(e) {
			// event triggered as you resize.
			eleAnimatedMapContentHolder.style.backgroundColor = sBGColor;       			
		});		
	};
	
	LogiXML.Resize._rdInitGoogleMapsResizer = function(GoogleMapID, eleGoogleMapObj) {
		// Function runs onLoad to wrap the Yui handles around the Div enclosing the Google Map.
		if(eleGoogleMapObj == undefined)
			return; 
		var eleGoogleMap = document.getElementById(GoogleMapID);    // This is a Div. But the Google Map sits on top of this Div
		if(!eleGoogleMap) return;
		var width = parseInt(eleGoogleMap.offsetWidth == 0 ? eleGoogleMap.style.pixelWidth : eleGoogleMap.offsetWidth) // To handle issue under DataTable.
		var height = parseInt(eleGoogleMap.offsetHeight == 0 ? eleGoogleMap.style.pixelHeight : eleGoogleMap.offsetHeight)
		var eleGoogleMapContentHolder = document.createElement("Div");  // Add a Div around the Google Map.
		eleGoogleMapContentHolder.setAttribute("id", "rdGoogleMap" + GoogleMapID);
		var eleContentParent = eleGoogleMap.parentNode;
		var eleNextSibling = eleGoogleMap.nextSibling;
		eleGoogleMap.parentNode.removeChild(eleGoogleMap);
		if (eleNextSibling != null)
			eleContentParent.insertBefore(eleGoogleMapContentHolder,eleNextSibling);
		else
			eleContentParent.appendChild(eleGoogleMapContentHolder);
		eleGoogleMapContentHolder.appendChild(eleGoogleMap);    
		eleGoogleMapContentHolder.src = 'javascript:void(0)';
		var center = eleGoogleMapObj.getCenter();

		var eleAttrs = document.getElementById("rdResizerAttrs_" + GoogleMapID);
		if (!Y.Lang.isValue(eleAttrs))
			return;
		
		try{    
			var sReportID = document.getElementById(GoogleMapID + '-Hidden').value;  
		}
		catch(e){   // Under a DataTable.
			if(!sReportID){
				var sRowIdentifier = eleGoogleMapContentHolder.parentNode.id.substring(eleGoogleMapContentHolder.parentNode.id.lastIndexOf('_'),eleGoogleMapContentHolder.parentNode.id.length);
				
				var sReportID = document.getElementById(GoogleMapID + '-Hidden').value;				
			}   
		}
		
		if (eleGoogleMapContentHolder.className.indexOf('yui3-resize') == 0)
				return;
				
		var yuiResize = Y.rdResize.AddYUIResizerHandles(GoogleMapID, eleAttrs);    		
		
		yuiResize.on('resize:end', function() {
			if((eleGoogleMapContentHolder.offsetWidth != eleGoogleMap.offsetWidth)|(eleGoogleMapContentHolder.offsetHeight != eleGoogleMap.offsetHeight)){
				rdAjaxRequest('rdAjaxCommand=RefreshElement&rdRefreshElementID=' + GoogleMapID + ',' + eleGoogleMapContentHolder.id + '&rdGoogleMapCurrentWidth=' + eleGoogleMap.offsetWidth + '&rdGoogleMapCurrentHeight=' + eleGoogleMap.offsetHeight +  '&rdGoogleMapId=' + GoogleMapID + '&rdReport=' + sReportID + '&rdGoogleMapResizerRefresh=True&rdRequestForwarding=Form');                      
				//eleGoogleMapObj.setCenter(center, zoom, type);   // Set the Center of the Map.          
			}
		});
		yuiResize.on('resize:start', function() { 
			center = eleGoogleMapObj.getCenter();   // Get the values at the beginning of the Resize.
		});     
		yuiResize.on('resize:resize', function() {
			eleGoogleMap.style.width = parseInt(eleGoogleMapContentHolder.offsetWidth) - 5 
			eleGoogleMap.style.height = parseInt(eleGoogleMapContentHolder.offsetHeight) - 5   
			
			google.maps.event.trigger(eleGoogleMapObj, 'resize');  // Resize the Map.
			eleGoogleMapObj.setCenter(center);   // Set the Center of the Map.          
		});	
	};		
});

//Expose YUI3 contained functions
function rdInitResizer(eleContent) {
	if (Y.Lang.isValue(LogiXML.Resize._rdInitResizer))
		LogiXML.Resize._rdInitResizer(eleContent);
	else
		setTimeout(function() {rdInitResizer(eleContent);}, 100);
}
function rdInitAnimatedChartResizer(sAnimatedChartMovieType, sAnimatedChartID, sAnimatedChartDataFile, sAnimatedChartOriginalID, sBGColor) {
	if (Y.Lang.isValue(LogiXML.Resize._rdInitAnimatedChartResizer))
		LogiXML.Resize._rdInitAnimatedChartResizer(sAnimatedChartMovieType, sAnimatedChartID, sAnimatedChartDataFile, sAnimatedChartOriginalID, sBGColor);
	else
		setTimeout(function() { rdInitAnimatedChartResizer(sAnimatedChartMovieType, sAnimatedChartID, sAnimatedChartDataFile, sAnimatedChartOriginalID, sBGColor); }, 100);
}
function rdInitAppletResizer(AppletID, sBGColor) {
	if (Y.Lang.isValue(LogiXML.Resize._rdInitAppletResizer))
		LogiXML.Resize._rdInitAppletResizer(AppletID, sBGColor);
	else
		setTimeout(function() { rdInitAppletResizer(AppletID, sBGColor); }, 100);
}
function rdInitAnimatedMapResizer(sAnimatedMapMovieType, sAnimatedMapID, sAnimatedMapDataFile, sAnimatedMapOriginalID, sBGColor) {
	if (Y.Lang.isValue(LogiXML.Resize._rdInitAnimatedMapResizer))
		LogiXML.Resize._rdInitAnimatedMapResizer(sAnimatedMapMovieType, sAnimatedMapID, sAnimatedMapDataFile, sAnimatedMapOriginalID, sBGColor);
	else
		setTimeout(function() { rdInitAnimatedMapResizer(sAnimatedMapMovieType, sAnimatedMapID, sAnimatedMapDataFile, sAnimatedMapOriginalID, sBGColor); }, 100);
}
function rdInitGoogleMapsResizer(GoogleMapID, eleGoogleMapObj) {
	if (Y.Lang.isValue(LogiXML.Resize._rdInitGoogleMapsResizer))
		LogiXML.Resize._rdInitGoogleMapsResizer(GoogleMapID, eleGoogleMapObj);
	else
		setTimeout(function() { rdInitGoogleMapsResizer(GoogleMapID, eleGoogleMapObj); }, 100);
}

//Supporting external functions

function rdRerenderAnimatedChart(sParams, eleDivObj){
    // Function extracts all the needed parameters, changes the Chart container Id with the new chart id and instantiates a new Chart object.
    
    var sChartParams = sParams.substring(sParams.indexOf('new FusionCharts('), sParams.lastIndexOf('rdAnimatedChart.setDataURL'));
    var myRegExp = new RegExp("'|\\\\|\"","g"); 
    sChartParams = sChartParams.replace(myRegExp, ""); 
    var aAnimatedChartParams = sChartParams.split(',');
    var sAnimChartType = LTrim(RTrim(aAnimatedChartParams[0].substring(aAnimatedChartParams[0].indexOf('rdTemplate'), aAnimatedChartParams[0].length)));
    var sChartId = LTrim(RTrim(aAnimatedChartParams[1]));
    var sChartWidth = LTrim(RTrim(aAnimatedChartParams[2]));
    var sChartHeight = LTrim(RTrim(aAnimatedChartParams[3]));
    var sACDFile = sParams.substring(sParams.indexOf('setDataURL('), sParams.indexOf('rdAnimatedChart.render')).replace(myRegExp, "");
    var sAnimatedChartDataFile =  LTrim(RTrim(sACDFile.substring(sACDFile.indexOf('(')+ 1, sACDFile.lastIndexOf(')'))));
    if(eleDivObj.id.indexOf('_') != -1){
        if(eleDivObj.id.substring(eleDivObj.id.lastIndexOf('_'), eleDivObj.id.length).length > 32){  // To handle Dashboards.   ' # 11496.
            var sChartOriginalID = eleDivObj.id.substring(eleDivObj.id.indexOf('rdAnimatedChart')+15, eleDivObj.id.lastIndexOf('_'));
            var ChartGUID = eleDivObj.id.substring(eleDivObj.id.lastIndexOf('_'), eleDivObj.id.length).substring(0,33)
            var eleResizerAttrs = document.getElementById("rdResizerAttrs_" + sChartOriginalID + ChartGUID) // Need to change the ID's for these elements below, 
            eleResizerAttrs.setAttribute('id', "rdResizerAttrs_" + sChartOriginalID)                        // so that they stay current with the returned Ajax response
            var eleHiddenReportID =  document.getElementById(sChartOriginalID + ChartGUID + '-' + 'Hidden') // and get picked up when looked up for with the current Id's. 
            eleHiddenReportID.setAttribute('id', sChartOriginalID + '-Hidden')
        }
    }
    eleDivObj.setAttribute('id', 'rdAnimatedChart'+ sChartId);
    // Create a new Chart object.
    var rdAnimatedChart = new FusionCharts(sAnimChartType, sChartId, sChartWidth, sChartHeight , "0", "0");
    rdAnimatedChart.setDataURL(sAnimatedChartDataFile);
    rdAnimatedChart.render('rdAnimatedChart'+ sChartId);     
}

function rdRerenderAnimatedMap(sParams, eleDivObj){
    // Function extracts all the needed parameters, changes the Map container Id with the new chart id and instantiates a new Map object.
    
    var sChartParams = sParams.substring(sParams.indexOf('new FusionMaps('), sParams.lastIndexOf('map.setDataURL'));
    var myRegExp = new RegExp("'|\\\\|\"","g"); 
    sChartParams = sChartParams.replace(myRegExp, ""); 
    var aAnimatedChartParams = sChartParams.split(',');
    var sAnimChartType = LTrim(RTrim(aAnimatedChartParams[0].substring(aAnimatedChartParams[0].indexOf('rdTemplate'), aAnimatedChartParams[0].length)));
    var sChartId = LTrim(RTrim(aAnimatedChartParams[1]));
    var sChartWidth = LTrim(RTrim(aAnimatedChartParams[2]));
    var sChartHeight = LTrim(RTrim(aAnimatedChartParams[3]));
    var sAMDFile = sParams.substring(sParams.indexOf('setDataURL('), sParams.indexOf('map.render')).replace(myRegExp, "");
    var sAnimatedChartDataFile =  LTrim(RTrim(sAMDFile.substring(sAMDFile.indexOf('(')+ 1, sAMDFile.lastIndexOf(')')))); 
    eleDivObj.setAttribute('id', 'rdFusionMap'+ sChartId);
    // Create a new Map object.
    var Map = new FusionMaps(sAnimChartType, sChartId , sChartWidth , sChartHeight , "0", "0");
    Map.setDataURL(sAnimatedChartDataFile);
    Map.render('rdFusionMap'+ sChartId);
}

//The Y.use contained in this function is MEMORY SAFE because this function only runs once and should never be hit by refresh element 
function rdInitDashboardPanelResizer(pnlContent, sPanelID, sTabId) {
	Y.use('rdResize', function(Y) {
	
		var sIdForAttrs = sPanelID
		var eleAttrs = document.getElementById("rdResizerAttrs_" + sIdForAttrs)
		if (!Y.Lang.isValue(eleAttrs))
			return;
				
		// Detect the element with the AutoSizer.
		var eleAutoSizerIndicator = document.getElementById('rdAutoSizer_' + sPanelID);
		var eleAutoSizeElement; var nAutoSizeAspect = 0;
		if(eleAutoSizerIndicator){
			eleAutoSizeElement = document.getElementById(eleAutoSizerIndicator.value);
			if(eleAutoSizeElement.offsetHeight == 0){ setTimeout(function(){ rdInitDashboardPanelResizer(pnlContent, sPanelID, sTabId)}, 10); return}
		}   	
			
			
		var yuiResize = Y.rdResize.AddYUIResizerHandles(pnlContent.ancestor('.rdDashboardPanel').get('id'), eleAttrs);
				
		var eleDashboardPanel = document.getElementById(sPanelID);

        if (navigator.appVersion.match('MSIE 7.0') != null) {
                if (eleDashboardPanel.style.height=="") { //height is blank 1st time panel is added.
		            eleDashboardPanel.firstChild.style.width = ''; 
		            eleDashboardPanel.style.width = (parseInt(eleDashboardPanel.clientWidth) + 10) + 'px';
		            eleDashboardPanel.style.height = (parseInt(eleDashboardPanel.clientHeight) + 10) + 'px';
		            eleDashboardPanel.firstChild.style.width = '100%'; 
		        }
		}else{
            if (eleDashboardPanel.style.height=="") { //height is blank 1st time panel is added.
                eleDashboardPanel.style.width = (parseInt(eleDashboardPanel.offsetWidth) + 10) + 'px';
                eleDashboardPanel.style.height = (parseInt(eleDashboardPanel.offsetHeight) + 10) + 'px';
            }
        }

		var elePanelTable = eleDashboardPanel.firstChild;
		
		rdResizePanelContent(pnlContent.getDOMNode(), sPanelID);
		
		eleDashboardPanel.style.overflow = 'hidden';
		pnlContent.getDOMNode().parentNode.style.padding = '0px 5px 5px 5px';			

		/* function goes through the panels and their z-indices and if there is a panel overlapping the panel passed in, 
		then moves the current panel to the top of the stack.*/
		Y.one('#' + sPanelID).on('click', function(e) { 
			var elePanel = Y.one('#' + sPanelID);
			var sPanelContainerId = "rdDivDashboardpanels";			
			var nCurrentPanelZindex = parseInt(elePanel.getStyle('zIndex'));
			var elePanelContainer = document.getElementById(sPanelContainerId)
			var aDashboardPanels = Y.all('.rdDashboardPanel');
			
			for (var i=0; i < aDashboardPanels.size(); i++) {
				var elePanelItem = aDashboardPanels.item(i);
				if(elePanelItem.get('id') == elePanel.get('id')) continue 				
				if(elePanel.intersect(elePanelItem) != null){
					var nPanelItemZindex = parseInt(elePanelItem.getStyle('zIndex'));
					if(nPanelItemZindex > nCurrentPanelZindex){
						rdSetDashboardPanelZIndex(elePanel);
						rdSaveFreeformLayoutPanelPosition(sPanelContainerId);
						return;
					}
				}
			}
		});
		
		if(elePanelTable.offsetHeight > eleDashboardPanel.offsetHeight)
			setTimeout(function(){rdResizePanelContent(pnlContent.getDOMNode(), sPanelID)}, 10);
		
		//events
		 yuiResize.on('resize:start', function(e) {
			freezeDashboardContainer();
			
			var pnlResizing = Y.one('#' + sPanelID);
			rdSetDashboardPanelZIndex(pnlResizing);			
			pnlResizing.setStyle("opacity", '.65');
			rdSetDashboardPanelOpacity(pnlResizing, 0.45);
		});  
		 yuiResize.on('resize:resize', function(e) {
			var sizedPanel = document.getElementById(sPanelID);
			var sizedTable = sizedPanel.firstChild;
			var sizedContent = document.getElementById(sPanelID.replace('rdDashboardPanel-', 'rdDashboard2PanelContent_')); //#15588.
			rdResizePanelContent(sizedContent, sPanelID);
	 		
			//Keep the width from shrinking beyond what the table allows
			/*if ((sizedTable.offsetWidth) > sizedPanel.offsetWidth) {
				alert(sizedTable.offsetWidth);
				sizedPanel.style.width = (sizedTable.offsetWidth + 15) + 'px';				
				e.target.plug(Y.Plugin.ResizeConstrained, {
					minWidth: parseInt((sizedTable.offsetWidth + 15)),
					minHeight: 50
				});				
			}*/		
				
			if (eleAutoSizerIndicator) {			
				var eleAutoSizeElement = document.getElementById(eleAutoSizerIndicator.value);			
				if(eleAutoSizeElement) {
					eleAutoSizeElement.style.width = (e.info.offsetWidth - 15) + 'px';
					rdAutoSizeContent(sizedContent, eleAutoSizeElement);   // Sets the height of the image while considering the height of the rest of the siblings in the panel.
				}
			}
			//resize the Tab.
			//rdResizeDashboardContainer();
		});
		yuiResize.on('resize:end', function(e) {
			unFreezeDashboardContainer();
						
			var pnlResized = Y.one('#' + sPanelID);
			pnlResized.setStyle('opacity', '1');
			rdSetDashboardPanelOpacity(pnlResized, 1);
			rdSaveFreeformLayoutPanelPosition("rdDivDashboardpanels");
										
			if(eleAutoSizerIndicator) 
				rdAjaxRequest('rdAjaxCommand=RefreshElement&rdRefreshElementID=' + pnlContent.get('id') + ',' + sPanelID.replace('rdDashboardPanel-', '') + 
					',' + sPanelID.substring(0, sPanelID.lastIndexOf('_')).replace('rdDashboardPanel-', '') +
					'&rdFreeformPanelAutoSizerHeight=' + e.target.info.offsetHeight +
					'&rdFreeformPanelAutoSizerWidth=' + e.target.info.offsetWidth + 
					'&rdFreeformPanelAutoSizer=True' + '&rdReport=' + document.getElementById("rdDashboardDefinition").value);
		});
	});
}

function rdResizePanelContent(eleContent, sPanelID) {
    var eleDashboardPanel = document.getElementById(sPanelID);
    var elePanelTitle = document.getElementById(sPanelID.replace('rdDashboardPanel', 'rdDashboardPanelTitle'));
    var nTitleHeight = elePanelTitle.offsetHeight;
    var elePanelParams =  elePanelTitle.nextSibling;
    var nParamsHeight;
	
    if(elePanelParams.style.display == 'none')
        nParamsHeight = 0;
    else
        nParamsHeight = elePanelParams.offsetHeight;
    
    eleContent.style.overflow = 'auto';
    
    eleContent.style.width = (eleDashboardPanel.offsetWidth - 17) + 'px';
    var nContentHeight = (eleDashboardPanel.offsetHeight - nTitleHeight - nParamsHeight - 17)

    eleContent.style.height = (nContentHeight < 20 ? 20 : nContentHeight) + 'px';
}

function rdResizePanelContentOnEditCancelSave(e, objIDs) {	
	var eleContent = document.getElementById(objIDs[0]);
	var eleDashboardPanel = document.getElementById(objIDs[1]);
	var sAction = objIDs[2];
	var elePanelTable = eleDashboardPanel.firstChild;
	var elePanelTitle = elePanelTable.firstChild.firstChild;
	var nTitleHeight = elePanelTitle.offsetHeight;
	var elePanelParams =  elePanelTitle.nextSibling;
	var nParamsHeight = 0;
			
	if(elePanelParams.style.display == 'none'){ 
		if(sAction == "Edit"){
		    setTimeout(function(){rdResizePanelContentOnEditCancelSave(e, objIDs)}, 10);
		    return;
		}
	}else{
	    if(sAction == "Save" || sAction == "Cancel"){
            setTimeout(function(){rdResizePanelContentOnEditCancelSave(e, objIDs)}, 10);
            return;
        }
	}
	nParamsHeight = (navigator.appVersion.match('MSIE 8.0') != null ? elePanelParams.clientHeight : elePanelParams.offsetHeight);		
	var nContentHeight = (eleContent.offsetHeight + nTitleHeight + nParamsHeight + 13);
	eleDashboardPanel.style.height = (nContentHeight < 20 ? 20 : nContentHeight) + 'px';
	rdResizeDashboardContainer();	
}

function rdAutoSizeContent(eleContent, eleElement) {
	var nPanelChildrenOffsetHeight = 0;
	var aPanelChildren = eleContent.childNodes;	
	for (var i=0; i < aPanelChildren.length; i++) {
		var aPanelChild = aPanelChildren[i];
		if (aPanelChild.id == eleElement.id) continue;
		nPanelChildrenOffsetHeight += (isNaN(aPanelChild.offsetHeight) ? 0 : aPanelChild.offsetHeight);
	}
	var adjustedHeight = eleContent.clientHeight - (nPanelChildrenOffsetHeight + 15);
	if (adjustedHeight > 0)
		eleElement.style.height = adjustedHeight + 'px';
	else
		eleElement.style.height = eleContent.clientHeight;
}

function freezeDashboardContainer() {
	var dashContainer = Y.one('#' + 'rdDivDashboardpanels');						
	var containerWidth = dashContainer.getDOMNode().clientWidth, 
		containerHeight = dashContainer.getDOMNode().clientHeight;						
	dashContainer.setStyle('position', 'absolute');
	dashContainer.ancestor('div').setStyles({							
		width: containerWidth,
		height: containerHeight
	});		
}

function unFreezeDashboardContainer() {
	var dashContainer = Y.one('#' + 'rdDivDashboardpanels');
	dashContainer.ancestor('div').setStyles({							
		width: '',
		height: ''
	});
	dashContainer.setStyle('position', 'relative');	
}

function ReCalculateDashboardPanelSize(sPanelID, nIteration){
    var eleDashboardPanel = document.getElementById(sPanelID);
    if(typeof(nIteration) == 'undefined') nIteration = 0;
    if(nIteration > 4){
        if (document.getElementById('rdFreeformLayout') != null) {
            unFreezeDashboardContainer();						
            rdSaveFreeformLayoutPanelPosition('rdDivDashboardpanels');
        }
        return;
    }
    eleDashboardPanel.style.height = '';
    eleDashboardPanel.style.height = eleDashboardPanel.scrollHeight + 'px';	
    nIteration += 1;	
    setTimeout(function(){ReCalculateDashboardPanelSize(sPanelID, nIteration)}, 100);
}

// Removes leading whitespaces
function LTrim( value ) {
	var re = /\s*((\S+\s*)*)/;
	return value.replace(re, "$1");	
}
// Removes ending whitespaces
function RTrim( value ) {
	var re = /((\s*\S+)*)\s*/;
	return value.replace(re, "$1");
}
// Removes leading and ending whitespaces
function trim( value ) {
	return LTrim(RTrim(value));
}
