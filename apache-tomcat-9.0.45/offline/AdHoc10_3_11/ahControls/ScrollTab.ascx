<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="ScrollTab.ascx.vb" Inherits="LogiAdHoc.ScrollTab" %>

    <table id="tbST" cellpadding="0" cellspacing="0" style="margin:0;padding-bottom:0;">
    <tr valign="middle">
    <td width="14px" height="20px">
    <div id="scLeft" class="tdArrowL" onclick="moveSC(0)">&nbsp; &nbsp;</div>
    </td>
    <td>
    <asp:Button ID="btnSC" runat="server" CssClass="NoShow" OnCommand="GoToStep" />
    <input type="hidden" id="actTab" name="actTab" runat="server" />
    <div id="divTab" class="tab_xp tab_default" style="position:relative; visibility: visible; overflow:hidden;">
        <span id="divContainer" runat="server" class="tab_header">
	    </span>
    </div>	
    </td>
    <td width="14px" style="padding-left:4px;">
    <div id="scRight" class="tdArrowR" onclick="moveSC(1)">&nbsp; &nbsp;</div>
    </td>
    </tr>
    </table>
