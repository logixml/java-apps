<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="NumBox.ascx.vb" Inherits="LogiAdHoc.NumBox" %>
<asp:Label ID="lblNumValue" runat="server" CssClass="NoShow" Text="<%$ Resources:LogiAdHoc, Value %>" AssociatedControlID="NumValue"></asp:Label>
	<asp:TextBox ID="NumValue" Runat="server" /><asp:CustomValidator ID="NumValidator" runat="server" ControlToValidate="NumValue" OnServerValidate="IsNumberValid">*</asp:CustomValidator>
