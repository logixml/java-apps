<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="RecompileReportGrid.ascx.vb" Inherits="LogiAdHoc.RecompileReportGrid" %>
    
<div id="divDependency4" runat="server" style=" overflow:auto; display:block;">

 <asp:GridView ID="grdMain" runat="server" AutoGenerateColumns="False" CssClass="gridWline" Width="650px" DataKeyNames="ID">
    <Columns>
        <asp:TemplateField>
            <HeaderStyle Width="30px"></HeaderStyle>
            <HeaderTemplate>
                <asp:CheckBox Checked="true" onclick="javascript: SelectAll(this.checked);" runat="server" ToolTip="<%$ Resources:LogiAdHoc, SelectDeselectAll %>" ID="CheckAll">
                </asp:CheckBox>
            </HeaderTemplate>
            <ItemTemplate>
                <asp:CheckBox ID="chk_select" runat="server" Checked="true" />
                <asp:HiddenField ID="ID" runat="server" Value='<%# Eval("ID") %>' />
                <asp:HiddenField ID="ReportName" runat="server" Value='<%# Eval("ReportName") %>' />
                <asp:HiddenField ID="PhysicalName" runat="server" Value='<%# Eval("PhysicalName") %>' />
                <asp:HiddenField ID="Mobile" runat="server" Value='<%# Eval("Mobile") %>' />
                <asp:HiddenField ID="Dashboard" runat="server" Value='<%# Eval("Dashboard") %>' />
                <asp:HiddenField ID="UserID" runat="server" Value='<%# Eval("UserID") %>' />
                <asp:HiddenField ID="GroupID" runat="server" Value='<%# Eval("GroupID") %>' />
                <asp:HiddenField ID="BreakReason" runat="server" Value='<%# Eval("BreakReason") %>' />
            </ItemTemplate>            
        </asp:TemplateField>        
        <asp:BoundField HeaderText="Report Name" DataField="ReportName" />
        <asp:BoundField HeaderText="Owner" DataField="Owner" />
        <asp:BoundField HeaderText="Folder" DataField="Folder" />
        <asp:TemplateField HeaderText="Result">
            <ItemTemplate>
                <asp:Label ID="lblRecompileResult" runat="Server" />
            </ItemTemplate>            
        </asp:TemplateField> 
    </Columns>
    <HeaderStyle CssClass="gridheader" />
</asp:GridView>
<br /><br />
<asp:Label ID="lblResult" runat="server" ForeColor="red" />

</div>

<script type="text/javascript">                                                       
    function SelectAll(checked)
    {
        var grid = document.getElementById("<%= grdMain.ClientID %>");
        var cell;
        
        if (grid.rows.length > 0)
        {
            for (i=1; i<grid.rows.length; i++)
            {
                cell = grid.rows[i].cells[0];                    
                for (j=0; j<cell.childNodes.length; j++)
                {           
                    if (cell.childNodes[j].type =="checkbox")
                    {
                        cell.childNodes[j].checked = checked;
                    }
                }
            }
        }
    }
</script>