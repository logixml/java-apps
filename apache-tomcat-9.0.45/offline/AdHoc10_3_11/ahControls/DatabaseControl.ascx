<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="DatabaseControl.ascx.vb" Inherits="LogiAdHoc.DatabaseControl" %>

<asp:panel ID="pnlDatabaseDropdown" runat="server">
<table>
    <tr>
        <td>
            <asp:Label ID="lblDatabases" runat="server" Text="" AssociatedControlID="ddlDatabases" />
        </td>
        <td>
            <asp:DropDownList ID="ddlDatabases" runat="server" AutoPostBack="True">
            </asp:DropDownList> 
        </td>
    </tr>
</table>
</asp:panel>