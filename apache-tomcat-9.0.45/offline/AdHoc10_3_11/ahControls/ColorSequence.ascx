<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="ColorSequence.ascx.vb" Inherits="LogiAdHoc.PieChartImage" %>

<table>
    <tr>
        <td>
            <asp:ImageMap ID="imgMapPieChart" runat="server" ImageUrl="~/rdDownload/ahColorSequence.jpg" OnClick="SelectPieSlice">
            </asp:ImageMap>
        </td>
        <td style="width: 157px">
            <table>
                <tr>
                    <td>
                        <asp:TextBox ID="txtColor" runat="server" Width="100px" class="color {hash:true,caps:false}"></asp:TextBox>                        
                    </td>
                </tr>
                <tr>
                    <td align="left">
                        <AdHoc:LogiButton ID="btnAdd" runat="server" OnClick="btnAdd_OnClick" Text="<%$ Resources:LogiAdHoc, Add %>" Width="65px" />
                    </td>
                </tr>
                <tr>
                    <td align="left">
                        <AdHoc:LogiButton ID="btnUpdate" runat="server" OnClick="btnUpdate_OnClick" Text="<%$ Resources:LogiAdHoc, Update %>" Width="65px" />
                    </td>
                </tr>
                <tr>
                    <td align="left">
                        <AdHoc:LogiButton ID="btnRemove" runat="server" OnClick="btnRemove_OnClick" Text="<%$ Resources:LogiAdHoc, Remove %>" Width="65px" />
                    </td>
                </tr>
            </table>    
            
        </td>
    </tr>
</table>
