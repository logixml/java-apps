<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="MainMenu.ascx.vb" Inherits="LogiAdHoc.ahControls_MainMenu" %>
<div id="menu">
      <div id="top_nav">
<span id="functions">
    <%--<asp:HyperLink ID="TestQuery" runat="server" Text="Query" NavigateUrl="..\ahConfiguration\TestQuery.aspx"></asp:HyperLink>
    <span class="separator" id="spTestQuery" runat="server">|</span>--%>
    <asp:HyperLink ID="Home" runat="server"></asp:HyperLink>
    <span class="separator" id="spHomeSeparator" runat="server">|</span>
    <asp:HyperLink ID="About" runat="server"></asp:HyperLink>
    <span class="separator">|</span>
    <asp:HyperLink ID="Help" runat="server"></asp:HyperLink>
    <span class="separator">|</span>
    <asp:HyperLink ID="Logout" runat="server"></asp:HyperLink>
</span>
    <table id="tbMenu" runat="server">
        <tr id="trMenu" runat="server">
            <td class="tdMnuLogo">
                <asp:HyperLink ID="SiteLogo" SkinID="SiteLogo" runat="server"/>
            </td>
            <td class="tdMnuLinks">
                <span id="links">
                    <img id="iconReports" class="menuicon" runat="server" /><asp:HyperLink ID="Reports" SkinID="mnuReports" runat="server"></asp:HyperLink>
                    <img id="iconUserProfile" class="menuicon" runat="server" /><asp:HyperLink ID="UserProfile" SkinID="mnuProfile" runat="server" ></asp:HyperLink>
                    <img id="iconConfiguration" class="menuicon" runat="server" /><asp:HyperLink ID="Configuration" SkinID="mnuConfig" runat="server"></asp:HyperLink>
                </span>
            </td>
            <td width="20">&nbsp;</td>
        </tr>
    </table>
    </div>
</div>
