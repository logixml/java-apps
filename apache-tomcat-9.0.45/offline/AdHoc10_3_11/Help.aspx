<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="Help.aspx.vb" Inherits="LogiAdHoc.Help" Culture="auto" meta:resourcekey="PageResource1" UICulture="auto" %>

<%@ Register TagPrefix="help" TagName="menu" Src="~/HelpMenu.ascx" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Help</title>
    <link rel="shortcut icon" href="ahImages/flav.ico" />
    <script language="JavaScript" type="text/javascript" src="ahScripts/Wizard.js"></script>
    <script type="text/javascript">
        function calcHeight()
        {
            try
            {
              //find the height of the internal page
              var the_height = document.getElementById('frm').contentWindow.document.body.scrollHeight;

              //change the height of the iframe
              document.getElementById('frm').height = the_height;
              //document.getElementById('frm').height = window.screen.availHeight;
            }
            catch(err)
            {
                //document.getElementById('frm').height = 5000;
                document.getElementById('frm').height = window.screen.availHeight
            }
        }
    </script> 
</head>
<body class="bodHelp">
    <form id="form1" runat="server">
    <div align="center">
  <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
      <td class="titleImage" align="left" valign="top"></td>
    </tr>
  </table>
  <br />
  <table width="92%" border="0" cellspacing="0" cellpadding="0">
    <tr align="left" valign="top">
      <td>&nbsp;</td>
      <td>&nbsp;</td>
	</tr>
    <tr align="left" valign="top">
      <td width="200">
		<!-- Help Menu goes here. Remember to set the CurrentPageName. -->
		<help:menu id="HelpMenu" CurrentPageName="Home" runat="server" />
	  </td>
      <td align="center" style="border-left: 1px #CDCDCD solid">
	  <table width="94%" border="0" cellspacing="0" cellpadding="0">
	    <tr>
	      <td align="left" valign="top">
            
            <iframe id="frm" runat="server" frameborder="0" width="100%"></iframe>
	      </td>
	    </tr>
	    </table>
	  </td>
    </tr>
    <tr align="center" valign="bottom">
      <td colspan="2">
	    <br>
	    <br><br><br>
	    <br>
       <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td width="60%" height="24" align="center" valign="middle" style="border-top: 1px #CDCDCD solid">
          <p>
          <asp:Localize ID="Localize3" runat="server" meta:resourcekey="LiteralResource3">
          Logi Ad Hoc Reporting &nbsp;&copy; Copyright 2004-2012 LogiXML, Inc. All rights reserved.
          </asp:Localize>
          </p>
          </td>
        </tr>
      </table></td>
    </tr>
  </table>
    </div>
    </form>
</body>
</html>
