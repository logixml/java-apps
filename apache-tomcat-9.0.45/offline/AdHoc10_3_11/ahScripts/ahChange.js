﻿// JScript File

var isPostBack = false;
var noMessage = false;
var __oldDoPostBack;
if (typeof __doPostBack != 'undefined') {
    __oldDoPostBack = __doPostBack;
    __doPostBack = AlwaysFireBeforeFormSubmit;
}

function AlwaysFireBeforeFormSubmit(eventTarget, eventArgument) {
    if (!noMessage) document.getElementById('ahDirty').value = '1';
    isPostBack = true;
    //noMessage=false;
    return __oldDoPostBack(eventTarget, eventArgument);
}

function AlertOnExit(e) {
    if ((document.getElementById('ahDirty').value == 1) && (!isPostBack) && (!noMessage)) {
        //event.returnValue='Any changes you have made to data on this page will be lost.';
        //alert('From Alert ' + document.getElementById('ahDirty').value + ' ' + isPostBack + ' ' + noMessage);
        return 'Any changes you have made to data on this page will be lost.';
    }
    noMessage = false;
}

function Select_OnChange(parentID) {
    if (document.getElementById('ahDirty').value == 1) {
        if (!confirm('Are you sure you want to navigate away from this page.\n\nAny changes you have made to data on this page will be lost.\n\nPress OK to continue, or CANCEL to stay on the current page.')) {
            document.getElementById(parentID).selectedIndex = document.getElementById('ahParentID').value
            return false;
        }
        else
            document.getElementById('ahDirty').value = 0;
    }
    document.getElementById('ahParentID').value = document.getElementById(parentID).selectedIndex
    return true;
}

function ControlAlert() {
    if ((document.getElementById('ahDirty').value == 1) && (!noMessage)) {
        event.returnValue = 'Any changes you have made to data on this page will be lost.';
    }
    noMessage = false;
}

function select_deselectAll(chkVal, idVal, gridID, chkID) {

    if (!gridID) gridID = 'grdMain';
    if (!chkID) chkID = 'chk_Select';

    var oRows = document.getElementById(gridID).getElementsByTagName('tr');
    var l = oRows.length;
    //var l = grdMain.rows.length;
    var e;
    var s;

    var iAdj = 2;
    if (gridID == 'grdColumnFormat') {
        iAdj = parseInt(document.getElementById("javaGrdColumnFormat").value); //1=Java 2=.Net 9919
    }


    if (idVal.indexOf('CheckAll') != -1) {
        var bSet;
        // Check if main checkbox is checked, then select or deselect datagrid checkboxes
        if (chkVal == true) {
            bSet = true;
        } else {
            bSet = false;
        }
        // Loop through all elements    
        for (i = iAdj; i <= l; i++) {
            var o = pack(i.toString(10), 2);
            s = gridID + "_ctl" + o + "_" + chkID;
            e = document.getElementById(s);
            if (e != null) e.checked = bSet;
        }
    }
}

function confirmMsg(frm, cnfMsg, errMsg) {
    // loop through all elements
    for (i = 0; i < frm.length; i++) {
        // Look for our checkboxes only
        if (frm.elements[i].name.indexOf("chk_Select") != - 1) {
            // If any are checked then confirm
            if (frm.elements[i].checked)
                if (cnfMsg == "")
                    return true
                else
                    return confirm(cnfMsg)
        }
    }
    //No Checkboxes have been checked so show an error message
    alert(errMsg)
    return false
}

function getConfirmation(cnfMsg, id) {
    if (confirm(cnfMsg)) {
        var btn = document.getElementById(id);
        if (btn) btn.click();
    }
}

function pack(i, num) {
    var o = "";
    for (n = 0; n < (num - i.length); n++) {
        o = "0" + o;
    }
    return o + i;
}

function DisableSave(btn, validationGroup) {
    var bDisabled = true;
    if (typeof (Page_ClientValidate) == "function") {
        bDisabled = Page_ClientValidate(validationGroup);
        //bDisabled = typeof( Page_IsValid ) != 'undefined' ? Page_IsValid : true;
    }
    else if (window && typeof (window.Page_ClientValidate) == "function") {
        bDisabled = window.Page_ClientValidate(validationGroup);
    }
    btn.disabled = bDisabled
}

function ShowSaveAnimation(validationGroup) {
    var bShow = true;
    if (typeof (Page_ClientValidate) == "function") {
        bShow = Page_ClientValidate(validationGroup);
        //bShow = typeof( Page_IsValid ) != 'undefined' ? Page_IsValid : true;
    }
    else if (window && typeof (window.Page_ClientValidate) == "function") {
        bShow = window.Page_ClientValidate(validationGroup);
    }
    if (bShow)
        $find('ahSavePopupBehavior').show();
}
function ShowSaveAnimationNoValidation() {
    $find('ahSavePopupBehavior').show();
}

function ShowRedirectSaveAnimation() {
    var ahDirty = document.getElementById('ahDirty');
    if ((ahDirty) && (ahDirty.value == 1) && (!isPostBack) && (!noMessage)) {
        //Do Nothing.
    }
    else {
        $find('ahSavePopupBehavior').show();
    }
}

function ScrollDiv(sel, div) {
    // This should only scroll the DIV if only one item has been selected (moving with arrow keys)
    var indx = sel.selectedIndex;
    var cont = true;
    for (i = indx + 1; i < sel.length; i++) {
        if (sel.options[i].selected == true) {
            cont = false;
            break;
        }
    }
    if (cont) {
        var nItems = 0;
        if (document.all) {
            //IE : use sel.Length as scrollHeight is correct in IE.
            nItems = sel.length;
        }
        else {
            //Firefox : use sel.Length as scrollHeight is correct in IE.
            if (sel.length < 18)
                nItems = 18;
            else
                nItems = sel.length;
        }
        var itemHeight = sel.scrollHeight / nItems;
        var scrollPos = (itemHeight * indx) + 3;
        if (scrollPos < div.scrollTop)
            div.scrollTop = scrollPos;
        else {
            if ((scrollPos + itemHeight) > (div.scrollTop + div.clientHeight))
                div.scrollTop = scrollPos + itemHeight - div.clientHeight;
        }
    }
}


function ScrollableListBoxRefineHeightAndWidth(sel, height, width) {
    if (sel) {
        sel.onresize = null;
        if (sel.scrollHeight < height) {
            sel.style.height = (height) + 'px';
        }
        else {
            sel.style.height = (sel.scrollHeight + 6) + 'px';
        }
        if (width > sel.clientWidth) {
            sel.style.width = (width) + 'px';
        }
    }
}
function ResetWidth(sel, height, width) {
    if (sel) {
        //sel.detachEvent('onresize', ResetWidth);
        sel.onresize = null;
        if (sel.clientHeight < height) {
            sel.style.height = height;
        }
        else {
            sel.style.height = sel.scrollHeight + 6;
        }
        if (width > sel.clientWidth) {
            sel.style.width = width;
        }
    }
}

var X = new Object();
var Y = new Object();
function RestorePopupPosition(popupID) {
    //This function does not work in IE6 so exit here.
    if (typeof document.body.style.maxHeight == "undefined") {
        return;
    }

    var pp = $find(popupID);
    if (pp) {
        pp.set_X(X[popupID]);
        pp.set_Y(Y[popupID]);
    }
}
function SavePopupPosition(popupID) {
    var pp = $find(popupID);
    if (pp) {
        if (pp._foregroundElement.style.display == '') {
            X[popupID] = pp._foregroundElement.style.left.replace('px', '');
            Y[popupID] = pp._foregroundElement.style.top.replace('px', '');
        }
        else {
            X[popupID] = -1;
            Y[popupID] = -1;
        }
    }
}

/*For Session Parameters Pages*/
function FollowDefaultCheckChanged(chk, txtID) {
    var txt = document.getElementById(txtID);
    if (chk.checked) {
        txt.value = '';
        txt.className = 'txtDisabled';
    }
    else {
        txt.className = 'txtEnabled';
    }
}
function ParamValueFocus(chkID, txt) {
    var chk = document.getElementById(chkID);
    txt.className = 'txtEnabled';
}
function ParamValueBlur(chkID, txt) {
    var chk = document.getElementById(chkID);
    if (chk.checked) {
        txt.className = 'txtDisabled';
    }
    else {
        txt.className = 'txtEnabled';
    }
}
function ParamValueKeyDown(chkID, txt) {
    var chk = document.getElementById(chkID);
    chk.checked = false;
}

window.addEventListener('load', (event) => {
    var txtLabel = document.getElementById('lblArchiveInfo')
    txtLabel.innerText = 'Selecting the HTML option will export as PDF.';

    var eleOutputFormat = document.getElementById('OutputFormat');
    eleOutputFormat.addEventListener('change', (event) => {
        document.getElementById('ahDirty').value = 1;
        //setTimeout('__doPostBack(\'OutputFormat\',\'\')', 0);
        console.log(document.getElementById('OutputFormat'));
        debugger;
        resetOptions(document.getElementById('OutputFormat'));
    });


    resetOptions(document.getElementById('OutputFormat'));
});


function resetOptions(ele) {
    for (var i = 0; i < ele.length; i++) {
        if (ele.options[i].value == '0')
            ele.remove(i);
    }
}