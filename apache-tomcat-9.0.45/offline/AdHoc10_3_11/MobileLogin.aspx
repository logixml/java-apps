<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="MobileLogin.aspx.vb" Inherits="LogiAdHoc.MobileLogin" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Welcome to Logi 10 Ad Hoc Reporting</title>
    <link rel="shortcut icon" href="ahImages/flav.ico" />
    <style type="text/css">
        td, input { font-size:26px; font-family: verdana, arial}
        .err { color:Red; }
    </style>
</head>
<body>
    <form NAME="frmLogin" METHOD="post" TARGET="_top" ACTION="Gateway.aspx">
    <img src="ahImages/logo10.png" alt="Welcome to Logi 10 Ad Hoc Reporting" />
    <br /><br />
    
    Username<br />
	<input type="text" id="username" runat="server" name="username" size="20" maxlength="50" class="tx">
	<br /><br />
	        
	Password<br />
	<input type="password" id="password" runat="server" name="password" size="20" maxlength="100">
	<br /><br />
	
	<input type="submit" title="Click here to proceed" id="IMAGE1" name="IMAGE1">
    
    <%If Session("rdLogonFailMessage")<>"" Then%> 
	    <div class="err"><%=Session("rdLogonFailMessage") %></div>    
    <%End If
    Session("lgx_MenuID") = "MainHeader"
    %>

    </form>
</body>
</html>
