<%@ Page Language="VB" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title>Welcome to Logi 10 Ad Hoc Reporting</title>
    <link rel="stylesheet" type="text/css" href="Login.css">
    <link rel="shortcut icon" href="ahImages/flav.ico" />
</head>
<body onLoad="document.getElementById('username').focus()" onKeyPress="if (event.keyCode==13){document.forms['frmLogin'].submit()}">
    <form NAME="frmLogin" METHOD="post" TARGET="_top" ACTION="Gateway.aspx">
        <table width="100%" border="0" cellspacing="0" cellpadding="0" height="100%">
          <tr align="center" valign="middle"> 
            <td> 
              <table width="604" border="0" cellspacing="0" cellpadding="3" align="center">
                <tr class="tr1" align="center" valign="middle"> 
                  <td> 
                    <table width="600" border="0" cellspacing="0" cellpadding="0">
                      <tr> 
                        <td colspan="2" align="left" height="65"><img src="ahImages/homeTop.png" alt="Logi 10 Ad Hoc Reporting login logo" width="600" height="65"></td>
                      </tr>
                      <tr> 
                        <td class="td1" align="center" valign="middle"><img src="ahImages/homeLeft.png" alt="Logi 10 Ad Hoc Reporting logo" width="272" height="235"></td>
                        <td class="td2" align="left" valign="middle"> 
                          <table class="tb1" cellpadding="0" cellspacing="0">
                            <tr>
                              <td width="45">&nbsp;</td>
                              <td align="left" valign="top">
						        <table class="tb2" cellpadding="0" cellspacing="0">
							        <tr> 
								        <td valign="bottom">
									        <table width="260" border="0" cellpadding="3" cellspacing="0">
										        <tr>
											        <td width="30%" align="right" valign="middle">
											            <asp:Label ID="lblUserName" AssociatedControlID="username" Text="<%$ Resources:LogiAdHoc, UserName %>" runat="server">:</asp:Label>
											        </td>
											        <td width="60%" height="25" valign="bottom">
												        <input type="text" id="username" runat="server" name="username" size="20" maxlength="50" class="tx">
											        </td>
										        </tr>
										        <tr> 
											        <td width="30%" align="right" valign="middle">
											            <asp:Label ID="lblpassword" AssociatedControlID="password" Text="<%$ Resources:LogiAdHoc, Password %>" runat="server">:</asp:Label>
											        </td>
											        <td width="60%" height="25" valign="bottom">
												        <input type="password" id="password" runat="server" name="password" size="20" maxlength="100">
											        </td>
										        </tr>
									        <tr> 
										        <td width="30%" align="right" valign="bottom">&nbsp;</td>
										        <td width="60%" height="35" valign="top">
											        <input type="IMAGE" class="nb" title="Click here to proceed" alt="Login button" src="ahImages/loginButton.gif" id="IMAGE1" name="IMAGE12">
										        </td>
									        </tr>
        <%If Session("rdLogonFailMessage")<>"" Then%> 
									        <tr>
										        <td colspan="2" align="right" valign="bottom">
											        <table width="100%" border="0" cellspacing="0" cellpadding="0" height="40">
												        <tr align="center" valign="middle"> 
													        <td align="center" valign="bottom">
														        <table width="80%" border="0" cellspacing="0" cellpadding="1" height="30">
															        <tr align="left" valign="middle"> 
																        <td class="err"><%=Session("rdLogonFailMessage") %></td>
															        </tr>
															        <tr align="left"> 
															        </tr>
														        </table>
													        </td>
												        </tr>
											        </table>
										        </td>
									        </tr>
        <%End If
        Session("lgx_MenuID") = "MainHeader"
        %>
        							  
								        </table>
							        </td>
                                </tr>
                              </table>                      
                             </td>
                            </tr>
                          </table>
                        </td>
                      </tr>
                    </table>
                  </td>
                </tr>
              </table>
            </td>
          </tr>
        </table>
    </form>
</body>
</html>
