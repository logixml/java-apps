<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="ReportingDatabaseList.aspx.vb" Theme="green" Inherits="LogiAdHoc.ahDbAdmin_ReportingDatabaseList" Culture="auto" meta:resourcekey="PageResource1" UICulture="auto" %>

<%@ Register TagPrefix="AdHoc" TagName="AdminMainMenu" Src="~/ahControls/AdminMainMenu.ascx" %>
<%@ Register TagPrefix="AdHoc" TagName="NavMenu" Src="~/ahControls/NavMenu.ascx" %>
<%@ Register TagPrefix="AdHoc" TagName="Search" Src="~/ahControls/Search.ascx" %>
<%@ Register TagPrefix="AdHoc" TagName="PagingControl" Src="../ahControls/PagingControl.ascx" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Databases</title>
    <script language="JavaScript" type="text/javascript" src="../ahScripts/ahChange.js"></script>
</head>
<body id="bod">
    <AdHoc:AdminMainMenu ID="menu" runat="server" />
    
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server" />
    <div id="submenu">
        <AdHoc:NavMenu ID="subnav" runat="server" />
    </div>

        <script type="text/javascript">
        Sys.WebForms.PageRequestManager.getInstance().add_endRequest(EndRequestHandler);
        function EndRequestHandler(sender, args) {
            noMessage=false;
            $find('ahSavePopupBehavior').hide();        
        }
        </script> 
        
        <div class="divForm">
            <AdHoc:Search ID="srch" runat="server" Title="Find Database" Width="400" />        

            <asp:UpdatePanel ID="UP1" runat="server">
                <ContentTemplate>
                    
                    <div class="divButtons">
                        <asp:Button ID="btnNewDatabase1" runat="server" CssClass="command"
                            OnClick="NewDatabase" Text="Add Database" ToolTip="Click to create a new database." />
                        <asp:Button ID="btnRemoveDatabase1" runat="server" CssClass="command"
                            OnClick="RemoveDatabase" Text="Delete Database" ToolTip="Click to remove selected databases." />
                    </div>
                    
                    <asp:GridView ID="grdMain" runat="server" AllowPaging="True" AllowSorting="True"
                        AutoGenerateColumns="False" CssClass="grid" DataKeyNames="DatabaseID"
                        OnRowDataBound="OnItemDataBoundHandler" OnSorting="OnSortCommandHandler">
                        <Columns>
                            <asp:TemplateField>
                                <HeaderTemplate>
                                    <asp:Label ID="lblChkAll" runat="server" AssociatedControlID="CheckAll" Text="<%$ Resources:LogiAdHoc, SelectDeselectAll %>" CssClass="NoShow"></asp:Label>
                                    <asp:CheckBox ID="CheckAll" runat="server" ToolTip="<%$ Resources:LogiAdHoc, SelectDeselectAll %>" />
                                </HeaderTemplate>
                                <ItemStyle HorizontalAlign="Center" />
                                <ItemTemplate>
                                    <asp:Label ID="lblChk" runat="server" AssociatedControlID="chk_Select" Text="<%$ Resources:LogiAdHoc, SelectDeselect %>" CssClass="NoShow"></asp:Label>
                                    <asp:CheckBox ID="chk_Select" runat="server" ToolTip="<%$ Resources:LogiAdHoc, SelectDeselect %>" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField DataField="DatabaseName" HeaderText="Database Name">
                                <HeaderStyle Width="200px" />
                            </asp:BoundField>
                            <asp:TemplateField HeaderText="<%$ Resources:LogiAdHoc, Actions %>" >
                                <ItemTemplate>
                                    <asp:ImageButton ID="Modify" runat="server" AlternateText="Modify Database" ImageUrl="../ahImages/modify.gif"
                                        OnCommand="ModifyDatabase" ToolTip="Modify Database" />
                                </ItemTemplate><ItemStyle HorizontalAlign="Center"></ItemStyle>
                            </asp:TemplateField>
                        </Columns>
                        <PagerStyle HorizontalAlign="Center" />
                        <HeaderStyle CssClass="gridheader" />
                        <PagerTemplate>
                            <AdHoc:PagingControl ID="PageCtrl" runat="server" OnGotoNextPage="ChangePageIndex"/>
                        </PagerTemplate>
                        <RowStyle CssClass="gridrow" />
                        <AlternatingRowStyle CssClass="gridalternaterow" />
                    </asp:GridView>
                    
                    <div id="divButtons" class="divButtons">
                        <asp:Button ID="btnNewDatabase2" runat="server" CssClass="command"
                            OnClick="NewDatabase" Text="Add Database" ToolTip="Click to create a new Database." />
                        <asp:Button ID="btnRemoveDatabase2" runat="server" CssClass="command"
                            OnClick="RemoveDatabase" Text="Delete Databases" ToolTip="Click to remove selected Databases." />
                    </div>
                </ContentTemplate>
                <Triggers>
                    <asp:AsyncPostBackTrigger ControlID="srch" EventName="DoSearch" />
                    <%--<asp:AsyncPostBackTrigger ControlID="btnRemoveDatabase1" EventName="Click" />
                    <asp:AsyncPostBackTrigger ControlID="btnRemoveDatabase2" EventName="Click" />--%>
                </Triggers>
            </asp:UpdatePanel>

            <div id="divSaveAnimation">
                <asp:Button runat="server" ID="btnSaveAnimation" Style="display: none" />
                <ajaxToolkit:ModalPopupExtender runat="server" ID="ahSavePopup" BehaviorID="ahSavePopupBehavior"
                    TargetControlID="btnSaveAnimation" PopupControlID="pnlSaving" BackgroundCssClass="modalBackground"
                    DropShadow="False">
                </ajaxToolkit:ModalPopupExtender>
                <asp:Panel id="pnlSaving" CssClass="savePopup" runat="server" style="display:none;">
                    <table><tr><td>
                    <asp:Image ID="imgSave" runat="server" SkinID="imgSaving" EnableViewState="false" AlternateText="<%$ Resources:LogiAdHoc, SaveImageAltText %>" />
                    </td>
                    <td valign="middle">
                    <asp:Label ID="lblSaveText" CssClass="lblSavePopup" runat="server" EnableViewState="false" Text="<%$ Resources:LogiAdHoc, PleaseWait %>"/>
                    </td></tr></table>
                </asp:Panel>
            </div>
            
            <div id="divLegend" class="legend" runat="server">
                            <asp:Localize ID="locLegend" runat="server" Text="<%$ Resources:LogiAdHoc, LegendLabel %>"></asp:Localize>                
                <dl>
                    <dt>
                        <img src="../ahImages/modify.gif" alt="Modify Icon" title="Click this icon in the Database list to modify the selected Database." runat="server"/></dt>
                    <dd>
                        <asp:Localize ID="Localize1" runat="server" Text="Modify Database"></asp:Localize>&nbsp;</dd><dd></dd></dl>
            </div>
            <%--<div id="divHelp" class="help">
                <a href="../Help.aspx?src=34" target="_blank">
                    <asp:Localize ID="GetHelp" runat="server" Text="Get help with the Database screen."></asp:Localize>
                </a>
            </div>--%>
        </div>
    </form>
</body>
</html>
