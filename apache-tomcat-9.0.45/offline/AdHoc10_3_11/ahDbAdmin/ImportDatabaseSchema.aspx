<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="ImportDatabaseSchema.aspx.vb" Theme="green" Inherits="LogiAdHoc.ahDbAdmin_ImportDatabaseSchema" Culture="auto" meta:resourcekey="PageResource1" UICulture="auto" %>

<%@ Register TagPrefix="AdHoc" TagName="AdminMainMenu" Src="~/ahControls/AdminMainMenu.ascx" %>
<%@ Register TagPrefix="AdHoc" TagName="NavMenu" Src="~/ahControls/NavMenu.ascx" %>
<%@ Register TagPrefix="AdHoc" TagName="Search" Src="~/ahControls/Search.ascx" %>
<%@ Register TagPrefix="AdHoc" TagName="PagingControl" Src="../ahControls/PagingControl.ascx" %>
<%@ Register TagPrefix="cc1" Assembly="LGXAHWCL" Namespace="LGXAHWCL" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Import Database Schema</title>
    <script language="JavaScript" type="text/javascript" src="../ahScripts/ahChange.js"></script>
    
    <script type="text/javascript">
        window.onbeforeunload = AlertOnExit;
        
        function AlertOnExit(e) {
           if ((document.getElementById('ahDirty').value==1) && (!isPostBack) && (!noMessage)) {
                return 'No data objects have been imported yet.';
           }
           noMessage=false;
        }
    </script>
    
    <script type="text/javascript"> 
    function OnCheckBoxCheckChanged(evt) { 
        var src = window.event != window.undefined ? window.event.srcElement : evt.target; 
        var isChkBoxClick = (src.tagName.toLowerCase() == "input" && src.type == "checkbox"); 
	    var javaCheckBox = document.getElementById("javaCheckBoxTagName").value; //span=Java,div=DotNet 7668

        if (isChkBoxClick) { 
            var parentTable = GetParentByTagName("table", src); 
            var nxtSibling = parentTable.nextSibling; 
            if (nxtSibling && nxtSibling.nodeType == 1)//check if nxt sibling is not null & is an element node 
            { 
                if (nxtSibling.tagName.toLowerCase() == javaCheckBox) //if node has children 
                { 
                    //check or uncheck children at all levels 
                    CheckUncheckChildren(parentTable.nextSibling, src.checked); 
                } 
            } 
            //check or uncheck parents at all levels 
            CheckUncheckParents(src, src.checked); 
        } 
    } 
    function CheckUncheckChildren(childContainer, check) { 
        var childChkBoxes = childContainer.getElementsByTagName("input"); 
        var childChkBoxCount = childChkBoxes.length; 
        for (var i = 0; i < childChkBoxCount; i++) { 
            childChkBoxes[i].checked = check; 
        } 
    } 
//    function CheckUncheckParents(srcChild, check) { 
//        var parentDiv = GetParentByTagName("div", srcChild); 
//        var parentNodeTable = parentDiv.previousSibling; 

//        if (parentNodeTable) { 
//            var checkUncheckSwitch; 

////            if (check) //checkbox checked 
////            { 
//            var isAnySiblingChecked = AreAnySiblingsChecked(srcChild); 
//            if (isAnySiblingChecked) 
//                checkUncheckSwitch = true; 
//            else 
//                checkUncheckSwitch = false;
////            } 
////            else //checkbox unchecked 
////            { 
////                checkUncheckSwitch = false; 
////            } 

//            var inpElemsInParentTable = parentNodeTable.getElementsByTagName("input"); 
//            if (inpElemsInParentTable.length > 0) { 
//                var parentNodeChkBox = inpElemsInParentTable[0]; 
//                parentNodeChkBox.checked = checkUncheckSwitch; 
//                //do the same recursively 
//                CheckUncheckParents(parentNodeChkBox, checkUncheckSwitch); 
//            } 
//        } 
//    } 
//    function AreAnySiblingsChecked(chkBox) { 
//        var parentDiv = GetParentByTagName("div", chkBox); 
//        var childCount = parentDiv.childNodes.length; 
//        for (var i = 0; i < childCount; i++) { 
//            if (parentDiv.childNodes[i].nodeType == 1) //check if the child node is an element node 
//            { 
//                if (parentDiv.childNodes[i].tagName.toLowerCase() == "table") { 
//                    var prevChkBox = parentDiv.childNodes[i].getElementsByTagName("input")[0]; 
//                    //if any of sibling nodes are not checked, return false 
//                    if (prevChkBox.checked) { 
//                        return true; 
//                    } 
//                } 
//            } 
//        } 
//        return false; 
//    } 
    //utility function to get the container of an element by tagname 
    function GetParentByTagName(parentTagName, childElementObj) { 
        var parent = childElementObj.parentNode; 
        while (parent.tagName.toLowerCase() != parentTagName.toLowerCase()) { 
            parent = parent.parentNode; 
        } 
        return parent; 
    } 
    function CheckUncheckParents(srcChild, check) { 
        var parentDiv = GetParentByTagName("div", srcChild); 
        var parentNodeTable = parentDiv.previousSibling; 
        if (parentNodeTable) { 
            var checkUncheckSwitch; 

            if (check) //checkbox checked 
            { 
                var isAllSiblingsChecked = AreAllSiblingsChecked(srcChild); 
                if (isAllSiblingsChecked) 
                    checkUncheckSwitch = true; 
                else 
                    return; //do not need to check parent if any(one or more) child not checked 
            } 
            else //checkbox unchecked 
            { 
                checkUncheckSwitch = false; 
            } 

            var inpElemsInParentTable = parentNodeTable.getElementsByTagName("input"); 
            if (inpElemsInParentTable.length > 0) { 
                var parentNodeChkBox = inpElemsInParentTable[0]; 
                parentNodeChkBox.checked = checkUncheckSwitch; 
                //do the same recursively 
                CheckUncheckParents(parentNodeChkBox, checkUncheckSwitch); 
            } 
        } 
    } 
    function AreAllSiblingsChecked(chkBox) { 
        var parentDiv = GetParentByTagName("div", chkBox); 
        var childCount = parentDiv.childNodes.length; 
        for (var i = 0; i < childCount; i++) { 
            if (parentDiv.childNodes[i].nodeType == 1) //check if the child node is an element node 
            { 
                if (parentDiv.childNodes[i].tagName.toLowerCase() == "table") { 
                    var prevChkBox = parentDiv.childNodes[i].getElementsByTagName("input")[0]; 
                    //if any of sibling nodes are not checked, return false 
                    if (!prevChkBox.checked) { 
                        return false; 
                    } 
                } 
            } 
        } 
        return true; 
    } 
    
</script>

</head>
<%--<body id="bod" onload="javascript: showtime();">--%>
<body id="bod">
    <AdHoc:AdminMainMenu ID="menu" runat="server" />
    
    <form id="form1" runat="server">
        <input type="hidden" id="javaCheckBoxTagName" value="div" runat="server" />    
        <asp:ScriptManager ID="ScriptManager1" runat="server" />
    <div id="submenu">
        <AdHoc:NavMenu ID="subnav" runat="server" />
    </div>

        <script type="text/javascript">
        Sys.WebForms.PageRequestManager.getInstance().add_endRequest(EndRequestHandler);
        function EndRequestHandler(sender, args) {
            noMessage=false;
            $find('ahSavePopupBehavior').hide();        
           // timerID = setTimeout("showtime()",1000);
        }
        
//        function showtime () {
//            var now = new Date();
//            var hours = now.getHours();
//            var minutes = now.getMinutes();
//            var seconds = now.getSeconds()
//            var timeValue = "" + ((hours >12) ? hours -12 :hours)
//            if (timeValue == "0") timeValue = 12;
//            timeValue += ((minutes < 10) ? ":0" : ":") + minutes
//            timeValue += ((seconds < 10) ? ":0" : ":") + seconds
//            timeValue += (hours >= 12) ? " P.M." : " A.M."
//            document.form1.face.value = timeValue;
//        }

        </script> 
        
        <%--<input type="text" name="face" size=13 value="">--%>
        
        <div class="divForm">

            <asp:UpdatePanel ID="UP1" runat="server">
                <ContentTemplate>
                    <input type="hidden" id="ahDirty" name="ahDirty" runat="server" />
                    
                    <h3>
                    Step 1 of 2 � Import Data Objects
                    </h3>
                    <%--<p class="info">
                        This is the first of the two steps of the Database Schema Wizard. While the next step is 
                        technically considered optional, it is highly recommended that you take it if you import 
                        any new data objects or columns, for an optimum user experience. <br />
                        Please select objects or columns that you would like to import to, or remove from the meta 
                        database and click �Add Selected�/ �Remove Selected� button. After you are done, click 
                        �Define Friendly Names� to go to the next step. If you don�t want to import or remove any 
                        objects, but would like to change or define friendly names, you may skip this step and go 
                        to the next.
                    </p>--%>
                    <p class="info">
                        To manage object and column definitions in the metadata database, select the objects and 
                        columns and click on either the "Add Selected" or "Remove Selected" buttons. <br /><br />
                        To set user-friendly names for objects and columns, click on the "Define Friendly Names" 
                        button. It is highly recommended that friendly names are defined to improve the end-user 
                        experience.
                    </p>
                    <br />
                    <%--<div class="divButtons">
                        <asp:Button ID="btnObjectInfo1" runat="server" CssClass="command"
                            OnClick="ModifyObjectInfo" Text="Define Friendly Names" ToolTip="Click to modify friendly names for tables and columns." />
                        <asp:Button ID="btnCancel1" runat="server" CssClass="command"
                            OnClick="btnCancel_OnClick" Text="Back to Database Connection" ToolTip="Click to go back to Database connection page." />
                        <br />
                        <br />
                        <asp:Button ID="btnAddSelected1" runat="server" CssClass="command"
                            OnClick="AddSelected" Text="Add Selected" ToolTip="Click to add the selected objects and columns." />
                        <asp:Button ID="btnRemoveSelected1" runat="server" CssClass="command"
                            OnClick="RemoveSelected" Text="Remove Selected" ToolTip="Click to remove selected objects and columns." />
                        <asp:Button ID="btnSelectSchema1" runat="server" CssClass="command"
                            OnClick="SelectSchema" Text="Select Schema" ToolTip="Click to select another schema." />
                    </div>
                    <br />--%>
                    Objects per page:&nbsp;&nbsp;<asp:TextBox ID="txtNumRowsPerPage" runat="server" Text="1" Width="100" />
                    <asp:RangeValidator ID="rvRowsPerPage" runat="server" ControlToValidate="txtNumRowsPerPage" 
                        Type="Integer" MinimumValue="0" MaximumValue="1000" meta:resourcekey="rvRowsPerPageResource1"
                        ErrorMessage="Objects per page accepts only integer values between 0 and 1000." >*</asp:RangeValidator>
                    <asp:Button ID="btnUpdateRowsPerPage" runat="server" Text="Update" OnClick="UpdateRowsPerPage" 
                        CssClass="command" ToolTip="Click to update the tree view to display the selected number of objects per page." />
                    <asp:ValidationSummary ID="vSummary1" runat="server" />
                    <br />
                    <br />
                    <asp:Button ID="btnShowTablesView" runat="server" CssClass="command" 
                        Text="Tables" OnClick="ShowTablesView" ToolTip="Click to show all tables." />
                    <asp:Button ID="btnShowViewsView" runat="server" CssClass="command" 
                        Text="Views" OnClick="ShowViewsView" ToolTip="Click to show all views." />   
                    <div id="divObjCols" runat="server">
                        <br /> 
                        <br />
                        <cc1:StyledTreeView ID="tvObjCols" runat="server" ShowCheckBoxes="All" ShowExpandCollapse="true" 
                            ShowLines="true" >
                            <NodeStyle HorizontalPadding="4px" />
                            <SelectedNodeStyle HorizontalPadding="4px" CssClass="treenodeSelected" />
                        </cc1:StyledTreeView>
                        
                        <br />
                        
                        <AdHoc:PagingControl id="pageCtrl" runat="server" OnGotoNextPage="ChangePageIndex" />
                    </div>
                    <br />
                    <div id="divButtons" class="divButtons">
                        <asp:Button ID="btnAddSelected2" runat="server" CssClass="command"
                            OnClick="AddSelected" Text="Add Selected" ToolTip="Click to add the selected objects and columns." />
                        <asp:Button ID="btnRemoveSelected2" runat="server" CssClass="command"
                            OnClick="RemoveSelected" Text="Remove Selected" ToolTip="Click to remove selected objects and columns." />
                        <asp:Button ID="btnSelectSchema2" runat="server" CssClass="command"
                            OnClick="SelectSchema" Text="Select Schema" ToolTip="Click to select another schema." />
                        <br />
                        <br />
                        <asp:Button ID="btnObjectInfo2" runat="server" CssClass="command"
                            OnClick="ModifyObjectInfo" Text="Define Friendly Names" ToolTip="Click to modify friendly names for tables and columns." />
                        <asp:Button ID="btnCancel2" runat="server" CssClass="command"
                            OnClick="btnCancel_OnClick" Text="Back to Database Connection" ToolTip="Click to go back to Database connection page." />
                    </div>
                    
                    <asp:Button runat="server" ID="Button1" Style="display: none" />
                    <ajaxToolkit:ModalPopupExtender runat="server" ID="ahModalPopup" BehaviorID="ahModalPopupBehavior"
                        TargetControlID="Button1" PopupControlID="ahPopup" BackgroundCssClass="modalBackground"
                        DropShadow="false">
                    </ajaxToolkit:ModalPopupExtender>
                    
                    <asp:Panel runat="server" CssClass="modalPopup" ID="ahPopup" Style="display: none;
                        width: 306;">
                        <div class="modalPopupHandle" style="width: 300px;">
                            Select Schema
                        </div>
                        <div class="modalDiv">
                            <asp:UpdatePanel ID="upPopup" runat="server">
                                <ContentTemplate>  
                                    <div style="width: 280px;">
                                        Please select the schemas you would like to import Tables and Views from:
                                    </div>   
                                    <br />
                                    <br />
                                    <asp:ListBox ID="lbSchemas" runat="server" Rows="10" SelectionMode="Multiple">
                                    </asp:ListBox>
                                    <div id="divButtons1" runat="server">
                                        <asp:Button ID="btnClosePopupOK" CssClass="command" OnClick="ClosePopupOK_OnClick" runat="server"
                                            Text="<%$ Resources:LogiAdHoc, OKWithSpaces %>" UseSubmitBehavior="false" CausesValidation="false" />
                                        <asp:Button ID="btnClosePopupCancel" CssClass="command" OnClick="ClosePopupCancel_OnClick"
                                            runat="server" Text="<%$ Resources:LogiAdHoc, Cancel %>" CausesValidation="False"/>
                                    </div>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </div>
                    </asp:Panel>    
                </ContentTemplate>
            </asp:UpdatePanel>

            <div id="divSaveAnimation">
                <asp:Button runat="server" ID="btnSaveAnimation" Style="display: none" />
                <ajaxToolkit:ModalPopupExtender runat="server" ID="ahSavePopup" BehaviorID="ahSavePopupBehavior"
                    TargetControlID="btnSaveAnimation" PopupControlID="pnlSaving" BackgroundCssClass="modalBackground"
                    DropShadow="False">
                </ajaxToolkit:ModalPopupExtender>
                <asp:Panel id="pnlSaving" CssClass="savePopup" runat="server" style="display:none;">
                    <table><tr><td>
                    <asp:Image ID="imgSave" runat="server" SkinID="imgSaving" EnableViewState="false" AlternateText="<%$ Resources:LogiAdHoc, SaveImageAltText %>" />
                    </td>
                    <td valign="middle">
                    <asp:Label ID="lblSaveText" CssClass="lblSavePopup" runat="server" EnableViewState="false" Text="<%$ Resources:LogiAdHoc, PleaseWait %>"/>
                    </td></tr></table>
                </asp:Panel>
            </div>
            
            <div id="divLegend" class="legend" runat="server">
                <asp:Localize ID="locLegend" runat="server" Text="<%$ Resources:LogiAdHoc, LegendLabel %>"></asp:Localize>
                <dl id="dlNewColumn" runat="server">
                    <dt>
                        <img id="Img1" runat="server" alt="Source only" border="0" height="13" 
                            src="../ahImages/spacer.gif" style="background-color: Black;" title="Source only" width="13" />
                    </dt>
                    <dd style="color: Black;">
                        <asp:Localize ID="Localize9" runat="server" Text="Source only"></asp:Localize>&nbsp;
                    </dd>
                </dl>
                <dl id="dlUnchangedColumn" runat="server">
                    <dt>
                        <img id="Img2" runat="server" alt="Both" border="0" height="13"
                            src="../ahImages/spacer.gif" style="background-color: #008800;" title="Both" width="13" />
                    </dt>
                    <dd style="color: #008800;">
                        <asp:Localize ID="Localize10" runat="server" Text="Both"></asp:Localize>&nbsp;
                    </dd>
                </dl>
                <dl id="dlChangedColumn" runat="server">
                    <dt>
                        <img id="Img3" runat="server" alt="Different" border="0" height="13" 
                            src="../ahImages/spacer.gif" style="background-color: Orange;" title="Different" width="13" />
                    </dt>
                    <dd style="color: Orange">
                        <asp:Localize ID="Localize11" runat="server" Text="Different"></asp:Localize>&nbsp;
                    </dd>
                </dl>
                <dl id="dlRemovedColumn" runat="server">
                    <dt>
                        <img id="imgMetaOnly" runat="server" alt="Logi Ad Hoc only" border="0" height="13" 
                            src="../ahImages/spacer.gif" style="background-color: #ff0000;" title="Logi Ad Hoc only" width="13" />
                    </dt>
                    <dd style="color: #ff0000;">
                        <asp:Localize ID="LcMetaOnly" runat="server" Text="Logi Ad Hoc only"></asp:Localize>&nbsp;
                    </dd>
                </dl>
            </div>
                            
            <%--<div id="divHelp" class="help">
                <a href="../Help.aspx?src=34" target="_blank">
                    <asp:Localize ID="GetHelp" runat="server" Text="Get help with importing a Database Schema."></asp:Localize>
                </a>
            </div>--%>
        </div>
    </form>
</body>
</html>
