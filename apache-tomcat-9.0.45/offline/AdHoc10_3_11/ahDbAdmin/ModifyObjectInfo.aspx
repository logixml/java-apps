<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="ModifyObjectInfo.aspx.vb" Theme="green" Inherits="LogiAdHoc.ahDbAdmin_ModifyObjectInfo" Culture="auto" meta:resourcekey="PageResource1" UICulture="auto" %>

<%@ Register TagPrefix="AdHoc" TagName="AdminMainMenu" Src="~/ahControls/AdminMainMenu.ascx" %>
<%@ Register TagPrefix="AdHoc" TagName="NavMenu" Src="~/ahControls/NavMenu.ascx" %>
<%@ Register TagPrefix="AdHoc" TagName="Search" Src="~/ahControls/Search.ascx" %>
<%@ Register TagPrefix="AdHoc" TagName="PagingControl" Src="../ahControls/PagingControl.ascx" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Databases</title>
    <script language="JavaScript" type="text/javascript" src="../ahScripts/ahChange.js"></script>
    
    <script type="text/javascript">
        window.onbeforeunload = AlertOnExit;
    </script>
    
    <script type="text/javascript">
        function ExpandCollapseAll()
        {
            var gridID = 'grdMain';
            var oRows = document.getElementById(gridID).getElementsByTagName('tr');
            var l = oRows.length;
            var bExpand = true;
            
            var ih = document.getElementById("ihExpandCollapseAll");
            var lbtn = document.getElementById("lbtnExpandCollapseAll");
            if (ih.value == "0")
            {
                bExpand = false;
                ih.value = "1";
                lbtn.innerText = "Expand All";
                lbtn.textContent = "Expand All"; //FireFox
            }
            else
            {
                bExpand = true;
                ih.value = "0";
                lbtn.innerText = "Collapse All";
                lbtn.textContent = "Collapse All"; //FireFox
            }
                
            for (i=2; i<=l; i++) {
                var o = pack(i.toString(10),2);
	            var imgID = gridID + "_ctl" + o + "_imgExpandCollapse";
	            var trID = gridID + "_ctl" + o + "_trColumnsGrid"
	            var ihID = gridID + "_ctl" + o + "_ihExpandCollapse";
	            var img = document.getElementById(imgID);
	            var tr = document.getElementById(trID);
	            ih = document.getElementById(ihID);
	            if (img && tr && ih) {
	                ExpandCollapseRow(bExpand, tr, img, ih)
	            }
            }
        }
        
        function ExpandCollapseObject(srcImg, trID, ihID)
        {
            var grdTR = document.getElementById(trID);
            var ih = document.getElementById(ihID);
            if (grdTR.style.display == 'none')
            {
                ExpandCollapseRow(true, grdTR, srcImg, ih)
                //grdTR.style.display = '';
                //srcImg.src = "../ahImages/iconMinus.gif";
                //ih.value = "1";
            }
            else
            {
                ExpandCollapseRow(false, grdTR, srcImg, ih)
                //grdTR.style.display = 'none';
                //srcImg.src = "../ahImages/iconPlus.gif";
                //ih.value = "0";
            }
        }
        function ExpandCollapseRow(bExpand, grdTR, srcImg, ih)
        {
            if (bExpand)
            {
                grdTR.style.display = '';
                srcImg.src = "../ahImages/iconMinus.gif";
                ih.value = "1";
            }
            else
            {
                grdTR.style.display = 'none';
                srcImg.src = "../ahImages/iconPlus.gif";
                ih.value = "0";
            }
        }
        
        function SelectAllObjects(chkVal, idVal) {

            var gridID = 'grdMain';
            var chkID = 'chk_Select';
            
            var oRows = document.getElementById(gridID).getElementsByTagName('tr');
            var l = oRows.length;
            //var l = grdMain.rows.length;
            var e;
            var s;
            if (idVal.indexOf ('CheckAll') != -1) {
	            var bSet; 
	            // Check if main checkbox is checked, then select or deselect datagrid checkboxes
	            if (chkVal == true) {
		            bSet = true;
	            } else {
		            bSet = false;
	            }
	            // Loop through all elements
	            for (i=2; i<=l; i++) {
	                var o = pack(i.toString(10),2);
		            s = gridID + "_ctl" + o + "_" + chkID;
		            e = document.getElementById(s);
		            if (e!=null) {
		                e.checked = bSet;
		                SelectAllColumns(chkVal, s);
		            }
	            }
            }
        }
        
        function SelectAllColumns(chkVal, idVal) {
            var chkBox = document.getElementById(idVal);
            var gridID = idVal.replace('chk_Select', '') + "grdColumns";  
            
            var oRows = document.getElementById(gridID).getElementsByTagName('tr');          
            var l = oRows.length;
            
            // Loop through all elements
            for (j=2; j<=l; j++) {
                var o = pack(j.toString(10),2);
	            s = gridID + "_ctl" + o + "_chk_Select";
	            e = document.getElementById(s);
	            if (e!=null) e.checked = chkVal;
            }
        }
    </script>
</head>
<body id="bod">
    <AdHoc:AdminMainMenu ID="menu" runat="server" />
    
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server" />
    <div id="submenu">
        <AdHoc:NavMenu ID="subnav" runat="server" />
    </div>

        <script type="text/javascript">
        Sys.WebForms.PageRequestManager.getInstance().add_endRequest(EndRequestHandler);
        function EndRequestHandler(sender, args) {
            noMessage=false;
            $find('ahSavePopupBehavior').hide();        
            //ResetGridDisplay();
        }
        
//        function ResetGridDisplay() {
//            
//            var oRows = document.getElementById('grdMain').getElementsByTagName('tr');
//            var l = oRows.length;
//            var img;
//            var s;
//            var ih;
//            var tr;
//	        
//	        // Loop through all elements
//	        for (i=2; i<=l; i++) {
//	            var o = pack(i.toString(10),2);
//		        s = "grdMain_ctl" + o + "_imgExpandCollapse";
//		        img = document.getElementById(s);
//		        if (img!=null) {
//		           s = "grdMain_ctl" + o + "_ihExpandCollapse";
//		           ih = document.getElementById(s);
//		           
//		           s = "grdMain_ctl" + o + "_trColumnsGrid";
//		           tr = document.getElementById(s);
//		           
//		           if (ih.value == "1") {
//		                tr.style.display = '';
//                        img.src = "../ahImages/iconMinus.gif";
//		           }
//		           else {
//		                tr.style.display = 'none';
//                        img.src = "../ahImages/iconPlus.gif";
//		           }
//	            }
//            }
//        }
        </script> 
        
        <div class="divForm">
            <%--<AdHoc:Search ID="srch" runat="server" Title="Find Database" Width="400" /> --%>       
            <input type="hidden" id="ahDirty" name="ahDirty" runat="server" />

            <asp:UpdatePanel ID="UP1" runat="server" UpdateMode="conditional">
                <ContentTemplate>
                    
                    <h3>
                    Step 2 of 2 � Define Friendly Names
                    </h3>
                    <%--<p class="info">
                        In this step you give your data objects and their columns user friendly names. These are 
                        names that your end users see when they build report, so it is important that they are 
                        relevant and meaningful. Objects� original names are used as default. You can manually 
                        change those or use tools provided to have Logi Ad Hoc guess appropriate names based on 
                        some rules.
                    </p>--%>
                    <p class="info">
                        Friendly Names are displayed to the end user and are used as the default identification 
                        of data in a report. The Friendly Names may be set manually or generated by selecting 
                        the objects/columns, clicking on the "Generate Friendly Name" button and selecting one 
                        of the available rules.
                    </p>
                    <br />
                    Objects per page:&nbsp;&nbsp;<asp:TextBox ID="txtNumRowsPerPage" runat="server" Text="1" Width="100" />
                    <asp:RangeValidator ID="rvRowsPerPage" runat="server" ControlToValidate="txtNumRowsPerPage"
                        Type="Integer" MinimumValue="0" MaximumValue="1000" meta:resourcekey="rvRowsPerPageResource1"
                        ErrorMessage="Objects per page accepts only integer values between 0 and 1000." >*</asp:RangeValidator>
                    <asp:Button ID="btnUpdateRowsPerPage" runat="server" Text="Update" OnClick="UpdateRowsPerPage" 
                        CssClass="command" ToolTip="Click to update the grid to display the selected number of objects per page." />
                    <br />
                    <br />
                    <asp:UpdatePanel ID="UP3" runat="server" UpdateMode="conditional">
                        <ContentTemplate>
                            <div class="divButtons" id="divButtons1" runat="server">
                                <asp:Button ID="btnSave1" runat="server" CssClass="command"
                                    OnClick="Save_OnClick" Text="Save" ToolTip="Click to save the changes." 
                                    OnClientClick="javascript: document.getElementById('ahDirty').value=0;ShowSaveAnimation();"/>
                                <asp:Button ID="btnCancel1" runat="server" CssClass="command"
                                    OnClick="Cancel_OnClick" Text="Back to Schema" ToolTip="Click to go back importing schema page." />
                                <br />
                                <br />
                                <asp:Button ID="btnGenerateLabels1" runat="server" CssClass="command" CausesValidation="false"
                                    OnClick="GenerateLabels_OnClick" Text="Generate Friendly Names" ToolTip="Click to automatically generate friendly names." />
                            </div>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                    
                    <asp:LinkButton ID="lbtnExpandCollapseAll" runat="server" OnClientClick="ExpandCollapseAll(); return false;" Text="Expand/Collapse All"/>
                    <input type="hidden" id="ihExpandCollapseAll" runat="server" />
                    <asp:GridView ID="grdMain" runat="server" AllowPaging="True" AllowSorting="True"
                        AutoGenerateColumns="False" CssClass="grid" DataKeyNames="ObjectID"
                        OnRowDataBound="OnItemDataBoundHandler" OnSorting="OnSortCommandHandler">
                        <Columns>
                            <asp:TemplateField>
                                <HeaderTemplate>
                                    <table>
                                        <tr>
                                            <td width="30px">
                                                
                                            </td>
                                            <td width="50px" style="text-align: left;">
                                                <asp:Label ID="lblChkAll" runat="server" AssociatedControlID="CheckAll" Text="<%$ Resources:LogiAdHoc, SelectDeselectAll %>" CssClass="NoShow"></asp:Label>
                                                <asp:CheckBox ID="CheckAll" runat="server" ToolTip="<%$ Resources:LogiAdHoc, SelectDeselectAll %>" />   
                                            </td>
                                            <td width="200px">
                                                Object Name
                                            </td>
                                            <td>
                                                Friendly Name
                                            </td>
                                        </tr>
                                    </table>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <input type="hidden" id="ihObjectID" runat="server" />
                                    <table>
                                        <tr>
                                            <td width="30px">
                                                <img id="imgExpandCollapse" runat="server" src="../ahImages/iconPlus.gif" />
                                                <input type="hidden" id="ihExpandCollapse" runat="server" />
                                            </td>
                                            <td width="50px">
                                                <asp:Label ID="lblChk" runat="server" AssociatedControlID="chk_Select" Text="<%$ Resources:LogiAdHoc, SelectDeselect %>" CssClass="NoShow"></asp:Label>
                                                <asp:CheckBox ID="chk_Select" runat="server" ToolTip="<%$ Resources:LogiAdHoc, SelectDeselect %>" />   
                                            </td>
                                            <td width="200px">
                                                <asp:Label ID="lblObjectName" runat="server" />
                                            </td>
                                            <td>
                                                <input type="hidden" id="ihObjectLabelChanged" runat="server" />
                                                <asp:TextBox ID="txtObjectLabel" runat="server"></asp:TextBox>
                                                <asp:RequiredFieldValidator ID="rfvObjectLabel" runat="server"
                                                    ControlToValidate="txtObjectLabel" ErrorMessage="Object Friendly Name is required.">*</asp:RequiredFieldValidator>
                                                <asp:CustomValidator ID="cvObjectLabel" runat="server" ControlToValidate="txtObjectLabel"
                                                    EnableClientScript="False" ErrorMessage="Friendly Name contains special characters not allowed in the application."
                                                    meta:resourcekey="cvObjectLabelResource1" OnServerValidate="IsObjectLabelValid">*</asp:CustomValidator>
                                            </td>
                                        </tr>
                                        <tr id="trColumnsGrid" runat="server">
                                            <td colspan="4" align="center">
                                                <asp:GridView ID="grdColumns" runat="server" AutoGenerateColumns="false"
                                                    CssClass="grid" OnRowDataBound="OnColumnDataBoundHandler" >
                                                    <Columns>
                                                        <asp:TemplateField>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblChk" runat="server" AssociatedControlID="chk_Select" Text="<%$ Resources:LogiAdHoc, SelectDeselect %>" CssClass="NoShow"></asp:Label>
                                                                <asp:CheckBox ID="chk_Select" runat="server" ToolTip="<%$ Resources:LogiAdHoc, SelectDeselect %>" />            
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Column">
                                                            <ItemStyle HorizontalAlign="Left" />
                                                            <ItemTemplate>
                                                                <input type="hidden" ID="ihColumnID" runat="server" />
                                                                <asp:Label ID="lblColumnName" runat="server" />
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Friendly Name">
                                                            <ItemTemplate>
                                                                <input type="hidden" id="ihColumnLabelChanged" runat="server" />
                                                                <asp:TextBox ID="txtColumnFriendlyName" runat="server" />
                                                                <asp:RequiredFieldValidator ID="rfvObjectLabel" runat="server"
                                                                    ControlToValidate="txtColumnFriendlyName" 
                                                                    ErrorMessage="Column Friendly Name is required.">*</asp:RequiredFieldValidator>
                                                                <asp:CustomValidator ID="cvColumnLabel" runat="server" ControlToValidate="txtColumnFriendlyName"
                                                                    EnableClientScript="False" ErrorMessage="Friendly Name contains special characters not allowed in the application."
                                                                    meta:resourcekey="cvObjectLabelResource1" OnServerValidate="IsObjectLabelValid">*</asp:CustomValidator>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                    </Columns>
                                                    <%--<PagerStyle HorizontalAlign="Center" />--%>
                                                    <HeaderStyle CssClass="gridheader" />
                                                    <%--<PagerTemplate>
                                                        <AdHoc:PagingControl ID="ColumnPageCtrl" runat="server" OnGotoNextPage="ChangePageIndex1"/>
                                                    </PagerTemplate>--%>
                                                    <RowStyle CssClass="gridrow" />
                                                    <AlternatingRowStyle CssClass="gridalternaterow" /> 
                                                </asp:GridView>
                                            </td>
                                        </tr>
                                    </table>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <PagerStyle HorizontalAlign="Center" />
                        <HeaderStyle CssClass="gridheader" />
                        <PagerTemplate>
                            <AdHoc:PagingControl ID="PageCtrl" runat="server" OnGotoNextPage="ChangePageIndex"/>
                        </PagerTemplate>
                        <RowStyle CssClass="gridrow" />
                        <AlternatingRowStyle CssClass="gridalternaterow" />
                    </asp:GridView>
                    
                    <asp:ValidationSummary ID="vSummary1" runat="server" />
                    <asp:UpdatePanel ID="UP2" runat="server" UpdateMode="conditional">
                        <ContentTemplate>
                            <div id="divButtons2" class="divButtons">
                                <asp:Button ID="btnGenerateLabels2" runat="server" CssClass="command" CausesValidation="false"
                                    OnClick="GenerateLabels_OnClick" Text="Generate Friendly Names" ToolTip="Click to automatically generate friendly names." />
                                <br />
                                <br />
                                <asp:Button ID="btnSave2" runat="server" CssClass="command"
                                    OnClick="Save_OnClick" Text="Save" ToolTip="Click to save the changes." 
                                    OnClientClick="javascript: document.getElementById('ahDirty').value=0;ShowSaveAnimation();"/>
                                <asp:Button ID="btnCancel2" runat="server" CssClass="command" CausesValidation="false"
                                    OnClick="Cancel_OnClick" Text="Back to Schema" ToolTip="Click to go back to importing schema page." />
                            </div>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                    
                    <asp:Button runat="server" ID="Button1" Style="display: none" />
                    <ajaxToolkit:ModalPopupExtender runat="server" ID="ahModalPopup" BehaviorID="ahModalPopupBehavior"
                        TargetControlID="Button1" PopupControlID="ahPopup" BackgroundCssClass="modalBackground"
                        DropShadow="false">
                    </ajaxToolkit:ModalPopupExtender>
                    
                    <asp:Panel runat="server" CssClass="modalPopup" ID="ahPopup" Style="display: none;width: 406px;">
                        <div class="modalPopupHandle" style="width: 400px">
                            Auto-generate Friendly Names
                        </div>
                        <div class="modalDiv">
                            <asp:UpdatePanel ID="upPopup" runat="server">
                                <ContentTemplate>     
                                    <div id="divOptions" runat="server">
                                        Please select your preferred method for generating Friendly Names:
                                        <br />
                                        <br />
                                        <asp:RadioButton ID="rbtnCamelCase" runat="server" GroupName="LabelMethod"
                                            Text="Based on camel case naming convention." Checked="True"/>
                                        <br />
                                        <asp:RadioButton ID="rbtnHyphenated" runat="server" GroupName="LabelMethod" 
                                            Text="Based on hyphenated naming convention"/>
                                    </div>
                                    <p>
                                    </p>
                                    <div id="div1" runat="server">
                                        <asp:Button ID="btnClosePopupOK" CssClass="command" OnClick="ClosePopupOK_OnClick" runat="server"
                                            Text="<%$ Resources:LogiAdHoc, OKWithSpaces %>" UseSubmitBehavior="false" CausesValidation="false" />
                                        <asp:Button ID="btnClosePopupCancel" CssClass="command" OnClick="ClosePopupCancel_OnClick"
                                            runat="server" Text="<%$ Resources:LogiAdHoc, Cancel %>" CausesValidation="False"/>
                                    </div>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </div>
                    </asp:Panel>
                    
                </ContentTemplate>
                <Triggers>
                    <%--<asp:AsyncPostBackTrigger ControlID="srch" EventName="DoSearch" />--%>
                    <%--<asp:AsyncPostBackTrigger ControlID="btnRemoveDatabase1" EventName="Click" />
                    <asp:AsyncPostBackTrigger ControlID="btnRemoveDatabase2" EventName="Click" />--%>
                </Triggers>
            </asp:UpdatePanel>

            <div id="divSaveAnimation">
                <asp:Button runat="server" ID="btnSaveAnimation" Style="display: none" />
                <ajaxToolkit:ModalPopupExtender runat="server" ID="ahSavePopup" BehaviorID="ahSavePopupBehavior"
                    TargetControlID="btnSaveAnimation" PopupControlID="pnlSaving" BackgroundCssClass="modalBackground"
                    DropShadow="False">
                </ajaxToolkit:ModalPopupExtender>
                <asp:Panel id="pnlSaving" CssClass="savePopup" runat="server" style="display:none;">
                    <table><tr><td>
                    <asp:Image ID="imgSave" runat="server" SkinID="imgSaving" EnableViewState="false" AlternateText="<%$ Resources:LogiAdHoc, SaveImageAltText %>" />
                    </td>
                    <td valign="middle">
                    <asp:Label ID="lblSaveText" CssClass="lblSavePopup" runat="server" EnableViewState="false" Text="<%$ Resources:LogiAdHoc, PleaseWait %>"/>
                    </td></tr></table>
                </asp:Panel>
            </div>
            
           <%-- <div id="divLegend" class="legend" runat="server">
                            <asp:Localize ID="locLegend" runat="server" Text="<%$ Resources:LogiAdHoc, LegendLabel %>"></asp:Localize>                
                <dl>
                    <dt>
                        <img src="../ahImages/modify.gif" alt="Modify Icon" title="Click this icon in the Database list to modify the selected Database." runat="server"/></dt>
                    <dd>
                        <asp:Localize ID="Localize1" runat="server" Text="Modify Database"></asp:Localize>&nbsp;</dd><dd></dd></dl>
            </div>--%>
            <%--<div id="divHelp" class="help">
                <a href="../Help.aspx?src=34" target="_blank">
                    <asp:Localize ID="GetHelp" runat="server" Text="Get help with the Objects screen."></asp:Localize>
                </a>
            </div>--%>
        </div>
    </form>
</body>
</html>
