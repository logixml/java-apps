<%@ Page Language="vb" AutoEventWireup="false" Codebehind="MetadataConnection.aspx.vb" Inherits="LogiAdHoc.MetadataConnection" Culture="auto" meta:resourcekey="PageResource1" UICulture="auto" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register TagPrefix="AdHoc" TagName="AdminMainMenu" Src="~/ahControls/AdminMainMenu.ascx" %>
<%@ Register TagPrefix="AdHoc" TagName="NavMenu" Src="~/ahControls/NavMenu.ascx" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Metadata Connection</title>
    <link href="../App_Themes/Green/AdHoc.css" rel="stylesheet" type="text/css" />
</head>
<body id="bod">
    <AdHoc:AdminMainMenu ID="menu" runat="server" />
    
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server" />
    <div id="submenu">
        <AdHoc:NavMenu ID="subnav" runat="server" />
    </div>
        
        <div class="divForm">
            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                <ContentTemplate>
                
                   <%-- <h2> Metadata Database Connection</h2>
                    <br />
                    <br />--%>
                    <p class="info">
                        A connection string is required to establish database connectivity. 
                        You can type in the string, copy and paste an existing string 
                        or use any of the provided wizards to build a connection string.
                    </p>
                    <table>
                        <tr>
                            <td width="120px">Connection String:</td>
                            <td id="tdConnectionString" runat="server">
                                <asp:TextBox ID="txtConnString" Rows="10" TextMode="MultiLine" Width="500px" runat="server" />
                                <asp:RequiredFieldValidator ID="rfvConnString" runat="server" ErrorMessage="Connection String is required."
                                    ControlToValidate="txtConnString" ValidationGroup="Connection" >*</asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td></td>
                            <td>
                                <div id="divWizards" runat="server" >
                                    <asp:LinkButton ID="lbtnMySQL" runat="server" Text="Build a MySQL connection string..."
                                         CausesValidation="false" OnClick="lbtnMySQL_Click" />
                                    <br />
                                    <asp:LinkButton ID="lbtnOracle" runat="server" Text="Build an Oracle connection string..."
                                         CausesValidation="false" OnClick="lbtnOracle_Click" />
                                    <br />
                                    <asp:LinkButton ID="lbtnSqlServer" runat="server" Text="Build a SQL Server connection string..."
                                         CausesValidation="false" OnClick="lbtnSqlServer_Click" />
                                </div>            
                            </td>
                        </tr>
                        <tr>
                            <td width="120px">Database:</td>
                            <td>
                                <asp:DropDownList ID="ddlDatabaseType" runat="server" Width="100px" AutoPostBack="true" OnSelectedIndexChanged="ddlDatabaseType_SelectedIndexChanged">
                                    <asp:ListItem Text="MySQL" Value="3" />
                                    <asp:ListItem Text="Oracle" Value="4" />
                                    <asp:ListItem Text="SQL Server" Value="1" />
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td width="120px">Connection Type:</td>
                            <td>
                                <asp:UpdatePanel ID="UP1" runat="server" UpdateMode="conditional">
                                    <ContentTemplate>
                                        <asp:DropDownList ID="ddlConnectionType" runat="server" Width="100px">
                                        </asp:DropDownList>
                                    </ContentTemplate>
                                    <Triggers>
                                        <asp:AsyncPostBackTrigger ControlID="ddlDatabaseType" EventName="SelectedIndexChanged" />
                                    </Triggers>
                                </asp:UpdatePanel>
                            </td>
                        </tr>
                    </table>
                    <asp:ValidationSummary ID="vSummary1" runat="server" ValidationGroup="Connection" />
                    <br />
                    <asp:Button ID="btnTest" runat="server" CssClass="command" OnClick="TestConnection"
                        Text="Test Connection" ToolTip="Test Connection" ValidationGroup="Connection" />
                    <asp:Button ID="btnSave" runat="server" CssClass="command" OnClick="SaveConnection"
                        Text="Save Connection" ToolTip="Save Connection" ValidationGroup="Connection" />

                    <asp:Button runat="server" ID="Button1" Style="display: none" />
                    <ajaxToolkit:ModalPopupExtender runat="server" ID="ahModalPopup" BehaviorID="ahModalPopupBehavior"
                        TargetControlID="Button1" PopupControlID="ahPopup" BackgroundCssClass="modalBackground"
                        DropShadow="false">
                    </ajaxToolkit:ModalPopupExtender>
                    
                    <asp:Panel runat="server" CssClass="modalPopup" ID="ahPopup" Style="display: none;
                        width: 300;">
                        <div class="modalPopupHandle">
                            <asp:Label ID="lblModalPopupHeader" runat="server"></asp:Label>
                        </div>
                        <div class="modalDiv">
                            <asp:UpdatePanel ID="upPopup" runat="server">
                                <ContentTemplate>     
                                    <input type="hidden" id="ihPopupType" value="0" runat="server" />
                                    
                                    <div id="divResults" runat="server">
                                        <h3>
                                            <b>Test Result: </b>
                                            <asp:Label ID="lblResult" runat="server"></asp:Label>
                                        </h3>
                                        <div id="divErrorMessage" runat="server" style="color: Red;">
                                            <b>Error Message: </b>
                                            <br /> 
                                            <asp:Label ID="lblErrorMessage" runat="server"></asp:Label>
                                        </div>
                                    </div>
                                    <div id="divConnectionParams" runat="server">
                                        <table>
                                            <tr>
                                                <td>Server:</td>
                                                <td>
                                                    <asp:TextBox ID="txtServer" runat="server" Width="180"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:Label ID="lblDatabase" runat="server" Text="Database:" />
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtDatabase" runat="server" Width="180"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr id="trPort" runat="server"> 
                                                <td>Port:</td>
                                                <td>
                                                    <asp:TextBox ID="txtPort" runat="server" Width="180"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr> 
                                                <td>Username:</td>
                                                <td>
                                                    <asp:TextBox ID="txtUsername" runat="server" Width="180"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr> 
                                                <td>Password:</td>
                                                <td>
                                                    <asp:TextBox ID="txtPassword" runat="server" Width="180"></asp:TextBox>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                    <p>
                                    </p>
                                    <div id="divButtons" runat="server">
                                        <asp:Button ID="btnClosePopupOK" CssClass="command" OnClick="ClosePopupOK_OnClick" runat="server"
                                            Text="<%$ Resources:LogiAdHoc, OKWithSpaces %>" UseSubmitBehavior="false" ValidationGroup="popup"/>
                                        <asp:Button ID="btnClosePopupCancel" CssClass="command" OnClick="ClosePopupCancel_OnClick"
                                            runat="server" Text="<%$ Resources:LogiAdHoc, Cancel %>" CausesValidation="False"/>
                                    </div>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </div>
                    </asp:Panel>
                    
                    <asp:Button runat="server" ID="Button2" Style="display: none" />
                    <ajaxToolkit:ModalPopupExtender runat="server" ID="mpeUpgradeRights" BehaviorID="mpeUpgradeRightsBehavior"
                        TargetControlID="Button2" PopupControlID="pnlUpgradeRights" BackgroundCssClass="modalBackground"
                        DropShadow="false">
                    </ajaxToolkit:ModalPopupExtender>
                    
                    <asp:Panel runat="server" CssClass="modalPopup" ID="pnlUpgradeRights" Style="display: none; width: 456;">
                        <div class="modalPopupHandle" style="width: 450px;">
                            Update Rights
                        </div>
                        <div class="modalDiv">
                            <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                                <ContentTemplate>     
                                    <asp:Label ID="lblUpgradeRights" runat="server" />
                                    <br />
                                    <br />
                                    <asp:ListBox ID="lbRights" runat="server" Rows="10" SelectionMode="Multiple">
                                    </asp:ListBox>
                                    <br />
                                    <div id="divButtons1" runat="server">
                                        <asp:Button ID="btnUpgradeRightsOK" CssClass="command" OnClick="btnUpgradeRightsOK_OnClick" runat="server"
                                            Text="<%$ Resources:LogiAdHoc, OKWithSpaces %>" UseSubmitBehavior="false" CausesValidation="false" />
                                        <asp:Button ID="btnUpgradeRightsCancel" CssClass="command" OnClick="btnUpgradeRightsCancel_OnClick"
                                            runat="server" Text="<%$ Resources:LogiAdHoc, Cancel %>" CausesValidation="False"/>
                                    </div>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </div>
                    </asp:Panel>    
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </form>
</body>
</html>
