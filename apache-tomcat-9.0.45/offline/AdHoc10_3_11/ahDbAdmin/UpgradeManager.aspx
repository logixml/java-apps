<%@ Page Language="vb" AutoEventWireup="false" Codebehind="UpgradeManager.aspx.vb" Inherits="LogiAdHoc.ahDbAdmin_UpgradeManager" Culture="auto" meta:resourcekey="PageResource1" UICulture="auto" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register TagPrefix="AdHoc" TagName="AdminMainMenu" Src="~/ahControls/AdminMainMenu.ascx" %>
<%@ Register TagPrefix="AdHoc" TagName="NavMenu" Src="~/ahControls/NavMenu.ascx" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Upgrade Manager</title>
    <link href="../App_Themes/Green/AdHoc.css" rel="stylesheet" type="text/css" />
    <script language="JavaScript" type="text/javascript" src="../ahScripts/ahChange.js"></script>
</head>
<body id="bod">
    <AdHoc:AdminMainMenu ID="menu" runat="server" />
    
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server" />
    <div id="submenu">
        <AdHoc:NavMenu ID="subnav" runat="server" />
    </div>
        
        <div class="divForm">
            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                <ContentTemplate>
                
                   <p class="info">
                        Upgrade Manager displays the status of all major areas of the application
                        and helps you update or repair those if needed.
                    </p>
                    
                    <div id="divMetadata" runat="server">
                        <h2><asp:Localize ID="Localize1" runat="server" Text="Metadata Database:"></asp:Localize></h2>
                        <table>
                            <tr>
                                <td width="120px">Current Version:</td>
                                <td id="tdConnectionString" runat="server">
                                    <asp:Label ID="lblMDBCurrVersion" runat="server"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td width="120px">Target Version</td>
                                <td>
                                    <asp:Label ID="lblMDBTgtVersion" runat="server"></asp:Label> 
                                </td>
                            </tr>
                        </table>
                        <br />
                        <p id="pMDBMsg" runat="server">
                            <asp:Label ID="lblMDBMessage" runat="server" />
                        </p>
                        <br />
                        <asp:Button ID="btnUpdateMetadata" runat="server" CssClass="command" OnClick="UpdateMetadata_OnClick"
                            Text="Update Metadata" ToolTip="Update Metadata" CausesValidation="False" />
                    </div>
                    
                    <div id="divReports" runat="server">
                        <h2><asp:Localize ID="Localize3" runat="server" Text="Reports:"></asp:Localize></h2>
                        <table>
                            <tr>
                                <td width="120px">Current Version:</td>
                                <td id="td1" runat="server">
                                    <asp:Label ID="lblRptCurrVersion" runat="server"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td width="120px">Target Version</td>
                                <td>
                                    <asp:Label ID="lblRptTgtVersion" runat="server"></asp:Label> 
                                </td>
                            </tr>
                        </table>
                        <br />
                        <p id="pRptMsg" runat="server">
                            <asp:Label ID="lblRptMessage" runat="server" />
                        </p>
                        <br />
                        <asp:Button ID="btnUpdateReports" runat="server" CssClass="command" OnClick="UpdateReports_OnClick"
                            Text="Update Reports" ToolTip="Update Reports" CausesValidation="False" />
                    </div>
                    
                    <div id="divRights" runat="server">
                        <h2><asp:Localize ID="Localize2" runat="server" Text="Rights:"></asp:Localize></h2>
                        <asp:ListBox ID="lbRights" runat="server" Rows="10" SelectionMode="Multiple">
                        </asp:ListBox>
                        <br />
                        <p id="pRightsMsg" runat="server">
                            <asp:Label ID="lblUpgradeRights" runat="server" />
                        </p>
                        <br />
                        <asp:Button ID="btnUpgradeRights" CssClass="command" OnClick="UpgradeRights_OnClick" runat="server"
                            Text="Update Rights" UseSubmitBehavior="false" CausesValidation="false" />
                    </div>
                    
                    <asp:Button runat="server" ID="Button1" Style="display: none" />
                    <ajaxToolkit:ModalPopupExtender runat="server" ID="ahModalPopup" BehaviorID="ahModalPopupBehavior"
                        TargetControlID="Button1" PopupControlID="ahPopup" BackgroundCssClass="modalBackground"
                        DropShadow="false">
                    </ajaxToolkit:ModalPopupExtender>
                    
                    <asp:Panel runat="server" CssClass="modalPopup" ID="ahPopup" Style="display: none; width: 500px;">
                        <div class="modalPopupHandle">
                            <asp:Localize ID="Localize4" runat="server" Text="Upgrade Reports"></asp:Localize>
                        </div>
                        <div class="modalDiv">
                            <asp:UpdatePanel ID="upPopup" runat="server">
                                <ContentTemplate>     
                                    <div id="divReportsList" runat="server" style="width: 450px; height: 500px; overflow: auto;">
                                        <asp:GridView ID="grdReportsList" runat="server" AllowPaging="True" AllowSorting="True"
                                            AutoGenerateColumns="False" CssClass="grid" OnRowDataBound="OnItemDataBoundHandler">
                                            <Columns>
                                                <asp:TemplateField>
                                                    <HeaderTemplate>
                                                        <asp:Label ID="lblChkAll" runat="server" AssociatedControlID="CheckAll" Text="<%$ Resources:LogiAdHoc, SelectDeselectAll %>" CssClass="NoShow"></asp:Label>
                                                        <asp:CheckBox ID="CheckAll" runat="server" ToolTip="<%$ Resources:LogiAdHoc, SelectDeselectAll %>" />
                                                    </HeaderTemplate>
                                                    <ItemStyle HorizontalAlign="Center" />
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblChk" runat="server" AssociatedControlID="chk_Select" Text="<%$ Resources:LogiAdHoc, SelectDeselect %>" CssClass="NoShow"></asp:Label>
                                                        <asp:CheckBox ID="chk_Select" runat="server" ToolTip="<%$ Resources:LogiAdHoc, SelectDeselect %>" />
                                                        <input type="hidden" id="ihReportID" runat="server" />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:BoundField DataField="ReportName" HeaderText="Report Name">
                                                    <HeaderStyle Width="200px" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="UserName" HeaderText="Owner">
                                                    <HeaderStyle Width="200px" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="FolderPath" HeaderText="Folder">
                                                    <HeaderStyle Width="200px" />
                                                </asp:BoundField>
                                            </Columns>
                                            <HeaderStyle CssClass="gridheader" />
                                            <RowStyle CssClass="gridrow" />
                                            <AlternatingRowStyle CssClass="gridalternaterow" />
                                        </asp:GridView>
                                    </div>
                                    <div id="divButtons" runat="server">
                                        <asp:Button ID="btnClosePopupOK" CssClass="command" OnClick="ClosePopupOK_OnClick" runat="server"
                                            Text="<%$ Resources:LogiAdHoc, OKWithSpaces %>" UseSubmitBehavior="false" ValidationGroup="popup"/>
                                        <asp:Button ID="btnClosePopupCancel" CssClass="command" OnClick="ClosePopupCancel_OnClick"
                                            runat="server" Text="<%$ Resources:LogiAdHoc, Cancel %>" CausesValidation="False"/>
                                    </div>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </div>
                    </asp:Panel>
                    
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </form>
</body>
</html>
