<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="UserGroupList.aspx.vb" Inherits="LogiAdHoc.ahConfiguration_UserGroupList" Culture="auto" meta:resourcekey="PageResource1" UICulture="auto" %>

<%@ Register Assembly="System.Web.Extensions" Namespace="System.Web.UI" TagPrefix="asp" %>

<%@ Register TagPrefix="AdHoc" TagName="MainMenu" Src="~/ahControls/MainMenu.ascx" %>
<%@ Register TagPrefix="AdHoc" TagName="NavMenu" Src="~/ahControls/NavMenu.ascx" %>
<%@ Register TagPrefix="AdHoc" TagName="BreadCrumbTrail" Src="~/ahControls/BreadCrumbTrail.ascx" %>
<%@ Register TagPrefix="AdHoc" TagName="Search" Src="~/ahControls/Search.ascx" %>
<%@ Register TagPrefix="Adhoc" TagName="PagingControl" Src="~/ahControls/PagingControl.ascx"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Organizations</title>
    <link rel="shortcut icon" href="../ahImages/flav.ico" />
    <script language="JavaScript" type="text/javascript" src="../ahScripts/ahChange.js"></script> 
    <script type="text/javascript">
        function ConfirmDelete(frm, cnfMsg, errMsg, adminUGID, adminUGMsg)
        {
            var bVal = confirmMsg(frm, cnfMsg, errMsg);
            if (bVal) 
            {
                e = document.getElementById(adminUGID);
		        if (e!=null && e.checked)
		            bVal = confirm(adminUGMsg)
            }
            return bVal;
        }
        
        function ConfirmDeleteItem(cnfMsg, adminUGID, adminUGMsg)
        {
            var bVal = confirm(cnfMsg);
            if (bVal) 
            {
                e = document.getElementById(adminUGID);
		        if (e!=null && e.checked)
		            bVal = confirm(adminUGMsg)
            }
            return bVal;
        }
        
    </script>
</head>
<body id="bod">
    <AdHoc:MainMenu ID="menu" runat="server" />
    
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server" />
    <div id="submenu">
        <AdHoc:NavMenu ID="subnav" runat="server" />
    </div>
        <AdHoc:BreadCrumbTrail ID="bct" runat="server" Key="Organizations" ParentLevels="0" />

        <script type="text/javascript">
            Sys.WebForms.PageRequestManager.getInstance().add_endRequest(EndRequestHandler);
            function EndRequestHandler(sender, args) {
                $find('ahSavePopupBehavior').hide();
            }
        </script>
<table class="limiting"><tr><td>
        <table class="gridForm"><tr><td>
            <asp:UpdatePanel ID="UP1" runat="server">
                <ContentTemplate>
                <div id="data_main">
                    <div id="activities">
                        <table width="100%" cellpadding="0" cellspacing="0">
                        <tr width="100%">
                        <td align="left" valign="top">
                        <AdHoc:LogiButton ID="btnNewGroup" runat="server"
                            OnClick="NewGroup" Text="<%$ Resources:LogiAdHoc, AddWithSpaces %>"  />
                        <AdHoc:LogiButton ID="btnRemoveGroups" runat="server" 
                            Text="<%$ Resources:LogiAdHoc, Delete %>" />
                        </td>
                        <td align="right" valign="top">
                            <AdHoc:Search ID="srch" runat="server" Title="Find Organizations" meta:resourcekey="AdHocSearch" />        
                        </td>
                        </tr>
                        </table>
                    </div>
                    <asp:GridView ID="grdMain" runat="server" AllowPaging="True" AllowSorting="True"
                        AutoGenerateColumns="False" CssClass="grid" DataKeyNames="GroupID"
                        OnRowDataBound="OnItemDataBoundHandler" OnSorting="OnSortCommandHandler" meta:resourcekey="grdMainResource1" >
                        <HeaderStyle CssClass="gridheader"></HeaderStyle>
                        <PagerTemplate>
                            <AdHoc:PagingControl ID="pageCtrl" runat="server" OnGotoNextPage="ChangePageIndex"/>
                        </PagerTemplate>
                        <RowStyle CssClass="gridrow" />
                        <AlternatingRowStyle CssClass="gridalternaterow"></AlternatingRowStyle>
                        <Columns>
                            <asp:TemplateField>
                                <HeaderStyle Width="30px"></HeaderStyle>
                                <HeaderTemplate>
                                    <asp:Label ID="lblChkAll" runat="server" AssociatedControlID="CheckAll" Text="<%$ Resources:LogiAdHoc, SelectDeselectAll %>" CssClass="NoShow"></asp:Label>
                                    <asp:CheckBox runat="server" ToolTip="<%$ Resources:LogiAdHoc, SelectDeselectAll %>" ID="CheckAll">
                                    </asp:CheckBox>
                                </HeaderTemplate>
                                <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblChk" runat="server" AssociatedControlID="chk_Select" Text="<%$ Resources:LogiAdHoc, SelectDeselect %>" CssClass="NoShow"></asp:Label>
                                    <asp:CheckBox runat="server" ID="chk_Select" ToolTip="<%$ Resources:LogiAdHoc, SelectDeselect %>">
                                    </asp:CheckBox>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Organization" SortExpression="GroupName" meta:resourcekey="TemplateFieldResource2">
                                <ItemTemplate>
                                    <input type="hidden" id="GroupID" runat="server" />
                                    <asp:Label ID="GroupName" runat="server" meta:resourcekey="GroupNameResource1" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField DataField="Description" HeaderText="<%$ Resources:LogiAdHoc, Description %>" >
                            </asp:BoundField>
                            <asp:TemplateField HeaderText="<%$ Resources:LogiAdHoc, Actions %>" meta:resourcekey="TemplateFieldResource3">
                                <HeaderStyle Width="50px"></HeaderStyle>
                                <ItemTemplate>
                                    <asp:Image ID="imgActions" AlternateText="<%$ Resources:LogiAdHoc, Actions %>" runat="server" 
                                        ToolTip="<%$ Resources:LogiAdHoc, Actions %>" ImageUrl="~/ahImages/arrowStep.gif" SkinID="imgActions" />
                                    <asp:Panel ID="pnlActionsMenu" runat="server" CssClass="popupMenu" 
                                        HorizontalAlign="Left" Wrap="false" style="display:none;">
                                        <div id="divModify" runat="server" class="hoverMenuActionLink" >
                                            <asp:LinkButton ID="lnkModify" runat="server" OnCommand="ModifyGroup" 
                                                Text="Modify Organization" meta:resourcekey="ModifyResource1"></asp:LinkButton>
                                        </div>
                                        <div id="divParameters" runat="server" class="hoverMenuActionLink" >
                                            <asp:LinkButton ID="lnkParameters" runat="server" OnCommand="SessionParameters" 
                                                Text="Set Session Parameters" meta:resourcekey="ParametersResource1"></asp:LinkButton>
                                        </div>
                                        
                                        <div id="divDelete" runat="server" class="hoverMenuActionLink" visible="false">
                                            <asp:LinkButton ID="lnkDelete" runat="server" OnCommand="DeleteOrganization" Visible="false" 
                                                Text="Delete Organization" meta:resourcekey="DeleteResource1"></asp:LinkButton>
                                        </div>
                                        
                                        
 				                    </asp:Panel>
                                    <ajaxToolkit:HoverMenuExtender ID="hme2" runat="server"
                                        PopupControlID="pnlActionsMenu" PopupPosition="right" 
                                        TargetControlID="imgActions" PopDelay="25" />
                                </ItemTemplate><ItemStyle HorizontalAlign="Center"></ItemStyle>
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                </div>
                </ContentTemplate>
                <Triggers>
                    <asp:AsyncPostBackTrigger ControlID="srch" EventName="DoSearch" />
                </Triggers>
            </asp:UpdatePanel>
            
            <div id="divSaveAnimation">
                <asp:Button runat="server" ID="btnSaveAnimation" Style="display: none" />
                <ajaxToolkit:ModalPopupExtender runat="server" ID="ahSavePopup" BehaviorID="ahSavePopupBehavior"
                    TargetControlID="btnSaveAnimation" PopupControlID="pnlSaving" BackgroundCssClass="modalBackground"
                    DropShadow="False">
                </ajaxToolkit:ModalPopupExtender>
                <asp:Panel id="pnlSaving" CssClass="savePopup" runat="server" style="display:none;">
                    <table><tr><td>
                    <asp:Image ID="imgSave" runat="server" SkinID="imgSaving" EnableViewState="false" AlternateText="<%$ Resources:LogiAdHoc, SaveImageAltText %>" />
                    </td>
                    <td valign="middle">
                    <asp:Label ID="lblSaveText" CssClass="lblSavePopup" runat="server" EnableViewState="false" Text="<%$ Resources:LogiAdHoc, PleaseWait %>"/>
                    </td></tr></table>
                </asp:Panel>
            </div>
        </td></tr></table>
</td></tr></table>
<br /><br />
    </form>
</body>
</html>
