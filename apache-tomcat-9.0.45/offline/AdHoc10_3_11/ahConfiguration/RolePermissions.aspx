<%@ Page Language="vb" AutoEventWireup="false" Codebehind="RolePermissions.aspx.vb"
    Inherits="LogiAdHoc.ahConfiguration_RolePermissions" Culture="auto" meta:resourcekey="PageResource1" UICulture="auto" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register TagPrefix="AdHoc" TagName="MainMenu" Src="~/ahControls/MainMenu.ascx" %>
<%@ Register TagPrefix="AdHoc" TagName="NavMenu" Src="~/ahControls/NavMenu.ascx" %>
<%@ Register TagPrefix="AdHoc" TagName="BreadCrumbTrail" Src="~/ahControls/BreadCrumbTrail.ascx" %>
<%@ Register TagPrefix="ctrl" TagName="ColPermissions" Src="~/ahControls/ColPermissions2.ascx" %>
<%@ Register TagPrefix="AdHoc" TagName="PagingControl" Src="~/ahControls/PagingControl.ascx" %>
<%@ Register TagPrefix="AdHoc" TagName="Search" Src="~/ahControls/Search.ascx" %>
<%@ Register TagPrefix="AdHoc" TagName="DatabaseControl" Src="~/ahControls/DatabaseControl.ascx" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Role Permissions</title>
    <link rel="shortcut icon" href="../ahImages/flav.ico" />
    <script language="JavaScript" type="text/javascript" src="../ahScripts/ahChange.js"></script>

    <script type="text/javascript">
        window.onbeforeunload = AlertOnExit;
    </script>

</head>
<body id="bod">
    <AdHoc:MainMenu ID="menu" runat="server" />
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server" />
        <div id="submenu">
            <AdHoc:NavMenu ID="subnav" runat="server" />
        </div>
        <asp:UpdatePanel ID="UPBct" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
        <AdHoc:BreadCrumbTrail ID="bct" runat="server" Key="RolePermissions" />
            </ContentTemplate>
            <Triggers>
                <asp:AsyncPostBackTrigger ControlID="ddlParentID" EventName="SelectedIndexChanged" />
            </Triggers>
        </asp:UpdatePanel>

        <script type="text/javascript">
        Sys.WebForms.PageRequestManager.getInstance().add_endRequest(EndRequestHandler);
        function EndRequestHandler(sender, args) {
            $find('ahSavePopupBehavior').hide();
            RestorePopupPosition('ahModalPopupBehavior');
            RestorePopupPosition('ahModalPopup2Behavior');
        }
        Type.registerNamespace('AHScripts');
        AHScripts.PositionPanel = function() {}
        AHScripts.PositionPanel.prototype = {
            posPanel: function() {
                //document.documentElement.offsetHeight : height of visible area of the document
                //document.documentElement.scrollTop    : scroll position
                var pnl =document.getElementById("pnlColPerm");
                if (pnl != null) {
                    var centerX = (document.documentElement.offsetWidth) / 2; //.clientWidth / 2;
                    var centerY = (document.documentElement.offsetHeight) / 2;
                    var l = parseInt(centerX - 250 + document.documentElement.scrollLeft) ;
                    var t = parseInt(centerY - 250 + document.documentElement.scrollTop);
                    if (l < document.documentElement.scrollLeft) l = document.documentElement.scrollLeft;
                    if (t < document.documentElement.scrollTop) t = document.documentElement.scrollTop;
                    
                    pnl.style.top = t;
                    pnl.style.left = l;
                }
            }
        }
        AHScripts.PositionPanel.registerClass('AHScripts.PositionPanel');
        
        var panelUpdated = new AHScripts.PositionPanel();
        var postbackElement;
        Sys.WebForms.PageRequestManager.getInstance().add_beginRequest(beginRequest);
        Sys.WebForms.PageRequestManager.getInstance().add_pageLoaded(pageLoaded);
        
        function beginRequest(sender, args) {
            postbackElement = args.get_postBackElement();
            SavePopupPosition('ahModalPopupBehavior');
            SavePopupPosition('ahModalPopup2Behavior');
        }
        function pageLoaded(sender, args) {
            var pnl =document.getElementById("pnlColPerm");
            if (pnl != null) {
                panelUpdated.posPanel();
            }
        }
        </script>

<table class="limiting"><tr><td>
        <table class="gridForm"><tr><td>
            <input type="hidden" id="ahDirty" name="ahDirty" runat="server" />
            <input type="hidden" id="ahParentID" name="ahParentID" runat="server" />
            <table id="tbParent" runat="server">
                <tr>
                    <td width="125px">
                        <asp:Localize ID="Localize1" runat="server" Text="Selected Role:" meta:resourcekey="Localize1Resource1"></asp:Localize></td>
                    <td>
                        <asp:DropDownList ID="ddlParentID" AutoPostBack="True" runat="server" />
                    </td>
                </tr>
            </table>
            <br />
            <asp:UpdatePanel ID="UP1" UpdateMode="Conditional" runat="Server">
                <ContentTemplate>
                <Adhoc:DatabaseControl ID="ucDatabaseControl" runat="server" ShowAllDatabasesOption="False" />
                <br /><br />
                
                <div id="data_main">
                    <div id="activities">
                        <table width="100%" cellpadding="0" cellspacing="0">
                        <tr width="100%">
                        <td align="left" valign="top">
                        <AdHoc:LogiButton ID="btnSetFull" OnClick="SetFull_OnClick" ToolTip="Click to set access level for all selected objects to Full."
                            Text="Set To Full" runat="server" meta:resourcekey="btnSetFullResource1" />
                        <AdHoc:LogiButton ID="btnSetNone" OnClick="SetNone_OnClick" ToolTip="Click to set access level for all selected objects to None."
                            Text="Set To None" runat="server" meta:resourcekey="btnSetNoneResource1" />
                        <AdHoc:LogiButton ID="btnDuplicate" OnClick="Duplicate_OnClick" ToolTip="Click to copy access rights for selected objects to other roles."
                            Text="Copy Object Access Rights" runat="server" meta:resourcekey="btnDuplicateResource1" />
                        </td>
                        <td align="right" valign="top">
                        <AdHoc:Search ID="srch" runat="server" Title="Find Objects" meta:resourcekey="AdHocSearch" />
                        </td>
                        </tr>
                        </table>
                    </div>
                    <asp:GridView ID="grdMain" runat="server" AutoGenerateColumns="False" AllowPaging="True"
                        AllowSorting="True" CssClass="grid" DataKeyNames="ObjectID" OnRowDataBound="OnItemDataBoundHandler"
                        OnSorting="OnSortCommandHandler">
                        <HeaderStyle CssClass="gridheader"></HeaderStyle>
                        <PagerTemplate>
                            <AdHoc:PagingControl ID="pageCtrl" runat="server" OnGotoNextPage="ChangePageIndex" />
                        </PagerTemplate>
                        <PagerStyle HorizontalAlign="Center" />
                        <RowStyle CssClass="gridrow" />
                        <AlternatingRowStyle CssClass="gridalternaterow"></AlternatingRowStyle>
                        <Columns>
                            <asp:TemplateField>
                                <HeaderStyle Width="30px"></HeaderStyle>
                                <HeaderTemplate>
                                    <asp:Label ID="lblChkAll" runat="server" AssociatedControlID="CheckAll" Text="<%$ Resources:LogiAdHoc, SelectDeselectAll %>" CssClass="NoShow"></asp:Label>
                                    <asp:CheckBox ID="CheckAll" runat="server" ToolTip="<%$ Resources:LogiAdHoc, SelectDeselectAll %>" />
                                </HeaderTemplate>
                                <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblChk" runat="server" AssociatedControlID="chk_Select" Text="<%$ Resources:LogiAdHoc, SelectDeselect %>" CssClass="NoShow"></asp:Label>
                                    <asp:CheckBox ID="chk_Select" runat="server" ToolTip="<%$ Resources:LogiAdHoc, SelectDeselect %>" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Data Object" meta:resourcekey="TemplateFieldResource2">
                                <ItemTemplate>
                                    <input type="hidden" id="ihObjectID" runat="server" />
                                    <input type="hidden" id="ihObjectLabel" runat="server" />
                                    <asp:Label ID="Description" runat="server" /><asp:Label
                                        ID="ObjectName" runat="server" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Type" meta:resourcekey="TemplateFieldResource3">
                                <ItemTemplate>
                                    <asp:Label ID="Type" runat="server" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Access" meta:resourcekey="TemplateFieldResource4">
                                <ItemTemplate>
                                    <asp:Label ID="AccessType" runat="server" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="<%$ Resources:LogiAdHoc, Actions %>">
                                <HeaderStyle Width="50px" />
                                <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                <ItemTemplate>
                                    <asp:ImageButton ID="imgModify" SkinID="imgSingleAction" AlternateText="Modify Column Access Rights" 
                                        ToolTip="Modify Column Access Rights" runat="server" OnCommand="ModifyColumnPermission" 
                                        CausesValidation="false" meta:resourcekey="imgModifyResource1" />
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                </div>
                    
                    <asp:Button runat="server" ID="Button1" Style="display: none" />
                    <ajaxToolkit:ModalPopupExtender runat="server" ID="ahModalPopup" BehaviorID="ahModalPopupBehavior"
                        TargetControlID="Button1" PopupControlID="ahPopup" BackgroundCssClass="modalBackground"
                        DropShadow="false" PopupDragHandleControlID="pnlDragHandle" RepositionMode="None">
                    </ajaxToolkit:ModalPopupExtender>
                    
                    <asp:Panel runat="server" CssClass="modalPopup" ID="ahPopup" Style="display: none; width: 600;">
                        <asp:Panel ID="pnlDragHandle" runat="server" Style="cursor: hand;">
                        <div class="modalPopupHandle">
                            <table cellpadding="0" cellspacing="0" style="width: 100%"><tr><td>
                                <asp:Localize ID="PopupHeader" runat="server"></asp:Localize>
                                </td>
                                <td style="width: 20px;">
                                    <asp:ImageButton ID="imgClosePopup" runat="server" 
                                        OnClick="imgClosePopup_Click" CausesValidation="false"
                                        SkinID="imgbClose" ImageUrl="../ahImages/remove.gif" 
                                        AlternateText="<%$ Resources:LogiAdHoc, CloseWindow %>" />
                            </td></tr></table>
                        </div>
                        </asp:Panel>
                        <div class="modalDiv">
                        <asp:UpdatePanel ID="upPopup" runat="server">
                            <ContentTemplate>
                                <ctrl:ColPermissions ID="ColumnPermissions" runat="server" />
                            </ContentTemplate>
                        </asp:UpdatePanel>
                        </div>
                    </asp:Panel>
                    
                    <ul class="validation_error" id="ErrorList" runat="server">
                        <li>
                            <asp:Label ID="lblValidationMessage" runat="server"></asp:Label></li>
                    </ul>
                </ContentTemplate>
                <Triggers>
                    <asp:AsyncPostBackTrigger ControlID="ddlParentID" EventName="SelectedIndexChanged" />
                    <asp:AsyncPostBackTrigger ControlID="srch" EventName="DoSearch" />
                    <asp:PostBackTrigger ControlID="btnCancel" />
                </Triggers>
            </asp:UpdatePanel>
            <br />
            <AdHoc:LogiButton ID="btnCancel" runat="server" OnClick="Cancel_OnClick"
        ToolTip="Click to return to the previous page." Text="Back to Roles" meta:resourcekey="btnCancelResource1" />

            <asp:Button runat="server" ID="Button2" Style="display: none" />
            <ajaxToolkit:ModalPopupExtender runat="server" ID="ahModalPopup2" BehaviorID="ahModalPopup2Behavior"
                TargetControlID="Button2" PopupControlID="ahPopup2" BackgroundCssClass="modalBackground"
                DropShadow="false" PopupDragHandleControlID="pnlDragHandle2" RepositionMode="None">
            </ajaxToolkit:ModalPopupExtender>
            <asp:Panel runat="server" CssClass="modalPopup" ID="ahPopup2" Style="display: none; width: 506;">
                <asp:Panel ID="pnlDragHandle2" runat="server" Style="cursor: hand;">
                <div class="modalPopupHandle" style="width: 500px;">
                    <table cellpadding="0" cellspacing="0" style="width: 100%"><tr><td>
                        <asp:Localize ID="Localize5" Text="Copy Access Rights" meta:resourcekey="Localize5Resource1" runat="server"></asp:Localize>
                        </td>
                        <td style="width: 20px;">
                            <asp:ImageButton ID="imgClosePopup2" runat="server" 
                                OnClick="imgClosePopup2_Click" CausesValidation="false"
                                SkinID="imgbClose" ImageUrl="../ahImages/remove.gif" 
                                AlternateText="<%$ Resources:LogiAdHoc, CloseWindow %>" />
                    </td></tr></table>
                </div>
                </asp:Panel>
                <div class="modalDiv">
                <asp:UpdatePanel ID="UPRoles" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <input type="hidden" id="SelectedObject" runat="server" />
                        <asp:Localize ID="Localize2" runat="server" Text="Select Role(s) to copy access rights to" meta:resourcekey="Localize2Resource1"></asp:Localize>
                        <br />
                        <select id="RoleList" multiple style="width: 250px" size="10" runat="server" />
                        <br />
                        <br />
                        <table><tr><td>
                            <AdHoc:LogiButton ID="btnPnlRolesSave" OnClick="DuplicateRoles_OnClick"
                                Text="<%$ Resources:LogiAdHoc, OKWithSpaces %>" runat="server" />
                            <AdHoc:LogiButton ID="btnPnlRolesCancel" OnClick="CancelRoles_OnClick"
                                Text="<%$ Resources:LogiAdHoc, Cancel %>" runat="server" />
                        </td></tr></table>
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="btnDuplicate" />
                    </Triggers>
                </asp:UpdatePanel>
                </div>
            </asp:Panel>
            
            <div id="divSaveAnimation">
                <asp:Button runat="server" ID="btnSaveAnimation" Style="display: none" />
                <ajaxToolkit:ModalPopupExtender runat="server" ID="ahSavePopup" BehaviorID="ahSavePopupBehavior"
                    TargetControlID="btnSaveAnimation" PopupControlID="pnlSaving" BackgroundCssClass="modalBackground"
                    DropShadow="False">
                </ajaxToolkit:ModalPopupExtender>
                <asp:Panel id="pnlSaving" CssClass="savePopup" runat="server" style="display:none;">
                    <table><tr><td>
                    <asp:Image ID="imgSave" runat="server" SkinID="imgSaving" EnableViewState="false" AlternateText="<%$ Resources:LogiAdHoc, SaveImageAltText %>" />
                    </td>
                    <td valign="middle">
                    <asp:Label ID="lblSaveText" CssClass="lblSavePopup" runat="server" EnableViewState="false" Text="<%$ Resources:LogiAdHoc, PleaseWait %>"/>
                    </td></tr></table>
                </asp:Panel>
            </div>
        </td></tr></table>
</td></tr></table>

    </form>
</body>
</html>
