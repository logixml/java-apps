<%@ Page Language="vb" AutoEventWireup="false" Codebehind="DataFormats.aspx.vb" Inherits="LogiAdHoc.ahConfiguration_DataFormats" Culture="auto" meta:resourcekey="PageResource1" UICulture="auto" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register TagPrefix="AdHoc" TagName="MainMenu" Src="~/ahControls/MainMenu.ascx" %>
<%@ Register TagPrefix="AdHoc" TagName="NavMenu" Src="~/ahControls/NavMenu.ascx" %>
<%@ Register TagPrefix="AdHoc" TagName="BreadCrumbTrail" Src="~/ahControls/BreadCrumbTrail.ascx" %>
<%@ Register TagPrefix="AdHoc" TagName="Search" Src="~/ahControls/Search.ascx" %>
<%@ Register TagPrefix="AdHoc" TagName="PagingControl" Src="../ahControls/PagingControl.ascx" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Data Formats</title>
    <link rel="shortcut icon" href="../ahImages/flav.ico" />
    <script language="JavaScript" type="text/javascript" src="../ahScripts/ahChange.js"></script>

    <script src="../rdTemplate/yui/build/yahoo-dom-event/yahoo-dom-event.js" type="text/javascript"></script>
    <script src="../rdTemplate/yui/build/animation/animation-min.js" type="text/javascript"></script>
    <script src="../rdTemplate/yui/build/dragdrop/dragdrop-min.js" type="text/javascript"></script>
    <script language="JavaScript" type="text/javascript" src="../ahScripts/ahReorderGrid.js"></script>
    
    <%--<script type="text/javascript">
    YAHOO.util.Event.onContentReady("grdMain", function () {
        var ih = document.getElementById('ihDisableReorder');
        if (ih.value == "1") {
            initializeTableDragAndDrop('grdMain', false);
        } else {
            initializeTableDragAndDrop('grdMain', true);
        }
    });
    </script>--%>
    
    <script type="text/javascript">
        window.onbeforeunload = AlertOnExit;
    </script>

   <%-- <script type="text/javascript">
    var SelectRowIndex = -1;
	function SelectRow(e)
	{
	    if(!e) e = window.event;//Added this for IE
	    
		var obj = e.srcElement;
		if(!obj) obj = e.target;//Added this for Firefox
		if (obj && obj.tagName=="TD") //this a table cell
		{
		    //get a pointer to the tablerow
		    var row = obj.parentNode;
		    
		    if (!e.ctrlKey && !e.shiftKey)
		    {
		        var table = row.parentNode;
		        for (var i=1;i<table.childNodes.length;i++) {
		            if (table.childNodes[i].tagName == "TR")//Added this for Firefox
		            {
//		                var chk = table.childNodes[i].cells[0].firstChild;
//		                if (chk.checked)
//		                {
//		                    chk.checked = false
                            if(i%2 == 1)
		                        table.childNodes[i].className="";
		                    else
		                        table.childNodes[i].className="gridalternaterow";
//		                }
		            }
		        }
                var oRows = document.getElementById('grdMain').getElementsByTagName('tr');
                var l = oRows.length;
                var chk;
                var s;
                for (i=2; i<=l; i++) {
	                var o = pack(i.toString(10),2);
		            s = "grdMain_ctl" + o + "_chk_Select";
		            chk = document.getElementById(s);
		            if (chk!=null) chk.checked = false;
	            }
		    }
		    
		    if (e.shiftKey && SelectRowIndex >= 0)
		    {
		        if (SelectRowIndex > row.rowIndex)
		        {
		            var table = row.parentNode;
		            for (var i=row.rowIndex;i<SelectRowIndex;i++) {
		                var idx = i+1;
		                var o = pack(idx.toString(10),2);
		                var s = "grdMain_ctl" + o + "_chk_Select";
		                //var chk = table.childNodes[i].cells[0].firstChild;
		                var chk = document.getElementById(s);
		                if(!chk.disabled)
		                {
		                    chk.checked = true;
		                    table.childNodes[i].className="SelectedRow";
		                }
		            }
		        }
		        else
		        {
		            var table = row.parentNode;
		            for (var i=row.rowIndex;i>SelectRowIndex;i--) {
		                var idx = i+1;
		                var o = pack(idx.toString(10),2);
		                var s = "grdMain_ctl" + o + "_chk_Select";
		                var chk = document.getElementById(s);
		                //var chk = table.childNodes[i].cells[0].firstChild;
		                if(!chk.disabled)
		                {
		                    chk.checked = true;
		                    table.childNodes[i].className="SelectedRow";
		                }
		            }
		        }
		    }
		    else
		    {
		        var idx = row.rowIndex+1;
		        var o = pack(idx.toString(10),2);
                var s = "grdMain_ctl" + o + "_chk_Select";
                var chk = document.getElementById(s);
		        //var chk = row.cells[0].firstChild;
		        if(!chk.disabled)
		        {
		            chk.checked = true;
		            if (chk.checked)
		               row.className="SelectedRow";
		            else
		            {
		                if(idx%2 == 0)
	                        row.className="";
	                    else
	                        row.className="gridalternaterow";
		            }
		        }
		    }
		    SelectRowIndex = row.rowIndex
		}
	}
	function MouseOver(select, e)
	{
	    if(!e) e = window.event;//Added this for IE
	    
	    var obj = e.srcElement;
	    if(!obj) obj = e.target;//Added this for Firefox
		if (obj && obj.tagName=="TD") //this a table cell
		{
		    //get a pointer to the tablerow
		    var row = obj.parentNode;
		    var idx = row.rowIndex+1;
		    var o = pack(idx.toString(10),2);
            var s = "grdMain_ctl" + o + "_chk_Select";
            var chk = document.getElementById(s);
		    //var chk = row.cells[0].firstChild;
		    if (chk && !chk.disabled)
		    {
		        if (select && !chk.checked)
		        {
		            row.className="SelectedRow";
		        }
		        else
		        {
		            if (chk.checked)
		            {
		               row.className="SelectedRow";
		            }
		            else
		            {
		                if(idx%2 == 0)
	                        row.className="";
	                    else
	                        row.className="gridalternaterow";
		            }
		        }
		     }
		}
	}
	function SelectRow_OnClick(chkVal, row)
	{
	    if (chkVal)
	        row.className="SelectedRow";
	    else
	    {
	        var idx = row.rowIndex+1;
	        if(idx%2 == 0)
                row.className="";
            else
                row.className="gridalternaterow";
	    }
	}
	function SelectAll_OnClick(chkVal, idVal, row)
	{
	    var oRows = document.getElementById('grdMain').getElementsByTagName('tr');
        var l = oRows.length;
        //var l = grdMain.rows.length;
        var e;
        var s;
        if (idVal.indexOf ('CheckAll') != -1) {
	        var bSet; 
	        // Check if main checkbox is checked, then select or deselect datagrid checkboxes
	        if (chkVal == true) {
		        bSet = true;
	        } else {
		        bSet = false;
	        }
	        // Loop through all elements
	        for (i=2; i<=l; i++) {
	            var o = pack(i.toString(10),2);
		        s = "grdMain_ctl" + o + "_chk_Select";
		        e = document.getElementById(s);
		        if (e!=null) e.checked = bSet;
	        }
	        var table = row.parentNode;
	        if (table.tagName == "TR") 
	            table = table.parentNode;
	        for (var i=1;i<table.childNodes.length;i++) {
	            if (table.childNodes[i].tagName == "TR")//Added this for Firefox
	            {
	                if (bSet)
	                    table.childNodes[i].className="SelectedRow";
	                else
	                {
	                    if(i%2 == 0)
	                        table.childNodes[i].className="";
	                    else
	                        table.childNodes[i].className="gridalternaterow";
	                }
	            }
	        }
        }
	}
	function SelectStart(e)
	{
	    if(!e) e = window.event;//Added this for IE
	    var obj = e.srcElement;
		if(!obj) obj = e.target;//Added this for Firefox
		if (obj && obj.tagName=="TD") //this a table cell
		    return false;
    }
    function MouseDown(e)
    {
          if(!e) return false;
          if(e.shiftKey || e.ctrlKey) return false;
    }
    </script>--%>
</head>
<body id="bod">
    <AdHoc:MainMenu ID="menu" runat="server" />
    <form id="form1" runat="server">
    <div id="submenu">
        <AdHoc:NavMenu ID="subnav" runat="server" />
    </div>
        <AdHoc:BreadCrumbTrail ID="bct" runat="server" ParentLevels="0" Key="DataFormats" />
        <asp:ScriptManager ID="ScriptManager1" runat="server" />
        
        <script type="text/javascript">
            Sys.WebForms.PageRequestManager.getInstance().add_beginRequest(BeginRequestHandler);
            Sys.WebForms.PageRequestManager.getInstance().add_endRequest(EndRequestHandler);
            function EndRequestHandler(sender, args) {
                $find('ahSavePopupBehavior').hide();
                RestorePopupPosition('ahModalPopupBehavior');
                //initializeTableDragAndDrop('grdMain');
                document.body.style.cursor = '';
                
//                var ih = document.getElementById('ihDisableReorder');
//                if (ih.value == "1") {
//                    initializeTableDragAndDrop('grdMain', false);
//                } else {
//                    initializeTableDragAndDrop('grdMain', true);
//                }
            }
            function BeginRequestHandler(sender, args) {
                SavePopupPosition('ahModalPopupBehavior');
                document.body.style.cursor = 'wait';
            }
        </script>
        
<table class="limiting"><tr><td>
        <table class="gridForm"><tr><td>
            <input type="hidden" id="ahDirty" name="ahDirty" runat="server" />
            <asp:UpdatePanel ID="UP1" UpdateMode="Conditional" runat="server" RenderMode="Inline">
                <ContentTemplate>
                <div id="data_main">
                    <div id="activities">
                        <table width="100%" cellpadding="0" cellspacing="0">
                        <tr width="100%">
                        <td align="left" valign="top">
                    <AdHoc:LogiButton ID="btnAddRow" OnClick="AddRow_OnClick" Text="<%$ Resources:LogiAdHoc, AddWithSpaces %>"
                        runat="server" CausesValidation="false" />
                    <AdHoc:LogiButton ID="btnRemoveRow2" OnClick="RemoveRow_OnClick" Text="<%$ Resources:LogiAdHoc, Delete %>"
                        runat="server" CausesValidation="false" />
                        </td>
                        <td align="right" valign="top">
                            <AdHoc:Search ID="srch" runat="server" Title="Find Data Formats" meta:resourcekey="AdHocSearch" />
                        </td>
                        </tr>
                        </table>
                    </div>
                    <div id="divDisabledReorder" runat="server">
                        <p class="info">
                            <asp:Localize ID="Localize14" runat="server" meta:resourcekey="LiteralResource14" Text="Reordering has been disabled due to some rows being hidden as a result of search." />
                        </p>
                    </div>
                    <%--<table onselectstart="javascript: return SelectStart(event);" onmousedown="javascript: return MouseDown(event);">--%>
                    <table>
                        <tr>
                            <td>
                                <input type="hidden" id="ihDisableReorder" runat="server" value ="0" />
                                <asp:Button ID="btnFakeReorderRows" runat="server" CssClass="NoShow" OnClick="SaveRowOrder" />
                                <AdHoc:DDGridView ID="grdMain" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                                    CssClass="grid" DataKeyNames="FormatID" OnRowDataBound="OnItemDataBoundHandler"
                                     DDDivID="divDragHandle" PostbackButtonID="btnFakeReorderRows" EnableSortingAndPagingCallbacks="false">
                                    <HeaderStyle CssClass="gridheader"></HeaderStyle>
                                    <PagerTemplate>
                                        <AdHoc:PagingControl ID="pageCtrl" runat="server" OnGotoNextPage="ChangePageIndex" />
                                    </PagerTemplate>
                                    <PagerStyle HorizontalAlign="Center" />
                                    <FooterStyle CssClass="gridfooter" />
                                    <RowStyle CssClass="gridrownocolor" />
                                    <AlternatingRowStyle CssClass="gridalternaterownocolor"></AlternatingRowStyle>
                                    <Columns>
                                        <asp:TemplateField>
                                            <HeaderTemplate>
                                                <asp:Label ID="lblChkAll" runat="server" AssociatedControlID="CheckAll" Text="<%$ Resources:LogiAdHoc, SelectDeselectAll %>" CssClass="NoShow"></asp:Label>
                                                <asp:CheckBox ID="CheckAll" runat="server" ToolTip="<%$ Resources:LogiAdHoc, SelectDeselectAll %>"
                                                    meta:resourcekey="CheckAllResource1" />
                                            </HeaderTemplate>
                                            <ItemStyle HorizontalAlign="Center" />
                                            <ItemTemplate>
                                                <input type="hidden" id="RowOrder" runat="server" />
                                                <table cellpadding="0" cellspacing="0">
                                                    <tr>
                                                        <td>
                                                            <div id="divDragHandle" class="dragHandle">
                                                                <%--<asp:Image ID="imgDragHandle" runat="server" ImageUrl="../ahImages/bg-menu-main.png" Width="10px" Height="15px" />--%>
                                                            </div>
                                                        </td>
                                                        <td>
                                                            <asp:Label ID="lblChk" runat="server" AssociatedControlID="chk_Select" Text="<%$ Resources:LogiAdHoc, SelectDeselect %>" CssClass="NoShow"></asp:Label>
                                                            <asp:CheckBox ID="chk_Select" runat="server" ToolTip="<%$ Resources:LogiAdHoc, SelectDeselect %>"/>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Format Name" meta:resourcekey="BoundFieldResource1">
                                            <ItemTemplate>
                                                <asp:Label ID="lblFormatName" runat="server" />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:BoundField DataField="Format" HeaderText="Format" meta:resourcekey="BoundFieldResource2">
                                            <HeaderStyle Width="200px" />
                                        </asp:BoundField>
                                        <asp:TemplateField HeaderText="Applies To" meta:resourcekey="BoundFieldResource3">
                                            <ItemTemplate>
                                                <input type="hidden" id="FormatID" runat="server" />
                                                <asp:Label ID="lblAppliesTo" runat="server" />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:BoundField DataField="Explanation" HeaderText="Explanation" meta:resourcekey="BoundFieldResource4">
                                            <HeaderStyle Width="200px" />
                                        </asp:BoundField>
                                        <asp:TemplateField HeaderText="<%$ Resources:LogiAdHoc, Actions %>" meta:resourcekey="TemplateFieldResource2">
                                            <HeaderStyle Width="50px" />
                                            <ItemStyle HorizontalAlign="Center" />
                                            <ItemTemplate>
                                                <asp:Image ID="imgActions" AlternateText="<%$ Resources:LogiAdHoc, Actions %>" runat="server" 
                                                    ToolTip="<%$ Resources:LogiAdHoc, Actions %>" ImageUrl="~/ahImages/arrowStep.gif" SkinID="imgActions" />
                                                <asp:Panel ID="pnlActionsMenu" runat="server" CssClass="popupMenu" 
                                                    HorizontalAlign="Left" Wrap="false" style="display:none;">
                                                    <div id="divModify" runat="server" class="hoverMenuActionLink" >
                                                        <asp:LinkButton ID="lnkModify" runat="server" OnCommand="EditItem"  Text="Modify Data Format" 
                                                            CausesValidation="False" meta:resourcekey="EditItemResource1"></asp:LinkButton>
                                                    </div>
                                                    <div id="divDependencies" runat="server" class="hoverMenuActionLink" >
                                                        <asp:LinkButton ID="lnkDependencies" runat="server" OnCommand="Dependency" Text="<%$ Resources:LogiAdHoc, ViewDependencyInfo %>"
                                                            CausesValidation="false"></asp:LinkButton>
                                                    </div>
                                                    
                                                    <div id="divDelete" runat="server" class="hoverMenuActionLink" visible="false">
                                                        <asp:LinkButton ID="lnkDelete" runat="server" OnCommand="DeleteItem" Visible="false" 
                                                            Text="Delete Data Format" meta:resourcekey="DeleteResource1"></asp:LinkButton>
                                                    </div>
                                                    
 				                                </asp:Panel>
                                                <ajaxToolkit:HoverMenuExtender ID="hme2" runat="server"
                                                    PopupControlID="pnlActionsMenu" PopupPosition="right" 
                                                    TargetControlID="imgActions" PopDelay="25" />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                </AdHoc:DDGridView>
                
                            </td>
                            <td width="30">
                                <div id="divColumnMove" runat="server">
                                    <asp:ImageButton ID="imgFormatMoveup" runat="server" AlternateText="Move Formats Up"
                                        CausesValidation="False" CssClass="mslbutton" ImageUrl="../ahImages/arrowUp.gif"
                                        meta:resourcekey="imgMoveupResource1" OnCommand="FormatMoveUp" ToolTip="Move Formats Up" />
                                    <asp:ImageButton ID="imgFormatMovedown" runat="server" AlternateText="Move Formats Down"
                                        CausesValidation="False" CssClass="mslbutton" ImageUrl="../ahImages/arrowDown.gif"
                                        meta:resourcekey="imgMovedownResource1" OnCommand="FormatMoveDown" ToolTip="Move Formats Down" />
                                </div>
                            </td>
                        </tr>
                    </table>
                </div>
                    
                    <asp:Button runat="server" ID="Button1" Style="display: none" />
                    <ajaxToolkit:ModalPopupExtender runat="server" ID="ahModalPopup" BehaviorID="ahModalPopupBehavior"
                        TargetControlID="Button1" PopupControlID="ahPopup" BackgroundCssClass="modalBackground"
                        DropShadow="false" PopupDragHandleControlID="pnlDragHandle" RepositionMode="None">
                    </ajaxToolkit:ModalPopupExtender>
                    
                    <asp:Panel runat="server" CssClass="modalPopup" ID="ahPopup" Style="display: none; width: 400;">
                        <asp:Panel ID="pnlDragHandle" runat="server" Style="cursor: hand;">
                        <div class="modalPopupHandle">
                            <table cellpadding="0" cellspacing="0" style="width: 100%"><tr><td>
                                    <asp:Localize ID="PopupHeader" runat="server" meta:resourcekey="LiteralResource1" Text="Data Format Details"></asp:Localize>
                                </td>
                                <td style="width: 20px;">
                                    <asp:ImageButton ID="imgClosePopup" runat="server" 
                                        OnClick="imgClosePopup_Click" CausesValidation="false"
                                        SkinID="imgbClose" ImageUrl="../ahImages/remove.gif" 
                                        AlternateText="<%$ Resources:LogiAdHoc, CloseWindow %>" />
                            </td></tr></table>
                        </div>
                        </asp:Panel>
                        <div class="modalDiv">
                        <asp:UpdatePanel ID="upPopup" runat="server">
                            <ContentTemplate>
                                <%--<asp:Panel ID="ParamDetails" CssClass="detailpanel" runat="server" meta:resourcekey="ParamDetailsResource1">--%>
                                <input type="hidden" id="NewFormatID" runat="server" />
                                <table class="tbTB">
                                    <tr>
                                        <td>
                                            <asp:Localize ID="Localize2" runat="server" meta:resourcekey="LiteralResource2" Text="Format Name:"></asp:Localize>
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtFormatName" MaxLength="50" runat="server" />
                                            <asp:RequiredFieldValidator ID="rtvFormatName" runat="server" ErrorMessage="Format Name is required."
                                                ControlToValidate="txtFormatName" ValidationGroup="DataFormat" meta:resourcekey="rtvFormatNameResource1">*</asp:RequiredFieldValidator>
                                            <asp:CustomValidator ID="cvValidName" runat="server" ErrorMessage="This name already exists."
                                                ControlToValidate="txtFormatName" ValidationGroup="DataFormat" OnServerValidate="IsNameValid" meta:resourcekey="cvValidNameResource1">*</asp:CustomValidator>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:Localize ID="Localize3" runat="server" meta:resourcekey="LiteralResource3" Text="Format:"></asp:Localize>
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtFormat" MaxLength="255" runat="server" />
                                            <asp:RequiredFieldValidator ID="rfvFormat" runat="server" ErrorMessage="Format definition is required."
                                                ControlToValidate="txtFormat" ValidationGroup="DataFormat" meta:resourcekey="rtvFormatResource1">*</asp:RequiredFieldValidator>
                                            <asp:CustomValidator ID="cvValidFormat" runat="server" ControlToValidate="txtFormat"
                                                ErrorMessage="This Format already exists." meta:resourcekey="cvValidFormatNameResource1"
                                                OnServerValidate="IsFormatValid" ValidationGroup="DataFormat">*</asp:CustomValidator>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:Localize ID="Localize4" runat="server" meta:resourcekey="LiteralResource4" Text="Applies to:"></asp:Localize>
                                        </td>
                                        <td>
                                            <table>
                                                <tr>
                                                    <td>
                                                        <asp:CheckBox ID="chkNumeric" runat="server" Text="<%$ Resources:LogiAdHoc, DataFormat_Numeric %>" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <asp:CheckBox ID="chkDateTime" runat="server" Text="<%$ Resources:LogiAdHoc, DataFormat_DateTime %>" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <asp:CheckBox ID="chkBoolean" runat="server" Text="<%$ Resources:LogiAdHoc, DataFormat_Boolean %>" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <asp:CheckBox ID="chkText" runat="server" Text="<%$ Resources:LogiAdHoc, DataFormat_Text %>" />
                                                    </td>
                                                </tr>
                                            </table>
                                            <asp:CustomValidator ID="cvAppliesTo" runat="server" ErrorMessage="This format must be applied to at least one data type."
                                                ControlToValidate="txtFormat" ValidationGroup="DataFormat" OnServerValidate="IsAppliesToValid" meta:resourcekey="cvAppliesToResource1">*</asp:CustomValidator>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:Localize ID="Localize6" runat="server" meta:resourcekey="LiteralResource12" Text="Is Available:"></asp:Localize>
                                        </td>
                                        <td>
                                            <asp:CheckBox ID="chkIsAvailable" runat="server" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:Localize ID="Localize5" runat="server" meta:resourcekey="LiteralResource5" Text="Explanation:"></asp:Localize>
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtExplanation" TextMode="MultiLine" Columns="45" Rows="3" runat="server" />
                                            <asp:CustomValidator ID="cvMaxLength" runat="server" ErrorMessage="<%$ Resources:Errors, Err_Explanation255 %>"
                                                ControlToValidate="txtExplanation" ValidationGroup="DataFormat" OnServerValidate="ReachedMaxLength">*</asp:CustomValidator>
                                        </td>
                                    </tr>
                                    <%--<tr>
                                        <td>
                                            <asp:Localize ID="Localize6" runat="server" meta:resourcekey="LiteralResource6" Text="Example before:"></asp:Localize>
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtExampleBefore" MaxLength="50" runat="server" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:Localize ID="Localize7" runat="server" meta:resourcekey="LiteralResource7" Text="Example after:"></asp:Localize>
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtExampleAfter" MaxLength="50" runat="server" />
                                        </td>
                                    </tr>--%>
                                    <%--<tr>
                                        <td>
                                            <asp:Localize ID="Localize8" runat="server" meta:resourcekey="LiteralResource8" Text="Available:"></asp:Localize>
                                        </td>
                                        <td>
                                            <asp:CheckBox ID="chkIsAvailable" runat="server" />
                                        </td>
                                    </tr>--%>
                                </table>
                                <br />
                                <!-- Buttons -->
                                <table>
                                    <tr>
                                        <td colspan="2">
                                            <AdHoc:LogiButton ID="btnSaveFormat" OnClick="SaveFormat_OnClick" runat="server"
                                                Text="<%$ Resources:LogiAdHoc, OKWithSpaces %>" ValidationGroup="DataFormat" UseSubmitBehavior="false" />
                                            <AdHoc:LogiButton ID="btnCancelFormat" OnClick="CancelFormat_OnClick"
                                                runat="server" Text="<%$ Resources:LogiAdHoc, Cancel %>" CausesValidation="False" />
                                            <asp:ValidationSummary ID="vsummary" ValidationGroup="DataFormat" runat="server"/>
                                        </td>
                                    </tr>
                                </table>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                        </div>
                    </asp:Panel>
                    &nbsp;
                </ContentTemplate>
            </asp:UpdatePanel>
            
            <div id="divSaveAnimation">
                <asp:Button runat="server" ID="btnSaveAnimation" Style="display: none" />
                <ajaxToolkit:ModalPopupExtender runat="server" ID="ahSavePopup" BehaviorID="ahSavePopupBehavior"
                    TargetControlID="btnSaveAnimation" PopupControlID="pnlSaving" BackgroundCssClass="modalBackground"
                    DropShadow="False">
                </ajaxToolkit:ModalPopupExtender>
                <asp:Panel id="pnlSaving" CssClass="savePopup" runat="server" style="display:none;">
                    <table><tr><td>
                    <asp:Image ID="imgSave" runat="server" SkinID="imgSaving" EnableViewState="false" AlternateText="<%$ Resources:LogiAdHoc, SaveImageAltText %>" />
                    </td>
                    <td valign="middle">
                    <asp:Label ID="lblSaveText" CssClass="lblSavePopup" runat="server" EnableViewState="false" Text="<%$ Resources:LogiAdHoc, PleaseWait %>"/>
                    </td></tr></table>
                </asp:Panel>
            </div>
            
            <div id="divLegend" class="legend" runat="server">
                <span>
                    <asp:Localize ID="locLegend" runat="server" Text="<%$ Resources:LogiAdHoc, LegendLabel %>"></asp:Localize>
                </span>
                <dl id="dlApplicationFormat" runat="server">
                    <dt>
                        <img id="Img2" runat="server" alt="Application Format" border="0" height="13" meta:resourcekey="LegendImage2"
                            src="../ahImages/spacer.gif" style="background-color: #008800;" title="Application Format"
                            width="13" />
                    </dt>
                    <dd style="color: #008800;">
                        <asp:Localize ID="Localize10" runat="server" meta:resourcekey="LiteralResource10"
                            Text="Application Format"></asp:Localize>&nbsp;
                    </dd>
                </dl>
                <dl id="dlUserDefinedFormat" runat="server">
                    <dt>
                        <img id="Img1" runat="server" alt="User-defined Format" border="0" height="13" meta:resourcekey="LegendImage3"
                            src="../ahImages/spacer.gif" style="background-color: Black;" title="User-defined Format"
                            width="13" />
                    </dt>
                    <dd style="color: Black;">
                        <asp:Localize ID="Localize1" runat="server" meta:resourcekey="LiteralResource11" Text="User-defined Format"></asp:Localize>&nbsp;
                    </dd>
                </dl>
            </div>
        </td></tr></table>
</td></tr></table>
    </form>
</body>
</html>
