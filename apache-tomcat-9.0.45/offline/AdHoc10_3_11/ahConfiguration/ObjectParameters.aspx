<%@ Page Language="vb" AutoEventWireup="false" Codebehind="ObjectParameters.aspx.vb"
    Inherits="LogiAdHoc.ahConfiguration_ObjectParameters" Culture="auto" meta:resourcekey="PageResource1"
    UICulture="auto" %>

<%@ Register TagPrefix="AdHoc" TagName="MainMenu" Src="~/ahControls/MainMenu.ascx" %>
<%@ Register TagPrefix="AdHoc" TagName="NavMenu" Src="~/ahControls/NavMenu.ascx" %>
<%@ Register TagPrefix="AdHoc" TagName="BreadCrumbTrail" Src="~/ahControls/BreadCrumbTrail.ascx" %>
<%@ Register TagPrefix="AdHoc" TagName="PagingControl" Src="../ahControls/PagingControl.ascx" %>
<%@ Register TagPrefix="wizard" TagName="SpecialValue" Src="../ahControls/SpecialValue.ascx" %>
<%@ Register TagPrefix="wizard" TagName="DatabaseValues" Src="../ahControls/DatabaseValues.ascx" %>
<%@ Register TagPrefix="AdHoc" TagName="RecompileGrid" Src="~/ahControls/RecompileReportGrid.ascx" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Data Object Parameters</title>
    <link rel="shortcut icon" href="../ahImages/flav.ico" />
    <script language="JavaScript" type="text/javascript" src="../ahScripts/ahChange.js"></script>

    <script type="text/javascript">
        window.onbeforeunload = AlertOnExit;
    </script>

</head>
<body id="bod">
    <AdHoc:MainMenu ID="menu" runat="server" />    
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server" />
    <div id="submenu">
        <AdHoc:NavMenu ID="subnav" runat="server" />
    </div>
        <asp:UpdatePanel ID="UPBct" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <AdHoc:BreadCrumbTrail ID="bct" runat="server" Key="DOParams" ParentLevels="1" />
            </ContentTemplate>
            <Triggers>
                <asp:AsyncPostBackTrigger ControlID="ddObjectID" EventName="SelectedIndexChanged" />
            </Triggers>
        </asp:UpdatePanel>

        <script type="text/javascript">
        Sys.WebForms.PageRequestManager.getInstance().add_beginRequest(BeginRequestHandler);
        Sys.WebForms.PageRequestManager.getInstance().add_endRequest(EndRequestHandler);
        function EndRequestHandler(sender, args) {
            noMessage=false;
            $find('ahSavePopupBehavior').hide();
            //RestorePopupPosition('ahModalPopupBehaviorRecompile');
            RestorePopupPosition('ahModalPopupBehavior');
        }
        function BeginRequestHandler(sender, args) {
            //SavePopupPosition('ahModalPopupBehaviorRecompile');
            SavePopupPosition('ahModalPopupBehavior');
        }
        </script>

        <div class="divForm">
            <input type="hidden" id="ahDirty" name="ahDirty" runat="server" />
            <input type="hidden" id="ahParentID" name="ahParentID" runat="server" />
            <input type="hidden" id="ahRecompiledFlag" name="ahRecompiledFlag" runat="server" />
            
            <table id="tbDBConn" runat="server" class="tbTB">
                <tr>
                    <td width="130px">
                        <asp:Localize ID="Localize6" runat="server" Text="<%$ Resources:LogiAdHoc, DatabaseConnection %>"></asp:Localize>
                    </td>
                    <td>
                        <asp:Label ID="lblDBConn" runat="server"></asp:Label>
                    </td>
                </tr>
            </table>
            <table>
                <tr>
                    <td width="130px">
                        <asp:Localize ID="Localize11" runat="server" Text="<%$ Resources:LogiAdHoc, SelectedDataObject %>"></asp:Localize>
                    </td>
                    <td>
                        <asp:DropDownList ID="ddObjectID" AutoPostBack="True" runat="server" meta:resourcekey="ddObjectIDResource1" />
                    </td>
                </tr>
            </table>
            <%--<br />--%>
            <asp:UpdatePanel ID="UP1" UpdateMode="conditional" runat="server">
                <ContentTemplate>
                    <div id="divReqFilters" runat="server">
                        <h2><asp:Localize ID="Localize13" runat="server" Text="Required Parameters" meta:resourcekey="LiteralResource13"></asp:Localize></h2>
                        <table><tr><td>
                        <div id="data_main2" >
                        <asp:GridView ID="grdReqFilters" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                            CssClass="grid" OnRowDataBound="OnReqFiltersItemDataBoundHandler" OnRowCommand="OnReqFiltersItemCommandHandler"
                            meta:resourcekey="grdReqFiltersResource1">
                            <Columns>
                                <asp:TemplateField HeaderText="Parameter Name" meta:resourcekey="TemplateFieldResource5">
                                    <ItemTemplate>
                                        <input type="hidden" id="hParam" runat="server" />
                                        <asp:Label ID="lblParamName" runat="server" meta:resourcekey="lblParamNameResource1"></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle VerticalAlign="Top" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Friendly Name" meta:resourcekey="TemplateFieldResource6">
                                    <ItemTemplate>
                                        <asp:Label ID="lblFriendlyName" runat="server" meta:resourcekey="lblFriendlyNameResource1"></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle VerticalAlign="Top" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Value" meta:resourcekey="TemplateFieldResource7">
                                    <ItemTemplate>
                                        <asp:Label ID="lblValue" runat="server" meta:resourcekey="lblValueResource1"></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle VerticalAlign="Top" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Editable" meta:resourcekey="TemplateFieldResource8">
                                    <ItemTemplate>
                                        <asp:Label ID="lblEditable" runat="server" meta:resourcekey="lblEditableResource1"></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle VerticalAlign="Top" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="<%$ Resources:LogiAdHoc, Actions %>" >
                                    <HeaderStyle Width="50px" />
                                    <ItemStyle HorizontalAlign="center" />
                                    <ItemTemplate>
                                        <asp:Image ID="imgActions" AlternateText="<%$ Resources:LogiAdHoc, Actions %>" runat="server" 
                                            ToolTip="<%$ Resources:LogiAdHoc, Actions %>" ImageUrl="~/ahImages/arrowStep.gif" SkinID="imgActions" />
                                        <asp:Panel ID="pnlActionsMenu" runat="server" CssClass="popupMenu" 
                                            HorizontalAlign="Left" Wrap="false" style="display:none;">
                                            <div id="divModify" runat="server" class="hoverMenuActionLink" >
                                                <asp:LinkButton ID="lnkModify" runat="server" CommandName="EditItem" Text="Edit Parameter"
                                                    CausesValidation="False" meta:resourcekey="EditItemResource1"></asp:LinkButton>
                                            </div>
	                                     </asp:Panel>
                                        <ajaxToolkit:HoverMenuExtender ID="hme2" runat="server"
                                            PopupControlID="pnlActionsMenu" PopupPosition="right" 
                                            TargetControlID="imgActions" PopDelay="25" />
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                            <PagerTemplate>
                                <AdHoc:PagingControl ID="pageCtrl" runat="server" OnGotoNextPage="ChangePageIndex2" />
                            </PagerTemplate>
                            <PagerStyle HorizontalAlign="Center" />
                            <HeaderStyle CssClass="gridheader" />
                            <RowStyle CssClass="gridrow" />
                            <AlternatingRowStyle CssClass="gridalternaterow" />
                        </asp:GridView>
                        </div>
                        </td></tr></table>
                        <br />
                    </div>
                    
                    <h2><asp:Localize ID="Localize14" runat="server" Text="Fixed Parameters" meta:resourcekey="LiteralResource14"></asp:Localize> </h2>   
                    <table><tr><td>
                    <div id="data_main">
                        <div id="activities" runat="server">
                            <table width="100%" cellpadding="0" cellspacing="0">
                                <tr width="100%">
                                    <td align="left" valign="top">
                                        <AdHoc:LogiButton ID="btnAddRow" OnClick="AddRow_OnClick" Text="<%$ Resources:LogiAdHoc, AddWithSpaces %>"
                                            runat="server" CausesValidation="False" />
                                    </td>
                                </tr>
                            </table>
                        </div>
                         
                        <asp:GridView ID="grdMain" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                            CssClass="grid" OnRowDataBound="OnItemDataBoundHandler" OnRowCommand="OnItemCommandHandler"
                            meta:resourcekey="grdMainResource1">
                            <Columns>
                                <asp:TemplateField HeaderText="Move" meta:resourcekey="TemplateFieldResource1">
                                    <ItemTemplate>
                                        <input type="hidden" id="hParam" runat="server" />
                                        <table>
                                            <tr>
                                                <td style="border: 0px; padding: 0px;">
                                                    <asp:Label ID="Spacer" runat="server" meta:resourcekey="SpacerResource1"></asp:Label>
                                                </td>
                                                <td style="border: 0px; padding: 0px;">
                                                    <table cellspacing="0" cellpadding="0" border="0">
                                                        <tr>
                                                            <td rowspan="2" style="border: 0px; padding: 0px;">
                                                                <asp:ImageButton ID="moveleft" CommandName="left" ImageUrl="../ahImages/arrowLeft.gif"
                                                                    CausesValidation="False" runat="server" meta:resourcekey="moveleftResource1" /></td>
                                                            <td style="border: 0px; padding: 1px 0px;">
                                                                <asp:ImageButton ID="moveup" CommandName="up" ImageUrl="../ahImages/arrowUp.gif"
                                                                    CausesValidation="False" runat="server" meta:resourcekey="moveupResource1" /></td>
                                                            <td rowspan="2" style="border: 0px; padding: 0px;">
                                                                <asp:ImageButton ID="moveright" CommandName="right" ImageUrl="../ahImages/arrowRight.gif"
                                                                    CausesValidation="False" runat="server" meta:resourcekey="moverightResource1" /></td>
                                                        </tr>
                                                        <tr>
                                                            <td style="border: 0px; padding: 1px 0px;">
                                                                <asp:ImageButton ID="movedown" CommandName="down" ImageUrl="../ahImages/arrowDown.gif"
                                                                    CausesValidation="False" runat="server" meta:resourcekey="movedownResource1" /></td>
                                                        </tr>
                                                    </table>
                                                </td>
                                                <td style="border: 0px; padding: 0px;">
                                                    <span>&nbsp;
                                                        <asp:Label ID="lblBitCmd" runat="server" meta:resourcekey="lblBitCmdResource1"></asp:Label></span>
                                                </td>
                                            </tr>
                                        </table>
                                    </ItemTemplate>
                                    <ItemStyle VerticalAlign="Top" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Column Name" meta:resourcekey="TemplateFieldResource2">
                                    <ItemTemplate>
                                        <asp:Label ID="lblOpenParen" runat="server" meta:resourcekey="lblOpenParenResource1"></asp:Label>
                                        <asp:Label ID="lblColumnName" runat="server" meta:resourcekey="lblColumnNameResource1"></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle VerticalAlign="Top" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Operator" meta:resourcekey="TemplateFieldResource3">
                                    <ItemTemplate>
                                        <asp:Label ID="lblOperator" runat="server" meta:resourcekey="lblOperatorResource1"></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle VerticalAlign="Top" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Value" meta:resourcekey="TemplateFieldResource4">
                                    <ItemTemplate>
                                        <asp:Label ID="lblValue" runat="server" meta:resourcekey="lblValueResource1"></asp:Label>
                                        <asp:Label ID="lblCloseParen" runat="server" meta:resourcekey="lblCloseParenResource1"></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle VerticalAlign="Top" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="<%$ Resources:LogiAdHoc, Actions %>" >
                                    <HeaderStyle Width="50px" />
                                    <ItemStyle HorizontalAlign="center" />
                                    <ItemTemplate>
                                        <asp:Image ID="imgActions" AlternateText="<%$ Resources:LogiAdHoc, Actions %>" runat="server" 
                                            ToolTip="<%$ Resources:LogiAdHoc, Actions %>" ImageUrl="~/ahImages/arrowStep.gif" SkinID="imgActions" />
                                        <asp:Panel ID="pnlActionsMenu" runat="server" CssClass="popupMenu" 
                                            HorizontalAlign="Left" Wrap="false" style="display:none;">
                                            <div id="divModify" runat="server" class="hoverMenuActionLink" >
                                                <asp:LinkButton ID="lnkModify" runat="server" CommandName="EditItem" Text="Edit Parameter"
                                                    CausesValidation="False" meta:resourcekey="EditItemResource1"></asp:LinkButton>
                                            </div>
                                            <div id="divRemove" runat="server" class="hoverMenuActionLink" >
                                                <asp:LinkButton ID="lnkRemove" runat="server" CommandName="RemoveItem" Text="Remove Parameter"
                                                    CausesValidation="False" meta:resourcekey="RemoveItemResource1"></asp:LinkButton>
                                            </div>
		                                 </asp:Panel>
                                        <ajaxToolkit:HoverMenuExtender ID="hme2" runat="server"
                                            PopupControlID="pnlActionsMenu" PopupPosition="right" 
                                            TargetControlID="imgActions" PopDelay="25" />
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                            <PagerTemplate>
                                <AdHoc:PagingControl ID="pageCtrl" runat="server" OnGotoNextPage="ChangePageIndex" />
                            </PagerTemplate>
                            <PagerStyle HorizontalAlign="Center" />
                            <HeaderStyle CssClass="gridheader" />
                            <RowStyle CssClass="gridrow" />
                            <AlternatingRowStyle CssClass="gridalternaterow" />
                        </asp:GridView>
                    </div>
                    </td></tr></table>
                        
                    <asp:Button runat="server" ID="Button1" Style="display: none" />
                    <ajaxToolkit:ModalPopupExtender runat="server" ID="ahModalPopup" BehaviorID="ahModalPopupBehavior"
                        TargetControlID="Button1" PopupControlID="ahPopup" BackgroundCssClass="modalBackground"
                        DropShadow="false" PopupDragHandleControlID="pnlDragHandle" RepositionMode="None">
                    </ajaxToolkit:ModalPopupExtender>
                    
                    <asp:Button runat="server" ID="Button2" Style="display: none" />
                    <ajaxToolkit:ModalPopupExtender runat="server" ID="ahModalPopupRecompile" BehaviorID="ahModalPopupBehaviorRecompile"
                        TargetControlID="Button2" PopupControlID="ahPopupRecompile" BackgroundCssClass="modalBackground"
                        DropShadow="false" PopupDragHandleControlID="pnlDragHandleRecompile" RepositionMode="None">
                    </ajaxToolkit:ModalPopupExtender>
                    
                    <asp:Panel runat="server" CssClass="modalPopup" ID="ahPopup" Style="display: none; width: 500;">
                        <%--<asp:Panel ID="pnlDetail" CssClass="detailpanel" runat="server" meta:resourcekey="pnlDetailResource1">--%>
                        <asp:Panel ID="pnlDragHandle" runat="server" Style="cursor: hand;">
                        <div class="modalPopupHandle">
                            <table cellpadding="0" cellspacing="0" style="width: 100%"><tr><td>
                                    <asp:Localize ID="PopupHeader" runat="server" Text="<%$ Resources:LogiAdHoc, ParameterDetails %>"></asp:Localize>
                                </td>
                                <td style="width: 20px;">
                                    <asp:ImageButton ID="imgClosePopup" runat="server" 
                                        OnClick="imgClosePopup_Click" CausesValidation="false"
                                        SkinID="imgbClose" ImageUrl="../ahImages/remove.gif" 
                                        AlternateText="<%$ Resources:LogiAdHoc, CloseWindow %>" />
                            </td></tr></table>
                        </div>
                        </asp:Panel>
                        <div class="modalDiv">
                        <asp:UpdatePanel ID="upPopup" runat="server">
                            <ContentTemplate>
                                <%--<asp:Panel ID="ParamDetails" CssClass="detailpanel" runat="server" meta:resourcekey="ParamDetailsResource1">--%>
                                <input type="hidden" id="NewParamID" runat="server" />
                                <input type="hidden" id="ihParamType" runat="server" />
                                <div id="divFixedParams" runat="server">
                                <table class="tbTB">
                                    <tr>
                                        <td width="100px">
                                        </td>
                                        <td valign="top">
                                            <asp:DropDownList ID="BitCmd" runat="server">
                                                <asp:ListItem Value="And" Text="<%$ Resources:LogiAdHoc, Res_And %>"></asp:ListItem>
                                                <asp:ListItem Value="Or" Text="<%$ Resources:LogiAdHoc, Res_Or %>"></asp:ListItem>
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td valign="top">
                                            <asp:Localize ID="Localize3" runat="server" meta:resourcekey="LiteralResource3" Text="Column:"></asp:Localize>
                                        </td>
                                        <td valign="top">
                                            <asp:DropDownList ID="ColumnID" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ColumnID_OnChangeHandler"
                                                meta:resourcekey="ColumnIDResource1" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td valign="top">
                                            <asp:Localize ID="Localize4" runat="server" meta:resourcekey="LiteralResource4" Text="Operator:"></asp:Localize>
                                        </td>
                                        <td valign="top">
                                            <asp:DropDownList ID="ddlOperator" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlOperator_OnChangeHandler"
                                                meta:resourcekey="ddlOperatorResource1" />
                                        </td>
                                    </tr>
                                </table>
                                <div id="divValues" runat="server" style="padding:0; margin:0;">
                                    <table>
                                        <tr>
                                            <td id="tdValueLabel" runat="server" valign="top" width="100px">
                                                <asp:Localize ID="Localize8" runat="server" meta:resourcekey="LiteralResource8" Text="Value:"></asp:Localize>
                                            </td>
                                            <td id="Td1" valign="top" runat="server">
                                                <span id="spnFV1" runat="server">
                                                    <wizard:SpecialValue ID="sv1" runat="server" />
                                                    <asp:ImageButton ID="btnFV1" AlternateText="Pick value from database" CausesValidation="False"
                                                        ImageUrl="../ahImages/iconFind.gif" OnClick="PickFromDatabase" runat="server"
                                                        meta:resourcekey="btnFV1Resource1" />
                                                </span>
                                                <asp:Label ID="lblTo" runat="server" meta:resourcekey="lblToResource1" Text="And"></asp:Label>
                                                <div id="spnFV2" runat="server">
                                                    <wizard:SpecialValue ID="sv2" runat="server" />
                                                    <asp:ImageButton ID="btnFV2" AlternateText="Pick value from database" CausesValidation="False"
                                                        ImageUrl="../ahImages/iconFind.gif" OnClick="PickFromDatabase" runat="server"
                                                        meta:resourcekey="btnFV2Resource1" />
                                                </div>
                                                <span id="spnFV3" runat="server">
                                                    <%--<asp:TextBox ID="txaTextArea" runat="server" TextMode="MultiLine" Rows="8" meta:resourcekey="txaTextAreaResource1" />--%>
                                                    <wizard:SpecialValue ID="sv3" runat="server" ShowAsList="True" />
                                                    <asp:ImageButton ID="btnFV3" AlternateText="Pick values from database" CausesValidation="False"
                                                        ImageUrl="../ahImages/iconFind.gif" OnClick="PickFromDatabase" runat="server"
                                                        meta:resourcekey="btnFV3Resource1" />
                                                </span>
                                                <asp:CheckBox ID="chkBitValue" runat="server" meta:resourcekey="chkBitValueResource1" />
                                            </td>
                                            <td id="tdFindValues" valign="top" runat="server">
                                                <table>
                                                    <tr>
                                                        <td>
                                                            <%--<asp:ListBox ID="lstList" SelectionMode="Multiple" Rows="8" Runat="server" meta:resourcekey="lstListResource1" />--%>
                                                            <wizard:DatabaseValues ID="dbValues" runat="server" />
                                                        </td>
                                                        <td>
                                                            <AdHoc:LogiButton ID="btnFVOk" Width="50px" runat="server" Text="<%$ Resources:LogiAdHoc, OKWithSpaces %>"
                                                                OnClick="btnFVOk_Click" CausesValidation="False" />
                                                            <br />
                                                            <AdHoc:LogiButton ID="btnFVCancel" Width="50px" runat="server" Text="<%$ Resources:LogiAdHoc, Cancel %>"
                                                                OnClick="btnFVCancel_Click" CausesValidation="False" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                                <p class="info" id="info" runat="server">
                                    <asp:Localize ID="Localize5" runat="server" Text="<%$ Resources:LogiAdHoc, UseEnterKey %>"></asp:Localize></p>
                                </div>
                                <div id="divReqFiltersValue" runat="server" style="padding:0; margin:0;">
                                    <table class="tbTB">
                                        <tr>
                                            <td valign="top">
                                                <asp:Localize ID="Localize9" runat="server" meta:resourcekey="LiteralResource9" Text="Parameter Name:"></asp:Localize>
                                            </td>
                                            <td valign="top">
                                                <asp:Label ID="lblParameterName" runat="server"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td valign="top">
                                                <asp:Localize ID="Localize10" runat="server" meta:resourcekey="LiteralResource10" Text="Friendly Name:"></asp:Localize>
                                            </td>
                                            <td valign="top">
                                                <asp:TextBox ID="txtParamFriendlyName" runat="server" MaxLength="128" Width="250px" />
                                                <asp:RequiredFieldValidator ID="rtvParamLabel" ControlToValidate="txtParamFriendlyName" ErrorMessage="Friendly Name is required."
                                                    runat="server" meta:resourcekey="rtvObjectLabelResource1">*</asp:RequiredFieldValidator>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td valign="top">
                                                <asp:Localize ID="Localize12" runat="server" meta:resourcekey="LiteralResource12" Text="Editable:"></asp:Localize>
                                            </td>
                                            <td valign="top">
                                                <asp:CheckBox ID="chkEditable" runat="server" />
                                            </td>
                                        </tr>
                                    </table>
                                    <div id="divReqParamValues" runat="server">
                                        <table>
                                            <tr>
                                                <td id="td2" runat="server" valign="top" width="100px">
                                                    <asp:Localize ID="Localize2" runat="server" meta:resourcekey="LiteralResource8" Text="Value:"></asp:Localize>
                                                </td>
                                                <td id="Td3" valign="top" runat="server">
                                                    <span id="Span1" runat="server">
                                                        <wizard:SpecialValue ID="svReqFiltVal" runat="server" />
                                                    </span>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                                
                                <asp:CustomValidator ID="cvValidParam" OnServerValidate="IsParameterValid" 
                                        EnableClientScript="False" ErrorMessage="This parameter leads to an invalid query."
                                        runat="server" meta:resourcekey="cvValidParamResource1">*</asp:CustomValidator>
                                        
                                <!-- Buttons -->
                                <table>
                                    <tr>
                                        <td colspan="2">
                                            <AdHoc:LogiButton ID="btnSaveParam" OnClick="SaveParam_OnClick" runat="server"
                                                Text="<%$ Resources:LogiAdHoc, OKWithSpaces %>" UseSubmitBehavior="false" 
                                                OnClientClick="javascript: document.getElementById('ahDirty').value=1; DisableSave(this);" />
                                            <AdHoc:LogiButton ID="btnCancelParam" OnClick="CancelParam_OnClick"
                                                runat="server" Text="<%$ Resources:LogiAdHoc, Cancel %>" CausesValidation="False" />
                                        </td>
                                        <td width="250px" align="right">
                                            <AdHoc:LogiButton ID="TestParam" OnClick="TestParam_OnClick" Text="Validate Parameter"
                                                runat="server" meta:resourcekey="TestParamResource1" OnClientClick="javascript: ShowSaveAnimation();" />
                                        </td>
                                    </tr>
                                </table>
                                <asp:ValidationSummary ID="vsummary" runat="server" meta:resourcekey="vsummaryResource1" />
                                <ul class="validation_error" id="ErrorList" runat="server">
                                    <li>
                                        <asp:Label ID="lblValidationMessage" runat="server" meta:resourcekey="lblValidationMessageResource1"></asp:Label></li>
                                </ul>
                                <p id="pParamValidationDone" runat="server">
                                    <asp:Label ID="lblParamValidationDone" runat="server"></asp:Label>
                                </p>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                        </div>
                        
                    </asp:Panel>
                    
                    
                    <asp:Panel runat="server" CssClass="modalPopup" ID="ahPopupRecompile" style="display:none; width:750;">
                        <asp:Panel ID="pnlDragHandleRecompile" runat="server" Style="cursor: hand;">
                            <div class="modalPopupHandle">
                                <table cellpadding="0" cellspacing="0" style="width: 100%"><tr><td>
                                        <asp:Localize ID="Localize1" runat="server" Text="<%$ Resources:LogiAdHoc, ParameterDetails %>"></asp:Localize>
                                    </td>
                                    <td style="width: 20px;">
                                        <asp:ImageButton ID="imgClosePopupRecompile" runat="server" 
                                            OnClick="imgClosePopupRecompile_Click" CausesValidation="false"
                                            SkinID="imgbClose" ImageUrl="../ahImages/remove.gif" 
                                            AlternateText="<%$ Resources:LogiAdHoc, CloseWindow %>" />
                                </td></tr></table>
                            </div>
                        </asp:Panel>
                        <div class="modalDiv">
                            <asp:UpdatePanel ID="upPopupRecompile2" runat="server">
                                <ContentTemplate>
                                    The following reports will be affected by this change, would you like to continue?
                                    <br /><br />
                                    
                                    <AdHoc:RecompileGrid runat="server" ID="grdRecompile" />
                                    <br /><br />
                                    
                                    
                                    <AdHoc:LogiButton ID="btnRecompile" runat="server" CausesValidation="False"
                                        UseSubmitBehavior="false" ToolTip="<%$ Resources:LogiAdHoc, SaveTooltip1 %>" OnClientClick="javascript: SetRebuilt();"
                                        Text="Recompile Selected" OnClick="btnRecompile_Click" />
                                    <AdHoc:LogiButton ID="btnCancelRecompile" runat="server" 
                                        ToolTip="Cancel" Text="Cancel"
                                        CausesValidation="False" OnClick="btnCancelRecompile_Click" />
                                
                                    
                                    <AdHoc:LogiButton ID="btnCloseRecompile" runat="server" Visible="false"
                                        ToolTip="close" Text="Close"
                                        CausesValidation="False" OnClick="btnCancelRecompile_Click" />
                                
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </div>
                    </asp:Panel>

                    <div id="divButtons" class="divButtons">
                        <AdHoc:LogiButton ID="btnSave" runat="server" OnClick="Save_OnClick" CausesValidation="False"
                            UseSubmitBehavior="false" ToolTip="<%$ Resources:LogiAdHoc, SaveTooltip1 %>"
                            Text="<%$ Resources:LogiAdHoc, Save %>" OnClientClick="javascript: document.getElementById('ahDirty').value=0; ShowSaveAnimationNoValidation();" />
                        <AdHoc:LogiButton ID="btnCancel" runat="server" OnClick="Cancel_OnClick"
                            ToolTip="<%$ Resources:LogiAdHoc, BackToObjectsTooltip %>" Text="<%$ Resources:LogiAdHoc, BackToObjects %>"
                            CausesValidation="False" />
                            
                            
                        <AdHoc:LogiButton ID="btnTest" runat="server" CausesValidation="False" Visible="false"
                            UseSubmitBehavior="false" ToolTip="Test"
                            Text="Test" OnClick="btnTest_Click" />
                        
                    </div>
<br />
                    
                    <div id="divSaveAnimation">
                        <asp:Button runat="server" ID="btnSaveAnimation" Style="display: none" />
                        <ajaxToolkit:ModalPopupExtender runat="server" ID="ahSavePopup" BehaviorID="ahSavePopupBehavior"
                            TargetControlID="btnSaveAnimation" PopupControlID="pnlSaving" BackgroundCssClass="modalBackground"
                            DropShadow="False">
                        </ajaxToolkit:ModalPopupExtender>
                        <asp:Panel id="pnlSaving" CssClass="savePopup" runat="server" style="display:none;">
                            <table><tr><td>
                            <asp:Image ID="imgSave" runat="server" SkinID="imgSaving" EnableViewState="false" AlternateText="<%$ Resources:LogiAdHoc, SaveImageAltText %>" />
                            </td>
                            <td valign="middle">
                            <asp:Label ID="lblSaveText" CssClass="lblSavePopup" runat="server" EnableViewState="false" Text="<%$ Resources:LogiAdHoc, PleaseWait %>"/>
                            </td></tr></table>
                        </asp:Panel>
                    </div>
                    
                </ContentTemplate>
                <Triggers>
                    <asp:AsyncPostBackTrigger ControlID="ddObjectID" EventName="SelectedIndexChanged" />
                    <asp:PostBackTrigger ControlID="btnCancel" />
                </Triggers>
            </asp:UpdatePanel>
            
        </div>
        <script type="text/javascript">
            
            function SetRebuilt()
            {
                document.getElementById('<%= ahRecompiledFlag.ClientID %>').value = 1;
            }
        
        </script>
    </form>
</body>
</html>
