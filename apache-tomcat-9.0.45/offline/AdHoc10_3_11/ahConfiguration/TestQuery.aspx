<%@ Page Language="vb" AutoEventWireup="false" Codebehind="TestQuery.aspx.vb" Inherits="LogiAdHoc.ahConfiguration_TestQuery" Culture="auto" meta:resourcekey="PageResource1" UICulture="auto" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register TagPrefix="AdHoc" TagName="MainMenu" Src="~/ahControls/MainMenu.ascx" %>
<%@ Register TagPrefix="AdHoc" TagName="NavMenu" Src="~/ahControls/NavMenu.ascx" %>
<%@ Register TagPrefix="AdHoc" TagName="PagingControl" Src="../ahControls/PagingControl.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Test Query</title>
    <link rel="shortcut icon" href="../ahImages/flav.ico" />
    <script language="JavaScript" type="text/javascript" src="../ahScripts/ahChange.js"></script>
    <script type="text/javascript">
        window.onbeforeunload = AlertOnExit;
    </script>
</head>
<body id="bod">

    <AdHoc:MainMenu ID="menu" runat="server" />
    
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server" />
    <div id="submenu">
        <AdHoc:NavMenu ID="subnav" runat="server" />
    </div>
        
        <div class="divForm">
            <input type="hidden" id="ahDirty" name="ahDirty" runat="server" />
            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                <ContentTemplate>
                    <table>
                        <tr>
                            <td width="120px">
                                <asp:Localize ID="Localize5" runat="server" meta:resourcekey="LiteralResource4" Text="Definition:"></asp:Localize></td>
                            <td>
                                <asp:TextBox ID="Definition" Rows="10" TextMode="MultiLine" Width="500px" runat="server" meta:resourcekey="DefinitionResource1" />
                                <asp:RequiredFieldValidator ID="rtvDefinition" runat="server" ErrorMessage="Data Object Definition is required."
                                    ControlToValidate="Definition" meta:resourcekey="rtvDefinitionResource1">*</asp:RequiredFieldValidator>
                            </td>
                        </tr>
                    </table>
                    <AdHoc:LogiButton ID="btnTest" runat="server" OnClick="TestDefinition"
                         ToolTip="Test Definition" Text="Test Definition" meta:resourcekey="btnTestResource1" />
                    <h2 id="GridHeader" runat="server">
                        <asp:Localize ID="Localize6" runat="server" meta:resourcekey="LiteralResource5" Text="Query Results"></asp:Localize></h2>
                    
                    <asp:ValidationSummary ID="vsummary" runat="server" meta:resourcekey="vsummaryResource1" />
                    <ul class="validation_error" id="ErrorList" runat="server">
                        <li>
                            <asp:Label ID="lblValidationMessage" runat="server" meta:resourcekey="lblValidationMessageResource1"></asp:Label></li>
                    </ul>
                    
                    <asp:UpdatePanel ID="UP1" UpdateMode="Conditional" runat="server">
                        <ContentTemplate>
                            <asp:GridView ID="grdMain" runat="server" AllowPaging="True" CssClass="grid">
                                <PagerStyle HorizontalAlign="Center" />
                                <PagerTemplate>
                                    <AdHoc:PagingControl ID="pageCtrl" runat="server" OnGotoNextPage="ChangePageIndex" />
                                </PagerTemplate>
                                <HeaderStyle CssClass="gridheader" />
                                <RowStyle CssClass="gridrow" />
                                <AlternatingRowStyle CssClass="gridalternaterow" />
                            </asp:GridView>
                        </ContentTemplate>
                        <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="btnTest" EventName="Click" />
                        </Triggers>
                    </asp:UpdatePanel>
                </ContentTemplate>
            </asp:UpdatePanel>
            <%--<div id="divHelp" class="help">
                <a href="../ahHelp/ahVirtualViews.aspx" target="_blank">
                    <asp:Localize ID="GetHelp" runat="server" meta:resourcekey="GetHelp" Text="Get help with the Test Query screen."></asp:Localize>
                </a>
            </div>--%>
        </div>
    </form>
</body>
</html>
