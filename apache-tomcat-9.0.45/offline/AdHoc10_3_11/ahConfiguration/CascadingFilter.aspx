<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="CascadingFilter.aspx.vb" Inherits="LogiAdHoc.ahConfiguration_CascadingFilter" Culture="auto" meta:resourcekey="PageResource1" UICulture="auto" %>

<%@ Register Assembly="System.Web.Extensions" Namespace="System.Web.UI" TagPrefix="asp" %>
<%@ Register TagPrefix="AdHoc" TagName="MainMenu" Src="~/ahControls/MainMenu.ascx" %>
<%@ Register TagPrefix="AdHoc" TagName="NavMenu" Src="~/ahControls/NavMenu.ascx" %>
<%@ Register TagPrefix="AdHoc" TagName="BreadCrumbTrail" Src="~/ahControls/BreadCrumbTrail.ascx" %>
<%@ Register TagPrefix="AdHoc" TagName="PagingControl" Src="~/ahControls/PagingControl.ascx" %>
<%@ Register TagPrefix="AdHoc" TagName="RecompileGrid" Src="~/ahControls/RecompileReportGrid.ascx" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Cascading Filter</title>
    <link rel="shortcut icon" href="../ahImages/flav.ico" />
    <script language="JavaScript" type="text/javascript" src="../ahScripts/ahChange.js"></script>
    <script type="text/javascript">
        window.onbeforeunload = AlertOnExit;
    </script>
</head>
<body id="bod" >
    <AdHoc:MainMenu ID="menu" runat="server" />
    
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server"  />
    <div id="submenu">
        <AdHoc:NavMenu ID="subnav" runat="server" />
    </div>
        <AdHoc:BreadCrumbTrail ID="bct" runat="server" Key="CascadingFilter" ParentLevels="1" />

        <script type="text/javascript">
        Sys.WebForms.PageRequestManager.getInstance().add_beginRequest(BeginRequestHandler);
        Sys.WebForms.PageRequestManager.getInstance().add_endRequest(EndRequestHandler);
        function EndRequestHandler(sender, args) {
            $find('ahSavePopupBehavior').hide();
            RestorePopupPosition('ahModalPopupBehavior');
        }
        function BeginRequestHandler(sender, args) {
            SavePopupPosition('ahModalPopupBehavior');
        }
        </script>

        <div class="divForm">
        <input type="hidden" id="ahDirty" name="ahDirty" runat="server" />
        <input type="hidden" id="ahParentID" name="ahParentID" runat="server" />
        <input type="hidden" id="ahRecompiledFlag" name="ahRecompiledFlag" runat="server" />
                
        <asp:UpdatePanel ID="UP1" runat="server" UpdateMode="Conditional" RenderMode="Inline">
            <ContentTemplate>
            <table id="tbDBConn" runat="server" class="tbTB">
                <tr>
                    <td width="130px">
                        <asp:Localize ID="Localize8" runat="server" Text="<%$ Resources:LogiAdHoc, DatabaseConnection %>"></asp:Localize>
                    </td>
                    <td>
                        <asp:Label ID="lblDBConn" runat="server"></asp:Label>
                    </td>
                </tr>
            </table>
            <table id="tbFilter" runat="server" class="tbTB">
                <tr>
                    <td width="130px">
                        <asp:Localize ID="Localize1" runat="server" meta:resourcekey="LiteralResource1" Text="Selected Filter:"></asp:Localize>
                    </td>
                    <td>
                        <asp:DropDownList ID="ddFilterID" AutoPostBack="True" runat="server" meta:resourcekey="ddFilterIDResource1" />
                    </td>
                </tr>
            </table>
            <table class="tbTB">
                <tr>
                    <td width="130">
                        <asp:Localize ID="Localize2" runat="server" Text="<%$ Resources:LogiAdHoc, Name %>"></asp:Localize>:
                    </td>
                    <td>
                        <asp:TextBox ID="FilterName" Columns="50" MaxLength="100" runat="server" meta:resourcekey="FilterNameResource1" />
                        <asp:RequiredFieldValidator ID="rtvFilterName" runat="server" ControlToValidate="FilterName"
                            ErrorMessage="Filter Name is required." meta:resourcekey="rtvFilterNameResource1">*</asp:RequiredFieldValidator>
                        <%--<asp:CustomValidator ID="cvValidFilter" OnServerValidate="IsFilterValid" ControlToValidate="FilterName"
                            EnableClientScript="False" runat="server">*</asp:CustomValidator>--%>
                    </td>
                </tr>
                <tr>
                    <td width="125"><asp:Localize ID="Localize9" runat="server" Text="<%$ Resources:LogiAdHoc, Description %>"></asp:Localize>:</td>
                    <td>
                        <asp:TextBox ID="FilterDescription" TextMode="MultiLine" Columns="50" Rows="3" runat="server" meta:resourcekey="DescriptionResource1" />
                        <asp:CustomValidator ID="cvMaxLength" runat="server" ErrorMessage="<%$ Resources:Errors, Err_Description255 %>"
                            ControlToValidate="FilterDescription" OnServerValidate="ReachedMaxLength">*</asp:CustomValidator>
                    </td>
                </tr>
            </table>

            <br />
                            
            <table><tr><td>
                <div id="data_main">
                    <div id="activities">
                        <AdHoc:LogiButton ID="btnAddRow" OnClick="AddRow_OnClick" Text="<%$ Resources:LogiAdHoc, AddWithSpaces %>"
                            runat="server" CausesValidation="False" />
                        <AdHoc:LogiButton ID="btnRemoveRow" OnClick="RemoveRow_OnClick" Text="<%$ Resources:LogiAdHoc, Delete %>"
                            runat="server" CausesValidation="False"/>
                    </div>
                    <asp:GridView ID="grdMain" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                        CssClass="gridDetail" DataKeyNames="FilterDetailID" meta:resourcekey="grdMainResource1"
                        OnRowDataBound="OnItemDataBoundHandler">
                        <Columns>
                            <asp:TemplateField meta:resourcekey="TemplateFieldResource1">
                                <HeaderStyle Width="30px"></HeaderStyle>
                                <HeaderTemplate>
                                    <asp:Label ID="lblChkAll" runat="server" AssociatedControlID="CheckAll" Text="<%$ Resources:LogiAdHoc, SelectDeselectAll %>" CssClass="NoShow"></asp:Label>
                                    <asp:CheckBox ID="CheckAll" runat="server" meta:resourcekey="CheckAllResource1" ToolTip="<%$ Resources:LogiAdHoc, SelectDeselectAll %>" />
                                </HeaderTemplate>
                                <ItemStyle HorizontalAlign="Center" />
                                <ItemTemplate>
                                    <asp:Label ID="lblChk" runat="server" AssociatedControlID="chk_Select" Text="<%$ Resources:LogiAdHoc, SelectDeselect %>" CssClass="NoShow"></asp:Label>
                                    <asp:CheckBox ID="chk_Select" runat="server" ToolTip="<%$ Resources:LogiAdHoc, SelectDeselect %>"/>
                                    <input type="hidden" id="ihFilterItemOrder" runat="server" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <%--<asp:BoundField DataField="FilterOrder" HeaderText="Order" meta:resourcekey="BoundFieldResource1" />--%>
                            <asp:BoundField DataField="ObjectName" HeaderText="Data Object" meta:resourcekey="BoundFieldResource2" />
                            <asp:BoundField DataField="FilterColumnName" HeaderText="Filter Column" meta:resourcekey="BoundFieldResource3" />
                            <asp:BoundField DataField="ValueColumnName" HeaderText="Value Column" meta:resourcekey="BoundFieldResource4" />
                            <asp:BoundField DataField="DisplayColumnName" HeaderText="Display Column" meta:resourcekey="BoundFieldResource5" />
                            <asp:TemplateField HeaderText="<%$ Resources:LogiAdHoc, Actions %>" meta:resourcekey="TemplateFieldResource2">
                                <HeaderStyle Width="50px" />
                                <ItemStyle HorizontalAlign="Center" />
                                <ItemTemplate>
                                    <asp:ImageButton ID="imgModify" SkinID="imgSingleAction" AlternateText="Modify Filter Item" ToolTip="Modify Filter Item"
                                        runat="server" OnCommand="ManageFilter" CausesValidation="false" meta:resourcekey="ModifyResource1" />
                                    <asp:TextBox ID="txtFakeValidator" runat="server" CssClass="NoShow"></asp:TextBox>
                                    <asp:CustomValidator ID="cvValidFilterItem" runat="server" ControlToValidate="txtFakeValidator"
                                         OnServerValidate="IsFilterItemValid" ErrorMessage="Filter Item is not valid." 
                                         ValidateEmptyText="true">*</asp:CustomValidator>
                                    <%--<asp:Image ID="imgActions" AlternateText="<%$ Resources:LogiAdHoc, Actions %>" runat="server" 
                                        ToolTip="<%$ Resources:LogiAdHoc, Actions %>" ImageUrl="~/ahImages/arrowStep.gif" SkinID="imgActions" />
                                    <asp:Panel ID="pnlActionsMenu" runat="server" CssClass="popupMenu" 
                                        HorizontalAlign="Left" Wrap="false" style="display:none;">
                                        <div id="divModify" runat="server" class="hoverMenuActionLink" >
                                            <asp:LinkButton ID="lnkModify" runat="server" OnCommand="ManageFilter" 
                                                Text="Modify Filter Item" CausesValidation="False" ToolTip="Modify Filter Item"
                                                meta:resourcekey="ModifyResource1"></asp:LinkButton>
                                        </div>
		                            </asp:Panel>
                                    <ajaxToolkit:HoverMenuExtender ID="hme2" runat="server"
                                        PopupControlID="pnlActionsMenu" PopupPosition="right" 
                                        TargetControlID="imgActions" PopDelay="25" />--%>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <PagerStyle HorizontalAlign="Center" />
                        <HeaderStyle CssClass="gridheader" />
                        <PagerTemplate>
                            <AdHoc:PagingControl ID="pageCtrl" runat="server" OnGotoNextPage="ChangePageIndex" />
                        </PagerTemplate>
                        <RowStyle CssClass="gridrow" />
                        <AlternatingRowStyle CssClass="gridalternaterow" />
                    </asp:GridView>
                </div>
            </td><td>
                <asp:CustomValidator ID="cvValidFilter" runat="server" ControlToValidate="FilterName"
                    EnableClientScript="False" OnServerValidate="IsFilterValid">*</asp:CustomValidator>
            </td></tr></table>
            <br />
                    
            <AdHoc:LogiButton ID="btnTestFilter" runat="server" OnClick="TestFilter"
                Tooltip="Click to test this filter." Text="Test Filter" 
                CausesValidation="True" meta:resourceKey="btnTestFilterResource1" />
            <br />

            <div id="divButtons" class="divButtons">
                <AdHoc:LogiButton ID="btnSave" runat="server" OnClick="SaveFilter" ToolTip="<%$ Resources:LogiAdHoc, SaveTooltip1 %>"
                    Text="<%$ Resources:LogiAdHoc, Save %>" UseSubmitBehavior="false" />
                <AdHoc:LogiButton ID="btnCancel" runat="server" OnClick="CancelFilter"
                    ToolTip="Click to cancel your unsaved changes and return to the previous page." Text="Back to Cascading Filters"
                    CausesValidation="False" meta:resourcekey="btnCancelResource1" />
                <asp:ValidationSummary ID="vsummary" runat="server" meta:resourcekey="vsummaryResource1" />
                <asp:Label ID="lblSemicolon" runat="server" OnPreRender="lblSemicolon_PreRender" />
            </div>
<br />
                    
                <asp:Button runat="server" ID="Button1" style="display:none"/>
                <ajaxToolkit:ModalPopupExtender runat="server" ID="ahModalPopup" BehaviorID="ahModalPopupBehavior"
                    TargetControlID="Button1" PopupControlID="ahPopup" BackgroundCssClass="modalBackground"
                    DropShadow="false" PopupDragHandleControlID="pnlDragHandle" RepositionMode="None">
                </ajaxToolkit:ModalPopupExtender>
                
                <asp:Button runat="server" ID="Button2" Style="display: none" />
                    <ajaxToolkit:ModalPopupExtender runat="server" ID="ahModalPopupRecompile" BehaviorID="ahModalPopupBehaviorRecompile"
                        TargetControlID="Button2" PopupControlID="ahPopupRecompile" BackgroundCssClass="modalBackground"
                        DropShadow="false" PopupDragHandleControlID="pnlDragHandleRecompile" RepositionMode="None">
                    </ajaxToolkit:ModalPopupExtender>
                
                <asp:Panel runat="server" CssClass="modalPopup" ID="ahPopup" style="display:none; width:550;">
                    <asp:Panel ID="pnlDragHandle" runat="server" Style="cursor: hand;">
                    <div class="modalPopupHandle" >
                        <table cellpadding="0" cellspacing="0" style="width: 100%"><tr><td>
                            <asp:Localize ID="PopupHeader" runat="server" meta:resourcekey="LiteralResource3" Text="Filter Item Details"></asp:Localize>
                            </td>
                            <td style="width: 20px;">
                                <asp:ImageButton ID="imgClosePopup" runat="server" 
                                    OnClick="imgClosePopup_Click" CausesValidation="false"
                                    SkinID="imgbClose" ImageUrl="../ahImages/remove.gif"  
                                    AlternateText="<%$ Resources:LogiAdHoc, CloseWindow %>"/>
                        </td></tr></table>
                    </div>
                    </asp:Panel>
                    <div class="modalDiv">
                    <asp:UpdatePanel ID="upPopup" runat="server" UpdateMode="Conditional" >
                        <ContentTemplate>
                            <%--<div class="modalDiv">--%>
                            <%--<asp:Panel ID="pnlDetail" CssClass="detailpanel" runat="server" meta:resourcekey="pnlDetailResource1">--%>
                            <h3>
                                <asp:Localize ID="Localize3" runat="server"></asp:Localize></h3>
                            <input id="FilterDetailIDpnl" type="hidden" runat="server" />
                            <table class="tbTB">
                                <tr>
                                    <td width="125">
                                        <label for="ddlObject">
                                            <asp:Localize ID="Localize4" runat="server" meta:resourcekey="LiteralResource4" Text="Data Object:"></asp:Localize></label></td>
                                    <td>
                                        <asp:DropDownList ID="ddlObject" AutoPostback="True" OnSelectedIndexChanged="Object_SelectedIndexChanged"
                                            runat="server" meta:resourcekey="ddlObjectResource1" />
                                    </td>
                                </tr>
                                <tr id="trFilterColumn" runat="server">
                                    <td width="125" runat="server">
                                        <label for="ddlFilterColumn">
                                            <asp:Localize ID="Localize5" runat="server" meta:resourcekey="LiteralResource5" Text="Filter Column:"></asp:Localize></label></td>
                                    <td runat="server">
                                        <asp:DropDownList ID="ddlFilterColumn" runat="server" />
                                    </td>
                                </tr>
                                <tr>
                                    <td width="125">
                                        <label for="ddlValueColumn">
                                            <asp:Localize ID="Localize6" runat="server" meta:resourcekey="LiteralResource6" Text="Value Column:"></asp:Localize></label></td>
                                    <td>
                                        <asp:DropDownList ID="ddlValueColumn" runat="server" meta:resourcekey="ddlValueColumnResource1" />
                                    </td>
                                </tr>
                                <tr>
                                    <td width="125">
                                        <label for="ddlDisplayColumn">
                                            <asp:Localize ID="Localize7" runat="server" meta:resourcekey="LiteralResource7" Text="Display Column:"></asp:Localize></label></td>
                                    <td>
                                        <asp:DropDownList ID="ddlDisplayColumn" runat="server" meta:resourcekey="ddlDisplayColumnResource1" />
                                    </td>
                                </tr>
                            </table>
                            <br />
                            <!-- Buttons -->
                            <table>
                                <tr>
                                    <td colspan="2">
                                        <AdHoc:LogiButton ID="btnSaveItem" OnClick="SaveItem_OnClick" runat="server" CausesValidation="false"
                                            Text="<%$ Resources:LogiAdHoc, OKWithSpaces %>" UseSubmitBehavior="false" />
                                        <AdHoc:LogiButton ID="btnCancelItem" OnClick="CancelItem_OnClick" runat="server"
                                            Text="<%$ Resources:LogiAdHoc, Cancel %>" CausesValidation="False" />
                                    </td>
                                </tr>
                            </table>
                            <p id="info" runat="server" class="info">
                                <asp:Localize ID="Localize10" runat="server" meta:resourcekey="LiteralResource9"
                                    Text="Selecting a different column for Value and Display may result in duplicate data if the columns do not have one-to-one relationship."></asp:Localize></p>
                            <%--</div>--%>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                    </div>
                </asp:Panel>
            
                
                    <asp:Panel runat="server" CssClass="modalPopup" ID="ahPopupRecompile" style="display:none; width:750;">
                        <asp:Panel ID="pnlDragHandleRecompile" runat="server" Style="cursor: hand;">
                            <div class="modalPopupHandle">
                                <table cellpadding="0" cellspacing="0" style="width: 100%"><tr><td>
                                        <asp:Localize ID="Localize11" runat="server" Text="<%$ Resources:LogiAdHoc, ParameterDetails %>"></asp:Localize>
                                    </td>
                                    <td style="width: 20px;">
                                        <asp:ImageButton ID="imgClosePopupRecompile" runat="server" 
                                            OnClick="imgClosePopupRecompile_Click" CausesValidation="false"
                                            SkinID="imgbClose" ImageUrl="../ahImages/remove.gif" 
                                            AlternateText="<%$ Resources:LogiAdHoc, CloseWindow %>" />
                                </td></tr></table>
                            </div>
                        </asp:Panel>
                        <div class="modalDiv">
                            <asp:UpdatePanel ID="upPopupRecompile2" runat="server">
                                <ContentTemplate>
                                    The following reports will be affected by this change, would you like to continue?
                                    <br /><br />
                                    
                                    <AdHoc:RecompileGrid runat="server" ID="grdRecompile" />
                                    <br /><br />
                                    
                                    
                                    <AdHoc:LogiButton ID="btnRecompile" runat="server" CausesValidation="False"
                                        UseSubmitBehavior="false" ToolTip="<%$ Resources:LogiAdHoc, SaveTooltip1 %>" OnClientClick="javascript: SetRebuilt();"
                                        Text="Recompile Selected" OnClick="btnRecompile_Click" />
                                    <AdHoc:LogiButton ID="btnCancelRecompile" runat="server" 
                                        ToolTip="Cancel" Text="Cancel"
                                        CausesValidation="False" OnClick="btnCancelRecompile_Click" />
                                
                                    
                                    <AdHoc:LogiButton ID="btnCloseRecompile" runat="server" Visible="false"
                                        ToolTip="close" Text="Close"
                                        CausesValidation="False" OnClick="btnCancelRecompile_Click" />
                                
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </div>
                    </asp:Panel>
                    
            
                <div id="divSaveAnimation">
                <asp:Button runat="server" ID="btnSaveAnimation" Style="display: none" />
                <ajaxToolkit:ModalPopupExtender runat="server" ID="ahSavePopup" BehaviorID="ahSavePopupBehavior"
                    TargetControlID="btnSaveAnimation" PopupControlID="pnlSaving" BackgroundCssClass="modalBackground"
                    DropShadow="False">
                </ajaxToolkit:ModalPopupExtender>
                <asp:Panel id="pnlSaving" CssClass="savePopup" runat="server" style="display:none;">
                    <table><tr><td>
                    <asp:Image ID="imgSave" runat="server" SkinID="imgSaving" EnableViewState="false" AlternateText="<%$ Resources:LogiAdHoc, SaveImageAltText %>" />
                    </td>
                    <td valign="middle">
                    <asp:Label ID="lblSaveText" CssClass="lblSavePopup" runat="server" EnableViewState="false" Text="<%$ Resources:LogiAdHoc, PleaseWait %>"/>
                    </td></tr></table>
                </asp:Panel>
            </div>
                
            </ContentTemplate>
            <Triggers>
                <asp:AsyncPostBackTrigger ControlID="ddFilterID" EventName="SelectedIndexChanged" />
                <asp:AsyncPostBackTrigger ControlID="btnRemoveRow" EventName="Click" />
                <asp:PostBackTrigger ControlID="btnCancel" />
            </Triggers>
        </asp:UpdatePanel>
        </div>
    </form>
</body>
</html>
