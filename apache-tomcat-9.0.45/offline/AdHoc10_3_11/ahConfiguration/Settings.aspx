<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="Settings.aspx.vb" Inherits="LogiAdHoc.ahConfiguration_Settings" Culture="auto" meta:resourcekey="PageResource2" UICulture="auto" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register TagPrefix="AdHoc" TagName="MainMenu" Src="~/ahControls/MainMenu.ascx" %>
<%@ Register TagPrefix="AdHoc" TagName="NavMenu" Src="~/ahControls/NavMenu.ascx" %>
<%@ Register TagPrefix="AdHoc" TagName="BreadCrumbTrail" Src="~/ahControls/BreadCrumbTrail.ascx" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Report Settings</title>
    <link rel="shortcut icon" href="../ahImages/flav.ico" />
    <script language="JavaScript" type="text/javascript" src="../ahScripts/ahChange.js"></script>
    <script type="text/javascript">
        window.onbeforeunload = AlertOnExit;
    </script>
</head>
<body id="bod">
    <AdHoc:MainMenu ID="menu" runat="server" />
    
    <form id="form1" runat="server">
        <div id="submenu">
            <AdHoc:NavMenu ID="subnav" runat="server" />
        </div>
        <AdHoc:BreadCrumbTrail ID="bct" runat="server" Key="ReportSettings" ParentLevels="0" />
        
        <asp:ScriptManager ID="ScriptManager1" runat="server"  />
        
        <script type="text/javascript">
        Sys.WebForms.PageRequestManager.getInstance().add_endRequest(EndRequestHandler);
        function EndRequestHandler(sender, args) {
            $find('ahSavePopupBehavior').hide();
        }
        </script>
        
        <div class="divForm">
        <input type="hidden" id="ahDirty" name="ahDirty" runat="server" />
        <!-- Put other global default settings here.  -->
            
            <p class="info">
                <asp:Localize ID="Localize1" runat="server" meta:resourcekey="LiteralResource1" Text="The settings below determine the behavior of the application and take effect immediately."></asp:Localize></p>
        
            <table>
                <tr>
                    <td style="width: 166px">
                        <label for="DesignTimeValidation">
                            <asp:Localize ID="Localize2" runat="server" meta:resourcekey="LiteralResource2" Text="Design-time Validation:"></asp:Localize></label></td>
                    <td>
                        <asp:DropDownList ID="DesignTimeValidation" runat="server" meta:resourcekey="DesignTimeValidationResource1">
                            <asp:ListItem Value="On" Text="<%$ Resources:LogiAdHoc, Res_On %>" />
                            <asp:ListItem Value="Off" Text="<%$ Resources:LogiAdHoc, Res_Off %>" />
                        </asp:DropDownList>
                    </td>
                    <td>
                        <img id="help3" runat="server" src="../ahImages/iconHelp.gif" />
                        <ajaxToolkit:PopupControlExtender ID="PopupControlExtender3" runat="server" TargetControlID="help3" PopupControlID="pnlHelp3" Position="Bottom" BehaviorID="PopupControlExtender3" DynamicServicePath="" Enabled="True" ExtenderControlID="" />
                    </td>
                    <td></td>
                </tr>
            </table>

            <p class="info">
                <asp:Localize ID="Localize4" runat="server" meta:resourcekey="LiteralResource4" Text="The settings below set limits and features of new reports. You can apply these settings
                to an existing report by rebuilding it."></asp:Localize></p>

            <table>
                <tr>
                    <td style="width: 166px">
                        <label for="MaxReportRecords">
                            <asp:Localize ID="Localize5" runat="server" meta:resourcekey="LiteralResource5" Text="Max Records:"></asp:Localize></label></td>
                    <td width="140">
                        <input id="MaxReportRecords" type="text" maxlength="7" runat="server" name="MaxReportRecords">
                        <asp:RangeValidator ID="rvMaxRecords" runat="server" ControlToValidate="MaxReportRecords"
                            Type="Integer" MinimumValue="0" MaximumValue="9999999" ErrorMessage="Max Records accepts only integer values." meta:resourcekey="rvMaxRecordsResource1">*</asp:RangeValidator>
                        <asp:RequiredFieldValidator ID="rtvMaxRecords" runat="server" ControlToValidate="MaxReportRecords"
                            ErrorMessage="Max Records is required." meta:resourcekey="rtvMaxRecordsResource1">*</asp:RequiredFieldValidator>
                    </td>
                    <td width="30">
                        <img id="help1" runat="server" src="../ahImages/iconHelp.gif" />
                        <ajaxToolkit:PopupControlExtender ID="PopupControlExtender1" runat="server" TargetControlID="help1" PopupControlID="pnlHelp1" Position="Bottom" BehaviorID="PopupControlExtender1" DynamicServicePath="" Enabled="True" ExtenderControlID="" />
                    </td>
                    <td width="300px"></td>
                </tr>
                <tr>
                    <td style="width: 166px">
                        <asp:Localize ID="Localize6" runat="server" meta:resourcekey="LiteralResource6" Text="Show message if maximum records reached:"></asp:Localize>
                    </td>
                    <td><asp:CheckBox ID="chkShowMessage" runat="server" /></td>
                    <td>
                        <img id="help6" runat="server" src="../ahImages/iconHelp.gif" />
                        <ajaxToolkit:PopupControlExtender ID="PopupControlExtender6" runat="server" TargetControlID="help6" PopupControlID="pnlHelp6" Position="Bottom" BehaviorID="PopupControlExtender6" DynamicServicePath="" Enabled="True" ExtenderControlID="" />
                    </td>
                    <td></td>
                </tr>
                <tr>
                    <td style="width: 166px">
                        <asp:Localize ID="Localize20" runat="server" meta:resourcekey="LiteralResource20" Text="Show message if no records returned:"></asp:Localize>
                    </td>
                    <td><asp:CheckBox ID="chkShowNoRowMessage" runat="server" /></td>
                    <td>
                        <img id="help10" runat="server" src="../ahImages/iconHelp.gif" />
                        <ajaxToolkit:PopupControlExtender ID="PopupControlExtender10" runat="server" TargetControlID="help10" PopupControlID="pnlHelp10" Position="Bottom" BehaviorID="PopupControlExtender10" DynamicServicePath="" Enabled="True" ExtenderControlID="" />
                    </td>
                    <td></td>
                </tr>
            </table>
            <table>
                <tr>
                    <td style="width: 166px">
                        <asp:Localize ID="Localize22" runat="server" meta:resourcekey="LiteralResource22" Text="Row count message:"></asp:Localize>
                    </td>
                    <td>
                        <input id="txtRecCountPre" type="text" runat="server" />&nbsp;<asp:Localize ID="Localize23" runat="server" meta:resourcekey="LiteralResource23" Text="[row count]"></asp:Localize>&nbsp;<asp:TextBox ID="txtRowCountPost" runat="server" Text="<%$ Resources:LogiAdHoc, RowCountPost %>" />
                    </td>
                    <td>
                        <img id="help4" runat="server" src="../ahImages/iconHelp.gif" />
                        <ajaxToolkit:PopupControlExtender ID="PopupControlExtender4" runat="server" TargetControlID="help4" PopupControlID="pnlHelp4" Position="Bottom" BehaviorID="PopupControlExtender4" DynamicServicePath="" Enabled="True" ExtenderControlID="" />
                    </td>
                </tr>
            </table>
            <table>
                <tr>
                    <td style="width: 166px">
                        <asp:Localize ID="Localize25" runat="server" meta:resourcekey="LiteralResource25" Text="Row count message style:"></asp:Localize>
                    </td>
                    <td>
                        <asp:DropDownList ID="ddlAppearance" runat="server" />
                    </td>
                    <td>
                        <img id="help11" runat="server" src="../ahImages/iconHelp.gif" />
                        <ajaxToolkit:PopupControlExtender ID="PopupControlExtender11" runat="server" TargetControlID="help11" PopupControlID="pnlHelp11" Position="Bottom" BehaviorID="PopupControlExtender11" DynamicServicePath="" Enabled="True" ExtenderControlID="" />
                    </td>
                    <td></td>
                </tr>
                <tr>
                    <td style="width: 166px">
                        <asp:Localize ID="Localize3" runat="server" meta:resourcekey="LiteralResource3" Text="Apply data format to Excel exports:"></asp:Localize>
                    </td>
                    <td>
                    <asp:DropDownList ID="ddlFormatExcel" runat="server">
                        <asp:ListItem Value="True" Text="Always" meta:resourcekey="ListItemResource1" />
                        <asp:ListItem Value="ExcludeDate" Text="Exclude Dates" meta:resourcekey="ListItemResource2" />
                        <asp:ListItem Value="False" Text="<%$ Resources:LogiAdHoc, Never %>" />
                    </asp:DropDownList>
                    </td>
                    <td>
                        <img id="help9" runat="server" src="../ahImages/iconHelp.gif" />
                        <ajaxToolkit:PopupControlExtender ID="PopupControlExtender9" runat="server" TargetControlID="help9" PopupControlID="pnlHelp9" Position="Bottom" BehaviorID="PopupControlExtender9" DynamicServicePath="" Enabled="True" ExtenderControlID="" />
                    </td>
                    <td></td>
                </tr>
                <tr>
                    <td style="width: 166px">
                        <label for="MaxListRecords">
                            <asp:Localize ID="Localize7" runat="server" meta:resourcekey="LiteralResource7" Text="Max List Records:"></asp:Localize></label></td>
                    <td>
                        <input id="MaxListRecords" type="text" maxlength="5" runat="server" name="MaxListRecords">
                        <asp:RangeValidator ID="rvMaxListRecords" runat="server" ControlToValidate="MaxListRecords"
                            Type="Integer" MinimumValue="0" MaximumValue="64000" ErrorMessage="Max List Records accepts only integer values between 0 and 64000." meta:resourcekey="rvMaxListRecordsResource1">*</asp:RangeValidator>
                        <asp:RequiredFieldValidator ID="rtvMaxListRecords" runat="server" ControlToValidate="MaxListRecords"
                            ErrorMessage="Max List Records is required." meta:resourcekey="rtvMaxListRecordsResource1">*</asp:RequiredFieldValidator>
                    </td>
                    <td>
                        <img id="help2" runat="server" src="../ahImages/iconHelp.gif" />
                        <ajaxToolkit:PopupControlExtender ID="PopupControlExtender2" runat="server" TargetControlID="help2" PopupControlID="pnlHelp2" Position="Bottom" BehaviorID="PopupControlExtender2" DynamicServicePath="" Enabled="True" ExtenderControlID="" />
                    </td>
                    <td></td>
                </tr>
            </table>
            
            <p class="info">
                <asp:Localize ID="Localize8" runat="server" meta:resourcekey="LiteralResource8" Text="The settings below determine default values for the report wizard and will be used for new reports only."></asp:Localize></p>
        
            <table>
                <tr>
                    <td style="width: 166px">
                        <asp:Localize ID="Localize9" runat="server" meta:resourcekey="LiteralResource9" Text="Default rows per report page:"></asp:Localize></td>
                    <td>
                        <input id="RowsPerPage" type="text" maxlength="3" runat="server" name="RowsPerPage">
                        <asp:RangeValidator ID="rvRowsPerPage" runat="server" ControlToValidate="RowsPerPage"
                            Type="Integer" MinimumValue="1" MaximumValue="100" ErrorMessage="Rows per report page accepts only integer values between 1 and 100." meta:resourcekey="rvRowsPerPageResource1">*</asp:RangeValidator>
                        <asp:RequiredFieldValidator ID="rtvRowsPerPage" runat="server" ControlToValidate="RowsPerPage"
                            ErrorMessage="Rows per report page is required." meta:resourcekey="rtvRowsPerPageResource1">*</asp:RequiredFieldValidator>
                    </td>
                    <td>
                        <img id="help7" runat="server" src="../ahImages/iconHelp.gif" />
                        <ajaxToolkit:PopupControlExtender ID="PopupControlExtender7" runat="server" TargetControlID="help7" PopupControlID="pnlHelp7" Position="Bottom" BehaviorID="PopupControlExtender7" DynamicServicePath="" Enabled="True" ExtenderControlID="" />
                    </td>
                    <td>
                    </td>
                </tr>
                <tr>
                    <td style="width: 166px">
                        <asp:Localize ID="Localize10" runat="server" meta:resourcekey="LiteralResource10" Text="Default rows per sub-report page:"></asp:Localize></td>
                    <td>
                        <input id="RowsPerSubreportPage" type="text" maxlength="3" runat="server" name="RowsPerSubreportPage">
                        <asp:RangeValidator ID="rvRowsPerSubreportPage" runat="server" ControlToValidate="RowsPerSubreportPage"
                            Type="Integer" MinimumValue="1" MaximumValue="100" ErrorMessage="Rows per sub-report page accepts only integer values between 1 and 100." meta:resourcekey="rvRowsPerSubreportPageResource1">*</asp:RangeValidator>
                        <asp:RequiredFieldValidator ID="rtvRowsPerSubreportPage" runat="server" ControlToValidate="RowsPerSubreportPage"
                            ErrorMessage="Rows per sub-report page is required." meta:resourcekey="rtvRowsPerSubreportPageResource1">*</asp:RequiredFieldValidator>
                    </td>
                    <td>
                        <img id="help8" runat="server" src="../ahImages/iconHelp.gif" />
                        <ajaxToolkit:PopupControlExtender ID="PopupControlExtender8" runat="server" TargetControlID="help8" PopupControlID="pnlHelp8" Position="Bottom" BehaviorID="PopupControlExtender8" DynamicServicePath="" Enabled="True" ExtenderControlID="" />
                    </td>
                    <td>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Localize ID="Localize27" runat="server" meta:resourcekey="LiteralResource27" Text="Default printable page size:"></asp:Localize>
                    </td>
                    <td>
                        <asp:DropDownList ID="ddlPageSize" runat="server" />
                    </td>
                    <td>
                        <img id="help12" runat="server" src="../ahImages/iconHelp.gif" />
                        <ajaxToolkit:PopupControlExtender ID="PopupControlExtender12" runat="server" TargetControlID="help12" PopupControlID="pnlHelp12" Position="Bottom" BehaviorID="PopupControlExtender12" DynamicServicePath="" Enabled="True" ExtenderControlID="" />
                    </td>
                    <td></td>
                </tr>
                <tr>
                    <td valign=top style="width: 166px" >
                        <label for="template">
                            <asp:Localize ID="Localize11" runat="server" meta:resourcekey="LiteralResource11" Text="Default Template:"></asp:Localize></label></td>
                    <td valign=top>
                        <asp:DropDownList ID="template" runat="server" AutoPostBack="True" meta:resourcekey="templateResource1">
                        </asp:DropDownList>
                    </td>
                    <td valign=top>
                        <img id="help5" runat="server" src="../ahImages/iconHelp.gif" />
                        <ajaxToolkit:PopupControlExtender ID="PopupControlExtender5" runat="server" TargetControlID="help5" PopupControlID="pnlHelp5" Position="Bottom" BehaviorID="PopupControlExtender5" DynamicServicePath="" Enabled="True" ExtenderControlID="" />
                    </td>
                    <td valign=top>
                        <div>
                            <asp:UpdatePanel ID="UP1" runat="server">
                                <ContentTemplate>
                                    <asp:Image ID="templatesample" runat="server" meta:resourcekey="templatesampleResource1" />
                                </ContentTemplate>
                                <Triggers>
                                    <asp:AsyncPostBackTrigger ControlID="template" EventName="SelectedIndexChanged" />
                                </Triggers>
                            </asp:UpdatePanel>
                        </div>
                    </td>
                </tr>
            </table>

            <asp:Panel ID="pnlHelp1" runat="server" CssClass="popupControl" Width="300px" meta:resourcekey="pnlHelp1Resource1">
                <asp:Localize ID="Localize12" runat="server" meta:resourcekey="LiteralResource12" >
                This setting inserts a "TOP n" clause into the main report query and any subqueries.
                It improves database performance, but users might not see all the results from their
                queries. Enter 0 (zero) to disable this feature.
                </asp:Localize>
            </asp:Panel>

            <asp:Panel ID="pnlHelp2" runat="server" CssClass="popupControl" Width="300px" meta:resourcekey="pnlHelp2Resource1">
                <asp:Localize ID="Localize13" runat="server" meta:resourcekey="LiteralResource13" >
                This setting inserts a "TOP n" clause into any <b>in-list</b> query. It is recommended
                to keep this limit as small as possible to improve performance and keep drop-down
                lists manageable.
                </asp:Localize>
            </asp:Panel>

            <asp:Panel ID="pnlHelp3" runat="server" CssClass="popupControl" Width="300px" meta:resourcekey="pnlHelp3Resource1">
                <asp:Localize ID="Localize14" runat="server" meta:resourcekey="LiteralResource14"> 
                Some features in the Report Wizard will perform a validation by running a query
                on the database. Although turning this feature off may boost performance of those
                pages, you must be aware that a non-detected bad syntax may break the report at
                run-time.</asp:Localize>
            </asp:Panel>

            <asp:Panel ID="pnlHelp4" runat="server" CssClass="popupControl" Width="300px" meta:resourcekey="pnlHelp4Resource1">
                <asp:Localize ID="Localize24" runat="server" meta:resourcekey="LiteralResource24">
                If the end user chooses to show number of records returned in a data section, 
                this message will be displayed, with [row count] replaced with actual number of records.
                </asp:Localize>
            </asp:Panel>

            <asp:Panel ID="pnlHelp5" runat="server" CssClass="popupControl" Width="300px" meta:resourcekey="pnlHelp5Resource1">
                <asp:Localize ID="Localize16" runat="server" meta:resourcekey="LiteralResource16">
                Reports will use this color template by default. The template can be changed in
                the Report Wizard and while building the report.</asp:Localize>
            </asp:Panel>

            <asp:Panel ID="pnlHelp6" runat="server" CssClass="popupControl" Width="300px" meta:resourcekey="pnlHelp6Resource1">
                <asp:Localize ID="Localize17" runat="server" meta:resourcekey="LiteralResource17">
                If checked, reports will display a message if the number of records reaches the maximum set here.
                </asp:Localize>
            </asp:Panel>

            <asp:Panel ID="pnlHelp10" runat="server" CssClass="popupControl" Width="300px">
                <asp:Localize ID="Localize21" runat="server" meta:resourcekey="LiteralResource21">
                If checked, reports will display a message if no records are returned.
                </asp:Localize>
            </asp:Panel>

            <asp:Panel ID="pnlHelp11" runat="server" CssClass="popupControl" Width="300px">
                <asp:Localize ID="Localize26" runat="server" meta:resourcekey="LiteralResource26">
                This style will be used for row count message.
                </asp:Localize>
            </asp:Panel>

            <asp:Panel ID="pnlHelp7" runat="server" CssClass="popupControl" Width="300px" meta:resourcekey="pnlHelp7Resource1">
                <asp:Localize ID="Localize18" runat="server" meta:resourcekey="LiteralResource18">
                This setting limits the default number of rows displayed in a page of report.
                Enter a value between 1 and 100.</asp:Localize>
            </asp:Panel>
            
            <asp:Panel ID="pnlHelp8" runat="server" CssClass="popupControl" Width="300px" meta:resourcekey="pnlHelp8Resource1">
                <asp:Localize ID="Localize19" runat="server" meta:resourcekey="LiteralResource19">
                This setting limits the default number of rows displayed in a page of sub-report.
                Enter a value between 1 and 100.</asp:Localize>
            </asp:Panel>
            
            <asp:Panel ID="pnlHelp12" runat="server" CssClass="popupControl" Width="300px">
                <asp:Localize ID="Localize28" runat="server" meta:resourcekey="LiteralResource28">
                This setting determines the default page size for printable paging in reports.</asp:Localize>
            </asp:Panel>

            <asp:Panel ID="pnlHelp9" runat="server" CssClass="popupControl" Width="300px">
                <asp:Localize ID="Localize15" runat="server" meta:resourcekey="LiteralResource15">
                If checked, data type will be set for exported columns in Excel and every attempt will be made to keep formatting close to the actual report.
                </asp:Localize>
            </asp:Panel>
            
            <div id="divSaveAnimation">
                <asp:Button runat="server" ID="btnSaveAnimation" Style="display: none" />
                <ajaxToolkit:ModalPopupExtender runat="server" ID="ahSavePopup" BehaviorID="ahSavePopupBehavior"
                    TargetControlID="btnSaveAnimation" PopupControlID="pnlSaving" BackgroundCssClass="modalBackground"
                    DropShadow="False">
                </ajaxToolkit:ModalPopupExtender>
                <asp:Panel id="pnlSaving" CssClass="savePopup" runat="server" style="display:none;">
                    <table><tr><td>
                    <asp:Image ID="imgSave" runat="server" SkinID="imgSaving" EnableViewState="false" AlternateText="<%$ Resources:LogiAdHoc, SaveImageAltText %>" />
                    </td>
                    <td valign="middle">
                    <asp:Label ID="lblSaveText" CssClass="lblSavePopup" runat="server" EnableViewState="false" Text="<%$ Resources:LogiAdHoc, PleaseWait %>"/>
                    </td></tr></table>
                </asp:Panel>
            </div>
        
            <div id="divButtons" class="divButtons">
                <AdHoc:LogiButton ID="btnSave" runat="server" OnClick="Save_OnClick" ToolTip="<%$ Resources:LogiAdHoc, SaveTooltip %>"
                    Text="<%$ Resources:LogiAdHoc, Save %>" UseSubmitBehavior="false" />
                <asp:ValidationSummary ID="vsummary" runat="server" meta:resourcekey="vsummaryResource1" />
            </div>
            <br />
        </div>
    </form>
</body>
</html>
