<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="CascadingFilterList.aspx.vb" Inherits="LogiAdHoc.ahConfiguration_CascadingFilterList" Culture="auto" meta:resourcekey="PageResource1" UICulture="auto" %>

<%@ Register Assembly="System.Web.Extensions" Namespace="System.Web.UI" TagPrefix="asp" %>

<%@ Register TagPrefix="AdHoc" TagName="MainMenu" Src="~/ahControls/MainMenu.ascx" %>
<%@ Register TagPrefix="AdHoc" TagName="NavMenu" Src="~/ahControls/NavMenu.ascx" %>
<%@ Register TagPrefix="AdHoc" TagName="BreadCrumbTrail" Src="~/ahControls/BreadCrumbTrail.ascx" %>
<%@ Register TagPrefix="AdHoc" TagName="Search" Src="~/ahControls/Search.ascx" %>
<%@ Register TagPrefix="AdHoc" TagName="PagingControl" Src="../ahControls/PagingControl.ascx" %> 
<%@ Register TagPrefix="AdHoc" TagName="RecompileGrid" Src="~/ahControls/RecompileReportGrid.ascx" %>
<%@ Register TagPrefix="AdHoc" TagName="DatabaseControl" Src="~/ahControls/DatabaseControl.ascx" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Cascading Filter List</title>
    <link rel="shortcut icon" href="../ahImages/flav.ico" />

    <script language="JavaScript" type="text/javascript" src="../ahScripts/ahChange.js"></script>    
</head>
<body>
    <AdHoc:MainMenu ID="menu" runat="server" />
    
    <form id="form1" runat="server">
    <div id="submenu">
        <AdHoc:NavMenu ID="subnav" runat="server" />
    </div>
        <AdHoc:BreadCrumbTrail ID="bct" runat="server" Key="CascadingFilters" ParentLevels="0" />
        
        <asp:ScriptManager ID="ScriptManager1" runat="server"  />

        <script type="text/javascript">
            Sys.WebForms.PageRequestManager.getInstance().add_endRequest(EndRequestHandler);
            function EndRequestHandler(sender, args) {
                $find('ahSavePopupBehavior').hide();
            }
        </script>

<table class="limiting"><tr><td>
        <table class="gridForm"><tr><td>

	    <asp:UpdatePanel ID="UP1" UpdateMode="Conditional" runat="server">
            <ContentTemplate>&nbsp;
                <Adhoc:DatabaseControl ID="ucDatabaseControl" runat="server" ShowAllDatabasesOption="False" />
                <br /><br />
                        
                <div id="data_main">
                    <div id="activities">
                    <table width="100%" cellpadding="0" cellspacing="0">
                    <tr width="100%">
                    <td align="left" valign="top">
                        <AdHoc:LogiButton ID="btnNewFilter" runat="server" 
                            OnClick="NewFilter" Text="<%$ Resources:LogiAdHoc, AddWithSpaces %>"  />
                        <AdHoc:LogiButton ID="btnRemoveFilter" runat="server" 
                            OnClick="RemoveFilter" Text="<%$ Resources:LogiAdHoc, Delete %>" />
                    </td>
                    <td align="right" valign="top">
                        <AdHoc:Search ID="srch" runat="server" Title="Find Filters" meta:resourcekey="AdHocSearch" />        
                    </td>                        
                    </tr>
                    </table>
                    </div>
            <asp:GridView id="grdMain" runat="server" CssClass="grid" AllowSorting="True" 
                AutoGenerateColumns="False" AllowPaging="True" DataKeyNames="FilterID" OnRowDataBound="OnItemDataBoundHandler"
                OnSorting="OnSortCommandHandler" meta:resourcekey="grdMainResource1" >
                        <HeaderStyle CssClass="gridheader"></HeaderStyle>
                    <PagerTemplate>
                        <AdHoc:PagingControl id="pageCtrl" runat="server" OnGotoNextPage="ChangePageIndex" />
                    </PagerTemplate>
                    <PagerStyle HorizontalAlign="Center"></PagerStyle>
                    <RowStyle CssClass="gridrow" />
                    <AlternatingRowStyle CssClass="gridalternaterow" />
                    <Columns>
                            <asp:TemplateField>
                                <HeaderStyle Width="30px"></HeaderStyle>
                                <HeaderTemplate>
                                    <asp:Label ID="lblChkAll" runat="server" AssociatedControlID="CheckAll" Text="<%$ Resources:LogiAdHoc, SelectDeselectAll %>" CssClass="NoShow"></asp:Label>
                                    <asp:CheckBox runat="server" ToolTip="<%$ Resources:LogiAdHoc, SelectDeselectAll %>" ID="CheckAll">
                                    </asp:CheckBox>
                                </HeaderTemplate>
                                <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblChk" runat="server" AssociatedControlID="chk_Select" Text="<%$ Resources:LogiAdHoc, SelectDeselect %>" CssClass="NoShow"></asp:Label>
                                    <asp:CheckBox runat="server" ID="chk_Select" ToolTip="<%$ Resources:LogiAdHoc, SelectDeselect %>">
                                    </asp:CheckBox>
                                </ItemTemplate>
                            </asp:TemplateField>
                        <asp:BoundField DataField="FilterName" HeaderText="<%$ Resources:LogiAdHoc, CascadingFilter %>" SortExpression="Filtername" >
                        </asp:BoundField>
                        <asp:BoundField DataField="Description" HeaderText="<%$ Resources:LogiAdHoc, Description %>" >
                        </asp:BoundField>
                        <asp:TemplateField HeaderText="<%$ Resources:LogiAdHoc, Actions %>" meta:resourcekey="TemplateFieldResource2">
                            <HeaderStyle Width="50px" />
                            <ItemStyle HorizontalAlign="Center" />
                            <ItemTemplate>
                                <%--<asp:ImageButton ID="imgModify" SkinID="imgSingleAction" AlternateText="Modify Filter" ToolTip="Modify Filter"
                                    runat="server" OnCommand="ModifyFilter" meta:resourcekey="ModifyResource1" />--%>
                                        
                                <asp:Image ID="imgActions" AlternateText="<%$ Resources:LogiAdHoc, Actions %>" runat="server" 
                                        ToolTip="<%$ Resources:LogiAdHoc, Actions %>" ImageUrl="~/ahImages/arrowStep.gif" SkinID="imgActions" />
                                <asp:Panel ID="pnlActionsMenu" runat="server" CssClass="popupMenu" 
                                    HorizontalAlign="Left" Wrap="false" style="display:none;">
                                    
                                    <div id="divModify" runat="server" class="hoverMenuActionLink" >
                                        <asp:LinkButton ID="lnkModify" runat="server" OnCommand="ModifyFilter" Text="Modify Filter"
                                            meta:resourcekey="ModifyResource1" ToolTip="Modify Filter"></asp:LinkButton>
                                    </div>
                                       
                                    <div id="divDelete" runat="server" class="hoverMenuActionLink" visible="false">
                                        <asp:LinkButton ID="lnkDelete" runat="server" OnCommand="DeleteItem" Visible="false" 
                                            Text="Delete Filter" meta:resourcekey="DeleteResource1"></asp:LinkButton>
                                    </div>
 				                </asp:Panel>
                                <ajaxToolkit:HoverMenuExtender ID="hme2" runat="server"
                                    PopupControlID="pnlActionsMenu" PopupPosition="right" 
                                    TargetControlID="imgActions" PopDelay="25" />
                                    
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>
                </div>
            </ContentTemplate>
            <Triggers>
                <asp:AsyncPostBackTrigger ControlID="srch" EventName="DoSearch" />
<%--                <asp:AsyncPostBackTrigger ControlID="btnRemoveFilter" EventName="Click" />
                <asp:AsyncPostBackTrigger ControlID="btnRemoveFilter1" EventName="Click" />
--%>            </Triggers>
        </asp:UpdatePanel>
        
        
            <asp:Panel runat="server" CssClass="modalPopup" ID="ahPopupRecompile" style="display:none; width:750;">
                <asp:Panel ID="pnlDragHandleRecompile" runat="server" Style="cursor: hand;">
                    <div class="modalPopupHandle">
                        <table cellpadding="0" cellspacing="0" style="width: 100%"><tr><td>
                                <asp:Localize ID="Localize11" runat="server" Text="<%$ Resources:LogiAdHoc, ParameterDetails %>"></asp:Localize>
                            </td>
                            <td style="width: 20px;">
                                <%--<asp:ImageButton ID="imgClosePopupRecompile" runat="server" 
                                    OnClick="imgClosePopupRecompile_Click" CausesValidation="false"
                                    SkinID="imgbClose" ImageUrl="../ahImages/remove.gif" 
                                    AlternateText="<%$ Resources:LogiAdHoc, CloseWindow %>" />--%>
                        </td></tr></table>
                    </div>
                </asp:Panel>
                <div class="modalDiv">
                    <asp:UpdatePanel ID="upPopupRecompile2" runat="server">
                        <ContentTemplate>
                            The following reports will be affected by this change, would you like to continue?
                            <br /><br />
                            
                            <AdHoc:RecompileGrid runat="server" ID="grdRecompile" />
                            <br /><br />
                            
                            
                            <%--<AdHoc:LogiButton ID="btnRecompile" runat="server" CausesValidation="False"
                                UseSubmitBehavior="false" ToolTip="<%$ Resources:LogiAdHoc, SaveTooltip1 %>" OnClientClick="javascript: SetRebuilt();"
                                Text="Recompile Selected" OnClick="btnRecompile_Click" />
                            <AdHoc:LogiButton ID="btnCancelRecompile" runat="server" 
                                ToolTip="Cancel" Text="Cancel"
                                CausesValidation="False" OnClick="btnCancelRecompile_Click" />
                        
                            
                            <AdHoc:LogiButton ID="btnCloseRecompile" runat="server" Visible="false"
                                ToolTip="close" Text="Close"
                                CausesValidation="False" OnClick="btnCancelRecompile_Click" />--%>
                        
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
            </asp:Panel>
            
        
        <div id="divSaveAnimation">
            <asp:Button runat="server" ID="btnSaveAnimation" Style="display: none" />
            <ajaxToolkit:ModalPopupExtender runat="server" ID="ahSavePopup" BehaviorID="ahSavePopupBehavior"
                TargetControlID="btnSaveAnimation" PopupControlID="pnlSaving" BackgroundCssClass="modalBackground"
                DropShadow="False">
            </ajaxToolkit:ModalPopupExtender>
            <asp:Panel id="pnlSaving" CssClass="savePopup" runat="server" style="display:none;">
                <table><tr><td>
                <asp:Image ID="imgSave" runat="server" SkinID="imgSaving" EnableViewState="false" AlternateText="<%$ Resources:LogiAdHoc, SaveImageAltText %>" />
                </td>
                <td valign="middle">
                <asp:Label ID="lblSaveText" CssClass="lblSavePopup" runat="server" EnableViewState="false" Text="<%$ Resources:LogiAdHoc, PleaseWait %>"/>
                </td></tr></table>
            </asp:Panel>
        </div>
            
        </td></tr></table>
</td></tr></table>
    </form>
</body>
</html>
