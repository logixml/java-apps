<%@ Page Language="vb" AutoEventWireup="false" Codebehind="ObjectInfo.aspx.vb" Inherits="LogiAdHoc.ahConfiguration_ObjectInfo"
    Culture="auto" meta:resourcekey="PageResource1" UICulture="auto" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register TagPrefix="cc1" Assembly="LGXAHWCL" Namespace="LGXAHWCL" %>
<%@ Register TagPrefix="AdHoc" TagName="MainMenu" Src="~/ahControls/MainMenu.ascx" %>
<%@ Register TagPrefix="AdHoc" TagName="NavMenu" Src="~/ahControls/NavMenu.ascx" %>
<%@ Register TagPrefix="AdHoc" TagName="BreadCrumbTrail" Src="~/ahControls/BreadCrumbTrail.ascx" %>
<%@ Register TagPrefix="AdHoc" TagName="PagingControl" Src="~/ahControls/PagingControl.ascx" %>
<%@ Register Src="~/ahControls/ScrollableListBox.ascx" TagName="ScrollableListBox" TagPrefix="AdHoc" %>      
<%@ Register TagPrefix="AdHoc" TagName="RecompileGrid" Src="~/ahControls/RecompileReportGrid.ascx" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Data Object</title>
    <link rel="shortcut icon" href="../ahImages/flav.ico" />
    <script language="JavaScript" type="text/javascript" src="../ahScripts/ahChange.js"></script>

    <script src="../rdTemplate/yui/build/yahoo-dom-event/yahoo-dom-event.js" type="text/javascript"></script>
    <script src="../rdTemplate/yui/build/animation/animation-min.js" type="text/javascript"></script>
    <script src="../rdTemplate/yui/build/dragdrop/dragdrop-min.js" type="text/javascript"></script>
    <script language="JavaScript" type="text/javascript" src="../ahScripts/ahReorderGrid.js"></script>
    
    <%--<script type="text/javascript">
    YAHOO.util.Event.onContentReady("grdMain", function () {
        initializeTableDragAndDrop('grdMain',true);
    });
    </script>--%>
    
    <script type="text/javascript">
        window.onbeforeunload = AlertOnExit;
    </script>

    <script type="text/javascript">
        var SelectRowIndex = -1;
	    function SelectRow(e)
	    {
	        if(!e) e = window.event;//Added this for IE
    	    
		    var obj = e.srcElement;
		    if(!obj) obj = e.target;//Added this for Firefox
		    if (obj && obj.tagName=="TD") //this a table cell
		    {
		        //get a pointer to the tablerow
		        var row = obj.parentNode;
    		    
		        if (!e.ctrlKey && !e.shiftKey)
		        {
		            var table = row.parentNode;
		            for (var i=1;i<table.childNodes.length;i++) {
		                if (table.childNodes[i].tagName == "TR")//Added this for Firefox
		                {
    //		                var chk = table.childNodes[i].cells[0].firstChild;
    //		                if (chk.checked)
    //		                {
    //		                    chk.checked = false
                                if(i%2 == 1)
		                            table.childNodes[i].className="";
		                        else
		                            table.childNodes[i].className="gridalternaterow";
    //		                }
		                }
		            }
                    var oRows = document.getElementById('grdMain').getElementsByTagName('tr');
                    var l = oRows.length;
                    var chk;
                    var s;
                    for (i=2; i<=l; i++) {
	                    var o = pack(i.toString(10),2);
		                s = "grdMain_ctl" + o + "_chk_Select";
		                chk = document.getElementById(s);
		                if (chk!=null) chk.checked = false;
	                }
		        }
    		    
		        if (e.shiftKey && SelectRowIndex >= 0)
		        {
		            if (SelectRowIndex > row.rowIndex)
		            {
		                var table = row.parentNode;
		                for (var i=row.rowIndex;i<SelectRowIndex;i++) {
		                    var idx = i+1;
		                    var o = pack(idx.toString(10),2);
		                    var s = "grdMain_ctl" + o + "_chk_Select";
		                    //var chk = table.childNodes[i].cells[0].firstChild;
		                    var chk = document.getElementById(s);
		                    if(!chk.disabled)
		                    {
		                        chk.checked = true;
		                        table.childNodes[i].className="SelectedRow";
		                    }
		                }
		            }
		            else
		            {
		                var table = row.parentNode;
		                for (var i=row.rowIndex;i>SelectRowIndex;i--) {
		                    var idx = i+1;
		                    var o = pack(idx.toString(10),2);
		                    var s = "grdMain_ctl" + o + "_chk_Select";
		                    var chk = document.getElementById(s);
		                    //var chk = table.childNodes[i].cells[0].firstChild;
		                    if(!chk.disabled)
		                    {
		                        chk.checked = true;
		                        table.childNodes[i].className="SelectedRow";
		                    }
		                }
		            }
		        }
		        else
		        {
		            var idx = row.rowIndex+1;
		            var o = pack(idx.toString(10),2);
                    var s = "grdMain_ctl" + o + "_chk_Select";
                    var chk = document.getElementById(s);
		            //var chk = row.cells[0].firstChild;
		            if(!chk.disabled)
		            {
		                chk.checked = true;
		                if (chk.checked)
		                   row.className="SelectedRow";
		                else
		                {
		                    if(idx%2 == 0)
	                            row.className="";
	                        else
	                            row.className="gridalternaterow";
		                }
		            }
		        }
		        SelectRowIndex = row.rowIndex
		    }
	    }
	    function MouseOver(select, e)
	    {
	        if(!e) e = window.event;//Added this for IE
    	    
	        var obj = e.srcElement;
	        if(!obj) obj = e.target;//Added this for Firefox
		    if (obj && obj.tagName=="TD") //this a table cell
		    {
		        //get a pointer to the tablerow
		        var row = obj.parentNode;
		        var idx = row.rowIndex+1;
		        var o = pack(idx.toString(10),2);
                var s = "grdMain_ctl" + o + "_chk_Select";
                var chk = document.getElementById(s);
		        //var chk = row.cells[0].firstChild;
		        if (chk && !chk.disabled)
		        {
		            if (select && !chk.checked)
		            {
		                row.className="SelectedRow";
		            }
		            else
		            {
		                if (chk.checked)
		                {
		                   row.className="SelectedRow";
		                }
		                else
		                {
		                    if(idx%2 == 0)
	                            row.className="";
	                        else
	                            row.className="gridalternaterow";
		                }
		            }
		         }
		    }
	    }
	    function SelectRow_OnClick(chkVal, row)
	    {
	        if (chkVal)
	            row.className="SelectedRow";
	        else
	        {
	            var idx = row.rowIndex+1;
	            if(idx%2 == 0)
                    row.className="";
                else
                    row.className="gridalternaterow";
	        }
	    }
	    function SelectAll_OnClick(chkVal, idVal, row)
	    {
	        var oRows = document.getElementById('grdMain').getElementsByTagName('tr');
            var l = oRows.length;
            //var l = grdMain.rows.length;
            var e;
            var s;
            if (idVal.indexOf ('CheckAll') != -1) {
	            var bSet; 
	            // Check if main checkbox is checked, then select or deselect datagrid checkboxes
	            if (chkVal == true) {
		            bSet = true;
	            } else {
		            bSet = false;
	            }
	            // Loop through all elements
	            for (i=2; i<=l; i++) {
	                var o = pack(i.toString(10),2);
		            s = "grdMain_ctl" + o + "_chk_Select";
		            e = document.getElementById(s);
		            if (e!=null) e.checked = bSet;
	            }
	            var table = row.parentNode;
	            if (table.tagName == "TR") 
	                table = table.parentNode;
	            for (var i=1;i<table.childNodes.length;i++) {
	                if (table.childNodes[i].tagName == "TR")//Added this for Firefox
	                {
	                    if (bSet)
	                        table.childNodes[i].className="SelectedRow";
	                    else
	                    {
	                        if(i%2 == 0)
	                            table.childNodes[i].className="";
	                        else
	                            table.childNodes[i].className="gridalternaterow";
	                    }
	                }
	            }
            }
	    }
	    function SelectStart(e)
	    {
	        if(!e) e = window.event;//Added this for IE
	        var obj = e.srcElement;
		    if(!obj) obj = e.target;//Added this for Firefox
		    if (obj && obj.tagName=="TD") //this a table cell
		        return false;
        }
        function MouseDown(e)
        {
              if(!e) return false;
              if(e.shiftKey || e.ctrlKey) return false;
        }
    </script>
    
    <script type="text/javascript">
    
    function ModifyLibraryColumn(columnID) {
        var ih = document.getElementById("ihModifyColumnID");
        ih.value = columnID;
        
        var btn = document.getElementById("btnModifyLibraryColumn");
        btn.click();
    }
    function RemoveLibraryColumn(columnID) {
        var ih = document.getElementById("ihModifyColumnID");
        ih.value = columnID;
        
        var btn = document.getElementById("btnRemoveLibraryColumn");
        btn.click();
    }
    </script>
</head>
<body id="bod">
    <AdHoc:MainMenu ID="menu" runat="server" />
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server" />
    <div id="submenu">
        <AdHoc:NavMenu ID="subnav" runat="server" />
    </div>
        <asp:UpdatePanel ID="UPBct" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
        <AdHoc:BreadCrumbTrail ID="bct" runat="server" Key="DataObject" ParentLevels="1" />
            </ContentTemplate>
            <Triggers>
                <asp:AsyncPostBackTrigger ControlID="ddObjectID" EventName="SelectedIndexChanged" />
            </Triggers>
        </asp:UpdatePanel>

        <script type="text/javascript">
        Sys.WebForms.PageRequestManager.getInstance().add_endRequest(EndRequestHandler);
        function EndRequestHandler(sender, args) {
            $find('ahSavePopupBehavior').hide();
            RestorePopupPosition('ahModalPopupBehavior');
            noMessage = false;
            
            //initializeTableDragAndDrop('grdMain', true);
            document.body.style.cursor = '';
        }
        
        Type.registerNamespace('AHScripts');
        AHScripts.ResetCaret = function() {}
        AHScripts.ResetCaret.prototype = {
            resCaret: function() {
                lastCaretPos=null;
            }
        }
        AHScripts.ResetCaret.registerClass('AHScripts.ResetCaret');
        
        var panelUpdated = new AHScripts.ResetCaret();
        var postbackElement;
        Sys.WebForms.PageRequestManager.getInstance().add_beginRequest(beginRequest);
        Sys.WebForms.PageRequestManager.getInstance().add_pageLoaded(pageLoaded);
        
        function beginRequest(sender, args) {
            postbackElement = args.get_postBackElement();
            SavePopupPosition('ahModalPopupBehavior');
            document.body.style.cursor = 'wait';
        }
        function pageLoaded(sender, args) {
            panelUpdated.resCaret();
        }
        
        var lastCaretPos;

        // Places the symbol at the end of the text in the formula box.
        function appendSymbol( symbol )
        {
	        // Symbols can be only one character long.
	        if (symbol.length = 1)
	        {
		        var target = document.getElementById( "txtFormula" );
		        if (target != null)
		        {
			        //target.value += symbol;		
			        insertAtCaret(target, " " + symbol + " ");
			        document.getElementById('ihCalcColDirty').value=1;
		        }
	        }
	        else
	        {
		        alert("Invalid operator");
	        }
        }

        function appendColumn(txt,bAsIs)
        {
	        //var el = document.getElementById("ColumnPicker");
	        var target = document.getElementById( "txtFormula" );
	        if (target != null)
	        {
	            if (bAsIs) {
        	        insertAtCaret(target, " ( " + txt + " ) ");
        	        document.getElementById('ihCalcColDirty').value=1;
        	    }
        	    else {
        	        insertAtCaret(target, " �" + txt.replace(".", "�.�") + "� ");
        	        document.getElementById('ihCalcColDirty').value=1;
        	    }
        	}
        }

        function appendFunction()
        {
	        var target = document.getElementById( "txtFormula" );
		    if (target != null)
		    {
			    var ih = document.getElementById("ihCalcColFormula");
			    ih.value = target.value;
			    insertAtCaret(target, "�");
		    }
        }
        
        function addCalculatedColumn(idTo)
        {
	        var oName = document.getElementById( "Name" );
	        var oFormula = document.getElementById( "txtFormula" );
	        var oTarget = document.getElementById(idTo);
        	
	        //TODO: Ensure that the name is unique.
	        //TODO: Validate the formula.
        		
	        if (oName != null && oFormula != null && oTarget != null)
	        {
		        var oOption = document.createElement("OPTION");
		        oOption.text = oName.value;
		        oOption.value = oFormula.value;
		        oTarget.options.add(oOption);
	        }
        	
	        // NOTE: toggle is defined in Wizard.js.
	        toggle('CalcPanel');
        }

        // The functions below insert text at a specified position in a textarea.
        // The code below only works for IE, since it relies on createTextRange().
        // Inserts will occur at the end of the textarea for all other browsers.
        // See http://www.faqts.com/knowledge_base/view.phtml/aid/1052/fid/130

        // Adjusts the caret position.
        // Used in conjunction with the [storeCaret] function.
        function setCaretToEnd (el) {
          if (el.createTextRange) {
            var v = el.value;
            var r = el.createTextRange();
            r.moveStart('character', v.length);
            r.select();
          }
        }

        // Inserts text at the current location of a cursor in an html form element.
        // Used in conjunction with the [storeCaret] function.
        function insertAtCaret(el, txt) {
          if (lastCaretPos) {
	        el.range = lastCaretPos;
            el.range.text = el.range.text.charAt(el.range.text.length - 1) != ' ' ? txt : txt + ' ';
            el.range.select();
          }
          else {
            insertAtEnd(el, txt);
          }
        }

        // Stores the current location of a cursor in an html form element.
        function storeCaret() {
          var el = document.getElementById( "txtFormula" );
          if (el != null) {
              if (el.createTextRange)
                lastCaretPos = document.selection.createRange().duplicate();
          }
        }

        // Inserts text at the end of an html form element.
        function insertAtEnd(el, txt) {
          el.value += txt;
          setCaretToEnd (el);
        }
        
        function ApplyChangesToCalcColumn() {
            var ih = document.getElementById('ihCalcColDirty');
            if (ih)
            {
                if (ih.value == 1) {
                    if(!confirm('You may lose any unsaved changes to this column.\n\nAre you sure you want to continue?'))
                        return false;
                    else
                        ih.value = 0;
                }
            }    
            return true;
        }
        </script>
        <script type="text/javascript">
            //var focusEl = "";
            function setFunctionParameterValue(txt,bAsIs)
            {
    //            if (focusEl == "") {
    //                var ih = document.getElementById("ihDefTxtCtrl");
    //                focusEl = ih.value;
    //            }
                var ih = document.getElementById("ihDefTxtCtrl");
                if (ih.value != "") {
                    var focusTxt = document.getElementById(ih.value);
                    if (focusTxt != null)
                    {
                        if (bAsIs) {
                            focusTxt.value = txt;
                        } else {
                            focusTxt.value = "�" + txt.replace(".", "�.�") + "�";
                        }
                    }
                }
            }
            
            function setFocusEl(el)
            {
                //focusEl = el;
                var ih = document.getElementById("ihDefTxtCtrl");
                ih.value = el;
            }
    //        function resetFocusEl()
    //        {
    //            focusEl = "";
    //        }
        </script>
        <div class="divForm">
            <asp:UpdatePanel ID="UPahDirty" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                    <input type="hidden" id="ahDirty" name="ahDirty" runat="server">
                </ContentTemplate>
            </asp:UpdatePanel>
            <input type="hidden" id="ahParentID" name="ahParentID" runat="server" />
            
            <input type="hidden" id="ahRecompiledFlag" name="ahRecompiledFlag" runat="server" />
            <table id="tbDBConn" runat="server" class="tbTB">
                <tr>
                    <td width="100px">
                        <asp:Localize ID="Localize8" runat="server" Text="<%$ Resources:LogiAdHoc, DatabaseConnection %>"></asp:Localize>
                    </td>
                    <td>
                        <asp:Label ID="lblDBConn" runat="server"></asp:Label>
                    </td>
                </tr>
            </table>
            <table>
                <tr>
                    <td width="100px">
                        <asp:Localize ID="Localize11" runat="server" Text="<%$ Resources:LogiAdHoc, SelectedDataObject %>"></asp:Localize>
                    </td>
                    <td>
                        <asp:DropDownList ID="ddObjectID" AutoPostBack="True" runat="server" meta:resourcekey="ddObjectIDResource1" />
                    </td>
                </tr>
            </table>
            <asp:UpdatePanel ID="UP1" runat="Server" UpdateMode="Conditional">
                <ContentTemplate>
                    <table>
                        <tr>
                            <td width="100px">
                                <asp:Localize ID="Localize1" runat="server" meta:resourcekey="LiteralResource1" Text="Friendly Name:"></asp:Localize></td>
                            <td>
                                <asp:TextBox ID="ObjectLabel" MaxLength="128" Width="250px" runat="server" meta:resourcekey="ObjectLabelResource1" />
                                <asp:RequiredFieldValidator ID="rtvObjectLabel" ControlToValidate="ObjectLabel" ErrorMessage="Friendly Name is required."
                                    runat="server" ValidationGroup="ColumnsPanel" meta:resourcekey="rtvObjectLabelResource1">*</asp:RequiredFieldValidator>
                                <asp:CustomValidator ID="cvObjectLabel" runat="server" ControlToValidate="ObjectLabel"
                                    EnableClientScript="False" ErrorMessage="Friendly Name cannot contain some special characters. Only alpha-numeric characters and _ are allowed in a name."
                                    meta:resourcekey="cvObjectLabelResource1" OnServerValidate="IsObjectLabelValid"
                                    ValidationGroup="ColumnsPanel">*</asp:CustomValidator>
                            </td>
                        </tr>
                        <tr>
                            <td width="100px"> 
                                <asp:Localize ID="Localize4" runat="server" meta:resourcekey="LiteralResource15" Text="Description:"></asp:Localize>
                            </td>
                            <td>
                                <asp:TextBox Rows="2" TextMode="MultiLine" ID="txtObjectExplanation" Width="250px" runat="server"
                                    meta:resourcekey="ExplanationResource1" />
                                <input type="hidden" id="ihObjectExplanationID" runat="server" />
                            </td>
                        </tr>
                        <tr id="TRHideObject" runat="server">
                            <td width="100px">
                                <label for="HideObject">
                                    <asp:Localize ID="Localize2" runat="server" meta:resourcekey="LiteralResource2" Text="Hide Data Object:"></asp:Localize></label></td>
                            <td >
                                <asp:CheckBox ID="HideObject" runat="server"></asp:CheckBox>
                            </td>
                        </tr>
                        <tr id="trPrimaryObject" runat="server">
                            <td width="100px" >
                                <label for="HideObject">
                                    <asp:Localize ID="Localize5" runat="server" meta:resourcekey="LiteralResource16" Text="Use as Primary Object:"></asp:Localize></label></td>
                            <td>
                                <asp:CheckBox ID="chkPrimaryObject" runat="server"></asp:CheckBox>
                            </td>
                            <td width="30">
                                <img id="help1" runat="server" src="../ahImages/iconHelp.gif" />
                                <ajaxToolkit:PopupControlExtender ID="PopupControlExtender1" runat="server" TargetControlID="help1" PopupControlID="pnlHelp1" Position="Bottom" BehaviorID="PopupControlExtender1" DynamicServicePath="" Enabled="True" ExtenderControlID="" />
                                <asp:Panel ID="pnlHelp1" runat="server" CssClass="popupControl" Width="300px" meta:resourcekey="pnlHelp1Resource1">
                                    <asp:Localize ID="Localize12" runat="server" meta:resourcekey="LiteralResource17" >
                                    Determines whether the data object will be shown in the initial display of the data objects selection list in the Report Builder.
                                    </asp:Localize>
                                </asp:Panel>
                            </td>
                        </tr>
                    </table>
                    <h2>
                        <asp:Localize ID="Localize3" runat="server" meta:resourcekey="LiteralResource3" Text="Data Object Columns"></asp:Localize></h2>
                    
                        
                    <%--<table onmousedown="javascript: return MouseDown(event);" onselectstart="javascript: return SelectStart(event);">--%>
                    <table>
                        <tr>
                            <td>
                                <div id="data_main">
                                    <div id="activities">
                                        <AdHoc:LogiButton ID="btnAddRow" OnClick="AddRow_OnClick" Text="Add a User-defined Column"
                                            runat="server" CausesValidation="False" meta:resourcekey="AddRowResource1" />
                                    </div>
                                    <asp:Button ID="btnFakeReorderRows" runat="server" CssClass="NoShow" OnClick="SaveRowOrder" />
                                    <AdHoc:DDGridView ID="grdMain" runat="server" AllowPaging="True" AllowSorting="True" AutoGenerateColumns="False"
                                        CssClass="grid" DataKeyNames="ColumnID" OnRowDataBound="OnItemDataBoundHandler" OnSorting="OnSortCommandHandler"
                                         meta:resourcekey="grdMainResource1" DDDivID="divDragHandle" PostbackButtonID="btnFakeReorderRows">
                                        <Columns>
                                            <asp:TemplateField>
                                                <HeaderTemplate>
                                                    <asp:Label ID="lblChkAll" runat="server" AssociatedControlID="CheckAll" Text="<%$ Resources:LogiAdHoc, SelectDeselectAll %>" CssClass="NoShow"></asp:Label>
                                                    <asp:CheckBox ID="CheckAll" runat="server" meta:resourcekey="CheckAllResource1" ToolTip="<%$ Resources:LogiAdHoc, SelectDeselectAll %>" />
                                                </HeaderTemplate>
                                                <ItemStyle HorizontalAlign="Center" />
                                                <ItemTemplate>
                                                    <input type="hidden" id="RowOrder" runat="server" />
                                                    <table cellpadding="0" cellspacing="0">
                                                        <tr>
                                                            <td>
                                                                <div id="divDragHandle" runat="server" class="dragHandle">
                                                                    <%--<asp:Image ID="imgDragHandle" runat="server" ImageUrl="../ahImages/bg-menu-main.png" Width="10px" Height="15px" />--%>
                                                                </div>
                                                            </td>
                                                            <td>
                                                                <asp:Label ID="lblChk" runat="server" AssociatedControlID="chk_Select" Text="<%$ Resources:LogiAdHoc, SelectDeselect %>" CssClass="NoShow"></asp:Label>
                                                                <asp:CheckBox ID="chk_Select" runat="server" ToolTip="<%$ Resources:LogiAdHoc, SelectDeselect %>"/>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Column Name" meta:resourcekey="TemplateFieldResource1">
                                                <ItemTemplate>
                                                    <input type="hidden" id="ColumnID" runat="server" />
                                                    <input type="hidden" id="ExplanationID" runat="server" />
                                                    <input type="hidden" id="hLinkURL" runat="server" />
                                                    <input id="ColumnOrder" runat="server" type="hidden" />
                                                    <asp:Label ID="ColumnName" runat="server" meta:resourcekey="ColumnNameResource1" />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Friendly Name" SortExpression="Description" meta:resourcekey="TemplateFieldResource2">
                                                <ItemTemplate>
                                                    <asp:TextBox ID="ColumnLabel" MaxLength="100" Width="200px" runat="server" meta:resourcekey="ColumnLabelResource1" />
                                                    <asp:RequiredFieldValidator ID="rtvColumnLabel" runat="server" ControlToValidate="ColumnLabel" ValidationGroup="ColumnsPanel"
                                                        ErrorMessage="Column Friendly Name is required." meta:resourcekey="rtvColumnLabelResource1">*</asp:RequiredFieldValidator>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Description" meta:resourcekey="TemplateFieldResource3">
                                                <ItemTemplate>
                                                    <asp:TextBox Rows="2" TextMode="MultiLine" ID="Explanation" Width="250px" runat="server"
                                                        meta:resourcekey="ExplanationResource1" />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Default Format" meta:resourcekey="TemplateFieldResource5">
                                                <ItemTemplate>
                                                    <asp:DropDownList ID="DefaultFormat" runat="server" meta:resourcekey="DefaultFormatResource1" />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Default Alignment" meta:resourcekey="TemplateFieldResource6">
                                                <ItemTemplate>
                                                    <asp:DropDownList ID="DefaultAlignment" runat="server" meta:resourcekey="DefaultAlignmentResource1">
                                                        <asp:ListItem Value="Left" meta:resourcekey="ListItemResource1">Left</asp:ListItem>
                                                        <asp:ListItem Value="Right" meta:resourcekey="ListItemResource2">Right</asp:ListItem>
                                                        <asp:ListItem Value="Center" meta:resourcekey="ListItemResource3">Center</asp:ListItem>
                                                    </asp:DropDownList>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Hidden" meta:resourcekey="TemplateFieldResource7">
                                                <ItemTemplate>
                                                    <asp:CheckBox ID="chkColHidden" runat="server" />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="<%$ Resources:LogiAdHoc, Actions %>" >
                                                <HeaderStyle Width="50px" />
                                                <ItemStyle HorizontalAlign="Center" />
                                                <ItemTemplate>
                                                    <asp:Image ID="imgActions" AlternateText="<%$ Resources:LogiAdHoc, Actions %>" runat="server" 
                                                        ToolTip="<%$ Resources:LogiAdHoc, Actions %>" ImageUrl="~/ahImages/arrowStep.gif" SkinID="imgActions" />
                                                    <asp:Panel ID="pnlActionsMenu" runat="server" CssClass="popupMenu" 
                                                        HorizontalAlign="Left" Wrap="false" style="display:none;">
                                                        <div id="divModify" runat="server" class="hoverMenuActionLink" >
                                                            <asp:LinkButton ID="lnkModify" runat="server" OnCommand="ManageColumn" Text="Modify Column"
                                                                CausesValidation="False" meta:resourcekey="ModifyResource1"></asp:LinkButton>
                                                        </div>
                                                        <div id="divDelete" runat="server" class="hoverMenuActionLink" >
                                                            <asp:LinkButton ID="lnkDelete" runat="server" OnCommand="ManageColumn" Text="Remove Column"
                                                                CausesValidation="False" meta:resourcekey="DeleteResource1" ></asp:LinkButton>
                                                        </div>
                                                        <div id="divDependencies" runat="server" class="hoverMenuActionLink" >
                                                            <asp:LinkButton ID="lnkDependencies" runat="server" OnCommand="ManageColumn" Text="<%$ Resources:LogiAdHoc, ViewDependencyInfo %>"
                                                                CausesValidation="false"></asp:LinkButton>
                                                        </div>
                 				                    </asp:Panel>
                                                    <ajaxToolkit:HoverMenuExtender ID="hme2" runat="server"
                                                        PopupControlID="pnlActionsMenu" PopupPosition="right" 
                                                        TargetControlID="imgActions" PopDelay="25" />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                        <PagerStyle HorizontalAlign="Center" />
                                        <HeaderStyle CssClass="gridheader" />
                                        <PagerTemplate>
                                            <AdHoc:PagingControl ID="pageCtrl" runat="server" OnGotoNextPage="ChangePageIndex" />
                                        </PagerTemplate>
                                        <RowStyle CssClass="gridrow" />
                                        <AlternatingRowStyle CssClass="gridalternaterow" />
                                    </AdHoc:DDGridView>
                                </div>
                            </td>
                            <td width="30">
                                <div id="divColumnMove" runat="server">
                                    <asp:ImageButton ID="imgColMoveup" runat="server" AlternateText="Move Columns Up"
                                        CausesValidation="False" CssClass="mslbutton" ImageUrl="../ahImages/arrowUp.gif"
                                        meta:resourcekey="imgMoveupResource1" OnCommand="ColumnMoveUp" ToolTip="Move Columns Up" />
                                    <asp:ImageButton ID="imgColumnsMovedown" runat="server" AlternateText="Move Columns Down"
                                        CausesValidation="False" CssClass="mslbutton" ImageUrl="../ahImages/arrowDown.gif"
                                        meta:resourcekey="imgMovedownResource1" OnCommand="ColumnsMoveDown" ToolTip="Move Columns Down" />
                                </div>
                            </td>
                        </tr>
                    </table>

                    <div id="divButtons" class="divButtons">
                        <AdHoc:LogiButton ID="btnSave" runat="server" OnClick="Save_OnClick"
                            UseSubmitBehavior="false" ToolTip="<%$ Resources:LogiAdHoc, SaveTooltip1 %>"
                            Text="<%$ Resources:LogiAdHoc, Save %>" ValidationGroup="ColumnsPanel" />
                        <AdHoc:LogiButton ID="btnCancel" runat="server" OnClick="Cancel_OnClick"
                            ToolTip="<%$ Resources:LogiAdHoc, BackToObjectsTooltip %>" Text="<%$ Resources:LogiAdHoc, BackToObjects %>"
                            CausesValidation="False" />
                        <asp:ValidationSummary ID="vsummary" runat="server" ValidationGroup="ColumnsPanel" meta:resourcekey="vsummaryResource1" />
                        <asp:Label ID="lblSemicolon" runat="server" OnPreRender="lblSemicolon_PreRender" />
                    </div>
<br />
                                            
                    <asp:Button runat="server" ID="Button1" Style="display: none" />
                    <ajaxToolkit:ModalPopupExtender runat="server" ID="ahModalPopup" BehaviorID="ahModalPopupBehavior"
                        TargetControlID="Button1" PopupControlID="ahPopup" BackgroundCssClass="modalBackground"
                        DropShadow="False" PopupDragHandleControlID="pnlDragHandle" RepositionMode="None">
                    </ajaxToolkit:ModalPopupExtender>
                    
                    <asp:Panel runat="server" CssClass="modalPopup" ID="ahPopup" Style="display: none; width: 806px;">
                        <asp:Panel ID="pnlDragHandle" runat="server" Style="cursor: hand;">
                        <div class="modalPopupHandle">
                            <table cellpadding="0" cellspacing="0" style="width: 800px"><tr><td>
                                    <asp:Localize ID="PopupHeader" runat="server" meta:resourcekey="LiteralResource4" Text="User-defined Column Details"></asp:Localize>
                                </td>
                                <td style="width: 20px;">
                                    <asp:ImageButton ID="imgClosePopup" runat="server" 
                                        OnClick="imgClosePopup_Click" CausesValidation="false"
                                        SkinID="imgbClose" ImageUrl="../ahImages/remove.gif" 
                                        AlternateText="<%$ Resources:LogiAdHoc, CloseWindow %>" />
                            </td></tr></table>
                        </div>
                        </asp:Panel>
                        <div class="modalDiv">
                            <asp:UpdatePanel ID="UPCalcColumn" runat="server" UpdateMode="Conditional" RenderMode="inline">
                                <ContentTemplate>
                                    <input type="hidden" id="ihCalculatedColumnID" runat="server" />
                                    <input type="hidden" id="ihCalcColFormula" runat="server" />
                                    <input type="hidden" id="ihCalcColDirty" runat="server" />
                                    <asp:Localize ID="LocFunctions" runat="server" Text="Functions:" meta:resourceKey="locFunc"></asp:Localize>
                                    <br />
                                    <div id="divFunctionsMenu" runat="server">
                                    </div>
                                    <div id="divMenuFunctions" runat="server" style="width: 100px; height: 10px;" >
                                    </div>
                                    <div id="divMoreMenu" >
                                        <asp:Panel CssClass="popupMenu" ID="PopupMenu" runat="server" style="display: none;">
                                            <%--<div style="border:1px outset white;padding:2px;">--%>
                                                <div><asp:LinkButton ID="btnCreateReport" runat="server" Text="<%$ Resources:LogiAdHoc, Res_And %>" 
                                                             OnClientClick="appendSymbol('AND');return false;" CausesValidation="false"/></div>
                                                <div><asp:LinkButton ID="btnCreateDashboard" runat="server" Text="<%$ Resources:LogiAdHoc, Res_Or %>" 
                                                            OnClientClick="appendSymbol('OR');return false;" CausesValidation="false"/></div>
                                                <div><asp:LinkButton ID="btnCreateFolder" runat="server" Text="<%$ Resources:LogiAdHoc, Res_Not %>" 
                                                            OnClientClick="appendSymbol('NOT');return false;" CausesValidation="false"/></div>
                                            <%--</div>--%>
                                        </asp:Panel>
                                        <ajaxToolkit:HoverMenuExtender ID="hme" runat="Server"
                                            PopupControlID="PopupMenu"
                                            PopupPosition="Bottom" 
                                            TargetControlID="spBtnMore"
                                            PopDelay="25" />
                                    </div>
                                    <table>
                                        <tr>
                                            <td valign="top" width="250px">
                                                <asp:Localize ID="locColumn" runat="server" Text="Columns:" meta:resourceKey="locColumn"></asp:Localize>
                                                <br />
                                                <div class="divColumnTreeView" style="width:240px; height: 290px;">
                                                    <cc1:DataSourceTreeView ID="trvColumns" runat="server" meta:resourcekey="trvColumnsResource1">
                                                    </cc1:DataSourceTreeView>
                                                </div>
                                                <input type="hidden" id="ihModifyColumnID" runat="server" />
                                                <br />
                                                <%--<asp:Button ID="btnModifyLibraryColumn" runat="server" CssClass="NoShow" CausesValidation="false" 
                                                     OnClick="ModifyLibraryColumn" meta:resourcekey="btnModifyLibraryColumnResource1" />
                                                <asp:Button ID="btnRemoveLibraryColumn" runat="server" CssClass="NoShow" CausesValidation="false" 
                                                     OnClick="RemoveLibraryColumn" meta:resourcekey="btnRemoveLibraryColumnResource1" />--%>
                                            </td>
                                            <td valign="top" width="480px" style="padding-top:3px;">
                                                <table>
                                                    <tr>
                                                        <td>
                                                            <asp:Label ID="locName" runat="server" Text="Name:" AssociatedControlID="txtName" meta:resourceKey="locName"></asp:Label>
                                                        </td>
                                                        <td>
                                                            <input id="txtName" runat="server" size="41" type="text" />
                                                            <asp:RequiredFieldValidator ID="rtvCalcName" ValidationGroup="Calculation" ControlToValidate="txtName" 
                                                                ErrorMessage="User-defined Column Name is required." runat="server" meta:resourcekey="rtvCalcNameResource1">*</asp:RequiredFieldValidator>
                                                            <asp:CustomValidator ID="cvUniqueCalcName" ValidationGroup="Calculation" runat="server"
                                                                ErrorMessage="Another calculation already uses this name. Please choose a new name."
                                                                ControlToValidate="txtName" OnServerValidate="IsCalcNameUnique" meta:resourcekey="cvUniqueCalcNameResource1">*</asp:CustomValidator>
                                                        </td>
                                                     </tr>
                                                </table>
                                                <br />
                                                <asp:Label ID="locDef" runat="server" Text="Definition:" AssociatedControlID="txtFormula" meta:resourceKey="locDef"></asp:Label>
                                                <br />
                                                <textarea id="txtFormula" runat="server" cols="92" rows="5" 
                                                    onclick="storeCaret()" onkeyup="storeCaret()" >
                                                </textarea>
                                                <br />
                                                <%--<textarea id="txtReplaceCalcColDef" runat="server" class="NoShow" />--%>
                                                <div style="float:right; padding-top:5px;">
                                                    <AdHoc:LogiButton ID="btnTestCalcColumn" runat="server" Text=" Test " 
                                                        OnClick="TestColumn_OnClick" ValidationGroup="Calculation" meta:resourcekey="LogiButton6Resource1" />
                                                </div>
                                                <br />
                                                <br />
                                                <asp:Localize ID="locOp" runat="server" Text="Operators:" meta:resourceKey="locOp"></asp:Localize>
                                                <AdHoc:LogiButton ID="btnP" runat="server" OnClientClick="appendSymbol('+');return false;" Text="+" CausesValidation="false" UseSubmitBehavior="False" />
                                                <AdHoc:LogiButton ID="btnM" runat="server" OnClientClick="appendSymbol('-');return false;" Text="-" CausesValidation="false" UseSubmitBehavior="False" />
                                                <AdHoc:LogiButton ID="btnL" runat="server" OnClientClick="appendSymbol('*');return false;" Text="x" CausesValidation="false" UseSubmitBehavior="False" />
                                                <AdHoc:LogiButton ID="btnD" runat="server" OnClientClick="appendSymbol('/');return false;" Text="/" CausesValidation="false" UseSubmitBehavior="False" />
                                                <AdHoc:LogiButton ID="btnO" runat="server" OnClientClick="appendSymbol('(');return false;" Text="(" CausesValidation="false" UseSubmitBehavior="False" />
                                                <AdHoc:LogiButton ID="btnC" runat="server" OnClientClick="appendSymbol(')');return false;" Text=")" CausesValidation="false" UseSubmitBehavior="False" />
                                                <span id="spBtnMore" runat="server"><AdHoc:LogiMenuBox ID="btnMore" runat="server" Text="More" meta:resourceKey="btnMore" /></span>
                                                <br />
                                                <br />
                                                <asp:Label ID="locType" runat="server" Text="Type:" AssociatedControlID="ddlDataType" meta:resourceKey="locType"></asp:Label>
                                                <asp:DropDownList ID="ddlDataType" runat="server">
                                                     <asp:ListItem Value="-1" Text="<%$ Resources:LogiAdHoc, Automatic %>" />
                                                     <asp:ListItem Value="2" Text="<%$ Resources:LogiAdHoc, Text %>" />
                                                     <asp:ListItem Value="1" Text="<%$ Resources:LogiAdHoc, Number %>" />
                                                     <asp:ListItem Value="4" Text="<%$ Resources:LogiAdHoc, Res_Date %>" />
                                                </asp:DropDownList>&nbsp;  
                                                <AdHoc:LogiButton ID="btnDetermineType" runat="server" Text="Determine Type" 
                                                    meta:resourcekey="LogiButton5Resource1" ValidationGroup="Calculation" 
                                                    OnClick="btnDetermineType_OnClick" />
                                                <br />
                                                <br />
                                                <%--<table>
                                                     <tr>
                                                        <td colspan="2">
                                                            <br />
                                                            <AdHoc:LogiButton ID="btnSaveCalc" runat="server" Text="Save" OnClick="SaveCalculation" 
                                                                meta:resourcekey="LogiButton3Resource1" ValidationGroup="Calculation"/>
                                                            <AdHoc:LogiButton ID="btnNewCalc" runat="server" Text=" New " OnClick="NewCalculation" 
                                                                meta:resourcekey="LogiButton2Resource1" CausesValidation="false" UseSubmitBehavior="false"/>
                                                        </td>
                                                    </tr>
                                                </table>--%>
                                                 
                                                <asp:ValidationSummary ID="ValidationSummary5" runat="server" ValidationGroup="Calculation" meta:resourcekey="ValidationSummary5Resource1" />
                                                <%--Adding this label as a temporary workaround to fix a bug in AJAXcontroltoolkit.--%>
                                                <asp:Label ID="lblSemicolon5" runat="server" OnPreRender="lblSemicolon_PreRender" />
                                                <ul class="validation_error" id="calcErrorList" runat="server">
                                                    <li>
                                                        <asp:Label ID="calcError" runat="server" meta:resourcekey="calcErrorResource1"></asp:Label>
                                                    </li>
                                                </ul>
                                            </td>
                                        </tr>
                                    </table>
                                    <table width="100%">
                                        <tr>
                                            <td align="left">
                                                <AdHoc:LogiButton ID="btnSaveItem" OnClick="SaveItem_OnClick" runat="server"
                                                    Text="<%$ Resources:LogiAdHoc, OKWithSpaces %>" ValidationGroup="DetailPanel" UseSubmitBehavior="false" />
                                                <AdHoc:LogiButton ID="btnCancelItem" OnClick="CancelItem_OnClick" runat="server"
                                                    Text="<%$ Resources:LogiAdHoc, Cancel %>" CausesValidation="False" />
                                            </td>
                                        </tr>
                                    </table>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        <%--<asp:UpdatePanel ID="upPopup" runat="server">
                            <ContentTemplate>
                                <input id="ColumnID_pnl" type="hidden" runat="server" />
                                <table class="tbTB">
                                    <tr>
                                        <td width="125">
                                            <asp:Localize ID="Localize5" runat="server" meta:resourcekey="LiteralResource5" Text="Column Name:"></asp:Localize></td>
                                        <td>
                                            <asp:TextBox ID="ColumnName_pnl" MaxLength="100" Width="200px" runat="server" meta:resourcekey="ColumnName_pnlResource1" />
                                            <asp:RequiredFieldValidator ID="rtvColumnName_pnl" ValidationGroup="DetailPanel"
                                                runat="server" ErrorMessage="Column Name is required." ControlToValidate="ColumnName_pnl"
                                                meta:resourcekey="rtvColumnName_pnlResource1">*</asp:RequiredFieldValidator>
                                            <asp:CustomValidator ID="cvValidName" ValidationGroup="DetailPanel" runat="server"
                                                EnableClientScript="False" ErrorMessage="This column name already exists. Please enter another name."
                                                ControlToValidate="ColumnName_pnl" OnServerValidate="IsNameValid" meta:resourcekey="cvValidNameResource1">*</asp:CustomValidator>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="125">
                                            <asp:Localize ID="Localize7" runat="server" meta:resourcekey="LiteralResource7" Text="Definition:"></asp:Localize></td>
                                        <td>
                                            <textarea id="Formula" rows="6" cols="70" onclick="storeCaret()" onkeyup="storeCaret()"
                                                runat="server"></textarea>
                                            <asp:RequiredFieldValidator ID="rtvValidFormula" ValidationGroup="DetailPanel" runat="server"
                                                ErrorMessage="Column Definition is required." ControlToValidate="Formula" meta:resourcekey="rtvValidFormulaResource1">*</asp:RequiredFieldValidator>
                                            <asp:CustomValidator ID="cvValidFormula" ValidationGroup="DetailPanel" runat="server"
                                                EnableClientScript="False" ErrorMessage="This column definition is not valid."
                                                ControlToValidate="Formula" OnServerValidate="IsDefinitionValid" meta:resourcekey="cvValidFormulaResource1">*</asp:CustomValidator>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td valign="top">
                                            <asp:Localize ID="Localize8" runat="server" meta:resourcekey="LiteralResource8" Text="Operators:"></asp:Localize>
                                            <div id="OperatorButtons">
                                                <button onclick="appendSymbol(this.value)" value="+">
                                                    +</button>
                                                <button onclick="appendSymbol(this.value)" value="-">
                                                    -</button>
                                                <button onclick="appendSymbol(this.value)" value="*">
                                                    *</button>
                                                <button onclick="appendSymbol(this.value)" value="/">
                                                    /</button>
                                                <button onclick="appendSymbol(this.value)" value="(">
                                                    (</button>
                                                <button onclick="appendSymbol(this.value)" value=")">
                                                    )</button>
                                            </div>
                                        </td>
                                        <td>
                                            <asp:Localize ID="Localize9" runat="server" meta:resourcekey="LiteralResource9" Text="Columns:"></asp:Localize>
                                            <div id="ColumnSelector">
                                                <AdHoc:ScrollableListBox ID="ColumnPicker" runat="server" DblClickFunction="appendColumn(this);"
                                                    MultipleSelection="True" SelectHeight="100" SelectWidth="300">
                                                </AdHoc:ScrollableListBox>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="125">
                                            <asp:Localize ID="Localize10" runat="server" meta:resourcekey="LiteralResource10"
                                                Text="Data Type:"></asp:Localize></td>
                                        <td>
                                            <asp:DropDownList ID="DataType_pnl" runat="server" meta:resourcekey="DataType_pnlResource1" />
                                        </td>
                                    </tr>
                                </table>
                                <br />
                                <table width="100%">
                                    <tr>
                                        <td align="left">
                                            <AdHoc:LogiButton ID="btnSaveItem" OnClick="SaveItem_OnClick" runat="server"
                                                Text="<%$ Resources:LogiAdHoc, OKWithSpaces %>" ValidationGroup="DetailPanel" UseSubmitBehavior="false" />
                                            <AdHoc:LogiButton ID="btnCancelItem" OnClick="CancelItem_OnClick" runat="server"
                                                Text="<%$ Resources:LogiAdHoc, Cancel %>" CausesValidation="False" />
                                        </td>
                                        <td align="right">
                                            <AdHoc:LogiButton ID="TestColumn" runat="server" meta:resourcekey="TestColumnResource1"
                                                OnClick="TestColumn_OnClick" Text="Validate Column" ValidationGroup="DetailPanel" />
                                        </td>
                                    </tr>
                                </table>
                                <ul id="calcErrorList" runat="server" class="validation_error">
                                    <li>
                                        <asp:Label ID="calcError" runat="server" meta:resourcekey="calcErrorResource1"></asp:Label>
                                    </li>
                                </ul>
                                <asp:ValidationSummary ID="ValidationSummary1" ValidationGroup="DetailPanel" runat="server"
                                    meta:resourcekey="ValidationSummary1Resource1" />
                                <p id="pCalcColumnValidationDone" runat="server">
                                    <asp:Label ID="lblCalcColumnValidationDone" runat="server"></asp:Label>
                                </p>
                            </ContentTemplate>
                        </asp:UpdatePanel>--%>
                        </div>
                        <asp:UpdatePanel ID="UPShowDataPopup" runat="server" UpdateMode="Conditional" RenderMode="inline">
                            <ContentTemplate>
                                <asp:Button runat="server" ID="btnShowDataPopup" Style="display: none" 
                                    CausesValidation="False" meta:resourcekey="btnShowDataPopupResource1"/>
                                <asp:Button runat="server" ID="Button3" Style="display: none" meta:resourcekey="Button1Resource1" />
                                <ajaxToolkit:ModalPopupExtender runat="server" ID="ahShowDataPopup" BehaviorID="ahShowDataPopupBehavior"
                                    TargetControlID="btnShowDataPopup" PopupControlID="pnlShowDataPopup" BackgroundCssClass="modalBackground" 
                                    Enabled="True">
                                </ajaxToolkit:ModalPopupExtender>
                                
                                <asp:Panel runat="server" CssClass="modalPopup" ID="pnlShowDataPopup" Style="display: none; width: 756px;" meta:resourcekey="ahPopupResource1">
                                    <asp:Panel ID="pnlDragHandle2" runat="server" meta:resourcekey="pnlDragHandleResource1">
                                    <div class="modalPopupHandle" style="width: 750px; cursor: default;">
                                        <table cellpadding="0" cellspacing="0" style="width: 100%;"><tr><td>
                                            <asp:UpdatePanel ID="UPShowDataHeader" runat="server" UpdateMode="Conditional" RenderMode="Inline">
                                                <ContentTemplate>
                                                    <asp:Label ID="lblShowDataHeader" runat="server" Text="Data" ></asp:Label>
                                                </ContentTemplate>
                                            </asp:UpdatePanel>
                                            </td>
                                            <td style="width: 20px;">
                                                <asp:ImageButton ID="imgClosePopup2" runat="server" 
                                                    OnClick="imgClosePopup2_Click" CausesValidation="False"
                                                    SkinID="imgbClose" ImageUrl="../ahImages/remove.gif" 
                                                    AlternateText="<%$ Resources:LogiAdHoc, CloseWindow %>" />
                                        </td></tr></table>
                                    </div>
                                    </asp:Panel>
                                    <div class="modalDiv">
                                    <div id="divPreviewInfo" runat="server">
                                    <p class="info">
                                        <asp:Localize ID="localize10" runat="server" Text="<%$ Resources:Errors, Wrn_LimitedPreview %>"></asp:Localize>
                                    </p>
                                    </div>
                                    
                                    <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional" RenderMode="inline">
                                        <ContentTemplate>
                                            <p></p>
                                            <div class="showDataPopup" style="width: 725px; overflow: auto;">
                                                <asp:GridView ID="grdData" runat="server" CssClass="grid" 
                                                    meta:resourcekey="grdDataResource1" >
                                                    <HeaderStyle CssClass="gridheader"></HeaderStyle>
                                                    <RowStyle CssClass="gridrow" Height="29px" Wrap="false"/>
                                                    <AlternatingRowStyle CssClass="gridalternaterow"  Height="29px" Wrap="false" ></AlternatingRowStyle>
                                                </asp:GridView>
                                            </div>
                                            <div id="divDataError" runat="server">
                                                <ul class="validation_error" id="UlDatError" runat="server">
                                                    <li>
                                                        <asp:Label ID="lblDataError" runat="server" ></asp:Label>
                                                    </li>
                                                </ul>
                                            </div>
                                            <div id="divValidationMessage" runat="server">
                                                <asp:Label ID="lblTestValidationMessage" runat="server"></asp:Label>
                                            </div>
                                            <p></p>
                                            <!-- Buttons -->
                                            <table>
                                                <tr>
                                                    <td colspan="2">
                                                        <AdHoc:LogiButton ID="btnShowDataClose" OnClick="btnShowDataClose_OnClick"
                                                            runat="server" Text="Close" CausesValidation="False" meta:resourcekey="btnShowDataCancelResource1" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                    </div>
                                </asp:Panel>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                        <asp:UpdatePanel Id="UPCalcColAddFunction" runat="server" UpdateMode="Conditional" RenderMode="inline">
                            <ContentTemplate>
                                <asp:Button runat="server" ID="btnFakeCalcColFuncDetails" Style="display: none" meta:resourcekey="btnFakeParamDetailsResource1" />
                                <ajaxToolkit:ModalPopupExtender runat="server" ID="mpeCalcColFuncDetails" BehaviorID="mpeCalcColFuncDetailsBehavior"
                                    TargetControlID="btnFakeCalcColFuncDetails" PopupControlID="pnlCalcColFuncDetails" BackgroundCssClass="modalBackground" >
                                </ajaxToolkit:ModalPopupExtender>
                                <asp:Panel runat="server" CssClass="modalPopup" ID="pnlCalcColFuncDetails" Style="display: none; width: 736;" meta:resourcekey="pnlParamDetailsResource1">
                                    <asp:Panel ID="pnlDragHandle9" runat="server" meta:resourcekey="pnlDragHandle8Resource1">
                                        <div class="modalPopupHandle" style="width: 730px; cursor: default;">
                                            <table cellpadding="0" cellspacing="0" style="width: 100%"><tr><td>
                                                    <asp:Label ID="lblCalcColFuncDetails" runat="server" />
                                                </td>
                                                <td style="width: 20px;">
                                                    <asp:ImageButton ID="imgClosePopup9" runat="server" 
                                                        OnClick="imgClosePopup9_Click" CausesValidation="False"
                                                        SkinID="imgbClose" ImageUrl="../ahImages/remove.gif" 
                                                        AlternateText="<%$ Resources:LogiAdHoc, CloseWindow %>" />
                                            </td></tr></table>
                                        </div>
                                    </asp:Panel>
                                    <div id="divCalcColumnFuncDetails" runat="server" class="modalDiv">
                                        <asp:UpdatePanel ID="UpdatePanel16" runat="server" UpdateMode="COnditional" RenderMode="inline">
                                            <ContentTemplate>
                                                <p>
                                                    <asp:Label ID="lblFunctionInfoText" runat="server" />
                                                </p>
                                                <table>
                                                    <tr>
                                                        <td valign="top" width="200px">
                                                            <div class="divColumnTreeView" style="width:190px; height: 200px;" >
                                                                <cc1:DataSourceTreeView ID="trvCalcFuncColumns" runat="server" meta:resourcekey="trvColumnsResource1">
                                                                </cc1:DataSourceTreeView>
                                                            </div>
                                                        </td>
                                                        <td valign="top">
                                                            <input type="hidden" id="ihDefTxtCtrl" runat="server" />
                                                             <div style="padding: 4px; margin-right:10px; height: 200px; overflow: auto;">
                                                                <asp:GridView ID="grdCalcColParams" runat="server" AutoGenerateColumns="False" 
                                                                    OnRowDataBound="OnRowDataBoundHandler" GridLines="None" ShowFooter="False" 
                                                                    ShowHeader="False" CellPadding="5" CellSpacing="5" >
                                                                    <Columns>
                                                                        <asp:TemplateField HeaderText="Parameter">
                                                                            <ItemTemplate>
                                                                                <input type="hidden" id="ihParameterID" runat="server" />
                                                                                <input type="hidden" id="ihParameterType" runat="server" />
                                                                                <asp:Label ID="lblParamName" runat="server"></asp:Label>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <%--<asp:TemplateField HeaderText="ValueType">
                                                                            <ItemTemplate>
                                                                                <asp:DropDownList ID="ddlValueType" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlValueType_SelectedIndexChanged">
                                                                                    <asp:ListItem Text="Column" Value="1"></asp:ListItem>
                                                                                    <asp:ListItem Text="Specific Value" Value="0"></asp:ListItem>
                                                                                </asp:DropDownList>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>--%>
                                                                        <asp:TemplateField HeaderText="Value">
                                                                            <ItemTemplate>
                                                                                <div id="divTxtValue" runat="server">
                                                                                    <asp:TextBox ID="txtValue" runat="server" columns="30" ></asp:TextBox>
                                                                                    <asp:HyperLink ID="Calendar" runat="server" ImageUrl="../_Images/calendar.gif" 
                                                                                        ToolTip="<%$ Resources:LogiAdHoc, CalendarLinkTooltip %>" 
                                                                                        Text="<%$ Resources:LogiAdHoc, CalendarLinkTooltip %>"></asp:HyperLink>
                                                                                    <asp:RequiredFieldValidator ID="rtvValidValue" ValidationGroup="CalcColParams" ControlToValidate="txtValue" EnableClientScript="false" 
                                                                                        ErrorMessage="Parameter Value is required." runat="server" meta:resourcekey="rtvParamNameResource1">*</asp:RequiredFieldValidator>
                                                                                    <asp:CustomValidator ID="cvValidValue" ValidationGroup="CalcColParams" runat="server" EnableClientScript="false"
                                                                                        ErrorMessage="Please enter a valid value for this parameter." ControlToValidate="txtValue" 
                                                                                        OnServerValidate="IsCalcParamValueValid" meta:resourcekey="cvUniqueCalcNameResource1">*</asp:CustomValidator>
                                                                                </div>
                                                                                <div id="divPredefinedValues" runat="server">
                                                                                    <asp:DropDownList ID="ddlColumn" runat="server"></asp:DropDownList>
                                                                                </div>
                                                                                <div id="divRangeValues" runat="server">
                                                                                    <asp:UpdatePanel ID="UPRangeVals" runat="server" UpdateMode="Conditional" RenderMode="inline">
                                                                                        <ContentTemplate>
                                                                                            <asp:GridView ID="grdRangeVals" runat="server" AutoGenerateColumns="False"
                                                                                                OnRowDataBound="OnRangeRowDataBoundHandler" CssClass="grid">
                                                                                                <Columns>
                                                                                                    <asp:TemplateField HeaderText="Operator">
                                                                                                        <ItemTemplate>
                                                                                                            <input type="hidden" id="ihIsNew" runat="server" />
                                                                                                            <asp:Label ID="lblRangeOperator" runat="server" Style="display: block; width: 150px; padding:2px; padding-right: 30px;" />
                                                                                                            <asp:Panel ID="pnlOperators" runat="server" Style="display :none; visibility: hidden;">
                                                                                                            </asp:Panel>
                                                                                                            <ajaxToolkit:DropDownExtender runat="server" ID="DDE"
                                                                                                                TargetControlID="lblRangeOperator"
                                                                                                                DropDownControlID="pnlOperators" />
                                                                                                        </ItemTemplate>
                                                                                                    </asp:TemplateField>
                                                                                                    <asp:TemplateField HeaderText="Value">
                                                                                                        <ItemTemplate>
                                                                                                            <asp:TextBox ID="txtParamValue" runat="server" columns="20" ></asp:TextBox>
                                                                                                        </ItemTemplate>
                                                                                                    </asp:TemplateField>
                                                                                                    <asp:TemplateField HeaderText="Maps to">
                                                                                                        <ItemTemplate>
                                                                                                            <asp:TextBox ID="txtMapsTo" runat="server" columns="20" ></asp:TextBox>
                                                                                                        </ItemTemplate>
                                                                                                    </asp:TemplateField>
                                                                                                    <asp:TemplateField HeaderText="<%$ Resources:LogiAdHoc, Actions %>">
                                                                                                        <ItemTemplate>
                                                                                                            <asp:ImageButton ID="Remove" ImageURL="../ahImages/remove.gif" AlternateText="Remove"
                                                                                                                ToolTip="Remove" runat="server" OnCommand="RangeParamAction" 
                                                                                                                CausesValidation="false" meta:resourcekey="ModifyResource1" />
                                                                                                        </ItemTemplate>
                                                                                                    </asp:TemplateField>
                                                                                                </Columns>
                                                                                            </asp:GridView>
                                                                                        </ContentTemplate>
                                                                                    </asp:UpdatePanel>
                                                                                </div>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                    </Columns>
                                                                </asp:GridView>
                                                            </div>
                                                            <asp:ValidationSummary ID="ValidationSummary6" runat="server" ValidationGroup="CalcColParams" meta:resourcekey="ValidationSummary5Resource1" />
                                                            <asp:Label ID="lblSemicolon6" runat="server" OnPreRender="lblSemicolon_PreRender" />
                                                        </td>
                                                    </tr>
                                                </table>
                                                <p></p>
                                                <br /><table><tr><td>
                                                <AdHoc:LogiButton ID="btnCalcColFuncOK" OnClick="btnCalcColFuncOK_OnClick" runat="server" ValidationGroup="CalcColParams"
                                                    Text="<%$ Resources:LogiAdHoc, OKWithSpaces %>" UseSubmitBehavior="False" />
                                                <AdHoc:LogiButton ID="btnCalcColFuncCancel" OnClick="btnCalcColFuncCancel_OnClick"
                                                    runat="server" Text="<%$ Resources:LogiAdHoc, Cancel %>" CausesValidation="False" />
                                                </td></tr></table>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </div>
                                </asp:Panel>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </asp:Panel>
                    </ContentTemplate>
                <Triggers>
                    <asp:AsyncPostBackTrigger ControlID="ddObjectID" EventName="SelectedIndexChanged" />
                    <asp:PostBackTrigger ControlID="btnCancel" />
                </Triggers>
            </asp:UpdatePanel>
            
            
                    <asp:Button runat="server" ID="Button2" Style="display: none" />
                    <ajaxToolkit:ModalPopupExtender runat="server" ID="ahModalPopupRecompile" BehaviorID="ahModalPopupBehaviorRecompile"
                        TargetControlID="Button2" PopupControlID="ahPopupRecompile" BackgroundCssClass="modalBackground"
                        DropShadow="false" PopupDragHandleControlID="pnlDragHandleRecompile" RepositionMode="None">
                    </ajaxToolkit:ModalPopupExtender>
                    <asp:Panel runat="server" CssClass="modalPopup" ID="ahPopupRecompile" style="display:none; width:750;">
                        <asp:Panel ID="pnlDragHandleRecompile" runat="server" Style="cursor: hand;">
                            <div class="modalPopupHandle">
                                <table cellpadding="0" cellspacing="0" style="width: 100%"><tr><td>
                                        <asp:Localize ID="Localize6" runat="server" Text="<%$ Resources:LogiAdHoc, ParameterDetails %>"></asp:Localize>
                                    </td>
                                    <td style="width: 20px;">
                                        <asp:ImageButton ID="imgClosePopupRecompile" runat="server" 
                                            OnClick="imgClosePopupRecompile_Click" CausesValidation="false"
                                            SkinID="imgbClose" ImageUrl="../ahImages/remove.gif" 
                                            AlternateText="<%$ Resources:LogiAdHoc, CloseWindow %>" />
                                </td></tr></table>
                            </div>
                        </asp:Panel>
                        <div class="modalDiv">
                            <asp:UpdatePanel ID="upPopupRecompile2" runat="server">
                                <ContentTemplate>
                                    The following reports will be affected by this change, would you like to continue?
                                    <br /><br />
                                    
                                    <AdHoc:RecompileGrid runat="server" ID="grdRecompile" />
                                    <br /><br />
                                    
                                    
                                    <AdHoc:LogiButton ID="btnRecompile" runat="server" CausesValidation="False"
                                        UseSubmitBehavior="false" ToolTip="<%$ Resources:LogiAdHoc, SaveTooltip1 %>" OnClientClick="javascript: SetRebuilt();"
                                        Text="Recompile Selected" OnClick="btnRecompile_Click" />
                                    <AdHoc:LogiButton ID="btnCancelRecompile" runat="server" 
                                        ToolTip="Cancel" Text="Cancel"
                                        CausesValidation="False" OnClick="btnCancelRecompile_Click" />
                                
                                    
                                    <AdHoc:LogiButton ID="btnCloseRecompile" runat="server" Visible="false"
                                        ToolTip="close" Text="Close"
                                        CausesValidation="False" OnClick="btnCancelRecompile_Click" />
                                
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </div>
                    </asp:Panel>
                    
            
            <div id="divSaveAnimation">
                <asp:Button runat="server" ID="btnSaveAnimation" Style="display: none" />
                <ajaxToolkit:ModalPopupExtender runat="server" ID="ahSavePopup" BehaviorID="ahSavePopupBehavior"
                    TargetControlID="btnSaveAnimation" PopupControlID="pnlSaving" BackgroundCssClass="modalBackground"
                    DropShadow="False">
                </ajaxToolkit:ModalPopupExtender>
                <asp:Panel id="pnlSaving" CssClass="savePopup" runat="server" style="display:none;">
                    <table><tr><td>
                    <asp:Image ID="imgSave" runat="server" SkinID="imgSaving" EnableViewState="false" AlternateText="<%$ Resources:LogiAdHoc, SaveImageAltText %>" />
                    </td>
                    <td valign="middle">
                    <asp:Label ID="lblSaveText" CssClass="lblSavePopup" runat="server" EnableViewState="false" Text="<%$ Resources:LogiAdHoc, PleaseWait %>"/>
                    </td></tr></table>
                </asp:Panel>
            </div>
        </div>
    </form>
</body>
</html>
