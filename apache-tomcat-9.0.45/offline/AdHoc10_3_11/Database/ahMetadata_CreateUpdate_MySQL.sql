﻿SET FOREIGN_KEY_CHECKS = 0;

-- -------------------------------------
-- Tables

CREATE TABLE IF NOT EXISTS `CascadeFilterDetails` (
  `FilterDetailID` INT(10) NOT NULL AUTO_INCREMENT,
  `FilterID` INT(10) NOT NULL DEFAULT 0,
  `ObjectID` INT(10) NOT NULL DEFAULT 0,
  `FilterColumnID` INT(10) NULL,
  `DisplayColumnID` INT(10) NOT NULL,
  `ValueColumnID` INT(10) NOT NULL,
  `FilterOrder` TINYINT(3) NOT NULL DEFAULT 0,
  PRIMARY KEY (`FilterDetailID`)
)
ENGINE = INNODB;

CREATE TABLE IF NOT EXISTS `CascadeFilters` (
  `FilterID` INT(10) NOT NULL AUTO_INCREMENT,
  `FilterName` VARCHAR(100) NOT NULL,
  `Description` VARCHAR(255) NULL,
  `ObjectID` INT(10) NOT NULL,
  `ColumnID` INT(10) NOT NULL,
  `DatabaseID` INT(10) NOT NULL,
  PRIMARY KEY (`FilterID`)
)
ENGINE = INNODB;

CREATE TABLE IF NOT EXISTS `Categories` (
  `CategoryID` INT(10) NOT NULL AUTO_INCREMENT,
  `DatabaseID` INT(10) NOT NULL,
  `CategoryName` VARCHAR(100) NOT NULL,
  `Description` VARCHAR(255) NULL,
  PRIMARY KEY (`CategoryID`)
)
ENGINE = INNODB;

CREATE TABLE IF NOT EXISTS `CategoryObjects` (
  `CategoryID` INT(10) NOT NULL,
  `ObjectID` INT(10) NOT NULL,
  PRIMARY KEY (`CategoryID`, `ObjectID`),
  INDEX `CategoryObjects_ObjectIDIdx` (`ObjectID`),
  INDEX `CategoryObjects_CategoryIDIdx` (`CategoryID`)
)
ENGINE = INNODB;

CREATE TABLE IF NOT EXISTS `Classes` (
  `ClassID` INT(10) NOT NULL AUTO_INCREMENT,
  `Class` VARCHAR(100) NOT NULL,
  `FriendlyName` VARCHAR(100) NOT NULL,
  PRIMARY KEY (`ClassID`),
  UNIQUE INDEX `Classes_FriendlyNameIdx` (`FriendlyName`)
)
ENGINE = INNODB;

CREATE TABLE IF NOT EXISTS `ColumnAccess` (
  `AccessID` INT(10) NOT NULL,
  `AccessName` VARCHAR(10) NOT NULL,
  PRIMARY KEY (`AccessID`)
)
ENGINE = INNODB;

CREATE TABLE IF NOT EXISTS `ColumnExplanation` (
  `ExplanationID` BIGINT(19) NOT NULL AUTO_INCREMENT,
  `Explanation` LONGTEXT NOT NULL,
  PRIMARY KEY (`ExplanationID`)
)
ENGINE = INNODB;

CREATE TABLE IF NOT EXISTS `Columns` (
  `ColumnID` INT(10) NOT NULL AUTO_INCREMENT,
  `ColumnName` VARCHAR(100) NOT NULL,
  `ColumnAlias` VARCHAR(100) NULL,
  `Description` VARCHAR(100) NULL,
  `ColumnType` VARCHAR(2) NULL DEFAULT 'D',
  `ObjectID` INT(10) NOT NULL,
  `OrdinalPosition` INT(10) NULL,
  `ColumnOrder` INT(10) NULL DEFAULT 0,
  `DataType` VARCHAR(20) NULL,
  `CharacterMaxLen` INT(10) NULL,
  `NumericPrecision` INT(10) NULL,
  `NumericScale` INT(10) NULL,
  `DisplayFormat` VARCHAR(50) NULL,
  `Alignment` VARCHAR(15) NULL,
  `NativeDataType` VARCHAR(20) NULL,
  `Definition` VARCHAR(4000) NULL,
  `ExplanationID` BIGINT(19) NOT NULL DEFAULT 0,
  `LinkRptID` INT(10) NULL DEFAULT 0,
  `LinkURL` VARCHAR(255) NULL,
  `FrameID` VARCHAR(25) NULL,
  `HideColumn` TINYINT(3) NULL DEFAULT 0,
  PRIMARY KEY (`ColumnID`),
  UNIQUE INDEX `IX_Columns` (`ObjectID`, `ColumnName`)
)
ENGINE = INNODB;

CREATE TABLE IF NOT EXISTS `DatabaseRole` (
  `DatabaseRoleID` INT(10) NOT NULL AUTO_INCREMENT,
  `DatabaseID` INT(10) NOT NULL,
  `RoleID` INT(10) NOT NULL,
  PRIMARY KEY (`DatabaseRoleID`)
)
ENGINE = INNODB;

CREATE TABLE IF NOT EXISTS `DataFormats` (
  `FormatID` INT(10) NOT NULL AUTO_INCREMENT,
  `FormatKey` VARCHAR(50) NULL,
  `FormatName` VARCHAR(50) NOT NULL,
  `Format` VARCHAR(255) NOT NULL,
  `Internal` TINYINT(3) NOT NULL DEFAULT 0,
  `AppliesTo` INT(10) NOT NULL DEFAULT 0,
  `Explanation` VARCHAR(255) NULL,
  `ExampleBefore` VARCHAR(50) NULL,
  `ExampleAfter` VARCHAR(50) NULL,
  `IsAvailable` TINYINT(3) NOT NULL DEFAULT 1,
  `SortOrder` INT(10) NULL DEFAULT 0,
  PRIMARY KEY (`FormatID`),
  INDEX `DataFormats_SortOrderIdx` (`SortOrder`)
)
ENGINE = INNODB;

CREATE TABLE IF NOT EXISTS `Folder` (
  `FolderID` INT(10) NOT NULL AUTO_INCREMENT,
  `FolderName` VARCHAR(100) NOT NULL,
  `OwnerUserID` INT(10) NOT NULL,
  `ModifiedUserID` INT(10) NULL,
  `FolderType` INT(10) NOT NULL DEFAULT 1,
  `ParentFolderID` INT(10) NOT NULL DEFAULT 0,
  `Description` VARCHAR(255) NULL,
  `AllRolesAccess` INT(10) NOT NULL DEFAULT 1,
  `TimeCreated` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `TimeSaved` DATETIME NULL,
  `DatabaseID` INT(10) NULL,
  `GroupID` INT(10) NULL DEFAULT 0,
  PRIMARY KEY (`FolderID`),
  INDEX `Folder_GroupIDIdx` (`GroupID`)
)
ENGINE = INNODB;

CREATE TABLE IF NOT EXISTS `FolderRole` (
  `FolderRoleID` INT(10) NOT NULL AUTO_INCREMENT,
  `FolderID` INT(10) NOT NULL,
  `RoleID` INT(10) NOT NULL,
  PRIMARY KEY (`FolderRoleID`)
)
ENGINE = INNODB;

CREATE TABLE IF NOT EXISTS `JoinRelation` (
  `JoinRelationID` INT(10) NOT NULL AUTO_INCREMENT,
  `ObjectID1` INT(10) NOT NULL,
  `Relation` VARCHAR(50) NOT NULL,
  `ObjectID2` INT(10) NOT NULL,
  `RelationName` VARCHAR(100) NULL,
  `Description` VARCHAR(255) NULL,
  `DatabaseID` INT(10) NULL,
  `ObjectLabel1` VARCHAR(100) NULL,
  `ObjectLabel2` VARCHAR(100) NULL,
  `HideJoin` TINYINT(3) NULL DEFAULT 0,
  `AutoReverse` TINYINT(3) NULL DEFAULT 1,
  `Automatic` TINYINT(3) NULL DEFAULT 0,
  PRIMARY KEY (`JoinRelationID`)
)
ENGINE = INNODB;

CREATE TABLE IF NOT EXISTS `JoinRelationDetails` (
  `JoinRelationDetailsID` INT(10) NOT NULL AUTO_INCREMENT,
  `JoinRelationID` INT(10) NOT NULL,
  `ColumnID1` INT(10) NOT NULL,
  `ColumnID2` INT(10) NOT NULL,
  PRIMARY KEY (`JoinRelationDetailsID`),
  INDEX `JoinRelationDetails_JoinRelatio` (`JoinRelationID`)
)
ENGINE = INNODB;

CREATE TABLE IF NOT EXISTS `LinkParameters` (
  `LinkParameterID` INT(10) NOT NULL AUTO_INCREMENT,
  `ColumnID` INT(10) NOT NULL DEFAULT 0,
  `ParameterName` VARCHAR(50) NOT NULL,
  `ParamDisplayName` VARCHAR(50) NOT NULL,
  `ParameterSourceType` TINYINT(3) NOT NULL DEFAULT 0,
  `ParameterSource` VARCHAR(100) NOT NULL,
  PRIMARY KEY (`LinkParameterID`),
  INDEX `LinkParameters_ParameterNameIdx` (`ParameterName`),
  INDEX `LP_ColumnIDIdx` (`ColumnID`)
)
ENGINE = INNODB;

CREATE TABLE IF NOT EXISTS `ObjectAccess` (
  `AccessID` INT(10) NOT NULL,
  `AccessName` VARCHAR(10) NOT NULL,
  PRIMARY KEY (`AccessID`)
)
ENGINE = INNODB;

CREATE TABLE IF NOT EXISTS `ObjectParameters` (
  `ObjectParameterID` INT(10) NOT NULL AUTO_INCREMENT,
  `ObjectID` INT(10) NOT NULL DEFAULT 0,
  `ColumnID` INT(10) NOT NULL DEFAULT 0,
  `Operator` VARCHAR(25) NULL,
  `ParamValue` VARCHAR(50) NULL,
  `BitCmd` VARCHAR(10) NULL,
  `ParamOrder` INT(10) NULL,
  `ParamLevel` INT(10) NULL,
  `ParamType` TINYINT(3) DEFAULT 0,
  `ParamName` VARCHAR(128) NULL,
  `ParamCaption` VARCHAR(128) NULL,
  `ParamDataType` VARCHAR(20) NULL,
  `ParamDirection` TINYINT(3) DEFAULT 0,
  PRIMARY KEY (`ObjectParameterID`),
  INDEX `ObjectParameters_ColumnIDIdx` (`ColumnID`),
  INDEX `ObjectParameters_ObjectIDIdx` (`ObjectID`)
)
ENGINE = INNODB;

CREATE TABLE IF NOT EXISTS `Objects` (
  `ObjectID` INT(10) NOT NULL AUTO_INCREMENT,
  `ObjectSchema` VARCHAR(255) NULL,
  `ObjectName` VARCHAR(128) NOT NULL,
  `ObjectAlias` VARCHAR(128) NULL,
  `Description` VARCHAR(128) NULL,
  `Type` VARCHAR(2) NULL,
  `DatabaseID` INT(10) NULL,
  `Definition` VARCHAR(7500) NULL,
  `HideObject` TINYINT(3) NULL DEFAULT 0,
  `ExplanationID` BIGINT(19) DEFAULT 0,
  `IsCatalogue` TINYINT(3) DEFAULT 1,
  PRIMARY KEY (`ObjectID`)
)
ENGINE = INNODB;

CREATE TABLE IF NOT EXISTS `ParameterValues` (
  `ParameterValuesID` INT(10) NOT NULL AUTO_INCREMENT,
  `ObjectParameterID` INT(10) NOT NULL,
  `ParamValue` VARCHAR(255) NOT NULL,
  `ParamValueType` TINYINT(3) DEFAULT 0,
  PRIMARY KEY (`ParameterValuesID`)
)
ENGINE = INNODB;

CREATE TABLE IF NOT EXISTS `PermissionRights` (
  `PermissionRightID` INT(10) NOT NULL AUTO_INCREMENT,
  `PermissionID` INT(10) NOT NULL,
  `RightID` INT(10) NOT NULL,
  PRIMARY KEY (`PermissionRightID`)
)
ENGINE = INNODB;

CREATE TABLE IF NOT EXISTS `Permissions` (
  `PermissionID` INT(10) NOT NULL,
  `Permission` VARCHAR(100) NOT NULL,
  `Description` VARCHAR(255) NULL,
  PRIMARY KEY (`PermissionID`)
)
ENGINE = INNODB;

CREATE TABLE IF NOT EXISTS `ReportParameters` (
  `ReportParameterID` INT(10) NOT NULL AUTO_INCREMENT,
  `ReportID` INT(10) NOT NULL,
  `ColumnID` INT(10) NOT NULL,
  `Operator` VARCHAR(25) NULL,
  `BitCmd` VARCHAR(10) NULL,
  `Ask` INT(10) NULL,
  `Scheduler` INT(10) NULL,
  `Caption` VARCHAR(100) NULL,
  `ParamType` TINYINT(3) DEFAULT 0,
  `ParamName` VARCHAR(128) NULL,
  PRIMARY KEY (`ReportParameterID`)
)
ENGINE = INNODB;

CREATE TABLE IF NOT EXISTS `ReportParameterValues` (
  `ReportParameterValuesID` INT(10) NOT NULL AUTO_INCREMENT,
  `ReportParameterID` INT(10) NOT NULL,
  `ReportScheduleID` INT(10) NOT NULL,
  `ParamValue` VARCHAR(255) NOT NULL,
  PRIMARY KEY (`ReportParameterValuesID`),
  INDEX `ReportParameterValues_ReportSch` (`ReportScheduleID`)
)
ENGINE = INNODB;

CREATE TABLE IF NOT EXISTS `ReportSchedules` (
  `ReportScheduleID` INT(10) NOT NULL AUTO_INCREMENT,
  `ReportID` INT(10) NOT NULL,
  `ModifiedUserID` INT(10) NULL,
  `TimeSaved` DATETIME NULL,
  `TaskName` VARCHAR(50) NOT NULL,
  `TaskID` INT(10) NULL, 
  `OutputFormat` INT(10) NULL,
  `Archive` INT(10) NULL,
  `Server` VARCHAR(255) NULL,
  `Method` TINYINT(3) NULL,
  `Broken` TINYINT(3) NULL,
  PRIMARY KEY (`ReportScheduleID`),
  INDEX `ReportSchedules_ModifiedUserIDI` (`ModifiedUserID`),
  INDEX `ReportSchedules_ReportIDIdx` (`ReportID`)
)
ENGINE = INNODB;

CREATE TABLE IF NOT EXISTS `ReportSessionParameters` (
  `ReportSessionParamID` BIGINT(19) NOT NULL AUTO_INCREMENT,
  `ReportID` INT(10) NOT NULL DEFAULT 0,
  `UserID` INT(10) NOT NULL DEFAULT 0,
  `ParamName` VARCHAR(100) NOT NULL,
  `ParamValue` VARCHAR(4000) NULL,
  PRIMARY KEY (`ReportID`, `UserID`, `ParamName`),
  UNIQUE INDEX `IX_ReportSessionParameters` (`ReportSessionParamID`),
  INDEX `ReportSessionParameters_ReportI` (`ReportID`),
  INDEX `ReportSessionParameters_UserIDI` (`UserID`)
)
ENGINE = INNODB;

CREATE TABLE IF NOT EXISTS `Rights` (
  `RightID` INT(10) NOT NULL,
  `RightName` VARCHAR(100) NOT NULL,
  `Description` VARCHAR(255) NULL,
  `RightGroup` VARCHAR(50) NOT NULL,
  PRIMARY KEY (`RightID`)
)
ENGINE = INNODB;

CREATE TABLE IF NOT EXISTS `Role` (
  `RoleID` INT(10) NOT NULL AUTO_INCREMENT,
  `RoleName` VARCHAR(64) NOT NULL,
  `Description` VARCHAR(255) NULL,
  PRIMARY KEY (`RoleID`),
  UNIQUE INDEX `Role_RoleNameIdx` (`RoleName`)
)
ENGINE = INNODB;

CREATE TABLE IF NOT EXISTS `RoleColumns` (
  `RoleID` INT(10) NOT NULL,
  `ColumnID` INT(10) NOT NULL,
  `AccessType` INT(10) NOT NULL,
  PRIMARY KEY (`RoleID`, `ColumnID`),
  INDEX `RoleColumns_ColumnIDIdx` (`ColumnID`),
  INDEX `RoleColumns_RoleIDIdx` (`RoleID`)
)
ENGINE = INNODB;

CREATE TABLE IF NOT EXISTS `RoleObjects` (
  `RoleID` INT(10) NOT NULL,
  `ObjectID` INT(10) NOT NULL,
  `AccessType` INT(10) NOT NULL,
  PRIMARY KEY (`RoleID`, `ObjectID`),
  INDEX `RoleObjects_ObjectIDIdx` (`ObjectID`),
  INDEX `RoleObjects_RoleIDIdx` (`RoleID`)
)
ENGINE = INNODB;

CREATE TABLE IF NOT EXISTS `RolePermissions` (
  `RolePermissionID` INT(10) NOT NULL AUTO_INCREMENT,
  `RoleID` INT(10) NOT NULL,
  `PermissionID` INT(10) NOT NULL,
  PRIMARY KEY (`RolePermissionID`)
)
ENGINE = INNODB;

CREATE TABLE IF NOT EXISTS `SessionParameters` (
  `SessionParameterID` INT(10) NOT NULL AUTO_INCREMENT,
  `ParameterName` VARCHAR(50) NOT NULL,
  `DefaultValue` VARCHAR(4000) NULL,
  `DataTypeCategory` INT(10) NULL DEFAULT 2,
  PRIMARY KEY (`SessionParameterID`),
  UNIQUE INDEX `SP_ParameterNameIdx` (`ParameterName`)
)
ENGINE = INNODB;

CREATE TABLE IF NOT EXISTS `SourceDatabase` (
  `DatabaseID` INT(10) NOT NULL AUTO_INCREMENT,
  `Description` VARCHAR(255) NULL,
  PRIMARY KEY (`DatabaseID`)
)
ENGINE = INNODB;

CREATE TABLE IF NOT EXISTS `SystemSetting` (
  `SettingName` VARCHAR(20) NOT NULL,
  `SettingValue` VARCHAR(20) NULL,
  `Description` VARCHAR(100) NULL,
  `DatabaseID` INT(10) NOT NULL,
  PRIMARY KEY (`SettingName`, `DatabaseID`),
  INDEX `SystemSetting_DatabaseIDIdx` (`DatabaseID`),
  INDEX `SystemSetting_SettingNameIdx` (`SettingName`)
)
ENGINE = INNODB;

CREATE TABLE IF NOT EXISTS `Templates` (
  `TemplateID` INT(10) NOT NULL AUTO_INCREMENT,
  `TemplateName` VARCHAR(255) NULL,
  `TemplateKey` VARCHAR(50) NULL,
  `TemplateType` TINYINT(3) NOT NULL DEFAULT 0,
  `Description` VARCHAR(255) NULL,
  `OwnerID` INT(10) NULL,
  `UsedObjects` VARCHAR(255) NULL,
  `UsedColumns` VARCHAR(6000) NULL,
  `IsDefault` TINYINT(3) NOT NULL DEFAULT 0,
  `IsAvailable` TINYINT(3) NOT NULL DEFAULT 1,
  `SortOrder` INT(10) NULL,
  `RightName` VARCHAR(100) NULL,
  PRIMARY KEY (`TemplateID`),
  INDEX `Templates_TemplateTypeIdx` (`TemplateType`)
)
ENGINE = INNODB;

CREATE TABLE IF NOT EXISTS `UserGroupRoles` (
  `UserGroupRoleID` INT(10) NOT NULL AUTO_INCREMENT,
  `GroupID` INT(10) NOT NULL,
  `RoleID` INT(10) NOT NULL,
  PRIMARY KEY (`UserGroupRoleID`)
)
ENGINE = INNODB;

CREATE TABLE IF NOT EXISTS `UserGroups` (
  `GroupID` INT(10) NOT NULL AUTO_INCREMENT,
  `GroupName` VARCHAR(100) NOT NULL,
  `Description` VARCHAR(255) NULL,
  `DefaultTheme` VARCHAR(255) NULL,
  PRIMARY KEY (`GroupID`),
  UNIQUE INDEX `SP_GroupNameIdx` (`GroupName`)
)
ENGINE = INNODB;

CREATE TABLE IF NOT EXISTS `UserGroupSessions` (
  `UserGroupSessionID` INT(10) NOT NULL AUTO_INCREMENT,
  `GroupID` INT(10) NOT NULL,
  `SessionParameterID` INT(10) NOT NULL,
  `SessionParameterValue` VARCHAR(4000) NOT NULL,
  PRIMARY KEY (`UserGroupSessionID`),
  INDEX `UGS_GroupIDIdx` (`GroupID`),
  INDEX `UGS_SessionParameterIDIdx` (`SessionParameterID`)
)
ENGINE = INNODB;

CREATE TABLE IF NOT EXISTS `UserSessions` (
  `UserSessionID` INT(10) NOT NULL AUTO_INCREMENT,
  `UserID` INT(10) NOT NULL,
  `SessionParameterID` INT(10) NOT NULL,
  `SessionParameterValue` VARCHAR(4000) NOT NULL,
  PRIMARY KEY (`UserSessionID`),
  INDEX `US_UserIDIdx` (`UserID`),
  INDEX `US_SessionParameterIDIdx` (`SessionParameterID`)
)
ENGINE = INNODB;

CREATE TABLE IF NOT EXISTS `UserProfile` (
  `UserProfileID` INT(10) NOT NULL AUTO_INCREMENT,
  `UserID` INT(10) NOT NULL,
  `PropertyName` VARCHAR(255) NOT NULL,
  `PropertyValue` VARCHAR(255) NULL,
  PRIMARY KEY (`UserProfileID`),
  INDEX `UserProfile_UserIDIdx` (`UserID`)
)
ENGINE = INNODB;

CREATE TABLE IF NOT EXISTS `UserReport` (
  `ID` INT(10) NOT NULL AUTO_INCREMENT,
  `UserID` INT(10) NOT NULL,
  `FolderType` INT(10) NOT NULL DEFAULT 1,
  `ReportName` VARCHAR(200) NULL DEFAULT 'New',
  `Description` VARCHAR(255) NULL,
  `TimeCreated` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `TimeSaved` DATETIME NULL,
  `ParentFolderID` INT(10) NOT NULL DEFAULT 0,
  `ModifiedUserID` INT(10) NULL,
  `DatabaseID` INT(10) NULL,
  `UsedDatabases` VARCHAR(255) NULL,
  `UsedObjects` VARCHAR(255) NULL,
  `UsedColumns` VARCHAR(6000) NULL,
  `UsedRelationships` VARCHAR(255) NULL,
  `UsedCascadingFilters` VARCHAR(255) NULL,
  `UsedClasses` VARCHAR(255) NULL,
  `UsedFormats` VARCHAR(255) NULL,
  `GroupID` INT(10) NULL DEFAULT 0,
  `PhysicalName` VARCHAR(200) NULL,
  `Broken` TINYINT(3) NULL,
  `BreakReason` BIGINT(19) NULL,
  `LastViewDate` DATETIME NULL,
  `LastViewUserID` INT(10) NULL,
  `ViewCount` BIGINT(19) NULL DEFAULT 0,
  `Dashboard` TINYINT(3) NOT NULL DEFAULT 0,
  `Mobile` TINYINT(3) NOT NULL DEFAULT 0,
  `ExpirationDate` DATETIME NULL,
  `ReportGUID` VARCHAR(100) NULL,
  PRIMARY KEY (`ID`),
  INDEX `UserReport_GroupIDIdx` (`GroupID`),
  INDEX `UserReport_LastViewUserIDIdx` (`LastViewUserID`)
)
ENGINE = INNODB;

CREATE TABLE IF NOT EXISTS `UserRole` (
  `UserID` INT(10) NOT NULL,
  `RoleID` INT(10) NOT NULL,
  PRIMARY KEY (`UserID`, `RoleID`),
  INDEX `UserRole_RoleIDIdx` (`RoleID`),
  INDEX `UserRole_UserIDIdx` (`UserID`)
)
ENGINE = INNODB;

CREATE TABLE IF NOT EXISTS `Users` (
  `UserID` INT(10) NOT NULL AUTO_INCREMENT,
  `UserName` VARCHAR(50) NOT NULL,
  `Password` VARCHAR(100) NOT NULL,
  `FirstName` VARCHAR(20) NULL,
  `LastName` VARCHAR(20) NULL,
  `Email` VARCHAR(50) NULL,
  `LastDatabaseID` INT(10) NULL,
  `GroupID` INT(10) NULL DEFAULT 0,
  `Locked` TINYINT(3) NULL DEFAULT 0,
  `LastPasswordChange` DATETIME NULL,
  PRIMARY KEY (`UserID`),
  INDEX `Users_GroupIDIdx` (`GroupID`),
  UNIQUE INDEX `Users_UserNameIdx` (`UserName`)
)
ENGINE = INNODB;

CREATE TABLE IF NOT EXISTS `UserScheduleSubscription` (
  `SubscriptionID` INT(10) NOT NULL AUTO_INCREMENT,
  `UserID` INT(10) NOT NULL,
  `ReportScheduleID` INT(10) NOT NULL,
  `Broken` TINYINT(3) NULL DEFAULT 0,
  PRIMARY KEY (`SubscriptionID`),
  INDEX `UserScheduleSubscription_Report` (`ReportScheduleID`)
)
ENGINE = INNODB;

-- -------------------------------------
-- Insert Data

INSERT INTO DataFormats (FormatKey, FormatName, Format, Internal, AppliesTo, SortOrder) VALUES 
	('GeneralNumber','General Number', 'General Number', 1, 9, 1);
INSERT INTO DataFormats (FormatKey, FormatName, Format, Internal, AppliesTo, SortOrder) VALUES 
	('Currency','Currency', 'Currency', 1, 9, 2);
INSERT INTO DataFormats (FormatKey, FormatName, Format, Internal, AppliesTo, SortOrder) VALUES
	('IntegerFormat','Integer', '#0', 1, 9, 3);
INSERT INTO DataFormats (FormatKey, FormatName, Format, Internal, AppliesTo, SortOrder) VALUES
	('Fixed','Fixed', 'Fixed', 1, 9, 4);
INSERT INTO DataFormats (FormatKey, FormatName, Format, Internal, AppliesTo, SortOrder) VALUES
	('Standard','Standard', 'Standard', 1, 9, 5);
INSERT INTO DataFormats (FormatKey, FormatName, Format, Internal, AppliesTo, SortOrder) VALUES
	('Percent','Percent', 'Percent', 1, 9, 6);
INSERT INTO DataFormats (FormatKey, FormatName, Format, Internal, AppliesTo, SortOrder) VALUES
	('Scientific','Scientific', 'Scientific', 1, 9, 7);
INSERT INTO DataFormats (FormatKey, FormatName, Format, Internal, AppliesTo, SortOrder) VALUES
	('TwoDigit','2-digit place holder', '2-digit place holder', 1, 9, 8);
INSERT INTO DataFormats (FormatKey, FormatName, Format, Internal, AppliesTo, SortOrder) VALUES
	('ThreeDigit','3-digit place holder', '3-digit place holder', 1, 9, 9);
INSERT INTO DataFormats (FormatKey, FormatName, Format, Internal, AppliesTo, SortOrder) VALUES
	('GeneralDate','General Date', 'General Date', 1, 10, 10);
INSERT INTO DataFormats (FormatKey, FormatName, Format, Internal, AppliesTo, SortOrder) VALUES
	('LongDate','Long Date', 'Long Date', 1, 10, 11);
INSERT INTO DataFormats (FormatKey, FormatName, Format, Internal, AppliesTo, IsAvailable, SortOrder) VALUES
	('MediumDate','Medium Date', 'Medium Date', 1, 10, 0, 12);
INSERT INTO DataFormats (FormatKey, FormatName, Format, Internal, AppliesTo, SortOrder) VALUES
	('ShortDate','Short Date', 'Short Date', 1, 10, 13);
INSERT INTO DataFormats (FormatKey, FormatName, Format, Internal, AppliesTo, SortOrder) VALUES
	('LongTime','Long Time', 'Long Time', 1, 10, 14);
INSERT INTO DataFormats (FormatKey, FormatName, Format, Internal, AppliesTo, IsAvailable, SortOrder) VALUES
	('MediumTime','Medium Time', 'Medium Time', 1, 10, 0, 15);
INSERT INTO DataFormats (FormatKey, FormatName, Format, Internal, AppliesTo, SortOrder) VALUES
	('ShortTime','Short Time', 'Short Time', 1, 10, 16);
INSERT INTO DataFormats (FormatKey, FormatName, Format, Internal, AppliesTo, SortOrder) VALUES
	('YesNo','Yes/No', 'Yes/No', 1, 12, 17);
INSERT INTO DataFormats (FormatKey, FormatName, Format, Internal, AppliesTo, SortOrder) VALUES
	('TrueFalse','True/False', 'True/False', 1, 12, 18);
INSERT INTO DataFormats (FormatKey, FormatName, Format, Internal, AppliesTo, SortOrder) VALUES
	('OnOff','On/Off', 'On/Off', 1, 12, 19);
INSERT INTO DataFormats (FormatKey, FormatName, Format, Internal, AppliesTo, SortOrder) VALUES
	('PreserveLineFeeds','Preserve Line Feeds', 'Preserve Line Feeds', 1, 8, 20);
INSERT INTO DataFormats (FormatKey, FormatName, Format, Internal, AppliesTo, SortOrder) VALUES
	('HTML','HTML', 'HTML', 2, 8, 21);

INSERT INTO Templates (TemplateName, Description, IsDefault, SortOrder, RightName) VALUES ('Tabular Report with Header', 'Start with a tabular report with a header.', 0, 1, '');
INSERT INTO Templates (TemplateName, Description, IsDefault, SortOrder, RightName) VALUES ('Tabular Report without Header', 'Start with a tabular report without a header or style. Perfect as a dashboard panel.', 1, 2, '');
INSERT INTO Templates (TemplateName, Description, IsDefault, SortOrder, RightName) VALUES ('Crosstab Report', 'Start with a crosstab, also known as a pivot table.', 0, 3, 'RB_CTB');
INSERT INTO Templates (TemplateName, Description, IsDefault, SortOrder, RightName) VALUES ('Bar Chart', 'Start with a bar chart without header or style. Perfect as a dashboard panel.', 0, 4, 'RB_BCT');
INSERT INTO Templates (TemplateName, Description, IsDefault, SortOrder, RightName) VALUES ('Tabular Report with Chart', 'Start with a tabular report and a pie chart.', 0, 5, 'RB_PCT');
INSERT INTO Templates (TemplateName, Description, IsDefault, SortOrder, RightName) VALUES ('Tabular Report with Export', 'Start with a tabular report with export to Excel, Word, and PDF.', 0, 6, 'RB_XLS,RB_WRD,RB_PDF');

Insert Into ObjectAccess Values (0, 'None');
Insert Into ObjectAccess Values (1, 'Limited');
Insert Into ObjectAccess Values (2, 'Full');

Insert Into Classes (Class,FriendlyName) Values ('bold', 'Bold');
Insert Into Classes (Class,FriendlyName) Values ('green', 'Green');
Insert Into Classes (Class,FriendlyName) Values ('red', 'Red');
Insert Into Classes (Class,FriendlyName) Values ('AlignLeft', 'Align Text Left');
Insert Into Classes (Class,FriendlyName) Values ('AlignCenter', 'Align Text Center');
Insert Into Classes (Class,FriendlyName) Values ('AlignRight', 'Align Text Right');
Insert Into Classes (Class,FriendlyName) Values ('imageAlignLeft', 'Align Image Left');
Insert Into Classes (Class,FriendlyName) Values ('imageAlignCenter', 'Align Image Center');
Insert Into Classes (Class,FriendlyName) Values ('imageAlignRight', 'Align Image Right');
Insert Into Classes (Class,FriendlyName) Values ('BlackTextYellowBackground', 'Black Text Yellow Background');
Insert Into Classes (Class,FriendlyName) Values ('WhiteTextGreenBackground', 'White Text Green Background');

Insert Into ColumnAccess Values (0, 'None');
Insert Into ColumnAccess Values (1, 'Full');

Insert Into UserGroups (GroupName) values ('Default');

Insert Into Role (RoleName) values ('System Admin');

Insert into RolePermissions (RoleID, PermissionID) Values (1,1);

Insert into UserGroupRoles (GroupID, RoleID) (select GroupID, RoleID from UserGroups, Role);

Insert into Users (UserName, Password, FirstName, GroupID) Values ('Admin', 'password', 'Administrator', 1);

Insert into UserRole (UserID,RoleID) (select UserID, RoleID from Users, Role);

-- -------------------------------------
-- Procedures
delimiter $$

CREATE PROCEDURE authenticateUser(IN prmUser VARCHAR(50), IN prmPass VARCHAR(100))
BEGIN
	SELECT Users.UserName, Users.UserID
		FROM Users
		WHERE Users.UserName = prmUser AND Users.Password=prmPass;
END$$
delimiter ;


SET FOREIGN_KEY_CHECKS = 1;

-- ----------------------------------------------------------------------
-- EOF