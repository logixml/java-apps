var displayLoading = function() {
	if (Y.Lang.isValue(LogiXML.WaitPanel.pageWaitPanel)) {
		var waitCfg = ['','rdThemeWaitPanel','rdThemeWaitCaption'];
		LogiXML.WaitPanel.pageWaitPanel.readyWait();
		LogiXML.WaitPanel.pageWaitPanel.showWaitPanel(waitCfg);
	}
}

var addLoadingOverlay = function() {
	$('#HomeReportTable > tbody a').on('click', function() {
		displayLoading();
	});
}