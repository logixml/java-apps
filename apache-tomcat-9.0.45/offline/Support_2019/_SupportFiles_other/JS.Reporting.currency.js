var currencySimbols = {
	"EUR": "€",
	"USD": "$",
	"GBP": "£"
};

function getCurrencySymbol(currency) {
	return currencySimbols[currency] ? currencySimbols[currency] :  currency;
};