var flagsDictionary = {	
	"en": {
		"aa": "Afar",
		"aa-dj": "Afar (Djibouti)",
		"aa-er": "Afar (Eritrea)",
		"aa-et": "Afar (Ethiopia)",
		"af": "Afrikaans",
		"af-na": "Afrikaans (Namibia)",
		"af-za": "Afrikaans (South Africa)",
		"agq": "Aghem",
		"agq-cm": "Aghem (Cameroon)",
		"ak": "Akan",
		"ak-gh": "Akan (Ghana)",
		"sq": "Albanian",
		"sq-al": "Albanian (Albania)",
		"sq-xk": "Albanian (Kosovo)",
		"sq-mk": "Albanian (Macedonia, FYRO)",
		"gsw": "Alsatian",
		"gsw-fr": "Alsatian (France)",
		"gsw-li": "Alsatian (Liechtenstein)",
		"gsw-ch": "Alsatian (Switzerland)",
		"am": "Amharic",
		"am-et": "Amharic (Ethiopia)",
		"ar": "Arabic",
		"ar-dz": "Arabic (Algeria)",
		"ar-bh": "Arabic (Bahrain)",
		"ar-td": "Arabic (Chad)",
		"ar-km": "Arabic (Comoros)",
		"ar-dj": "Arabic (Djibouti)",
		"ar-eg": "Arabic (Egypt)",
		"ar-er": "Arabic (Eritrea)",
		"ar-iq": "Arabic (Iraq)",
		"ar-il": "Arabic (Israel)",
		"ar-jo": "Arabic (Jordan)",
		"ar-kw": "Arabic (Kuwait)",
		"ar-lb": "Arabic (Lebanon)",
		"ar-ly": "Arabic (Libya)",
		"ar-mr": "Arabic (Mauritania)",
		"ar-ma": "Arabic (Morocco)",
		"ar-om": "Arabic (Oman)",
		"ar-ps": "Arabic (Palestinian Authority)",
		"ar-qa": "Arabic (Qatar)",
		"ar-sa": "Arabic (Saudi Arabia)",
		"ar-so": "Arabic (Somalia)",
		"ar-ss": "Arabic (South Sudan)",
		"ar-sd": "Arabic (Sudan)",
		"ar-sy": "Arabic (Syria)",
		"ar-tn": "Arabic (Tunisia)",
		"ar-ae": "Arabic (U.A.E.)",
		"ar-001": "Arabic (World)",
		"ar-ye": "Arabic (Yemen)",
		"hy": "Armenian",
		"hy-am": "Armenian (Armenia)",
		"as": "Assamese",
		"as-in": "Assamese (India)",
		"ast": "Asturian",
		"ast-es": "Asturian (Spain)",
		"asa": "Asu",
		"asa-tz": "Asu (Tanzania)",
		"quz-x-ay-sdl": "Aymara",
		"az": "Azeri",
		"az-cyrl": "Azeri (Cyrillic)",
		"az-cyrl-az": "Azeri (Cyrillic, Azerbaijan)",
		"az-latn": "Azeri (Latin)",
		"az-latn-az": "Azeri (Latin, Azerbaijan)",
		"ksf": "Bafia",
		"ksf-cm": "Bafia (Cameroon)",
		"bm": "Bamanankan",
		"bm-latn": "Bamanankan (Latin)",
		"bm-latn-ml": "Bamanankan (Latin, Mali)",
		"bas": "Basaa",
		"bas-cm": "Basaa (Cameroon)",
		"ba": "Bashkir",
		"ba-ru": "Bashkir (Russia)",
		"eu": "Basque",
		"eu-es": "Basque (Basque)",
		"be": "Belarusian",
		"be-by": "Belarusian (Belarus)",
		"bem": "Bemba",
		"bem-zm": "Bemba (Zambia)",
		"bez": "Bena",
		"bez-tz": "Bena (Tanzania)",
		"bn": "Bengali",
		"bn-bd": "Bengali (Bangladesh)",
		"bn-in": "Bengali (India)",
		"hi-x-bh-sdl": "Bihari",
		"en-x-bi-sdl": "Bislama",
		"byn": "Blin",
		"byn-er": "Blin (Eritrea)",
		"brx": "Bodo",
		"brx-in": "Bodo (India)",
		"bs": "Bosnian",
		"bs-cyrl": "Bosnian (Cyrillic)",
		"bs-cyrl-ba": "Bosnian (Cyrillic, Bosnia and Herzegovina)",
		"bs-latn": "Bosnian (Latin)",
		"bs-latn-ba": "Bosnian (Latin, Bosnia and Herzegovina)",
		"br": "Breton",
		"br-fr": "Breton (France)",
		"bg": "Bulgarian",
		"bg-bg": "Bulgarian (Bulgaria)",
		"my": "Burmese",
		"my-mm": "Burmese (Myanmar)",
		"ca": "Catalan",
		"ca-ad": "Catalan (Andorra)",
		"ca-es": "Catalan (Catalan)",
		"ca-fr": "Catalan (France)",
		"ca-it": "Catalan (Italy)",
		"tzm-arab": "Central Atlas Tamazight (Arabic)",
		"tzm-arab-ma": "Central Atlas Tamazight (Arabic, Morocco)",
		"tzm-latn-ma": "Central Atlas Tamazight (Latin, Morocco)",
		"tzm-tfng": "Central Atlas Tamazight (Tifinagh)",
		"tzm-tfng-ma": "Central Atlas Tamazight (Tifinagh, Morocco)",
		"ku": "Central Kurdish",
		"ku-arab": "Central Kurdish (Arabic)",
		"ku-arab-iq": "Central Kurdish (Iraq)",
		"chr": "Cherokee",
		"chr-cher-us": "Cherokee (Cherokee)",
		"chr-cher": "Cherokee (Cherokee)",
		"sw-x-ny-sdl": "Chewa",
		"cgg": "Chiga",
		"cgg-ug": "Chiga (Uganda)",
		"zh": "Chinese",
		"zh-hans-hk": "Chinese (Simplified Han, Hong Kong SAR)",
		"zh-hans-mo": "Chinese (Simplified Han, Macao SAR)",
		"zh-hans": "Chinese (Simplified)",
		"zh-chs": "Chinese (Simplified) Legacy",
		"zh-cn": "Chinese (Simplified, PRC)",
		"zh-sg": "Chinese (Simplified, Singapore)",
		"zh-hant": "Chinese (Traditional)",
		"zh-cht": "Chinese (Traditional) Legacy",
		"zh-hk": "Chinese (Traditional, Hong Kong S.A.R.)",
		"zh-mo": "Chinese (Traditional, Macao S.A.R.)",
		"zh-tw": "Chinese (Traditional, Taiwan)",
		"swc": "Congo Swahili",
		"swc-cd": "Congo Swahili (Congo DRC)",
		"kw": "Cornish",
		"kw-gb": "Cornish (United Kingdom)",
		"co": "Corsican",
		"co-fr": "Corsican (France)",
		"hr": "Croatian",
		"hr-hr": "Croatian (Croatia)",
		"hr-ba": "Croatian (Latin, Bosnia and Herzegovina)",
		"cs": "Czech",
		"cs-cz": "Czech (Czech Republic)",
		"da": "Danish",
		"da-dk": "Danish (Denmark)",
		"da-gl": "Danish (Greenland)",
		"prs": "Dari",
		"prs-af": "Dari (Afghanistan)",
		"dv": "Divehi",
		"dv-mv": "Divehi (Maldives)",
		"dua": "Duala",
		"dua-cm": "Duala (Cameroon)",
		"nl": "Dutch",
		"nl-aw": "Dutch (Aruba)",
		"nl-be": "Dutch (Belgium)",
		"nl-bq": "Dutch (Bonaire, Sint Eustatius and Saba)",
		"nl-cw": "Dutch (Cura\u00e7ao)",
		"nl-nl": "Dutch (Netherlands)",
		"nl-sx": "Dutch (Sint Maarten)",
		"nl-sr": "Dutch (Suriname)",
		"dz": "Dzongkha",
		"dz-bt": "Dzongkha (Bhutan)",
		"bin": "Edo",
		"bin-ng": "Edo (Nigeria)",
		"ebu": "Embu",
		"ebu-ke": "Embu (Kenya)",
		"en": "English",
		"en-as": "English (American Samoa)",
		"en-ai": "English (Anguilla)",
		"en-ag": "English (Antigua and Barbuda)",
		"en-au": "English (Australia)",
		"en-bs": "English (Bahamas)",
		"en-bb": "English (Barbados)",
		"en-be": "English (Belgium)",
		"en-bz": "English (Belize)",
		"en-bm": "English (Bermuda)",
		"en-bw": "English (Botswana)",
		"en-io": "English (British Indian Ocean Territory)",
		"en-vg": "English (British Virgin Islands)",
		"en-cm": "English (Cameroon)",
		"en-ca": "English (Canada)",
		"en-029": "English (Caribbean)",
		"en-ky": "English (Cayman Islands)",
		"en-cx": "English (Christmas Island)",
		"en-cc": "English (Cocos (Keeling) Islands)",
		"en-ck": "English (Cook Islands)",
		"en-dm": "English (Dominica)",
		"en-er": "English (Eritrea)",
		"en-150": "English (Europe)",
		"en-fk": "English (Falkland Islands)",
		"en-fj": "English (Fiji)",
		"en-gm": "English (Gambia)",
		"en-gh": "English (Ghana)",
		"en-gi": "English (Gibraltar)",
		"en-gd": "English (Grenada)",
		"en-gu": "English (Guam)",
		"en-gg": "English (Guernsey)",
		"en-gy": "English (Guyana)",
		"en-hk": "English (Hong Kong)",
		"en-in": "English (India)",
		"en-id": "English (Indonesia)",
		"en-ie": "English (Ireland)",
		"en-im": "English (Isle of Man)",
		"en-jm": "English (Jamaica)",
		"en-je": "English (Jersey)",
		"en-ke": "English (Kenya)",
		"en-ki": "English (Kiribati)",
		"en-ls": "English (Lesotho)",
		"en-lr": "English (Liberia)",
		"en-mo": "English (Macao SAR)",
		"en-mg": "English (Madagascar)",
		"en-mw": "English (Malawi)",
		"en-my": "English (Malaysia)",
		"en-mt": "English (Malta)",
		"en-mh": "English (Marshall Islands)",
		"en-mu": "English (Mauritius)",
		"en-fm": "English (Micronesia)",
		"en-ms": "English (Montserrat)",
		"en-na": "English (Namibia)",
		"en-nr": "English (Nauru)",
		"en-nz": "English (New Zealand)",
		"en-ng": "English (Nigeria)",
		"en-nu": "English (Niue)",
		"en-nf": "English (Norfolk Island)",
		"en-mp": "English (Northern Mariana Islands)",
		"en-pk": "English (Pakistan)",
		"en-pw": "English (Palau)",
		"en-pg": "English (Papua New Guinea)",
		"en-pn": "English (Pitcairn Islands)",
		"en-pr": "English (Puerto Rico)",
		"en-ph": "English (Republic of the Philippines)",
		"en-rw": "English (Rwanda)",
		"en-kn": "English (Saint Kitts and Nevis)",
		"en-lc": "English (Saint Lucia)",
		"en-vc": "English (Saint Vincent and the Grenadines)",
		"en-ws": "English (Samoa)",
		"en-sc": "English (Seychelles)",
		"en-sl": "English (Sierra Leone)",
		"en-sg": "English (Singapore)",
		"en-sx": "English (Sint Maarten)",
		"en-sb": "English (Solomon Islands)",
		"en-za": "English (South Africa)",
		"en-ss": "English (South Sudan)",
		"en-sh": "English (St Helena, Ascension, Tristan da Cunha)",
		"en-sd": "English (Sudan)",
		"en-sz": "English (Swaziland)",
		"en-tz": "English (Tanzania)",
		"en-tk": "English (Tokelau)",
		"en-to": "English (Tonga)",
		"en-tt": "English (Trinidad and Tobago)",
		"en-tc": "English (Turks and Caicos Islands)",
		"en-tv": "English (Tuvalu)",
		"en-vi": "English (U.S. Virgin Islands)",
		"en-um": "English (US Minor Outlying Islands)",
		"en-ug": "English (Uganda)",
		"en-gb": "English (United Kingdom)",
		"en-us": "English (United States)",
		"en-vu": "English (Vanuatu)",
		"en-001": "English (World)",
		"en-zm": "English (Zambia)",
		"en-zw": "English (Zimbabwe)",
		"eo": "Esperanto",
		"eo-001": "Esperanto (World)",
		"et": "Estonian",
		"et-ee": "Estonian (Estonia)",
		"ee": "Ewe",
		"ee-gh": "Ewe (Ghana)",
		"ee-tg": "Ewe (Togo)",
		"ewo": "Ewondo",
		"ewo-cm": "Ewondo (Cameroon)",
		"fo": "Faroese",
		"fo-fo": "Faroese (Faroe Islands)",
		"en-x-fj-sdl": "Fijian",
		"fil": "Filipino",
		"fil-ph": "Filipino (Philippines)",
		"fi": "Finnish",
		"fi-fi": "Finnish (Finland)",
		"fr": "French",
		"fr-dz": "French (Algeria)",
		"fr-be": "French (Belgium)",
		"fr-bj": "French (Benin)",
		"fr-bf": "French (Burkina Faso)",
		"fr-bi": "French (Burundi)",
		"fr-cm": "French (Cameroon)",
		"fr-ca": "French (Canada)",
		"fr-029": "French (Caribbean)",
		"fr-cf": "French (Central African Republic)",
		"fr-td": "French (Chad)",
		"fr-km": "French (Comoros)",
		"fr-cd": "French (Congo [DRC])",
		"fr-cg": "French (Congo)",
		"fr-dj": "French (Djibouti)",
		"fr-gq": "French (Equatorial Guinea)",
		"fr-fr": "French (France)",
		"fr-gf": "French (French Guiana)",
		"fr-pf": "French (French Polynesia)",
		"fr-ga": "French (Gabon)",
		"fr-gp": "French (Guadeloupe)",
		"fr-gn": "French (Guinea)",
		"fr-ht": "French (Haiti)",
		"fr-ci": "French (Ivory Coast)",
		"fr-lu": "French (Luxembourg)",
		"fr-mg": "French (Madagascar)",
		"fr-ml": "French (Mali)",
		"fr-mq": "French (Martinique)",
		"fr-mr": "French (Mauritania)",
		"fr-mu": "French (Mauritius)",
		"fr-yt": "French (Mayotte)",
		"fr-mc": "French (Monaco)",
		"fr-ma": "French (Morocco)",
		"fr-nc": "French (New Caledonia)",
		"fr-ne": "French (Niger)",
		"fr-rw": "French (Rwanda)",
		"fr-re": "French (R\u00e9union)",
		"fr-bl": "French (Saint Barth\u00e9lemy)",
		"fr-mf": "French (Saint Martin)",
		"fr-pm": "French (Saint Pierre and Miquelon)",
		"fr-sn": "French (Senegal)",
		"fr-sc": "French (Seychelles)",
		"fr-ch": "French (Switzerland)",
		"fr-sy": "French (Syria)",
		"fr-tg": "French (Togo)",
		"fr-tn": "French (Tunisia)",
		"fr-vu": "French (Vanuatu)",
		"fr-wf": "French (Wallis and Futuna)",
		"fy": "Frisian",
		"fy-nl": "Frisian (Netherlands)",
		"fur": "Friulian",
		"fur-it": "Friulian (Italy)",
		"ff": "Fulah",
		"ff-cm": "Fulah (Cameroon)",
		"ff-gn": "Fulah (Guinea)",
		"ff-latn": "Fulah (Latin)",
		"ff-latn-sn": "Fulah (Latin, Senegal)",
		"ff-mr": "Fulah (Mauritania)",
		"ff-ng": "Fulah (Nigeria)",
		"gl": "Galician",
		"gl-es": "Galician (Galician)",
		"lg": "Ganda",
		"lg-ug": "Ganda (Uganda)",
		"ka": "Georgian",
		"ka-ge": "Georgian (Georgia)",
		"de": "German",
		"de-at": "German (Austria)",
		"de-be": "German (Belgium)",
		"de-de": "German (Germany)",
		"de-li": "German (Liechtenstein)",
		"de-lu": "German (Luxembourg)",
		"de-ch": "German (Switzerland)",
		"el": "Greek",
		"el-cy": "Greek (Cyprus)",
		"el-gr": "Greek (Greece)",
		"kl": "Greenlandic",
		"kl-gl": "Greenlandic (Greenland)",
		"gn": "Guarani",
		"gn-py": "Guarani (Paraguay)",
		"gu": "Gujarati",
		"gu-in": "Gujarati (India)",
		"guz": "Gusii",
		"guz-ke": "Gusii (Kenya)",
		"ha": "Hausa",
		"ha-latn": "Hausa (Latin)",
		"ha-latn-gh": "Hausa (Latin, Ghana)",
		"ha-latn-ne": "Hausa (Latin, Niger)",
		"ha-latn-ng": "Hausa (Latin, Nigeria)",
		"haw": "Hawaiian",
		"haw-us": "Hawaiian (United States)",
		"he": "Hebrew",
		"he-il": "Hebrew (Israel)",
		"hi": "Hindi",
		"hi-in": "Hindi (India)",
		"zh-x-hmn-sdl": "Hmong",
		"hu": "Hungarian",
		"hu-hu": "Hungarian (Hungary)",
		"ibb": "Ibibio",
		"ibb-ng": "Ibibio (Nigeria)",
		"is": "Icelandic",
		"is-is": "Icelandic (Iceland)",
		"ig": "Igbo",
		"ig-ng": "Igbo (Nigeria)",
		"id": "Indonesian",
		"id-id": "Indonesian (Indonesia)",
		"ia": "Interlingua",
		"ia-fr": "Interlingua (France)",
		"ia-001": "Interlingua (World)",
		"iu": "Inuktitut",
		"iu-latn": "Inuktitut (Latin)",
		"iu-latn-ca": "Inuktitut (Latin, Canada)",
		"iu-cans": "Inuktitut (Syllabics)",
		"iu-cans-ca": "Inuktitut (Syllabics, Canada)",
		"iu-x-ik-sdl": "Inupiaq",
		"ga": "Irish",
		"ga-ie": "Irish (Ireland)",
		"it": "Italian",
		"it-it": "Italian (Italy)",
		"it-sm": "Italian (San Marino)",
		"it-ch": "Italian (Switzerland)",
		"ja": "Japanese",
		"ja-jp": "Japanese (Japan)",
		"jv": "Javanese",
		"jv-latn-id": "Javanese (Indonesia)",
		"jv-java": "Javanese (Javanese)",
		"jv-java-id": "Javanese (Javanese, Indonesia)",
		"jv-latn": "Javanese (Latin)",
		"dyo": "Jola-Fonyi",
		"dyo-sn": "Jola-Fonyi (Senegal)",
		"quc": "K'iche'",
		"quc-latn-gt": "K'iche' (Guatemala)",
		"kea": "Kabuverdianu",
		"kea-cv": "Kabuverdianu (Cabo Verde)",
		"kab": "Kabyle",
		"kab-dz": "Kabyle (Algeria)",
		"kkj": "Kako",
		"kkj-cm": "Kako (Cameroon)",
		"kln": "Kalenjin",
		"kln-ke": "Kalenjin (Kenya)",
		"kam": "Kamba",
		"kam-ke": "Kamba (Kenya)",
		"kn": "Kannada",
		"kn-in": "Kannada (India)",
		"kr": "Kanuri",
		"kr-ng": "Kanuri (Nigeria)",
		"ks": "Kashmiri",
		"ks-deva": "Kashmiri (Devanagari)",
		"ks-deva-in": "Kashmiri (Devanagari, India)",
		"ks-arab-in": "Kashmiri (Perso-Arabic)",
		"kk": "Kazakh",
		"kk-kz": "Kazakh (Kazakhstan)",
		"km": "Khmer",
		"km-kh": "Khmer (Cambodia)",
		"ki": "Kikuyu",
		"ki-ke": "Kikuyu (Kenya)",
		"rw": "Kinyarwanda",
		"rw-rw": "Kinyarwanda (Rwanda)",
		"sw": "Kiswahili",
		"sw-ke": "Kiswahili (Kenya)",
		"sw-tz": "Kiswahili (Tanzania)",
		"sw-ug": "Kiswahili (Uganda)",
		"kok": "Konkani",
		"kok-in": "Konkani (India)",
		"ko": "Korean",
		"ko-kr": "Korean (Korea)",
		"khq": "Koyra Chiini",
		"khq-ml": "Koyra Chiini (Mali)",
		"ses": "Koyraboro Senni",
		"ses-ml": "Koyraboro Senni (Mali)",
		"nmg": "Kwasio",
		"nmg-cm": "Kwasio (Cameroon)",
		"ky": "Kyrgyz",
		"ky-kg": "Kyrgyz (Kyrgyzstan)",
		"lkt": "Lakota",
		"lkt-us": "Lakota (United States)",
		"lag": "Langi",
		"lag-tz": "Langi (Tanzania)",
		"lo": "Lao",
		"lo-la": "Lao (Lao P.D.R.)",
		"lv": "Latvian",
		"lv-lv": "Latvian (Latvia)",
		"ln": "Lingala",
		"ln-ao": "Lingala (Angola)",
		"ln-cf": "Lingala (Central African Republic)",
		"ln-cd": "Lingala (Congo DRC)",
		"ln-cg": "Lingala (Congo)",
		"lt": "Lithuanian",
		"lt-lt": "Lithuanian (Lithuania)",
		"dsb": "Lower Sorbian",
		"dsb-de": "Lower Sorbian (Germany)",
		"lu": "Luba-Katanga",
		"lu-cd": "Luba-Katanga (Congo DRC)",
		"luo": "Luo",
		"luo-ke": "Luo (Kenya)",
		"lb": "Luxembourgish",
		"lb-lu": "Luxembourgish (Luxembourg)",
		"luy": "Luyia",
		"luy-ke": "Luyia (Kenya)",
		"mk": "Macedonian (FYROM)",
		"mk-mk": "Macedonian (Former Yugoslav Republic of Macedonia)",
		"jmc": "Machame",
		"jmc-tz": "Machame (Tanzania)",
		"mgh": "Makhuwa-Meetto",
		"mgh-mz": "Makhuwa-Meetto (Mozambique)",
		"kde": "Makonde",
		"kde-tz": "Makonde (Tanzania)",
		"mg": "Malagasy",
		"mg-mg": "Malagasy (Madagascar)",
		"ms": "Malay",
		"ms-bn": "Malay (Brunei Darussalam)",
		"ms-sg": "Malay (Latin, Singapore)",
		"ms-my": "Malay (Malaysia)",
		"ml": "Malayalam",
		"ml-in": "Malayalam (India)",
		"mt": "Maltese",
		"mt-mt": "Maltese (Malta)",
		"mni": "Manipuri",
		"mni-in": "Manipuri (India)",
		"gv": "Manx",
		"gv-im": "Manx (Isle of Man)",
		"mi": "Maori",
		"mi-nz": "Maori (New Zealand)",
		"arn": "Mapudungun",
		"arn-cl": "Mapudungun (Chile)",
		"mr": "Marathi",
		"mr-in": "Marathi (India)",
		"mas": "Masai",
		"mas-ke": "Masai (Kenya)",
		"mas-tz": "Masai (Tanzania)",
		"mer": "Meru",
		"mer-ke": "Meru (Kenya)",
		"mgo": "Meta\u02bc",
		"mgo-cm": "Meta\u02bc (Cameroon)",
		"moh": "Mohawk",
		"moh-ca": "Mohawk (Mohawk)",
		"mn": "Mongolian",
		"mn-cyrl": "Mongolian (Cyrillic)",
		"mn-mn": "Mongolian (Cyrillic, Mongolia)",
		"mn-mong": "Mongolian (Traditional Mongolian)",
		"mn-mong-mn": "Mongolian (Traditional Mongolian, Mongolia)",
		"mn-mong-cn": "Mongolian (Traditional Mongolian, PRC)",
		"mfe": "Morisyen",
		"mfe-mu": "Morisyen (Mauritius)",
		"mua": "Mundang",
		"mua-cm": "Mundang (Cameroon)",
		"nqo": "N'ko",
		"nqo-gn": "N'ko (Guinea)",
		"naq": "Nama",
		"naq-na": "Nama (Namibia)",
		"haw-x-na-sdl": "Nauru",
		"ne": "Nepali",
		"ne-in": "Nepali (India)",
		"ne-np": "Nepali (Nepal)",
		"nnh": "Ngiemboon",
		"nnh-cm": "Ngiemboon (Cameroon)",
		"jgo": "Ngomba",
		"jgo-cm": "Ngomba (Cameroon)",
		"nd": "North Ndebele",
		"nd-zw": "North Ndebele (Zimbabwe)",
		"no": "Norwegian",
		"nb": "Norwegian (Bokm\u00e5l)",
		"nn": "Norwegian (Nynorsk)",
		"nb-sj": "Norwegian Bokm\u00e5l (Svalbard and Jan Mayen)",
		"nb-no": "Norwegian, Bokm\u00e5l (Norway)",
		"nn-no": "Norwegian, Nynorsk (Norway)",
		"nus": "Nuer",
		"nus-sd": "Nuer (Sudan)",
		"nyn": "Nyankole",
		"nyn-ug": "Nyankole (Uganda)",
		"oc": "Occitan",
		"oc-fr": "Occitan (France)",
		"or": "Oriya",
		"or-in": "Oriya (India)",
		"om": "Oromo",
		"om-et": "Oromo (Ethiopia)",
		"om-ke": "Oromo (Kenya)",
		"os": "Ossetian",
		"os-ge": "Ossetian (Georgia)",
		"os-ru": "Ossetian (Russia)",
		"pap": "Papiamento",
		"pap-029": "Papiamento (Caribbean)",
		"ps": "Pashto",
		"ps-af": "Pashto (Afghanistan)",
		"fa": "Persian",
		"fa-ir": "Persian",
		"pl": "Polish",
		"pl-pl": "Polish (Poland)",
		"pt": "Portuguese",
		"pt-ao": "Portuguese (Angola)",
		"pt-br": "Portuguese (Brazil)",
		"pt-cv": "Portuguese (Cabo Verde)",
		"pt-gw": "Portuguese (Guinea-Bissau)",
		"pt-mo": "Portuguese (Macao SAR)",
		"pt-mz": "Portuguese (Mozambique)",
		"pt-pt": "Portuguese (Portugal)",
		"pt-st": "Portuguese (S\u00e3o Tom\u00e9 and Pr\u00edncipe)",
		"pt-tl": "Portuguese (Timor-Leste)",
		"pa": "Punjabi",
		"pa-arab": "Punjabi (Arabic)",
		"pa-in": "Punjabi (India)",
		"pa-arab-pk": "Punjabi (Pakistan)",
		"quz": "Quechua",
		"quz-bo": "Quechua (Bolivia)",
		"quz-ec": "Quechua (Ecuador)",
		"quz-pe": "Quechua (Peru)",
		"ksh": "Ripuarian",
		"ksh-de": "Ripuarian (Germany)",
		"ro": "Romanian",
		"ro-md": "Romanian (Moldova)",
		"ro-ro": "Romanian (Romania)",
		"rm": "Romansh",
		"rm-ch": "Romansh (Switzerland)",
		"rof": "Rombo",
		"rof-tz": "Rombo (Tanzania)",
		"rn": "Rundi",
		"rn-bi": "Rundi (Burundi)",
		"ru": "Russian",
		"ru-by": "Russian (Belarus)",
		"ru-kz": "Russian (Kazakhstan)",
		"ru-kg": "Russian (Kyrgyzstan)",
		"ru-md": "Russian (Moldova)",
		"ru-ru": "Russian (Russia)",
		"ru-ua": "Russian (Ukraine)",
		"rwk": "Rwa",
		"rwk-tz": "Rwa (Tanzania)",
		"ssy": "Saho",
		"ssy-er": "Saho (Eritrea)",
		"saq": "Samburu",
		"saq-ke": "Samburu (Kenya)",
		"smn": "Sami (Inari)",
		"smj": "Sami (Lule)",
		"se": "Sami (Northern)",
		"sms": "Sami (Skolt)",
		"sma": "Sami (Southern)",
		"smn-fi": "Sami, Inari (Finland)",
		"smj-no": "Sami, Lule (Norway)",
		"smj-se": "Sami, Lule (Sweden)",
		"se-fi": "Sami, Northern (Finland)",
		"se-no": "Sami, Northern (Norway)",
		"se-se": "Sami, Northern (Sweden)",
		"sms-fi": "Sami, Skolt (Finland)",
		"sma-no": "Sami, Southern (Norway)",
		"sma-se": "Sami, Southern (Sweden)",
		"haw-x-sm-sdl": "Samoan",
		"sg": "Sango",
		"sg-cf": "Sango (Central African Republic)",
		"sbp": "Sangu",
		"sbp-tz": "Sangu (Tanzania)",
		"sa": "Sanskrit",
		"sa-in": "Sanskrit (India)",
		"gd": "Scottish Gaelic",
		"gd-gb": "Scottish Gaelic (United Kingdom)",
		"seh": "Sena",
		"seh-mz": "Sena (Mozambique)",
		"sr": "Serbian",
		"sr-cyrl": "Serbian (Cyrillic)",
		"sr-cyrl-ba": "Serbian (Cyrillic, Bosnia and Herzegovina)",
		"sr-cyrl-xk": "Serbian (Cyrillic, Kosovo)",
		"sr-cyrl-me": "Serbian (Cyrillic, Montenegro)",
		"sr-cyrl-cs": "Serbian (Cyrillic, Serbia and Montenegro (Former))",
		"sr-cyrl-rs": "Serbian (Cyrillic, Serbia)",
		"sr-latn": "Serbian (Latin)",
		"sr-latn-ba": "Serbian (Latin, Bosnia and Herzegovina)",
		"sr-latn-xk": "Serbian (Latin, Kosovo)",
		"sr-latn-me": "Serbian (Latin, Montenegro)",
		"sr-latn-cs": "Serbian (Latin, Serbia and Montenegro (Former))",
		"sr-latn-rs": "Serbian (Latin, Serbia)",
		"st-ls": "Sesotho (Lesotho)",
		"nso": "Sesotho sa Leboa",
		"nso-za": "Sesotho sa Leboa (South Africa)",
		"tn": "Setswana",
		"tn-bw": "Setswana (Botswana)",
		"tn-za": "Setswana (South Africa)",
		"ksb": "Shambala",
		"ksb-tz": "Shambala (Tanzania)",
		"sn": "Shona",
		"sn-latn": "Shona (Latin)",
		"sn-latn-zw": "Shona (Latin, Zimbabwe)",
		"sd-arab": "Sindhi",
		"sd": "Sindhi",
		"sd-deva": "Sindhi (Devanagari)",
		"sd-deva-in": "Sindhi (Devanagari, India)",
		"sd-arab-pk": "Sindhi (Pakistan)",
		"si": "Sinhala",
		"si-lk": "Sinhala (Sri Lanka)",
		"sk": "Slovak",
		"sk-sk": "Slovak (Slovakia)",
		"sl": "Slovenian",
		"sl-si": "Slovenian (Slovenia)",
		"xog": "Soga",
		"xog-ug": "Soga (Uganda)",
		"so": "Somali",
		"so-dj": "Somali (Djibouti)",
		"so-et": "Somali (Ethiopia)",
		"so-ke": "Somali (Kenya)",
		"so-so": "Somali (Somalia)",
		"nr": "South Ndebele",
		"nr-za": "South Ndebele (South Africa)",
		"st": "Southern Sotho",
		"st-za": "Southern Sotho (South Africa)",
		"es": "Spanish",
		"es-ar": "Spanish (Argentina)",
		"es-ve": "Spanish (Bolivarian Republic of Venezuela)",
		"es-bo": "Spanish (Bolivia)",
		"es-cl": "Spanish (Chile)",
		"es-co": "Spanish (Colombia)",
		"es-cr": "Spanish (Costa Rica)",
		"es-cu": "Spanish (Cuba)",
		"es-do": "Spanish (Dominican Republic)",
		"es-ec": "Spanish (Ecuador)",
		"es-sv": "Spanish (El Salvador)",
		"es-gq": "Spanish (Equatorial Guinea)",
		"es-gt": "Spanish (Guatemala)",
		"es-hn": "Spanish (Honduras)",
		"es-x-int-sdl": "Spanish (International)",
		"es-419": "Spanish (Latin America)",
		"x-es-xl": "Spanish (Latin America) deprecated",
		"es-mx": "Spanish (Mexico)",
		"es-x-mo-sdl": "Spanish (Modern)",
		"es-ni": "Spanish (Nicaragua)",
		"es-pa": "Spanish (Panama)",
		"es-py": "Spanish (Paraguay)",
		"es-pe": "Spanish (Peru)",
		"es-ph": "Spanish (Philippines)",
		"es-pr": "Spanish (Puerto Rico)",
		"es-es": "Spanish (Spain, International Sort)",
		"es-us": "Spanish (United States)",
		"es-uy": "Spanish (Uruguay)",
		"zgh": "Standard Morrocan Tamazight",
		"zgh-tfng": "Standard Morrocan Tamazight (Tifinagh)",
		"zgh-tfng-ma": "Standard Morrocan Tamazight (Tifinagh, Morocco)",
		"ml-x-su-sdl": "Sundanese (Roman)",
		"sw-cd": "Swahili (Congo - Kinshasa)",
		"sv": "Swedish",
		"sv-fi": "Swedish (Finland)",
		"sv-se": "Swedish (Sweden)",
		"sv-ax": "Swedish (\u00c5land Islands)",
		"syr": "Syriac",
		"syr-sy": "Syriac (Syria)",
		"shi": "Tachelhit",
		"shi-latn": "Tachelhit (Latin)",
		"shi-latn-ma": "Tachelhit (Latin, Morocco)",
		"shi-tfng": "Tachelhit (Tifinagh)",
		"shi-tfng-ma": "Tachelhit (Tifinagh, Morocco)",
		"tl-x-sdl": "Tagalog",
		"dav": "Taita",
		"dav-ke": "Taita (Kenya)",
		"tg": "Tajik",
		"tg-cyrl": "Tajik (Cyrillic)",
		"tg-cyrl-tj": "Tajik (Cyrillic, Tajikistan)",
		"tzm": "Tamazight",
		"tzm-latn": "Tamazight (Latin)",
		"tzm-latn-dz": "Tamazight (Latin, Algeria)",
		"ta": "Tamil",
		"ta-in": "Tamil (India)",
		"ta-my": "Tamil (Malaysia)",
		"ta-sg": "Tamil (Singapore)",
		"ta-lk": "Tamil (Sri Lanka)",
		"twq": "Tasawaq",
		"twq-ne": "Tasawaq (Niger)",
		"tt": "Tatar",
		"tt-ru": "Tatar (Russia)",
		"te": "Telugu",
		"te-in": "Telugu (India)",
		"teo": "Teso",
		"teo-ke": "Teso (Kenya)",
		"teo-ug": "Teso (Uganda)",
		"th": "Thai",
		"th-th": "Thai (Thailand)",
		"bo": "Tibetan",
		"bo-in": "Tibetan (India)",
		"bo-cn": "Tibetan (PRC)",
		"tig": "Tigre",
		"tig-er": "Tigre (Eritrea)",
		"ti": "Tigrinya",
		"ti-er": "Tigrinya (Eritrea)",
		"ti-et": "Tigrinya (Ethiopia)",
		"to": "Tongan",
		"to-to": "Tongan (Tonga)",
		"ts": "Tsonga",
		"ts-za": "Tsonga (South Africa)",
		"tr": "Turkish",
		"tr-cy": "Turkish (Cyprus)",
		"tr-tr": "Turkish (Turkey)",
		"tk": "Turkmen",
		"tk-tm": "Turkmen (Turkmenistan)",
		"uk": "Ukrainian",
		"uk-ua": "Ukrainian (Ukraine)",
		"hsb": "Upper Sorbian",
		"hsb-de": "Upper Sorbian (Germany)",
		"ur": "Urdu",
		"ur-in": "Urdu (India)",
		"ur-pk": "Urdu (Islamic Republic of Pakistan)",
		"ug": "Uyghur",
		"ug-cn": "Uyghur (PRC)",
		"uz": "Uzbek",
		"uz-cyrl": "Uzbek (Cyrillic)",
		"uz-cyrl-uz": "Uzbek (Cyrillic, Uzbekistan)",
		"uz-latn": "Uzbek (Latin)",
		"uz-latn-uz": "Uzbek (Latin, Uzbekistan)",
		"uz-arab": "Uzbek (Perso-Arabic)",
		"uz-arab-af": "Uzbek (Perso-Arabic, Afghanistan)",
		"vai": "Vai",
		"vai-latn": "Vai (Latin)",
		"vai-latn-lr": "Vai (Latin, Liberia)",
		"vai-vaii": "Vai (Vai)",
		"vai-vaii-lr": "Vai (Vai, Liberia)",
		"ca-es-valencia": "Valencian (Spain)",
		"ve": "Venda",
		"ve-za": "Venda (South Africa)",
		"vi": "Vietnamese",
		"vi-vn": "Vietnamese (Vietnam)",
		"vo": "Volap\u00fck",
		"vo-001": "Volap\u00fck (World)",
		"vun": "Vunjo",
		"vun-tz": "Vunjo (Tanzania)",
		"wae": "Walser",
		"wae-ch": "Walser (Switzerland)",
		"cy": "Welsh",
		"cy-gb": "Welsh (United Kingdom)",
		"wal": "Wolaytta",
		"wal-et": "Wolaytta (Ethiopia)",
		"wo": "Wolof",
		"wo-sn": "Wolof (Senegal)",
		"sah": "Yakut",
		"sah-ru": "Yakut (Russia)",
		"yav": "Yangben",
		"yav-cm": "Yangben (Cameroon)",
		"ii": "Yi",
		"ii-cn": "Yi (PRC)",
		"yi": "Yiddish",
		"yi-001": "Yiddish (World)",
		"yo": "Yoruba",
		"yo-bj": "Yoruba (Benin)",
		"yo-ng": "Yoruba (Nigeria)",
		"dje": "Zarma",
		"dje-ne": "Zarma (Niger)",
		"xh": "isiXhosa",
		"xh-za": "isiXhosa (South Africa)",
		"zu": "isiZulu",
		"zu-za": "isiZulu (South Africa)",
		"ss": "siSwati",
		"ss-za": "siSwati (South Africa)",
		"ss-sz": "siSwati (Swaziland)"}
}

function getLanguageName(languageCode) {
	return flagsDictionary["en"][languageCode.toLowerCase()];
};

function getFlagHTML(language) {
	return '<i class="flag  ' + language.toLowerCase() + '" data-qtip="' + language + '"></i>\xa0';
};


function getFlagsTooltips(sourceLanguage, targetLanguages) {
	if(sourceLanguage == "" && targetLanguages == ""){
		return "";
	}
	var table = '<table><tr><td>' + renderer(sourceLanguage) + '</td>';
	var languagesArray = targetLanguages.split(',');
	var newLine = true;
	for (var i = 0; i < languagesArray.length; i++)
	{
		if (!newLine)
		{
			table +=  '<tr><td></td>';
		}
		table += '<td> <i class="fa fa-caret-right" aria-hidden="true"></i> ' + renderer(languagesArray[i]) + '</td>';
		table += '</tr>';
		newLine = false;
	}
	table += '</table>';
	return table;
};

function renderer (language) {
	return getFlagHTML(language) + ' ' + flagsDictionary["en"][language.toLowerCase()];
};