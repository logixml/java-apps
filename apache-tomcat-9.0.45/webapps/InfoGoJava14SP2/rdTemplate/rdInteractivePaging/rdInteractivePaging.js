YUI.add("rdInteractivePaging", function (Y) {

    //create namespaced plugin class
    Y.namespace("LogiXML").rdInteractivePaging = Y.Base.create("rdInteractivePaging", Y.Base, [], {
        _listCaptionsElementId: "",
        iScrollHeiht : 0,
        initializer: function () {
        },
        destructor: function () {
        },
    }, {
            NAME: "rdInteractivePaging",
            NS: "rdInteractivePaging",
            ATTRS: {
            },
            
            AttachAutoScrolling: function (divID) {
                Y.LogiXML.rdInteractivePaging._recalculateScrollArea(divID)
                LogiXML.Ajax.AjaxTarget().on('reinitialize', function (e) { Y.LogiXML.rdInteractivePaging._recalculateScrollArea(divID) });
            },
            ResetScrollingInDocument: function () {
                var scrollDivs = Y.all("DIV[rdScrollRowsY],DIV[rdScrollRowsX]")
                for (var i = 0; i < scrollDivs.size(); i++)
                    Y.LogiXML.rdInteractivePaging._recalculateScrollArea(scrollDivs.item(i).get("id"))
            },
            
             /**
              * PRIVATE
              */
            _recalculateScrollArea: function (divID) {
                var divNode = document.getElementById(divID)
                if (divNode == undefined) return;

                var maxRowCount = parseInt(divNode.getAttribute("rdScrollRowsY"))
                var scrolledNode = divNode.querySelector("#" + divID.replace("_scrolling", ""));

                divNode.style.maxHeight = ""//for PROTECTION (such as 'can not caculate', 'Not support', 'Not need')

                if (scrolledNode.tagName == "TABLE") {
                    var tableNode = scrolledNode;
                    var tBody = tableNode.tBodies.item(0) //JUST 1 in this case

                    if (maxRowCount < (tBody ? tBody.childElementCount : 0 )) {
                        //Note: Getting size (especially the table-size was set as relative value) MUST SKIP the scroll-DIV, set maxwidth=100% or width=100% to scroll-div is a simplest way.
                        var tableHeader = tableNode.tHead;
                        var sumHeight = tableHeader.offsetHeight

                        if (sumHeight == 0) {
                            //HEIGHT is 'auto' (or it does not displayed, or error)->can not caculate
                            return;
                        }
                        var tBodyChildren = tBody.children;
                        for (var i = 0; i < maxRowCount; i++) {
                            var e = tBodyChildren.item(i)
                            if (e.tagName == "TR") {
                                sumHeight = sumHeight + e.getBoundingClientRect().height
                            }
                        }
                        //BorderSpace Fixing
                        var iVerticalBorderSpace = 0
                        if (window.getComputedStyle(tableNode).getPropertyValue("border-collapse") == "separate") {                            
                            var sVerticalBorderspace = window.getComputedStyle(tableNode).getPropertyValue("-webkit-border-vertical-spacing");
                            if (!sVerticalBorderspace) {
                                var temp = window.getComputedStyle(tableNode).getPropertyValue("border-spacing");
                                if (temp) {
                                    temp = temp.split(" ");
                                    if (temp.length >= 2) sVerticalBorderspace = temp[1];
                                    else sVerticalBorderspace = temp[0];
                                }
                            }
                            if (sVerticalBorderspace) {
                                iVerticalBorderSpace = parseFloat(sVerticalBorderspace);//1. In Edge, it is Float-value; 2. Rightnow, the unit always pixel.
                            }
                        }
                        sumHeight = sumHeight + (maxRowCount + 2) * iVerticalBorderSpace; //+2 : + top of Header+buttom of last TR

                        //Caption Fixing
                        var allCapNodes = tableNode.getElementsByTagName("Caption")
                        for (var i = 0; i < allCapNodes.length; i++) {
                            var capNode = allCapNodes.item(i);
                            sumHeight = sumHeight + capNode.offsetHeight + iVerticalBorderSpace                           
                        }

                        //H-ScrollBar Fixing
                        new function () {
                            var el = document.createElement('div');
                            el.style.visibility = 'hidden';
                            el.style.overflow = 'scroll';
                            divNode.appendChild(el);
                            VSCROLL_W = el.offsetWidth - el.clientWidth;
                            HSCROLL_H = el.offsetHeight - el.clientHeight;
                            divNode.removeChild(el);
                        }();//Is there any way to get scroll's size?
                        if (isIE()) {
                            //In some IE version, clientWidth gets 0
                            if (divNode.clientWidth > 0 && divNode.clientWidth < tableNode.offsetWidth) {
                                sumHeight = sumHeight + HSCROLL_H;
                            }
                        } else {
                            if (divNode.clientWidth < tableNode.offsetWidth) {
                                sumHeight = sumHeight + HSCROLL_H;
                            }
                        }
                        //Width Fixing
                        //1) If table's width has been set relative(such as %), 'fit-content' would make scrollbar away from the table
                        //2) Note: IE not support 'Fit-Content', it should be set a directly value.
                        divNode.style.maxWidth = tableNode.offsetWidth + VSCROLL_W + "px";
                        //add scroll-behavior:soomth?? 
                        divNode.style.maxHeight = Math.ceil(sumHeight) + "px"
                        divNode.style.overflowY = "auto";

                        tableHeader.style.position = "sticky";
                        tableHeader.style.top = "0px";
                        //LOCK POPUPMENU ON THEADER.
                        var popupDivs = tableHeader.querySelectorAll("span>div.rdPopupMenu");//The Class condition please CHECK sSetAction on rdServer
                        for (var i = 0; i < popupDivs.length; i++) {
                            var xNode = document.createElement("div")
                            xNode.setAttribute("style", "position:fixed");
                            var dNode = popupDivs.item(i);
                            dNode.parentNode.replaceChild(xNode, dNode);
                            xNode.appendChild(dNode);
                        }
                    }
                }
            },
    });

}, "1.0.0", { requires: ["base", "plugin", "json"] });

