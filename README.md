# Logi Java Applications

### Catalog of Joe Viscomes' Java Test Applications

- Apache Tomcat (10, 9, 8)
- Wildfly 18.1

### To run the applications:

Each individual may have their own method of running a Java web server, however, these are the steps I would take to start the Apache Tomcat servers. This method provides the logs from standard out directly in the opened terminal; allowing you to more clearly see when an error occurs.

- Open a Terminal window
- `cd` to the bin directory of the Tomcat instance
- Run this command to change the JDK version used: `$Env:JAVA_HOME="<path to JDK version>"`
  - For example: `$Env:JAVA_HOME="C:\Program Files\Java\openjdk-11"`
- Then, run the Tomcat instance with `./startup.bat`

### Contents

Each web server contains test applications in their respective deployment folders. For the Apache Tomcat servers (9 and 8), there is also an **_offline_** folder where applications that were not being used could be placed to take them out of the startup process.

The Wildfly server was a test of an application deployment, but may be useful for review.

#### Application Structure

For these applications, the naming convention is as follows:

- Application Name should follow the Support\_\<year\> nomenclature
- Most report definitions are named or included in a folder with the associated Case Number
  - This case number may be an old Salesforce or Zendesk number, depending on the age of the application
