function convertTime(time, format) {

	var d = new Date(time);
	var fmt = "";
	
	if (format == 'date') {
		var dd = d.getDate();
		var mm = d.getMonth()+1; 
		var yyyy = d.getFullYear();
	
		if(dd<10) {
    		dd='0'+dd;
		} 

		if(mm<10) {
   			mm='0'+mm;
		}
		fmt= yyyy + '-' + mm + '-' + dd;
	} else if (format == 'time') {
		var hh = d.getHours();
		var mm = d.getMinutes();
		var ss = d.getSeconds();
		
		if(hh<10) {
    		hh='0'+hh;
		}
		if(mm<10) {
    		mm='0'+mm;
		}
		if(ss<10) {
    		ss='0'+ss;
		}
		fmt= hh + ':' + mm + ':' + ss;
	
	
	}
	//There are definitely better ways to do this. This is the most straightforward way. 
	//fmt = d.getFullYear().toString()+ '/'+ (d.getMonth()+1).toString() + '/' + d.getDate().toString() + ' ' + d.getHours().toString() + ':' + d.getMinutes().toString() + ':' + d.getSeconds().toString();
	
	return fmt;
}