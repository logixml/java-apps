$(document).ready(function() {
	if(document.title.indexOf('Lambda') == -1)
		document.title += ' | Lambda Ex | Logi Analytics';
		
	loadScript('_SupportFiles/js.highlight.min.js', function() {
		//$('pre').wrapInner('<code></code>');
		//hljs.initHighlightingOnLoad();
		$('pre').each(function(i, e) {
			$(e).wrapInner('<code></code>');
			hljs.highlightBlock(e.querySelector('code'))
		});
	});
	
	loadScript('_SupportFiles/clipboard.clipboard.min.js', function() {
		var copyBtns = document.querySelectorAll('.btnCopy');
		var clipboard;
		for (var i = 0; i < copyBtns.length; i++) {
			clipboard = new Clipboard(copyBtns[i], {
				text: function(trigger) {
					return $(trigger).parent().parent().parent().find('[id^="copyXMLElement"]').text()
				}
			});
			clipboard.on('success', function(e) {
				showTooltip(e.trigger, 'Copied')
			});
			copyBtns[i].addEventListener('mouseleave', function(e) {
				e.currentTarget.removeAttribute('aria-label')
			});
			setupTooltipAnimation($(copyBtns[i]))
		}
	});
	
	$('pre:contains("[tilde]"):contains("[at]"), #copyXMLElement:contains("[tilde]"):contains("[at]")').each(function() {
		var replaced = $(this).html().replaceAll('[tilde]', '~').replaceAll('[at]', '@');
		$(this).html(replaced);
	});
	$('.pre-well').click(function() {
    	var selection = window.getSelection();
    	if(selection.toString().length === 0) {
			$(this).toggleClass('expand');
			if($(this).hasClass('expand')) {
				var totalHeight = 0;
				$(this).children().each(function(){
					totalHeight = totalHeight + $(this).outerHeight(true);
				});
				$(this).css('max-height', totalHeight+30);
			}
			else {
				$(this).removeAttr('style');
			}
		}
	})
});

function loadScript ( src, callback ) {
    var script = document.createElement ('script');
    script.type = 'text/javascript';
    script.async = true;
    script.onerror = function ( err )
    {
        throw new URIError('The script ' + err.target.src + ' is not accessible.');
    }
    if(callback && typeof callback == 'function')
        script.onload = callback;
    document.head.appendChild(script);
    script.src = src;
	console.log('Script loaded');
}

function isEllipsisActive(e) {
     return (e.offsetWidth < e.scrollWidth);
}

var getCSS = function (prop, fromClass) {
    var $inspector = $("<div>").css('display', 'none').addClass(fromClass);
    $("body").append($inspector); // add to DOM, in order to read the CSS property
    try {
        return $inspector.css(prop);
    } finally {
        $inspector.remove(); // and remove from DOM
    }
};




function urlParam(param) {
    param = param.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
    var regex = new RegExp("[\\?&]" + param + "=([^&#]*)"),
        results = regex.exec(location.search);
    return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
}

// Start cookie set, get, and remove functions
	function setCookie(n,v,days) {
		if (days) {
			var date = new Date();
			date.setTime(date.getTime()+(days*24*60*60*1000));
			var expires = "; expires="+date.toGMTString();
		}
		else var expires = "";
		document.cookie = n+"="+v+expires+"; path=/";
	}

	function readCookie(n) {
		var nameEQ = n + "=";
		var ca = document.cookie.split(';');
		for(var i=0;i < ca.length;i++) {
			var c = ca[i];
			while (c.charAt(0)==' ') c = c.substring(1,c.length);
			if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
		}
		return null;
	}

	function eraseCookie(n) {
		setCookie(n,"",-1);
	}
function reflowCharts(){
	//Get all charts on the page
	var chartsArray = Highcharts.charts.filter(function(n){ return n != undefined });
	
	//Loop through the array
	$.each(chartsArray,function(i){
		//Update the current chart
		chartsArray[i].reflow();
	});
}
window.mobilecheck = function() {
  var check = false;
  (function(a){if(/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino/i.test(a)||/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0,4))) check = true;})(navigator.userAgent||navigator.vendor||window.opera);
  return check;
};

function showTooltip(elem, msg) {
    addClass(elem, 'tooltipped');
    elem.setAttribute('aria-label', msg);
}

function fallbackMessage(action) {
    var actionMsg = '';
    var actionKey = (action === 'cut' ? 'X' : 'C');
    if (/iPhone|iPad/i.test(navigator.userAgent)) {
        actionMsg = 'No support :(';
    } else if (/Mac/i.test(navigator.userAgent)) {
        actionMsg = 'Press ⌘-' + actionKey + ' to ' + action;
    } else {
        actionMsg = 'Press Ctrl-' + actionKey + ' to ' + action;
    }
    return actionMsg;
}
function setupTooltipAnimation(elem, copyText) {
	if(typeof(copyText) == 'undefined')
		copyText='Copied to clipboard!';
	elem.tooltip({title: copyText, trigger: 'manual'}).on('shown.bs.tooltip', function (e) {
		setTimeout(function () {
			$(e.target).tooltip('hide');
			removeClass(e.target, 'tooltipped');
		}, 1500);
	}).click(function() {
		$(this).tooltip('show');
	});
}

function hasClass(el, className) {
  if (el.classList)
    return el.classList.contains(className)
  else
    return !!el.className.match(new RegExp('(\\s|^)' + className + '(\\s|$)'))
}

function addClass(el, className) {
  if (el.classList)
    el.classList.add(className)
  else if (!hasClass(el, className)) el.className += " " + className
}

function removeClass(el, className) {
  if (el.classList)
    el.classList.remove(className)
  else if (hasClass(el, className)) {
    var reg = new RegExp('(\\s|^)' + className + '(\\s|$)')
    el.className=el.className.replace(reg, ' ')
  }
}
String.prototype.replaceAll = function(str1, str2, ignore) { return this.replace(new RegExp(str1.replace(/([\/\,\!\\\^\$\{\}\[\]\(\)\.\*\+\?\|\<\>\-\&])/g,"\\$&"),(ignore?"gi":"g")),(typeof(str2)=="string")?str2.replace(/\$/g,"$$$$"):str2); }