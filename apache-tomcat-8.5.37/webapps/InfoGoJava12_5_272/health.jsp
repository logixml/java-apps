<%@ page language="java" contentType="application/json"%>
<%@ page import="java.text.*,java.util.*" %>
<%@page import="java.io.*" %>
<%@page import="java.net.*" %>
<%@page import="java.lang.*" %>
<%@page import="java.lang.StringBuffer" %>
<%@ page import="java.util.*,javax.servlet.jsp.JspWriter,java.io.*" %>
<%@ page import="java.io.FileNotFoundException, java.io.IOException, java.io.InputStreamReader, java.util.ArrayList, java.util.Map, java.util.logging.Logger" %>

<%!

  public void cookieCheck( CookieManager cm ) {
  CookieStore cookieStore = cm.getCookieStore();

  List<HttpCookie> cookieList = cookieStore.getCookies();
  System.out.println("Cookie List: " + cookieList.size());

    // iterate HttpCookie object
    for (HttpCookie cookie : cookieList)
    {
      // gets domain set for the cookie
      System.out.println("Domain: " + cookie.getDomain());

      // gets max age of the cookie
      System.out.println("max age: " + cookie.getMaxAge());

      // gets name cookie
      System.out.println("name of cookie: " + cookie.getName());

      // gets path of the server
      System.out.println("server path: " + cookie.getPath());

      // gets boolean if cookie is being sent with secure protocol
      System.out.println("is cookie secure: " + cookie.getSecure());

      // gets the value of the cookie
      System.out.println("value of cookie: " + cookie.getValue());

      // gets the version of the protocol with which the given cookie is related.
      System.out.println("value of version: " + cookie.getVersion());


    }

  }
%>

<%!
  public String tail( File file ) {
      RandomAccessFile fileHandler = null;
      try {
          fileHandler = new RandomAccessFile( file, "r" );
          long fileLength = fileHandler.length() - 1;
          StringBuilder sb = new StringBuilder();

          for(long filePointer = fileLength; filePointer != -1; filePointer--){
              fileHandler.seek( filePointer );
              int readByte = fileHandler.readByte();

              if( readByte == 0xA ) {
                  if( filePointer == fileLength ) {
                      continue;
                  }
                  break;

              } else if( readByte == 0xD ) {
                  if( filePointer == fileLength - 1 ) {
                      continue;
                  }
                  break;
              }

              sb.append( ( char ) readByte );
          }

          String lastLine = sb.reverse().toString();
          if (lastLine == null || lastLine.isEmpty()) {
            return null;
          }
          return lastLine;
      } catch( java.io.FileNotFoundException e ) {
          e.printStackTrace();
          return null;
      } catch( java.io.IOException e ) {
          e.printStackTrace();
          return null;
      } finally {
          if (fileHandler != null )
              try {
                  fileHandler.close();
              } catch (IOException e) {
                  /* ignore */
              }
      }
  }
%>

<%!

  public String get_status(File logFile){

    String lastLine = null;

    try(FileWriter fw = new FileWriter(logFile, true))
    {
      lastLine = tail(logFile);
    } catch (IOException e) {

        System.out.println(e);
  e.printStackTrace();

    }

    return lastLine;

  }

%>

<%!
  public String write_status(File logFile, String status, String writeType) throws IOException {

    String pattern = "yyyy.MM.dd HH:mm:ss";
    SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
    String date = simpleDateFormat.format(new Date());

    try(PrintWriter out = new PrintWriter(logFile))
    {
    	out.println(date + " " + status + " " + writeType);
    } catch (IOException e) {

        System.out.println(e);
	e.printStackTrace();

    }
    return date + " " + status + " " + writeType;

  }
%>

<%!
  public String run_healthcheck() {

    String result = null;

    String authName = "${BISERVICE_BASIC_AUTH_USER}";
    String authPw = "${BISERVICE_BASIC_AUTH_PWD}";
    String username = "Joe";
    String authuser = "5a288ff05049431f28c6062d";
    String corpid = "6115";

    String urlServer = "http://127.0.0.1:8080";
    String getKeyUrl = "/InfoGoJava12_5_272/rdTemplate/rdGetSecureKey.aspx?username=" + username +
                       "&authUser=" + authuser +
                       "&corporateId=" + corpid +
                       "&locale=en_us&originatingId=SecureKey";
    String secureUrl = "/InfoGoJava12_5_272/rdPage.aspx?";
    String healthUrl = "/InfoGoJava12_5_272/rdPage.aspx?rdLoadBookmark=True&rdReport=InfoGo.goAnalysisGrid&rdBookmarkCollection=JoegoCollection&rdBookmarkUserName=Joe&rdBookmarkID=536ea8af-c94d-4d85-9832-15e36dfefa2a&rdSharedBookmarkID=&goBookmarkCaption=Basic+Health+Check&rdIsDefaultReportBookmark=";

    BufferedReader getKeyInput = null;
    BufferedReader healthReportInput = null;
    HttpURLConnection connGetKey = null;
    HttpURLConnection conn = null;
    int getKeyCode;
    int secureCode;
    int healthReportCode;
    String userCredentials = null;
    String basicAuth = null;
    String getKeyInputLine = null;
    String healthReportHtml = null;
    String healthReportInputLine = null;
    String secureKey = null;
    String successString = "agCol_HealthCheck_BasicHealthCheck_Row1\">Success";
    StringBuffer getKeyResp = null;
    StringBuffer healthReportResp = null;
    URL url = null;
    URL loginUrl = null;
    URL secureReportUrl = null;
    URL healthReportUrl = null;

    CookieManager cookieManager = new CookieManager(null, CookiePolicy.ACCEPT_ALL);
    CookieHandler.setDefault(cookieManager);

    try {

       try {
         url = new URL(urlServer + getKeyUrl);
         connGetKey = (HttpURLConnection)url.openConnection();
         userCredentials = authName + ":" + authPw;
         basicAuth = "Basic " + java.util.Base64.getEncoder().encodeToString(userCredentials.getBytes());
         connGetKey.setRequestProperty ("Authorization", basicAuth);
         connGetKey.setRequestMethod("GET");
         connGetKey.connect();

         getKeyCode = connGetKey.getResponseCode();
         System.out.println("\nSending 'GET' request to url : " + url);
         System.out.println("Response Code : " + getKeyCode + "\n");


         cookieCheck(cookieManager);

         getKeyInput = new BufferedReader(new InputStreamReader(connGetKey.getInputStream()));
         getKeyResp = new StringBuffer();

         while ((getKeyInputLine = getKeyInput.readLine()) != null) {
            getKeyResp.append(getKeyInputLine);
         }

        secureKey = "&rdSecureKey=" + getKeyResp.toString();
      }
      catch (Exception e) {
        System.out.println("Exception occurred.");
        System.out.println(e);
        e.printStackTrace();
      }
      finally {
        try {
        getKeyInput.close();
        getKeyResp.setLength(0);
        connGetKey.disconnect();
    cookieManager.getCookieStore().removeAll();
        }
        catch (Exception e) {
            System.out.println("Exception occurred.");
            System.out.println(e);
            e.printStackTrace();
        }
      }

      try {

    secureReportUrl = new URL(urlServer + secureUrl + secureKey);
    System.out.println("Secure URL (redirects to home): " + secureReportUrl);
    conn = (HttpURLConnection)secureReportUrl.openConnection();
    conn.connect();

    System.out.println("Run Cookie Check");
    cookieCheck(cookieManager);

    secureCode = conn.getResponseCode();
        System.out.println("\nSending 'GET' request to healthReportUrl : " + secureReportUrl);
        System.out.println("Response Code : " + secureCode + "\n");

        healthReportUrl = new URL(urlServer + healthUrl);
        System.out.println("Health URL : " + healthReportUrl);
        conn = (HttpURLConnection)healthReportUrl.openConnection();
    conn.connect();

              cookieCheck(cookieManager);

              healthReportCode = conn.getResponseCode();
              System.out.println("\nSending 'GET' request to healthReportUrl : " + healthReportUrl);
              System.out.println("Response Code : " + healthReportCode + "\n");

              healthReportInput = new BufferedReader(new InputStreamReader(conn.getInputStream()));
              healthReportResp = new StringBuffer();

              while ((healthReportInputLine = healthReportInput.readLine()) != null) {
                 healthReportResp.append(healthReportInputLine);
              }

              healthReportHtml = healthReportResp.toString();
        System.out.println("healthReportHtml: " + healthReportHtml);


      }
      catch (Exception e) {
        System.out.println("Exception occurred.");
        System.out.println(e);
        e.printStackTrace();
      }
      finally {
        try {
            healthReportResp.setLength(0);
            healthReportInput.close();
            conn.disconnect();
        }
        catch (Exception e) {
            System.out.println("Exception occurred.");
            System.out.println(e);
            e.printStackTrace();
        }
      }

      if (healthReportHtml.contains(successString)) {
        result = "{\"status\":\"Ok\"}";
      }
      else {
        result = "{\"status\": Could not find success in HealthReport. }";
      }
    }
    catch (Exception e) {
      System.out.println("Exception occurred.");
      System.out.println(e);
      e.printStackTrace();
    }

    return result;

  }
%>

<%
  // Main Function

  // int healthInterval = 15;
  // File logFile = new File("/usr/local/tomcat/webapps/ROOT/bihealth.log");
  // String lastStatus = get_status(logFile);
  String displayStatus = null;
   String displayStatusMessage = null;

  // if (lastStatus != null){

    // String arr[] = lastStatus.split(" ", 4);
    // SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy.MM.dd HH:mm:ss");
    // Date lastStatusTime = dateFormat.parse(arr[0] + " " + arr[1]);
    // Date currentTime = dateFormat.parse(dateFormat.format(new Date()));
    // long diffInSec = Math.abs(currentTime.getTime() - lastStatusTime.getTime()) / 1000;

    // String lastStatusMessage = arr[2];

    // if(diffInSec > healthInterval){

      // String cacheStatus = write_status(logFile, lastStatusMessage, "Cached");
      // String newStatusMessage = run_healthcheck();
      // displayStatus = write_status(logFile, newStatusMessage, "Exec");

    // }
    // else {

      // displayStatus = get_status(logFile);

    // }
  // }
  // else{
  	String newStatusMessage = run_healthcheck();
  	// displayStatus = write_status(logFile, newStatusMessage, "Exec");
  // }

  // String displayStatusArr[] = displayStatus.split(" ", 4);
  displayStatusMessage = newStatusMessage; //displayStatusArr[2];

%>

<%= displayStatusMessage %>
